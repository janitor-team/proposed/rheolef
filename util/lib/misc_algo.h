# ifndef _RHEOLEF_MISC_ALGO_H
# define _RHEOLEF_MISC_ALGO_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
/// various algorithms that cannot go elsewhere

#include "rheolef/config.h"

namespace rheolef {
namespace algo {

// from STL but not in std c++ lib
template<typename InputIterator, typename Size, typename OutputIterator>
OutputIterator
copy_n (InputIterator first, Size n, OutputIterator result)
{
  for (; n > 0; --n) {
    *result = *first;
    ++first;
    ++result;
  }
  return result;
}

} // namespace algo
} // namespace rheolef
# endif /* _RHEOLEF_MISC_ALGO_H */
