#ifndef _RHEOLEF_ROUNDER_H
#define _RHEOLEF_ROUNDER_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/compiler.h"
#include <cmath>

namespace rheolef {

template <class T>
struct rounder_type : std::unary_function<T,T> {
  rounder_type (const T& prec) : _prec(prec) {}
  T operator() (const T& x) const {
     // use floor : std::round() is non-standard (INTEL C++)
     T value = _prec*floor(x/_prec + 0.5);
     if (1+value == 1) value = T(0);
     return value;
  }
  T _prec;
};
template <class T>
struct floorer_type : std::unary_function<T,T> {
  floorer_type (const T& prec) : _prec(prec) {}
  T operator() (const T& x) const { 
     T value = _prec*floor(x/_prec);
     if (1+value == 1) value = T(0);
     return value;
  }
  T _prec;
};
template <class T>
struct ceiler_type : std::unary_function<T,T> {
  ceiler_type (const T& prec) : _prec(prec) {}
  T operator() (const T& x) const { 
     T value = _prec*ceil(x/_prec);
     if (1+value == 1) value = T(0);
     return value;
  }
  T _prec;
};

template <class T> rounder_type<T> rounder (const T& prec) { return rounder_type<T>(prec); }
template <class T> floorer_type<T> floorer (const T& prec) { return floorer_type<T>(prec); }
template <class T> ceiler_type<T>  ceiler  (const T& prec) { return ceiler_type<T>(prec); }

}//namespace rheolef
#endif // _RHEOLEF_ROUNDER_H
