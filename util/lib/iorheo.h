# ifndef _IORHEO_H
# define _IORHEO_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/iorheobase.h"
#include <bitset>
namespace rheolef { 
/*Class:iorheo
NAME: @code{iorheo} - input and output functions and manipulation 
@cindex graphic render
@clindex iorheo
@clindex csr
@clindex geo
@clindex field
@fiindex @file{.geo} mesh
@fiindex @file{.hb} Harwell-Boeing matrix
@fiindex @file{.mm} Matrix-Market matrix
@fiindex @file{.field} field
@fiindex @file{.tcl} tool command language
@fiindex @file{.vtk} visualization toolkit
@fiindex @file{.off} geomview data
@fiindex @file{.plot} gnuplot script
@fiindex @file{.gdat} gnuplot data
@fiindex @file{.mtv} plotmtv
@fiindex @file{.x3d} x3d mesh
@fiindex @file{.atom} PlotM mesh
@toindex @code{gnuplot}
@toindex @code{plotmtv}
@toindex @code{vtk}
@toindex @code{geomview}
@toindex @code{PlotM}
SMALL PIECES OF CODE:
  input geo in standard file format:
@example
        cin >> g;
@end example
@noindent
    output geo in standard file format:
@example
        cout << g;
@end example
@noindent
  output geo in gnuplot format:
@example
        cout << gnuplot << g;
@end example

DESCRIPTION:
    @noindent
    output manipulators enable the selection of
    some pretty graphic options, in an elegant fashion.

BOOLEAN MANIPULATORS:
    @noindent 
    The boolean manipulators set an internal optional flag.
    Each option has its negative counterpart, as 
    @code{verbose} and @code{noverbose}, by adding the
    @code{no} prefix.
    @example
    	cout << noverbose << a;
    @end example
    @table @code
    @item verbose
       trace some details, such as loading, storing or
       unix command operations on @code{cerr}.
       Default is on.
    @item clean
       delete temporary files during graphic outputs.
       Default is on.
    @item execute
       run unix operations, such as @code{gnuplot} or
       @code{plotmtv} or @code{vtk}. Note that the corresponding
       files are created.
       Default is on.
    @item transpose
       perform transposition when loading/soring a @code{csr} matrix
       from Harwell-Boeing file.
       This feature is available, since the file format store
       matrix in transposed format.
       Default is off.
    @item logscale
       when using matrix sparse postscript plot manipulator @code{ps}
       and @code{color}. The color scale is related to a logarithmic
       scale.
    @item fill
    @itemx grid
    @itemx shrink
    @itemx tube
    @itemx ball
    @itemx full
    @itemx stereo
    @itemx cut
    @itemx iso
    @itemx split
       when using the @code{vtk} or @code{paraview} manipulators
       for a mesh or a field.
    
    @item volume
	volume rendering by using ray cast functions.
@cindex velocity
@cindex deformation

    @item velocity
    @itemx deformation
       Vector-valued fields are rendered by using arrows (@code{velocity})
	or deformed meshes (@code{deformation}).
       For @code{vtk} or @code{plotmtv} rendering.

@cindex elevation

    @item elevation
	Scalar valued fields in two dimension are rendered by using
	a tridimensionnal surface elevation.
       	For @code{vtk} or @code{plotmtv} rendering.

@fiindex @file{.vtk} visualization toolkit
@toindex @code{paraview}
@clindex branch

    @item skipvtk
	For the @code{branch} class.
	Combined with @code{paraview}, only create the main python @file{.py} file,
        do not regenerate all the @file{.vtk} data files.

    @end table

@findex fastfieldload
    @table @code
    @item fastfieldload
       try to reuse the supplied space. Default is on.
    @end table

FILE FORMAT MANIPULATORS:
@fiindex @file{.bamg} bamg mesh
@fiindex @file{.mesh} mmg3d mesh
@fiindex @file{.node} tetgen mesh nodes
@fiindex @file{.ele} tetgen mesh elements
@fiindex @file{.face} tetgen mesh boundary faces
@fiindex @file{.v} grummp tridimensionnal mesh
@fiindex @file{.m} grummp bidimensionnal mesh
@fiindex @file{.gmsh} gmsh mesh
@fiindex @file{.gmsh_pos} gmsh metric mesh
@fiindex @file{.hb} Harwell-Boeing matrix
@fiindex @file{.geo} mesh
@fiindex @file{.field} field
@fiindex @file{.m} matlab matrix
@fiindex @file{.ps} postscript
@fiindex @file{.vtk} visualization toolkit
@fiindex @file{.plot} gnuplot script
@fiindex @file{.mtv} plotmtv
@fiindex @file{.x3d} x3d mesh
@fiindex @file{.atom} PlotM mesh
@toindex @code{vtk}
@toindex @code{gnuplot}
@toindex @code{plotmtv}
@toindex @code{x3d}
@toindex @code{PlotM}
@toindex @code{bamg}
@toindex @code{mmg3d}
@toindex @code{tetgen}

    The @dfn{format} manipulator group applies for streams.
    Its value is an enumerated type, containing the
    following possibilities:
    @table @code
    @item rheo
	use the default textual input/output file format.
	For instance, this is
	@file{.geo} for meshes, 
	@file{.field} for discretized functions.
	This default format is specified in the corresponding
	class documentation
	(see also @ref{geo class} and @ref{field class}).
	This is the default.
    @item bamg
	uses @file{.bamg} Frederic Hecht's bidimensional anisotropic
	mesh generator file format for geo input/output operation. 
    @item tetgen
	uses @file{.node} @file{.ele} and @code{.face} Hang Si's tridimensional 
	mesh generator file format for geo input/output operation. 
    @item mmg3d
	uses @file{.mmg3d} Cecile Dobrzynski's tridimensional anisotropic
	mesh generator file format for geo input/output operation. 
    @item gmsh
	uses @file{.gmsh} 
	gmsh Christophe Geuzaine and Jean-Francois Remacle 
        mesh generator file format for geo input/output operation. 
    @item gmsh_pos
	uses @file{.gmsh_pos} 
	gmsh Christophe Geuzaine and Jean-Francois Remacle 
        mesh metric file format for geo adapt input/output operation. 
    @item grummp
	uses @file{.m} (bidimensional) or @file{.v} (tridimensionnal)
	Carl Ollivier-Gooch 's mesh generator file format for geo input/output
	operation. 
    @item qmg
	uses @file{.qmg}
	Stephen A. Vavasis's mesh generator file format for geo input/output
	operation. 
    @item vtkdata
	uses @file{.vtk}
	mesh file format for geo input/output operations.
	This file format is suitable for graphic treatment.
    @item vtkpolydata
	uses @file{.vtk} polydata (specific for polygonal boundaries)
	mesh file format for geo input/output operations.
	This file format is suitable for graphic treatment.

@fiindex @file{.cemagref} cemagref topographic mesh
@cindex  @code{cemagref} topographic mesh
@cindex topography
    @item cemagref
	uses @file{.cemagref} surface mesh (topography, with a @code{z} cote).
	This file format is used at Cemagref (french research center for mountains,
	@url{http://www.cemagref.fr}).
    @item dump
	output an extensive listing of the class data structure.
	This option is used for debugging purpose.
    @item hb
	uses @file{.hb} Harwell-Boeing file format for sparse matrix input/output
	operation. 
	This is the default.
    @item matrix_market
	uses @file{.mm} Matrix-Market file format for sparse matrix input/output
	operation. 
    @item ml
    @itemx matlab
	uses @file{.m} Matlab file format for sparse matrix output
	operation. 
    @item sparse_matlab
	uses @file{.m} Matlab sparse file format for sparse matrix output
	operation. 
    @item ps
	uses @file{.ps} postscript for sparse matrix output
	operation. 
    @item vtk
	for mesh and field outputs.
	Generate @file{.vtk} data file and the
	@file{.tcl} command script file
	and run the @code{vtk} command
	on the @file{.tcl}. 
@fiindex @file{.py} python script file (for paraview visualization tool)
@toindex @code{paraview}
    @item paraview
	for field outputs.
	Generate @file{.vtk} data file and the
	@file{.py} command script file
	and run the @code{python} command on the @file{.py}
	associated to the @code{paraview}/@code{vtk} library.
    @item geomview
	for boundary cad outputs.
	Generate @file{.off} data file
	and run the @code{geomview} command.
    @item gnuplot
	for mesh and field outputs.
	Generate @file{.gdat} data file and the
	@file{.plot} command script file
	and run the @code{gnuplot} command
	on the @file{.plot}. 
    @item plotmtv
	for mesh and field outputs.
	Generate @file{.mtv} data file
	and run the @code{plotmtv} command.
    @item x3d
	for mesh output.
	Generate @file{.x3d} data file
	and run the @code{x3d} command.
	This tool has fast rotation rendering.
    @item atom
	for mesh output.
	Generate @file{.atom} data file
	and run the @code{PlotM} command.
	Tridimensional mesh rendering is performed
	as a chemical molecule: nodes as balls and
	edges as tubes.
	The @code{PlotM} tool is developed
	at Cornell University Laboratory of Atomic and Solid
	State Physics (LASSP) in a Joint Study with IBM, with support by
	the Materials Science Center and Corning Glassworks.
    @end table
COLOR MANIPULATORS:
    The @dfn{color} manipulator group acts for
    sparse matrix postscript output. Its value is an
    enumerated type, containing three possibilities:
    @example
    	cout << color << a;
    	cout << gray  << b;
    	cout << black_and_white  << c;
    @end example
    The default is to generate a color postcript file.
    Conversely, its act for field rendering, via @code{paraview}.

VALUATED MANIPULATORS:
@pindex mfield
    Some manipulators takes an argument that specifies a value. 
    @example
        cout << geomview << bezieradapt << subdivide(5) << my_cad_boundary;
    	cout << vtk << iso << isovalue(2.5) << my_mesh;
    	cout << velocity << plotmtv << vectorscale(0.1) << uh;
    @end example
@clindex catchmark
    See also @ref{catchmark algorithm} for input-output of vector-valued fields.
    @table @code
    @item  isovalue @var{float}
    @itemx n_isovalue @var{int}
    @itemx n_isovalue_negative @var{int}
	Used by 2D and 3D graphic renders.
    @item  vectorscale @var{float}
	Used for vector field visualizattion.
@toindex @code{paraview}
    @item  subdivide @var{float}
	Used for high-order curved mesh visualizatiion
        and also for high-order picewise polynomial fields.
        From paraview-5.5, this features is obsolete.
    @item label @var{string}
        For colorbar anotation.
    @item  rounding_precision @var{float}
	This manipulator set a rounding precision that could be used
	by some output functions, e.g. by the @code{geo} class.
	By default, the rounding value is zero and there is no rounding.
@cindex image file format
@cindex graphic render
@fiindex @file{.png} image 
@fiindex @file{.jpg} image
@fiindex @file{.pdf} image
    @item image_format @var{string}
	The argument is any valid image format, such as @code{png}, @code{jpg} or @code{pdf},
	that could be handled by the corresponding graphic render.
    @item branch_counter @var{int}
	Counter for even outputed to a stream.
	Used internally by the branch class during i/o.
    @end table
End:*/

class iorheo {
public:
	// ------------------------------------------------------
	// 1) booleans = set of bits
	// ------------------------------------------------------
	enum fmt_mode {				// specific to one stream

		// static field are positionned simultaneoulsy for all streams
		// as global variables:

		verbose,			// print comments
		execute,			// execute graphic commands
		clean,				// clear temporary graphic files

     		// locals:

		transpose,			// avoid hb-matrix(csc) to/in csr conversion: 
						// thus, handle the transposed matrix
		upgrade,	 		// rheolef upgrade file format version

		// color plot options

		black_and_white,
		gray,
		color,

		// file formats:

		rheo,	 		        // rheolef file format
		hb = rheo,			// Harwell-Boeing mat-vec file fmt
		matrix_market,			// matrix market Cerfacs file format
		ml,				// Matlab mat-vec file fmt
		matlab = ml,			// alias
		sparse_matlab,			// Matlab sparse matrix format
		dump,				// Dump format (debug)

		bamg,				// bamg F. hecht mesh & metric format
		peschetola,			// Custom meshes by Valentina Peschetola
		grummp,				// grummp mesh file format
		gmsh,				// gmsh mesh file format
		gmsh_pos,			// gmsh metric mesh file format
		mmg3d,				// mmg3d mesh format
		tetgen,				// tetgen mesh format
		qmg,				// qmg mesh file format
		cemagref,			// topography mesh file format at Cemagref
		vtkdata,			// vtk mesh file format
		vtkpolydata,			// vtk boundary mesh file format

		// renderer:

		ps,				// Postscript plot
		gnuplot,
		plotmtv,
		x3d,
		vtk,				// Visualization ToolKit
		paraview,			// paraview based on vtk
		geomview,			// geomview
		atom,				// PlotM, from Cornell

		// render option are not exclusive:
		logscale,			// log scale (for hb+ps)
		grid,				// grid rendering (mesh)
		domains,			// domains rendering
		fill,				// fill faces with light effects
		tube,				// edges as tube
		ball,				// vertices as balls
		shrink,				// shrink 3d geo elements
		full,				// all edges in 3d
		stereo,				// stereo rendering
		cut,				// cut and clip 3d mesh/field with a plane
		iso,				// isosurface for a specified value
		split,				// P2-iso-P1 element split
		velocity,			// vectors rendered as arrows
		deformation,			// vectors rendered as deformed meshes
                elevation,              	// 2d scalar field as 3d surface elevation
                volume,   	           	// 3d scalar field visualization by using ray cast functions (paraview)
                fastfieldload,          	// try to reuse supplied mesh space when loading field
                bezieradapt, 	  	       	// subdivide each Bezier subpatch instead of global patch, for cad/geomview format
                lattice, 	  	       	// subdivide each element by two, as in P2-iso-P1, for geo/vtkdata format
                showlabel, 	 	      	// write labels on plotmtv contour lines
		autovscale,			// adapt vscale to actual length of vectors and meshsize
		skipvtk,			// with paraview, only create the .py file, do not regenerate all .vtk
		reader_on_stdin,		// when read on stdin, add -persist to gnuplot

		last
	};
        typedef std::bitset<last>  flag_type;
        typedef size_t        size_type;

	static flag_type static_field;
	static flag_type color_field;
	static flag_type format_field;
	static flag_type render_field;

    private:
	// default and current flags as a long (32 bits available)
	static flag_type default_f;

    public:
	// ------------------------------------------------------
# define boolean(stream,name) iorheobase_def_boolean_accessor_macro(iorheo,std::stream,name)
# define member(stream,name,groupe) iorheobase_def_member_macro(iorheo,std::stream,name,groupe)
# define i_scalar(t, a)  iorheobase_def_scalar_macro(iorheo,t, a)
# define o_scalar(t, a)  iorheobase_def_scalar_macro(iorheo,t, a)
# define io_scalar(t, a) iorheobase_def_scalar_macro(iorheo,t, a)
# include "rheolef/iorheo-members.h"
# undef boolean
# undef member
# undef i_scalar
# undef o_scalar
# undef io_scalar
	// ------------------------------------------------------
	// 3) some implementation details
	// ------------------------------------------------------
public:
	//     basics members
	iorheo();
	~iorheo();

        static flag_type flags  (std::ios& s);
        static flag_type flags  (std::ios& s, flag_type f);
        static flag_type setf   (std::ios& s, size_type i_add);
        static flag_type setf   (std::ios& s, size_type i_add, flag_type field);
        static flag_type unsetf (std::ios& s, size_type i_del);

private:
	// flags: shared and stream-local one
	static flag_type  globals_ /* = default_f & static_field */;
        flag_type         flags_;

        // protected manipulations (see also class ios)
        flag_type flags () const;
        flag_type flags (flag_type f1);

        flag_type setf   (size_type i_add);
        flag_type setf   (size_type i_add, flag_type field);
        flag_type unsetf (size_type i_del);

        // ------------------------------------------------------
        // 4) memory handler
        // ------------------------------------------------------
protected:
        // local memory handler
        static iorheo* get_pointer (std::ios& s);
public:
	struct force_initialization {
      	      force_initialization ();
	};
};
// -------------------------------------------------------------------------
// define iostream manipulators outside of the class for all scalars
// -------------------------------------------------------------------------
# define boolean(stream,name) iorheobase_manip_boolean_accessor_macro(iorheo,std::stream,name)
# define member(stream,name,groupe) iorheobase_manip_member_macro(iorheo,std::stream,name,groupe)
# define i_scalar(t, a)  iorheobase_manip_scalar_macro(iorheo,t, a, i)
# define o_scalar(t, a)  iorheobase_manip_scalar_macro(iorheo,t, a, o)
# define io_scalar(t, a) iorheobase_manip_scalar_macro(iorheo,t, a, io)
# include "rheolef/iorheo-members.h"
# undef boolean
# undef member
# undef i_scalar
# undef o_scalar
# undef io_scalar
}// namespace rheolef
# endif /* _IORHEO_H */
