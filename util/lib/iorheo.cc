///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// Input/Output option management
//
// author: Pierre.Saramito@imag.fr
//
// date: 4 february 1997
//
// HOW TO ADD NEW OPTION ?
//
//  1st case: it's only a boolean flag, e.g. 
//	       cout << color
//    => add it 
//       a) as an enumerate flag in `enum fmt_mode'
//       b) and in the `boolean' iorheo.h declaration
// Jocelyn: also add it below in "force intialization" !!!
//
//  2nd case: you want to store e.g. a string or an int. e.g.
//            cout << ncolor(10)
//    => add it 
//       a) in iorheo.h in `scalar' declaration
//       b) in iorheo.c in `RHEO_SCALAR'
//       c) in iorheo constructor initializer
//
# include "rheolef/iorheo.h"
namespace rheolef {
using namespace std;

//
// 		RHEO_SCALAR's
//
# define boolean(stream,name)
# define member(stream,name,groupe)
# define i_scalar(t,a)  iorheobase_io_scalar_body_macro(iorheo,t,a)
# define o_scalar(t,a)  iorheobase_io_scalar_body_macro(iorheo,t,a)
# define io_scalar(t,a) iorheobase_io_scalar_body_macro(iorheo,t,a)
# include "rheolef/iorheo-members.h"
# undef boolean
# undef member
# undef i_scalar
# undef o_scalar
# undef io_scalar

//
// static member declaration:
//
iorheo::flag_type iorheo::static_field;
iorheo::flag_type iorheo::color_field;
iorheo::flag_type iorheo::format_field;
iorheo::flag_type iorheo::render_field;

iorheo::flag_type iorheo::default_f;
iorheo::flag_type iorheo::globals_;
//
// initialization of static data member
//
#define IORHEO_PREFIX
iorheo::force_initialization::force_initialization() {

	IORHEO_PREFIX default_f [verbose] = true;
	IORHEO_PREFIX default_f [execute] = true;
	IORHEO_PREFIX default_f [clean] = true;
	IORHEO_PREFIX default_f [rheo] = true;
	IORHEO_PREFIX default_f [color] = true;
	IORHEO_PREFIX default_f [fill] = true;
	IORHEO_PREFIX default_f [fastfieldload] = true;
	IORHEO_PREFIX default_f [bezieradapt] = true;

	IORHEO_PREFIX static_field [verbose] = true;
	IORHEO_PREFIX static_field [execute] = true;
	IORHEO_PREFIX static_field [clean]   = true;

	IORHEO_PREFIX color_field [black_and_white] = true;
	IORHEO_PREFIX color_field [gray]            = true;
	IORHEO_PREFIX color_field [color]           = true;
 
	IORHEO_PREFIX format_field [rheo] = true;

	IORHEO_PREFIX format_field [hb] = true;
	IORHEO_PREFIX format_field [matrix_market] = true;
	IORHEO_PREFIX format_field [ml] = true;
	IORHEO_PREFIX format_field [matlab] = true;
	IORHEO_PREFIX format_field [sparse_matlab] = true;
	IORHEO_PREFIX format_field [dump] = true;

	IORHEO_PREFIX format_field [bamg] = true;
	IORHEO_PREFIX format_field [peschetola] = true;
	IORHEO_PREFIX format_field [grummp] = true;
	IORHEO_PREFIX format_field [gmsh] = true;
	IORHEO_PREFIX format_field [gmsh_pos] = true;
	IORHEO_PREFIX format_field [mmg3d] = true;
	IORHEO_PREFIX format_field [tetgen] = true;
	IORHEO_PREFIX format_field [qmg] = true;
	IORHEO_PREFIX format_field [vtkdata] = true;
	IORHEO_PREFIX format_field [vtkpolydata] = true;

	IORHEO_PREFIX format_field [ps] = true;
	IORHEO_PREFIX format_field [gnuplot] = true;
	IORHEO_PREFIX format_field [plotmtv] = true;
	IORHEO_PREFIX format_field [vtk] = true;
	IORHEO_PREFIX format_field [paraview] = true;
	IORHEO_PREFIX format_field [geomview] = true;
	IORHEO_PREFIX format_field [x3d] = true;
	IORHEO_PREFIX format_field [atom] = true;

	IORHEO_PREFIX render_field [logscale] = true;
	IORHEO_PREFIX render_field [grid] = true;
	IORHEO_PREFIX render_field [domains] = true;
	IORHEO_PREFIX render_field [fill] = true;
	IORHEO_PREFIX render_field [tube] = true;
	IORHEO_PREFIX render_field [ball] = true;
	IORHEO_PREFIX render_field [shrink] = true;
	IORHEO_PREFIX render_field [full] = true;
	IORHEO_PREFIX render_field [stereo] = true;
	IORHEO_PREFIX render_field [cut] = true;
	IORHEO_PREFIX render_field [iso] = true;
	IORHEO_PREFIX render_field [split] = true;
	IORHEO_PREFIX render_field [volume] = true;
	IORHEO_PREFIX render_field [velocity] = true;
	IORHEO_PREFIX render_field [deformation] = true;
	IORHEO_PREFIX render_field [lattice] = true;
	IORHEO_PREFIX render_field [showlabel] = true;
	IORHEO_PREFIX render_field [skipvtk] = false;
	IORHEO_PREFIX render_field [reader_on_stdin] = true;

        IORHEO_PREFIX globals_ = default_f & static_field;
}
static iorheo::force_initialization dummy;
//
// constructor and destructor, copy and assignement
//
iorheo::iorheo()
 :
    rhstype_(string("")),
    rhsfmt_(string("")),
    nrhs_(0),
    ivec_(0),
    nptr_(0),
    nidx_(0),
    line_no_(0),
    subdivide_(0),
    ncolor_(0),
    basename_(string("")),
    image_format_(string("")),
    mark_(string("")),
    label_(string("")),
    isovalue_(std::numeric_limits<Float>::max()),
    n_isovalue_(15),
    n_isovalue_negative_(0),
    vectorscale_(1),
    anglecorner_(0),
    rounding_precision_(0),
    branch_counter_(0),
    flags_()
{
    // initialized may be called before main() starts
    // constructor of global variable "dummy" is used for that
    // but some compiler/linker do not call constructor of global vars
    check_macro (default_f != flag_type(), "static data member not initialized");

    flags_ = default_f;
}
iorheo::~iorheo()
{
    // trace_macro ("iorheo::dstor");
}
// ---------------------------------------------------------------------
// trivial memory handler instanciation
// ---------------------------------------------------------------------

// static variable initialization in template class:
template <>
list<iorheo*> *iorheobase_memory_handler<iorheo>::pointer_list = 0;

// class instanciation:
template class iorheobase_memory_handler<iorheo>;

iorheo* 
iorheo::get_pointer (ios& s)
{
    return iorheobase_memory_handler<iorheo>::get_pointer(s);
}
// ---------------------------------------------------------------------
// flags
// ---------------------------------------------------------------------
iorheo::flag_type 
iorheo::flags () const      
{
	flag_type ret_f = (globals_ & static_field) | (flags_   & (~static_field));
	return ret_f;
}
iorheo::flag_type
iorheo::flags (flag_type f1) 
{ 
	flag_type old_f = flags(); 
        globals_ = f1 & static_field;
	flags_   = f1 & (~static_field);
	return old_f; 
}
iorheo::flag_type 
iorheo::setf (size_type i_add) 
{ 
	flag_type new_f = flags();
	new_f [i_add] = true;
	return flags(new_f);
}
iorheo::flag_type 
iorheo::setf (size_type i_add, flag_type field) 
{
	flag_type new_f =  flags() & (~field);
        new_f [i_add] = field [i_add];
        return flags(new_f);
}
iorheo::flag_type 
iorheo::unsetf (size_type i_del) 
{ 
	flag_type new_f =  flags();
        new_f [i_del] = false;
	return flags(new_f);
}
//
// for extern usage at application level
//
iorheo::flag_type 
iorheo::flags (ios& s)
{ 
	return iorheo::get_pointer(s) -> flags();
}
iorheo::flag_type 
iorheo::flags (ios& s, flag_type f)
{ 
	return iorheo::get_pointer(s) -> flags(f);
}
iorheo::flag_type 
iorheo::setf   (ios& s, size_type i_add)
{ 
	return iorheo::get_pointer(s) -> setf (i_add);
}
iorheo::flag_type 
iorheo::setf   (ios& s, size_type i_add, flag_type field)
{ 
	return iorheo::get_pointer(s) -> setf (i_add, field);
}
iorheo::flag_type 
iorheo::unsetf (ios& s, size_type i_del) 
{ 
	return iorheo::get_pointer(s) -> unsetf (i_del);
}

} // namespace rheolef
