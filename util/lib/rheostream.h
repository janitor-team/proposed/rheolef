#ifndef _RHEOLEF_STREAM_H
#define _RHEOLEF_STREAM_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// author: Pierre.Saramito@imag.fr
// date:   31 october 1997

namespace rheolef {
/**
@utilclassfile rheostream i/o utilities
@addindex RHEOPATH environment variable

Description
===========
Here, utilities for stream input and output are suitable are reviewed.
Two classes are provided, together with a set of useful functions.

Output stream
=============
The `irheostream` is suitable for a sequential memory envinement:
for a distributed memory and parallel computation case,
see the @ref diststream_2 class.
Indeed, the `irheostream` is used as a base class for the
`idiststream` one.

File decompresion is assumed using
`gzip` and a recursive search in a directory list
is provided for input.

        orheostream foo("NAME", "suffix");

is like

        ofstream foo("NAME.suffix").

However, if `NAME` does not end with `.suffix`, then
`.suffix` is automatically added.
By default, compression is performed on the fly with `gzip`,
adding an additional `.gz` suffix.
The `flush` action is nicely handled in compression mode:

        foo.flush();

This feature allows intermediate results to be available during long computations.
The compression can be deactivated while opening a file by an optional argument:

        orheostream foo("NAME", "suffix", io::nogz);

An existing compressed file can be reopen in `append` mode: new results will
be appended at the end of an existing file:

        orheostream foo("NAME", "suffix", io::app);

Input stream
============
Conversely,

        irheostream foo("NAME","suffix");

is like

        ifstream foo("NAME.suffix").
  
However, we look at a search path environment variable 
`RHEOPATH` in order to find `NAME` while suffix is assumed.
Moreover, `gzip` compressed files, ending
with the `.gz` suffix is assumed, and decompression is done.

Options
=======
The following code:

    irheostream is("results", "data");

will recursively look for a `results[.data[.gz]]` file in the
rectory mentioned by the `RHEOPATH` environment variable.

For instance, if you insert in our ".cshrc" something like:

        setenv RHEOPATH ".:/home/dupont:/usr/local/math/demo"

the process will study the current directory `.`, then, if
neither `square.data.gz` nor `square.data` exits, it scan
all subdirectory of the current directory. Then, if file
is not founded, it start recusively in `/home/dupond`
and then in `/usr/local/math/demo`.

File decompression is performed by using
the `gzip` command, and data are pipe-lined 
directly in memory.

If the file start with `.` as `./square` or with a `/`
as `/home/oscar/square`, no search occurs and `RHEOPATH`
environment variable is not used.

Also, if the environment variable `RHEOPATH` is not set, the
default value is the current directory `.`.

For output stream:

        orheostream os("newresults", "data");

file compression is assumed, and "newresults.data.gz" will be created.

File loading and storing are mentioned by a message, either:

        ! load "./results.data.gz"

or:

        ! file "./newresults.data.gz" created.

on the `clog` stream.
By adding the following:

        clog << noverbose;

you turn off these messages.

Usefull functions
=================
@snippet rheostream.h verbatim_functions

Implementation
==============
@showfromfile

@snippet rheostream.h verbatim_irheostream
@snippet rheostream.h verbatim_irheostream_cont
@snippet rheostream.h verbatim_orheostream
@snippet rheostream.h verbatim_orheostream_cont
*/
}// namespace rheolef

#include "rheolef/compiler.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#include <boost/iostreams/filtering_stream.hpp>
#pragma GCC diagnostic pop

namespace rheolef {

// input & output  modes:
namespace io {
  typedef enum {
    out  = 0,
    app  = 1, // append mode
    gz   = 0, // default is to use gzip
    nogz = 2  // force not using gzip
  } mode_type;
}
// [verbatim_irheostream]
class irheostream : public boost::iostreams::filtering_stream<boost::iostreams::input> {
public:
    irheostream() : boost::iostreams::filtering_stream<boost::iostreams::input>(), _ifs() {}
    irheostream(const std::string& name, const std::string& suffix = std::string());
    virtual ~irheostream();
    void open  (const std::string& name, const std::string& suffix = std::string());
    void close();
// [verbatim_irheostream]
protected:
    std::ifstream _ifs;
// [verbatim_irheostream_cont]
};
// [verbatim_irheostream_cont]
static const bool dont_gzip = false;
// [verbatim_orheostream]
class orheostream : public boost::iostreams::filtering_stream<boost::iostreams::output> {
public:
    orheostream() : boost::iostreams::filtering_stream<boost::iostreams::output>(), _mode(), _full_name() {}
    orheostream(const std::string& name, const std::string& suffix = std::string(),
	io::mode_type mode = io::out);
    virtual ~orheostream();
    void open  (const std::string& name, const std::string& suffix = std::string(),
	io::mode_type mode = io::out);
    void flush();
    void close();
    const std::string& filename() const { return _full_name; }
// [verbatim_orheostream]
protected:
    void  _open_internal (io::mode_type mode);
    void _close_internal ();
// data:
    io::mode_type     _mode;
    std::string       _full_name;
// [verbatim_orheostream_cont]
};
// [verbatim_orheostream_cont]

// [verbatim_functions]
//! @brief itof: see the @ref rheostream_7 page for the full documentation
// float-to-string conversion
std::string ftos (const Float& x);

//! @brief scatch: see the @ref rheostream_7 page for the full documentation
// catch first occurrence of string in file
bool scatch (std::istream& in, const std::string& ch, bool full_match = true);

//! @brief has_suffix: see the @ref rheostream_7 page for the full documentation
// has_suffix("toto.suffix", "suffix") -> true
bool has_suffix (const std::string& name, const std::string& suffix);

//! @brief delete_suffix: see the @ref rheostream_7 page for the full documentation
// "toto.suffix" --> "toto"
std::string delete_suffix (const std::string& name, const std::string& suffix);

//! @brief has_any_suffix: see the @ref rheostream_7 page for the full documentation
// has_any_suffix("toto.any_suffix") -> true
bool has_any_suffix (const std::string& name);

//! @brief delete_any_suffix: see the @ref rheostream_7 page for the full documentation
// delete_any_suffix("toto.any_suffix") --> "toto"
std::string delete_any_suffix (const std::string& name);

//! @brief get_basename: see the @ref rheostream_7 page for the full documentation
// "/usr/local/dir/toto.suffix" --> "toto.suffix"
std::string get_basename (const std::string& name);

//! @brief get_dirname: see the @ref rheostream_7 page for the full documentation
// "/usr/local/dir/toto.suffix" --> "/usr/local/dir"
std::string get_dirname (const std::string& name);

//! @brief get_full_name_from_rheo_path: see the @ref rheostream_7 page for the full documentation
// "toto" --> "/usr/local/math/data/toto.suffix"
std::string get_full_name_from_rheo_path (const std::string& rootname, const std::string& suffix);

//! @brief append_dir_to_rheo_path: see the @ref rheostream_7 page for the full documentation
// "." + "../geodir" --> ".:../geodir"
void append_dir_to_rheo_path (const std::string& dir);

//! @brief prepend_dir_to_rheo_path: see the @ref rheostream_7 page for the full documentation
// "../geodir" + "." --> "../geodir:."
void prepend_dir_to_rheo_path (const std::string& dir);

//! @brief file_exists: see the @ref rheostream_7 page for the full documentation
// predicate when a file exists
bool file_exists (const std::string& filename);

//! @brief is_float: see the @ref rheostream_7 page for the full documentation
// is_float("3.14") -> true
bool is_float (const std::string&);

//! @brief to_float: see the @ref rheostream_7 page for the full documentation
// string-to-float conversion
Float to_float (const std::string&);

//! @brief get_tmpdir: see the @ref rheostream_7 page for the full documentation
// in TMPDIR environment variable or "/tmp" by default
std::string get_tmpdir();
// [verbatim_functions]

// integer-to-string conversion (backward compatibility)
inline std::string itos (std::size_t i) { return std::to_string(i); }

}// namespace rheolef
#endif // _RHEOLEF_STREAM_H
