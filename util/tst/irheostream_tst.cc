///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/rheostream.h"
using namespace rheolef;
using namespace std;

void
usage() {
   cerr << "irheostream_tst: usage: irheostream_tst "
	<< " filename suffix"
	<< endl;
   
   cerr << " ex: irheostream_tst ../test/in-saylr1 hb\n";
   exit (1);
}
int main(int argc, char** argv) {
    if (argc < 3) usage();
    irheostream data(argv[1], argv[2]);
    if (!data) 
      fatal_macro("irheostream_tst: \"" 
	<< argv[1] << "[." << argv[2] << "[.gz]]\" not found.");

    while (data) {
        int c = data.get();
        if (c != EOF) cout.put(c);
    }
}
