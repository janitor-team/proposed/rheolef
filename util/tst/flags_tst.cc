///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/iorheo.h"
using namespace rheolef;
using namespace std;

int main()
{
    // check basic flag access: initialize data structure
    iorheo::flag_type flags = iorheo::flags(clog);

    // check boolean accessors
    bool verbose = iorheo::getverbose(clog);
    if (verbose) clog << "have verbose" << endl;
    else clog << "have no verbose" << endl;

    cout << ml;
    bool have_ml = iorheo::getml(cout);
    warning_macro ("have_ml = " << have_ml);
    check_macro (have_ml, "io manipulators failed");

    // check field accessors
    iorheo::flag_type fmt = iorheo::flags(cout) & iorheo::format_field;
    warning_macro ("have_ml = " << fmt [iorheo::ml]);
    check_macro (fmt [iorheo::ml], "io group manipulators failed");

    // trace format_field move:
    warning_macro ("ml_flags = " << fmt);
    cout << bamg;
    fmt = iorheo::flags(cout) & iorheo::format_field;
    warning_macro ("bamg_flags = " << fmt);
    check_macro (!fmt [iorheo::ml], "io group manipulators failed");
    return 0;
}
