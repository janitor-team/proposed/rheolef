///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/geo_domain_indirect.h"

namespace rheolef {

// ========================================================================
// cstors
// ========================================================================
// duplicate cases for seq & dis classes:
#define _RHEOLEF_geo_domain_cstor(M) 					\
template <class T>							\
geo_domain_indirect_rep<T,M>::geo_domain_indirect_rep()			\
 : base()								\
{									\
}									\
template <class T>							\
geo_domain_indirect_rep<T,M>::geo_domain_indirect_rep (			\
    const geo_domain_indirect_rep<T,M>& x)				\
 : base(x)								\
{									\
  trace_macro ("*** PHYSICAL COPY OF GEO_DOMAIN_INDIRECT \""<<base::name()<<"\" ***");	\
}									\
template <class T>							\
geo_abstract_rep<T,M>*							\
geo_domain_indirect_rep<T,M>::clone() const				\
{									\
  trace_macro ("*** CLONE GEO_DOMAIN_INDIRECT \""<<base::name()<<"\"***"); \
  typedef geo_domain_indirect_rep<T,M> rep;				\
  return new_macro(rep(*this));						\
}									\
template <class T>							\
geo_domain_indirect_rep<T,M>::geo_domain_indirect_rep (			\
    const domain_indirect_basic<M>& indirect, 				\
    const geo_basic<T,M>& omega)					\
 : base(indirect,omega)							\
{									\
}

_RHEOLEF_geo_domain_cstor(sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_geo_domain_cstor(distributed)
#endif // _RHEOLEF_HAVE_MPI
#undef _RHEOLEF_geo_domain_cstor

// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M)                             \
template class geo_domain_indirect_rep<T,M>; 

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
