#ifndef _RHEOLEF_DOMAIN_H
#define _RHEOLEF_DOMAIN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

/*Class:domain
NAME: @code{geo_domain_indirect_rep} - a named part of a finite element mesh
@cindex  mesh boundary
@clindex geo_domain_indirect_rep
DESCRIPTION:
  @noindent
  The @code{geo_domain_indirect_rep} class defines a container for a part of a
  finite element mesh.
  This describes the connectivity of edges or faces.
  This class is useful for boundary condition setting.
IMPLEMENTATION NOTE:
  The @code{geo_domain_indirect_rep} class is split into two parts.
  The first one is the @code{domain_indirect} class, that contains the main
  renumbering features: it acts as an indirection on a @code{geo} class(@pxref{geo class}).
  The second one is the @code{geo} class itself, named here the background geo.
  Thus, the @code{geo_domain_indirect} class develops a complete @code{geo}-like interface, 
  via the @code{geo_abstract_rep} pure virtual class derivation,
  and can be used by the @code{space} class (@pxref{space class}).

  The split between @code{domain_indirect} and @code{geo_domain_indirect} is necessary,
  because the @code{geo} class contains a list of domain_indirect.
  The @code{geo} class cannot contains a list of @code{geo_domain_indirect} classes, that refers
  to the @code{geo} class itself: a loop in reference counting
  leads to a blocking situation in the automatic deallocation.

DATE: 20 february 2011
End:
*/

#include "rheolef/geo.h"
#include "rheolef/domain_indirect.h"

namespace rheolef {

typedef geo domain;

// ========================================================================
// shared by seq & mpi rep
// ========================================================================
template <class T, class M>
class geo_domain_indirect_base_rep : public geo_abstract_rep<T,M> {
public:
// typedefs:
    typedef geo_abstract_rep<T,M>                     base;
    typedef typename base::size_type                  size_type;
    typedef typename base::node_type                  node_type;
    typedef typename base::variant_type               variant_type;
    typedef typename base::const_reference            const_reference;
    typedef typename base::const_iterator             const_iterator;
    typedef typename base::const_iterator_by_variant  const_iterator_by_variant;
    typedef typename base::iterator_by_variant        iterator_by_variant;
    typedef typename base::coordinate_type            coordinate_type;
    typedef typename base::geo_element_map_type       geo_element_map_type;

// allocators:

    geo_domain_indirect_base_rep ();
    geo_domain_indirect_base_rep (const geo_domain_indirect_base_rep<T,M>&);
    geo_domain_indirect_base_rep (const domain_indirect_basic<M>& indirect, const geo_basic<T,M>& omega);

// accessors:

    const domain_indirect_basic<M>& get_indirect()        const { return _indirect; }
    const geo_basic<T,M>&           get_background_geo()  const { return _omega; }

// implemented geo_abstract interface:

    size_type variant() const { return geo_abstract_base_rep<T>::geo_domain_indirect; }
    std::string name() const { return _indirect.name(); }
    std::string familyname() const { return _indirect.name(); }
    size_type serial_number() const { return _omega.serial_number(); }
    size_type dimension() const { return _omega.dimension(); }
    size_type map_dimension() const { return _indirect.map_dimension(); }
    bool is_broken() const { return _indirect.is_broken(); }
    coordinate_type coordinate_system() const { return _omega.coordinate_system(); }
    const basis_basic<T>& get_piola_basis() const { return _omega.get_piola_basis(); }

    size_type n_node() const { return _omega.n_node(); }
    const node_type& node (size_type inod) const { return _omega.node (inod); }
    const disarray<node_type,M>& get_nodes() const { return _omega.get_nodes(); }
    const node_type& xmin () const { return _omega.xmin (); }
    const node_type& xmax () const { return _omega.xmax (); }
    const T& hmin () const { return _omega.hmin (); }
    const T& hmax () const { return _omega.hmax (); }

// partially implemented geo_abstract interface:

    const distributor& geo_element_ownership (size_type dim) const;
    const_reference get_geo_element (size_type dim, size_type ige) const;
    size_type neighbour (size_type ie, size_type loc_iside) const;
    void neighbour_guard() const;

// unimplemented geo_abstract interface:

    size_type n_domain_indirect() const;
    bool have_domain_indirect(const std::string& name) const;
    const domain_indirect_basic<M>& get_domain_indirect (size_type i) const;
    const domain_indirect_basic<M>& get_domain_indirect (const std::string& name) const;
    void  insert_domain_indirect (const domain_indirect_basic<M>& dom) const;
    const_iterator_by_variant begin_by_variant (variant_type variant) const;
    const_iterator_by_variant   end_by_variant (variant_type variant) const;
    const geo_element_map_type& get_external_geo_element_map (size_type variant) const;
    const node_type& dis_node (size_type dis_inod) const;
    size_type dis_inod2dis_iv (size_type dis_inod) const;
    void dis_inod (const geo_element& K, std::vector<size_type>& dis_inod) const;
    const geo_size&      sizes() const;
    const geo_size&  ios_sizes() const;
    odiststream& put (odiststream& ops) const;
    void set_nodes (const disarray<node_type,M>&);
    void reset_order (size_type order);
    bool check(bool verbose) const;
    size_type seq_locate(
		const point_basic<T>&     x,
		size_type                 dis_ie_guest = std::numeric_limits<size_type>::max()) const;
    size_type dis_locate(
		const point_basic<T>&     x,
		size_type                 dis_ie_guest = std::numeric_limits<size_type>::max()) const;
    void locate (
                const disarray<point_basic<T>,M>& x,
                      disarray<size_type,M>&      dis_ie,
                bool do_check = false) const;
    size_type seq_trace_move (
                const point_basic<T>&     x,
                const point_basic<T>&     v,
                      point_basic<T>&     y) const;
    size_type dis_trace_move  (
                const point_basic<T>&     x,
                const point_basic<T>&     v,
                      point_basic<T>&     y) const;
    void trace_move (
                const disarray<point_basic<T>,M>&     x,
                const disarray<point_basic<T>,M>&     v,
                      disarray<size_type, M>&         dis_ie,
                      disarray<point_basic<T>,M>&     y) const;
    void trace_ray_boundary (
                const disarray<point_basic<T>,M>&     x,
                const disarray<point_basic<T>,M>&     v,
                      disarray<size_type, M>&         dis_ie,
                      disarray<point_basic<T>,M>&     y,
                bool do_check = false) const;
    size_type seq_nearest (
                const point_basic<T>&    x,
                      point_basic<T>&    x_nearest) const;
    size_type dis_nearest (
                const point_basic<T>&    x,
                      point_basic<T>&    x_nearest) const;
    void nearest (
                const disarray<point_basic<T>,M>&     x,
                      disarray<point_basic<T>,M>&     x_nearest,
                      disarray<size_type, M>&         dis_ie) const;
protected:
// data:
    domain_indirect_basic<M>   _indirect;
    geo_basic<T,M>             _omega;
};
template <class T, class M>
inline
geo_domain_indirect_base_rep<T,M>::geo_domain_indirect_base_rep()
  : geo_abstract_rep<T,M>(),   
    _indirect(),
    _omega()
{
}
template <class T, class M>
inline
geo_domain_indirect_base_rep<T,M>::geo_domain_indirect_base_rep(
 const geo_domain_indirect_base_rep<T,M>& dom)
  : geo_abstract_rep<T,M>(dom),   
    _indirect(dom._indirect),
    _omega(dom._omega)
{
}
template <class T, class M>
inline
geo_domain_indirect_base_rep<T,M>::geo_domain_indirect_base_rep (const domain_indirect_basic<M>& indirect, const geo_basic<T,M>& omega)
  : geo_abstract_rep<T,M>(),   
    _indirect (indirect),
    _omega (omega)
{
}
// ========================================================================
// sequential rep
// ========================================================================
template <class T, class M>
class geo_domain_indirect_rep {};

template <class T>
class geo_domain_indirect_rep<T,sequential> : public geo_domain_indirect_base_rep<T,sequential> {
public:
    typedef geo_domain_indirect_base_rep<T,sequential>	base;
    typedef typename base::size_type                    size_type;
    typedef typename base::node_type                    node_type;

    geo_domain_indirect_rep ();
    geo_domain_indirect_rep (const geo_domain_indirect_rep<T,sequential>& x);
    geo_domain_indirect_rep (const domain_indirect_basic<sequential>& indirect, const geo_basic<T,sequential>& omega);
    geo_abstract_rep<T,sequential>* clone() const;
};
// ========================================================================
// distributed rep
// ========================================================================
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
class geo_domain_indirect_rep<T,distributed> : public geo_domain_indirect_base_rep<T,distributed> {
public:
    typedef geo_domain_indirect_base_rep<T,distributed>	base;
    typedef typename base::size_type                    size_type;
    typedef typename base::node_type                    node_type;
    typedef typename base::const_reference              const_reference;

    geo_domain_indirect_rep();
    geo_domain_indirect_rep (const geo_domain_indirect_rep<T,distributed>& x);
    geo_domain_indirect_rep (const domain_indirect_basic<distributed>& indirect, const geo_basic<T,distributed>& omega);
    geo_abstract_rep<T,distributed>* clone() const;

// accessors: the geo_abstract mpi interface is defined here

    distributor geo_element_ios_ownership (size_type dim) const;
    const_reference dis_get_geo_element (size_type map_dim, size_type dis_ige) const;
    size_type             ige2ios_dis_ige (size_type dim, size_type     ige) const;
    size_type         dis_ige2ios_dis_ige (size_type dim, size_type dis_ige) const;
    size_type             ios_ige2dis_ige (size_type dim, size_type ios_ige) const;
    void set_ios_permutation (disarray<size_type,distributed>& idof2ios_dis_idof) const;
    // used by space_constritution for ios numbering
    const std::array<disarray<size_type,distributed>,reference_element::max_variant>&
    get_igev2ios_dis_igev() const;
};
#endif // _RHEOLEF_HAVE_MPI
// ========================================================================
// geo_basic allocator and accessor related to domains
// ========================================================================
#define _RHEOLEF_geo_domain(M)					\
template<class T>						\
inline								\
geo_basic<T,M>::geo_basic (					\
    const domain_indirect_basic<M>& dom, 			\
    const geo_basic<T,M>& omega)				\
 : base (new_macro((geo_domain_indirect_rep<T,M>)(dom,omega)))	\
{								\
}								\
template<class T>						\
inline								\
geo_basic<T,M>							\
geo_basic<T,M>::get_domain (size_type i) const			\
{								\
    const domain_indirect_basic<M>& dm = base::data().get_domain_indirect (i); \
    return geo_basic<T,M> (dm, *this);				\
}								\
template<class T>						\
inline								\
geo_basic<T,M>							\
geo_basic<T,M>::operator[] (const std::string& name) const	\
{								\
  if (name == "boundary")             boundary_guard (*this);   \
  if (name == "sides")                   sides_guard (*this);   \
  if (name == "internal_sides") internal_sides_guard (*this);   \
  const domain_indirect_basic<M>& dm  				\
   = base::data().get_domain_indirect (name); 			\
  return geo_basic<T,M> (dm, *this);				\
}								\
template<class T>						\
inline										\
geo_basic<T,M>									\
operator+ (const geo_basic<T,M>& a, const geo_basic<T,M>& b) 			\
{                                                                               \
  check_macro (a.variant() == geo_abstract_base_rep<T>::geo_domain_indirect &&	\
               b.variant() == geo_abstract_base_rep<T>::geo_domain_indirect,	\
    "union of geo may be domains of a mesh (was "				\
	<<a.name()<<" and "<<b.name()<<")");					\
  check_macro (a.get_background_geo().name() == b.get_background_geo().name(),	\
    "union of domains between incompatible meshes (was "			\
	<<a.get_background_geo().name()<<" and "<<b.get_background_geo().name()<<")"); \
  const geo_domain_indirect_rep<T,M>& a_dom                                     \
       = dynamic_cast<const geo_domain_indirect_rep<T,M>&>(a.data());           \
  const geo_domain_indirect_rep<T,M>& b_dom                                     \
       = dynamic_cast<const geo_domain_indirect_rep<T,M>&>(b.data());           \
 domain_indirect_basic<M> c_dom							\
   = build_union (a.get_background_geo(), a_dom.get_indirect(), b_dom.get_indirect()); \
  return geo_basic<T,M> (c_dom, a.get_background_geo());			\
}

_RHEOLEF_geo_domain(sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_geo_domain(distributed)
#endif // _RHEOLEF_HAVE_MPI
#undef _RHEOLEF_domain

} // namespace rheolef
#endif // _RHEOLEF_DOMAIN_H
