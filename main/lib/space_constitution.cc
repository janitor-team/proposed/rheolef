///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// helper for dof numbering with space product and component access
//
// contents:
// 1. allocators
// 2. accessors
// 3. inquire
// 4. initialize
// 5. output
// 6. do_act: block & unblock on a domain
// 7. ios accessors
// 8. comp_dis_idof2dis_idof : for wdof_sliced
// 9. dis_idof, for assembly: moved to space_constitution_assembly.cc
//
#include "rheolef/geo_domain.h"
#include "rheolef/space.h"
#include "rheolef/space_numbering.h"
#include "rheolef/space_mult.h"
#include "rheolef/piola_util.h"
#include "rheolef/basis_get.h"
#include <sstream>

namespace rheolef {

// ======================================================================
// 1. allocators
// ======================================================================
template <class T, class M>
space_constitution_terminal_rep<T,M>::space_constitution_terminal_rep(
		const geo_basic<T,M>&    omega,
		std::string              approx)
  : _acts(),
    _omega(compact(omega)),
    _fem_basis()
{
  if (approx == "") return;
  const size_type unset = std::numeric_limits<basis_option::size_type>::max();
  family_index_option_type fio;
  basis_parse_from_string (approx, fio);
  if (fio.option.valued_tag() != space_constant::scalar && fio.option.dimension() == unset) {
    // when vector/tensor on surface geometry: fix the number of components by forcing the physical dimension
    // e.g. 3D-vector on a surface (map_dimension = 2)
    fio.option.set_dimension (omega.dimension());
  }
  if (fio.option.valued_tag() == space_constant::tensor &&
      omega.coordinate_system() != space_constant::cartesian) {
    // when tensor on an axisymmetric geometry: should add a theta-theta tensor component
    fio.option.set_coordinate_system (omega.coordinate_system());
  }
  std::string approx_d = basis_rep<T>::standard_naming (fio.family, fio.index, fio.option);
  _fem_basis = basis_basic<T>(approx_d);
}
template <class T, class M>
void
space_constitution_terminal_rep<T,M>::do_act (const space_act& act)
{
  check_macro (get_geo().map_dimension() < get_geo().dimension() ||
              !get_basis().have_compact_support_inside_element(),
    "try to [un]block domain `" << act.get_domain_name() << "' on discontinuous or bubble space("
    << get_basis().name()<<"(" <<_omega.name()<<"))");
  _acts.push_back (act);
}
template <class T, class M>
void
space_constitution_rep<T,M>::do_act (const space_act& act)
{
  typedef distributor::size_type size_type;
  if (! is_hierarchical()) {
    space_constitution_terminal<T,M>& terminal_constit = get_terminal();
    terminal_constit.do_act (act);
    return;
  }
  // hierarchical case:
  for (iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    space_constitution<T,M>& constit = *iter;
    constit.do_act (act); // recursive call
  }
}
// -----------------------------------------------------------------------
// loop on domains: mark blocked dofs (called by space::freeze)
// -----------------------------------------------------------------------
template <class T, class M>
void
space_constitution_terminal_rep<T,M>::build_blocked_flag (
  disarray<size_type,M>& blocked_flag, // disarray<bool,M> not supported
  const distributor&  comp_ownership,
  const distributor&  start_by_flattened_component) const
{
  check_macro (get_basis().is_initialized(), "space with undefined finite element method cannot be used");
  std::vector<size_type> comp_dis_idof_t;
  distributor dof_ownership = blocked_flag.ownership();
  size_type   dis_ndof      = dof_ownership.dis_size();
  bool prev_act = space_act::block;
  size_type n_comp = get_basis().size();
  size_type dim = get_geo().dimension();
  for (const_iterator iter = begin(), last = end(); iter != last; iter++) {
    typename space_act::act_type act = (*iter).get_act();
    const std::string&      dom_name = (*iter).get_domain_name();
    size_type               i_comp   = (*iter).get_component_index();
    check_macro (i_comp == space_act::unset_index || i_comp < n_comp,
	"invalid blocked domain for the "<<i_comp << "-th component in a "
        <<n_comp<<"-component non-hierarchical space with basis=\""
        << get_basis().name() << "\"");
    // sync all blocked_flags when there is an alternance of block and unblock (bug fixed)
    if (prev_act != act) {
      blocked_flag.dis_entry_assembly();
      prev_act = act;
    }
    const domain_indirect_basic<M>& dom = _omega.get_domain_indirect(dom_name);
    size_type dom_dim = dom.map_dimension();
    distributor ige_ownership = _omega.geo_element_ownership (dom_dim);
    size_type   first_dis_ige = ige_ownership.first_index();
    size_type    last_dis_ige = ige_ownership.last_index();
    bool blk = (act == space_act::block || act == space_act::block_n) ? true : false;
    for (size_type ioige = 0, noige = dom.size(); ioige < noige; ioige++) {
      size_type ige     = dom.oige (ioige).index();
      size_type dis_ige = ige + first_dis_ige;
      const geo_element& S = _omega.get_geo_element (dom_dim, ige);
      space_numbering::dis_idof (get_basis(), _omega.sizes(), S, comp_dis_idof_t);
      size_type loc_idof_start, loc_idof_incr;
      if (act == space_act::block || act == space_act::unblock) {
        loc_idof_start = (i_comp == space_act::unset_index) ? 0 : i_comp;
        loc_idof_incr  = (i_comp == space_act::unset_index) ? 1 : n_comp;
      } else { // block_n or unblock_n
        loc_idof_start = dim-1;
        loc_idof_incr  = dim;
      }
      // vector-valued : do not work yet with RTk, but only with (Pk)^d
      // note that RTk Hdiv-conform could block_n in theory
      for (size_type loc_idof = loc_idof_start, loc_ndof = comp_dis_idof_t.size(); loc_idof < loc_ndof; loc_idof += loc_idof_incr) {
        size_type comp_dis_idof = comp_dis_idof_t [loc_idof];
        size_type iproc = comp_ownership.find_owner (comp_dis_idof);
        size_type first_comp_dis_idof = comp_ownership.first_index (iproc);
        assert_macro (comp_dis_idof >= first_comp_dis_idof, "unexpected comp_dis_idof");
        size_type comp_idof = comp_dis_idof - first_comp_dis_idof;
        size_type comp_start_idof = start_by_flattened_component.size(iproc);
        size_type idof = comp_start_idof + comp_idof;
        size_type first_dis_idof = dof_ownership.first_index(iproc);
        size_type dis_idof = first_dis_idof + idof;
        assert_macro (dis_idof < dis_ndof, "unexpected dis_idof");
        blocked_flag.dis_entry (dis_idof) = blk;
      }
#ifdef TODO
      basis scalar_basis = ...; // scalar Pk
      space_numbering::dis_idof (scalar_basis, _omega.sizes(), S, scalar_dis_idof_t);
      for (size_type loc_scalar_idof = 0, loc_scalar_ndof = comp_dis_idof_t.size()/n_comp; loc_scalar_idof < loc_scalar_ndof; ++loc_scalar_idof) {
        size_type dis_scalar_idof = scalar_dis_idof_t [loc_idof]; // as if we do a scalar Pk element
        has_nt_basis.dis_entry (dis_scalar_idof) = (act == space_act::block_n || act == space_act::unblock_n);
        normal.dis_entry       (dis_scalar_idof) = copy_of_a_previous_computation;
      }
#endif // TODO
    }
  }
}
template <class T, class M>
void
space_constitution_rep<T,M>::build_blocked_flag_recursive (
  disarray<size_type,M>&             blocked_flag, // disarray<bool,M> not supported
  const std::vector<distributor>&    start_by_flattened_component,
  size_type&                         i_flat_comp) const
{
  if (! is_hierarchical()) {
    const space_constitution_terminal<T,M>& terminal_constit = get_terminal();
    terminal_constit.data().build_blocked_flag (blocked_flag, ownership(), start_by_flattened_component [i_flat_comp]);
    i_flat_comp++;
    return;
  }
  // hierarchical case:
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    constit.data().build_blocked_flag_recursive (blocked_flag, start_by_flattened_component, i_flat_comp); // recursive call
  }
}
template <class T, class M>
disarray<typename space_constitution_rep<T,M>::size_type,M>
space_constitution_rep<T,M>::build_blocked_flag() const
{
  disarray<size_type,M> blocked_flag (ownership(), 0); // disarray<bool,M> not supported
  size_type i_flat_comp = 0;
  build_blocked_flag_recursive (blocked_flag, _start_by_flattened_component, i_flat_comp);
  blocked_flag.dis_entry_assembly();
  return blocked_flag;
}
// ======================================================================
// 2. accessors
// ======================================================================
template <class T, class M>
const geo_basic<T,M>&
space_constitution_rep<T,M>::get_geo() const
{
  if (! is_hierarchical()) {
    return get_terminal().get_geo();
  }
  // heterogeneous: get as geo the maximum domain of definition
  // => it is the intersection of all domain definition
  // implementation note: use "const geo*" ptr_dom for returning a "const geo&"
  check_macro (_hier_constit.size() > 0, "get_geo: empty space product");
  const geo_basic<T,M>* ptr_dom = &(_hier_constit[0].get_geo()); // recursive call
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    const geo_basic<T,M>* ptr_comp_dom = &(constit.get_geo()); // recursive call
    if (ptr_comp_dom->name() == ptr_dom->name()) continue;
    if (ptr_dom->get_background_geo().name() == ptr_comp_dom->name()) continue;
    if (ptr_comp_dom->get_background_geo().name() == ptr_dom->name()) {
      ptr_dom = ptr_comp_dom;
    } else {
      error_macro ("get_geo: incompatible domains: \""<<ptr_dom->name()<<"\" and \""<<ptr_comp_dom->name() << "\"");
    }
  }
  return *(ptr_dom);
}
template <class T, class M>
const geo_basic<T,M>&
space_constitution_rep<T,M>::get_background_geo() const
{
  if (! is_hierarchical()) {
    return get_terminal().get_background_geo();
  }
  // component have the same background mesh: assume it without check (TODO: constit.check_have_same_bgd_geo())
  check_macro (_hier_constit.size() > 0, "get_background_geo: empty space product");
  const_iterator iter = _hier_constit.begin();
  const space_constitution<T,M>& constit = (*iter);
  return constit.get_background_geo(); // recursive call
}
template <class T, class M>
const basis_basic<T>&
space_constitution_rep<T,M>::get_basis() const
{
  check_macro(! is_hierarchical(), "get_basis: undefined for heterogeneous space products");
  return get_terminal().get_basis();
}
// ======================================================================
// 3. inquire
// ======================================================================
template <class T, class M>
bool
space_constitution_rep<T,M>::have_compact_support_inside_element() const
{
  if (! is_hierarchical()) {
    return get_terminal().get_basis().have_compact_support_inside_element();
  }
  bool has = true;
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    has = has && constit.have_compact_support_inside_element(); // recursive call
  }
  return has;
}
template <class T, class M>
bool
space_constitution_rep<T,M>::is_discontinuous() const
{
  if (! is_hierarchical()) {
    return get_terminal().get_basis().is_discontinuous();
  }
  bool is = true;
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    is = is && constit.is_discontinuous(); // recursive call
  }
  return is;
}
// build a space::constitution hierrachy from a list of spaces
template <class T, class M>
inline
space_constitution_rep<T,M>::space_constitution_rep(const space_mult_list<T,M>& expr)
 : _is_initialized(false),
   _flattened_size(0),
   _start_by_flattened_component(),
   _start_by_component(),
   _ownership(),
   _valued_tag(space_constant::mixed),
   _is_hier(true),
   _terminal_constit(),
   _hier_constit(expr.size()),
   _loc_ndof()
{
  _loc_ndof.fill (std::numeric_limits<size_type>::max());
  typename space_constitution_rep<T,M>::hierarchy_type::iterator iter_constit = _hier_constit.begin();
  for (typename space_mult_list<T,M>::const_iterator iter = expr.begin(), last = expr.end(); iter != last; ++iter, ++iter_constit) {
    const space_basic<T,M>& Xi = *iter;
    *iter_constit = Xi.get_constitution();
  }
  initialize();
}
template <class T, class M>
bool
space_constitution_rep<T,M>::operator== (const space_constitution_rep<T,M>& V2) const
{
  if (_is_hier != V2._is_hier) { return false; }
  if (!_is_hier) { return (_terminal_constit == V2._terminal_constit); }
  // here, two hierarchies:
  if (_hier_constit.size() != V2._hier_constit.size()) return false;
  for (const_iterator iter1 = _hier_constit.begin(), iter2 = V2._hier_constit.begin(), last1 = _hier_constit.end(); iter1 != last1; ++iter1, ++iter2) {
    if (! (*iter1).data().operator==((*iter2).data())) { // recursive call
      return false;
    }
  }
  return true;
}
// =======================================================================
// 4. initialize
// =======================================================================
template <class T, class M>
typename space_constitution_rep<T,M>::size_type
space_constitution_rep<T,M>::_init_flattened_size() const
{
  if (! is_hierarchical()) return 1;
  size_type flattened_size = 0;
  for (size_type i_comp = 0, n_comp = size(); i_comp < n_comp; i_comp++) {
    const space_constitution<T,M>& comp_constit = operator[] (i_comp);
    flattened_size += comp_constit.data()._init_flattened_size(); // recursive call
  }
  return flattened_size;
}
template <class T, class M>
void
space_constitution_rep<T,M>::_init_start_by_flattened_component(
	size_type&            i_flat_comp,
        size_type&        start_flat_comp_idof,
        size_type&    dis_start_flat_comp_idof,
        std::vector<distributor>& start_by_flattened_component) const
{
  if (! is_hierarchical()) {
    if (! get_basis().is_initialized()) return; // empty numbering => empty space
    start_by_flattened_component [i_flat_comp] = distributor (dis_start_flat_comp_idof, comm(), start_flat_comp_idof);
    size_type     ndof = space_numbering::    ndof (get_basis(), get_geo().sizes(), get_geo().map_dimension());
    size_type dis_ndof = space_numbering::dis_ndof (get_basis(), get_geo().sizes(), get_geo().map_dimension());
            i_flat_comp++;
        start_flat_comp_idof +=     ndof;
    dis_start_flat_comp_idof += dis_ndof;
    return;
  }
  // hierarchical case:
  for (size_type i_comp = 0, n_comp = size(); i_comp < n_comp; i_comp++) {
    const space_constitution<T,M>& comp_constit = operator[] (i_comp);
    comp_constit.data()._init_start_by_flattened_component (i_flat_comp, start_flat_comp_idof, dis_start_flat_comp_idof, start_by_flattened_component); // recursive call
  }
}
template <class T, class M>
void
space_constitution_rep<T,M>::_init_start_by_component() const
{
  if (! is_hierarchical()) {
    if (! get_basis().is_initialized()) return; // empty numbering => empty space
    _start_by_component.resize (1);
    _start_by_component [0] = distributor (0, comm(), 0);
    return;
  }
  // hierarchical case:
  _start_by_component.resize (size());
  size_type comp_start_idof     = 0;
  size_type comp_start_dis_idof = 0;
  for (size_type i_comp = 0, n_comp = size(); i_comp < n_comp; i_comp++) {
    const space_constitution<T,M>& comp_constit = operator[] (i_comp);
    _start_by_component [i_comp] = distributor (comp_start_dis_idof, comm(), comp_start_idof);
    comp_start_idof     += comp_constit.ownership().    size();
    comp_start_dis_idof += comp_constit.ownership().dis_size();
  }
}
template <class T, class M>
void
space_constitution_rep<T,M>::initialize() const
{
  if (_is_initialized) return;
  _is_initialized = true;
 
  _flattened_size = _init_flattened_size();
  _start_by_flattened_component.resize (_flattened_size);
  size_type            i_flat_comp      = 0;
  size_type        start_flat_comp_idof = 0;
  size_type    dis_start_flat_comp_idof = 0;
  _init_start_by_flattened_component (i_flat_comp, start_flat_comp_idof, dis_start_flat_comp_idof, _start_by_flattened_component);
  _ownership = distributor (dis_start_flat_comp_idof, comm(), start_flat_comp_idof);
  _init_start_by_component();
}
//  ----------------------------------------------------------------------
// get external idofs: used by space<distributed>::freeze
// ---------------------------------------------------------------,--------
static
void
local_append_external_dis_idof (
  const std::vector<size_t>&    comp_dis_idof_tab,
  const distributor&            comp_ownership,
  const distributor&            dof_ownership,
  const distributor&            start_by_flattened_component,
  std::set<size_t>&             ext_dof_set)
{
  using size_type = size_t;
  size_type comp_dis_ndof = comp_ownership.dis_size();
  for (size_type loc_idof = 0, loc_ndof = comp_dis_idof_tab.size(); loc_idof < loc_ndof; loc_idof++) {
    size_type comp_dis_idof = comp_dis_idof_tab [loc_idof];
    assert_macro (comp_dis_idof < comp_dis_ndof, "idof " << comp_dis_idof_tab [loc_idof] << " out of range[0:"<< comp_dis_ndof << "[");
    if (! comp_ownership.is_owned (comp_dis_idof)) {
      size_type iproc = comp_ownership.find_owner (comp_dis_idof);
      size_type comp_first_dis_idof = comp_ownership.first_index(iproc);
      assert_macro (comp_dis_idof >= comp_first_dis_idof, "unexpected comp_dis_idof");
      size_type comp_idof = comp_dis_idof - comp_first_dis_idof;
      size_type comp_start_idof = start_by_flattened_component.size(iproc);
      size_type idof = comp_start_idof + comp_idof;
      size_type first_dis_idof = dof_ownership.first_index(iproc);
      size_type dis_idof = first_dis_idof + idof;
      assert_macro (! dof_ownership.is_owned (dis_idof), "unexpected dis_idof="<<dis_idof<<" in range ["
	<< first_dis_idof << ":" << dof_ownership.last_index() << "[");
      ext_dof_set.insert (dis_idof);
    }
  }
}
template <class T, class M>
void
space_constitution_rep<T,M>::append_external_dof (
        const geo_basic<T,M>&         omega,
        std::set<size_type>&          ext_dof_set,
        const distributor&            dof_ownership,
        const distributor&            start_by_flattened_component) const
{
  // assume here a scalar (non-multi valued) case:
  distributor comp_ownership = ownership();
  size_type comp_dis_ndof    = comp_ownership.dis_size();
  std::vector<size_type> comp_dis_idof_tab;
  // loop on internal elements
  for (size_type ie = 0, ne = omega.size(); ie < ne; ie++) {
      const geo_element& K = omega [ie];
      assembly_dis_idof (omega, K, comp_dis_idof_tab);
      local_append_external_dis_idof (comp_dis_idof_tab, comp_ownership, dof_ownership, start_by_flattened_component, ext_dof_set);
  }
  // loop also on external elements
  size_type map_d = omega.map_dimension();
  for (size_type variant = reference_element::first_variant_by_dimension (map_d);
                 variant < reference_element:: last_variant_by_dimension (map_d); ++variant) {
    // for (auto iter : omega._geo_element[variant].get_dis_map_entries()) {
    for (auto iter : omega.get_external_geo_element_map(variant)) {
      const geo_element& K = iter.second;
      assembly_dis_idof (omega, K, comp_dis_idof_tab);
      local_append_external_dis_idof (comp_dis_idof_tab, comp_ownership, dof_ownership, start_by_flattened_component, ext_dof_set);
    }
  }
}
template <class T, class M>
void
space_constitution_rep<T,M>::compute_external_dofs (
        std::set<size_type>&            ext_dof_set,
        const distributor&              dof_ownership,
	const std::vector<distributor>& start_by_flattened_component,
	size_type&                      i_flat_comp) const
{
  if (! is_hierarchical()) {
    const space_constitution_terminal<T,M>& terminal_constit = get_terminal();
    append_external_dof (terminal_constit.get_geo(), ext_dof_set, dof_ownership, start_by_flattened_component [i_flat_comp]);
    i_flat_comp++;
    return;
  }
  // hierarchical case:
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    (*iter).data().compute_external_dofs (ext_dof_set, dof_ownership, start_by_flattened_component, i_flat_comp);
  }
}
template <class T, class M>
void
space_constitution_rep<T,M>::compute_external_dofs (
        std::set<size_type>&          ext_dof_set) const
{
  ext_dof_set.clear();
  size_type i_flat_comp = 0;
  compute_external_dofs (ext_dof_set, ownership(), _start_by_flattened_component, i_flat_comp);
}
// ======================================================================
// 5. output
// ======================================================================
template<class T, class M>
void
space_constitution_rep<T,M>::put (std::ostream& out, size_type level) const
{
  if (! is_hierarchical()) {
    const space_constitution_terminal<T,M>& terminal_constit = get_terminal();
    if (!terminal_constit.get_basis().is_initialized()) return;
    out << terminal_constit.get_basis().name() << "{" << terminal_constit.get_geo().name() << "}";
  } else {
    if (level > 0) out << "(";
    typename space_constitution<T,M>::hierarchy_type::const_iterator x = get_hierarchy().begin();
    for (size_type i = 0, n = get_hierarchy().size(); i < n; i++) {
      const space_constitution<T,M>& xi = x[i];
      xi.data().put (out, level+1); // recursive call
      if (i+1 < n) { out << "*"; }
    }
    if (level > 0) out << ")";
  }
}
template<class T, class M>
std::string 
space_constitution_rep<T,M>::name() const
{
  std::ostringstream ostrstr;
  put (ostrstr, 0);
  return ostrstr.str();
}
// ======================================================================
// 6. do_act: block & unblock on a domain
// ======================================================================
template <class T, class M>
typename space_constitution_rep<T,M>::size_type
space_constitution_rep<T,M>::degree_max() const
{
  if (! is_hierarchical()) {
    return get_terminal().get_basis().degree();
  }
  size_type degree_max = 0;
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    degree_max = std::max(degree_max, constit.degree_max()); // recursive call
  }
  return degree_max;
}
template <class T, class M>
void
space_constitution_rep<T,M>::neighbour_guard() const
{
  if (! is_hierarchical()) {
    if (get_geo().dimension() == get_geo().map_dimension()) {
      // 3d surface mesh or 2d lineique mesh are not yet treated for neighbours
      get_terminal().neighbour_guard();
    }
    return;
  }
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    constit.neighbour_guard(); // recursive call
  }
}
// ======================================================================
// 7. ios accessors
// ======================================================================
template <class T, class M>
typename space_constitution_rep<T,M>::size_type
space_constitution_rep<T,M>::ios_ndof () const
{
  if (!_is_hier) {
    communicator comm =_terminal_constit.get_geo().comm();
    size_type dis_ndof = space_numbering::dis_ndof (_terminal_constit.get_basis(), _terminal_constit.get_geo().sizes(), _terminal_constit.get_geo().map_dimension());
    // TODO: memorize ios_ownership : avoid comms & risks of errors
    distributor ios_ownership (dis_ndof, comm, distributor::decide);
    return ios_ownership.size();
  }
  size_type ios_ndof = 0;
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    ios_ndof += constit.ios_ndof(); // recursive call
  }
  return ios_ndof;
}
template <class T, class M>
void
space_constitution_terminal_rep<T,M>::set_ios_permutations (
        disarray<size_type,M>& idof2ios_dis_idof,
        disarray<size_type,M>& ios_idof2dis_idof) const
{
  space_numbering::set_ios_permutations (get_basis(), get_geo(), idof2ios_dis_idof, ios_idof2dis_idof);
}
template <class T, class M>
void
space_constitution_rep<T,M>::set_ios_permutation_recursion (
	disarray<size_type,M>& idof2ios_dis_idof, 
	size_type&          comp_start_idof,
	size_type&          comp_start_dis_idof) const
{
  if (!_is_hier) {
    // non-hierarchical case:
    disarray<size_type,M> comp_idof2ios_dis_idof; 
    disarray<size_type,M> comp_ios_idof2dis_idof; 
    // TODO: avoid allocating comp_idof2ios_dis_idof: numbering supports directly comp_start_{dis_}idof
    // TODO: avoid invert permut here: numbering support no invert perm arg
    _terminal_constit.set_ios_permutations (comp_idof2ios_dis_idof, comp_ios_idof2dis_idof);
    // then shift
    size_type     comp_ndof = comp_idof2ios_dis_idof.size();
    size_type comp_dis_ndof = comp_idof2ios_dis_idof.dis_size();
    size_type          ndof = idof2ios_dis_idof.size();
    size_type      dis_ndof = idof2ios_dis_idof.dis_size();
    for (size_type comp_idof = 0; comp_idof < comp_ndof; comp_idof++) {
      size_type comp_ios_dis_idof = comp_idof2ios_dis_idof [comp_idof];
      size_type ios_dis_idof      = comp_start_dis_idof + comp_ios_dis_idof;
      size_type idof              = comp_start_idof + comp_idof;
      assert_macro (idof < ndof, "unexpected idof="<<idof<<" out of range [0:"<<ndof<<"[");
      assert_macro (ios_dis_idof < dis_ndof, "unexpected ios_dis_idof="<<ios_dis_idof<<" out of range [0:"<<dis_ndof<<"[");
      idof2ios_dis_idof [idof]    = ios_dis_idof;
    }
    comp_start_idof     +=     comp_ndof;
    comp_start_dis_idof += comp_dis_ndof;
    return;
  }
  // hierarchical case:
  for (const_iterator iter = _hier_constit.begin(), last = _hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& constit = *iter;
    constit.set_ios_permutation_recursion (idof2ios_dis_idof, comp_start_idof, comp_start_dis_idof); // recursive call
  }
}
template <class T, class M>
void
space_constitution_rep<T,M>::set_ios_permutations (
    disarray<size_type,M>& idof2ios_dis_idof,
    disarray<size_type,M>& ios_idof2dis_idof) const
{
  if (!_is_hier) {
    // non-hierarchical case: direct
    _terminal_constit.set_ios_permutations (idof2ios_dis_idof, ios_idof2dis_idof);
    return;
  }
  // hierarchical case:
  // ----------------------------------------------
  // 1) compute permutation
  // ----------------------------------------------
  communicator comm1  = comm();
  distributor dof_ownership = ownership();
  size_type ndof1     = dof_ownership.size();
  size_type dis_ndof1 = dof_ownership.dis_size();
  idof2ios_dis_idof.resize (dof_ownership, std::numeric_limits<size_type>::max());
  size_type comp_start_idof     = 0;
  size_type comp_start_dis_idof = 0;
  set_ios_permutation_recursion (idof2ios_dis_idof, comp_start_idof, comp_start_dis_idof);
  // ----------------------------------------------
  // 2) invert permutation into ios_idof2dis_idof
  // ----------------------------------------------
  size_type ios_ndof1 = ios_ndof();
  distributor ios_dof_ownership (dis_ndof1, idof2ios_dis_idof.comm(), ios_ndof1);
  ios_idof2dis_idof.resize (ios_dof_ownership, std::numeric_limits<size_type>::max());
  idof2ios_dis_idof.reverse_permutation (ios_idof2dis_idof);
}
// ======================================================================
// 8. comp_dis_idof2dis_idof : for wdof_sliced
// ======================================================================
template <class T, class M>
typename space_constitution_rep<T,M>::size_type
space_constitution_rep<T,M>::comp_dis_idof2dis_idof (
    size_type i_comp,
    size_type comp_dis_idof) const
{
  if (! is_hierarchical()) {
    // component of a "vector" or "tensor" valued space
    size_type n_comp = get_terminal().get_basis().size();
    size_type dis_idof = comp_dis_idof*n_comp + i_comp;
    return dis_idof;
  }
  // here: any space product such as Zh=Xh*Yh
  const distributor& comp_ownership = _hier_constit [i_comp].ownership();
  size_type iproc = comp_ownership.find_owner (comp_dis_idof);
  size_type first_comp_dis_idof = comp_ownership.first_index (iproc);
  check_macro (comp_dis_idof >= first_comp_dis_idof, "comp_dis_idof="<<comp_dis_idof << " is out of range");
  size_type comp_idof = comp_dis_idof - first_comp_dis_idof;
  size_type comp_start_idof = _start_by_flattened_component[i_comp].size (iproc);
  size_type idof = comp_start_idof + comp_idof;
  size_type first_dis_idof = _ownership.first_index (iproc);
  size_type dis_idof = first_dis_idof + idof;
  return dis_idof;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class space_constitution_terminal_rep<Float,sequential>;
template class space_constitution_rep<Float,sequential>;
template class space_constitution<Float,sequential>;

#ifdef _RHEOLEF_HAVE_MPI
template class space_constitution_terminal_rep<Float,distributed>;
template class space_constitution_rep<Float,distributed>;
template class space_constitution<Float,distributed>;
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
