#ifndef _RHEOLEF_HACK_ARRAY_H
#define _RHEOLEF_HACK_ARRAY_H
//
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// massive hack vector for all T=geo_element of the same variant & order
// => elements have exactly the same size 
// Since the order is known only at at run time, geo_element size is unknown at
// compile time and the disarray<T> cannot be used.
// 
// Nevertheless, the situation is similar at run time:
//  - elements can be stored contiguously in memory
//  - mpi communications can be efficienly performed, as for known MPI_Datatype
//
#include "rheolef/disarray.h"

namespace rheolef {

// -------------------------------------------------------------
// iterator
// -------------------------------------------------------------
template <class T, class Ref, class Ptr, class Raw, class RawIterator>
struct hack_array_iterator {
    typedef hack_array_iterator<T, Ref, Ptr, Raw, RawIterator>  _self;
    typedef hack_array_iterator<T, T&,  T*,  Raw, Raw*>         _iterator;

    typedef std::bidirectional_iterator_tag         iterator_category;
    typedef T                                       value_type;
    typedef Ref                                     reference;
    typedef Ptr                                     pointer;
    typedef typename T::size_type                   size_type;
    typedef typename std::iterator_traits<RawIterator>::difference_type difference_type;

    hack_array_iterator () 
      : _raw_iter(), _incr() {}
    hack_array_iterator (RawIterator raw_iter, size_type incr) 
      : _raw_iter(raw_iter), _incr(incr) {}
    hack_array_iterator (const _iterator& y) 
      : _raw_iter(y._raw_iter), _incr(y._incr) {}
    _self& operator++() { _raw_iter += _incr; return *this; }
    _self  operator++(int) { _self tmp = *this; operator++(); return tmp; }
    pointer   operator->() const { return   reinterpret_cast<pointer>(_raw_iter); }
    reference operator* () const { return *(reinterpret_cast<pointer>(_raw_iter)); }
    reference operator[] (size_type i) const { return *(reinterpret_cast<pointer>(_raw_iter + i*_incr)); }
    _self& operator+= (size_type n) { _raw_iter += n*_incr; return *this; }
    _self& operator-= (size_type n) { _raw_iter -= n*_incr; return *this; }
    _self  operator+  (size_type n) const { _self tmp = *this; tmp += n; return tmp; }
    _self  operator-  (size_type n) const { _self tmp = *this; tmp -= n; return tmp; }
    bool operator== (const _self& y) const { return _raw_iter == y._raw_iter && _incr == y._incr; }
    bool operator!= (const _self& y) const { return ! operator== (y); }

// data :
    RawIterator    _raw_iter;
    size_type      _incr;
};
// -------------------------------------------------------------
// the sequential representation
// -------------------------------------------------------------
template <class T, class A>
class hack_array_seq_rep : public disarray_rep<typename T::raw_type,sequential,A> {
public:

// typedefs:

  typedef disarray_rep<typename T::raw_type, sequential, A> base;
  typedef T		  	              raw_value_type;
  typedef typename T::generic_type            value_type;
  typedef typename T::generic_type            generic_value_type;
  typedef typename T::automatic_type          automatic_value_type;
  typedef A		  	              allocator_type;
  typedef typename T::parameter_type          parameter_type;
  typedef typename generic_value_type::raw_type	raw_type;
  typedef typename base::size_type            size_type;
  typedef value_type&		      	      reference;
  typedef const value_type&		      const_reference;
  typedef reference		  	      dis_reference;
  typedef sequential		  	      memory_type;

  typedef hack_array_iterator<generic_value_type, generic_value_type&, generic_value_type*, raw_type, raw_type*> 
	  iterator;
  typedef hack_array_iterator<generic_value_type, const generic_value_type&, const generic_value_type*, raw_type, const raw_type*>
	  const_iterator;

// allocators:

  hack_array_seq_rep (const A& alloc = A());
  hack_array_seq_rep (const distributor& ownership, const parameter_type& param, const A& alloc = A());
  void resize           (const distributor& ownership, const parameter_type& param);
  hack_array_seq_rep (size_type n, const parameter_type& param, const A& alloc = A());
  void resize           (size_type n, const parameter_type& param);

// accesors & modifiers

  A get_allocator() const               { return base::get_allocator(); }
  const distributor& ownership() const  { return _ownership; }
  const communicator& comm() const      { return ownership().comm(); }
  size_type          size()      const  { return ownership().size(); }
  size_type dis_size () const           { return ownership().dis_size(); }
  const generic_value_type& operator[] (size_type ie) const { 
    const raw_type *p = base::begin().operator->() + ie*_value_size;
    const T* q = (const T*)p;
    return *q;
  }
  generic_value_type& operator[] (size_type ie) { 
    raw_type *p = base::begin().operator->() + ie*_value_size;
    T* q = (T*)p;
    return *q;
  }
        iterator begin()       { return       iterator(base::begin().operator->(), _value_size); }
  const_iterator begin() const { return const_iterator(base::begin().operator->(), _value_size); }
        iterator end()         { return       iterator(base::begin().operator->() + size()*_value_size, _value_size); }
  const_iterator end()   const { return const_iterator(base::begin().operator->() + size()*_value_size, _value_size); }

// i/o:

  idiststream& get_values (idiststream& ips);
  odiststream& put_values (odiststream& ops) const;
  template <class GetFunction> idiststream& get_values (idiststream& ips, GetFunction get_element);
  template <class PutFunction> odiststream& put_values (odiststream& ops, PutFunction put_element) const;

protected:

// internals:

  void _init (const distributor& ownership, const parameter_type& param);

// data:

  distributor    _ownership;
  parameter_type _parameter;
  size_type      _value_size;
  size_type      _data_size;
};
// -------------------------------------------------------------
// the distributed representation
// -------------------------------------------------------------
#ifdef _RHEOLEF_HAVE_MPI
template <class T, class A>
class hack_array_mpi_rep : public hack_array_seq_rep<T,A> {
public:

// typedefs:

  typedef hack_array_seq_rep<T,A>          base;
  typedef typename base::base                 raw_base;
  typedef typename base::size_type   	      size_type;
  typedef typename base::value_type   	      value_type;
  typedef typename base::allocator_type       allocator_type;
  typedef typename base::generic_value_type   generic_value_type;
  typedef typename base::automatic_value_type automatic_value_type;
  typedef typename base::raw_type   	      raw_type;
  typedef typename base::parameter_type       parameter_type;
  typedef typename base::reference            reference;
  typedef typename base::const_reference      const_reference;
  typedef typename base::iterator             iterator;
  typedef typename base::const_iterator       const_iterator;
  typedef distributed		  	      memory_type;
  typedef std::map <size_type, automatic_value_type> scatter_map_type; // TODO: map<T,A> for heap_alloc


  struct dis_reference {
    dis_reference (hack_array_mpi_rep<T,A>& x, size_type dis_i)
     : _x(x), _dis_i(dis_i) {}

    dis_reference& operator= (const generic_value_type& value) {
      _x.set_dis_entry (_dis_i, value);
      return *this;
    }
  // data:
  protected:
    hack_array_mpi_rep<T,A>& _x;
    size_type                 _dis_i;
  };

// allocators:

  hack_array_mpi_rep (const A& alloc = A());
  hack_array_mpi_rep (const distributor& ownership, const parameter_type& param, const A& alloc = A());
  void resize           (const distributor& ownership, const parameter_type& param);

// accessors & modifiers:

  A get_allocator() const               { return base::get_allocator(); }
  const distributor& ownership() const  { return base::_ownership; }
  const communicator& comm() const      { return ownership().comm(); }
  size_type dis_size () const           { return ownership().dis_size(); }

  size_type size() const       { return base::size(); }
  const generic_value_type& operator[] (size_type ie) const { return base::operator[] (ie); }
        generic_value_type& operator[] (size_type ie)       { return base::operator[] (ie); }

        iterator begin()       { return base::begin(); }
  const_iterator begin() const { return base::begin(); }
        iterator end()         { return base::end(); }
  const_iterator end()   const { return base::end(); }

  dis_reference dis_entry (size_type dis_i) { return dis_reference (*this, dis_i); }

  void dis_entry_assembly_begin ();
  void dis_entry_assembly_end ();
  void dis_entry_assembly ()       { dis_entry_assembly_begin (); dis_entry_assembly_end (); }

  template<class Set, class Map>
  void append_dis_entry (const Set& ext_idx_set, Map& ext_idx_map) const;

  template<class Set, class Map>
  void get_dis_entry (const Set& ext_idx_set, Map& ext_idx_map) const {
    	    ext_idx_map.clear();
    	    append_dis_entry (ext_idx_set, ext_idx_map);
  }

  template<class Set>
  void append_dis_indexes (const Set& ext_idx_set) const { append_dis_entry (ext_idx_set, _ext_x); }

  template<class Set>
  void set_dis_indexes    (const Set& ext_idx_set) { get_dis_entry    (ext_idx_set, _ext_x); }
  
  void update_dis_entries() const;

  const_reference dis_at (size_type dis_i) const;

  template <class A2>
  void repartition (					      // old_numbering for *this
        const disarray_rep<size_type,distributed,A2>& partition,	      // old_ownership
        hack_array_mpi_rep<T,A>&                new_array,	      // new_ownership (created)
        disarray_rep<size_type,distributed,A2>&    old_numbering,     // new_ownership
        disarray_rep<size_type,distributed,A2>&    new_numbering) const; // old_ownership

#ifdef TODO
    void permutation_apply (				      // old_numbering for *this
        const disarray_rep<size_type,distributed,>& new_numbering,        // old_ownership
        disarray_rep<T,distributed>&               new_array) const;     // new_ownership (already allocated)

    void reverse_permutation (                                // old_ownership for *this=iold2dis_inew
        disarray_rep<size_type,distributed>& inew2dis_iold) const;       // new_ownership
#endif // TODO

    // get all external pairs (dis_i, values):
    const scatter_map_type& get_dis_map_entries() const { return _ext_x; }

// i/o:

  idiststream& get_values (idiststream& ips);
  odiststream& put_values (odiststream& ops) const;
  template <class GetFunction> idiststream& get_values (idiststream& ips, GetFunction get_element);
  template <class PutFunction> odiststream& put_values (odiststream& ops, PutFunction put_element) const;

  template <class PutFunction, class Permutation>
  odiststream& permuted_put_values (
	odiststream& 	                   ops,	
	const Permutation&                 perm,
        PutFunction                        put_element) const;

protected:
    void set_dis_entry (size_type dis_i, const generic_value_type& val);
// typedefs:
    /** 1) stash: store data before assembly() communications:
      */
    typedef std::map <size_type, raw_type>   stash_map_type; // TODO: map<T,A> for heap_alloc

    /** 2) message: for communication during assembly_begin(), assembly_end()
      */
    struct message_type {
        std::list<std::pair<size_type,mpi::request> >    waits; // TODO: list<T,A> for heap_alloc
        std::vector<std::pair<size_type,raw_type> >      data;  // TODO: vector<T,A> for heap_alloc
        message_type() : waits(), data() {}
    };
    /** 3) scatter (get_entry): specialized versions for T=container and T=simple type
      */
protected:
// data:
    stash_map_type   _stash;		// for assembly msgs:
    message_type     _send;
    message_type     _receive;
    size_type        _receive_max_size;
    mutable scatter_map_type _ext_x;	// for ext values (scatter)
};
#endif // _RHEOLEF_HAVE_MPI

/*Class:hack_array
NAME:  hack_array - container in distributed environment (@PACKAGE@-@VERSION@)
SYNOPSIS:       
@noindent
STL-like vector container for a distributed memory machine model.
Contrarily to disarray<T>, here T can have a size only known at compile time.
This class is used when T is a geo_element raw class, i.e. T=geo_element_e_raw.
The size of the geo_element depends upon the oder and is known only at run-time.
For efficiency purpose, the hack_array allocate all geo_elements of the 
same variant (e.g. edge) and order in a contiguous area, since the coreesponding
element size is constant.
EXAMPLE:
@noindent
 A sample usage of the class is:
@example
    std::pair<size_t,size_t> param (reference_element::t, 3); // triangle, order=3
    hack_array<geo_element_raw> x (distributor(100), param);
@end example
The hack_array<T> interface is similar to those of the disarray<T> one.

OBJECT REQUIREMENT:
 There are many pre-requises for the template objet type T:
@example
    class T : public T::generic_type @{
     typedef variant_type;
     typedef raw_type;
     typedef genetic_type;
     typedef automatic_type;
     static const variant_type _variant;
     static size_t _data_size(const parameter_type& param);
     static size_t _value_size(const parameter_type& param);
    @};
    class T::automatic_type : public T::generic_type @{
     automatic_type (const parameter_type& param);
    @};
    class T::generic_type @{
     typedef raw_type;
     typedef iterator;
     typedef const_iterator;
     iterator _data_begin();
     const_iterator _data_begin() const;
    @};
    ostream& operator<< (ostream&, const T::generic_type&);
@end example
AUTHOR: Pierre.Saramito@imag.fr
End:
*/
template <class T, class M = rheo_default_memory_model, class A = std::allocator<T> >
class hack_array {
public:
    typedef M memory_type;
    typedef typename std::vector<T,A>::size_type      size_type;
    typedef typename std::vector<T,A>::iterator       iterator;
    typedef typename std::vector<T,A>::const_iterator const_iterator;
};
//<verbatim:
template <class T, class A>
class hack_array<T,sequential,A> : public smart_pointer<hack_array_seq_rep<T,A> > {
public:

// typedefs:

    typedef hack_array_seq_rep<T,A>    rep;
    typedef smart_pointer<rep> 		  base;

    typedef sequential 			  memory_type;
    typedef typename rep::size_type 	  size_type;
    typedef typename rep::value_type 	  value_type;
    typedef typename rep::reference 	  reference;
    typedef typename rep::dis_reference   dis_reference;
    typedef typename rep::iterator 	  iterator;
    typedef typename rep::const_reference const_reference;
    typedef typename rep::const_iterator  const_iterator;
    typedef typename rep::parameter_type  parameter_type;

// allocators:

    hack_array (const A& alloc = A());
    hack_array (size_type loc_size,           const parameter_type& param, const A& alloc = A());
    void resize   (const distributor& ownership, const parameter_type& param);
    hack_array (const distributor& ownership, const parameter_type& param, const A& alloc = A());
    void resize   (size_type loc_size,           const parameter_type& param);

// local accessors & modifiers:

    A get_allocator() const              { return base::data().get_allocator(); }
    size_type     size () const          { return base::data().size(); }
    size_type dis_size () const          { return base::data().dis_size(); }
    const distributor& ownership() const { return base::data().ownership(); }
    const communicator& comm() const     { return ownership().comm(); }

    reference       operator[] (size_type i)       { return base::data().operator[] (i); }
    const_reference operator[] (size_type i) const { return base::data().operator[] (i); }

    const_reference dis_at (size_type dis_i) const { return base::data().operator[] (dis_i); }

          iterator begin()       { return base::data().begin(); }
    const_iterator begin() const { return base::data().begin(); }
          iterator end()         { return base::data().end(); }
    const_iterator end() const   { return base::data().end(); }

// global accessors (for compatibility with distributed interface):

    template<class Set> void append_dis_indexes (const Set& ext_idx_set) const {}
    void update_dis_entries() const {}

// global modifiers (for compatibility with distributed interface):

    dis_reference dis_entry (size_type dis_i) { return operator[] (dis_i); }
    void dis_entry_assembly()                 {}
    template<class SetOp>
    void dis_entry_assembly(SetOp my_set_op)        {}
    template<class SetOp>
    void dis_entry_assembly_begin (SetOp my_set_op) {}
    template<class SetOp>
    void dis_entry_assembly_end (SetOp my_set_op)   {}

// apply a partition:
 
#ifdef TODO
    template<class RepSize>
    void repartition (			             // old_numbering for *this
        const RepSize&         partition,	     // old_ownership
        hack_array<T,sequential,A>& new_array,	     // new_ownership (created)
        RepSize&               old_numbering,	     // new_ownership
        RepSize&               new_numbering) const  // old_ownership
        { return base::data().repartition (partition, new_array, old_numbering, new_numbering); }

    template<class RepSize>
    void permutation_apply (                       // old_numbering for *this
        const RepSize&          new_numbering,     // old_ownership
        hack_array<T,sequential,A>&  new_array) const   // new_ownership (already allocated)
        { return base::data().permutation_apply (new_numbering, new_array); }
#endif // TODO

// i/o:

    odiststream& put_values (odiststream& ops) const { return base::data().put_values(ops); }
    idiststream& get_values (idiststream& ips)       { return base::data().get_values(ips); }
    template <class GetFunction>
    idiststream& get_values (idiststream& ips, GetFunction get_element)       { return base::data().get_values(ips, get_element); }
    template <class PutFunction>
    odiststream& put_values (odiststream& ops, PutFunction put_element) const { return base::data().put_values(ops, put_element); }
#ifdef TODO
    void dump (std::string name) const { return base::data().dump(name); }
#endif // TODO
};
//>verbatim:
template <class T, class A>
inline
hack_array<T,sequential,A>::hack_array (
        const A&  alloc)
 : base(new_macro(rep(alloc)))
{
}
template <class T, class A>
inline
hack_array<T,sequential,A>::hack_array (
    	size_type             loc_size,
	const parameter_type& param,
        const A&              alloc)
 : base(new_macro(rep(loc_size,param,alloc)))
{
}
template <class T, class A>
inline
hack_array<T,sequential,A>::hack_array (
    	const distributor&    ownership,
	const parameter_type& param,
        const A&              alloc)
 : base(new_macro(rep(ownership,param,alloc)))
{
}
template <class T, class A>
inline
void
hack_array<T,sequential,A>::resize (
    	size_type             loc_size,
	const parameter_type& param)
{
  base::data().resize (loc_size,param);
}
template <class T, class A>
inline
void
hack_array<T,sequential,A>::resize (
    	const distributor& ownership,
	const parameter_type& param)
{
  base::data().resize (ownership,param);
}

#ifdef _RHEOLEF_HAVE_MPI
//<verbatim:
template <class T, class A>
class hack_array<T,distributed,A> : public smart_pointer<hack_array_mpi_rep<T,A> > {
public:

// typedefs:

    typedef hack_array_mpi_rep<T,A>    rep;
    typedef smart_pointer<rep> 		  base;

    typedef distributed 		  memory_type;
    typedef typename rep::size_type 	  size_type;
    typedef typename rep::value_type 	  value_type;
    typedef typename rep::reference 	  reference;
    typedef typename rep::dis_reference   dis_reference;
    typedef typename rep::iterator 	  iterator;
    typedef typename rep::parameter_type  parameter_type;
    typedef typename rep::const_reference const_reference;
    typedef typename rep::const_iterator  const_iterator;
    typedef typename rep::scatter_map_type scatter_map_type;

// allocators:

    hack_array (const A& alloc = A());
    hack_array (const distributor& ownership, const parameter_type& param, const A& alloc = A());
    void resize   (const distributor& ownership, const parameter_type& param);

// local accessors & modifiers:

    A get_allocator() const              { return base::data().get_allocator(); }
    size_type     size () const          { return base::data().size(); }
    size_type dis_size () const          { return base::data().dis_size(); }
    const distributor& ownership() const { return base::data().ownership(); }
    const communicator& comm() const     { return base::data().comm(); }

    reference       operator[] (size_type i)       { return base::data().operator[] (i); }
    const_reference operator[] (size_type i) const { return base::data().operator[] (i); }

          iterator begin()       { return base::data().begin(); }
    const_iterator begin() const { return base::data().begin(); }
          iterator end()         { return base::data().end(); }
    const_iterator end() const   { return base::data().end(); }

// global accessor:

    template<class Set, class Map>
    void append_dis_entry (const Set& ext_idx_set, Map& ext_idx_map) const { base::data().append_dis_entry (ext_idx_set, ext_idx_map); }

    template<class Set, class Map>
    void get_dis_entry    (const Set& ext_idx_set, Map& ext_idx_map) const { base::data().get_dis_entry (ext_idx_set, ext_idx_map); }

    template<class Set>
    void append_dis_indexes (const Set& ext_idx_set) const { base::data().append_dis_indexes (ext_idx_set); }

    template<class Set>
    void set_dis_indexes    (const Set& ext_idx_set)  { base::data().set_dis_indexes (ext_idx_set); }

    const_reference dis_at (size_type dis_i) const { return base::data().dis_at (dis_i); }

    // get all external pairs (dis_i, values):
    const scatter_map_type& get_dis_map_entries() const { return base::data().get_dis_map_entries(); }

    void update_dis_entries() const { base::data().update_dis_entries(); }

// global modifiers (for compatibility with distributed interface):

    dis_reference dis_entry (size_type dis_i)       { return base::data().dis_entry(dis_i); }

    void dis_entry_assembly()                       { return base::data().dis_entry_assembly(); }

    template<class SetOp>
    void dis_entry_assembly       (SetOp my_set_op) { return base::data().dis_entry_assembly       (my_set_op); }
    template<class SetOp>
    void dis_entry_assembly_begin (SetOp my_set_op) { return base::data().dis_entry_assembly_begin (my_set_op); }
    template<class SetOp>
    void dis_entry_assembly_end   (SetOp my_set_op) { return base::data().dis_entry_assembly_end   (my_set_op); }

// apply a partition:

    template<class RepSize>
    void repartition (			            // old_numbering for *this
        const RepSize&        partition,            // old_ownership
        hack_array<T,distributed>& new_array,            // new_ownership (created)
        RepSize&              old_numbering,        // new_ownership
        RepSize&              new_numbering) const  // old_ownership
        { return base::data().repartition (partition.data(), new_array.data(), old_numbering.data(), new_numbering.data()); }

#ifdef TODO
    template<class RepSize>
    void permutation_apply (                       // old_numbering for *this
        const RepSize&          new_numbering,     // old_ownership
        hack_array<T,distributed,A>& new_array) const   // new_ownership (already allocated)
        { base::data().permutation_apply (new_numbering.data(), new_array.data()); }

    void reverse_permutation (                                 // old_ownership for *this=iold2dis_inew
        hack_array<size_type,distributed,A>& inew2dis_iold) const   // new_ownership
        { base::data().reverse_permutation (inew2dis_iold.data()); }
#endif // TODO

// i/o:

    odiststream& put_values (odiststream& ops) const { return base::data().put_values(ops); }
    idiststream& get_values (idiststream& ips)       { return base::data().get_values(ips); }
#ifdef TODO
    void dump (std::string name) const 	    { return base::data().dump(name); }
#endif // TODO

    template <class GetFunction>
    idiststream& get_values (idiststream& ips, GetFunction get_element)   
	     { return base::data().get_values(ips, get_element); }
    template <class PutFunction>
    odiststream& put_values (odiststream& ops, PutFunction put_element) const
	     { return base::data().put_values(ops, put_element); }
  
    template <class PutFunction, class Permutation>
    odiststream& permuted_put_values (
	odiststream& 	                   ops,	
	const Permutation&                 perm,
        PutFunction                        put_element) const
	     { return base::data().permuted_put_values (ops, perm.data(), put_element); }
};
//>verbatim:
template <class T, class A>
inline
hack_array<T,distributed,A>::hack_array (
        const A&           alloc)
 : base(new_macro(rep(alloc)))
{
}
template <class T, class A>
inline
hack_array<T,distributed,A>::hack_array (
    	const distributor&    ownership,
	const parameter_type& param,
        const A&              alloc)
 : base(new_macro(rep(ownership,param,alloc)))
{
}
template <class T, class A>
inline
void
hack_array<T,distributed,A>::resize (
    	const distributor&    ownership,
	const parameter_type& param)
{
  base::data().resize (ownership,param);
}
#endif // _RHEOLEF_HAVE_MPI

// -------------------------------------------------------------
// i/o with operator<< & >>
// -------------------------------------------------------------
template <class T, class A>
inline
idiststream&
operator >> (idiststream& ips,  hack_array<T,sequential,A>& x)
{ 
    return x.get_values(ips); 
}
template <class T, class A>
inline
odiststream&
operator << (odiststream& ops, const hack_array<T,sequential,A>& x)
{
    return x.put_values(ops);
}
#ifdef _RHEOLEF_HAVE_MPI
template <class T, class A>
inline
idiststream&
operator >> (idiststream& ips,  hack_array<T,distributed,A>& x)
{ 
    return x.get_values(ips); 
}
template <class T, class A>
inline
odiststream&
operator << (odiststream& ops, const hack_array<T,distributed,A>& x)
{
    return x.put_values(ops);
}
#endif // _RHEOLEF_HAVE_MPI

}// namespace rheolef

// -------------------------------------------------------------
// not inlined : longer code
// -------------------------------------------------------------
#include "rheolef/hack_array_seq.icc"
#include "rheolef/hack_array_mpi.icc"

#endif // _RHEOLEF_HACK_ARRAY_H
