///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/adapt.h"
#include "rheolef/form.h"
#include "rheolef/field_wdof_sliced.h"
#include "rheolef/rheostream.h"
#include "rheolef/field_expr.h"

namespace rheolef {

// extern in adapt.cc
template<class T, class M>
field_basic<T,M>
hessian_criterion (
    const field_basic<T,M>&  uh0,
    const adapt_option& opts);

template<class T, class M>
field_basic<T,M>
proj (const field_basic<T,M>& uh, const std::string& approx = "P1");

// -------------------------------------------
// bamgcad handler
// -------------------------------------------
// catch Geometry "NAME.bamgcad" and return "NAME"
std::string bamgcad_catch_name (idiststream& bamg_in)
{
  using namespace std;
  if (!bamg_in) return string();
  check_macro (dis_scatch(bamg_in, "Geometry"), "mark \"Geometry\" not founded in bamg file");
  string arg;
  bamg_in >> arg;
  ostringstream out;
  for (size_t i = 0; i < arg.length(); ++i) {
    if (arg[i] != '"') out << arg[i];
  }
  string file_bamgcad = out.str();
  return file_bamgcad;
}
// -----------------------------------------
// adapt
// -----------------------------------------
template<class T, class M>
geo_basic<T,M>
adapt_bamg (
    const field_basic<T,M>&  uh,
    const adapt_option& opts)
{
  using namespace std;
  typedef typename geo_basic<T,M>::size_type size_type;
  bool do_verbose  = iorheo::getverbose(clog);
  bool do_clean    = iorheo::getclean(clog);
  string clean_files;
  string command;
  // -----------------------------------------
  // 1a) sortie du maillage : nom-<i>.geo
  // -----------------------------------------
  const geo_basic<T,M>& omega = uh.get_geo();
  size_type i = omega.serial_number();
  string i_name = omega.name();
  string geo_name = i_name + ".geo";
  odiststream out;
  if (! dis_file_exists(geo_name) &&
      ! dis_file_exists(geo_name + ".gz")) {
    out.open (geo_name);
    out << omega;
    check_macro (out.good(), "adapt: file \"" << geo_name << "\"creation failed");
    out.close();
    clean_files += " " + geo_name + " " + geo_name + ".gz";
  }
  // ----------------------------------------
  // 1b) conversion geo : nom-<i>.bamg
  // ----------------------------------------
  string bamg_name = i_name + ".bamg";
  if (i != 0) {
    clean_files += " " + bamg_name;
  }
  // -----------------------------------------
  // 2a) sortie du critere : nom-crit-<i>.field
  // -----------------------------------------
  string crit_name = i_name + "-crit.field";
  clean_files += " " + crit_name + ".gz";
  out.open (crit_name, "field");
  out << proj (uh);
  check_macro (out.good(), "adapt: file \"" << crit_name << "\"creation failed");
  out.close();
  // --------------------------------------------
  // 2b) conversion field : nom-crit-<i>.bb
  // --------------------------------------------
  string bb_name = i_name + "-crit.bb";
  clean_files += " " + bb_name;
  string bindir = string(_RHEOLEF_RHEOLEF_LIBDIR) + "/rheolef";
  command = "zcat < " + crit_name + ".gz | " + bindir + "/field2bb > " + bb_name;
  if (do_verbose) derr << "! " << command << endl;
  check_macro (dis_system (command) == 0, "adapt: unix command failed");
  // ----------------------------------------
  // 3) run bamg in adapt mode:
  //     => nom-<i+1>.bamg
  // ----------------------------------------
  idiststream bamg_in (bamg_name);
  string bamgcad_file = bamgcad_catch_name (bamg_in);
  if (bamgcad_file.length() == 0) bamgcad_file = omega.familyname() + ".bamgcad";
  check_macro (dis_file_exists(bamgcad_file), "adapt: missing \""<<bamgcad_file<<"\" file");
  size_type k = uh.get_space().degree();
  if (! uh.get_space().get_basis().is_continuous()) k++; // uh is a grad of a solution
  Float error = opts.err;
  if (k > 1) {
    // when k > 1, decreses the interpolation error level for bamg, as computed when k=1
    error = pow (error, 2./(k+1));
  }
  string options =
       "-coef "     + ftos(opts.hcoef)
    + " -err  "     + ftos(error)
    + " -errg "     + ftos(opts.errg)
    + " -hmin "     + ftos(opts.hmin)
    + " -hmax "     + ftos(opts.hmax)
    + " -ratio "    + ftos(opts.ratio)
    + " -anisomax " + ftos(opts.anisomax)
    + " -nbv "      + std::to_string(opts.n_vertices_max)
    + " -NbJacobi " + std::to_string(opts.n_smooth_metric)
    + " -CutOff "   + ftos(opts.cutoff);
  if (opts.splitpbedge) {
    options += " -splitpbedge ";
  }
  if (opts.thetaquad != numeric_limits<Float>::max()) {
    options += " -thetaquad " + ftos(opts.thetaquad);
  }
  options += " " + opts.additional;
  char buffer [100];
  sprintf (buffer, "%.3d", int(i+1)); // see geo::name() in geo.cc
  string i1_name = omega.familyname() + "-" + buffer;
  command = "bamg "
    + options
    + " -b " + bamg_name
    + " -Mbb " + bb_name
    + " -o " + i1_name + ".bamg 1>&2";
  if (do_verbose) derr << "! " << command << endl;
  check_macro (dis_system (command) == 0, "adapt: command failed");
  // ----------------------------------------
  // 4) conversion : nom-crit-<i>.bamg
  // ----------------------------------------
  string dmn_name = delete_any_suffix(bamgcad_file) + ".dmn";
  check_macro (dis_file_exists(dmn_name), "adapt: missing \""<<dmn_name<<"\" file");
  command = "cat " + i1_name + ".bamg " + dmn_name
          + " | bamg2geo -" + omega.coordinate_system_name()
          + " | gzip -9 > " + i1_name + ".geo.gz";
  if (do_verbose) derr << "! " << command << endl;
  check_macro (dis_system (command) == 0, "adapt: command failed");
  // ----------------------------------------
  // 5) chargement  nom-<i+1>.geo
  // ----------------------------------------
  idiststream in (i1_name, "geo");
  geo_basic<T,M> new_omega;
  in >> new_omega;
  new_omega.set_name (omega.familyname());
  new_omega.set_serial_number (i+1);
  clean_files += " " + i1_name + ".geo.gz";
  // ----------------------------------------
  // 6) clean
  // ----------------------------------------
  if (do_clean) {
    command = "rm -f" + clean_files;
    if (do_verbose) derr << "! " << command << endl;
    check_macro (dis_system (command) == 0, "adapt: command failed");
  }
  return new_omega;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M) 							\
template geo_basic<T,M> adapt_bamg (const field_basic<T,M>&, const adapt_option&);

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
