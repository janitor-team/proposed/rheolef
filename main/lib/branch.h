#ifndef _RHEO_BRANCH_H
#define _RHEO_BRANCH_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr

namespace rheolef {
/**
@classfile branch parameter-dependent sequence of field
@addindex animation
@addindex continuation methods
@addindex time-dependent problems

Description
===========
The `branch` class stores a `field` sequence together with its associated parameter
value.
A `branch` variable represents a pair `(t,uh(t))`
for a specific value of the parameter `t`.
Applications concern time-dependent problems and continuation methods.
This class is convenient for file inputs/outputs
and for building animations.
It extends to multi-field sequences, such as `(t,uh(t),ph(t))`,
up to an arbitrarily number of fields.
See also the @ref branch_1 unix command for running animations.

Implementation
==============
@showfromfile
The `branch` class is simply an alias to the `branch_basic` class

@snippet branch.h verbatim_branch
@par

The `branch_basic` class provides an interface
to a n-uplet of fields together with a parameter value:

@snippet branch.h verbatim_branch_basic
@snippet branch.h verbatim_branch_basic_cont
*/
} // namespace rheolef

#include "rheolef/field.h"

namespace rheolef { 

template <class T, class M> class __obranch;
template <class T, class M> class __iobranch;
template <class T, class M> class __branch_header;
template <class T, class M> class __const_branch_header;
template <class T, class M> class __const_branch_finalize;

// =============================================================================
// class definition
// =============================================================================

// [verbatim_branch_basic]
template <class T, class M = rheo_default_memory_model>
class branch_basic : public std::vector<std::pair<std::string,field_basic<T,M> > > {
public :
// typedefs:

  typedef std::vector<std::pair<std::string,field_basic<T,M> > >     base;
  typedef typename base::size_type                                   size_type;
        
// allocators:

  branch_basic();
  ~branch_basic();
  branch_basic (const branch_basic<T,M>&);
  branch_basic<T,M>& operator= (const branch_basic<T,M>&);

  template <typename... Args>
  branch_basic(const std::string& parameter, Args... names);

// accessors:

  const T&  parameter () const;
  const std::string& parameter_name () const;
  size_type     n_value () const;
  size_type     n_field () const;

// modifiers:

  void set_parameter_name (const std::string& name);
  void set_parameter (const T& value);
  void set_range (const std::pair<T,T>& u_range);
// [verbatim_branch_basic]

// input/output:

  // get/set current value
  template <typename... Args>
  __obranch<T,M>  operator()  (const T& t, const field_basic<T,M>& u0, Args... uk);
  template <typename... Args>
  __iobranch<T,M> operator()  (T& t,             field_basic<T,M>& u0, Args&... uk);

  __branch_header<T,M>         header ();
  __const_branch_header<T,M>   header () const;
  __const_branch_finalize<T,M> finalize () const;

// implementation

public:
  template <class T1, class M1> friend class  __obranch;
  template <class T1, class M1> friend class __iobranch;

// inputs (internals):

  template <class T1> friend void get_header (idiststream&, branch_basic<T1,sequential>&);
  template <class T1> friend void get_event  (idiststream&, branch_basic<T1,sequential>&);

#ifdef _RHEOLEF_HAVE_MPI
  template <class T1> friend void get_header (idiststream&, branch_basic<T1,distributed>&);
  template <class T1> friend void get_event  (idiststream&, branch_basic<T1,distributed>&);
#endif // _RHEOLEF_HAVE_MPI

  void get_header_rheolef (idiststream&);
  void get_event_rheolef  (idiststream&);

  template <class T1> friend void get_header_vtk (idiststream&, branch_basic<T1,sequential>&);
  template <class T1> friend void get_event_vtk  (idiststream&, branch_basic<T1,sequential>&);

// outputs (internals):

  void put_header   (odiststream&) const;
  void put_finalize (odiststream&) const;

// file formats
  template <class T1> friend void put_header  (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_event   (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_finalize(odiststream&, const branch_basic<T1,sequential>&);

#ifdef _RHEOLEF_HAVE_MPI
  template <class T1> friend void put_header  (odiststream&, const branch_basic<T1,distributed>&);
  template <class T1> friend void put_event   (odiststream&, const branch_basic<T1,distributed>&);
  template <class T1> friend void put_finalize(odiststream&, const branch_basic<T1,distributed>&);
#endif // _RHEOLEF_HAVE_MPI

  void put_header_rheolef   (odiststream&) const;
  void put_event_rheolef    (odiststream&) const;
  void put_finalize_rheolef (odiststream&) const;

  template <class T1> friend void put_header_gnuplot   (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_event_gnuplot    (odiststream&, const branch_basic<T1,sequential>&);

// file formats
  template <class T1> friend void put_header  (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_event   (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_finalize(odiststream&, const branch_basic<T1,sequential>&);

#ifdef _RHEOLEF_HAVE_MPI
  template <class T1> friend void put_header  (odiststream&, const branch_basic<T1,distributed>&);
  template <class T1> friend void put_event   (odiststream&, const branch_basic<T1,distributed>&);
  template <class T1> friend void put_finalize(odiststream&, const branch_basic<T1,distributed>&);
#endif // _RHEOLEF_HAVE_MPI

  template <class T1> friend void put_header_gnuplot   (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_event_gnuplot    (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_finalize_gnuplot (odiststream&, const branch_basic<T1,sequential>&);

  template <class T1> friend void put_header_paraview  (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_event_paraview   (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_finalize_paraview(odiststream&, const branch_basic<T1,sequential>&);

  template <class T1> friend void put_header_vtk       (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_event_vtk        (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_event_vtk_stream (odiststream&, const branch_basic<T1,sequential>&);
  template <class T1> friend void put_finalize_vtk     (odiststream&, const branch_basic<T1,sequential>&);

private:
  template <typename... Args>
  void fill_field_names (const std::string &field_name, Args... others);
  void fill_field_names () {}
  template <typename... Args>
  void fill_field_values (std::size_t i, const field_basic<T,M>& ui, Args... uk);
  void fill_field_values (std::size_t i) {}
// data:
protected:
  std::string            _parameter_name;
  T                      _parameter_value;
  size_type              _n_value;
  mutable size_type      _count_value;
  mutable std::ostream*  _p_data_out;
  mutable std::ostream*  _p_ctrl_out;
  mutable bool           _header_in_done;
  mutable bool           _header_out_done;
  mutable bool           _finalize_out_done;
  mutable std::string    _to_clean;
  mutable std::pair<T,T> _u_range;
  mutable std::pair<bool,bool> _have_u_range;
// [verbatim_branch_basic_cont]
};
template <class T, class M> idiststream& operator>> (idiststream&, branch_basic<T,M>&);
template <class T, class M> odiststream& operator<< (odiststream&, const branch_basic<T,M>&);
// [verbatim_branch_basic_cont]

template <class T, class M>
idiststream& operator>> (idiststream&, branch_basic<T,M>&);

template <class T, class M>
odiststream& operator<< (odiststream&, const branch_basic<T,M>&);

// [verbatim_branch]
typedef branch_basic<Float> branch;
// [verbatim_branch]

// =============================================================================
// inlined
// =============================================================================
template <class T, class M>
inline
branch_basic<T,M>::branch_basic ()
 : std::vector<std::pair<std::string,field_basic<T,M> > >(0),
   _parameter_name ("*unnamed*"),
   _parameter_value(std::numeric_limits<T>::max()),
   _n_value        (std::numeric_limits<size_type>::max()),
   _count_value    (std::numeric_limits<size_type>::max()),
   _p_data_out	   (0),
   _p_ctrl_out	   (0),
   _header_in_done     (false),
   _header_out_done    (false),
   _finalize_out_done  (false),
   _to_clean(),
   _u_range(std::pair<T,T>( std::numeric_limits<T>::max(),
                           -std::numeric_limits<T>::max())),
   _have_u_range(std::pair<bool,bool>(false,false))
{
}
template <class T, class M>
inline
branch_basic<T,M>::branch_basic (const branch_basic<T,M>& x)
 : std::vector<std::pair<std::string,field_basic<T,M> > >(x),
   _parameter_name (x._parameter_name),
   _parameter_value(x._parameter_value),
   _n_value        (x._n_value),
   _count_value    (x._count_value),
   _p_data_out	   (x._p_data_out),
   _p_ctrl_out	   (x._p_ctrl_out),
   _header_in_done     (x._header_in_done),
   _header_out_done    (x._header_out_done),
   _finalize_out_done  (x._finalize_out_done),
   _to_clean       (x._to_clean),
   _u_range        (x._u_range),
   _have_u_range   (x._have_u_range)
{
}
template <class T, class M>
inline
branch_basic<T,M>&
branch_basic<T,M>::operator= (const branch_basic<T,M>& x)
{
   std::vector<std::pair<std::string,field_basic<T,M> > >::operator= (x);
   _parameter_name  = x._parameter_name;
   _parameter_value = x._parameter_value;
   _n_value         = x._n_value;
   _count_value     = x._count_value;
   _p_data_out	    = x._p_data_out;
   _p_ctrl_out	    = x._p_ctrl_out;
   _header_in_done      = x._header_in_done;
   _header_out_done     = x._header_out_done;
   _finalize_out_done   = x._finalize_out_done;
   _to_clean        = x._to_clean;
   _u_range         = x._u_range;
   _have_u_range    = x._have_u_range;
   return *this;
}
template <class T, class M>
    template <typename... Args>
inline branch_basic<T,M>::branch_basic(const std::string& parameter_name, Args... field_names) 
 : std::vector<std::pair<std::string,field_basic<T,M> > >(0),
   _parameter_name (parameter_name),
   _parameter_value(std::numeric_limits<Float>::max()),
   _n_value        (std::numeric_limits<size_type>::max()),
   _count_value    (std::numeric_limits<size_type>::max()),
   _p_data_out	   (0),
   _p_ctrl_out	   (0),
   _header_in_done     (false),
   _header_out_done    (false),
   _finalize_out_done  (false),
   _to_clean(),
   _u_range(std::pair<T,T>( std::numeric_limits<T>::max(),
                           -std::numeric_limits<T>::max())),
   _have_u_range(std::pair<bool,bool>(false,false))
{
    check_macro (parameter_name.length() != 0, "empty parameter name not allowed");
    fill_field_names (field_names...);
}
template <class T, class M>
template <typename... Args>
inline void branch_basic<T,M>::fill_field_names (const std::string& field_name, Args... others) {
    check_macro (field_name.length() != 0, "empty field name not allowed");
    base::push_back (std::make_pair(field_name, field_basic<T,M>()));
    fill_field_names (others...);
}
// -----------------------------------------------------------------------------
// accessors
// -----------------------------------------------------------------------------
template <class T, class M>
inline
const T&
branch_basic<T,M>::parameter () const
{
    return _parameter_value;
}
template <class T, class M>
inline
const std::string&
branch_basic<T,M>::parameter_name () const
{
    return _parameter_name;
}
template <class T, class M>
inline
typename branch_basic<T,M>::size_type
branch_basic<T,M>::n_value () const
{
    return _n_value;
}
template <class T, class M>
inline
typename branch_basic<T,M>::size_type
branch_basic<T,M>::n_field () const
{
    return base::size();
}
// -----------------------------------------------------------------------------
// modifiers
// -----------------------------------------------------------------------------
template <class T, class M>
inline
void 
branch_basic<T,M>::set_parameter_name (const std::string& name)
{
    _parameter_name = name;
}
template <class T, class M>
inline
void 
branch_basic<T,M>::set_parameter (const T& x)
{
    _parameter_value = x;
}
template <class T, class M>
inline
void 
branch_basic<T,M>::set_range (const std::pair<T,T>& u_range)
{
   _u_range = u_range;
   _have_u_range = std::pair<bool,bool>(false,false);
}
// -----------------------------------------------------------------------------
// io header wrapper
// -----------------------------------------------------------------------------
template <class T, class M>
class __branch_header {
    public:
	__branch_header (branch_basic<T,M>& b) : _b(b) {}
	friend idiststream& operator>> (idiststream& in, __branch_header<T,M> h) { 
		get_header(in, h._b); return in; }
	friend odiststream& operator<< (odiststream& out, __branch_header<T,M> h) { 
		h._b.put_header(out); return out; }
    protected:
    	branch_basic<T,M>& _b;
	template <class T1, class M1> friend class __const_branch_header;
};
template <class T, class M>
class __const_branch_header {
    public:
	__const_branch_header (const branch_basic<T,M>& b) : _b(b) {}
	__const_branch_header (__branch_header<T,M> h) : _b(h._b) {}
	friend odiststream& operator<< (odiststream& out, __const_branch_header<T,M> h) { 
		h._b.put_header(out); return out; }
    protected:
    	const branch_basic<T,M>& _b;
};
template <class T, class M>
inline
__branch_header<T,M>
branch_basic<T,M>::header ()
{
    return *this;
}
template <class T, class M>
inline
__const_branch_header<T,M>
branch_basic<T,M>::header () const
{
    return *this;
}
// -----------------------------------------------------------------------------
// o finalize wrapper
// -----------------------------------------------------------------------------
template <class T, class M>
class __const_branch_finalize {
    public:
	__const_branch_finalize (const branch_basic<T,M>& b) : _b(b) {}
	friend odiststream& operator<< (odiststream& out, __const_branch_finalize<T,M> h) { 
		h._b.put_finalize(out); return out; }
    protected:
    	const branch_basic<T,M>& _b;
};
template <class T, class M>
inline
__const_branch_finalize<T,M>
branch_basic<T,M>::finalize () const
{
    return *this;
}
// -----------------------------------------------------------------------------
// io value wrapper
// -----------------------------------------------------------------------------
template <class T, class M>
class __obranch {
    public:
        __obranch (odiststream& (*put)(odiststream&, const branch_basic<T,M>&), const branch_basic<T,M>& x)
         : _put(put), _x(x) {}
        friend odiststream& operator<< (odiststream& os, __obranch<T,M> m)
         { m._put (os, m._x); return os; }
    private:
	odiststream&    (*_put) (odiststream&, const branch_basic<T,M>&);
        const branch_basic<T,M>& _x;
};
template <class T, class M>
class __iobranch {
    public:
	template <typename... Args>
        __iobranch (odiststream& (*put)(odiststream&, const branch_basic<T,M>&), 
	            idiststream& (*get)(idiststream&, branch_basic<T,M>&),
		    branch_basic<T,M>& x,
		    T&                 t,
		    field_basic<T,M>&  u0, 
                    Args&...           uk)
         : _put(put), _get(get), _px(&x), _pt(&t), _pu()
         {
           std::size_t n = sizeof...(Args)+1;
           _pu.resize(n);
	   _pu[0] = &u0;
           fill_field_references (1, uk...);
         }
        __iobranch(const __iobranch<T,M>& x) 
         : _put(x._put), _get(x._get), _px(x._px), _pt(x._pt), _pu(x._pu) {}
	template <typename... Args>
        void fill_field_references (std::size_t i, field_basic<T,M>& ui, Args&... uk) {
           _pu [i] = &ui;
           fill_field_references (i+1, uk...);
        }
        void fill_field_references (std::size_t i) {}

        template <class T1, class M1>
	friend odiststream& operator<< (odiststream& os, __iobranch<T1,M1> m);
        template <class T1, class M1>
	friend idiststream& operator>> (idiststream& is, __iobranch<T1,M1> m);
    private:
	odiststream&    (*_put) (odiststream&, const branch_basic<T,M>&);
	idiststream&    (*_get) (idiststream&, branch_basic<T,M>&);
        branch_basic<T,M> *_px;
        T                 *_pt;

        std::vector<field_basic<T,M>*>  _pu;

private:
   __iobranch<T,M>& operator= (const __iobranch<T,M>&);
};
template <class T, class M>
inline
odiststream& operator<< (odiststream& os, __iobranch<T,M> m) {
    m._put (os, *m._px);
    return os;
}
template <class T, class M>
inline
idiststream& operator>> (idiststream& is, __iobranch<T,M> m) { 
    m._get (is, *m._px); 
    if (m._pt)  { *m._pt  = (*m._px).parameter(); }
    for (std::size_t i = 0, n = (*m._px).size(); i < n; ++i) {
      *(m._pu[i]) = (*m._px)[i].second;
    }
    return is;
}
template <class T, class M>
template <typename... Args>
inline 
__obranch<T,M> 
branch_basic<T,M>::operator()  (const T& t, const field_basic<T,M>& u0, Args... uk) {
    check_macro (base::size() >= sizeof...(Args)+1, "attempt to output a " << (sizeof...(Args)+1) <<"-field branch when a " 
            << base::size() << "-field one was supplied");
    _parameter_value = t;
    base::operator[](0).second = u0;
    fill_field_values (1, uk...);
    return __obranch<T,M> (operator<<, *this);
}
template <class T, class M>
template <typename... Args>
inline
__iobranch<T,M>
branch_basic<T,M>::operator() (T& t, field_basic<T,M>& u0, Args&... uk) {
    check_macro (base::size() >= sizeof...(Args)+1, "attempt to output a " << (sizeof...(Args)+1) <<"-field branch when a " 
            << base::size() << "-field one was supplied");
    _parameter_value = t;
    base::operator[](0).second = u0;
    fill_field_values (1, uk...);
    return __iobranch<T,M> (operator<<, operator>>, *this, t, u0, uk...);
}
template <class T, class M>
template <typename... Args>
inline
void branch_basic<T,M>::fill_field_values(std::size_t id, const field_basic<T,M>& u_id, Args... uk) {
    base::operator[](id++).second = u_id;
    fill_field_values(id, uk...);
}

}// namespace rheolef
#endif // define_RHEO_BRANCH_H
