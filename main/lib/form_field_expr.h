# ifndef _RHEOLEF_FORM_FIELD_EXPR_H
# define _RHEOLEF_FORM_FIELD_EXPR_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/form.h"
#include "rheolef/field_wdof_sliced.h"
#include "rheolef/field_wdof_indirect.h"
#include "rheolef/field_expr.h"
namespace rheolef { 

// -----------------------------------------------------------------------
// form*field_expr
// -----------------------------------------------------------------------
#ifdef TO_CLEAN
#define _RHEOLEF_form_mult_field_var_old(field_var)			\
template <class T, class M>						\
inline									\
field_basic<T,M>							\
operator* (const form_basic<T,M>& a, const field_var<T,M>& expr)	\
{									\
  field_basic<T,M> eh = expr;						\
  return a.operator* (eh);						\
}
#undef _RHEOLEF_form_mult_field_var_old
#endif // TO_CLEAN

// TODO: via a template FieldRdof instead of field_var<FieldRdof>
//       and move it in the form.h file
#define _RHEOLEF_form_mult_field_var(field_var)				\
template <class T, class M, class FieldRdof>				\
inline									\
field_basic<T,M>							\
operator* (const form_basic<T,M>& a, const field_var<FieldRdof>& expr)	\
{									\
  field_basic<T,M> eh = expr;						\
  return a.operator* (eh);						\
}
_RHEOLEF_form_mult_field_var(details::field_wdof_sliced)
_RHEOLEF_form_mult_field_var(details::field_rdof_sliced_const)
_RHEOLEF_form_mult_field_var(details::field_wdof_indirect)
_RHEOLEF_form_mult_field_var(details::field_rdof_indirect_const)
#undef _RHEOLEF_form_mult_field_var_new

template <class T, class M, class Expr>
inline
typename
std::enable_if<
       details::is_field_wdof<Expr>::value
  && ! details::is_field            <Expr>::value
 ,field_basic<T,M>
>::type
operator* (const form_basic<T,M>& a, const Expr& expr)
{
  field_basic<T,M> eh = expr;
  return a.operator* (eh);
}

}// namespace rheolef
# endif /* _RHEOLEF_FORM_FIELD_EXPR_H */
