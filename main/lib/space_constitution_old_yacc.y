%{
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
%}
%union  { 
        int             empty;
        size_t          string_value;
	list_type*	list;
	tree_type*	tree;
}
%type <tree>                    all top_expr expr factor basis_geo
%type <list>                    expr_list
%token<string_value>            IDENTIFIER VALUED
%%
all     : top_expr
		{ result_ptr = $1; }
top_expr: factor
	| expr_list
		{ $$ = new_macro(tree_type("mixed",*$1)); delete_macro ($1); }
	;
factor  : basis_geo
		{ $$ = $1; }
	| VALUED '(' basis_geo ')'
		{ list_type l; l.push_back(*$3); delete_macro ($3);
                  $$ = new_macro(tree_type(symbol($1),l));
                }
	;
basis_geo: IDENTIFIER '(' IDENTIFIER ')'
		{ $$ = new_macro(tree_type(symbol($1),symbol($3))); }
	;
expr_list: expr '*' expr
		{
		  $$ = new_macro (list_type);
		  $$->push_back(*$1); delete_macro ($1);
		  $$->push_back(*$3); delete_macro ($3);
		}
	| expr_list '*' expr
		{ $$ = $1; $$->push_back(*$3); delete_macro ($3); }
	;
expr	: factor
	| '(' expr_list ')'
		{ $$ = new_macro(tree_type("mixed",*$2)); delete_macro ($2); }
	;
%%
