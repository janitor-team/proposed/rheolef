#ifndef _RHEOLEF_GEO_DOMAIN_H
#define _RHEOLEF_GEO_DOMAIN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

/*Class:domain
NAME: @code{geo_domain} - a named part of a finite element mesh that behaves as a mesh
@cindex  mesh boundary
@clindex geo_domain
DESCRIPTION:
  @noindent
  The @code{geo_domain} class defines a container for a part of a
  finite element mesh.
  This class re-describes the vertices, edges or faces in 
  a compact way, i.e. by skipping unused elements from
  the surrounding mesh.
IMPLEMENTATION NOTE:
  The @code{geo_domain} class conserves the link to the original mesh
  such that fields defined on a @code{geo_domain} can inter-operate with
  fields defined on the surrounding mesh.
AUTHOR: Pierre.Saramito@imag.fr
DATE: 16 february 2011
End:
*/

#include "rheolef/geo.h"
#include "rheolef/geo_domain_indirect.h"

namespace rheolef {

// ========================================================================
// representation
// ========================================================================
template <class T, class M>
class geo_domain_rep : public geo_rep<T,M> {
public:
// typedefs:
    typedef geo_rep<T,M>                   base;
    typedef typename base::size_type       size_type;

// allocators:

    geo_domain_rep (const geo_domain_rep<T,M>&);
    geo_domain_rep (const geo_domain_indirect_rep<T,M>& dom);
    geo_abstract_rep<T,M>* clone() const;

// accessors: the geo_abstract interface overloaded here:

    size_type variant() const { return geo_abstract_base_rep<T>::geo_domain; }
    bool is_broken() const { return _dom.is_broken(); }
    const geo_element& bgd2dom_geo_element (const geo_element& bgd_K) const;
    const geo_element& dom2bgd_geo_element (const geo_element& dom_K) const;

    size_type                     n_domain_indirect() const { return 0; }
    const domain_indirect_basic<M>& get_domain_indirect (size_type i) const;
    const domain_indirect_basic<M>& get_domain_indirect (const std::string& name) const;

// extended accessors:

   const geo_basic<T,M>& get_background_geo()    const { return _dom.get_background_geo(); }
         geo_basic<T,M>  get_background_domain() const;

   size_type     bgd_ie2dom_ie     (size_type     bgd_ie) const;
   size_type dis_bgd_ie2dis_dom_ie (size_type dis_bgd_ie) const;

protected:
// data:
    geo_domain_indirect_rep<T,M>          _dom;
    std::map<size_type,size_type>         _bgd_ie2dom_ie,
                                          _dis_bgd_ie2dis_dom_ie;
};
// ------------------------------------------------------------------------
// inlined 
// ------------------------------------------------------------------------
template <class T, class M>
inline
geo_basic<T,M>
geo_domain_rep<T,M>::get_background_domain() const
{
    const domain_indirect_basic<M>& indirect = _dom.get_indirect ();
    const geo_basic<T,M>&              omega = _dom.get_background_geo ();
    return geo_basic<T,M> (indirect, omega);
}
template <class T, class M>
inline
const domain_indirect_basic<M>&
geo_domain_rep<T,M>::get_domain_indirect (size_type i) const
{
    return geo_rep<T,M>::get_domain_indirect (i);
}
template <class T, class M>
inline
const domain_indirect_basic<M>&
geo_domain_rep<T,M>::get_domain_indirect (const std::string& name) const
{
    return geo_rep<T,M>::get_domain_indirect (name);
}
// ------------------------------------------------------------------------
// geo_basic<T,M> complement
// ------------------------------------------------------------------------
template <class T, class M>
geo_basic<T,M>
compact (const geo_basic<T,M>& gamma);

// only two specialized M=seq & dist have the member fct; thus, use a macro:
#define _RHEOLEF_get_bgd_geo(M) 						\
template <class T>								\
inline										\
const geo_basic<T,M>&								\
geo_basic<T,M>::get_background_geo() const 					\
{										\
    if (variant() == geo_abstract_base_rep<T>::geo_domain) {			\
      const geo_domain_rep<T,M>& dom 						\
       = dynamic_cast<const geo_domain_rep<T,M>&>(base::data());		\
      return dom.get_background_geo();						\
    }										\
    if (variant() == geo_abstract_base_rep<T>::geo_domain_indirect) { 		\
      const geo_domain_indirect_rep<T,M>& dom					\
       = dynamic_cast<const geo_domain_indirect_rep<T,M>&>(base::data());	\
      return dom.get_background_geo();						\
    }										\
    /* else classic geo: is its own background_geo */				\
    return *this;								\
}
#define _RHEOLEF_get_bgd_domain(M) 						\
template <class T>								\
inline										\
geo_basic<T,M>									\
geo_basic<T,M>::get_background_domain() const 					\
{										\
    if (variant() == geo_abstract_base_rep<T>::geo_domain) {			\
      const geo_domain_rep<T,M>& dom						\
       = dynamic_cast<const geo_domain_rep<T,M>&>(base::data()); 		\
      return dom.get_background_domain();					\
    }										\
    if (variant() == geo_abstract_base_rep<T>::geo_domain_indirect) { 		\
      return *this;								\
    }										\
    /* else classic geo: is its own background_geo */				\
    return *this;								\
}

_RHEOLEF_get_bgd_geo(sequential)
_RHEOLEF_get_bgd_domain(sequential)

#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_get_bgd_geo(distributed)
_RHEOLEF_get_bgd_domain(distributed)
#endif // _RHEOLEF_HAVE_MPI

#undef _RHEOLEF_get_bgd_geo
#undef _RHEOLEF_get_bgd_domain

} // namespace rheolef
#endif // _RHEOLEF_GEO_DOMAIN_H
