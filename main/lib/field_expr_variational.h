#ifndef _RHEOLEF_FIELD_EXPR_VARIATIONAL_H
#define _RHEOLEF_FIELD_EXPR_VARIATIONAL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// variational expressions are used for field assembly
// e.g. for right-hand-side of linear systems
//
// author: Pierre.Saramito@imag.fr
//
// date: 21 september 2015 
//
// Notes: use template expressions and SFINAE techniques
//
// SUMMARY:
// 1. unary function 
//    1.1. unary node
//    1.2. unary calls
// 2. binary operators +-  between two variational exprs
//    2.1. binary node
//    2.2. binary calls
// 3. binary operators */ between a variational and a nonlinear expr
//    3.1. helper
//    3.2. binary node
//    3.3. binary calls
//
#include "rheolef/field_expr_variational_terminal.h"

namespace rheolef {

// ---------------------------------------------------------------------------
// 1. unary function 
// ---------------------------------------------------------------------------
// 1.1. unary node
// ---------------------------------------------------------------------------
// ex: -v, 2*v, v/3
namespace details {

template <class UnaryFunction, class Expr>
class field_expr_v2_variational_unary {
public:
// typedefs:

  typedef geo_element::size_type                        size_type;
  typedef typename Expr::memory_type                    memory_type;
  typedef typename details::generic_unary_traits<UnaryFunction>::template result_hint<typename Expr::value_type>::type
                                                        value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<scalar_type>::type 	float_type;
  typedef space_basic<scalar_type,memory_type>		space_type;
  typedef typename Expr::vf_tag_type                    vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef field_expr_v2_variational_unary<UnaryFunction,Expr>                      self_type;
  typedef field_expr_v2_variational_unary<UnaryFunction,typename Expr::dual_self_type>  
                                                        dual_self_type;

// alocators:

  field_expr_v2_variational_unary (const UnaryFunction& f, const Expr& expr) 
    : _f(f), _expr(expr) {}

  field_expr_v2_variational_unary (const field_expr_v2_variational_unary<UnaryFunction,Expr>& x)
    : _f(x._f), _expr(x._expr) {}

// accessors:

  static bool have_test_space() { return true; } // check !
  const space_type&  get_vf_space()  const { return _expr.get_vf_space(); }
  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;
  space_constant::valued_type valued_tag() const {
    return details::generic_unary_traits<UnaryFunction>::valued_tag(_expr.valued_tag());
  }
  size_type n_derivative() const { return _expr.n_derivative(); }

// mutable modifiers:

  void initialize (const piola_on_pointset<float_type>& pops, const integrate_option& iopt) { 
    _expr.initialize (pops, iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const piola_on_pointset<float_type>& pops, const integrate_option& iopt) {  
    _expr.initialize (gh, pops, iopt);
  }
  // -------------
  // evaluate
  // -------------
  // evaluate when all arg types are determinated
  template<class Result, class Arg, class Status>
  struct evaluate_call_check {
    template<class M>
    void operator() (
      const self_type&                                                           obj,
      const geo_basic<float_type,M>&                                             omega_K,
      const geo_element& K, Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      fatal_macro ("invalid type resolution: Result="<<typename_macro(Result)
          << ", Arg="<<typename_macro(Arg)
          << ", UnaryFunction="<<typename_macro(UnaryFunction)
      );
    }
  };
  template<class Result, class Arg>
  struct evaluate_call_check<Result,Arg,std::true_type> {
    template<class M>
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      Eigen::Matrix<Arg,Eigen::Dynamic,Eigen::Dynamic> arg_value;
      obj._expr.evaluate (omega_K, K, arg_value);
      value = arg_value.unaryExpr (obj._f);
    }
    template<class M>
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      Eigen::Matrix<Arg,Eigen::Dynamic,Eigen::Dynamic> arg_value;
      obj._expr.evaluate_on_side (omega_K, K, sid, arg_value, do_local_component_assembly);
      value = arg_value.unaryExpr (obj._f);
    }
  };
  template<class Result, class Arg, class M>
  void evaluate_internal (
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    // check if Result is a valid return_type for this function:
    typedef typename scalar_traits<Result>::type S;
    typedef undeterminated_basic<S>                 undet;
    typedef typename details::generic_unary_traits<UnaryFunction>::template hint<Arg,undet>::result_type result_type;
    // TODO: instead of is_equal, could have compatible scalars T1,T2 ?
    typedef typename details::is_equal<Result,result_type>::type status_t;
    evaluate_call_check<Result,Arg,status_t> eval;
    eval (*this, omega_K, K, value);
  }
  template<class Result, class Arg, class M>
  void evaluate_internal (
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
  {
    // check if Result is a valid return_type for this function:
    typedef typename scalar_traits<Result>::type S;
    typedef undeterminated_basic<S>                 undet;
    typedef typename details::generic_unary_traits<UnaryFunction>::template hint<Arg,undet>::result_type result_type;
    // TODO: instead of is_equal, could have compatible scalars T1,T2 ?
    typedef typename details::is_equal<Result,result_type>::type status_t;
    evaluate_call_check<Result,Arg,status_t> eval;
    eval (*this, omega_K, K, sid, value, do_local_component_assembly);
  }
  // when Arg is defined at compile time:
  template<class This, class Result, class Arg, class Status>
  struct evaluate_switch {
    template<class M>
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      obj.template evaluate_internal<Result, Arg> (omega_K, K, value);
    }
    template<class M>
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      obj.template evaluate_internal<Result, Arg> (omega_K, K, sid, value, do_local_component_assembly);
    }
  };
  // when Arg is undeterminated at compile time
  template<class This, class Result, class Arg>
  struct evaluate_switch<This, Result, Arg, std::true_type> {
    template<class M>
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      typedef typename scalar_traits<Arg>::type T;
      space_constant::valued_type arg_valued_tag = obj._expr.valued_tag();
      switch (arg_valued_tag) {
#define _RHEOLEF_switch(VALUED,VALUE)								\
        case space_constant::VALUED:								\
	  obj.template evaluate_internal<Result, VALUE>(omega_K, K, value); break;
_RHEOLEF_switch(scalar,T)
_RHEOLEF_switch(vector,point_basic<T>)
_RHEOLEF_switch(tensor,tensor_basic<T>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T>)
_RHEOLEF_switch(tensor3,tensor3_basic<T>)
_RHEOLEF_switch(tensor4,tensor4_basic<T>)
#undef _RHEOLEF_switch
        default: error_macro ("unexpected argument valued tag="<<arg_valued_tag);
      }
    }
    template<class M>
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      typedef typename scalar_traits<Arg>::type T;
      space_constant::valued_type arg_valued_tag = obj._expr.valued_tag();
      switch (arg_valued_tag) {
#define _RHEOLEF_switch(VALUED,VALUE)								\
        case space_constant::VALUED:								\
	  obj.template evaluate_internal<Result, VALUE>(omega_K, K, sid,value, do_local_component_assembly); break;
_RHEOLEF_switch(scalar,T)
_RHEOLEF_switch(vector,point_basic<T>)
_RHEOLEF_switch(tensor,tensor_basic<T>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T>)
_RHEOLEF_switch(tensor3,tensor3_basic<T>)
_RHEOLEF_switch(tensor4,tensor4_basic<T>)
#undef _RHEOLEF_switch
        default: error_macro ("unexpected argument valued tag="<<arg_valued_tag);
      }
    }
  };
  template<class Result, class M>
  void evaluate (
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    typedef typename details::generic_unary_traits<UnaryFunction>::template hint<typename Expr::value_type,Result>::argument_type
                     A1;
    typedef typename is_undeterminated<A1>::type status_t;
    evaluate_switch <self_type, Result, A1, status_t> eval;
    eval (*this, omega_K, K, value);
  }
  template<class Result, class M>
  void evaluate_on_side (
      const geo_basic<float_type,M>&                       omega_K,
      const geo_element&                                   K,
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
  {
    typedef typename details::generic_unary_traits<UnaryFunction>::template hint<typename Expr::value_type,Result>::argument_type
                     A1;
    typedef typename is_undeterminated<A1>::type status_t;
    evaluate_switch <self_type, Result, A1, status_t> eval;
    eval (*this, omega_K, K, sid, value, do_local_component_assembly);
  }
  template<class Result>
  void valued_check() const {
    typedef typename details::generic_unary_traits<UnaryFunction>::template hint<typename Expr::value_type,Result>::argument_type
                     A1;
    if (! is_undeterminated<A1>::value) _expr.template valued_check<A1>();
  }
protected:
// data:
  UnaryFunction  _f;
  Expr           _expr;
};
template<class F, class Expr> struct is_field_expr_v2_variational_arg    <field_expr_v2_variational_unary<F,Expr> > : std::true_type {};

} // namespace details

// ---------------------------------------------------------------------------
// 1.2. unary calls
// ---------------------------------------------------------------------------

#define _RHEOLEF_make_field_expr_v2_variational_unary_operator(FUNCTION,FUNCTOR)	\
template<class Expr>								\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_v2_variational_arg<Expr>::value			\
 ,details::field_expr_v2_variational_unary<					\
    FUNCTOR									\
   ,Expr									\
  >										\
>::type										\
FUNCTION (const Expr& expr)							\
{										\
  return details::field_expr_v2_variational_unary <FUNCTOR,Expr> (FUNCTOR(), expr); \
}

_RHEOLEF_make_field_expr_v2_variational_unary_operator (operator+, details::unary_plus)
_RHEOLEF_make_field_expr_v2_variational_unary_operator (operator-, details::negate)
_RHEOLEF_make_field_expr_v2_variational_unary_operator (tr,        details::tr_)
_RHEOLEF_make_field_expr_v2_variational_unary_operator (trans,     details::trans_)
#undef _RHEOLEF_make_field_expr_v2_variational_unary_operator

// ---------------------------------------------------------------------------
// 2. binary operators +-  between variationals
// ---------------------------------------------------------------------------
// 2.1. binary node
// ---------------------------------------------------------------------------
// ex: v+v, v-v

namespace details {

template<class BinaryFunction, class Expr1, class Expr2>
class field_expr_v2_variational_binary {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename promote_memory<typename Expr1::memory_type,typename Expr2::memory_type>::type 
 				                   	memory_type;
  typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<
          typename Expr1::value_type
         ,typename Expr2::value_type>::type             result_hint;
  typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<scalar_type,memory_type>		space_type; // TODO: deduce from Exprs
  typedef typename details::bf_vf_tag<BinaryFunction,
	typename Expr1::vf_tag_type,
	typename Expr2::vf_tag_type>::type              vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef field_expr_v2_variational_binary <BinaryFunction,Expr1,Expr2>        self_type;
  typedef field_expr_v2_variational_binary <BinaryFunction,typename Expr1::dual_self_type,typename Expr2::dual_self_type>  
                                                        dual_self_type;

// alocators:

  field_expr_v2_variational_binary (const BinaryFunction& f, 
		    const Expr1&    expr1,
                    const Expr2&    expr2)
    : _f(f), _expr1(expr1), _expr2(expr2) {}

// accessors:

  static bool have_test_space() { return true; }
  const space_type&  get_vf_space()  const { return _expr1.get_vf_space(); }
  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;
  space_constant::valued_type valued_tag() const {
    return details::generic_binary_traits<BinaryFunction>::valued_tag(_expr1.valued_tag(), _expr2.valued_tag());
  }
  size_type n_derivative() const { return std::min(_expr1.n_derivative(), _expr2.n_derivative()); }

// mutable modifiers:

  // TODO: check that expr1 & expr2 have the same get_vf_space()
  void initialize (const piola_on_pointset<float_type>& pops, const integrate_option& iopt) { 
    _expr1.initialize (pops, iopt);
    _expr2.initialize (pops, iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const piola_on_pointset<float_type>& pops, const integrate_option& iopt) {  
    _expr1.initialize (gh, pops, iopt);
    _expr2.initialize (gh, pops, iopt);
  }
  // evaluate when all arg types are determinated
  template<class Result, class Arg1, class Arg2, class Status>
  struct evaluate_call_check {
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      fatal_macro ("invalid type resolution");
    }
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      fatal_macro ("invalid type resolution");
    }
  };
  template<class Result, class Arg1, class Arg2>
  struct evaluate_call_check<Result,Arg1,Arg2,std::true_type> {
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      Eigen::Matrix<Arg1,Eigen::Dynamic,Eigen::Dynamic> value1; obj._expr1.evaluate (omega_K, K, value1);
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> value2; obj._expr2.evaluate (omega_K, K, value2);
      // TODO: DVT_EIGEN_BLAS2
      value.resize (value1.rows(), value1.cols());
      for (size_type loc_inod = 0, loc_nnod = value1.rows(); loc_inod < loc_nnod; ++loc_inod) {
      for (size_type loc_jdof = 0, loc_ndof = value1.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
        value(loc_inod,loc_jdof) = obj._f (value1(loc_inod,loc_jdof), value2(loc_inod,loc_jdof));
      }}
    }
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      Eigen::Matrix<Arg1,Eigen::Dynamic,Eigen::Dynamic> value1; obj._expr1.evaluate_on_side (omega_K, K, sid, value1, do_local_component_assembly);
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> value2; obj._expr2.evaluate_on_side (omega_K, K, sid, value2, do_local_component_assembly);
      // TODO: DVT_EIGEN_BLAS2
      value.resize (value1.rows(), value1.cols());
      for (size_type loc_inod = 0, loc_nnod = value1.rows(); loc_inod < loc_nnod; ++loc_inod) {
      for (size_type loc_jdof = 0, loc_ndof = value1.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
        value(loc_inod,loc_jdof) = obj._f (value1(loc_inod,loc_jdof), value2(loc_inod,loc_jdof));
      }}
    }
  };
  template<class Result, class Arg1, class Arg2>
  void evaluate_internal (
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<Arg1,Arg2>::type result_type;
    // TODO: instead of is_equal, could have compatible scalars T1,T2 ?
    typedef typename details::is_equal<Result,result_type>::type status_t;
    evaluate_call_check<Result,Arg1,Arg2,status_t> eval;
    eval (*this, omega_K, K, value);
  }
  template<class Result, class Arg1, class Arg2>
  void evaluate_on_side_internal (
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
  {
    typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<Arg1,Arg2>::type result_type;
    // TODO: instead of is_equal, could have compatible scalars T1,T2 ?
    typedef typename details::is_equal<Result,result_type>::type status_t;
    evaluate_call_check<Result,Arg1,Arg2,status_t> eval;
    eval (*this, omega_K, K, sid, value, do_local_component_assembly);
  }
  template<class This, class Result, class Arg1, class Arg2, class Undet1, class Undet2>
  struct evaluate_switch {};
  // --------------------------------------------------------------------------
  // when both args are defined at compile time:
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::false_type, std::false_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      obj.template evaluate_internal<Result, Arg1, Arg2> (omega_K, K, value);
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      obj.template evaluate_on_side_internal<Result, Arg1, Arg2> (omega_K, K, sid, value, do_local_component_assembly);
    }
  };
  // --------------------------------------------------------------------------
  // when first arg is undeterminated
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::true_type, std::false_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      space_constant::valued_type arg1_valued_tag = obj._expr1.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch1(VALUED1,VALUE1)							\
        case space_constant::VALUED1:								\
	  obj.template evaluate_internal<Result, VALUE1, Arg2>(omega_K, K, value); break;
_RHEOLEF_switch1(scalar,T1)
_RHEOLEF_switch1(vector,point_basic<T1>)
_RHEOLEF_switch1(tensor,tensor_basic<T1>)
_RHEOLEF_switch1(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch1(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch1(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch1 
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      space_constant::valued_type arg1_valued_tag = obj._expr1.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch1(VALUED1,VALUE1)							\
        case space_constant::VALUED1:								\
	  obj.template evaluate_on_side_internal<Result, VALUE1, Arg2>(omega_K, K, sid, value, do_local_component_assembly); break;
_RHEOLEF_switch1(scalar,T1)
_RHEOLEF_switch1(vector,point_basic<T1>)
_RHEOLEF_switch1(tensor,tensor_basic<T1>)
_RHEOLEF_switch1(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch1(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch1(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch1 
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
  };
  // --------------------------------------------------------------------------
  // when second arg is undeterminated
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::false_type, std::true_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg2_valued_tag = obj._expr2.valued_tag();
      switch (arg2_valued_tag) {
#define _RHEOLEF_switch2(VALUED2,VALUE2)							\
        case space_constant::VALUED2:								\
	  obj.template evaluate_internal<Result, Arg1, VALUE2>(omega_K, K, value); break;
_RHEOLEF_switch2(scalar,T2)
_RHEOLEF_switch2(vector,point_basic<T2>)
_RHEOLEF_switch2(tensor,tensor_basic<T2>)
_RHEOLEF_switch2(unsymmetric_tensor,tensor_basic<T2>)
_RHEOLEF_switch2(tensor3,tensor3_basic<T2>)
_RHEOLEF_switch2(tensor4,tensor4_basic<T2>)
#undef _RHEOLEF_switch2
        default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);
      }
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg2_valued_tag = obj._expr2.valued_tag();
      switch (arg2_valued_tag) {
#define _RHEOLEF_switch2(VALUED2,VALUE2)							\
        case space_constant::VALUED2:								\
	  obj.template evaluate_on_side_internal<Result, Arg1, VALUE2>(omega_K, K, sid, value, do_local_component_assembly); break;
_RHEOLEF_switch2(scalar,T2)
_RHEOLEF_switch2(vector,point_basic<T2>)
_RHEOLEF_switch2(tensor,tensor_basic<T2>)
_RHEOLEF_switch2(unsymmetric_tensor,tensor_basic<T2>)
_RHEOLEF_switch2(tensor3,tensor3_basic<T2>)
_RHEOLEF_switch2(tensor4,tensor4_basic<T2>)
#undef _RHEOLEF_switch2
        default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);
      }
    }
  };
  // --------------------------------------------------------------------------
  // when both are undefined at compile time:
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::true_type, std::true_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg1_valued_tag = obj._expr1.valued_tag();
      space_constant::valued_type arg2_valued_tag = obj._expr2.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch2(VALUE1,VALUED2,VALUE2)							\
            case space_constant::VALUED2:							\
	      obj.template evaluate_internal<Result, VALUE1, VALUE2>(omega_K, K, value); break;
#define _RHEOLEF_switch1(VALUED1,VALUE1)							\
        case space_constant::VALUED1: {								\
          switch (arg2_valued_tag) {								\
_RHEOLEF_switch2(VALUE1,scalar,T2)								\
_RHEOLEF_switch2(VALUE1,vector,point_basic<T2>)							\
_RHEOLEF_switch2(VALUE1,tensor,tensor_basic<T2>)						\
_RHEOLEF_switch2(VALUE1,unsymmetric_tensor,tensor_basic<T2>)					\
_RHEOLEF_switch2(VALUE1,tensor3,tensor3_basic<T2>)						\
_RHEOLEF_switch2(VALUE1,tensor4,tensor4_basic<T2>)						\
            default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);	\
          }											\
          break;										\
        }
_RHEOLEF_switch1(scalar,T1)
_RHEOLEF_switch1(vector,point_basic<T1>)
_RHEOLEF_switch1(tensor,tensor_basic<T1>)
_RHEOLEF_switch1(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch1(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch1(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch2
#undef _RHEOLEF_switch1
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K,
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg1_valued_tag = obj._expr1.valued_tag();
      space_constant::valued_type arg2_valued_tag = obj._expr2.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch2(VALUE1,VALUED2,VALUE2)							\
            case space_constant::VALUED2:							\
	      obj.template evaluate_on_side_internal<Result, VALUE1, VALUE2>(omega_K, K, sid, value, do_local_component_assembly); break;
#define _RHEOLEF_switch1(VALUED1,VALUE1)							\
        case space_constant::VALUED1: {								\
          switch (arg2_valued_tag) {								\
_RHEOLEF_switch2(VALUE1,scalar,T2)								\
_RHEOLEF_switch2(VALUE1,vector,point_basic<T2>)							\
_RHEOLEF_switch2(VALUE1,tensor,tensor_basic<T2>)						\
_RHEOLEF_switch2(VALUE1,unsymmetric_tensor,tensor_basic<T2>)					\
_RHEOLEF_switch2(VALUE1,tensor3,tensor3_basic<T2>)						\
_RHEOLEF_switch2(VALUE1,tensor4,tensor4_basic<T2>)						\
            default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);	\
          }											\
          break;										\
        }
_RHEOLEF_switch1(scalar,T1)
_RHEOLEF_switch1(vector,point_basic<T1>)
_RHEOLEF_switch1(tensor,tensor_basic<T1>)
_RHEOLEF_switch1(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch1(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch1(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch2
#undef _RHEOLEF_switch1
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
  };
  // --------------------------------------------------------------------------
  // main eval call:
  // --------------------------------------------------------------------------
  template<class Result>
  struct hint {
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,Result>::first_argument_type   A1;
    typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename Expr1::value_type
	 ,typename Expr2::value_type
	 ,Result>::second_argument_type A2;
  };
  template<class Result>
  void evaluate (
    const geo_basic<float_type,memory_type>&             omega_K,
    const geo_element&                                   K, 
    Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    typedef typename hint<Result>::A1 A1;
    typedef typename hint<Result>::A2 A2;
    typedef typename is_undeterminated<A1>::type undet_1;
    typedef typename is_undeterminated<A2>::type undet_2;
    evaluate_switch <self_type, Result, A1, A2, undet_1, undet_2> eval;
    eval (*this, omega_K, K, value);
  }
  template<class Result>
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&             omega_K,
    const geo_element&                                   K, 
    const side_information_type&                         sid,
    Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
    bool                                                 do_local_component_assembly) const
  {
    typedef typename hint<Result>::A1 A1;
    typedef typename hint<Result>::A2 A2;
    typedef typename is_undeterminated<A1>::type undet_1;
    typedef typename is_undeterminated<A2>::type undet_2;
    evaluate_switch <self_type, Result, A1, A2, undet_1, undet_2> eval;
    eval (*this, omega_K, K, sid, value, do_local_component_assembly);
  }
  template<class Result>
  void valued_check() const {
    typedef typename hint<Result>::A1 A1;
    typedef typename hint<Result>::A2 A2;
    if (! is_undeterminated<A1>::value) _expr1.template valued_check<A1>();
    if (! is_undeterminated<A2>::value) _expr2.template valued_check<A2>();
  }
protected:
// data:
  BinaryFunction  _f;
  Expr1           _expr1;
  Expr2           _expr2;
};
template <class F, class Expr1, class Expr2> struct is_field_expr_v2_variational_arg    <field_expr_v2_variational_binary<F,Expr1,Expr2> > : std::true_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 2.2. binary call
// ---------------------------------------------------------------------------
namespace details {
  
template<class Expr1, class Expr2, class Sfinae = void>
struct is_field_expr_v2_variational_binary_plus_minus : std::false_type {};

template<class Expr1, class Expr2>
struct is_field_expr_v2_variational_binary_plus_minus <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
       is_field_expr_v2_variational_arg<Expr1>::value
    && is_field_expr_v2_variational_arg<Expr2>::value
  >::type
>
: and_type<
    is_field_expr_v2_variational_arg<Expr1>
   ,is_field_expr_v2_variational_arg<Expr2>
   ,std::is_same <
      typename Expr1::vf_tag_type
     ,typename Expr2::vf_tag_type
    >
  >
{};

} // namespace details

#define _RHEOLEF_make_field_expr_v2_variational_binary_operator_plus_minus(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_v2_variational_binary_plus_minus <Expr1,Expr2>::value	\
 ,details::field_expr_v2_variational_binary<					\
    FUNCTOR									\
   ,Expr1									\
   ,Expr2									\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::field_expr_v2_variational_binary <FUNCTOR, Expr1, Expr2> (FUNCTOR(), expr1, expr2); \
}

_RHEOLEF_make_field_expr_v2_variational_binary_operator_plus_minus (operator+, details::plus)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_plus_minus (operator-, details::minus)
#undef _RHEOLEF_make_field_expr_v2_variational_binary_operator_plus_minus

// ---------------------------------------------------------------------------
// 3. binary operators */ between a variational and a nonlinear expr
// ---------------------------------------------------------------------------
// 3.1. helper
// ---------------------------------------------------------------------------
namespace details {

  template<class This, class Arg1>
  struct nl_switch {
    typedef typename This::size_type size_type;
    void element_initialize (const This& obj, const geo_element& K) const {
      space_constant::valued_type nl_arg_valued_tag = obj._nl_expr.valued_tag();
      switch (nl_arg_valued_tag) {
        case space_constant::scalar:
          obj._nl_expr.evaluate (K, obj._scalar_nl_value_quad); break;
        case space_constant::vector:
          obj._nl_expr.evaluate (K, obj._vector_nl_value_quad); break;
        case space_constant::tensor:
        case space_constant::unsymmetric_tensor:
          obj._nl_expr.evaluate (K, obj._tensor_nl_value_quad); break;
        case space_constant::tensor3:
          obj._nl_expr.evaluate (K, obj._tensor3_nl_value_quad); break;
        case space_constant::tensor4:
          obj._nl_expr.evaluate (K, obj._tensor4_nl_value_quad); break;
        default: error_macro ("unexpected first argument valued tag="<<nl_arg_valued_tag); // ICI
      }
    }
    void element_initialize_on_side (const This& obj, const geo_element& K, const side_information_type& sid) const {
      space_constant::valued_type nl_arg_valued_tag = obj._nl_expr.valued_tag();
      switch (nl_arg_valued_tag) {
        case space_constant::scalar:
          obj._nl_expr.evaluate_on_side (K, sid, obj._scalar_nl_value_quad); break;
        case space_constant::vector:
          obj._nl_expr.evaluate_on_side (K, sid, obj._vector_nl_value_quad); break;
        case space_constant::tensor:
        case space_constant::unsymmetric_tensor:
          obj._nl_expr.evaluate_on_side (K, sid, obj._tensor_nl_value_quad); break;
        case space_constant::tensor3:
          obj._nl_expr.evaluate_on_side (K, sid, obj._tensor3_nl_value_quad); break;
        case space_constant::tensor4:
          obj._nl_expr.evaluate_on_side (K, sid, obj._tensor4_nl_value_quad); break;
        default: error_macro ("unexpected first argument valued tag="<<nl_arg_valued_tag);
      }
    }
    Arg1 get_nl_value (const This& obj, size_type q) const {
      // Arg1 may be solved at compile time for real evaluation
      fatal_macro ("unexpected argument type="<<typename_macro(Arg1));
      return Arg1();
    }
  };
  template<class This> struct nl_switch<This,typename This::scalar_type> {
    typedef typename This::size_type size_type;
    typedef typename This::scalar_type scalar_type;
    void element_initialize (const This& obj, const geo_element& K) const {
      obj._nl_expr.evaluate (K, obj._scalar_nl_value_quad); }
    void element_initialize_on_side (const This& obj, const geo_element& K, const side_information_type& sid) const {
      obj._nl_expr.evaluate_on_side (K, sid, obj._scalar_nl_value_quad); }
    const scalar_type& get_nl_value (const This& obj, size_type q) const {
      return obj._scalar_nl_value_quad[q]; }
  };
  template<class This> struct nl_switch<This,point_basic<typename This::scalar_type> > {
    typedef typename This::size_type size_type;
    typedef typename This::scalar_type scalar_type;
    void element_initialize (const This& obj, const geo_element& K) const {
      obj._nl_expr.evaluate (K, obj._vector_nl_value_quad); }
    void element_initialize_on_side (const This& obj, const geo_element& K, const side_information_type& sid) const {
      obj._nl_expr.evaluate_on_side (K, sid, obj._vector_nl_value_quad); }
    const point_basic<scalar_type>& get_nl_value (const This& obj, size_type q) const {
      return obj._vector_nl_value_quad[q]; }
  };
  template<class This> struct nl_switch<This,tensor_basic<typename This::scalar_type> > {
    typedef typename This::size_type size_type;
    typedef typename This::scalar_type scalar_type;
    void element_initialize (const This& obj, const geo_element& K) const {
      obj._nl_expr.evaluate (K, obj._tensor_nl_value_quad); }
    void element_initialize_on_side (const This& obj, const geo_element& K, const side_information_type& sid) const {
      obj._nl_expr.evaluate_on_side (K, sid, obj._tensor_nl_value_quad); }
    const tensor_basic<scalar_type>& get_nl_value (const This& obj, size_type q) const {
      return obj._tensor_nl_value_quad[q]; }
  };
  template<class This> struct nl_switch<This,tensor3_basic<typename This::scalar_type> > {
    typedef typename This::size_type size_type;
    typedef typename This::scalar_type scalar_type;
    void element_initialize (const This& obj, const geo_element& K) const {
      obj._nl_expr.evaluate (K, obj._tensor3_nl_value_quad); }
    void element_initialize_on_side (const This& obj, const geo_element& K, const side_information_type& sid) const {
      obj._nl_expr.evaluate_on_side (K, sid, obj._tensor3_nl_value_quad); }
    const tensor3_basic<scalar_type>& get_nl_value (const This& obj, size_type q) const {
      return obj._tensor3_nl_value_quad[q]; }
  };
  template<class This> struct nl_switch<This,tensor4_basic<typename This::scalar_type> > {
    typedef typename This::size_type size_type;
    typedef typename This::scalar_type scalar_type;
    void element_initialize (const This& obj, const geo_element& K) const {
      obj._nl_expr.evaluate (K, obj._tensor4_nl_value_quad); }
    void element_initialize_on_side (const This& obj, const geo_element& K, const side_information_type& sid) const {
      obj._nl_expr.evaluate_on_side (K, sid, obj._tensor4_nl_value_quad); }
    const tensor4_basic<scalar_type>& get_nl_value (const This& obj, size_type q) const {
      return obj._tensor4_nl_value_quad[q]; }
  };

} // namespace details

// ---------------------------------------------------------------------------
// 3.2. binary node
// ---------------------------------------------------------------------------
// 
// function call: (f nl_expr vf_expr)
// examples: f = operator*, operator/
//   eta_h*v
//   v/eta_h
//   dot(v,normal())
// at any quadrature node xq, the compuation eta_h(xq) is performed 
// and then we loop on the basis functions for v :
//     eta_q = eta_h(xq);
//     for i=0..nk-1
//       value[i] = f (eta_q, v(xq)[i]);
// since we can swap the two args (see the details::swap_fun<f> class),
// we assume that the first argument is a field or a general field_nl_expr
//       and that the second argument is a test of a general field_vf_expr
//
// Implementation note: this operation do not reduces to field_expr_v2_variational_unary
// with a class-function that contains eta_h since :
//  - the value of eta_h may be refreshed at each xq 
//    (this could be achieved by replacing std::binder1st with an adequate extension)
//  - the valued category of eta_h is not always known at compile-time.
//    It is known in dot(eta_h,v) but not with eta_h*v
//    and the class-functions for field_expr_v2_variational_unary may have Arg1 and Result determined.
// So we switch to a specific field_expr_v2_variational_binary_binded that is abble to solve the
// valued type at run time. When it is possible, it is determined at compile-time.
// 
namespace details {

template<class BinaryFunction, class NLExpr, class VFExpr>
class field_expr_v2_variational_binary_binded {
public:
// typedefs:

  typedef geo_element::size_type                   	size_type;
  typedef typename promote_memory<typename NLExpr::memory_type,typename VFExpr::memory_type>::type 
 				                   	memory_type;
  typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<
          typename NLExpr::value_type
         ,typename VFExpr::value_type>::type             result_hint;
  typedef typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename NLExpr::value_type
	 ,typename VFExpr::value_type
	 ,result_hint>::result_type                     value_type;
  typedef typename scalar_traits<value_type>::type  	scalar_type;
  typedef typename  float_traits<value_type>::type 	float_type;
  typedef space_basic<scalar_type,memory_type>		space_type; // TODO: deduce from Exprs
  typedef typename VFExpr::vf_tag_type                  vf_tag_type;
  typedef typename details::dual_vf_tag<vf_tag_type>::type
                                                        vf_dual_tag_type;
  typedef field_expr_v2_variational_binary_binded<BinaryFunction,NLExpr,VFExpr> self_type;
  typedef field_expr_v2_variational_binary_binded<BinaryFunction,NLExpr,typename VFExpr::dual_self_type>  
                                                        dual_self_type;

// alocators:

  field_expr_v2_variational_binary_binded (const BinaryFunction& f, 
		    const NLExpr&    nl_expr,
                    const VFExpr&    vf_expr)
    : _f(f), 
      _nl_expr(nl_expr),
      _vf_expr(vf_expr)
    {}

  field_expr_v2_variational_binary_binded (const field_expr_v2_variational_binary_binded<BinaryFunction,NLExpr,VFExpr>& x)
    : _f(x._f), 
      _nl_expr(x._nl_expr),
      _vf_expr(x._vf_expr)
    {}

// accessors:

  static bool have_test_space() { return true; } // deduce & check !
  const space_type&  get_vf_space()  const { return _vf_expr.get_vf_space(); }
  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<value_type>::value;
  space_constant::valued_type valued_tag() const {
    return details::generic_binary_traits<BinaryFunction>::valued_tag(_nl_expr.valued_tag(), _vf_expr.valued_tag());
  }
  size_type n_derivative() const { return _vf_expr.n_derivative(); }

// mutable modifiers:

  void initialize (const piola_on_pointset<float_type>& pops, const integrate_option& iopt) { 
    _nl_expr.initialize (pops, iopt);
    _vf_expr.initialize (pops, iopt);
  }
  void initialize (const band_basic<float_type,memory_type>& gh, const piola_on_pointset<float_type>& pops, const integrate_option& iopt) {  
    _nl_expr.initialize (    pops, iopt);
    _vf_expr.initialize (gh, pops, iopt);
  }
  // ---------------------------------------------
  // evaluate
  // ---------------------------------------------
  // evaluate when all arg types are determinated
  template<class Result, class Arg1, class Arg2, class Status>
  struct evaluate_call_check {
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      fatal_macro ("invalid type resolution");
    }
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      fatal_macro ("invalid type resolution");
    }
  };
  template<class Result, class Arg1, class Arg2>
  struct evaluate_call_check<Result,Arg1,Arg2,std::true_type> {
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      Eigen::Matrix<Arg1,Eigen::Dynamic,1>              value1; obj._nl_expr.evaluate (omega_K, K, value1);
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> value2; obj._vf_expr.evaluate (omega_K, K, value2);
      check_macro (value1.size() == value2.rows(), "invalid sizes value1(nnod="<<value1.size()
	<<") and value2(nnod="<<value2.rows()<<",ndof="<<value2.cols()<<")");
      value.resize (value2.rows(), value2.cols());
      for (size_type loc_inod = 0, loc_nnod = value2.rows(); loc_inod < loc_nnod; ++loc_inod) {
      for (size_type loc_jdof = 0, loc_ndof = value2.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
        value(loc_inod,loc_jdof) = obj._f (value1[loc_inod], value2(loc_inod,loc_jdof)); // TODO: DVT_EIGEN_BLAS2
      }}
    }
    void operator() (
      const self_type&                                     obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      Eigen::Matrix<Arg1,Eigen::Dynamic,1>              value1; obj._nl_expr.evaluate_on_side (omega_K, K, sid, value1);
      Eigen::Matrix<Arg2,Eigen::Dynamic,Eigen::Dynamic> value2; obj._vf_expr.evaluate_on_side (omega_K, K, sid, value2, do_local_component_assembly);
      check_macro (value1.size() == value2.rows(), "invalid sizes value1(nnod="<<value1.size()
	<<") and value2(nnod="<<value2.rows()<<",ndof="<<value2.cols()<<")");
      value.resize (value2.rows(), value2.cols());
      for (size_type loc_inod = 0, loc_nnod = value2.rows(); loc_inod < loc_nnod; ++loc_inod) {
      for (size_type loc_jdof = 0, loc_ndof = value2.cols(); loc_jdof < loc_ndof; ++loc_jdof) {
        value(loc_inod,loc_jdof) = obj._f (value1[loc_inod], value2(loc_inod,loc_jdof)); // TODO: DVT_EIGEN_BLAS2
      }}
    }
  };
  template<class Result, class Arg1, class Arg2>
  void evaluate_internal (
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<Arg1,Arg2>::type result_type;
    // TODO: instead of is_equal, could have compatible scalars T1,T2 ?
    typedef typename details::is_equal<Result,result_type>::type status_t;
    evaluate_call_check<Result,Arg1,Arg2,status_t> eval;
    eval (*this, omega_K, K, value);
  }
  template<class Result, class Arg1, class Arg2>
  void evaluate_on_side_internal (
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
  {
    typedef typename details::generic_binary_traits<BinaryFunction>::template result_hint<Arg1,Arg2>::type result_type;
    // TODO: instead of is_equal, could have compatible scalars T1,T2 ?
    typedef typename details::is_equal<Result,result_type>::type status_t;
    evaluate_call_check<Result,Arg1,Arg2,status_t> eval;
    eval (*this, omega_K, K, sid, value, do_local_component_assembly);
  }
  template<class This, class Result, class Arg1, class Arg2, class Undet1, class Undet2>
  struct evaluate_switch {};
  // --------------------------------------------------------------------------
  // when both args are defined at compile time:
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::false_type, std::false_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      obj.template evaluate_internal<Result, Arg1, Arg2> (omega_K, K, value);
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      obj.template evaluate_on_side_internal<Result, Arg1, Arg2> (omega_K, K, sid, value, do_local_component_assembly);
    }
  };
  // --------------------------------------------------------------------------
  // when first arg is undeterminated
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::true_type, std::false_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      space_constant::valued_type arg1_valued_tag = obj._nl_expr.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch(A1_VALUED,A1_VALUE) 							\
        case space_constant::A1_VALUED:								\
	  obj.template evaluate_internal<Result, A1_VALUE, Arg2> (omega_K, K, value); break;
_RHEOLEF_switch(scalar,T1)
_RHEOLEF_switch(vector,point_basic<T1>)
_RHEOLEF_switch(tensor,tensor_basic<T1>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      space_constant::valued_type arg1_valued_tag = obj._nl_expr.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch(A1_VALUED,A1_VALUE) 							\
        case space_constant::A1_VALUED:								\
	  obj.template evaluate_on_side_internal<Result, A1_VALUE, Arg2> (omega_K, K, sid, value, do_local_component_assembly); break;
_RHEOLEF_switch(scalar,T1)
_RHEOLEF_switch(vector,point_basic<T1>)
_RHEOLEF_switch(tensor,tensor_basic<T1>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
  };
  // --------------------------------------------------------------------------
  // when second arg is undeterminated
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::false_type, std::true_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg2_valued_tag = obj._vf_expr.valued_tag();
      switch (arg2_valued_tag) {
#define _RHEOLEF_switch(A2_VALUED,A2_VALUE) 							\
        case space_constant::A2_VALUED:								\
	  obj.template evaluate_internal<Result, Arg1, A2_VALUE> (omega_K, K, value); break;
_RHEOLEF_switch(scalar,T2)
_RHEOLEF_switch(vector,point_basic<T2>)
_RHEOLEF_switch(tensor,tensor_basic<T2>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T2>)
_RHEOLEF_switch(tensor3,tensor3_basic<T2>)
_RHEOLEF_switch(tensor4,tensor4_basic<T2>)
#undef _RHEOLEF_switch
        default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);
      }
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg2_valued_tag = obj._vf_expr.valued_tag();
      switch (arg2_valued_tag) {
#define _RHEOLEF_switch(A2_VALUED,A2_VALUE) 							\
        case space_constant::A2_VALUED:								\
	  obj.template evaluate_on_side_internal<Result, Arg1, A2_VALUE> (omega_K, K, sid, value, do_local_component_assembly); break;
_RHEOLEF_switch(scalar,T2)
_RHEOLEF_switch(vector,point_basic<T2>)
_RHEOLEF_switch(tensor,tensor_basic<T2>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T2>)
_RHEOLEF_switch(tensor3,tensor3_basic<T2>)
_RHEOLEF_switch(tensor4,tensor4_basic<T2>)
#undef _RHEOLEF_switch
        default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);
      }
    }
  };
  // --------------------------------------------------------------------------
  // when both are undefined at compile time:
  // --------------------------------------------------------------------------
  template<class This, class Result, class Arg1, class Arg2>
  struct evaluate_switch<This, Result, Arg1, Arg2, std::true_type, std::true_type> {
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg1_valued_tag = obj._nl_expr.valued_tag();
      space_constant::valued_type arg2_valued_tag = obj._vf_expr.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch_A2(A1_VALUE,A2_VALUED,A2_VALUE) 					\
            case space_constant::A2_VALUED:							\
	      obj.template evaluate_internal<Result, A1_VALUE, A2_VALUE> (omega_K, K, value); break;

#define _RHEOLEF_switch(A1_VALUED,A1_VALUE) 							\
        case space_constant::A1_VALUED: {							\
          switch (arg2_valued_tag) {								\
_RHEOLEF_switch_A2(A1_VALUE,scalar,T2)								\
_RHEOLEF_switch_A2(A1_VALUE,vector,point_basic<T2>)						\
_RHEOLEF_switch_A2(A1_VALUE,tensor,tensor_basic<T2>)						\
_RHEOLEF_switch_A2(A1_VALUE,unsymmetric_tensor,tensor_basic<T2>)				\
_RHEOLEF_switch_A2(A1_VALUE,tensor3,tensor3_basic<T2>)						\
_RHEOLEF_switch_A2(A1_VALUE,tensor4,tensor4_basic<T2>)						\
            default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);	\
          }											\
          break;										\
        }
_RHEOLEF_switch(scalar,T1)
_RHEOLEF_switch(vector,point_basic<T1>)
_RHEOLEF_switch(tensor,tensor_basic<T1>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch
#undef _RHEOLEF_switch_A2
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
    void operator() (
      const This&                                          obj,
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
    {
      typedef typename scalar_traits<Arg1>::type T1;
      typedef typename scalar_traits<Arg2>::type T2;
      space_constant::valued_type arg1_valued_tag = obj._nl_expr.valued_tag();
      space_constant::valued_type arg2_valued_tag = obj._vf_expr.valued_tag();
      switch (arg1_valued_tag) {
#define _RHEOLEF_switch_A2(A1_VALUE,A2_VALUED,A2_VALUE) 					\
            case space_constant::A2_VALUED:							\
	      obj.template evaluate_on_side_internal<Result, A1_VALUE, A2_VALUE> (omega_K, K, sid, value, do_local_component_assembly); break;

#define _RHEOLEF_switch(A1_VALUED,A1_VALUE) 							\
        case space_constant::A1_VALUED: {							\
          switch (arg2_valued_tag) {								\
_RHEOLEF_switch_A2(A1_VALUE,scalar,T2)								\
_RHEOLEF_switch_A2(A1_VALUE,vector,point_basic<T2>)						\
_RHEOLEF_switch_A2(A1_VALUE,tensor,tensor_basic<T2>)						\
_RHEOLEF_switch_A2(A1_VALUE,unsymmetric_tensor,tensor_basic<T2>)				\
_RHEOLEF_switch_A2(A1_VALUE,tensor3,tensor3_basic<T2>)						\
_RHEOLEF_switch_A2(A1_VALUE,tensor4,tensor4_basic<T2>)						\
            default: error_macro ("unexpected second argument valued tag="<<arg2_valued_tag);	\
          }											\
          break;										\
        }
_RHEOLEF_switch(scalar,T1)
_RHEOLEF_switch(vector,point_basic<T1>)
_RHEOLEF_switch(tensor,tensor_basic<T1>)
_RHEOLEF_switch(unsymmetric_tensor,tensor_basic<T1>)
_RHEOLEF_switch(tensor3,tensor3_basic<T1>)
_RHEOLEF_switch(tensor4,tensor4_basic<T1>)
#undef _RHEOLEF_switch
#undef _RHEOLEF_switch_A2
        default: error_macro ("unexpected first argument valued tag="<<arg1_valued_tag);
      }
    }
  };
  // --------------------------------------------------------------------------
  // hint fort argument's types when Result is known
  // --------------------------------------------------------------------------
  template<class Result>
  struct hint {
    typedef typename promote<
       typename NLExpr::value_type
      ,typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename NLExpr::value_type
	 ,typename VFExpr::value_type
	 ,Result>::first_argument_type
      >::type  A1;
    typedef typename promote<
        typename VFExpr::value_type
       ,typename details::generic_binary_traits<BinaryFunction>::template hint<
	  typename NLExpr::value_type
	 ,typename VFExpr::value_type
	 ,Result>::second_argument_type
       >::type A2;
  };
  // --------------------------------------------------------------------------
  // evaluate : main call with possibly runtime type resolutiion
  // --------------------------------------------------------------------------
  template<class Result>
  void evaluate (
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    typedef typename hint<Result>::A1 A1;
    typedef typename hint<Result>::A2 A2;
    static const space_constant::valued_type  first_argument_tag = space_constant::valued_tag_traits<A1>::value;
    static const space_constant::valued_type second_argument_tag = space_constant::valued_tag_traits<A2>::value;
    typedef typename is_undeterminated<A1>::type undet_1;
    typedef typename is_undeterminated<A2>::type undet_2;
    evaluate_switch <self_type, Result, A1, A2, undet_1, undet_2> eval;
    eval (*this, omega_K, K, value);
  }
  template<class Result>
  void evaluate_on_side (
      const geo_basic<float_type,memory_type>&             omega_K, 
      const geo_element&                                   K, 
      const side_information_type&                         sid,
      Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic>& value,
      bool                                                 do_local_component_assembly) const
  {
    typedef typename hint<Result>::A1 A1;
    typedef typename hint<Result>::A2 A2;
    static const space_constant::valued_type  first_argument_tag = space_constant::valued_tag_traits<A1>::value;
    static const space_constant::valued_type second_argument_tag = space_constant::valued_tag_traits<A2>::value;
    typedef typename is_undeterminated<A1>::type undet_1;
    typedef typename is_undeterminated<A2>::type undet_2;
    evaluate_switch <self_type, Result, A1, A2, undet_1, undet_2> eval;
    eval (*this, omega_K, K, sid, value, do_local_component_assembly);
  }
  template<class Value>
  void local_dg_merge_on_side (
    const geo_basic<float_type,memory_type>&                  omega_K, 
    const geo_element&                                        S,
    const geo_element&                                        K0,
    const geo_element&                                        K1,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value0,
    const Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value1,
          Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>& value) const
  {
    _vf_expr.local_dg_merge_on_side (omega_K, S, K0, K1, value0, value1, value);
  }
  template<class Result>
  void valued_check() const {
    typedef typename hint<Result>::A1 A1;
    typedef typename hint<Result>::A2 A2;
    if (! is_undeterminated<A1>::value) _nl_expr.template valued_check<A1>();
    if (! is_undeterminated<A2>::value) _vf_expr.template valued_check<A2>();
  }
//protected:
// data:
  BinaryFunction  _f;
  NLExpr          _nl_expr;
  VFExpr          _vf_expr;
};
template<class F, class Expr1, class Expr2> struct is_field_expr_v2_variational_arg    <field_expr_v2_variational_binary_binded<F,Expr1,Expr2> > : std::true_type {};

} // namespace details
// ---------------------------------------------------------------------------
// 3.3. binary calls
// ---------------------------------------------------------------------------
namespace details {
  
template<class Expr1, class Expr2, class Sfinae = void>
struct is_field_expr_v2_variational_binary_multiplies_divides_left : std::false_type {};

template<class Expr1, class Expr2>
struct is_field_expr_v2_variational_binary_multiplies_divides_left <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
         is_field_expr_v2_nonlinear_arg  <Expr1>::value
    && ! is_field_expr_v2_constant       <Expr1>::value
    &&   is_field_expr_v2_variational_arg<Expr2>::value
  >::type
>
: std::true_type
{};

template<class Expr1, class Expr2>
struct is_field_expr_v2_variational_binary_multiplies_divides_right
:      is_field_expr_v2_variational_binary_multiplies_divides_left <Expr2,Expr1> {};

} // namespace details

#define _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_left(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_v2_variational_binary_multiplies_divides_left <Expr1,Expr2>::value \
 ,details::field_expr_v2_variational_binary_binded<				\
    FUNCTOR									\
   ,typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr1>::type \
   ,Expr2 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  typedef typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr1>::type wrap1_t; \
  return details::field_expr_v2_variational_binary_binded 			\
	<FUNCTOR,   wrap1_t,        Expr2> 					\
	(FUNCTOR(), wrap1_t(expr1), expr2); 					\
}

#define _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_right(FUNCTION,FUNCTOR) \
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_v2_variational_binary_multiplies_divides_right <Expr1,Expr2>::value \
 ,details::field_expr_v2_variational_binary_binded<				\
    details::swapper<FUNCTOR>							\
   , typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr2>::type \
   ,Expr1 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  typedef typename details::field_expr_v2_nonlinear_terminal_wrapper_traits<Expr2>::type wrap2_t; \
  return details::field_expr_v2_variational_binary_binded 			\
	<details::swapper<FUNCTOR>,            wrap2_t,        Expr1> 		\
	(details::swapper<FUNCTOR>(FUNCTOR()), wrap2_t(expr2), expr1); 		\
}
#define _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides(FUNCTION,FUNCTOR) \
        _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_left  (FUNCTION,FUNCTOR) \
        _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_right (FUNCTION,FUNCTOR)

_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides       (operator*, details::multiplies)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_right (operator/, details::divides)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides       (dot,       details::dot_)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides       (ddot,      details::ddot_)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides       (dddot,     details::dddot_)
#undef _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_left
#undef _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_right
#undef _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides

// ---------------------------------------------------------------------------
// 5. binary operators */ between one variational and a constant
// ---------------------------------------------------------------------------
namespace details {
  
template<class Expr1, class Expr2, class Sfinae = void>
struct is_field_expr_v2_variational_binary_multiplies_divides_constant_left : std::false_type {};

template<class Expr1, class Expr2>
struct is_field_expr_v2_variational_binary_multiplies_divides_constant_left <
  Expr1
 ,Expr2
 ,typename
  std::enable_if<
       is_field_expr_v2_constant       <Expr1>::value
    && is_field_expr_v2_variational_arg<Expr2>::value
  >::type
>
: std::true_type
{};

template<class Expr1, class Expr2>
struct is_field_expr_v2_variational_binary_multiplies_divides_constant_right
:      is_field_expr_v2_variational_binary_multiplies_divides_constant_left <Expr2,Expr1> {};

} // namespace details

#define _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant_left(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_v2_variational_binary_multiplies_divides_constant_left <Expr1,Expr2>::value \
 ,details::field_expr_v2_variational_unary<				\
    details::binder_first <FUNCTOR, Expr1> 					\
   ,Expr2 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::field_expr_v2_variational_unary 				\
	<details::binder_first <FUNCTOR,Expr1>,                    Expr2> 	\
	(details::binder_first <FUNCTOR,Expr1> (FUNCTOR(), expr1), expr2); 	\
}

#define _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant_right(FUNCTION,FUNCTOR)	\
template<class Expr1, class Expr2>						\
inline										\
typename									\
std::enable_if<									\
  details::is_field_expr_v2_variational_binary_multiplies_divides_constant_right <Expr1,Expr2>::value \
 ,details::field_expr_v2_variational_unary<				\
    details::binder_second <FUNCTOR, Expr2> 					\
   ,Expr1 /* vf */								\
  >										\
>::type										\
FUNCTION (const Expr1& expr1, const Expr2& expr2)				\
{										\
  return details::field_expr_v2_variational_unary 				\
	<details::binder_second <FUNCTOR,Expr2>,                    Expr1> 	\
	(details::binder_second <FUNCTOR,Expr2> (FUNCTOR(), expr2), expr1); 	\
}

#define _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant(FUNCTION,FUNCTOR)		\
        _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant_left  (FUNCTION,FUNCTOR) 	\
        _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant_right (FUNCTION,FUNCTOR)


_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant       (operator*, details::multiplies)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant_right (operator/, details::divides)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant       (dot,       details::dot_)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant       (ddot,      details::ddot_)
_RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant       (dddot,     details::dddot_)

#undef _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant_right
#undef _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant_left
#undef _RHEOLEF_make_field_expr_v2_variational_binary_operator_multiplies_divides_constant

} // namespace rheolef
#endif // _RHEOLEF_FIELD_EXPR_VARIATIONAL_H
