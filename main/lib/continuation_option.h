#ifndef _RHEOLEF_CONTINUATION_OPTION_H
#define _RHEOLEF_CONTINUATION_OPTION_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@functionfile continuation_option continuation solver options
@addindex continuation

Description
===========
This class is used to send options to the @ref continuation_3 algorithm.

Options
=======
`tol` \n
`max_iter`
>	These options are transmitted to the @ref damped_newton_3 algorithm
>	during the @ref continuation_3 algorithm.

`ini_delta_parameter` \n
`max_delta_parameter` \n
`max_delta_parameter`
>	These options control the @ref continuation_3 evolution
>	along the branch of solution.
>	When using the Keller continuation, these options control the arc-length parameter.

`n_adapt`
>	When non-zero, it allows one to activate the optional mesh adaptation feature
>	(see @ref adapt_3) embedded in the continuation algorithm.

TODO
====
Complete the documentation of all the others options.

Implementation
==============
@showfromfile
The `continuation_option` class is simply derivated
from the `adapt_option` one (see @ref adapt_3):

@snippet continuation_option.h verbatim_continuation_option
*/
} // namespace rheolef

#include "rheolef/adapt.h"

namespace rheolef {

// [verbatim_continuation_option]
//! @brief see the @ref continuation_option_3 page for the full documentation
struct continuation_option : adapt_option {
  Float ini_direction;
  Float kappa;
  Float tol;
  size_t max_iter;
  size_t newton_max_iter;
  Float min_delta_parameter;
  Float max_delta_parameter;
  Float ini_delta_parameter;
  Float theta_decr;
  Float theta_incr;
  Float theta_variation;
  size_t min_delta_parameter_successive_count_max;
  Float tol_cos_angle;
  bool do_prediction;
  bool do_check_going_back;
  size_t n_adapt;
  continuation_option(const adapt_option& aopt = adapt_option());
  void check() const;
};
// [verbatim_continuation_option]

// backward compat:
using continuation_option_type = continuation_option;

inline
continuation_option::continuation_option(const adapt_option& aopt)
: adapt_option(aopt),
  ini_direction(1),
  kappa (0.5),
  tol (std::numeric_limits<Float>::epsilon()),
  max_iter (10000),
  newton_max_iter(100),
  min_delta_parameter (0.001),
  max_delta_parameter (0.025),
  ini_delta_parameter (0.01),
  theta_decr(0.75),
  theta_incr(1.1),
  theta_variation(0.25),
  min_delta_parameter_successive_count_max(3),
  tol_cos_angle(0.1),
  do_prediction(true),
  do_check_going_back(true),
  n_adapt(0)
{}
inline
void
continuation_option::check() const {
  check_macro (ini_direction == 1 || ini_direction == -1, "ini_direction="<<tol<<" may be 1 or -1");
  check_macro (tol > 0, "tol="<<tol<<" may be > 0");
  check_macro (min_delta_parameter > 0, "min_delta_parameter="<<min_delta_parameter<<" may be > 0");
  check_macro (min_delta_parameter <= max_delta_parameter, "min_delta_parameter="<<min_delta_parameter
		<< " may be <= max_delta_parameter="<<max_delta_parameter);
  check_macro (min_delta_parameter <= ini_delta_parameter && ini_delta_parameter <= max_delta_parameter,
	 	"ini_delta_parameter="<<ini_delta_parameter<<" may be in range ["
  	 	<< min_delta_parameter << ":" << max_delta_parameter << "]");
  check_macro (0 < kappa && kappa < 1, "kappa="<<kappa<<" may be in range ]0:1[");
  check_macro (0 < theta_variation && theta_variation < 1, "theta_variation="<<theta_variation<<" may be in range ]0:1[");
  check_macro (1 < theta_incr, "theta_incr="<<theta_incr << " may be > 1");
  check_macro (0 < theta_decr && theta_decr < 1, "theta_decr="<<theta_decr<<" may be in range ]0:1[");
  check_macro (0 < tol_cos_angle && tol_cos_angle <= 1, "tol_cos_angle="<<tol_cos_angle<<" may be in range ]0:1]");
}

} // namespace rheolef
#endif // _RHEOLEF_CONTINUATION_OPTION_H
