///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// gmsh output
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997  update: 23 oct 2011
//
#include "rheolef/field.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/field_evaluate.h"
#include "rheolef/space_component.h"
#include "rheolef/field_expr.h"

namespace rheolef { 
using namespace std;

// extern:
template <class T>
odiststream&
geo_put_gmsh (odiststream& ods, const geo_basic<T,sequential>&);

template <class T>
odiststream&
field_put_gmsh (odiststream& ods, const field_basic<T,sequential>& uh, std::string name)
{
  typedef typename field_basic<T,sequential>::size_type size_type;
  geo_put_gmsh (ods, uh.get_geo());
  if (name == "") { name = uh.get_space().valued(); }
  ostream& gmsh = ods.os();
  gmsh << setprecision(numeric_limits<T>::digits10);
  size_type n_gmsh_comp = 1;
  switch (uh.get_space().valued_tag()) {
    case space_constant::scalar: n_gmsh_comp = 1; break;
    case space_constant::vector: n_gmsh_comp = 3; break;
    case space_constant::tensor: n_gmsh_comp = 9; break;
    default: error_macro ("put_gmsh: do not known how to print " << uh.valued() << "-valued field");
  }
  if (uh.get_space().degree() == uh.get_geo().order() && uh.get_space().get_basis().is_continuous()) {
    // ======================================================
    // Pk-C0 approx and k-th order mesh: use gmsh "NodeData" 
    // ======================================================
    gmsh << "$NodeData" << endl
         << "1" << endl
         << "\"" << name << "\"" << endl
         << "1" << endl
         << "0.0" << endl
         << "3" << endl
         << "0" << endl
         << n_gmsh_comp << endl
         << uh.get_geo().n_node() << endl;
    switch (uh.get_space().valued_tag()) {
      // ---------------------------
      case space_constant::scalar: {
      // ---------------------------
        for (size_type idof = 0, ndof = uh.ndof(); idof < ndof; idof++) {
          gmsh << idof+1 << " " << uh.dof(idof) << endl;
        }
        break;
      }
      // ---------------------------
      case space_constant::vector: {
      // ---------------------------
        // without memory allocation: use field_rdof_sliced_const proxy directly:
        size_type n_comp = uh.size();
#ifdef TO_CLEAN
        std::vector<details::field_rdof_sliced_const<field_basic<T,sequential>>> uh_comp (n_comp);
        for (size_type i_comp = 0; i_comp < n_comp; i_comp++) {
          uh_comp[i_comp].proxy_assign (uh[i_comp]);
        }
#endif // TO_CLEAN
        std::vector<T> u_dof (n_comp);
        for (size_type idof = 0, ndof = uh[0].ndof(); idof < ndof; idof++) {
          gmsh << idof+1;
          for (size_type i_comp = 0; i_comp < n_comp; i_comp++) {
            gmsh << " " << uh[i_comp].dof (idof);
          }
          for (size_type i_comp = n_comp; i_comp < 3; i_comp++) {
            gmsh << " 0";
          }
          gmsh << endl;
        }
        break;
      }
      // ---------------------------
      case space_constant::tensor: {
      // ---------------------------
        size_type d = uh.get_geo().dimension();
        switch (d) {
          case 1: {
            field_basic<T,sequential> t00 = uh(0,0);
            for (size_type idof = 0, ndof = t00.ndof(); idof < ndof; idof++) {
              gmsh << t00.dof(idof) << " 0 0 "
                   << "0 0 0 "
                   << "0 0 0" << endl;
            }
            break;
          }
          case 2: {
            field_basic<T,sequential> t00 = uh(0,0);
            field_basic<T,sequential> t01 = uh(0,1);
            field_basic<T,sequential> t11 = uh(1,1);
            for (size_type idof = 0, ndof = t00.ndof(); idof < ndof; idof++) {
              gmsh << t00.dof(idof) << " " << t01.dof(idof) << " 0 "
                   << t01.dof(idof) << " " << t11.dof(idof) << " 0 "
                   << "0 0 0" << endl;
            }
            break;
          }
          case 3: {
            field_basic<T,sequential> t00 = uh(0,0);
            field_basic<T,sequential> t01 = uh(0,1);
            field_basic<T,sequential> t11 = uh(1,1);
            field_basic<T,sequential> t02 = uh(0,2);
            field_basic<T,sequential> t12 = uh(1,2);
            field_basic<T,sequential> t22 = uh(2,2);
            for (size_type idof = 0, ndof = t00.ndof(); idof < ndof; idof++) {
              gmsh << t00.dof(idof) << " " << t01.dof(idof) << " " << t02.dof(idof) << " "
                   << t01.dof(idof) << " " << t11.dof(idof) << " " << t12.dof(idof) << " "
                   << t02.dof(idof) << " " << t12.dof(idof) << " " << t22.dof(idof) << endl;
            }
          }
          default: break;
        }
        break;
      }
      default: error_macro ("put_gmsh: do not known how to print " << uh.valued() << "-valued field");
    }
    gmsh << "$EndNodeData" << endl;
  } else if (uh.get_space().degree() == 0) {
    // ======================================
    // P0 approx: use gmsh "ElementData" 
    // ======================================
    gmsh << "$ElementData" << endl
         << "1" << endl
         << "\"" << name << "\"" << endl
         << "1" << endl
         << "0.0" << endl
         << "3" << endl
         << "0" << endl
         << n_gmsh_comp << endl
         << uh.get_geo().size() << endl;
    switch (uh.get_space().valued_tag()) {
      // ---------------------------
      case space_constant::scalar: {
      // ---------------------------
        for (size_type idof = 0, ndof = uh.ndof(); idof < ndof; idof++) {
          gmsh << idof+1 << " " << uh.dof(idof) << endl;
        }
        break;
      }
      default: error_macro ("put_gmsh: do not known how to print " << uh.valued() << "-valued field");
    }
    gmsh << "$EndElementData" << endl;
  } else {
    // TODO: check non-Lagrange, e.g. RTk ?
    // ======================================
    // Pkd approx: use gmsh "ElementNodeData" 
    // ======================================
    gmsh << "$ElementNodeData" << endl
         << "1" << endl
         << "\"" << name << "\"" << endl
         << "1" << endl
         << "0.0" << endl
         << "3" << endl
         << "0" << endl
         << n_gmsh_comp << endl
         << uh.get_geo().size() << endl;
    const   geo_basic<T,sequential>& omega = uh.get_geo();
    const space_basic<T,sequential>& Vh    = uh.get_space();
    switch (uh.get_space().valued_tag()) {
      // ---------------------------
      case space_constant::scalar: {
      // ---------------------------
        std::vector<size_type> idof;
        for (size_type ie = 0, ne = omega.size(); ie < ne; ie++) {
          const geo_element& K = omega[ie];
          Vh.dis_idof (K, idof);
          gmsh << ie+1 << " " << idof.size();
          for (size_type loc_idof = 0; loc_idof < idof.size(); loc_idof++) {
            gmsh << " " << uh.dof(idof[loc_idof]);
          }
          gmsh << endl;
        }
        break;
      }
      default: error_macro ("put_gmsh: do not known how to print " << uh.valued() << "-valued field");
    }
    gmsh << "$EndElementNodeData" << endl;
  }
  return ods;
}
template <class T>
odiststream&
field_put_gmsh (odiststream& ods, const field_basic<T,sequential>& uh)
{
  return field_put_gmsh (ods, uh, "");
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template odiststream& field_put_gmsh<Float>  (odiststream&, const field_basic<Float,sequential>&, std::string);
template odiststream& field_put_gmsh<Float>  (odiststream&, const field_basic<Float,sequential>&);

}// namespace rheolef
