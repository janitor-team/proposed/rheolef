///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/config.h"
#ifdef _RHEOLEF_HAVE_MPI
// mesh partition 
#include "rheolef/communicator.h"

#include "geo_partition_scotch.h"

namespace rheolef {
using namespace std;

void geo_dual (
	my_idxtype *elmdist,
	my_idxtype *eptr,
	vector<my_idxtype>& eind, 
	int *ncommonnodes,
	vector<my_idxtype>& xadj, 
	vector<my_idxtype>& adjncy, 
	const mpi::communicator& comm);

extern "C" {
void ParMETIS_V3_PartKway (const int * const, int * const, int * const, int * const, int * const, const int * const, const int * const, const int * const, const int * const, const float * const, const float * const, const int * const, int * const, int * const, MPI_Comm * const);
}
// -------------------------------------------------------------------------------------
// This function is the entry point of the distributed k-way multilevel mesh partitionioner
// This function assumes nothing about the mesh distribution.
// It is the general case.
// -------------------------------------------------------------------------------------
void geo_partition_scotch (
	my_idxtype *elmdist,
	my_idxtype *eptr, 
	vector<my_idxtype>& eind,
	my_idxtype *elmwgt, 
	int *ncon,
	int *ncommonnodes,
	int *nparts, 
	float *tpwgts,
	float *ubvec,
	int *edgecut,
	my_idxtype *part, 
	const mpi::communicator& comm)
{
  if (eind.size() == 0) {
    error_macro ("empty sizes not supported");
  }
  // Try and take care bad inputs
  if (elmdist == NULL || eptr == NULL ||
      ncon == NULL || ncommonnodes == NULL || nparts == NULL ||
      tpwgts == NULL || ubvec == NULL || edgecut == NULL || 
      part == NULL) {
    error_macro ("one or more required parameters is NULL");
  }
  comm.barrier();

  vector<my_idxtype> xadj;
  vector<my_idxtype> adjncy;
  geo_dual (
	elmdist,
	eptr,
	eind,
	ncommonnodes,
	xadj,
	adjncy,
	comm);

  comm.barrier();

  // Partition the graph
  MPI_Comm raw_comm = comm;
  int options[10];
  options[0] = 1;
  options[PMV3_OPTION_DBGLVL] = 0; // otherwise: timming print to stdout...
  options[PMV3_OPTION_SEED] = 0;
  int numflag = 0;
  int wgtflag = 0;
  ParMETIS_V3_PartKway(
	elmdist, 
	xadj.begin().operator->(),
	adjncy.begin().operator->(),
	elmwgt, 
	NULL, 
	&wgtflag, 
	&numflag, 
	ncon, 
        nparts,
	tpwgts,
	ubvec,
	options,
	edgecut,
	part,
	&raw_comm);

  comm.barrier();
}

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
