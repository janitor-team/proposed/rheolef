///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/config.h"
#ifdef _RHEOLEF_HAVE_MPI
#include "rheolef/geo.h"
#include "rheolef/geo_domain.h"
#include "rheolef/dis_macros.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/index_set.h"

namespace rheolef {

// ----------------------------------------------------------------------------
// output
// ----------------------------------------------------------------------------
/// @brief helper permutation class for geo i/o
struct permutation_proxy {
  typedef geo_element::size_type size_type;
  permutation_proxy (const hack_array<geo_element_hack>& elts, size_type first_dis_v = 0) 
    : _elts(elts), _first_dis_v(first_dis_v) {}
  size_type size() const { return _elts.size(); }
  size_type operator[] (size_type ie) const {
    const geo_element& K = _elts [ie];
    return K.ios_dis_ie() - _first_dis_v;
  }
  const permutation_proxy& data() const { return *this; }
// data:
  const hack_array<geo_element_hack>& _elts; 
  size_type                           _first_dis_v;
};
template <class T>
odiststream&
geo_rep<T,distributed>::put (odiststream& ops)  const
{
  using namespace std;
  iorheo::flag_type format = iorheo::flags(ops.os()) & iorheo::format_field;
  check_macro (format[iorheo::rheo], "geo distributed: unsupported geo output file format=" << format);

  communicator comm = base::_geo_element[reference_element::p].ownership().comm();
  size_type io_proc = odiststream::io_proc();
  size_type my_proc = comm.rank();
  size_type nproc   = comm.size();
  size_type dis_nnod = base::_node.dis_size ();
  size_type dis_nv   = base::_geo_element[reference_element::p].dis_size ();
  //
  // 1) put header
  //
  ops << setprecision(numeric_limits<Float>::digits10)
      << "#!geo" << endl
      << endl
      << "mesh" << endl
      << base::_version;

  geo_header h;
  h.dimension = base::_dimension;
  h.sys_coord = base::_sys_coord;
  h.order     = base::order();
  h.dis_size_by_variant [0] = base::_node.dis_size();
  for (size_type variant = 1; variant < reference_element::max_variant; variant++) {
    h.dis_size_by_variant [variant] = base::_geo_element [variant].dis_size();
  }
  ops << endl << h << endl;
  //
  // 2) put nodes
  //
  if (base::_dimension > 0) {
    T rounding_prec = iorheo::getrounding_precision(ops.os());
    if (rounding_prec == 0) {
      base::_node.permuted_put_values (ops,
		_inod2ios_dis_inod,
		_point_put<T>(base::_dimension));
    } else {
      base::_node.permuted_put_values (ops,
		_inod2ios_dis_inod, 
      		_round_point_put<T>(base::_dimension, rounding_prec));
    }
    ops << endl;
  }
  //
  // 3) build a node permutation disarray on io_proc
  //
  // elements are permuted back to original order and may
  // refer to original node numbering
  // TODO: disarray node_perm = merge_array_on_proc (_inod2ios_dis_inod , io_proc);
  std::vector<size_type> node_perm ((my_proc == io_proc) ? dis_nnod : 0);
  size_type tag_gather = distributor::get_new_tag();
  if (my_proc == io_proc) {
    size_type i_start = _inod2ios_dis_inod.ownership().first_index(my_proc);
    size_type i_size  = _inod2ios_dis_inod.ownership().size       (my_proc);
    for (size_type i = 0; i < i_size; i++) {
      node_perm [i_start + i] = _inod2ios_dis_inod [i];
    }
    for (size_type iproc = 0; iproc < nproc; iproc++) {
      if (iproc == my_proc) continue;
      size_type i_start = _inod2ios_dis_inod.ownership().first_index(iproc);
      size_type i_size  = _inod2ios_dis_inod.ownership().size       (iproc);
      comm.recv (iproc, tag_gather, node_perm.begin().operator->() + i_start, i_size);
    }
  } else {
    comm.send (0, tag_gather, _inod2ios_dis_inod.begin().operator->(), _inod2ios_dis_inod.size());
  }
  //
  // 4) put elements, faces & edges
  //
  geo_element_permuted_put put_element (node_perm);
  for (size_type dim = base::_gs._map_dimension; dim > 0; dim--) {
    size_type first_dis_v = 0;
    for (size_type variant = reference_element::first_variant_by_dimension(dim);
                   variant < reference_element:: last_variant_by_dimension(dim); variant++) {

      if (base::_gs.ownership_by_variant[variant].dis_size() == 0) continue;
      permutation_proxy perm (base::_geo_element [variant], first_dis_v);
      base::_geo_element [variant].permuted_put_values (ops, perm, put_element);
      first_dis_v += base::_gs.ownership_by_variant[variant].dis_size();
    }
    ops << endl;
  }
  //
  // 6) put domains
  //
  for (typename std::vector<domain_indirect_basic<distributed> >::const_iterator
        iter = base::_domains.begin(),
        last = base::_domains.end();
	iter != last; ++iter) {
    ops << endl;
    (*iter).put (ops, *this);
  }
  return ops;
}
template <class T>
void
geo_rep<T,distributed>::dump (std::string name)  const
{
  fatal_macro ("dump: not yet");
#ifdef TODO
  base::_node.dump (name + "-node");
  base::_geo_element[variant].dump(name + "-elem-"+ref::name(variant));
#endif // TODO
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template class geo_rep<Float,distributed>;

} // namespace rheolef
#endif // _RHEOLEF_HAVE_MPI
