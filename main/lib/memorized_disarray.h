#ifndef _RHEOLEF_MEMORIZED_DISARRAY_H
#define _RHEOLEF_MEMORIZED_DISARRAY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// 
// utility:
// internal data area accessor: return
//	_scalar_val
//	_vector_val
//	_tensor_val
// depending upon Value template parameter
//   => allows generic programmation vs Value
//
// implementation note:
//   template specialized accessor: use class-specialization
//   since specialization is neither available for func and class-member
// 
// ----------------------------------------------------------------------------
#include "rheolef/compiler_eigen.h"

namespace rheolef { namespace details {

// ----------------------------------------------------------------------------
// disarray-valued data
// ----------------------------------------------------------------------------
template <class T, class Value> struct memorized_disarray {};

#define _RHEOLEF_class_specialization(VALUE,MEMBER)			\
template <class T>							\
struct memorized_disarray<T,VALUE> {					\
  template <class Object>						\
  disarray<VALUE,typename Object::memory_type>&				\
  get (const Object& obj) const {					\
    return obj.MEMBER;							\
  }									\
};									\

_RHEOLEF_class_specialization(T,_scalar_val)
_RHEOLEF_class_specialization(point_basic<T>,_vector_val)
_RHEOLEF_class_specialization(tensor_basic<T>,_tensor_val)
_RHEOLEF_class_specialization(tensor3_basic<T>,_tensor3_val)
_RHEOLEF_class_specialization(tensor4_basic<T>,_tensor4_val)
#undef _RHEOLEF_class_specialization

}} // namespace rheolef::details
#endif // _RHEOLEF_MEMORIZED_DISARRAY_H
