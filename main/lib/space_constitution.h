#ifndef _RHEOLEF_SPACE_CONSTITUTION_H
#define _RHEOLEF_SPACE_CONSTITUTION_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/geo.h"
#include "rheolef/geo_domain.h"
#include "rheolef/space_constant.h"

namespace rheolef {

// forward declarations:
template <class T, class M> class space_mult_list;
template <class T, class M> class space_constitution;

// =====================================================================
// space_act = a domain + an act (block, unblock)
// =====================================================================

class space_act {
public:

// typedefs:

    typedef size_t size_type;
    static const size_type unset_index = std::numeric_limits<size_type>::max();

    enum act_type {
        block     = 0,
        unblock   = 1,
        block_n   = 2,
        unblock_n = 3
    };

// allocators:

    space_act(const std::string& dom_name, act_type act)
	: _dom_name(dom_name), _i_comp(unset_index), _act(act) {}
    space_act(const std::string& dom_name, size_type i_comp, act_type act)
	: _dom_name(dom_name), _i_comp(i_comp), _act(act) {}
    space_act()
        : _dom_name(), _i_comp(unset_index), _act() {}
    space_act(const space_act& x)
	: _dom_name(x._dom_name), _i_comp(x._i_comp), _act(x._act) {}

// accessors:

    const std::string& get_domain_name() const { return _dom_name; }
    act_type           get_act()         const { return _act; }
    size_type          get_component_index() const { return _i_comp; }

// data:
protected:
    std::string _dom_name;
    size_type   _i_comp;
    act_type    _act;
};
// =====================================================================
// space_constitution_terminal = a table of acts
// =====================================================================

template <class T, class M>
class space_constitution_terminal_rep {
public:
    typedef typename  std::vector<space_act>         container_type;
    typedef typename  container_type::size_type      size_type;
    typedef typename  container_type::const_iterator const_iterator;

// allocators:

    space_constitution_terminal_rep()
      : _acts(), _omega(), _fem_basis() {}

    space_constitution_terminal_rep(
		const geo_basic<T,M>&    omega,
		std::string              approx);

    space_constitution_terminal_rep (const space_constitution_terminal_rep<T,M>& scr)
      : _acts(scr._acts), _omega(scr._omega), _fem_basis(scr._fem_basis)
    {
	trace_macro ("physical copy of space_constitution_terminal_rep: size="<< size());
    }

// accessors:

    const geo_basic<T,M>& get_geo()            const { return _omega; }
    const geo_basic<T,M>& get_background_geo() const { return _omega.get_background_geo(); }
    const basis_basic<T>& get_basis()          const { return _fem_basis; }

    bool           is_initialized() const { return _fem_basis.is_initialized(); }
    size_type      size()  const { return _acts.size(); }
    const_iterator begin() const { return _acts.begin(); }
    const_iterator end()   const { return _acts.end(); }

// utility:

    void set_ios_permutations (
        disarray<size_type,M>& idof2ios_dis_idof,
        disarray<size_type,M>& ios_idof2dis_idof) const;

// inquiries:

    size_type degree_max() const { return _fem_basis.degree(); }
    bool have_compact_support_inside_element() const { return _fem_basis.have_compact_support_inside_element(); }
    bool is_discontinuous() const { return ! _fem_basis.is_continuous(); }
    void neighbour_guard() const { return get_geo().neighbour_guard(); }

// modifiers

    void set_geo   (const geo_basic<T,M>& omega)    { _omega = omega; }
    void set_basis (const basis_basic<T>& b) { _fem_basis = b; }
    void do_act    (const space_act& act);

// comparator:

    bool operator== (const space_constitution_terminal_rep<T,M>& V2) const { return _omega == V2._omega && _fem_basis.name() == V2._fem_basis.name(); }

// internal:
    void build_blocked_flag (
      disarray<size_type,M>& blocked_flag,
      const distributor&  comp_ownership,
      const distributor&  start_by_component) const;
protected:
// data:
    std::vector<space_act> _acts;
    geo_basic<T,M>         _omega;
    basis_basic<T>         _fem_basis;
};
template <class T, class M>
class space_constitution_terminal : public smart_pointer<space_constitution_terminal_rep<T,M> > {
public:

    typedef space_constitution_terminal_rep<T,M>   rep;
    typedef smart_pointer<rep>                   base;
    typedef typename  rep::size_type             size_type;
    typedef typename  rep::const_iterator        const_iterator;

// allocators:

    space_constitution_terminal()
     : smart_pointer<rep>(new_macro(rep)) {}

    space_constitution_terminal(
		const geo_basic<T,M>&	 omega,
		std::string              approx)
     : smart_pointer<rep>(new_macro(rep (omega,approx))) {}

// accessors:

    const geo_basic<T,M>& get_geo()            const { return base::data().get_geo(); }
    const geo_basic<T,M>& get_background_geo() const { return base::data().get_background_geo(); }
    const basis_basic<T>& get_basis()          const { return base::data().get_basis(); }
    size_type      size()  const { return base::data().size(); }
    const_iterator begin() const { return base::data().begin(); }
    const_iterator end()   const { return base::data().end(); }

// utility:

    void set_ios_permutations (
        disarray<size_type,M>& idof2ios_dis_idof,
        disarray<size_type,M>& ios_idof2dis_idof) const
    	{ base::data().set_ios_permutations (idof2ios_dis_idof, ios_idof2dis_idof); }

// inquiries:

    bool have_compact_support_inside_element() const { return base::data().have_compact_support_inside_element(); }
    bool is_discontinuous() const { return base::data().is_discontinuous(); }
    size_type degree_max() const { return base::data().degree_max(); }
    void neighbour_guard() const { return base::data().neighbour_guard(); }

// comparator:

    bool operator== (const space_constitution_terminal<T,M>& V2) const { return base::data().operator==(V2.data()); }

// modifiers

    void set_geo (const geo_basic<T,M>& omega) { base::data().set_geo (omega); }
    void set_basis (const basis_basic<T>& b)   { base::data().set_basis (b); }
    void do_act (const space_act& act)         { base::data().do_act(act); }

    void block     (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::block)); }
    void unblock   (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::unblock)); }
    void block_n   (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::block_n)); }
    void unblock_n (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::unblock_n)); }
};
// =======================================================================
// space_constitution = a recursive hierarchy of constitution
// =======================================================================
template <class T, class M>
class space_constitution_rep {
public:
    typedef space_constitution_rep<T,M>                this_type;
    typedef space_constitution<T,M>                    value_type;
    typedef std::vector<value_type>                    hierarchy_type;
    typedef space_constitution_terminal<T,M>             scalar_type;
    typedef typename  hierarchy_type::size_type        size_type;
    typedef typename  hierarchy_type::iterator         iterator;
    typedef typename  hierarchy_type::const_iterator   const_iterator;
    typedef typename  space_constant::valued_type valued_type;

// allocator:

    space_constitution_rep();
    space_constitution_rep (const space_constitution_rep<T,M>& x);
    space_constitution_rep (const geo_basic<T,M>& omega, std::string approx);
    space_constitution_rep (const space_mult_list<T,M>&);

// accessors & modifiers:

    const distributor& ownership() const { return _ownership; }
    std::string name()  const;
    size_type     ndof() const { return _ownership.size(); }
    size_type dis_ndof() const { return _ownership.dis_size(); }
    communicator comm()  const { return _ownership.comm(); }
    size_type ios_ndof() const;
    void do_act         (const space_act& act);
    const geo_basic<T,M>& get_geo()            const;
    const geo_basic<T,M>& get_background_geo() const;
    const basis_basic<T>& get_basis() const;
    size_type loc_ndof (const reference_element& hat_K) const;
    size_type assembly_loc_ndof (const geo_basic<T,M>& dom, const geo_element& bgd_K) const;
    void assembly_dis_idof (const geo_basic<T,M>& dom,
                   const geo_element& bgd_K, std::vector<geo_element::size_type>& dis_idof) const;
    void compute_external_dofs (std::set<size_type>& ext_dof_set) const;
    void set_valued_tag (valued_type valued_tag){ _valued_tag = valued_tag; }
    void set_valued     (const std::string& valued){ _valued_tag = space_constant::valued_tag (valued); }
    const valued_type& valued_tag() const       { return _valued_tag; }
    const std::string& valued()     const       { return space_constant::valued_name (_valued_tag); }
    bool is_hierarchical() const                { return _is_hier; }
    void set_hierarchy (bool hier)              { _is_hier = hier; }
 
// inquiries:

    bool have_compact_support_inside_element() const;
    bool is_discontinuous() const;
    size_type degree_max() const;
    void neighbour_guard() const;

// scalar accessors & modifiers:

    const scalar_type& get_terminal() const { scalar_guard(); return _terminal_constit; }
    scalar_type&       get_terminal()       { scalar_guard(); return _terminal_constit; }

// hierarchy accessors & modifiers:

    const hierarchy_type& get_hierarchy() const { hierarchy_guard(); return _hier_constit; }
    hierarchy_type&       get_hierarchy()       { hierarchy_guard(); return _hier_constit; }
    size_type size() const { return _is_hier ? _hier_constit.size() : 0; }
    space_constitution<T,M>&       operator[] (size_type i_comp)       { return get_hierarchy() [i_comp]; }
    const space_constitution<T,M>& operator[] (size_type i_comp) const { return get_hierarchy() [i_comp]; }

    size_type flattened_size() const { return _flattened_size; }
    size_type comp_dis_idof2dis_idof (size_type i_comp, size_type comp_dis_idof) const;

// utility:

    void set_ios_permutations (
        disarray<size_type,M>& idof2ios_dis_idof,
        disarray<size_type,M>& ios_idof2dis_idof) const;

// comparator:

    bool operator== (const space_constitution_rep<T,M>& V2) const;

// internal:
    void initialize() const;
    void append_external_dof (
        const geo_basic<T,M>&         dom,
        std::set<size_type>&          ext_dof_set,
        const distributor&            dof_ownership,
        const distributor&            start_by_component) const;
    void compute_external_dofs (
        std::set<size_type>&            ext_dof_set,
        const distributor&              dof_ownership,
	const std::vector<distributor>& start_by_component,
	size_type&                      i_comp) const;
    void build_blocked_flag_recursive (
      disarray<size_type,M>&             blocked_flag, // disarray<bool,M> not supported
      const std::vector<distributor>& start_by_component,
      size_type&                      i_comp) const;
    disarray<size_type,M> build_blocked_flag() const;

// io:
   void put (std::ostream& out, size_type level = 0) const;

protected:
// internals:
    friend class space_constitution<T,M>;
    void hierarchy_guard() const { check_macro ( _is_hier, "invalid access to a non-hierarchical constitution"); }
    void scalar_guard()   const { check_macro (!_is_hier, "invalid access to a hierarchical constitution"); }
    void set_ios_permutation_recursion (
	disarray<size_type,M>& idof2ios_dis_idof, 
	size_type&          comp_start_idof,
	size_type&          comp_start_dis_idof) const;
    size_type _init_flattened_size() const;
    void _init_start_by_flattened_component(
        size_type&            i_flat_comp,
        size_type&        start_flat_comp_idof,
        size_type&    dis_start_flat_comp_idof,
        std::vector<distributor>& start_by_flattened_component) const;
    void _init_start_by_component() const;
    void _assembly_dis_idof_recursive (
  	const geo_basic<T,M>&                dom,
  	const geo_element&                   bgd_K,
  	typename std::vector<geo_element::size_type>::iterator& dis_idof_t,
        const distributor&                   hier_ownership,
  	const std::vector<distributor>&      start_by_flattened_component,
  	size_type&                           i_flat_comp) const;

// data:
    // union (hier,not hier):
    mutable bool                     _is_initialized;
    mutable size_type                _flattened_size;
    mutable std::vector<distributor> _start_by_flattened_component;
    mutable std::vector<distributor> _start_by_component;
    mutable distributor              _ownership;
    valued_type                      _valued_tag;
    bool		             _is_hier;
    scalar_type                      _terminal_constit;
    hierarchy_type                   _hier_constit;
    mutable std::array<size_type, reference_element::max_variant> _loc_ndof; // working array
};
// ----------------------------------------------------------------------------
// inlined
// ----------------------------------------------------------------------------
// allocators: there are 4 allocators, here are 2, the 2 others are in .cc
template <class T, class M>
inline
space_constitution_rep<T,M>::space_constitution_rep()
 : _is_initialized(false),
   _flattened_size(0),
   _start_by_flattened_component(),
   _start_by_component(),
   _ownership(),
   _valued_tag(space_constant::mixed),
   _is_hier(false),
   _terminal_constit(),
   _hier_constit(),
   _loc_ndof()
{
  _loc_ndof.fill (std::numeric_limits<size_type>::max());
}
template <class T, class M>
inline
space_constitution_rep<T,M>::space_constitution_rep (const space_constitution_rep<T,M>& x)
 : _is_initialized(x._is_initialized),
   _flattened_size(x._flattened_size),
   _start_by_flattened_component(x._start_by_flattened_component),
   _start_by_component(x._start_by_component),
   _ownership(x._ownership),
   _valued_tag(x._valued_tag),
   _is_hier(x._is_hier),
   _terminal_constit(x._terminal_constit),
   _hier_constit(x._hier_constit),
   _loc_ndof()
{
  _loc_ndof.fill (std::numeric_limits<size_type>::max());
}
template <class T, class M>
inline
space_constitution_rep<T,M>::space_constitution_rep (
    const geo_basic<T,M>&    omega,
    std::string              approx)
 : _is_initialized(false),
   _flattened_size(0),
   _start_by_flattened_component(),
   _start_by_component(),
   _ownership(),
   _valued_tag(space_constant::scalar),
   _is_hier(false),
   _terminal_constit(),
   _hier_constit(),
   _loc_ndof()
{
  _loc_ndof.fill (std::numeric_limits<size_type>::max());
  _terminal_constit = scalar_type (omega, approx);
  if (_terminal_constit.get_basis().is_initialized()) {
    _valued_tag = _terminal_constit.get_basis().valued_tag();
  }
  initialize();
}
// ----------------------------------------------------------------------------
// space_constitution
// ----------------------------------------------------------------------------

template <class T, class M = rheo_default_memory_model>
class space_constitution : public smart_pointer<space_constitution_rep<T,M> > {
public:

    typedef space_constitution_rep<T,M>                rep;
    typedef smart_pointer<rep>                         base;
    typedef typename  rep::size_type                   size_type;
    typedef typename  rep::const_iterator              const_iterator;
    typedef typename  rep::scalar_type                 scalar_type;
    typedef typename  rep::hierarchy_type              hierarchy_type;
    typedef typename  rep::valued_type                 valued_type;

// allocators:

    space_constitution()
     : base(new_macro(rep)) {}

    space_constitution(const base& b)
     : base(b) {}

    space_constitution(
		const geo_basic<T,M>&	 omega,
		std::string              approx)
     : base(new_macro(rep (omega,approx))) {}

    space_constitution(const space_mult_list<T,M>& expr)
     : base(new_macro(rep (expr))) {}

// accessors & modifiers:

    const distributor& ownership() const { return base::data().ownership(); }
    std::string name()     const { return base::data().name(); }
    size_type ndof()        const { return base::data().ndof(); }
    size_type dis_ndof()    const { return base::data().dis_ndof(); }
    size_type ios_ndof()    const { return base::data().ios_ndof(); }
    communicator comm()     const { return base::data().comm(); }
    const geo_basic<T,M>& get_geo()            const { return base::data().get_geo(); }
    const geo_basic<T,M>& get_background_geo() const { return base::data().get_background_geo(); }
    const basis_basic<T>& get_basis()          const { return base::data().get_basis(); }
    size_type loc_ndof (const reference_element& hat_K) const { return base::data().loc_ndof (hat_K); }
    size_type assembly_loc_ndof (const geo_basic<T,M>& dom, const geo_element& bgd_K) const {
    		return base::data().assembly_loc_ndof (dom, bgd_K); }
    void assembly_dis_idof (  const geo_basic<T,M>& dom, const geo_element& bgd_K, std::vector<geo_element::size_type>& dis_idof) const {
		base::data().assembly_dis_idof (dom, bgd_K, dis_idof); }
    void do_act    (const space_act& act)       { base::data().do_act(act); }
    void block     (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::block)); }
    void unblock   (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::unblock)); }
    void block_n   (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::block_n)); }
    void unblock_n (const domain_indirect_basic<M>& dom) { do_act (space_act(dom.name(), space_act::unblock_n)); }
    disarray<size_type,M> build_blocked_flag() const { return base::data().build_blocked_flag(); }
    void compute_external_dofs (std::set<size_type>& ext_dof_set) const
    					{ base::data().compute_external_dofs(ext_dof_set); }
    bool is_hierarchical() const { return base::data().is_hierarchical(); }

// inquiries:

    bool have_compact_support_inside_element() const { return base::data().have_compact_support_inside_element(); }
    bool is_discontinuous() const { return base::data().is_discontinuous(); }
    size_type degree_max() const { return base::data().degree_max(); }
    void neighbour_guard() const { return base::data().neighbour_guard(); }

// scalar accessors & modifiers:

    const space_constitution_terminal<T,M>& get_terminal() const { return base::data().get_terminal(); }
    space_constitution_terminal<T,M>&       get_terminal()       { return base::data().get_terminal(); }

// hierarchy accessors & modifiers:

    void set_hierarchy(bool hier = true)        { return base::data().set_hierarchy(hier); }
    void set_valued_tag (valued_type valued_tag){ base::data().set_valued_tag(valued_tag); }
    void set_valued     (const std::string& valued){ base::data().set_valued (valued); }
    const valued_type& valued_tag() const       { return base::data().valued_tag(); }
    const std::string& valued()     const       { return base::data().valued(); }
    const hierarchy_type& get_hierarchy() const { return base::data().get_hierarchy(); }
    hierarchy_type&       get_hierarchy()       { return base::data().get_hierarchy(); }
    size_type size() const { return base::data().size(); }
    space_constitution<T,M>&       operator[] (size_type i_comp)       { return base::data().operator[] (i_comp); }
    const space_constitution<T,M>& operator[] (size_type i_comp) const { return base::data().operator[] (i_comp); }
    size_type flattened_size() const { return base::data().flattened_size(); }
    size_type comp_dis_idof2dis_idof (size_type i_comp, size_type comp_dis_idof) const
                                   { return base::data().comp_dis_idof2dis_idof (i_comp, comp_dis_idof); }

// utility:

    void set_ios_permutations (
        disarray<size_type,M>& idof2ios_dis_idof,
        disarray<size_type,M>& ios_idof2dis_idof) const
        { base::data().set_ios_permutations (idof2ios_dis_idof, ios_idof2dis_idof); }

// comparator:

    bool operator== (const space_constitution<T,M>& V2) const { return base::data().operator==(V2.data()); }

protected:
// internal:
    friend class space_constitution_rep<T,M>;
    void set_ios_permutation_recursion (
	disarray<size_type,M>& idof2ios_dis_idof, 
	size_type&          comp_start_idof,
	size_type&          comp_start_dis_idof) const
        { base::data().set_ios_permutation_recursion (idof2ios_dis_idof, comp_start_idof, comp_start_dis_idof); }
};
template<class T, class M>
idiststream&
operator>> (idiststream& ids, space_constitution<T,M>& constit);

} // namespace rheolef
#endif // _RHEOLEF_SPACE_CONSTITUTION_H
