/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// x in K ?
//   where x is a point and K a geo_element
//
// TODO: omega.order == 1 only: not valid for curved elements
// ---------------------------------------------------------------------
#include "rheolef/geo_element_contains.h"

namespace rheolef { namespace details {

template <class T, class M>
bool
point_belongs_to_e (
  const geo_element&                K,
  const disarray<point_basic<T>,M>& node,
  const point_basic<T>&             x) 
{
  const point_basic<T>& p0 = node.dis_at(K[0]);
  const point_basic<T>& p1 = node.dis_at(K[1]);
  return x[0] >= p0[0] && x[0] <= p1[0];
}
template <class T, class M>
bool
point_belongs_to_t (
  const geo_element&                K,
  const disarray<point_basic<T>,M>& node,
  const point_basic<T>&             x) 
{
  static const T eps = 1e3*std::numeric_limits<T>::epsilon();
  const point_basic<T>& p0 = node.dis_at(K[0]);
  const point_basic<T>& p1 = node.dis_at(K[1]);
  const point_basic<T>& p2 = node.dis_at(K[2]);
  if (orient2d( x, p1, p2) < -eps) return false;
  if (orient2d(p0,  x, p2) < -eps) return false;
  if (orient2d(p0, p1,  x) < -eps) return false;
  return true;
}
template <class T, class M>
bool
point_belongs_to_q (
  const geo_element&                K,
  const disarray<point_basic<T>,M>& node,
  const point_basic<T>&             x) 
{
  static const T eps = 1e3*std::numeric_limits<T>::epsilon();
  const point_basic<T>& p0 = node.dis_at(K[0]);
  const point_basic<T>& p1 = node.dis_at(K[1]);
  const point_basic<T>& p2 = node.dis_at(K[2]);
  const point_basic<T>& p3 = node.dis_at(K[3]);
  if (orient2d(x, p0, p1) < -eps) return false;
  if (orient2d(x, p1, p2) < -eps) return false;
  if (orient2d(x, p2, p3) < -eps) return false;
  if (orient2d(x, p3, p0) < -eps) return false;
  return true;
}
template <class T, class M>
bool
point_belongs_to_T (
  const geo_element&                K,
  const disarray<point_basic<T>,M>& node,
  const point_basic<T>&             x) 
{
  static const T eps = 1e3*std::numeric_limits<T>::epsilon();
  const point_basic<T>& p0 = node.dis_at(K[0]);
  const point_basic<T>& p1 = node.dis_at(K[1]);
  const point_basic<T>& p2 = node.dis_at(K[2]);
  const point_basic<T>& p3 = node.dis_at(K[3]);
  if (orient3d( x, p1, p2, p3) < -eps) return false;
  if (orient3d(p0,  x, p2, p3) < -eps) return false;
  if (orient3d(p0, p1,  x, p3) < -eps) return false;
  if (orient3d(p0, p1, p2,  x) < -eps) return false;
  return true;
}
template <class T, class M>
bool
point_belongs_to_H (
  const geo_element&                K,
  const disarray<point_basic<T>,M>& node,
  const point_basic<T>&             x) 
{
  typedef reference_element::size_type           size_type;
  static const T eps = 1e3*std::numeric_limits<T>::epsilon();
  const point_basic<T>& p0 = node.dis_at(K[0]);
  const point_basic<T>& p1 = node.dis_at(K[1]);
  const point_basic<T>& p2 = node.dis_at(K[2]);
  const point_basic<T>& p3 = node.dis_at(K[3]);
  const point_basic<T>& p4 = node.dis_at(K[4]);
  const point_basic<T>& p5 = node.dis_at(K[5]);
  const point_basic<T>& p6 = node.dis_at(K[6]);
  const point_basic<T>& p7 = node.dis_at(K[7]);
  const point_basic<T>* q[8] = {&p0, &p1, &p2, &p3, &p4, &p5, &p6, &p7};
  for (size_type loc_isid = 0, loc_nsid = 6; loc_isid < loc_nsid; loc_isid++) {
    size_type j0 = reference_element_H::subgeo_local_node (1, 2, loc_isid, 0);
    size_type j1 = reference_element_H::subgeo_local_node (1, 2, loc_isid, 1);
    size_type j2 = reference_element_H::subgeo_local_node (1, 2, loc_isid, 2);
    if (orient3d(x, *(q[j0]), *(q[j1]), *(q[j2])) < -eps) return false;
  }
  return true;
}
template <class T, class M>
bool
point_belongs_to_P (
  const geo_element&                K,
  const disarray<point_basic<T>,M>& node,
  const point_basic<T>&             x) 
{
  typedef reference_element::size_type           size_type;
  static const T eps = 1e3*std::numeric_limits<T>::epsilon();
  const point_basic<T>& p0 = node.dis_at(K[0]);
  const point_basic<T>& p1 = node.dis_at(K[1]);
  const point_basic<T>& p2 = node.dis_at(K[2]);
  const point_basic<T>& p3 = node.dis_at(K[3]);
  const point_basic<T>& p4 = node.dis_at(K[4]);
  const point_basic<T>& p5 = node.dis_at(K[5]);
  const point_basic<T>* q[6] = {&p0, &p1, &p2, &p3, &p4, &p5};
  for (size_type loc_isid = 0, loc_nsid = 5; loc_isid < loc_nsid; loc_isid++) {
    size_type j0 = reference_element_P::subgeo_local_node (1, 2, loc_isid, 0);
    size_type j1 = reference_element_P::subgeo_local_node (1, 2, loc_isid, 1);
    size_type j2 = reference_element_P::subgeo_local_node (1, 2, loc_isid, 2);
    if (orient3d(x, *(q[j0]), *(q[j1]), *(q[j2])) < -eps) return false;
  }
  return true;
}
template <class T, class M>
bool
contains (
  const geo_element&                K,
  const disarray<point_basic<T>,M>& node,
  const point_basic<T>&             x)
{
  switch (K.variant()) {
    case reference_element::e: return point_belongs_to_e (K, node, x);
    case reference_element::t: return point_belongs_to_t (K, node, x);
    case reference_element::q: return point_belongs_to_q (K, node, x);
    case reference_element::T: return point_belongs_to_T (K, node, x);
    case reference_element::P: return point_belongs_to_P (K, node, x);
    case reference_element::H: return point_belongs_to_H (K, node, x);
    default: error_macro ("unsupported element type '" << K.name() << "'"); return false;
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M)                     \
template bool contains (				\
  const geo_element&		,			\
  const disarray<point_basic<T>,M>&,			\
  const point_basic<T>&);

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI
#undef _RHEOLEF_instanciation

}} // namespace rheolef::details
