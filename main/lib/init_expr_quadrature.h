#ifndef _RHEOLEF_INIT_EXPR_QUADRATURE_H
#define _RHEOLEF_INIT_EXPR_QUADRATURE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// set iopt.order when not yet set, from expr polynomial degree
//
// author: Pierre.Saramito@imag.fr
//
// date: 18 march 2018 
//
#include "rheolef/form_expr_variational.h"

namespace rheolef { namespace details {

template<class T, class M>
integrate_option
expr_quadrature_init_iopt (
    const geo_basic<T,M>&   omega_K,
    const space_basic<T,M>& X,
    size_t                  n_derivative,
    const integrate_option& iopt)
{
  using size_type = quadrature_option::size_type;
  integrate_option new_iopt = iopt;
  if (iopt.get_order() == std::numeric_limits<quadrature_option::size_type>::max()) {
    size_type k = X.get_constitution().degree_max();
    size_type quad_order = 2*k + 1;
    new_iopt.set_order (quad_order);
  }
  return new_iopt;
}
template<class T, class M>
integrate_option
expr_quadrature_init_iopt (
    const geo_basic<T,M>&   omega_K,
    const space_basic<T,M>& X,
    const space_basic<T,M>& Y,
    size_t                  n_derivative,
    const integrate_option& iopt)
{
  using size_type = quadrature_option::size_type;
  integrate_option new_iopt = iopt;
  if (new_iopt.get_order() == std::numeric_limits<size_type>::max()) {
    size_type k1 = X.get_constitution().degree_max();
    size_type k2 = Y.get_constitution().degree_max();
    size_type quad_order = k1 + k2 + 1;
    if (omega_K.get_background_geo().sizes().ownership_by_variant[reference_element::q].dis_size() != 0 ||
        omega_K.get_background_geo().sizes().ownership_by_variant[reference_element::P].dis_size() != 0 ||
        omega_K.get_background_geo().sizes().ownership_by_variant[reference_element::H].dis_size() != 0) {
          // integrate exactly ??
          quad_order += 2;
    }
    if (omega_K.coordinate_system() != space_constant::cartesian) quad_order++; // multiplies by a 'r' weight
    if (quad_order >= n_derivative) quad_order -= n_derivative;
    new_iopt.set_order (quad_order);
  }
  return new_iopt;
}
template<class T>
quadrature<T>
expr_quadrature_init_quad (const integrate_option& iopt)
{
  quadrature<T> quad;
  quad.set_order (iopt.get_order());
  quad.set_family(iopt.get_family());
  return quad;
}
} // namespace details
} // namespace rheolef
#endif // _RHEOLEF_INIT_EXPR_QUADRATURE_H
