#ifndef _RHEOLEF_SPACE_H
#define _RHEOLEF_SPACE_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// AUTHORS: Pierre.Saramito@imag.fr
// DATE:   14 december 2010

namespace rheolef {
/**
@classfile space piecewise polynomial finite element space
@addindex class space

Description
===========
The `space` class contains the numbering 
of the *degrees of freedoms*.
The degrees of freedoms are the coefficients in
the basis of the vectorial space of
piecewise polynomial finite element functions.
The set of degrees of freedom coefficients is splited in two
subsets: the *unknowns*
and the *blocked* degrees of freedoms.
The blocked degrees of freedom are associated to
Dirichlet boundary conditions, where the values
of the function, and thus, of the coefficients,  are prescribed.
The unknown degrees of freedom are all the others.
Finally, the `space` class is mainly a table of renumbering
for these coefficients.

The space is characterized by two entities: 
its finite element, described by the @ref basis_2 class
and its mesh, described by the @ref geo_2 class.
By default, the space is scalar valued.
It extends to vector and tensor valued functions.

Example
=======

        space Q (omega, "P1");
        space V (omega, "P2", "vector");
        space T (omega, "P1d", "tensor");

Product of spaces
=================

        space X = T*V*Q;
        space Q2 = pow(Q,2);

Space of reals
==============
The 'space::real()' function returns
the space of reals and could be used in a product
of spaces as:

	space IR = space::real();
	space Yh = Xh*IR;

This convention is useful e.g. when a Lagrange multiplier 
is a real, not a field.
It extends to `IR^n` for any `size_t n` as:

        space IRn = pow(IR,n);

Implementation
==============
@showfromfile
The `space` class is simply an alias to the `space_basic` class

@snippet space.h verbatim_space
@par

The `space_basic` class provides an interface
to a data container:

@snippet space.h verbatim_space_basic
*/
} // namespace rheolef

#include "rheolef/geo.h"
#include "rheolef/geo_domain.h"
#include "rheolef/space_constitution.h"

namespace rheolef {

// forward declarations:
template <class T, class M> class field_basic;
template <class T, class M> class space_mult_list;
template <class T, class M> class space_component;
template <class T, class M> class space_component_const;

// =====================================================================
// a dof = a degree-of-freedom 
//       = space_pair
//       = pair (bool is_blocked ; size_t dis_iub)
// =====================================================================
// TODO: compact the bool as an extra bit in size_type ?
struct space_pair_type {
    typedef disarray<size_t>::size_type size_type;
    space_pair_type () : _blk(false), _dis_iub (std::numeric_limits<size_type>::max()) {}
    space_pair_type (bool blk, size_type dis_iub) : _blk(blk), _dis_iub(dis_iub) {}
    bool is_blocked() const { return _blk; }
    size_type dis_iub()   const { return _dis_iub; }
    void set_dis_iub (size_type dis_iub) { _dis_iub = dis_iub; }
    void set_blocked (bool blk)  { _blk = blk; }
    friend std::ostream&  operator<< (std::ostream& os, const space_pair_type& x) {
	return os << "{" << x.is_blocked() << "," << x.dis_iub() << "}"; }
    template<class Archive>
    void serialize (Archive& ar, const unsigned int version) { ar & _blk; ar & _dis_iub; }
protected:
    bool      _blk;
    size_type _dis_iub;
};

} // namespace rheolef

#ifdef _RHEOLEF_HAVE_MPI
// =====================================================================
// Some serializable types, like geo_element, have a fixed amount of data stored at fixed field positions.
// When this is the case, boost::mpi can optimize their serialization and transmission to avoid extraneous 
// copy operations.
// To enable this optimization, we specialize the type trait is_mpi_datatype, e.g.:
namespace boost {
 namespace mpi {
  template <> struct is_mpi_datatype<rheolef::space_pair_type> : mpl::true_ { };
 } // namespace mpi
} // namespace boost
#endif // _RHEOLEF_HAVE_MPI

namespace rheolef {

// =====================================================================
// 1) representation: space_base_rep and space_rep
// =====================================================================
template <class T, class M>
class space_base_rep {
public:

// typedefs:

    typedef typename space_pair_type::size_type  size_type;
    typedef typename space_constant::valued_type valued_type;

// allocators:

    space_base_rep ();
    space_base_rep (
      		const geo_basic<T,M>&    omega,
      		std::string              approx,
      		std::string              prod_valued);
    space_base_rep (
                const geo_basic<T,M>&    omega,
	        const basis_basic<T>&    b);
    space_base_rep (const space_constitution<T,M>& constit);
    space_base_rep (const space_mult_list<T,M>&);
    virtual ~space_base_rep () {}

// accessors:

    const distributor&  ownership() const { return _idof2blk_dis_iub.ownership(); }
    size_type                ndof() const { return ownership().size(); }
    size_type            dis_ndof() const { return ownership().dis_size(); }
    const communicator&      comm() const { return ownership().comm(); }

    const space_constitution<T,M>& get_constitution() const { return _constit; }
    const geo_basic<T,M>&     get_geo()       const { return _constit.get_geo(); }
    const basis_basic<T>& get_basis() const { return _constit.get_basis(); }
    valued_type           valued_tag()   const { return _constit.valued_tag(); }
    const std::string&    valued()       const { return _constit.valued(); }
    size_type             size()   const { return _constit.size(); }
    space_component<T,M>       operator[] (size_type i_comp);
    space_component_const<T,M> operator[] (size_type i_comp) const;

    std::string name() const; /// e.g. "P1(square)", for field_expr<Expr> checks

    void block  (const domain_indirect_basic<M>& dom) { no_freeze_check(); _constit.block  (dom); }
    void unblock(const domain_indirect_basic<M>& dom) { no_freeze_check(); _constit.unblock(dom);}

    void block_n  (const domain_indirect_basic<M>& dom) { no_freeze_check(); _constit.block_n  (dom); }
    void unblock_n(const domain_indirect_basic<M>& dom) { no_freeze_check(); _constit.unblock_n(dom);}

    bool                 is_blocked (size_type idof) const { freeze_check(); return _idof2blk_dis_iub [idof].is_blocked(); }
    size_type               dis_iub (size_type idof) const { freeze_check(); return _idof2blk_dis_iub [idof].dis_iub(); }
    const point_basic<T>& xdof (size_type idof) const { return _xdof [idof]; }
    const disarray<point_basic<T>,M >& get_xdofs() const { return _xdof; }

    const distributor& iu_ownership() const { freeze_guard(); return _iu_ownership; }
    const distributor& ib_ownership() const { freeze_guard(); return _ib_ownership; }

    void dis_idof (const geo_element& K, std::vector<size_type>& dis_idof) const;

    // TODO: merge all V.xxx_momentum: the return type can be computed from Function::result_type
    template <class Function>
    T momentum (const Function& f, size_type idof) const { return f (xdof(idof)); }

    template <class Function>
    point_basic<T> vector_momentum (const Function& f, size_type idof) const { return f (xdof(idof)); }

    template <class Function>
    tensor_basic<T> tensor_momentum (const Function& f, size_type idof) const { return f (xdof(idof)); }

    disarray<size_type, M> build_dom_dis_idof2bgd_dis_idof (
    	const space_base_rep<T,M>& Wh, const std::string& dom_name) const;

    disarray<size_type, M> build_dom_dis_idof2bgd_dis_idof (
        const space_base_rep<T,M>& Wh, const geo_basic<T,M>& bgd_gamma) const;

// comparator:

    bool operator== (const space_base_rep<T,M>& V2) const { 
	return _constit.operator==(V2._constit); } // TODO: compare also blocked/unknown sizes:

    friend bool are_compatible (const space_base_rep<T,M>& V1, const space_base_rep<T,M>& V2) {
    	return V1._constit.operator==(V2._constit); }

// for lazy (un-assembled) on geo_domains:

    void parent_subgeo_owner_check() const {
      check_macro (_parent_subgeo_owner_initialized, "parent_subgeo_owner_guard() should be called before local accessors");
    }
    bool parent_subgeo_owner_guard() const;
    size_type get_parent_subgeo_owner (size_type dis_idof) const;
    template <class Set>
    void get_parent_subgeo_owner_dis_indexes (Set& ext_dis_idofs) const;

protected:
    template <class T1, class M1> friend class field_basic;
// internal:
    void init_xdof();
    void freeze_guard() const {
    	if (_have_freezed) return;
    	_have_freezed = true;
    	freeze_body();
    }
    void freeze_check() const {
    	check_macro (_have_freezed, "space should be freezed before calling local accessors");
    }
    void no_freeze_check() const {
    	check_macro (!_have_freezed, "freezed space cannot accept new (un)blocked domains");
    }
    void base_freeze_body() const;
    virtual void freeze_body() const { return base_freeze_body(); }
// data: lazy initialization (on demand only), thus most are mutable
    space_constitution<T,M>                _constit;
    disarray<point_basic<T>,M >            _xdof;          // nodal approx only
    mutable bool                           _have_freezed;
    mutable disarray<space_pair_type,M>    _idof2blk_dis_iub;  // pair (is_blocked ; dis_iu_or_ib); use ownership
    mutable disarray<int,M>                _has_nt_basis;  // whether block_n or block_t : for piola ; use ownership
    mutable disarray<point_basic<T>,M>     _normal;        // when block_{nt} ; use ownership
    mutable distributor                    _iu_ownership;  // unknown values distribution
    mutable distributor                    _ib_ownership;  // blocked values distribution
    mutable bool                           _parent_subgeo_owner_initialized;
    mutable bool                           _parent_subgeo_owner_reattribution_required;
    mutable disarray<size_type,M>          _parent_subgeo_owner;
};
template <class T, class M>
template <class Set>
void
space_base_rep<T,M>::get_parent_subgeo_owner_dis_indexes (Set& ext_dis_idofs) const
{
  parent_subgeo_owner_guard();
  if (! _parent_subgeo_owner_reattribution_required) {
    ext_dis_idofs = Set();
    return;
  }
  _parent_subgeo_owner.get_dis_indexes (ext_dis_idofs);
}
// ---------------------------------------------------------------------
// space_rep
// ---------------------------------------------------------------------
template <class T, class M> class space_rep {};

template <class T>
class space_rep<T,sequential> : public space_base_rep<T,sequential> {
public:

// typedefs:

    typedef space_base_rep<T,sequential>    base;
    typedef typename base::size_type        size_type;

// allocators:

    space_rep ( const geo_basic<T,sequential>&    omega,
      		std::string                       approx,
      		std::string                       prod_valued);
    space_rep ( const geo_basic<T,sequential>&    omega,
	        const basis_basic<T>&             b);
    space_rep (const space_constitution<T,sequential>& constit);
    space_rep (const space_mult_list<T,sequential>&);
    ~space_rep () {}

// for compatibility with the distributed interface:

    bool             dis_is_blocked (size_type dis_idof) const { return base::is_blocked(dis_idof); }
    size_type      dis_idof2dis_iub (size_type dis_idof) const { return base::dis_iub   (dis_idof); }

    const distributor& ios_ownership() const { return base::ownership(); }
    size_type     idof2ios_dis_idof (size_type     idof) const { return idof; }
    size_type     ios_idof2dis_idof (size_type ios_idof) const { return ios_idof; }

    // for compatibility with the distributed case:
    const std::set<size_type>& ext_iu_set() const;
    const std::set<size_type>& ext_ib_set() const;
};
// ---------------------------------------------------------------------
#ifdef _RHEOLEF_HAVE_MPI
template <class T>
class space_rep<T,distributed> : public space_base_rep<T,distributed> {
public:

// typedefs:

    typedef space_base_rep<T,distributed> base;
    typedef typename base::size_type size_type;

// allocators:

    space_rep ( const geo_basic<T,distributed>&   omega,
      		std::string                       approx,
      		std::string                       prod_valued);
    space_rep ( const geo_basic<T,distributed>&   omega,
	        const basis_basic<T>&             b);
    space_rep (const space_constitution<T,distributed>& constit);
    space_rep (const space_mult_list<T,distributed>&);
    ~space_rep () {}

// accessors:

    const communicator&      comm() const { return base::comm(); }

    bool             dis_is_blocked (size_type dis_idof) const;
    size_type      dis_idof2dis_iub (size_type dis_idof) const;

    const distributor& ios_ownership() const { return _ios_idof2dis_idof.ownership(); }
    size_type     idof2ios_dis_idof (size_type     idof) const { base::freeze_guard(); return _idof2ios_dis_idof [idof]; }
    size_type     ios_idof2dis_idof (size_type ios_idof) const { base::freeze_guard(); return _ios_idof2dis_idof [ios_idof]; }
    const std::set<size_type>& ext_iu_set() const { return _ext_iu_set; }
    const std::set<size_type>& ext_ib_set() const { return _ext_ib_set; }

protected:
    template <class T1, class M1> friend class field_basic;
// internal procedures:
    void freeze_body() const;
    void append_external_dof (const geo_basic<T,distributed>& dom, std::set<size_type>& ext_dof_set) const;
// data:
    disarray<size_type,distributed>   _idof2ios_dis_idof;   // permut to/from ios dof numbering (before geo part), for i/o
    disarray<size_type,distributed>   _ios_idof2dis_idof;
// mutable data, affected by freeze_*()const:
    mutable std::set<size_type>    _ext_iu_set;    // external dofs used by field::dis_dof
    mutable std::set<size_type>    _ext_ib_set;
};
#endif // _RHEOLEF_HAVE_MPI
// ====================================================================
// 2) wrapper class: seq & mpi specializations
// ====================================================================
/// @brief the finite element space
template <class T, class M = rheo_default_memory_model>
class space_basic {
public:
};
// [verbatim_space]
typedef space_basic<Float> space;
// [verbatim_space]
// ---------------------------------------------------------------------
// [verbatim_space_basic]
template <class T>
class space_basic<T,sequential> : public smart_pointer<space_rep<T,sequential> > {
public:

// typedefs:

    typedef space_rep<T,sequential>   rep;
    typedef smart_pointer<rep>        base;
    typedef typename rep::size_type   size_type;
    typedef typename rep::valued_type valued_type;

// allocators:

    space_basic (const geo_basic<T,sequential>& omega = (geo_basic<T,sequential>()),
		 std::string approx                   = "", 
                 std::string prod_valued              = "scalar");
    space_basic (const geo_basic<T,sequential>& omega,
		 const basis_basic<T>&          b);
    space_basic (const space_mult_list<T,sequential>& expr);
    space_basic (const space_constitution<T,sequential>& constit);
    static space_basic<T,sequential> real();

// accessors:

    void block  (std::string dom_name);
    void unblock(std::string dom_name);
    void block  (const domain_indirect_basic<sequential>& dom);
    void unblock(const domain_indirect_basic<sequential>& dom);

    void block_n  (std::string dom_name);
    void unblock_n(std::string dom_name);
    void block_n  (const domain_indirect_basic<sequential>& dom);
    void unblock_n(const domain_indirect_basic<sequential>& dom);

    const distributor&  ownership() const;
    const communicator& comm() const;
    size_type           ndof() const;
    size_type           dis_ndof() const;

    const geo_basic<T,sequential>& get_geo() const;
    const basis_basic<T>& get_basis() const;
    size_type size() const;
    valued_type           valued_tag()   const;
    const std::string&    valued()       const;
    space_component<T,sequential>       operator[] (size_type i_comp);
    space_component_const<T,sequential> operator[] (size_type i_comp) const;
    const space_constitution<T,sequential>& get_constitution() const;
    size_type degree() const;
    std::string get_approx() const;
    std::string name() const;

    void dis_idof (const geo_element& K, std::vector<size_type>& dis_idof) const;

    const distributor& iu_ownership() const;
    const distributor& ib_ownership() const;

    bool            is_blocked (size_type     idof) const;
    size_type          dis_iub (size_type     idof) const;
    bool        dis_is_blocked (size_type dis_idof) const;
    size_type dis_idof2dis_iub (size_type dis_idof) const;

    const distributor& ios_ownership() const;
    size_type idof2ios_dis_idof (size_type idof) const;
    size_type ios_idof2dis_idof (size_type ios_idof) const;

    const point_basic<T>& xdof (size_type idof) const;
    const disarray<point_basic<T>,sequential>& get_xdofs() const;

    template <class Function>
    T momentum (const Function& f, size_type idof) const;

    template <class Function>
    point_basic<T> vector_momentum (const Function& f, size_type idof) const;

    template <class Function>
    tensor_basic<T> tensor_momentum (const Function& f, size_type idof) const;

    disarray<size_type, sequential> build_dom_dis_idof2bgd_dis_idof (
    	const space_basic<T,sequential>& Wh, const std::string& dom_name) const;

    disarray<size_type, sequential> build_dom_dis_idof2bgd_dis_idof (
        const space_basic<T,sequential>& Wh, const geo_basic<T,sequential>& bgd_gamma) const;

    const std::set<size_type>& ext_iu_set() const { return base::data().ext_iu_set(); }
    const std::set<size_type>& ext_ib_set() const { return base::data().ext_ib_set(); }

// comparator:

    bool operator== (const space_basic<T,sequential>& V2) const { return base::data().operator==(V2.data()); }
    bool operator!= (const space_basic<T,sequential>& V2) const { return ! operator== (V2); }
    friend bool are_compatible (const space_basic<T,sequential>& V1, const space_basic<T,sequential>& V2) {
	return are_compatible (V1.data(), V2.data()); }

// for lazy (un-assembled) on geo_domains:

    template <class Set>
    void get_parent_subgeo_owner_dis_indexes (Set& ext_dis_idofs) const
        {        base::data().get_parent_subgeo_owner_dis_indexes (ext_dis_idofs); }
    size_type get_parent_subgeo_owner (size_type dis_idof) const
        { return base::data().get_parent_subgeo_owner (dis_idof); }
};
// [verbatim_space_basic]
template<class T>
inline
space_basic<T,sequential>::space_basic (
    const geo_basic<T,sequential>& omega,
    std::string                    approx,
    std::string                    prod_valued)
 : base (new_macro(rep(omega, approx, prod_valued)))
{
}
template<class T>
inline
space_basic<T,sequential>::space_basic (
    const geo_basic<T,sequential>& omega,
    const basis_basic<T>&          b)
 : base (new_macro(rep(omega, b)))
{
}
template<class T>
inline
space_basic<T,sequential>::space_basic (
    const space_constitution<T,sequential>& constit)
 : base (new_macro(rep(constit)))
{
}
template<class T>
inline
space_basic<T,sequential>::space_basic (const space_mult_list<T,sequential>& expr)
 : base (new_macro(rep(expr)))
{
}
template<class T>
inline
const distributor&
space_basic<T,sequential>::ownership() const
{
    return base::data().ownership();
}
template<class T>
inline
const distributor&
space_basic<T,sequential>::ios_ownership() const
{
    return base::data().ios_ownership();
}
template<class T>
inline
const communicator&
space_basic<T,sequential>::comm() const
{
    return base::data().comm();
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::ndof() const
{
    return base::data().ndof();
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::dis_ndof() const
{
    return base::data().dis_ndof();
}
template<class T>
inline
const geo_basic<T,sequential>&
space_basic<T,sequential>::get_geo() const
{
    return base::data().get_geo();
}
template<class T>
inline
const basis_basic<T>&
space_basic<T,sequential>::get_basis() const
{
    return base::data().get_basis();
}
template<class T>
inline
const space_constitution<T,sequential>&
space_basic<T,sequential>::get_constitution() const
{
    return base::data().get_constitution();
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::size() const
{
    return base::data().size();
}
template<class T>
inline
const std::string&
space_basic<T,sequential>::valued() const
{
    return base::data().valued();
}
template<class T>
inline
typename space_basic<T,sequential>::valued_type
space_basic<T,sequential>::valued_tag() const
{
    return base::data().valued_tag();
}
template<class T>
inline
space_component<T,sequential>
space_basic<T,sequential>::operator[] (size_type i_comp)
{
    return base::data().operator[] (i_comp);
}
template<class T>
inline
space_component_const<T,sequential>
space_basic<T,sequential>::operator[] (size_type i_comp) const
{
    return base::data().operator[] (i_comp);
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::degree() const
{
    return get_basis().degree();
}
template<class T>
inline
std::string
space_basic<T,sequential>::get_approx() const
{
    return get_basis().name();
}
template<class T>
inline
std::string
space_basic<T,sequential>::name() const
{
    return base::data().name();
}
template<class T>
inline
void
space_basic<T,sequential>::dis_idof (const geo_element& K, std::vector<size_type>& dis_idof) const
{
    return base::data().dis_idof (K, dis_idof);
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::idof2ios_dis_idof (size_type idof) const
{
    return base::data().idof2ios_dis_idof (idof);
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::ios_idof2dis_idof (size_type ios_idof) const
{
    return base::data().ios_idof2dis_idof (ios_idof);
}
template<class T>
inline
const distributor&
space_basic<T,sequential>::iu_ownership() const
{
    return base::data().iu_ownership();
}
template<class T>
inline
const distributor&
space_basic<T,sequential>::ib_ownership() const
{
    return base::data().ib_ownership();
}
template<class T>
inline
bool
space_basic<T,sequential>::is_blocked (size_type idof) const
{
    return base::data().is_blocked (idof);
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::dis_iub (size_type idof) const
{
    return base::data().dis_iub (idof);
}
template<class T>
inline
bool
space_basic<T,sequential>::dis_is_blocked (size_type dis_idof) const
{
    return base::data().dis_is_blocked (dis_idof);
}
template<class T>
inline
typename space_basic<T,sequential>::size_type
space_basic<T,sequential>::dis_idof2dis_iub (size_type dis_idof) const
{
    return base::data().dis_idof2dis_iub (dis_idof);
}
template<class T>
inline
void
space_basic<T,sequential>::block (std::string dom_name)
{
    return base::data().block (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,sequential>::unblock (std::string dom_name)
{
    return base::data().unblock (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,sequential>::block (const domain_indirect_basic<sequential>& dom)
{
    return base::data().block (dom);
}
template<class T>
inline
void
space_basic<T,sequential>::unblock (const domain_indirect_basic<sequential>& dom)
{
    return base::data().unblock (dom);
}
template<class T>
inline
void
space_basic<T,sequential>::block_n (std::string dom_name)
{
    return base::data().block_n (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,sequential>::unblock_n (std::string dom_name)
{
    return base::data().unblock_n (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,sequential>::block_n (const domain_indirect_basic<sequential>& dom)
{
    return base::data().block_n (dom);
}
template<class T>
inline
void
space_basic<T,sequential>::unblock_n (const domain_indirect_basic<sequential>& dom)
{
    return base::data().unblock_n (dom);
}
template<class T>
inline
const point_basic<T>&
space_basic<T,sequential>::xdof (size_type idof) const
{
    return base::data().xdof (idof);
}
template<class T>
inline
const disarray<point_basic<T>,sequential>&
space_basic<T,sequential>::get_xdofs() const
{
    return base::data().get_xdofs();
}
template<class T>
template <class Function>
inline
T
space_basic<T,sequential>::momentum (const Function& f, size_type idof) const
{
    return base::data().momentum (f, idof);
}
template<class T>
template <class Function>
inline
point_basic<T>
space_basic<T,sequential>::vector_momentum (const Function& f, size_type idof) const
{
    return base::data().vector_momentum (f, idof);
}
template<class T>
template <class Function>
tensor_basic<T>
space_basic<T,sequential>::tensor_momentum (const Function& f, size_type idof) const
{
    return base::data().tensor_momentum (f, idof);
}

// ---------------------------------------------------------------------
#ifdef _RHEOLEF_HAVE_MPI
//<verbatim:
template <class T>
class space_basic<T,distributed> : public smart_pointer<space_rep<T,distributed> > {
public:

// typedefs:

    typedef space_rep<T,distributed>  rep;
    typedef smart_pointer<rep>        base;
    typedef typename rep::size_type   size_type;
    typedef typename rep::valued_type valued_type;

// allocators:

    space_basic (const geo_basic<T,distributed>& omega = (geo_basic<T,distributed>()),
		 std::string approx                   = "", 
                 std::string prod_valued              = "scalar");
    space_basic (const geo_basic<T,distributed>& omega,
		 const basis_basic<T>&           b);
    space_basic (const space_mult_list<T,distributed>&);
    space_basic (const space_constitution<T,distributed>& constit);
    static space_basic<T,distributed> real();

// accessors:

    void block  (std::string dom_name);
    void unblock(std::string dom_name);
    void block  (const domain_indirect_basic<distributed>& dom);
    void unblock(const domain_indirect_basic<distributed>& dom);

    void block_n  (std::string dom_name);
    void unblock_n(std::string dom_name);
    void block_n  (const domain_indirect_basic<distributed>& dom);
    void unblock_n(const domain_indirect_basic<distributed>& dom);

    const distributor&  ownership() const;
    const communicator& comm() const;
    size_type           ndof() const;
    size_type           dis_ndof() const;

    const geo_basic<T,distributed>& get_geo() const;
    const basis_basic<T>& get_basis() const;
    size_type size() const;
    valued_type           valued_tag()   const;
    const std::string&    valued()       const;
    space_component<T,distributed>       operator[] (size_type i_comp);
    space_component_const<T,distributed> operator[] (size_type i_comp) const;
    const space_constitution<T,distributed>& get_constitution() const;
    size_type degree() const;
    std::string get_approx() const;
    std::string name() const;

    void dis_idof (const geo_element& K, std::vector<size_type>& dis_idof) const;

    const distributor& iu_ownership() const;
    const distributor& ib_ownership() const;

    bool      is_blocked (size_type     idof) const;
    size_type    dis_iub (size_type     idof) const;

    bool        dis_is_blocked (size_type dis_idof) const;
    size_type dis_idof2dis_iub (size_type dis_idof) const;

    const distributor& ios_ownership() const;
    size_type idof2ios_dis_idof (size_type idof) const;
    size_type ios_idof2dis_idof (size_type ios_idof) const;

    const point_basic<T>& xdof (size_type idof) const;
    const disarray<point_basic<T>,distributed>& get_xdofs() const;

    template <class Function>
    T momentum (const Function& f, size_type idof) const;

    template <class Function>
    point_basic<T> vector_momentum (const Function& f, size_type idof) const;

    template <class Function>
    tensor_basic<T> tensor_momentum (const Function& f, size_type idof) const;

    disarray<size_type, distributed> build_dom_dis_idof2bgd_dis_idof (
    	const space_basic<T,distributed>& Wh, const std::string& dom_name) const;

    disarray<size_type, distributed> build_dom_dis_idof2bgd_dis_idof (
        const space_basic<T,distributed>& Wh, const geo_basic<T,distributed>& bgd_gamma) const;

    const std::set<size_type>& ext_iu_set() const { return base::data().ext_iu_set(); }
    const std::set<size_type>& ext_ib_set() const { return base::data().ext_ib_set(); }

// comparator:

    bool operator== (const space_basic<T,distributed>& V2) const { return base::data().operator==(V2.data()); }
    bool operator!= (const space_basic<T,distributed>& V2) const { return ! operator== (V2); }
    friend bool are_compatible (const space_basic<T,distributed>& V1, const space_basic<T,distributed>& V2) {
	return are_compatible (V1.data(), V2.data()); }

// for lazy (un-assembled) on geo_domains:

    template <class Set>
    void get_parent_subgeo_owner_dis_indexes (Set& ext_dis_idofs) const
        {        base::data().get_parent_subgeo_owner_dis_indexes (ext_dis_idofs); }
    size_type get_parent_subgeo_owner (size_type dis_idof) const
        { return base::data().get_parent_subgeo_owner (dis_idof); }
};
//>verbatim:

template<class T>
inline
space_basic<T,distributed>::space_basic (
    const geo_basic<T,distributed>& omega,
    std::string                    approx,
    std::string                    prod_valued)
 : base (new_macro(rep(omega, approx, prod_valued)))
{
}
template<class T>
inline
space_basic<T,distributed>::space_basic (
    const geo_basic<T,distributed>& omega,
    const basis_basic<T>&          b)
 : base (new_macro(rep(omega, b)))
{
}
template<class T>
inline
space_basic<T,distributed>::space_basic (
    const space_constitution<T,distributed>& constit)
 : base (new_macro(rep(constit)))
{
}
template<class T>
inline
space_basic<T,distributed>::space_basic (const space_mult_list<T,distributed>& expr)
 : base (new_macro(rep(expr)))
{
}
template<class T>
inline
const distributor&
space_basic<T,distributed>::ownership() const
{
    return base::data().ownership();
}
template<class T>
inline
const distributor&
space_basic<T,distributed>::ios_ownership() const
{
    return base::data().ios_ownership();
}
template<class T>
inline
const communicator&
space_basic<T,distributed>::comm() const
{
    return base::data().comm();
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::ndof() const
{
    return base::data().ndof();
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::dis_ndof() const
{
    return base::data().dis_ndof();
}
template<class T>
inline
const geo_basic<T,distributed>&
space_basic<T,distributed>::get_geo() const
{
    return base::data().get_geo();
}
template<class T>
inline
const basis_basic<T>&
space_basic<T,distributed>::get_basis() const
{
    return base::data().get_basis();
}
template<class T>
inline
const space_constitution<T,distributed>&
space_basic<T,distributed>::get_constitution() const
{
    return base::data().get_constitution();
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::size() const
{
    return base::data().size();
}
template<class T>
inline
const std::string&
space_basic<T,distributed>::valued() const
{
    return base::data().valued();
}
template<class T>
inline
typename space_basic<T,distributed>::valued_type
space_basic<T,distributed>::valued_tag() const
{
    return base::data().valued_tag();
}
template<class T>
inline
space_component<T,distributed>
space_basic<T,distributed>::operator[] (size_type i_comp)
{
    return base::data().operator[] (i_comp);
}
template<class T>
inline
space_component_const<T,distributed>
space_basic<T,distributed>::operator[] (size_type i_comp) const
{
    return base::data().operator[] (i_comp);
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::degree() const
{
    return get_basis().degree();
}
template<class T>
inline
std::string
space_basic<T,distributed>::get_approx() const
{
    return get_basis().name();
}
template<class T>
inline
std::string
space_basic<T,distributed>::name() const
{
    return base::data().name();
}
template<class T>
inline
void
space_basic<T,distributed>::dis_idof (const geo_element& K, std::vector<size_type>& dis_idof) const
{
    return base::data().dis_idof (K, dis_idof);
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::idof2ios_dis_idof (size_type idof) const
{
    return base::data().idof2ios_dis_idof (idof);
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::ios_idof2dis_idof (size_type ios_idof) const
{
    return base::data().ios_idof2dis_idof (ios_idof);
}
template<class T>
inline
const distributor&
space_basic<T,distributed>::iu_ownership() const
{
    return base::data().iu_ownership();
}
template<class T>
inline
const distributor&
space_basic<T,distributed>::ib_ownership() const
{
    return base::data().ib_ownership();
}
template<class T>
inline
bool
space_basic<T,distributed>::is_blocked (size_type idof) const
{
    return base::data().is_blocked (idof);
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::dis_iub (size_type idof) const
{
    return base::data().dis_iub (idof);
}
template<class T>
inline
bool
space_basic<T,distributed>::dis_is_blocked (size_type dis_idof) const
{
    return base::data().dis_is_blocked (dis_idof);
}
template<class T>
inline
typename space_basic<T,distributed>::size_type
space_basic<T,distributed>::dis_idof2dis_iub (size_type dis_idof) const
{
    return base::data().dis_idof2dis_iub (dis_idof);
}
template<class T>
inline
void
space_basic<T,distributed>::block (std::string dom_name)
{
    return base::data().block (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,distributed>::unblock (std::string dom_name)
{
    return base::data().unblock (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,distributed>::block (const domain_indirect_basic<distributed>& dom)
{
    base::data().block (dom);
}
template<class T>
inline
void
space_basic<T,distributed>::unblock (const domain_indirect_basic<distributed>& dom)
{
    base::data().unblock (dom);
}
template<class T>
inline
void
space_basic<T,distributed>::block_n (std::string dom_name)
{
    return base::data().block_n (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,distributed>::unblock_n (std::string dom_name)
{
    return base::data().unblock_n (get_geo().get_domain_indirect(dom_name));
}
template<class T>
inline
void
space_basic<T,distributed>::block_n (const domain_indirect_basic<distributed>& dom)
{
    base::data().block_n (dom);
}
template<class T>
inline
void
space_basic<T,distributed>::unblock_n (const domain_indirect_basic<distributed>& dom)
{
    base::data().unblock_n (dom);
}
template<class T>
inline
const point_basic<T>&
space_basic<T,distributed>::xdof (size_type idof) const
{
    return base::data().xdof (idof);
}
template<class T>
inline
const disarray<point_basic<T>,distributed>&
space_basic<T,distributed>::get_xdofs() const
{
    return base::data().get_xdofs();
}
template<class T>
template <class Function>
inline
T
space_basic<T,distributed>::momentum (const Function& f, size_type idof) const
{
    return base::data().momentum (f, idof);
}
template<class T>
template <class Function>
inline
point_basic<T>
space_basic<T,distributed>::vector_momentum (const Function& f, size_type idof) const
{
    return base::data().vector_momentum (f, idof);
}
template<class T>
template <class Function>
tensor_basic<T>
space_basic<T,distributed>::tensor_momentum (const Function& f, size_type idof) const
{
    return base::data().tensor_momentum (f, idof);
}
#endif // _RHEOLEF_HAVE_MPI

// only valid when M=sequential or M=distributed => use a macro
#define _RHEOLEF_space_build_dom_dis_idof2bgd_dis_idof(M)						\
template<class T>									\
inline											\
disarray<typename space_basic<T,M>::size_type, M>					\
space_basic<T,M>::build_dom_dis_idof2bgd_dis_idof (						\
	const space_basic<T,M>& Wh,							\
	const std::string& dom_name) const						\
{											\
    return base::data().build_dom_dis_idof2bgd_dis_idof (Wh.data(), dom_name);			\
}											\
template<class T>									\
inline											\
disarray<typename space_basic<T,M>::size_type, M>					\
space_basic<T,M>::build_dom_dis_idof2bgd_dis_idof (						\
	const space_basic<T,M>& Wh,							\
	const geo_basic<T,M>&   bgd_gamma) const					\
{											\
    return base::data().build_dom_dis_idof2bgd_dis_idof (Wh.data(), bgd_gamma);			\
}
_RHEOLEF_space_build_dom_dis_idof2bgd_dis_idof(sequential)

#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_space_build_dom_dis_idof2bgd_dis_idof(distributed)
#endif // _RHEOLEF_HAVE_MPI

#undef _RHEOLEF_space_build_dom_dis_idof2bgd_dis_idof

} // namespace rheolef
#endif // _RHEOLEF_SPACE_H
