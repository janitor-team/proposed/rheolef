///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo_header.h"

namespace rheolef {

static
const char* label_variant [] = {
  "nodes",
  "edges",
  "triangles",
  "quadrangles",
  "tetrahedra",
  "prisms",
  "hexahedra"
};
idiststream&
operator>> (idiststream& ips, geo_header& h) 
{
  typedef geo_header::size_type size_type;
  std::string label;
  ips >> label;
  check_macro (label == "header", "geo file format version 4: \"header\" keyword not found");
  while (ips.good()) {
    size_type value;
    ips >> label;
    if (label == "end") break;
         if (label == "dimension") { ips >> h.dimension; }
    else if (label == "order")     { ips >> h.order; }
    else if (label == "coordinate_system") {
        std::string sys_coord_name;
        ips >> sys_coord_name;
        h.sys_coord = space_constant::coordinate_system (sys_coord_name);
    }
    else {
      size_type variant = 0;
      for (; variant < reference_element::max_variant; variant++) {
	if (label == label_variant[variant]) break;
      }
      check_macro (variant < reference_element::max_variant, "unexpected header member: \""<<label<<"\"");
      ips >> h.dis_size_by_variant [variant];
    }
  }
  ips >> label;
  check_macro (label == "header", "geo file format version 4: \"end header\" keyword not found");
  // build also size_by_dimension (useful)
  for (size_type dim = 0; dim < 4; dim++) {
    h.dis_size_by_dimension [dim] = 0;
    for (size_type variant = reference_element::first_variant_by_dimension(dim);
                   variant < reference_element:: last_variant_by_dimension(dim); variant++) {
      h.dis_size_by_dimension [dim] += h.dis_size_by_variant [variant];
    }
  }
  // compute also map_dimension = max dimension with non-empty geo_element set
  for (h.map_dimension = h.dimension; h.map_dimension != 0; h.map_dimension--) {
    if (h.dis_size_by_dimension [h.map_dimension] != 0) break;
  }
  return ips;
}
bool
geo_header::need_upgrade () const
{
  return (map_dimension > 1) && (dis_size_by_dimension [map_dimension] > 0) && (dis_size_by_dimension[1] == 0);
}
odiststream&
operator<< (odiststream& ops, const geo_header& h) 
{
  using namespace std;
  typedef geo_header::size_type size_type;
  ops << "header" << endl
      << " dimension " << h.dimension << endl;
  if (h.sys_coord != space_constant::cartesian) {
    ops << " coordinate_system " << space_constant::coordinate_system_name(h.sys_coord) << endl;
  }
  if (h.order != 1) {
    ops << " order     " << h.order << endl;
  }
  ops << " " << label_variant [0] << "\t" << h.dis_size_by_variant[0] << endl;
  for (size_type dim = 3; dim > 0; dim--) {
    for (size_type variant = reference_element::first_variant_by_dimension(dim);
                   variant < reference_element:: last_variant_by_dimension(dim); variant++) {
      if (h.dis_size_by_variant[variant] != 0) {
        ops << " " << label_variant [variant] << "\t" << h.dis_size_by_variant[variant] << endl;
      }
    }
  }
  ops << "end header" << endl;
  return ops;
}

} // namespace rheolef
