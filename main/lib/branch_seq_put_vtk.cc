///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// vtk for paraview animations
//
// author: Pierre.Saramito@imag.fr
//
// date: 4 may 2001
//
#include "rheolef/branch.h"
#include "rheolef/iofem.h"
#include "rheolef/iorheo.h"
#include "rheolef/rheostream.h"
namespace rheolef {
using namespace std;

// extern:
template <class T> odiststream& field_put_vtk (odiststream&, const field_basic<T,sequential>&, std::string, bool);

template<class T>
void
put_header_vtk (odiststream& out, const branch_basic<T,sequential>& b)
{
  ostream& os = out.os();
  iorheo::setbranch_counter(os, 0);
  b._count_value = 0;
}
// put data on a vtk stream
template<class T>
void 
put_event_vtk_stream (odiststream& out_vtk, const branch_basic<T,sequential>& b)
{
    string basename = iorheo::getbasename(out_vtk.os());
    out_vtk << setbasename(basename)
            << setprecision(numeric_limits<T>::digits10);
    string approx0;
    bool put_geo = true;
    for (size_t i = 0; i < b.n_field(); i++) {
      if (i == 0) {
        const field_basic<T,sequential>& u0 = b[0].second;
        basis b0 = u0.get_space().get_basis();
        approx0 = (! b0.is_hierarchical()) ? b0.name() : b0[0].name();
      }
      const string&                     name = b[i].first;
      const field_basic<T,sequential>&  ui   = b[i].second;
      
      basis bi = ui.get_space().get_basis();
      string approx = (! bi.is_hierarchical()) ? bi.name() : bi[0].name();
      if (approx != approx0) {
	warning_macro("field #"<<i<<" name '"<<name<<": approx `"<<approx<<"' incompatible with field #0 approx `"<<approx0<<"'");
        error_macro ("HINT: use the \"branch -proj\" option");
      }
      field_put_vtk (out_vtk, ui, name, put_geo);
      put_geo = false;
    }
}
// open the stream for a vtk file & put data on it
template<class T>
void 
put_event_vtk (odiststream& out, const branch_basic<T,sequential>& b)
{
    ostream& os = out.os();
    string basename = iorheo::getbasename(os);
    if (basename == "") basename = "output";
    string data_file_name = basename + "-" + std::to_string(b._count_value) + ".vtk";
    ofstream vtk (data_file_name.c_str());
    odiststream out_vtk (vtk);
    bool verbose = iorheo::getverbose(clog);
    verbose && clog << "! file `" << data_file_name << "' created" << endl;
    put_event_vtk_stream (out_vtk, b);
    vtk.close();
}
template<class T>
void 
put_finalize_vtk (odiststream& out, const branch_basic<T,sequential>& b)
{
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template void put_header_vtk      (odiststream&, const branch_basic<Float,sequential>&);
template void put_event_vtk       (odiststream&, const branch_basic<Float,sequential>&);
template void put_event_vtk_stream(odiststream&, const branch_basic<Float,sequential>&);
template void put_finalize_vtk    (odiststream&, const branch_basic<Float,sequential>&);

} // namespace rheolef
