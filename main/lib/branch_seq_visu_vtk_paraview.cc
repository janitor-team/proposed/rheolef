///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// animations using paraview
//
// author: Pierre.Saramito@imag.fr
//
// date: 25 janv 2020
//
#include "rheolef/branch.h"
#include "rheolef/field_expr.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/iofem.h"
#include "rheolef/interpolate.h"
#include "rheolef/render_option.h"

namespace rheolef {
using namespace std;

// extern: in "field_seq_visu_vtk_paraview.cc"
template<class T> std::string python (const point_basic<T>& x, size_t d=3);

template<class T>
void
put_header_paraview (odiststream& out, const branch_basic<T,sequential>& b)
{
  ostream& os = out.os();
  iorheo::setbranch_counter(os, 0);
  b._u_range = std::make_pair( std::numeric_limits<T>::max(),
                              -std::numeric_limits<T>::max());
  b._have_u_range = std::make_pair(false,false);
}
template<class T>
void 
put_event_paraview (odiststream& out, const branch_basic<T,sequential>& b)
{
  ostream& os = out.os();
  // ---------------
  // update _u_range
  // ---------------
  size_t idx = 0;
  string mark = iorheo::getmark(os);
  if (mark == "") {
    mark = b[0].first;
  } else {
    for (; idx < b.n_field(); ++idx) if (mark == b[idx].first) break;
    check_macro (idx < b.n_field(), "mark `" << mark << "' not found in branch");
  }
  const field_basic<T,sequential>& uh = b[idx].second;
  b._u_range.first  = std::min (b._u_range.first,  uh.min());
  b._u_range.second = std::max (b._u_range.second, uh.max());
  // ---------------
  // put vtk
  // ---------------
  size_t branch_counter = iorheo::getbranch_counter(os);
  iorheo::setbranch_counter(os, branch_counter+1);
  bool skipvtk = iorheo::getskipvtk(os);
  if (skipvtk) return;
  bool clean = iorheo::getclean(os);
  string basename = iorheo::getbasename(os);
  if (basename == "") basename = "output";
  string tmp = get_tmpdir() + "/";
  if (!clean) tmp = "";
  string data_file_name = tmp + basename + "-" + std::to_string(b._count_value) + ".vtk";
  ofstream vtk (data_file_name.c_str());
  odiststream out_vtk (vtk);
  bool verbose = iorheo::getverbose(clog);
  verbose && clog << "! file `" << data_file_name << "' created" << endl;
  put_event_vtk_stream (out_vtk, b);
  vtk.close();
}
// ----------------------------------------------------------------------------
// paraview render call
// ----------------------------------------------------------------------------
template<class T>
void 
put_finalize_paraview (odiststream& out, const branch_basic<T,sequential>& b)
{
  // TODO: 99% of code similar with field_seq_visu_vtk_paraview.cc => merge
  // also support anim for vector & tensor
  //
  // 0) prerequises
  //
  using namespace std;
  typedef typename field_basic<T,sequential>::float_type float_type;
  typedef typename geo_basic<float_type,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  if (b.n_field() == 0) {
    warning_macro ("empty branch: animation skipped");
    return;
  }
  ostream& os = out.os();
  //
  // 1) set render options
  //
  render_option popt;
  popt.mark = iorheo::getmark(os);
  size_t idx = 0;
  if (popt.mark == "") {
    popt.mark = b[0].first;
  } else {
    for (; idx < b.n_field(); ++idx) if (popt.mark == b[idx].first) break;
    check_macro (idx < b.n_field(), "mark `" << popt.mark << "' not found in branch");
  }
  const field_basic<T,sequential>& uh = b[idx].second;
  string valued = uh.get_space().valued();
  bool is_scalar = (valued == "scalar");
  // if (popt.mark != "" && !is_scalar) popt.mark = "|"+popt.mark +"|";
  popt.fill      = iorheo::getfill(os);    // isocontours or color fill
  popt.elevation = iorheo::getelevation(os);
  popt.color   = iorheo::getcolor(os);
  popt.gray    = iorheo::getgray(os);
  popt.black_and_white = iorheo::getblack_and_white(os);
  popt.showlabel = iorheo::getshowlabel(os);
  popt.stereo    = iorheo::getstereo(os);
  popt.volume    = iorheo::getvolume(os);
  popt.iso       = true;
  popt.cut       = iorheo::getcut(os);
  popt.grid      = iorheo::getgrid(os);
  popt.format    = iorheo::getimage_format(os);
  popt.scale     = iorheo::getvectorscale(os);
  popt.origin    = iofem::getorigin(os);
  popt.normal    = iofem::getnormal(os);
  popt.resolution          = iofem::getresolution(os);
  popt.n_isovalue          = iorheo::getn_isovalue(os);
  popt.n_isovalue_negative = iorheo::getn_isovalue_negative(os);
  popt.isovalue  = iorheo::getisovalue(os); // isovalue is always a Float
  popt.label     = iorheo::getlabel(os);

  const geo_basic<float_type,sequential>& omega = uh.get_geo();
  size_type dim     = omega.dimension();
  size_type map_dim = omega.map_dimension();
  popt.view_2d      = (dim == 2);
  popt.view_map     = (dim > map_dim);
#if (_RHEOLEF_PARAVIEW_VERSION_MAJOR >= 5) && (_RHEOLEF_PARAVIEW_VERSION_MINOR >= 5)
  // paraview version >= 5.5 has high order elements
  popt.high_order = (omega.order() > 1 || uh.get_space().degree() > 1);
#else
  popt.high_order = false;
#endif
  popt.xmin = uh.get_geo().xmin();
  popt.xmax = uh.get_geo().xmax();
  popt.branch_size = iorheo::getbranch_counter(os);
  // min and max over all the branch
  popt.f_min = b._u_range.first;
  popt.f_max = b._u_range.second;
  //
  // 2) create python data file
  //
  bool verbose  = iorheo::getverbose(os);
  bool clean    = iorheo::getclean(os);
  bool execute  = iorheo::getexecute(os);
  bool skipvtk   = iorheo::getskipvtk(os);
  string tmp = get_tmpdir() + "/";
  if (skipvtk || !clean) tmp = "";
  string clean_filelist;
  string basename = iorheo::getbasename(os);
  if (basename == "") basename = "output";
  std::string py_name = tmp+basename + ".py";
  clean_filelist = clean_filelist + " " + py_name;
  ofstream py (py_name.c_str());
  if (verbose) clog << "! file \"" << py_name << "\" created.\n";
  py << popt
     << "filelist = [";
  for (size_type i = 0; i < popt.branch_size; ++i) {
    string i_vtk_name = tmp + basename + "-" + std::to_string(i) + ".vtk";
    string opt_coma = (i+1 != popt.branch_size) ? ", " : "";
    py << "\"" << i_vtk_name + "\"" + opt_coma;
    if (!skipvtk) clean_filelist = clean_filelist + " " + i_vtk_name;
  }
  py << "]" << endl
     << endl
     << "paraview_branch_" << valued << "(paraview, \"" << tmp+basename << "\", filelist, opt)" << endl
     << endl
     ;
  py.close();
  //
  // 3) run pyton
  //
  int status = 0;
  string command;
  if (execute) {
    string prog = (popt.format == "") ? "paraview --script=" : "pvbatch --use-offscreen-rendering ";
    command = "LANG=C PYTHONPATH=" + string(_RHEOLEF_PKGDATADIR) + " " + prog + py_name;
    if (popt.format != "") command = "DISPLAY=:0.0 " + command;
    if (popt.stereo && popt.format == "") command = command + " --stereo";
    if (verbose) clog << "! " << command << endl;
    status = system (command.c_str());
  }
  //
  // 4) clear vtk and py files
  //
  if (clean) {
      command = "/bin/rm -f " + clean_filelist;
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
template void put_header_paraview   (odiststream&, const branch_basic<Float,sequential>&);
template void put_event_paraview    (odiststream&, const branch_basic<Float,sequential>&);
template void put_finalize_paraview (odiststream&, const branch_basic<Float,sequential>&);

} // namespace rheolef
