#ifndef _RHEOLEF_FIELD_FUNCTOR_H
#define _RHEOLEF_FIELD_FUNCTOR_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// ==========================================================================
// 
// field_vf: template expressions
//
// author: Pierre.Saramito@imag.fr
//
// date: 20 march 2011
//
#include "rheolef/point.h"
#include <functional>         // for std::function

namespace rheolef {

/*Class:field_functor
NAME:  @code{field_functor} - a functor wrapper suitable for field expressions (obsolete)
DESCRIPTION:       
  @noindent
  This class is now obsolete, from Rheolef version 6.7
  and is maintained for backward compatibility purpose only.
  Until Rheolef version 6.6, this class was used
  to mark functors with profil compatible with fields, i.e. that accepts
  @code{point} as parameter and returns a field value (scalar, vector, tensor).
  This mark was used to filter field expression arguments in @code{interpolate}
  and @code{integrate}.
  From version 6.7, this mark is no more required, and any function or functor
  that is callable with a @code{point} as argument is valid in a field expression.

  @noindent
  A functor is a class-function, i.e. a class that defines
  the @code{operator()}. A variable @code{f} of a class-function can be
  used as @code{f(arg)} and when its argument is of type @code{point}
  @pxref{point class}, the function @code{f} interprets as a continuous field field.
  Thus, it can be interpolated @pxref{interpolate algorithm} and
  it can be combined within field expressions @pxref{field class}
  that appears in arguments of @pxref{integrate algorithm}.
EXAMPLE:
  @example
    struct f : field_functor<f,Float> @{
      Float operator() (const point& x) const @{ return 1-norm(x); @}
    @};
    // ...
    geo omega ("square");
    space Xh (omega, "P1");
    field fh = interpolate (Xh, f);
    test  (Xh);
    field lh = integrate (f*v);
  @end example
IMPLEMENTATION NOTE:
  The current implementation of a @code{field_functor} class
  bases on the curiously recurring template pattern (CRTP) C++ idiom:
  the definition of the class @code{f} derives from
  @code{field_functor}<@code{f},Float> that depend itself upon @code{f}.
  So, be carrefull when using copy-paste, as there is no checks if
  you write e.g. @code{field_functor}<@code{g},Float> with another function @code{g}
  instead of @code{f}.
AUTHOR: Pierre.Saramito@imag.fr
DATE:   12 march 2013
End:
*/
//<doc:
template <class Function, class Result>
struct field_functor
  : std::unary_function<point_basic<float_traits<Result> >,Result> {
 const Function& get_ref() const { return static_cast<const Function&>(*this); }
 operator Function() const { return get_ref(); }
 Result operator() (const point& x) const { return get_ref().operator()(x); }
};
//>doc:

#ifdef TODO
// transforme a function or a functor into a field_functor, suitable for expressions mixing field and functions
template <class F, class R>
struct field_function_s : field_functor<field_function_s<F,R>, R> {
  typedef typename float_traits<R>::type float_type;
  R operator() (const point_basic<float_type>& x) const { return _f.operator()(x); }
  field_function_s(F f) : _f(f) {}
  F _f;
};
template <class F>
field_function_s<std::function<F>, typename F::result_type>
field_function (F f) {
  typedef typename F::result_type R;
  return field_function_s<function<F>,R>(function<F>(f));
}
#endif // TODO



} // namespace rheolef
#endif // _RHEOLEF_FIELD_FUNCTOR_H
