#ifndef _RHEOLEF_LIMITER_H
#define _RHEOLEF_LIMITER_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   3 october 2015

namespace rheolef {
/**
@functionfile limiter discontinuous Galerkin slope limiter

Synopsis
========
@snippet limiter.h verbatim_limiter

Description
===========
This function returns a slope limited field for any 
supplied discontinuous approximation.

        geo omega ("square");
        space Xh (omega, "P1d");
        field uh (Xh);
        ...
        field vh = limiter(uh);

It implements the minmod_TVB limiter for hyperbolic nonlinear problems
approximated by discontinuous Galerkin methods.
See the @ref usersguide_page for details and examples.

Options
=======
@snippet limiter.h verbatim_limiter_option

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/field.h"
#include "rheolef/test.h"

namespace rheolef {

// [verbatim_limiter_option]
//! @brief see the @ref limiter_3 page for the full documentation
struct limiter_option {
  bool active; 
  Float theta; // > 1, see Coc-1998, P. 209
  Float M;     // M=max|u''(t=0)(x)| at x where u'(t)(x)=0 :extremas
  limiter_option() : active(true), theta(1.5), M(1) {}
};
// [verbatim_limiter_option]
typedef limiter_option limiter_option_type;

// [verbatim_limiter]
template <class T, class M>
field_basic<T,M>
limiter (
   const field_basic<T,M>& uh,
   const T&                bar_g_S = 1.0,
   const limiter_option&   opt = limiter_option());
// [verbatim_limiter]

} // namespace rheolef
#endif // _RHEOLEF_LIMITER_H
