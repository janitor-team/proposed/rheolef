///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/field_wdof_sliced.h" // TODO: should be cleaned after rdof
#include "rheolef/field.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/piola_util.h"
#include "rheolef/field_expr.h"

namespace rheolef {

// ---------------------------------------------------------------------------
// allocators
// ---------------------------------------------------------------------------
// TODO: DVT_EXPR_CTE_VEC : init_value as point or tensor
template <class T, class M>
field_basic<T,M>::field_basic (
	const space_type& V,
	const T& init_value)
 : _V (V), 
   _u (),
   _b (),
   _dis_dof_indexes_requires_update(true),
   _dis_dof_assembly_requires_update(true)
{
   _u.resize (_V.iu_ownership(), init_value);
   _b.resize (_V.ib_ownership(), init_value);
}
template <class T, class M>
void 
field_basic<T,M>::resize (
	const space_type& V,
	const T& init_value)
{
   if (_V == V) return;
   _V = V;
   _u.resize (_V.iu_ownership(), init_value);
   _b.resize (_V.ib_ownership(), init_value);
   dis_dof_indexes_requires_update();
   dis_dof_assembly_requires_update();
}
// ---------------------------------------------------------------------------
// input
// ---------------------------------------------------------------------------
template <class T, class M>
static
void
get_field_recursive (
    idiststream&                   ids,
    vec<T,M>&                      u_io,
    const space_constitution<T,M>& constit,
    distributor::size_type&        comp_start_dis_idof,
    distributor::size_type&        comp_start_ios_idof,
    bool                           read_header = false)
{
  using namespace std;
  typedef distributor::size_type size_type;
  if (! constit.is_hierarchical()) {
    // non-hierarchical case:
    const space_constitution_terminal<T,M>& terminal_constit = constit.get_terminal();
    size_type comp_dis_ndof = constit.dis_ndof();
#ifdef TODO
    // conserve a copy of V[i_comp].ios_ownership()
    const distributor& comp_ios_ownership = terminal_constit.ios_ownership();
    size_type comp_ios_ndof = comp_ios_ownership.size();
#else // TODO
    // recompute V[i_comp].ios_ownership() with comms and risks of errors
    size_type comp_ios_ndof = constit.ios_ndof();
    distributor comp_ios_ownership (comp_dis_ndof, ids.comm(), comp_ios_ndof);
#endif // TODO
    if (read_header) {
      check_macro (dis_scatch(ids, comp_ios_ownership.comm(), "\nfield"), "read field failed");
      size_type      version,  dis_size1;
      std::string    geo_name, approx;
      ids >> version >> dis_size1 >> geo_name >> approx;
      // TODO: some checks here: geo.name, size, approx
    }
    vec<T,M> u_comp_io (comp_ios_ownership, std::numeric_limits<T>::max());
    u_comp_io.get_values (ids);
    size_type ios_ndof = u_io.size();
    for (size_type comp_ios_idof = 0; comp_ios_idof < comp_ios_ndof; comp_ios_idof++) {
      size_type ios_idof = comp_start_ios_idof + comp_ios_idof;
      assert_macro (ios_idof < ios_ndof, "ios_idof="<<ios_idof<<" out of range [0:"<<ios_ndof<<"[");
      u_io [ios_idof] = u_comp_io [comp_ios_idof];
    }
    comp_start_dis_idof += comp_dis_ndof;
    comp_start_ios_idof += comp_ios_ndof;
    return;
  }
  // hierarchical case:
  typedef typename space_constitution<T,M>::hierarchy_type hier_t;
  const hier_t& hier_constit = constit.get_hierarchy();
  for (typename hier_t::const_iterator iter = hier_constit.begin(), last = hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& curr_constit = *iter;
    get_field_recursive (ids, u_io, curr_constit, comp_start_dis_idof, comp_start_ios_idof, true);
  }
}
// extern: defined in space_constitution_old_get.cc
template<class T, class M>
void space_constitution_old_get (idiststream& ids, space_constitution<T,M>& constit);

template <class T, class M>
idiststream& 
field_basic<T,M>::get (idiststream& ids)
{
  using namespace std;
  dis_dof_indexes_requires_update();
  communicator comm = ids.comm();
  if ( ! dis_scatch (ids, comm, "\nfield")) {
    error_macro ("read field failed");
    return ids;
  }
  size_type version;
  ids >> version;
  check_macro (version <= 3, "unexpected field version " << version);
  space_constitution<T,M> constit;
  std::string constit_name_input;
  if (version == 1) {
    // scalar case
    size_type   dis_size1;
    std::string geo_name, approx;
    ids >> dis_size1
        >> geo_name
        >> approx;
    geo_type omega;
    if (_V.get_geo().name() == geo_name) {
      omega = _V.get_geo(); // reuse the previous mesh
    } else {
      omega = geo_type (geo_name); // load a new mesh
    }
    // TODO: get directly "P1" as a space_constitution by: "ids >> constit"; as for the version 2 format
    constit = space_constitution<T,M>(omega, approx);
  } else {
    // version=2,3: multi-field header
    // version == 2 : old format, hierarchical for vector & tensor valued fields
    // version == 3 : new format, hierarchical for only heterogeneous fields based on spaces product
    bool have_constit = false;
    std::string label;
    ids >> label;
    check_macro (label == "header", "field file format version "<< version << ": \"header\" keyword not found");
    while (ids.good()) {
      ids >> label;
      if (label == "end") {
        break;
      } else if (label == "size") {
        size_type dummy_sz;
        ids >> dummy_sz;
      } else if (label == "constitution") {
        ids >> constit_name_input;
        std::istringstream istrstr (constit_name_input);
        idiststream idiststrstr (istrstr);
        if (version == 3) {
          idiststrstr >> constit;
        } else {
          space_constitution_old_get (idiststrstr, constit);
        }
        have_constit = true;
      } else {
        error_macro ("unexpected field header member: \""<<label<<"\"");
      }
    }
    ids >> label;
    check_macro (label == "header", "field file format version "<< version << ": \"end header\" keyword not found");
    check_macro (have_constit, "field file format version "<< version << ": \"constitution\" keyword not found");
  }
  // TODO: do not load mesh when we re-use _V: read only string constit and compare to _V.name()
  if (!(_V.get_constitution() == constit)) {
    // here cannot re-use _V: build a new space and resize the field
    resize (space_type(constit));
  }
  size_type dis_ndof = _V.ownership().dis_size();
  size_type my_proc  = comm.rank();
  size_type io_proc  = ids.io_proc();
  vec<T,M> u_io  (_V.ios_ownership(), std::numeric_limits<T>::max());
  vec<T,M> u_dof (_V.ownership(),     std::numeric_limits<T>::max());
  size_type comp_start_ios_idof = 0;
  size_type comp_start_dis_idof = 0;
  u_io.get_values (ids);
  for (size_type ios_idof = 0, ios_ndof = _V.ios_ownership().size(); ios_idof < ios_ndof; ios_idof++) {
    const T& value = u_io [ios_idof];
    size_type dis_idof = _V.ios_idof2dis_idof (ios_idof);
    u_dof.dis_entry (dis_idof) = value;
  }
  // here dispatch: communications:
  u_dof.dis_entry_assembly();
  // then copy vector into field (unknown, blocked) without comms
  bool need_old2new_convert 
   = (version == 2
	&& constit.valued_tag() != space_constant::scalar
	&& constit.valued_tag() != space_constant::mixed);
  if (!need_old2new_convert) {
    for (size_type idof = 0, ndof = _V.ownership().size(); idof < ndof; idof++) {
      dof(idof) = u_dof [idof];
    }
    return ids;
  }
  // automatically convert vector/tensor to version 3 by grouping components
  std::string valued = constit.valued();
  std::string approx = constit[0].get_basis().name();
  std::string geo_name = constit.get_geo().name();
  size_type n_comp = constit.size();
  std::string new_constit_name_input = valued + "(" + approx + "){" + geo_name + "}";
  std::istringstream new_istrstr (new_constit_name_input);
  idiststream new_idiststrstr (new_istrstr);
  space_constitution<T,M> new_constit;
  new_idiststrstr >> new_constit;
  resize (space_type(new_constit));
  size_type ndof = u_dof.size();
  size_type comp_ndof = ndof/n_comp;
  // convert: new vector/tensor dof numbering have grouped components
  for (size_type i_comp = 0; i_comp < n_comp; i_comp++) {
    for (size_type comp_idof = 0; comp_idof < comp_ndof; comp_idof++) {
      size_type new_idof = comp_idof*n_comp + i_comp;
      size_type old_idof = i_comp*comp_ndof + comp_idof;
      dof(new_idof) = u_dof [old_idof];
    }
  }
  return ids;
}
// ---------------------------------------------------------------------------
// output
// ---------------------------------------------------------------------------
template <class T, class M>
static
void
put_field_recursive (
    odiststream&                   ods,
    const field_basic<T,M>&        uh,
    const space_constitution<T,M>& constit,
    distributor::size_type&        comp_start_idof,
    distributor::size_type&        comp_start_dis_idof,
    bool 			   write_header = false)
{
  using namespace std;
  typedef distributor::size_type size_type;
  if (! constit.is_hierarchical()) {
    // non-hierarchical case:
    // 1) merge distributed blocked and non-blocked and apply iso_dof permutation:
    size_type     comp_ndof = constit.ndof();
    size_type comp_dis_ndof = constit.dis_ndof();
    communicator comm       = constit.comm();
    const space_constitution_terminal<T,M>& terminal_constit = constit.get_terminal();
    size_type io_proc = odiststream::io_proc();
    size_type my_proc = comm.rank();
    distributor comp_ios_ownership (comp_dis_ndof, comm, (my_proc == io_proc ? comp_dis_ndof : 0));
    vec<T,M> comp_u_io (comp_ios_ownership, std::numeric_limits<T>::max());
    for (size_type comp_idof = 0; comp_idof < comp_ndof; comp_idof++) {
      size_type idof = comp_start_idof + comp_idof;
      T value = uh.dof (idof);
      size_type ios_dis_idof = uh.get_space().idof2ios_dis_idof (idof);
      assert_macro (ios_dis_idof >= comp_start_dis_idof, "invalid comp ios index");
      size_type comp_ios_dis_idof = ios_dis_idof - comp_start_dis_idof;
      comp_u_io.dis_entry (comp_ios_dis_idof) = value;
    }
    comp_u_io.dis_entry_assembly();
    // 2) then output the current field component
    size_type old_prec = ods.os().precision();
    ods << setprecision(std::numeric_limits<Float>::digits10);
    if (write_header) {
      ods << "field" << endl
          << "1 " << comp_dis_ndof << endl 
          << terminal_constit.get_geo().name() << endl
          << terminal_constit.get_basis().name() << endl
          << endl;
    }
    ods << comp_u_io
        << setprecision(old_prec);
    comp_start_idof     += comp_ndof;
    comp_start_dis_idof += comp_dis_ndof;
    if (comp_start_dis_idof != uh.dis_ndof()) {
      ods << endl;
    }
    return;
  }
  // hierarchical case:
  typedef typename space_constitution<T,M>::hierarchy_type hier_t;
  const hier_t& hier_constit = constit.get_hierarchy();
  for (typename hier_t::const_iterator iter = hier_constit.begin(), last = hier_constit.end(); iter != last; ++iter) {
    const space_constitution<T,M>& curr_constit = *iter;
    put_field_recursive (ods, uh, curr_constit, comp_start_idof, comp_start_dis_idof, false);
  }
}
template <class T, class M>
odiststream&
field_basic<T,M>::put_field (odiststream& ods) const
{
  using namespace std;
  bool need_header = (get_space().get_constitution().is_hierarchical());
  if (need_header) {
    // multi-field header or non-equispaced node set
    ods << "field" << endl
        << "3" << endl
        << "header" << endl
        << "  constitution " << get_space().get_constitution().name() << endl
        << "  size         " << get_space().get_constitution().dis_ndof() << endl
        << "end header" << endl
        << endl;
  } else {
    // scalar-field header and equispaced node set
    const space_constitution_terminal<T,M>& terminal_constit = get_space().get_constitution().get_terminal();
    ods << "field" << endl
        << "1 " << dis_ndof() << endl 
        << terminal_constit.get_geo().name() << endl
        << terminal_constit.get_basis().name() << endl
        << endl;
  }
  size_type comp_start_idof     = 0;
  size_type comp_start_dis_idof = 0;
  put_field_recursive (ods, *this, get_space().get_constitution(), comp_start_idof, comp_start_dis_idof);
  return ods;
}
// ----------------------------------------------------------------------------
// graphic output switch
// ----------------------------------------------------------------------------
// class-member template partial specialization 'field<T, M>::put(ods)' is not allowed
// here, we want to specialize for M=seq & M=dist since only seq graphic is vailable yet:
template <class T> odiststream& visu_gnuplot      (odiststream&, const field_basic<T,sequential>&);
template <class T> odiststream& visu_gmsh         (odiststream&, const field_basic<T,sequential>&);
template <class T> odiststream& visu_vtk_paraview (odiststream&, const field_basic<T,sequential>&);
template <class T> odiststream& field_put_gmsh    (odiststream&, const field_basic<T,sequential>&, std::string);
template <class T> odiststream& field_put_gmsh_pos(odiststream&, const field_basic<T,sequential>&);
template <class T> odiststream& field_put_bamg_bb (odiststream&, const field_basic<T,sequential>&);

// => use an intermediate class-function with full class specialization
template <class T, class M>
struct field_put {
  odiststream& operator() (odiststream& ods, const field_basic<T, M>& uh) const {
  	return uh.put_field (ods);
  }
};
// now, we can specialize the full class when M=seq:
template <class T>
struct field_put<T,sequential> {
  odiststream& operator() (odiststream& ods, const field_basic<T,sequential>& uh) const
  {
    iorheo::flag_type format = iorheo::flags(ods.os()) & iorheo::format_field;
    if (format [iorheo::gnuplot]) { return visu_gnuplot      (ods,uh); }
    if (format [iorheo::paraview]){ return visu_vtk_paraview (ods,uh); }
    if (format [iorheo::gmsh])    { return visu_gmsh         (ods,uh); }
 // if (format [iorheo::gmsh])    { return field_put_gmsh    (ods,uh,""); }
    if (format [iorheo::gmsh_pos]){ return field_put_gmsh_pos(ods,uh); }
    if (format [iorheo::bamg])    { return field_put_bamg_bb (ods,uh); }
    return uh.put_field (ods);
  }
};
// finally, the field::put member function uses the class-function
template <class T, class M>
odiststream&
field_basic<T,M>::put (odiststream& ods) const
{
  field_put<T,M> put_fct;
  return put_fct (ods, *this);
}
// ----------------------------------------------------------------------------
// access to non-local dofs
// ----------------------------------------------------------------------------
template <class T, class M>
const T&
field_basic<T,M>::dis_dof (size_type dis_idof) const
{
  if (is_distributed<M>::value) {
    if (_dis_dof_indexes_requires_update || _dis_dof_assembly_requires_update) {
      size_type nproc = comm().size();
      check_macro (nproc == 1, "field::dis_dof_update() need to be called before field::dis_dof(dis_idof)");
    }
  }
  if (ownership().is_owned (dis_idof)) {
    size_type first_idof = ownership().first_index ();
    size_type idof = dis_idof - first_idof;
    return dof (idof);
  }
  // here dis_idof is owned by another proc
  space_pair_type blk_dis_iub = _V.data()._idof2blk_dis_iub.dis_at (dis_idof); // TODO: write a better access !
  size_type dis_iub = blk_dis_iub.dis_iub();
  const T& value = (! blk_dis_iub.is_blocked()) ? _u.dis_at (dis_iub) : _b.dis_at (dis_iub);
  return value;
}
template <class T, class M>
typename field_basic<T,M>::dis_reference
field_basic<T,M>::dis_dof_entry (size_type dis_idof)
{
  dis_dof_assembly_requires_update();
  const space_pair_type& blk_dis_iub = _V.data()._idof2blk_dis_iub.dis_at (dis_idof); // TODO: write a better access !
  size_type dis_iub = blk_dis_iub.dis_iub();
  if (! blk_dis_iub.is_blocked()) {
    return _u.dis_entry (dis_iub);
  } else {
    return _b.dis_entry (dis_iub);
  }
}
// ----------------------------------------------------------------------------
// evaluation
// ----------------------------------------------------------------------------
template <class T, class M>
T
field_basic<T,M>::evaluate (const geo_element& K, const point_basic<T>& hat_x, size_type i_comp) const
{
  const basis_basic<T>& b = _V.get_basis();
  size_type loc_ndof = b.ndof (K.variant()) ;
  std::vector<size_type> dis_idof1 (loc_ndof);
  _V.dis_idof (K, dis_idof1);

  std::vector<T> dof (loc_ndof);
  for (size_type loc_idof = 0; loc_idof < loc_ndof; loc_idof++) {
    dof [loc_idof] = dis_dof (dis_idof1 [loc_idof]);
  }
  // WARNING: not efficient since it evaluate the hat_basis at each hat_x
  // when hat_x is on a repetitive pattern, such as quadrature nodes or Lagrange basis nodes
  // TODO: evaluate basis one time for all on hat_K
  Eigen::Matrix<T,Eigen::Dynamic,1>   b_value (loc_ndof);
  b.evaluate (K, hat_x, b_value);

  T value = 0;
  for (size_type loc_idof = 0; loc_idof < loc_ndof; loc_idof++) {
    value += dof [loc_idof] * b_value[loc_idof]; // sum_i w_coef(i)*hat_phi(hat_x)
  }
  return value;
}
template <class T, class M>
T
field_basic<T,M>::dis_evaluate (const point_basic<T>& x, size_type i_comp) const
{
  dis_dof_update();
  const geo_basic<T,M>& omega = _V.get_geo();
  size_type dis_ie = omega.dis_locate (x);
  check_macro (dis_ie != std::numeric_limits<size_type>::max(), "x="<<x<<" is outside the domain");
  // only the proc owner of dis_ie compute the value; other procs are waiting for the value
  T value = std::numeric_limits<T>::max();
  size_type map_dim = omega.map_dimension();
  const distributor& ownership = omega.geo_element_ownership(map_dim);
  size_type ie_proc = ownership.find_owner(dis_ie);
  size_type my_proc = ownership.comm().rank();
  std::vector<size_type> dis_inod;
  if (my_proc == ie_proc) {
    size_type first_dis_ie = ownership.first_index();
    size_type ie = dis_ie - first_dis_ie;
    const geo_element& K = omega[ie];
    omega.dis_inod (K, dis_inod);
    point_basic<T> hat_x = inverse_piola_transformation (_V.get_geo(), K, dis_inod, x);
    value = evaluate (K, hat_x, i_comp);
  }
#ifdef _RHEOLEF_HAVE_MPI
  if (is_distributed<M>::value) {
    mpi::broadcast (mpi::communicator(), value, ie_proc);
  }
#endif // _RHEOLEF_HAVE_MPI
  return value;
}
template <class T, class M>
point_basic<T>
field_basic<T,M>::dis_vector_evaluate (const point_basic<T>& x) const
{
  fatal_macro ("dis_vector_evaluate: not yet");
  return point_basic<T>();
}
// ----------------------------------------------------------------------------
// tensor component access: sigma_h(i,j)
// ----------------------------------------------------------------------------
#ifdef TO_CLEAN
template <class T, class M>
details::field_rdof_sliced_const<field_basic<T,M>>
field_basic<T,M>::operator() (size_type i_comp, size_type j_comp) const
{
  space_constant::coordinate_type sys_coord = get_geo().coordinate_system();
  size_type ij_comp = space_constant::tensor_index (valued_tag(), sys_coord, i_comp, j_comp);
  return details::field_rdof_sliced_const<field_basic<T,M>> (*this, ij_comp);
}
template <class T, class M>
details::field_wdof_sliced<field_basic<T,M>>
field_basic<T,M>::operator() (size_type i_comp, size_type j_comp)
{
  space_constant::coordinate_type sys_coord = get_geo().coordinate_system();
  size_type ij_comp = space_constant::tensor_index (valued_tag(), sys_coord, i_comp, j_comp);
  return details::field_wdof_sliced<field_basic<T,M>> (*this, ij_comp);
}
#endif // TO_CLEAN
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation_base(T,M) 				\
template class field_basic<T,M>;					\
template odiststream& operator<< (odiststream&, const field_basic<T,M>&);

#ifdef TODO
#define _RHEOLEF_instanciation(T,M) 					\
_RHEOLEF_instanciation_base(T,M) 					\
_RHEOLEF_instanciation_base(std::complex<T>,M)
#else // TODO
#define _RHEOLEF_instanciation(T,M) 					\
_RHEOLEF_instanciation_base(T,M)
#endif // TODO

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
