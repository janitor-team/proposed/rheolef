# ifndef _RHEOLEF_FIELD_RDOF_H
# define _RHEOLEF_FIELD_RDOF_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// field_rdof concept: field with read accessors at the dof level
// terminals : field_wdof, field_rdof_sliced_const, field_rdof_indirect_const
// + all affine homogeneous expressions involving these terminals
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   23 april 2020

// TODO: add an automatic field_lazy-compatible interface
//       for accessors at the element level
//  => could combine eg field_lazy + field_rdof
//
#include "rheolef/field_lazy.h"

namespace rheolef { namespace details {

template<class FieldRdof> class field_rdof_sliced_const; // forward declaration
template<class FieldRdof> class field_rdof_indirect_const;

template<class Derived>
class field_rdof_base {
public:
// definitions:

  using size_type   = typename field_traits<Derived>::size_type;
  using scalar_type = typename field_traits<Derived>::scalar_type;
  using memory_type = typename field_traits<Derived>::memory_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using geo_type    = geo_basic<float_type,memory_type>;
  using space_type  = space_basic<float_type,memory_type>;

// accessors:

  field_rdof_indirect_const<Derived> operator[] (const std::string& dom_name) const;
  field_rdof_indirect_const<Derived> operator[] (const geo_type& dom) const;
  field_rdof_sliced_const  <Derived> operator[] (size_type i_comp) const;
  field_rdof_sliced_const  <Derived> operator() (size_type i_comp, size_type j_comp) const;

// for compatibility with the lazy interface

  bool have_homogeneous_space (space_type& Xh) const { Xh = derived().get_space(); return true; }

protected:
  Derived&       derived()       { return *static_cast<      Derived*>(this); }
  const Derived& derived() const { return *static_cast<const Derived*>(this); }
};

// eg field_wdof_sliced --> field_rdof_sliced_const
template<class FieldWdof>
struct field_wdof2rdof_traits {
  using type = FieldWdof;
};

}}// namespace rheolef::details
# endif /* _RHEOLEF_FIELD_RDOF_H */
