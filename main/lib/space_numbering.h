#ifndef _RHEOLEF_SPACE_NUMBERING_H
#define _RHEOLEF_SPACE_NUMBERING_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo.h"

/*Class:space_numbering
NAME: @code{space_numbering} - global degree of freedom numbering
@cindex  numbering, global degree of freedom
@cindex  polynomial basis
@clindex space_numbering
SYNOPSIS:
  @noindent
  The @code{space_numbering} class defines methods that furnish global
  numbering of degrees of freedom. This numbering depends upon
  the degrees of polynoms on elements and upon the continuity
  requirement at inter-element boundary. For instance the
  "P1" continuous finite element approximation has one degree
  of freedom per vertice of the mesh, while its discontinuous
  counterpart has dim(basis) times the number of elements of the
  mesh, where dim(basis) is the size of the local finite element basis.

AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
   | Pierre.Saramito@imag.fr
DATE:   7 january 2004
End:
*/

namespace rheolef { namespace space_numbering {

  typedef geo_element::size_type size_type;

  template<class T> size_type     ndof (const basis_basic<T>& b, const geo_size& gs, size_type map_dim);
  template<class T> size_type     nnod (const basis_basic<T>& b, const geo_size& gs, size_type map_dim);
  template<class T> size_type dis_ndof (const basis_basic<T>& b, const geo_size& gs, size_type map_dim);
  template<class T> size_type dis_nnod (const basis_basic<T>& b, const geo_size& gs, size_type map_dim);
  template<class T> void      dis_idof (const basis_basic<T>& b, const geo_size& gs, const geo_element& K, std::vector<size_type>& dis_idof);
  template<class T> void      dis_inod (const basis_basic<T>& b, const geo_size& gs, const geo_element& K, std::vector<size_type>& dis_inod);
  template<class T> void      dis_idof (const basis_basic<T>& b, const geo_size& gs, const geo_element& K, typename std::vector<size_type>::iterator dis_idof);
  template<class T> void      dis_inod (const basis_basic<T>& b, const geo_size& gs, const geo_element& K, typename std::vector<size_type>::iterator dis_inod);

  template<class T>
  inline void set_ios_permutations (
		const basis_basic<T>&             b,
                const geo_basic<T,sequential>&    omega,
    		disarray<size_type,sequential>&   idof2ios_dis_idof,
    		disarray<size_type,sequential>&   ios_idof2dis_idof) {}
#ifdef _RHEOLEF_HAVE_MPI
  template<class T>
  void set_ios_permutations (
		const basis_basic<T>&             b,
                const geo_basic<T,distributed>&   omega,
    		disarray<size_type,distributed>&  idof2ios_dis_idof,
    		disarray<size_type,distributed>&  ios_idof2dis_idof);

  // low-level interface, used by geo_mpi_get.cc
  template<class T>
  void generic_set_ios_permutation (
    		const basis_basic<T>&         b,
    		size_t                        map_d,
    		const geo_size&               gs,
    		const std::array<disarray<size_t,distributed>,reference_element::max_variant>&
                                  	      igev2ios_dis_igev,
    		disarray<size_t,distributed>& idof2ios_dis_idof);
#endif // _RHEOLEF_HAVE_MPI
}} // namespace rheolef::space_numbering
#endif // _RHEOLEF_SPACE_NUMBERING_H
