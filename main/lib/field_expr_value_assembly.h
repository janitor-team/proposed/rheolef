#ifndef _RHEO_FIELD_EXPR_VALUE_ASSEMBLY_H
#define _RHEO_FIELD_EXPR_VALUE_ASSEMBLY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/field.h"
#include "rheolef/test.h"
#include "rheolef/quadrature.h"
/*
let:
I = int_omega expr(x) dx

The integrals are evaluated over each element K of the domain omega
by using a quadrature formulae given by iopt

expr(x) is a nonlinear expression 

The integrals over K are transformed on the reference element with
the piola transformation:
F : hat_K ---> K
 hat_x |--> x = F(hat_x)

int_K expr(x) dx 
= int_{hat_K} expr(F(hat_x) det(DF(hat_x)) d hat_x
= sum_q expr(F(hat_xq)) det(DF(hat_xq)) hat_wq

On each K, the computation needs a loop in q.
The expresion is represented by a tree at compile-time.
The 'expr' is represented by field_expr_terminal<f> : it evaluation gives : 
value1(q) = expr(F(hat_xq))

This approch generilizes to an expression tree.
*/
namespace rheolef { namespace details {

template <class T, class M, class Expr, class Result>
void
field_expr_v2_value_assembly (
  const geo_basic<T,M>&         omega,
  const Expr&                   expr0,
  const integrate_option&       iopt, 
  Result&                       result)
{
  Expr expr = expr0; // so could expr.initialze()
  typedef typename geo_basic<T,M>::size_type  size_type;
  // ------------------------------------
  // 0) initialize the loop
  // ------------------------------------
  quadrature<T> quad;
  check_macro (iopt.get_order() != std::numeric_limits<quadrature_option::size_type>::max(),
    "integrate: the quadrature formulae order may be chosen (HINT: use qopt.set_order(n))");
  quad.set_order  (iopt.get_order());
  quad.set_family (iopt.get_family());
  piola_on_pointset<T> pops;
  pops.initialize (omega.get_piola_basis(), quad, iopt);
  expr.initialize (pops, iopt);
  expr.template valued_check<Result>();
  size_type map_d = omega.map_dimension();
  Eigen::Matrix<Result,Eigen::Dynamic,1> value;
  // ------------------------------------
  // 1) loop on elements
  // ------------------------------------
  result = Result(0);
  for (size_type ie = 0, ne = omega.size(map_d); ie < ne; ie++) {
    const geo_element& K = omega.get_geo_element (map_d, ie);
    // ------------------------------------
    // 1.1) locally compute values
    // ------------------------------------
    // locally evaluate int_K expr dx
    // with loop on a quadrature formulae 
    // and accumulate in result
    expr.evaluate (omega, K, value);
    const Eigen::Matrix<T,Eigen::Dynamic,1>& w = pops.get_weight (omega, K);
    check_macro (w.size() == value.size(),
      "incompatible sizes w("<<w.size()<<") and value("<<value.size()<<")");
    // TODO: DVT_BLAS1 Eigen::Matrix<Result,Eigen::Dynamic,Eigen::Dynamic> ri = value.transpose()*w; 
    //       => compil pb with Eigen when Result is point, tensor, etc:
    size_type loc_nnod = value.size();
    for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
      result += value[loc_inod]*w[loc_inod];
    }
  }
#ifdef _RHEOLEF_HAVE_MPI
  if (is_distributed<M>::value) {
    result =  mpi::all_reduce (omega.geo_element_ownership(0).comm(), result, std::plus<Result>());
  }
#endif // _RHEOLEF_HAVE_MPI
}

}}// namespace rheolef::details
#endif // _RHEO_FIELD_EXPR_VALUE_ASSEMBLY_H
