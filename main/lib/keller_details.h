#ifndef _RHEOLEF_KELLER_DETAILS_H
#define _RHEOLEF_KELLER_DETAILS_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/pair_with_linear_algebra.h"

namespace rheolef { namespace details {

// -----------------
// reset
// -----------------
template<class T>
void reset (T& x) { x = 0; }

template<class T>
void reset (std::valarray<T>& x) { 
  for (size_t i = 0; i < x.size(); ++i) reset(x[i]);
}
template<class T1, class T2>
void reset (pair_with_linear_algebra<T1,T2>& x) 
{
  reset (x.first);
  reset (x.second);
}
// -----------------
// max_abs
// -----------------
template<class T>
T max_abs (const T& x) { return abs(x); }

template<class T>
T max_abs (const field_basic<T>& x) { return x.max_abs(); }

template<class T>
typename field_basic<T>::float_type
max_abs (const std::valarray<field_basic<T> >& x) {
  typename field_basic<T>::float_type value = 0;
  for (size_t i = 0; i < x.size(); ++i) {
    value = std::max(value, x[i].max_abs());
  }
  return value;
}
// -----------------
// unknown_size
// -----------------
template<class T>
size_t get_unknown (const T& x) { return 1; }

template<class T>
size_t
unknown_size (const field_basic<T>& x) { return x.u().size(); }

template<class T>
size_t
unknown_size (const std::valarray<field_basic<T> >& x) {
  size_t n = 0;
  for (size_t i = 0; i < x.size(); ++i) {
    n     += x[i].u().size();
  }
  return n;
}
// -----------------
// get_unknown part
// -----------------
template<class T>
T get_unknown (const T& x) { return x; }
template<class T>
T get_unknown (const T& x, const distributor&) { return x; }

template<class T>
vec<T> get_unknown (const field_basic<T>& x) { return x.u(); }
template<class T>
vec<T> get_unknown (const field_basic<T>& x, const distributor&) { return x.u(); }

template<class T>
vec<T> get_unknown (const std::valarray<field_basic<T> >& x, const distributor& ownership) {
  vec<T> y (ownership, T(0));
  for (size_t i = 0, start = 0; i < x.size(); ++i) {
    std::copy (x[i].u().begin(), x[i].u().end(), y.begin()+start);
    start += x[i].u().size();
  }
  return y;
}

template<class T>
vec<T> get_unknown (const std::valarray<field_basic<T> >& x) {
  if (x.size() == 0) return vec<T>();
  size_t n = 0, dis_n = 0;
  for (size_t i = 0; i < x.size(); ++i) {
    n     += x[i].u().size();
    dis_n += x[i].u().dis_size();
  }
  communicator comm = x[0].u().ownership().comm();
  distributor ownership (dis_n, comm, n);
  return get_unknown (x, ownership);
}
// -----------------
// set_unknown part
// -----------------
template<class T>
void set_unknown (T& x, const T& value) { x = value; }

template<class T>
void set_unknown (field_basic<T>& x, const vec<T>& value) {
  // value.size == x.size+1 on one proc
  size_t size = x.u().size();
  std::copy (value.begin(), value.begin() + size, x.set_u().begin());
}

template<class T>
void set_unknown (std::valarray<field_basic<T> >& x, const vec<T>& value) { 
  size_t start = 0;
  for (size_t i = 0; i < x.size(); ++i) {
    size_t last = start + x[i].u().size();
    std::copy (value.begin() + start, value.begin() + last, x[i].set_u().begin());
    start = last;
  }
}

}} // namespace rheolef::details
#endif // _RHEOLEF_KELLER_DETAILS_H
