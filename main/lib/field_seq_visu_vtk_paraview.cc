///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// paraview vtk visualization
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997  update: 23 oct 2011
//
#include "rheolef/field_expr.h"
#include "rheolef/piola_util.h"
#include "rheolef/rheostream.h"
#include "rheolef/iorheo.h"
#include "rheolef/iofem.h"
#include "rheolef/interpolate.h"
#include "rheolef/render_option.h"

using namespace std;
namespace rheolef { 

// ----------------------------------------------------------------------------
// field puts
// ----------------------------------------------------------------------------
// extern:
template <class T> odiststream& field_put_vtk (odiststream&, const field_basic<T,sequential>&);

template <class T>
odiststream&
visu_vtk_paraview (odiststream& ops, const field_basic<T,sequential>& uh)
{
  //
  // 0) prerequises
  //
  using namespace std;
  typedef typename field_basic<T,sequential>::float_type float_type;
  typedef typename geo_basic<float_type,sequential>::size_type size_type;
  typedef point_basic<size_type>                      ilat;
  ostream& os = ops.os();
  //
  // 1) set render options
  //
  render_option popt;
  popt.valued    = uh.get_space().valued();
  popt.fill      = iorheo::getfill(os);    // isocontours or color fill
  popt.elevation = iorheo::getelevation(os);
  popt.color   = iorheo::getcolor(os);
  popt.gray    = iorheo::getgray(os);
  popt.black_and_white = iorheo::getblack_and_white(os);
  popt.showlabel = iorheo::getshowlabel(os);
  popt.stereo    = iorheo::getstereo(os);
  popt.volume    = iorheo::getvolume(os);
  popt.iso       = true;
  popt.cut       = iorheo::getcut(os);
  popt.grid      = iorheo::getgrid(os);
  popt.format  = iorheo::getimage_format(os);
  popt.mark     = iorheo::getmark(os); 
  bool is_scalar = (popt.valued == "scalar");
  if (popt.mark == "") popt.mark = popt.valued; 
  popt.style = (popt.valued == "vector") ? (iorheo::getvelocity(os) ? "velocity" : "deformation") : "none"; 
  popt.scale = iorheo::getvectorscale(os);
  popt.origin     = iofem::getorigin(os);
  popt.normal     = iofem::getnormal(os);
  popt.resolution = iofem::getresolution(os);
  popt.n_isovalue          = iorheo::getn_isovalue(os);
  popt.n_isovalue_negative = iorheo::getn_isovalue_negative(os);
  popt.isovalue            = iorheo::getisovalue(os); // isovalue is always a Float
  popt.label = iorheo::getlabel(os);
  const geo_basic<float_type,sequential>& omega = uh.get_geo();
  size_type dim     = omega.dimension();
  size_type map_dim = omega.map_dimension();
  popt.view_2d      = (dim == 2);
  popt.view_map     = (dim > map_dim);
#if (_RHEOLEF_PARAVIEW_VERSION_MAJOR >= 5) && (_RHEOLEF_PARAVIEW_VERSION_MINOR >= 5)
  // paraview version >= 5.5 has high order elements
  popt.high_order = (omega.order() > 1 || uh.get_space().degree() > 1);
#else
  popt.high_order = false;
#endif
#if (_RHEOLEF_PARAVIEW_VERSION_MAJOR == 5) && (_RHEOLEF_PARAVIEW_VERSION_MINOR == 7)
  // paraview version == 5.7 has opacity bug
  popt.have_opacity_bug = true;
#else
  popt.have_opacity_bug = false;
#endif
  popt.xmin = uh.get_geo().xmin();
  popt.xmax = uh.get_geo().xmax();
  field_basic<T,sequential> uh_scalar;
  if (popt.valued == "scalar") {
    uh_scalar = uh;
  } else {
    size_type k = uh.get_space().degree();
    space_basic<T,sequential> T0h (uh.get_geo(), "P"+std::to_string(k));
    uh_scalar = interpolate(T0h, norm(uh));
  }
  // TODO: u_min=min(uh.u.min,uh.b.min) instead of interpolate norm ?
  popt.f_min = uh_scalar.min();
  popt.f_max = uh_scalar.max();
  //
  // 2) output data
  //
  bool verbose  = iorheo::getverbose(os);
  bool clean    = iorheo::getclean(os);
  bool execute  = iorheo::getexecute(os);
  string basename = iorheo::getbasename(os);
  string outfile_fmt = "";
  string tmp = get_tmpdir() + "/";
  if (!clean) tmp = "";
  string filelist;
  string filename = tmp+basename + ".vtk";
  filelist = filelist + " " + filename;
  ofstream vtk_os (filename.c_str());
  odiststream vtk (vtk_os);
  if (verbose) clog << "! file \"" << filename << "\" created.\n";
  field_put_vtk (vtk, uh);
  vtk.close();
  //
  // 3) create python data file
  //
  std::string py_name = filename = tmp+basename + ".py";
  filelist = filelist + " " + filename;
  ofstream py (filename.c_str());
  if (verbose) clog << "! file \"" << filename << "\" created.\n";
  point xmin = uh.get_geo().xmin(),
        xmax = uh.get_geo().xmax();
  py << popt
     << endl
     << "paraview_field_" << popt.valued << "(paraview, \"" << tmp+basename << "\", opt)" << endl
     << endl
     ;
  py.close();
  //
  // 4) run pyton
  //
  int status = 0;
  string command;
  if (execute) {
      string prog = (popt.format == "") ? "paraview --script=" : "pvbatch --use-offscreen-rendering ";
      command = "LANG=C PYTHONPATH=" + string(_RHEOLEF_PKGDATADIR) + " " + prog + py_name;
      if (popt.format != "") command = "DISPLAY=:0.0 " + command;
      if (popt.stereo && popt.format == "") command = command + " --stereo";
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  //
  // 4) clear vtk data
  //
  if (clean) {
      command = "/bin/rm -f " + filelist;
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  return ops;
}
// --------------------------------------------------------------------
// plane cut : via vtk-paraview script
// --------------------------------------------------------------------
template <class T> idiststream& geo_get_vtk (idiststream&, geo_basic<T,sequential>&);

template <class T>
field_basic<T,sequential>
paraview_plane_cut (
  const field_basic<T,sequential>& uh,
  const point_basic<T>&            origin,
  const point_basic<T>&            normal)
{
  //
  // 1) prerequises
  //
  using namespace std;
  typedef typename field_basic<T,sequential>::float_type float_type;
  typedef typename geo_basic<float_type,sequential>::size_type size_type;
  typedef typename geo_basic<float_type,sequential>::node_type node_type;
  typedef point_basic<size_type>                      ilat;
  ostream& os = std::cout;
  bool verbose  = iorheo::getverbose(os);
  bool clean    = iorheo::getclean(os);
  bool execute  = iorheo::getexecute(os);
  string basename = iorheo::getbasename(os);
  string tmp = get_tmpdir() + "/";
  if (!clean) tmp = "";
  //
  // 2) output data
  //
  string filelist;
  string vtk_name = tmp+basename + ".vtk";
  filelist = filelist + " " + vtk_name;
  ofstream vtk_os (vtk_name.c_str());
  odiststream vtk (vtk_os);
  if (verbose) clog << "! file \"" << vtk_name << "\" created.\n";
  field_put_vtk (vtk, uh);
  vtk.close();
  //
  // create python script
  //
  //
  // 3) create python data file
  //
  string      py_name = tmp+basename + "-cut.py";
  string vtk_cut_name = tmp+basename + "-cut.vtk";
  filelist = filelist + " " + py_name + " " + vtk_cut_name;
  ofstream py (py_name.c_str());
  if (verbose) clog << "! file \"" << py_name << "\" created.\n";
  py << setprecision(numeric_limits<T>::digits10)
     << "from paraview.simple import *" << endl
     << "reader = LegacyVTKReader(FileNames=['" << vtk_name << "'])" << endl
     << "slice = Slice(SliceType=\"Plane\")" << endl
     << "slice.SliceOffsetValues = [0.0]" << endl
     << "slice.SliceType.Origin = " << render_option::python(origin) << endl
     << "slice.SliceType.Normal = " << render_option::python(normal) << endl
     << "writer = paraview.simple.CreateWriter(\"" << vtk_cut_name << "\", slice)" << endl
     << "writer.FileType = 'Ascii'" << endl
     << "writer.UpdatePipeline()" << endl
     ;
  py.close();
  //
  // 3) run pyton
  //
  int status = 0;
  string command;
  command = "LANG=C PYTHONPATH=" + string(_RHEOLEF_PKGDATADIR) + " pvpython < " + py_name;
  if (verbose) clog << "! " << command << endl;
  status = system (command.c_str());
  //  
  // 4) load vtk cutted mesh
  //
  ifstream vtk_polydata (vtk_cut_name.c_str());
  check_macro (vtk_polydata, "field: vtk polydata file \"" << vtk_cut_name << "\" not found.");
  if (verbose) clog << "! load `" << vtk_cut_name << "'." << endl;
  idiststream ips_vtk_polydata (vtk_polydata);
  geo_basic<T,sequential> gamma_cut; 
  geo_get_vtk (ips_vtk_polydata, gamma_cut);
  gamma_cut.set_name (basename + "-cut");
  check_macro (gamma_cut.n_node() > 0, "empty mesh & plane intersection: HINT check normal and origin");
  //
  // clean
  //
  if (clean) {
      command = "/bin/rm -f " + filelist;
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  //
  // project geometry from 2d/3d on plane in 1d/2d
  // 
  //   1D) x := dot(OM, tangent)
  //
  //   2D) x1 := dot(OM, t1)
  //       x2 := dot(OM, t2)
  //
  bool use_projection = true;
  if (use_projection) {
    switch (uh.get_geo().dimension()) {
      case 2: {
        // project in 1D space
        gamma_cut.set_dimension (1);
        point_basic<T> tangent = point_basic<T>(normal[1], -normal[0]);
	disarray<node_type,sequential> node = gamma_cut.get_nodes();
        for (size_type i = 0, n = node.size(); i < n; i++) {
	  node[i] = point_basic<T> (dot(node[i], tangent), 0);
        }
        gamma_cut.set_nodes (node); 
        break;
      }
      case 3: {
        gamma_cut.set_dimension (2);
        point_basic<T> t1 = point_basic<T>(1, 0, 0);
        if (norm(vect(t1, normal)) == Float(0)) t1 = point_basic<T>(0, 1, 0);
        t1 = t1 - dot(t1,normal)*normal;
        t1 = t1 / norm(t1);
        point_basic<T> t2 = vect(normal,t1);
	disarray<node_type,sequential> node = gamma_cut.get_nodes();
        for (size_type i = 0, n = node.size(); i < n; i++) {
	  node[i] = point_basic<T> (dot(node[i] - origin, t1), dot(node[i] - origin, t2), 0);
        }
        gamma_cut.set_nodes (node); 
        break;
      }
      default: error_macro ("unexpected dimension="<< uh.get_geo().dimension());
    }
  }
  // -------------------------------
  // get field values
  // polydata header:
  //     POINT_DATA <n_node>
  //     SCALARS scalars float
  //     LOOKUP_TABLE default
  // -------------------------------
  string data_type;
  bool founded = false;
  while (vtk_polydata.good()) {
    vtk_polydata >> data_type;
    if ((data_type == "POINT_DATA") || 
        (data_type == "CELL_DATA" )) {
      break;
    }
  }
  check_macro (data_type == "POINT_DATA" || data_type == "CELL_DATA",
    "paraview_plane_cut: unexpected vtk polydata file");
  scatch (vtk_polydata, "default");
  string approx = (data_type == "POINT_DATA") ? "P1" : "P0";
  space_basic<T,sequential> V_cut (gamma_cut, approx);
  field_basic<T,sequential> u_cut (V_cut);
  u_cut.set_u().get_values (ips_vtk_polydata);
  return u_cut;
}
// --------------------------------------------------------------------
// isovalue surface : via vtk-paraview script
// --------------------------------------------------------------------
template <class T> idiststream& geo_get_vtk (idiststream&, geo_basic<T,sequential>&);

template <class T>
geo_basic<T,sequential>
paraview_extract_isosurface (const field_basic<T,sequential>& uh)
{
  //
  // 1) prerequises
  //
  using namespace std;
  typedef typename field_basic<T,sequential>::float_type float_type;
  typedef typename geo_basic<float_type,sequential>::size_type size_type;
  typedef typename geo_basic<float_type,sequential>::node_type node_type;
  typedef point_basic<size_type>                      ilat;
  ostream& os = std::cout;
  bool verbose  = iorheo::getverbose(os);
  bool clean    = iorheo::getclean(os);
  bool execute  = iorheo::getexecute(os);
  T isovalue = iorheo::getisovalue(os);
  string basename = iorheo::getbasename(os);
  string tmp = get_tmpdir() + "/";
  if (!clean) tmp = "";
  //
  // 2) output data
  //
  string filelist;
  string vtk_name = tmp+basename + ".vtk";
  filelist = filelist + " " + vtk_name;
  ofstream vtk_os (vtk_name.c_str());
  odiststream vtk (vtk_os);
  if (verbose) clog << "! file \"" << vtk_name << "\" created.\n";
  field_put_vtk (vtk, uh);
  vtk.close();
  //
  // create python script
  //
  //
  // 3) create python data file
  //
  string      py_name = tmp+basename + "-iso.py";
  string vtk_iso_name = tmp+basename + "-iso.vtk";
  filelist = filelist + " " + py_name + " " + vtk_iso_name;
  ofstream py (py_name.c_str());
  if (verbose) clog << "! file \"" << py_name << "\" created.\n";
  py << setprecision(numeric_limits<T>::digits10)
     << "from paraview.simple import *" << endl
     << "reader = LegacyVTKReader(FileNames=['" << vtk_name << "'])" << endl
     << "iso_surface = Contour(PointMergeMethod=\"Uniform Binning\")" << endl
     << "iso_surface.ContourBy = ['POINTS', 'scalar']" << endl
     << "iso_surface.Isosurfaces = [" << isovalue << "]" << endl
     << "writer = paraview.simple.CreateWriter(\"" << vtk_iso_name << "\", iso_surface)" << endl
     << "writer.FileType = 'Ascii'" << endl
     << "writer.UpdatePipeline()" << endl
     ;
  py.close();
  //
  // 3) run pyton
  //
  int status = 0;
  string command;
  command = "LANG=C PYTHONPATH=" + string(_RHEOLEF_PKGDATADIR) + " pvbatch " + py_name;
  if (verbose) clog << "! " << command << endl;
  status = system (command.c_str());
  //  
  // 4) load vtk isosurface mesh
  //
  ifstream vtk_polydata (vtk_iso_name.c_str());
  check_macro (vtk_polydata, "field: vtk polydata file \"" << vtk_iso_name << "\" not found.");
  if (verbose) clog << "! load `" << vtk_iso_name << "'." << endl;
  idiststream ips_vtk_polydata (vtk_polydata);
  geo_basic<T,sequential> gamma_iso; 
  geo_get_vtk (ips_vtk_polydata, gamma_iso);
  gamma_iso.set_name (basename + "-iso");
  gamma_iso.set_dimension (uh.get_geo().dimension());
  check_macro (gamma_iso.n_node() > 0, "empty mesh & plane intersection: HINT check normal and origin");
  //
  // clean
  //
  if (clean) {
      command = "/bin/rm -f " + filelist;
      if (verbose) clog << "! " << command << endl;
      status = system (command.c_str());
  }
  return gamma_iso;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                             	\
template odiststream&						\
visu_vtk_paraview<Float>  (					\
  odiststream&,							\
  const field_basic<Float,sequential>&);			\
template field_basic<Float,sequential>				\
paraview_plane_cut (						\
  const field_basic<Float,sequential>&,				\
  const point_basic<Float>&,					\
  const point_basic<Float>&);					\
template geo_basic<Float,sequential>				\
paraview_extract_isosurface (					\
  const field_basic<Float,sequential>&);

_RHEOLEF_instanciation(Float)
#undef _RHEOLEF_instanciation

}// namespace rheolef
