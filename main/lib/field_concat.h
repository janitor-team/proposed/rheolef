#ifndef _RHEOLEF_FIELD_CONCAT_H
#define _RHEOLEF_FIELD_CONCAT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// build field from initializer lists
//
#include "rheolef/field.h"

// ----------------------------------------------------------------------------
// 1) field initializer handler
// ----------------------------------------------------------------------------
namespace rheolef { namespace details {

template <class T, class M>
class field_concat_value {
public:

// typedef:

  typedef enum { scalar, vector_scalar, field} variant_type;

// allocators:
 
  template <class U,
            class Sfinae
                  = typename std::enable_if<
                      is_rheolef_arithmetic<U>::value
                     ,void
                    >::type
           >
  field_concat_value (const U& x)                : s(x), vs(),  f(),  variant(scalar) {}
  field_concat_value (const std::vector<T>& x)   : s(),  vs(x), f(),  variant(vector_scalar) {}
  field_concat_value (const field_basic<T,M>& x) : s(),  vs(),  f(x), variant(field) {}

// io/debug:
  friend std::ostream& operator<< (std::ostream& o, const field_concat_value<T,M>& x) {
    if (x.variant == scalar)        return o << "s";
    if (x.variant == vector_scalar) return o << "vs";
    else                            return o << "f";
  }
// data:
public:
  T                    s;
  std::vector<T>       vs;
  field_basic<T,M>     f;
  variant_type         variant;
};

template <class T, class M>
class field_concat {
public:

// typedef:

  typedef typename field_basic<T,M>::size_type   size_type;
  typedef field_concat_value<T,M>                value_type;
  typedef typename std::initializer_list<value_type>::const_iterator const_iterator;

// allocators:

  field_concat () : _l() {}

  field_concat (const std::initializer_list<value_type>& il) : _l() {
    for(const_iterator iter = il.begin(); iter != il.end(); ++iter) {
      _l.push_back(*iter);
    }
  }
  friend std::ostream& operator<< (std::ostream& o, const field_concat<T,M>& x) {
    std::cout << "{";
    for(typename std::list<value_type>::const_iterator iter = x._l.begin(); iter != x._l.end(); ++iter) {
        std::cout << *iter << " ";
    }
    return std::cout << "}";
  }
  field_basic<T,M> build_field() const;

// data:
protected:
 std::list<value_type> _l;
};

} // namespace details

// ----------------------------------------------------------------------------
// 2) field member functions
// ----------------------------------------------------------------------------
template <class T, class M>
inline
field_basic<T,M>::field_basic (const std::initializer_list<details::field_concat_value<T,M> >& init_list)
 : _V(), _u(), _b(), _dis_dof_indexes_requires_update(true), _dis_dof_assembly_requires_update(true)
{
  details::field_concat<T,M> vc (init_list);
  field_basic<T,M>::operator= (vc.build_field());
}
template <class T, class M>
inline
field_basic<T,M>&
field_basic<T,M>::operator= (const std::initializer_list<details::field_concat_value<T,M> >& init_list)
{
  details::field_concat<T,M> vc (init_list);
  field_basic<T,M>::operator= (vc.build_field());
  return *this;
}

} // namespace rheolef
#endif // _RHEOLEF_FIELD_CONCAT_H
