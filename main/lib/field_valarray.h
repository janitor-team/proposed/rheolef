# ifndef _RHEOLEF_FIELD_VALARRAY_H
# define _RHEOLEF_FIELD_VALARRAY_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// valarray<field> : utilities (for generic newton.h and damped_newton.h)
//
// note: obsolete : has been super-setted by field_eigen.h
// maintained for backward compat.
//
#include "rheolef/field.h"
#include <valarray>

namespace rheolef {
template<class T>
std::valarray<field_basic<T> >
operator* (const T& a, const std::valarray<field_basic<T> >& xh)
{
  std::valarray<field_basic<T> > yh (xh.size());
  for (size_t i = 0, n = xh.size(); i < n; ++i)
    yh[i] = a*xh[i];
  return yh;
}
} // namespace rheolef
# endif // _RHEOLEF_FIELD_VALARRAY_H
