# ifndef _RHEOLEF_FIELD_CONCEPT_H
# define _RHEOLEF_FIELD_CONCEPT_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// field_wdof concept: field with read & write accessors at the dof level
// terminals: field_basic, field_wdof_sliced, field_indirect
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   21 april 2020

#include "rheolef/space_constant.h"

namespace rheolef { namespace details {

// define concept: for filtering the field_basic class:
template<class FieldWdof, class Sfinae = void>
struct is_field: std::false_type {};

// define concept: is_field_wdof
template<class FieldWdof, class Sfinae = void>
struct is_field_wdof: std::false_type {};

// define concept: has_field_wdof_interface
template<class FieldWdof, class Sfinae = void>
struct has_field_wdof_interface: std::false_type {};

template<class FieldWdof>
struct has_field_wdof_interface <FieldWdof, typename std::enable_if<
        is_field_wdof<FieldWdof>::value>::type> : std::true_type {};

template<class FieldWdof>
struct has_field_wdof_interface <FieldWdof, typename std::enable_if<
        is_field<FieldWdof>::value>::type> : std::true_type {};

// define concept: is_field_rdof
template<class FieldRdof, class Sfinae = void>
struct is_field_rdof: std::false_type {};

// define concept: has_field_rdof_interface
template<class FieldRdof, class Sfinae = void>
struct has_field_rdof_interface: std::false_type {};

template<class FieldRdof>
struct has_field_rdof_interface <FieldRdof, typename std::enable_if<
        is_field_rdof<FieldRdof>::value>::type> : std::true_type {};

template<class FieldRdof>
struct has_field_rdof_interface<FieldRdof, typename std::enable_if<
       has_field_wdof_interface<FieldRdof>::value>::type> : std::true_type {};

// define concept: is_field_lazy
template<class FieldLazy, class Sfinae = void>
struct is_field_lazy: std::false_type {};

// define concept: has_field_lazy_interface
template<class FieldLazy, class Sfinae = void>
struct has_field_lazy_interface: std::false_type {};

template<class FieldLazy>
struct has_field_lazy_interface <FieldLazy, typename std::enable_if<
        is_field_lazy<FieldLazy>::value>::type> : std::true_type {};

template<class FieldLazy>
struct has_field_lazy_interface<FieldLazy, typename std::enable_if<
       has_field_rdof_interface<FieldLazy>::value>::type> : std::true_type {};

// define concept: field expression valid arguments
// -> constant and field_convertible are valid terminals:
// TODO: merge with has_field_lazy_interface ?
template<class Expr, class Sfinae = void>
struct is_field_expr_v2_nonlinear_arg : std::false_type {};
template<class Expr>
struct is_field_expr_v2_nonlinear_arg <Expr, typename std::enable_if<
       is_field_expr_v2_constant<Expr>::value>::type> : std::true_type {};
template<class Expr>
struct is_field_expr_v2_nonlinear_arg <Expr, typename std::enable_if<
       has_field_rdof_interface<Expr>::value>::type> : std::true_type {};

// define concept: linear & homogeneous expressions
// -> these expressions should act homogeneously in the same finite element space
// TODO: rename as is_field_piecewise_polynomial<FieldLazy>
template<class FieldLazy, class Sfinae = void>
struct is_field_expr_affine_homogeneous: std::false_type {};
template<class Expr>
struct is_field_expr_affine_homogeneous <Expr, typename std::enable_if<
       is_field_expr_v2_constant<Expr>::value>::type> : std::true_type {};
template<class Expr>
struct is_field_expr_affine_homogeneous <Expr, typename std::enable_if<
       has_field_rdof_interface<Expr>::value>::type> : std::true_type {};

// ---------------------------------------------------------------------------
// class F is field_functor or field_true_function ?
//
//  is_field_true_function : F = R (const point_basic<T>&) 
//  is_field_functor :       F have R (F::*) (const point_basic<T>&) const
//    with some T = some float type and R = any result_type
// ---------------------------------------------------------------------------
// is_field_true_function
template<class F>          struct is_field_true_function                            : std::false_type {};
template<class R, class T> struct is_field_true_function <R(const point_basic<T>&)> : std::true_type {};
template<class R, class T> struct is_field_true_function <R(*)(const point_basic<T>&)> : std::true_type {};

// is_field_functor
template<class F, class Sfinae = void> struct is_field_functor : std::false_type {};
template<class F>                      struct is_field_functor<F,typename std::enable_if<
    std::conjunction<
       std::negation<has_field_lazy_interface<F>>
      ,std::is_class<F>
      ,is_functor<F>
      ,std::disjunction<
         // TODO: arg = basic_point<T> with any T instead of T=Float
         is_callable<F, Float  (const point&) const>
        ,is_callable<F, point  (const point&) const>
        ,is_callable<F, tensor (const point&) const>
        ,is_callable<F, tensor3(const point&) const>
        ,is_callable<F, tensor4(const point&) const>
       >
#ifdef TODO
         // TODO: result from functor F instead of any one ?
      ,is_callable<F,typename get_functor_result<F>::type (const point&) const>
#endif // TODO
    >::value
  >::type
> : std::true_type {};

// is_field_function = is_field_true_function || is_field_functor 
template<class F, class Sfinae = void> struct is_field_function    : std::false_type {};
template<class F>                      struct is_field_function<F,
  typename std::enable_if<
      std::disjunction<
        is_field_true_function<F>
       ,is_field_functor<F>
      >::value
  >::type
> : std::true_type {};

template<class F, class Sfinae = void> struct field_function_traits {};
#ifdef TO_CLEAN
template<class R, class T> struct field_function_traits <R(*)(const point_basic<T>&)> : 
			          field_function_traits <R(const point_basic<T>&)> {};
#endif // TO_CLEAN
template<class F>                      struct field_function_traits<F, 
  typename std::enable_if<
       is_field_true_function<F>::value
  >::type
> {
  typedef typename function_traits<F>::result_type result_type;
};
template<class F>                      struct field_function_traits<F, 
  typename std::enable_if<
    is_field_functor<F>::value
  >::type
> {
  typedef typename functor_traits<F>::result_type result_type;
};

}}// namespace rheolef::details
# endif /* _RHEOLEF_FIELD_CONCEPT_H */
