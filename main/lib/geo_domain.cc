///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================

#include "rheolef/geo_domain.h"

namespace rheolef {

// ------------------------------------------------------------------------
// cstors 
// ------------------------------------------------------------------------
template <class T, class M>
geo_domain_rep<T,M>::geo_domain_rep (const geo_domain_rep<T,M>& x)
  : base(x),
    _dom (x._dom),
    _bgd_ie2dom_ie(x._bgd_ie2dom_ie),
    _dis_bgd_ie2dis_dom_ie(x._dis_bgd_ie2dis_dom_ie)
{
  trace_macro ("*** PHYSICAL COPY OF GEO_DOMAIN ***"); 
}
template <class T, class M>
geo_abstract_rep<T,M>*
geo_domain_rep<T,M>::clone() const
{
  trace_macro ("*** CLONE GEO_DOMAIN ***"); 
  typedef geo_domain_rep<T,M> rep;
  return new_macro(rep(*this));
}
template <class T, class M>
geo_domain_rep<T,M>::geo_domain_rep (const geo_domain_indirect_rep<T,M>& dom)
  : base(),
    _dom (dom),
    _bgd_ie2dom_ie(),
    _dis_bgd_ie2dis_dom_ie()
{
    geo_rep<T,M>::build_from_domain (
	_dom.get_indirect().data(),
	_dom.get_background_geo().data(),
	_bgd_ie2dom_ie,
        _dis_bgd_ie2dis_dom_ie);
}
// ----------------------------------------------------------------------------
// geo_element: goes from bgd to domain element
// ----------------------------------------------------------------------------
template <class T, class M>
typename geo_domain_rep<T,M>::size_type
geo_domain_rep<T,M>::bgd_ie2dom_ie (size_type bgd_ie) const
{
  typename std::map<size_type,size_type>::const_iterator iter = _bgd_ie2dom_ie.find (bgd_ie);
  if (iter != _bgd_ie2dom_ie.end()) {
    return (*iter).second;
  } else {
    return std::numeric_limits<size_type>::max();
  }
}
template <class T, class M>
typename geo_domain_rep<T,M>::size_type
geo_domain_rep<T,M>::dis_bgd_ie2dis_dom_ie (size_type dis_bgd_ie) const
{
  size_type map_d = base::map_dimension();
  size_type first_dis_bgd_ie = get_background_geo().sizes().ownership_by_dimension[map_d].first_index();
  size_type  last_dis_bgd_ie = get_background_geo().sizes().ownership_by_dimension[map_d]. last_index();
  if (dis_bgd_ie >= first_dis_bgd_ie && dis_bgd_ie < last_dis_bgd_ie) {
    size_type bgd_ie = dis_bgd_ie - first_dis_bgd_ie;
    return bgd_ie2dom_ie (bgd_ie);
  }
  typename std::map<size_type,size_type>::const_iterator iter = _dis_bgd_ie2dis_dom_ie.find (dis_bgd_ie);
  if (iter != _dis_bgd_ie2dis_dom_ie.end()) {
    return (*iter).second;
  } else {
    return std::numeric_limits<size_type>::max();
  }
}
template <class T, class M>
const geo_element&
geo_domain_rep<T,M>::bgd2dom_geo_element (const geo_element& bgd_K) const
{
  size_type map_d = bgd_K.dimension();
  size_type dis_bgd_ie = bgd_K.dis_ie();
  size_type first_dis_bgd_ie = get_background_geo().sizes().ownership_by_dimension[map_d].first_index();
  size_type  last_dis_bgd_ie = get_background_geo().sizes().ownership_by_dimension[map_d]. last_index();
  if (dis_bgd_ie >= first_dis_bgd_ie && dis_bgd_ie < last_dis_bgd_ie) {
    size_type bgd_ie = dis_bgd_ie - first_dis_bgd_ie;
    size_type dom_ie = bgd_ie2dom_ie (bgd_ie);
    const geo_element& dom_K = base::get_geo_element (map_d, dom_ie);
    return dom_K;
  } else {
    size_type dis_dom_ie = dis_bgd_ie2dis_dom_ie (dis_bgd_ie);
    const geo_element& dom_K = base::dis_get_geo_element (map_d, dis_dom_ie);
    return dom_K;
  }
}
template <class T, class M>
const geo_element&
geo_domain_rep<T,M>::dom2bgd_geo_element (const geo_element& dom_K) const
{
  size_type map_d = dom_K.dimension();
  size_type dom_dis_ie = dom_K.dis_ie();
  size_type first_dom_dis_ie = base::sizes().ownership_by_dimension[map_d].first_index();
  check_macro (dom_dis_ie >= first_dom_dis_ie, "unexpected dis_index "<<dom_dis_ie<<": out of local range");
  size_type dom_ie = dom_dis_ie - first_dom_dis_ie;
  const geo_basic<T,M>& dom = get_background_domain();
  const geo_element& bgd_K = dom[dom_ie];
  return bgd_K;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M)                             \
template class geo_domain_rep<T,M>;

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
