#ifndef _RHEO_INTEGRATE_INTERNAL_ICC
#define _RHEO_INTEGRATE_INTERNAL_ICC
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/geo_domain.h"
#include "rheolef/integrate.h"
namespace rheolef { namespace details{

// ----------------------------------------------
// numeric integration
// ----------------------------------------------

template <class T, class M, class Expr>
T
integrate_internal (const geo_basic<T,M>& omega, const rheolef::field_nonlinear_expr<Expr>& f, const quadrature_option& qopt, const T&)
{
  if (omega.map_dimension() < omega.get_background_geo().map_dimension()) {
    omega.get_background_geo().neighbour_guard();
  }
  space_basic<T,M> Xh (omega, "P0");
  test v (Xh);
  field_basic<T,M> lh = integrate (omega, f*v, qopt);
  return dual (1, lh);
}
template <class T, class M, class Expr>
typename rheolef::field_nonlinear_expr<Expr>::scalar_type
integrate_numeric (const geo_basic<T,M>& omega, const rheolef::field_nonlinear_expr<Expr>& f, const quadrature_option& qopt)
{
  typedef typename rheolef::field_nonlinear_expr<Expr>::scalar_type scalar_type;
  return integrate_internal (omega, f, qopt, scalar_type());
}

}}// namespace rheolef::details
#endif // _RHEO_INTEGRATE_INTERNAL_ICC
