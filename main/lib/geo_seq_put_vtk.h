#ifndef _RHEOLEF_GEO_SEQ_PUT_VTK_H
#define _RHEOLEF_GEO_SEQ_PUT_VTK_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// vtk visualization
//
// author: Pierre.Saramito@imag.fr
//
// date: 12 may 1997  update: 23 oct 2011
//
#include "rheolef/geo.h"
namespace rheolef { 

template <class T>
odiststream&
geo_put_vtk (
  odiststream&                               ops,
  const geo_basic<T,sequential>&             omega,
  const basis_basic<T>&                      my_numb,
  const disarray<point_basic<T>,sequential>& my_node,
  bool                                       append_data = true,
  size_t                                     subgeo_dim  = std::numeric_limits<size_t>::max());

}// namespace rheolef
#endif // _RHEOLEF_GEO_SEQ_PUT_VTK_H
