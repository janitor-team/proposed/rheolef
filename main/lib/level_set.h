#ifndef _RHEOLEF_LEVEL_SET_H
#define _RHEOLEF_LEVEL_SET_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@functionfile level_set zero level set of a field

Synopsis
========

        geo level_set (const field& fh);
        geo level_set (const field& fh, const level_set_option& lopt);

Description
===========
Given a @ref field_2 `fh` defined in a domain `Lambda`,
this function computes the level set defined by `{x in Lambda, fh(x) = 0}`.
This level set is represented by the @ref geo_2 class.

Options
=======
The option class `leve_set_option`
controls the slit of quadrilaterals into triangles
for tridimensional intersected surface and also the
zero machine precision, `epsilon`.

@snippet level_set.h verbatim_level_set_option

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/geo.h"

namespace rheolef {

// forward declaration:
template <class T, class M> class field_basic;

// [verbatim_level_set_option]
struct level_set_option {
  bool split_to_triangle; 
  Float epsilon;
  level_set_option()
   : split_to_triangle(true),
     epsilon(100*std::numeric_limits<Float>::epsilon())
     {}
};
// [verbatim_level_set_option]

template <class T, class M>
geo_basic<T,M> level_set (
  const field_basic<T,M>& fh,
  const level_set_option& opt = level_set_option());

// backward compat:
using level_set_option_type = level_set_option;

} // namespace
#endif // _RHEOLEF_LEVEL_SET_H
