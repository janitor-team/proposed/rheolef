#ifndef _RHEOLEF_KELLER_H
#define _RHEOLEF_KELLER_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/newton_add_missing.h"
#include "rheolef/keller_details.h"

namespace rheolef {

// switch depending on existing Problem::adapt() method
//! @brief see the @ref continuation_3 page for the full documentation
template<class Problem, class Sfinae = typename details::has_inherited_member_adapt<Problem>::type>
class keller {};

// -----------------------------
// class without mesh adaptation
// -----------------------------
template<class Problem>
class keller<Problem,std::false_type> {
public:
  typedef Float float_type;
  typedef details::pair_with_linear_algebra<float_type,typename Problem::value_type> value_type;
  keller(const Problem& pb, std::string metric="orthogonal");
  keller(const keller<Problem,std::false_type>&);
  void reset(const Problem& pb, std::string metric="orthogonal");
  float_type parameter() const { return s; }
  std::string parameter_name() const { return "s"; }
  Problem& get_problem() { return pb; }
  void set_parameter(float_type s1) { s = s1; }
  value_type initial (std::string restart="") const;
  void refresh (float_type s, const value_type& xh, const value_type& d_xh_ds) const;
  value_type direction (value_type& xh) const;
  value_type derivative_versus_parameter (const value_type& xh) const;
  value_type residue          (const value_type& xh) const;
  int solve (value_type& uh, const continuation_option& opts) const;
  solver::determinant_type update_derivative (const value_type& xh) const;
  value_type derivative_solve      (const value_type& mrh) const;
  value_type derivative_trans_mult (const value_type& mrh) const;
  float_type space_norm       (const value_type& xh) const;
  float_type dual_space_norm  (const value_type& mrh) const;
  float_type space_dot       (const value_type& xh, const value_type& yh) const;
  odiststream& put (odiststream& os, const value_type& xh) const;
  idiststream& get (idiststream& is, value_type& xh);
  bool stop (const value_type& xh) const { return pb.stop(xh.second); }
// adapt (dummy):
  void adapt     (const value_type&, const adapt_option&) {}
  void reset_geo (const value_type&) {}
  value_type reinterpolate (const value_type& xh) { return xh; }
protected:
  float_type s;
  mutable details::add_missing_continuation<Problem> pb;
  mutable csr<float_type> A1;
  mutable solver sA1;
  mutable typename Problem::value_type m_df_d_parameter;
  mutable float_type s0;
  mutable value_type xh0, d_xh0_ds;
  mutable bool has_init, has_refresh, has_spherical;
  mutable distributor u_ownership;
};
template<class Problem>
keller<Problem,std::false_type>::keller (const Problem& pb1, std::string metric)
 : s(0), pb(pb1), A1(), sA1(), m_df_d_parameter(), s0(0), xh0(), d_xh0_ds(),
   has_init(false), has_refresh(false), has_spherical(false), u_ownership()
{
  reset (pb1, metric);
}
template<class Problem>
keller<Problem,std::false_type>::keller (const keller<Problem,std::false_type>& x)
 : s(x.s), pb(x.pb), A1(x.A1), sA1(x.sA1), m_df_d_parameter(x.m_df_d_parameter),
   s0(x.s0), xh0(x.xh0), d_xh0_ds(x.d_xh0_ds),
   has_init(x.has_init), has_refresh(x.has_refresh), has_spherical(x.has_spherical),
   u_ownership(x.u_ownership)
{
  std::string metric = (!has_spherical) ? "orthogonal" : "spherical";
  reset (pb, metric);
}
template<class Problem>
void
keller<Problem,std::false_type>::reset (const Problem& pb1, std::string metric) {
  pb = pb1;
  check_macro (metric == "orthogonal" || metric == "spherical", "invalid metric="<<metric);
  has_spherical = (metric != "orthogonal");
}
template<class Problem>
typename
keller<Problem,std::false_type>::value_type
keller<Problem,std::false_type>::initial (std::string restart) const {
  s0 = 0;
  xh0.second = pb.initial (restart);
  xh0.first  = pb.parameter();
  typename Problem::value_type d_uh0_ds = pb.direction(xh0.second);
  float_type c = 1/sqrt(1 + sqr(pb.space_norm(d_uh0_ds)));
  d_xh0_ds.first  = c;
  d_xh0_ds.second = c*d_uh0_ds;
  has_init = true;
  return xh0;
}
template<class Problem>
typename
keller<Problem,std::false_type>::value_type
keller<Problem,std::false_type>::residue (const value_type& xh) const { 
  pb.set_parameter (xh.first);
  value_type mrh;
  if (!has_spherical) {
    mrh.first = space_dot(d_xh0_ds, xh-xh0) - (s-s0);
  } else {
    mrh.first = sqr(space_norm(xh-xh0)) - sqr(s-s0);
  }
  mrh.second = pb.residue (xh.second);
  return mrh;
}
template<class Problem>
typename solver::determinant_type
keller<Problem,std::false_type>::update_derivative (const value_type& xh) const {
  pb.set_parameter (xh.first);
  m_df_d_parameter = pb.derivative_versus_parameter(xh.second);
  form df_du = pb.derivative(xh.second);
  u_ownership = df_du.uu().row_ownership();
  vec<float_type> b2 = details::get_unknown (m_df_d_parameter, u_ownership);
  vec<float_type> b1;
  Float c;
  if (!has_spherical) {
    b1 = details::get_unknown (pb.massify(d_xh0_ds.second), u_ownership);
    c  = d_xh0_ds.first;
  } else {
    typename Problem::value_type duh = xh.second - xh0.second;
    b1 = details::get_unknown (pb.massify(Float(2)*duh), u_ownership);
    c  =  2*(xh.first - xh0.first);
  }
  A1 = {{ df_du.uu(), b2 },
        { trans(b1),  c  }};
  solver_option sopt;
  sopt.compute_determinant = true;
  sA1 = solver_basic<float_type> (A1, sopt);
  return sA1.det();
}
template<class Problem>
typename
keller<Problem,std::false_type>::value_type
keller<Problem,std::false_type>::derivative_solve (const value_type& mrh) const {
  vec<float_type> MB = { details::get_unknown(mrh.second,u_ownership),
                         mrh.first };
  vec<float_type> X = sA1.solve(MB);
  value_type delta_xh;
  delta_xh.second = mrh.second; details::reset (delta_xh.second);
  details::set_unknown (delta_xh.second, X);
  size_t iproc0 = 0;
  delta_xh.first = (size_t(X.comm().rank()) == iproc0) ? X[X.size()-1] : 0;
#ifdef _RHEOLEF_HAVE_MPI
  mpi::broadcast (X.comm(), delta_xh.first, iproc0);
#endif // _RHEOLEF_HAVE_MPI
  return delta_xh;
}
template<class Problem>
typename
keller<Problem,std::false_type>::value_type
keller<Problem,std::false_type>::derivative_trans_mult (const value_type& mrh) const {
  vec<float_type> MRH = { details::get_unknown(mrh.second,u_ownership),
                         mrh.first };
  vec<float_type> MGH = A1.trans_mult(MRH);
  value_type mgh = mrh; // for std::valarray alloc
  mgh.second = mrh.second; details::reset (mgh.second);
  details::set_unknown (mgh.second, MGH);
  size_t iproc0 = 0;
  mgh.first = (size_t(MGH.comm().rank()) == iproc0) ? MGH[MGH.size()-1] : 0;
#ifdef _RHEOLEF_HAVE_MPI
  mpi::broadcast (MGH.comm(), mgh.first, iproc0);
#endif // _RHEOLEF_HAVE_MPI
  return mgh;
}
template<class Problem>
int 
keller<Problem,std::false_type>::solve (value_type& uh, const continuation_option& opts) const {
  float_type res     = opts.tol;
  size_t     n_iter  = opts.max_iter;
  odiststream* p_err = &derr;
  int status = damped_newton (*this, uh, res, n_iter, p_err);
  if (p_err) *p_err << std::endl << std::endl;
  return ((status == 0 && res <= opts.tol) || sqr(res) <= opts.tol) ? 0 : 1;
}
template<class Problem>
typename
keller<Problem,std::false_type>::value_type
keller<Problem,std::false_type>::direction (value_type& xh) const {
  if (!has_refresh) return d_xh0_ds;
  update_derivative(xh);
  return -derivative_solve(derivative_versus_parameter(xh));
}
template<class Problem>
void
keller<Problem,std::false_type>::refresh (float_type s, const value_type& xh, const value_type& d_xh_ds) const { 
  has_refresh = true;
  d_xh0_ds = d_xh_ds;
  xh0 = xh;
  s0  = s;
}
template<class Problem>
typename
keller<Problem,std::false_type>::value_type
keller<Problem,std::false_type>::derivative_versus_parameter (const value_type& xh) const { 
  value_type m_df_ds;
  if (!has_spherical) {
    m_df_ds.first  = -1;
  } else {
    m_df_ds.first  = -2*(s-s0);
  }
  m_df_ds.second = xh.second; details::reset(m_df_ds.second); // allocate+reset
  return m_df_ds;
}
template<class Problem>
typename
keller<Problem,std::false_type>::float_type
keller<Problem,std::false_type>::space_dot (const value_type& xh, const value_type& yh) const {
  return xh.first*yh.first + pb.space_dot(xh.second, yh.second);
}
template<class Problem>
typename
keller<Problem,std::false_type>::float_type
keller<Problem,std::false_type>::space_norm (const value_type& xh) const {
  return sqrt (space_dot(xh,xh));
}
template<class Problem>
typename
keller<Problem,std::false_type>::float_type
keller<Problem,std::false_type>::dual_space_norm (const value_type& mrh) const {
  return sqrt (sqr(mrh.first) + sqr(pb.dual_space_norm(mrh.second)));
}
template<class Problem>
odiststream&
keller<Problem,std::false_type>::put (odiststream& os, const value_type& xh) const {
  static const bool trace = true;
  float_type prev_param = pb.parameter();
  pb.set_parameter(xh.first);
  if (trace) {
    static size_t n = 0;
    if (n==0) derr << "#[keller] n s "<<pb.parameter_name()<<" |x|"<<std::endl;
    derr << "[keller] " << n << " " << s << " " << xh.first << " "
         << pb.space_norm(xh.second) << std::endl;
    n++;
  }
  os << std::endl << "#s " << s << std::endl;
  pb.put(os,xh.second);
  pb.set_parameter(prev_param);
  return os;
}
template<class Problem>
idiststream&
keller<Problem,std::false_type>::get (idiststream& is, value_type& xh) {
  is >> catchmark("s") >> s;
  if (!is) return is;
  pb.get (is, xh.second);
  xh.first = pb.parameter();
  if (!has_init) { initial(); }
  return is;
}
// -----------------------------
// class with mesh adaptation
// -----------------------------
template<class Problem>
class      keller<Problem,std::true_type>
  : public keller<Problem,std::false_type>
{
public:
  typedef keller<Problem,std::false_type> base;
  typedef typename base::float_type       float_type;
  typedef typename base::value_type       value_type;
  keller    (const Problem& pb, std::string metric="orthogonal") : base(pb,metric) {}
// adapt:
  void adapt     (const value_type& xh, const adapt_option& aopt);
  void reset_geo (const value_type& xh);
  value_type reinterpolate (const value_type& xh);
};
template<class Problem>
void
keller<Problem,std::true_type>::adapt (const value_type& xh, const adapt_option& aopt) {
  base::pb.adapt (xh.second, aopt);
       base::xh0.second = base::pb.reinterpolate (base::xh0.second);
  base::d_xh0_ds.second = base::pb.reinterpolate (base::d_xh0_ds.second);
}
template<class Problem>
void
keller<Problem,std::true_type>::reset_geo (const value_type& xh) {
  base::pb.reset_geo (xh.second);
       base::xh0.second = base::pb.reinterpolate (base::xh0.second);
  base::d_xh0_ds.second = base::pb.reinterpolate (base::d_xh0_ds.second);
}
template<class Problem>
typename
keller<Problem,std::true_type>::value_type
keller<Problem,std::true_type>::reinterpolate (const value_type& xh) {
  typename keller<Problem>::value_type yh = xh;
  yh.second = base::pb.reinterpolate (yh.second);
  return yh;
}

} // namespace rheolef
#endif // _RHEOLEF_KELLER_H
