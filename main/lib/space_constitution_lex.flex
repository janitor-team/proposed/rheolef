%{
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// =========================================================================
// lexer for space_constitution header field files
//
// author: Pierre.Saramito@imag.fr
//
// date: 19 decembre 2002
//
%}
%option noyywrap
%x GEO_STATE
%%
[\n]     			{ /* newline */ ++space_constitution_line_no; }
[ \t]    			{ /* space  */; }
<INITIAL>"bubble"               { yylval.string_value = insert(yytext); return FAMILY_NO_INDEX; }
<INITIAL>"P1qd"                 { yylval.string_value = insert(yytext); return FAMILY_NO_INDEX; }
<INITIAL>"scalar"		{ yylval.string_value = insert(yytext); return VALUED; }
<INITIAL>"vector"		{ yylval.string_value = insert(yytext); return VALUED; }
<INITIAL>"tensor"		{ yylval.string_value = insert(yytext); return VALUED; }
<INITIAL>"unsymmetric_tensor"	{ yylval.string_value = insert(yytext); return VALUED; }
<INITIAL>"tensor4"		{ yylval.string_value = insert(yytext); return VALUED; }
<INITIAL>"cartesian"            { yylval.string_value = insert(yytext); return COORDINATE; }
<INITIAL>"rz"                   { yylval.string_value = insert(yytext); return COORDINATE; }
<INITIAL>"zr"                   { yylval.string_value = insert(yytext); return COORDINATE; }
<INITIAL>[0-9]+                 { yylval.string_value = insert(yytext); return INTEGER; }
<INITIAL>[a-zA-Z][a-zA-Z]*      { yylval.string_value = insert(yytext); return IDENTIFIER; }
<INITIAL>[\[\]=]		{ return *yytext; }
<INITIAL>"{"			{ BEGIN(GEO_STATE); return *yytext; }
<GEO_STATE>"}"			{ BEGIN(INITIAL); return *yytext; }
<GEO_STATE>[a-zA-Z0-9\-_=@$;\.\[\]]+ { yylval.string_value = insert(yytext); return GEO_NAME; }
"#".*    			{ /* comment */; }
.        			{ return *yytext ;  /* default */ }
%%

