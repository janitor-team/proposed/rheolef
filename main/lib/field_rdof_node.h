# ifndef _RHEOLEF_FIELD_RDOF_NODE_H
# define _RHEOLEF_FIELD_RDOF_NODE_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// field_rdof: unary+- ; lambda*rdof ; binary+-
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   12 may 2020

// SUMMARY
// 1. unary node
//    1.1. unary iterator
//    1.2. unary class
//    1.3. unary operator+-
//    1.4. multiplication & division by a constant scalar
// 2. binary node
//    TODO

#include "rheolef/field_rdof.h"
#include "rheolef/expression.h"

namespace rheolef {

// ============================================================================
// 1. unary node
// ============================================================================
namespace details {
// ----------------------------------------------------------------------------
// 1.1. unary iterator
// ----------------------------------------------------------------------------

template<class UnaryFunction, class InputIterator>
class field_rdof_unary_iterator {
public:

// definitions:

  using iterator_category = std::forward_iterator_tag;
  using size_type         = std::size_t;
  using value_type        = typename std::iterator_traits<InputIterator>::value_type;
  using reference         = const value_type&;
  using pointer           = const value_type*;
  using difference_type   = std::ptrdiff_t;
  using self_type         = field_rdof_unary_iterator<UnaryFunction,InputIterator>;

// allocators:

  field_rdof_unary_iterator() = delete;
  field_rdof_unary_iterator(const UnaryFunction& f, const InputIterator& iter)
  : _f(f), _iter(iter) {}

// accessors & modifiers:

  value_type operator*  ()            const { return _f(*_iter); }
  value_type operator[] (size_type n) const { return _f(*(_iter + n)); }

  self_type& operator++ () { ++_iter; return *this; }
  self_type  operator++ (int) { self_type tmp = *this; operator++(); return tmp; }
  self_type& operator+= (difference_type n) { _iter += n; return *this; }
  self_type  operator+  (difference_type n) const { self_type tmp = *this; return tmp += n; }

// comparators:

  bool operator== (const self_type& j) const { return _iter == j._iter; }
  bool operator!= (const self_type& j) const { return ! operator== (j); }

protected:
// data:
  UnaryFunction _f;
  InputIterator _iter;
};
// ----------------------------------------------------------------------------
// 1.2. unary class
// ----------------------------------------------------------------------------
template<class UnaryFunction, class FieldRdof>
class field_rdof_unary: public field_rdof_base<field_rdof_unary<UnaryFunction,FieldRdof>> {
public:
// definitions:

  using self_type   = field_rdof_unary<UnaryFunction,FieldRdof>;
  using base        = field_rdof_base<self_type>;
  using size_type   = typename FieldRdof::size_type;
  using scalar_type = typename FieldRdof::scalar_type;
  using memory_type = typename FieldRdof::memory_type;
  using float_type  = typename float_traits<scalar_type>::type;
  using geo_type    = geo_basic<float_type,memory_type>;
  using space_type  = space_basic<float_type,memory_type>;
  using const_iterator  = field_rdof_unary_iterator<UnaryFunction,typename FieldRdof::const_iterator>;

// allocators:

  field_rdof_unary (const UnaryFunction& f, const FieldRdof& expr) 
  : base(),
    _f(f),
    _expr(expr)
  {}

// accessors:

  const space_type& get_space() const { return _expr.get_space(); }
  const_iterator begin_dof() const { return const_iterator (_f, _expr.begin_dof()); }
  const_iterator end_dof()   const { return const_iterator (_f, _expr.end_dof()); }

protected:
  using field_rdof  = typename field_wdof2rdof_traits<FieldRdof>::type;
  UnaryFunction    _f;
  field_rdof       _expr;  
};
// concept:
template<class UnaryFunction, class FieldRdof>
struct is_field_rdof<field_rdof_unary<UnaryFunction,FieldRdof>>: std::true_type {};

template<class UnaryFunction, class FieldRdof>
struct field_traits<field_rdof_unary<UnaryFunction,FieldRdof>> {
  using size_type    = typename FieldRdof::size_type;
  using scalar_type  = typename FieldRdof::scalar_type;
  using memory_type  = typename FieldRdof::memory_type;
};

}// namespace details
// -------------------------------------------
// 1.3. unary operator+-
// -------------------------------------------

#define _RHEOLEF_make_field_rdof_unary(FUNCTION,FUNCTOR)			\
template<class FieldRdof>							\
typename									\
std::enable_if<									\
  details::has_field_rdof_interface<FieldRdof>::value				\
 ,details::field_rdof_unary<FUNCTOR,FieldRdof>					\
>::type										\
FUNCTION (const FieldRdof& rdof)						\
{										\
  return details::field_rdof_unary<FUNCTOR,FieldRdof> (FUNCTOR(), rdof); 	\
}

_RHEOLEF_make_field_rdof_unary (operator+, details::unary_plus)
_RHEOLEF_make_field_rdof_unary (operator-, details::negate)
#undef _RHEOLEF_make_field_rdof_unary

// ---------------------------------------------------
// 1.4. multiplication & division by a constant scalar
// ---------------------------------------------------
#define _RHEOLEF_make_field_rdof_unary_scalar_first(FUNCTION,FUNCTOR)		\
template<class FieldRdof>							\
typename									\
std::enable_if<									\
  details::has_field_rdof_interface<FieldRdof>::value				\
 ,details::field_rdof_unary<details::binder_first<FUNCTOR,typename FieldRdof::scalar_type>,FieldRdof>	\
>::type										\
FUNCTION (const typename FieldRdof::scalar_type& lambda, const FieldRdof& rdof)	\
{										\
  using A1 = details::binder_first<FUNCTOR,typename FieldRdof::scalar_type>;	\
  return details::field_rdof_unary<A1,FieldRdof> (A1(FUNCTOR(),lambda), rdof);	\
}

#define _RHEOLEF_make_field_rdof_unary_scalar_second(FUNCTION,FUNCTOR)	        \
template<class FieldRdof>							\
typename 									\
std::enable_if<									\
  details::has_field_rdof_interface<FieldRdof>::value				\
 ,details::field_rdof_unary<details::binder_second<FUNCTOR,typename FieldRdof::scalar_type>,FieldRdof>	\
>::type										\
FUNCTION (const FieldRdof& rdof, const typename FieldRdof::scalar_type& lambda)	\
{										\
  using A2 = details::binder_second<FUNCTOR,typename FieldRdof::scalar_type>;	\
  return details::field_rdof_unary<A2,FieldRdof> (A2(FUNCTOR(),lambda), rdof);	\
}

_RHEOLEF_make_field_rdof_unary_scalar_first (operator+, details::plus)
_RHEOLEF_make_field_rdof_unary_scalar_first (operator-, details::minus)
_RHEOLEF_make_field_rdof_unary_scalar_first (operator*, details::multiplies)
_RHEOLEF_make_field_rdof_unary_scalar_second(operator+, details::plus)
_RHEOLEF_make_field_rdof_unary_scalar_second(operator-, details::minus)
_RHEOLEF_make_field_rdof_unary_scalar_second(operator*, details::multiplies)
_RHEOLEF_make_field_rdof_unary_scalar_second(operator/, details::divides)
#undef _RHEOLEF_make_field_rdof_unary_scalar_first
#undef _RHEOLEF_make_field_rdof_unary_scalar_second

}// namespace rheolef
# endif /* _RHEOLEF_FIELD_RDOF_NODE_H */
