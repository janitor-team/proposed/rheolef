///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/characteristic.h"
#include "rheolef/fem_on_pointset.h"
#include "rheolef/rheostream.h"
#include "rheolef/band.h"
#include "rheolef/field_evaluate.h"

// TODO: replace piola_on_quad aka basis_on_pointset by piola_on_poinset

// ===========================================================================
// allocator
// ===========================================================================
namespace rheolef {

template<class T, class M>
characteristic_rep<T,M>::characteristic_rep(const field_basic<T,M>& dh)
  : _dh(dh),
    _coq_map()
{
  // automatically add the "boundary" domain, if not already present:
  _dh.get_geo().boundary();
}

} // namespace rheolef
// ===========================================================================
// externs
// ===========================================================================
namespace rheolef { namespace details { 

template<class T, class M>
void
interpolate_pass1_symbolic (
    const geo_basic<T,M>&                      omega,
    const disarray<point_basic<T>,M>&          x,
    const disarray<geo_element::size_type,M>&  ix2dis_ie, // x has been already localized in K
          disarray<index_set,M>&               ie2dis_ix, // K -> list of ix
          disarray<point_basic<T>,M>&          hat_y);

// ===========================================================================
// integrate(omega,compose (uh,X)) returns a field:
// It is done in two passes:
// - the first one is symbolic: localize the moved quadrature points yq=xq+X.dh(xq)
//   this is done one time for all 
// - the second pass is valued: compte uh(yq) during integration
// ===========================================================================
template <class T, class M>
void
integrate_pass1_symbolic (
 // input :
    const space_basic<T,M>&                Xh,
    const field_basic<T,M>&                dh,
    const piola_on_pointset<T>&            pops,
 // output :
          disarray<index_set,M>&           ie2dis_ix,
          disarray<point_basic<T>,M>&      hat_y,
          disarray<point_basic<T>,M >&     yq)
{
  // ----------------------------------------------------------------------
  // 1) set the quadrature formulae
  // ----------------------------------------------------------------------
  typedef typename space_basic<T,M>::size_type size_type;
  const geo_basic<T,M>& omega = Xh.get_geo();
  check_macro (omega == dh.get_geo(), "characteristic: unsupported different meshes for flow ("
		<< dh.get_geo().name() << ") and convected field (" << omega.name() << ")");
  if (omega.order() > 1) {
    warning_macro ("characteristic: high-order curved elements not yet supported (treated as first order)");
  }
  fem_on_pointset<T> d_fops;
  d_fops.initialize (dh.get_space().get_constitution().get_basis(), pops);
  // ----------------------------------------------------------------------
  // 2) size of the the disarray of all quadrature nodes
  // ----------------------------------------------------------------------
  // NOTE: since these nodes are moved (convected) by the flow associated to the characteristic
  // they can change from partition and go to an element managed by another proc
  // thus, in order to group comms, a whole disarray of quadrature point is allocated:
  // In sequential mode (or with only one proc), this can do in an element by element way
  // with less memory.
  size_type dim     = omega.dimension();
  size_type map_dim = omega.map_dimension();
  size_type     sz = 0;
  size_type dis_sz = 0;
  const basis_on_pointset<T>& piola_on_quad = pops.get_basis_on_pointset();
  for (size_type variant = reference_element::first_variant_by_dimension(map_dim);
                 variant < reference_element:: last_variant_by_dimension(map_dim); variant++) {
    distributor ige_ownership = omega.sizes().ownership_by_variant [variant];
    if (ige_ownership.dis_size() == 0) continue;
    reference_element hat_K (variant);
    size_type nq = piola_on_quad.nnod (hat_K);
        sz += nq*ige_ownership.size();
    dis_sz += nq*ige_ownership.dis_size();
  }
  distributor xq_ownership (dis_sz, omega.comm(), sz);
  // ----------------------------------------------------------------------
  // 3) build the disarray of all quadrature nodes; xq
  // ----------------------------------------------------------------------
  disarray<point_basic<T>,M> xq (xq_ownership);
  std::vector<size_type> dis_inod;
  for (size_type ixq = 0, ie = 0, ne = omega.size(); ie < ne; ie++) {
    const geo_element& K = omega[ie];
    const Eigen::Matrix<piola<T>,Eigen::Dynamic,1>& piola = pops.get_piola (omega, K);
    for (size_type q = 0, nq = piola.size(); q < nq; q++, ixq++) {
      xq[ixq] = piola[q].F;
    }
  }
  // ----------------------------------------------------------------------
  // 4) interpolate dh on the xq nodes: dq(i)=dh(xq(i))
  // ----------------------------------------------------------------------
  dh.dis_dof_update();
  disarray<point_basic<T>,M> dq        (xq_ownership);
  disarray<size_type,M>      ix2dis_ie (xq_ownership);
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1> d_value;
  for (size_type ixq = 0, ie = 0, ne = omega.size(); ie < ne; ie++) {
    const geo_element& K = omega[ie];
    field_evaluate (dh, d_fops, omega, K, d_value);
    for (size_type q = 0, nq = d_value.size(); q < nq; q++, ixq++) {
      dq[ixq] = d_value [q];
      ix2dis_ie[ixq] = ie; // will be a guest for locate(yq)
    }
  }
  // ----------------------------------------------------------------------
  // 5) build the disarray of all moved quadrature nodes yq(i) = xq(i)+dq(i)
  //    with the constraint to remain inside Omega
  // ----------------------------------------------------------------------
  yq.resize (xq_ownership);
  omega.trace_move (xq, dq, ix2dis_ie, yq);
  // ----------------------------------------------------------------------
  // 6.a) symbolic interpolation-like pass: localize the yq nodes
  // ----------------------------------------------------------------------
  interpolate_pass1_symbolic (omega, yq, ix2dis_ie, ie2dis_ix, hat_y);
}

} // namespace details
// ========================================================================
// symbolic pass 1 is stored one time for all in the characteristic class
// ========================================================================
template<class T, class M>
const characteristic_on_quadrature<T,M>&
characteristic_rep<T,M>::get_pre_computed (
    const space_basic<T,M>&       Xh,
    const field_basic<T,M>&       dh,
    const piola_on_pointset<T>&   pops) const
{
  // get the quadrature used for integration
  check_macro (pops.has_quadrature(), "unexpected point set, expect quadrature point set");
  const quadrature<T>&     quad = pops.get_quadrature();
  const quadrature_option& qopt = quad.get_options();

  check_macro (qopt.get_family() == quadrature_option::gauss_lobatto,
           "integrate characteristics HINT: use Gauss-Lobatto quadrature)");
  check_macro (qopt.get_order() != std::numeric_limits<quadrature_option::size_type>::max(),
           "integrate characteristics HINT: invalid unset order");

  // search if already used & memorized
  std::string label = qopt.get_family_name() + "(" + std::to_string(qopt.get_order()) + ")";
  typename map_type::const_iterator iter = _coq_map.find (label);
  if (iter != _coq_map.end()) {
    return (*iter).second;
  }
  // build a new one & memorize it
  // search if already used & memorized
  characteristic_on_quadrature<T,M> coq (qopt);
  characteristic_on_quadrature_rep<T,M>& coq_r = coq.data();
  coq_r._qopt          = qopt;
  coq_r._quad          = quad;
  coq_r._piola_on_quad = pops.get_basis_on_pointset();
  details::integrate_pass1_symbolic (Xh, dh, pops,
    coq_r._ie2dis_ix,
    coq_r._hat_y, 
    coq_r._yq);

 // output :
  std::pair <typename map_type::iterator,bool> iter_b = _coq_map.insert (std::make_pair(label,coq));
  typename map_type::iterator iter2 = iter_b.first;
  return (*iter2).second;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M)                             \
template class characteristic_rep<T,M>;				\

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

}// namespace rheolef
