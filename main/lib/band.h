#ifndef _RHEOLEF_BAND_H
#define _RHEOLEF_BAND_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@classfile band banded level set method
@addindex  level set

Description
===========
Let `fh` be a finite element function
defined on a geometric domain `Lambda`.
Then, the set of elements intersecting the
zero level set is defined by `{x in Lambda, fh(x) = 0}`
and is called a *band*. 
This class represents such a band: it is used for solving problems defined on a 
surface, when the surface itself is implicitly
defined by a @ref level_set_3 function.

Acessors
========
Each side in the surface mesh, as returned by the `level_set` member
function, is included into an element of the band mesh, as returned by the
`band` member function.
Moreover, in the distributed memory environment,
this correspondence is on the same local memory,
so local indexes can be used for this correspondence:
it is provided by the `sid_ie2bnd_ie` member function.

Topology and domains
====================
For the direct resolution of systems posed on the band, the mesh returned
by the `band()` member function provides some domains of vertices.
The `"zero"` vertex domain lists all vertices `xi` such that `fh(xi)=0`.
The `"isolated"` vertex domain lists all vertices `xi`
such that `fh(xi)!=0` and `xi` is contained by only one element `K`
and all vertices `xj!=xi` of `K` satisfies `fh(xj)=0`.
Others vertices of the band, separated by the zero and isolated ones,
are organized by connected components:
the `n_connex_component` member function returns its number.
Corresponding vertex domains of the band are named `"cci"`
where *i* should be replaced
by any number between 0 and `n_connex_component-1`,
e.g. `cc0`, `cc1`, etc.

Implementation
==============
@showfromfile
The `band` class is simply an alias to the `band_basic` class

@snippet band.h verbatim_band
@par

The `band_basic` class provides an interface,
via the @ref smart_pointer_7 class family,
to a mesh container:

@snippet band.h verbatim_band_basic
@snippet band.h verbatim_band_basic_cont
*/
} // namespace rheolef

#include "rheolef/level_set.h"

namespace rheolef {

// [verbatim_band_basic]
template <class T, class M = rheo_default_memory_model>
class band_basic {
public:

  typedef typename geo_basic<T,M>::size_type size_type;

// allocators:

  band_basic();
  band_basic(const field_basic<T,M>& fh,
    const level_set_option& opt = level_set_option());

// accessors:

  const geo_basic<T,M>& band() const { return _band; }
  const geo_basic<T,M>& level_set() const { return _gamma; }
  size_type sid_ie2bnd_ie (size_type sid_ie) const { return _sid_ie2bnd_ie [sid_ie]; }
  size_type n_connected_component() const { return _ncc; }
// [verbatim_band_basic]

// data:
protected:
  geo_basic<T,M>        _gamma;
  geo_basic<T,M>        _band;
  disarray<size_type,M> _sid_ie2bnd_ie;
  size_type             _ncc;
// [verbatim_band_basic_cont]
};
// [verbatim_band_basic_cont]

// [verbatim_band]
typedef band_basic<Float> band;
// [verbatim_band]

// ----------------------------------------------------------------------------
// inlined
// ----------------------------------------------------------------------------
template<class T, class M>
inline
band_basic<T,M>::band_basic ()
  : _gamma(),
    _band(),
    _sid_ie2bnd_ie(),
    _ncc(0)
{
}

} // namespace
#endif // _RHEOLEF_BAND_H
