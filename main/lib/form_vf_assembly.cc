///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
#include "rheolef/form_vf_assembly.h"
#include "rheolef/eigen_util.h"

namespace rheolef { namespace details {
using namespace std;

// ------------------------------------------------------------------------------
// norm_max
// ------------------------------------------------------------------------------
template <class T>
T
norm_max (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& m) {
  T m_max = 0;
  for (size_t i = 0; i < size_t(m.rows()); i++) {
  for (size_t j = 0; j < size_t(m.cols()); j++) {
    m_max = std::max (m_max, m(i,j));
  }}
  return m_max;
}
// ------------------------------------------------------------------------------
// is_symmetric
// ------------------------------------------------------------------------------
template <class T>
bool
check_is_symmetric (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& m, const T& tol_m_max) {
  if (m.rows() != m.cols()) return false; // non-square matrix
  const T eps = std::numeric_limits<T>::epsilon();
  if (tol_m_max < sqr(eps)) return true; // zero matrix
  for (size_t i = 0;   i < size_t(m.rows()); i++) {
  for (size_t j = i+1; j < size_t(m.cols()); j++) {
    if (abs(m(i,j) - m(j,i)) > tol_m_max) return false;
  }}
  return true;
}
// ------------------------------------------------------------------------------
// local lump
// ------------------------------------------------------------------------------
template <class T>
void
local_lump (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& m) {
  check_macro (m.rows() == m.cols(), "unexpected rectangular matrix for lumped mass");
  for   (size_t i = 0; i < size_t(m.rows()); i++) {
    T sum = 0;
    for (size_t j = 0; j < size_t(m.cols()); j++) {
      sum += m(i,j);
      m(i,j) = T(0);
    }
    m(i,i) = sum;
  }
}
// ------------------------------------------------------------------------------
// local inversion
// ------------------------------------------------------------------------------
template <class T>
void
local_invert (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& m, bool is_diag) {
    check_macro (m.rows() == m.cols(), "unexpected rectangular matrix for local invert");
    if (is_diag) {
      // matrix has been lumped: easy diagonal inversion
      for (size_t i = 0; i < size_t(m.rows()); i++) {
        m(i,i) = 1./m(i,i);
      }
      return;
    }
    // general inversion
    Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> inv_m (m.rows(),m.cols());
    bool status = invert(m,inv_m);
    if (!status) {
#ifndef TO_CLEAN
      std::ofstream out ("inv_failed.mtx");
      put_matrix_market (out, m);
      out.close();
#endif // TO_CLEAN
      error_macro("singular element matrix founded");
    }
    m = inv_m;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciate(T)								\
template T norm_max (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>& m);		\
template bool check_is_symmetric (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&, const T&);	\
template void local_lump   (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&);		\
template void local_invert (Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&, bool);	\

_RHEOLEF_instanciate(Float)

}} // namespace rheolef::details
