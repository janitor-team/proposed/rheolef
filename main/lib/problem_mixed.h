#ifndef _RHEOLEF_PROBLEM_MIXED_H
#define _RHEOLEF_PROBLEM_MIXED_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// AUTHORS: Pierre.Saramito@imag.fr
// DATE:   21 february 2020

namespace rheolef {
/**
@classfile problem_mixed linear solver

Description
===========
The `problem_mixed` class solves a given linear mixed system
for PDEs in variational formulation.

Mixed problem appear in many applications such as 
the Stokes or the nearly incompressible elasticity.
Let `Xh` and `Qh` be two finite element @ref space_2.
Let `a: Xh*Xh --> IR`,
    `b: Xh*Qh --> IR`
and `c: Xh*Qh --> IR`
be three bilinear @ref form_2.
The system writes:

        [ A  B^T ] [ uh ]   [ lh ]
        [        ] [    ] = [    ]
        [ B  -C  ] [ ph ]   [ kh ]

where `A`, `B` and `C` are the three operators associated
to `a`, `b` and `c`, respectively,
and `lh in Xh` and `kh in Qh` are the two given right-hand-sides.
The `c` bilinear form is called the stabilization, and
could be zero.

The degrees of freedom are split between *unknown*
degrees of freedom and *blocked* one.
See also @ref form_2 and @ref space_2.
The linear system expands as:

        [ a.uu  a.ub   b.uu^T b.bu^T ] [ uh.u ]   [ lh.u ]
	[                            ] [      ] = [      ]
        [ a.bu  a.bb   b.ub^T b.bb^T ] [ uh.b ]   [ lh.b ]
        [                            ] [      ] = [      ]
        [ b.uu  b.ub  -c.uu  -c.ub   ] [ ph.u ]   [ kh.u ]
        [                            ] [      ] = [      ]
        [ b.bu  a.bb  -c.bu  -c.bb   ] [ ph.b ]   [ kh.b ]

Both the `uh.b` and `ph.b` are blocked degrees of freedom:
their values are prescribed and the corresponding values
are move to the right-hand-side of the system that reduces to:

        a.uu*uh.u + b.uu^T*ph.u = lh.u - a.ub*uh.b - b.bu^T*ph.b

        b.uu*uh.u -   c.uu*ph.u = kh.u - b.ub*uh.b +   c.ub*ph.b

This writes:

        problem_mixed p (a, b, c);
        p.solve (lh, kh, uh, ph);

When `c` is zero, the last `c` argument could be omitted, as:

        problem_mixed p (a, b);

Observe that, during the `p.solve` call,
`uh` is both an input variable, for the `uh.b`
contribution to the right-hand-side,
and an output variable, with `uh.u`.
This is also true for `ph` when `ph.b` is non-empty.
The previous linear system is solved via the @ref solver_abtb_4 class:
the `problem_mixed` class is simply a convenient wrapper around the @ref solver_abtb_4 one.

Example
=======
See @ref stokes_cavity.cc

Options
=======
The @ref solver_abtb_4 could be customized via the constructor optional
@ref solver_option_4 argument:

        problem p (a, b, sopt);

or

        problem p (a, b, c, sopt);

This @ref solver_abtb_4 is used to switch between a direct
and an iterative method for solving the mixed system.

Metric
======
A *metric* in the `Qh` space could also be provided:

	p.set_metric (mp);

By default, when nothing has been specified,
this metric is the L2 scalar product for the `Qh` space.

When using a direct @ref solver_4 method, 
this metric is used to add a constraint when
the multiplier is known up to a constant.
For instance, when the pressure is known up to a constant,
a constraint for a zero average pressure is automatically added
to the linear system.

When using an iterative @ref solver_4 method, 
this metric is used as a Schur complement preconditionner,
see @ref solver_abtb_4 for details.
The @ref solver_4 associated to this Schur preconditionner 
could be customized by specifying the @ref solver_4 associated
to the @ref problem_2 related to the `mp` operator:

    p.set_preconditionner (pmp);

Note this preconditionner subproblem
could be solved either by a direct or an iterative method,
while the whole mixed problem solver is iterative.

Inner solver
============
When using an iterative @ref solver_abtb_4,
the inner @ref problem_2 related to the `a` operator could also be customized
by specifying its @ref solver_4 :

	p.set_inner_problem (pa);

Note this inner subproblem
could be solved either by a direct or an iterative method,
while the whole mixed problem solver is iterative.

Implementation
==============
@showfromfile
The `problem_mixed` class is simply an alias to the `problem_mixed_basic` class

@snippet problem_mixed.h verbatim_problem_mixed
@par

The `problem_mixed_basic` class provides a generic interface:

@snippet problem_mixed.h verbatim_problem_mixed_basic
@snippet problem_mixed.h verbatim_problem_mixed_basic_cont
*/
} // namespace rheolef

#include "rheolef/problem.h"

namespace rheolef {

// [verbatim_problem_mixed_basic]
template <class T, class M = rheo_default_memory_model>
class problem_mixed_basic {
public:

// typedefs:
 
  typedef typename solver_basic<T,M>::size_type         size_type;
  typedef typename solver_basic<T,M>::determinant_type  determinant_type;

// allocators:

  problem_mixed_basic();

  problem_mixed_basic(
      const form_basic<T,M>& a,
      const form_basic<T,M>& b,
      const solver_option& sopt = solver_option());

  problem_mixed_basic(
      const form_basic<T,M>& a,
      const form_basic<T,M>& b,
      const form_basic<T,M>& c,
      const solver_option& sopt = solver_option());

// accessor:

  void solve (const field_basic<T,M>& lh, const field_basic<T,M>& kh, 
                    field_basic<T,M>& uh,       field_basic<T,M>& ph) const;

  bool initialized() const;
  const solver_option&     option() const;

// modifiers:

  void set_metric          (const form& mp);
  void set_preconditionner (const problem& pmp);
  void set_inner_problem   (const problem& pa);
// [verbatim_problem_mixed_basic]

protected:
// internal:

  void _init_s() const;

// data:

  form_basic<T,M>                _a, _b, _c;
  solver_option                  _sopt;
  mutable form_basic<T,M>        _mp;
  problem_basic<T,M>             _pmp, _pa;
  mutable solver_abtb_basic<T,M> _s;
  mutable bool                   _init_s_done;

// [verbatim_problem_mixed_basic_cont]
};
// [verbatim_problem_mixed_basic_cont]

//! @brief see the @ref problem_mixed_2 page for the full documentation
// [verbatim_problem_mixed]
typedef problem_mixed_basic<Float> problem_mixed;
// [verbatim_problem_mixed]

// -----------------------------------------------------------------------------
// inlined
// -----------------------------------------------------------------------------
template<class T, class M>
inline
problem_mixed_basic<T,M>::problem_mixed_basic()
 : _a(),
   _b(),
   _c(),
   _sopt(),
   _mp(),
   _pmp(),
   _pa(),
   _s(),
   _init_s_done(false)
{
}
template<class T, class M>
inline
problem_mixed_basic<T,M>::problem_mixed_basic (
  const form_basic<T,M>& a,
  const form_basic<T,M>& b,
  const solver_option&   sopt)
 : _a(a),
   _b(b),
   _c(),
   _sopt(),
   _mp(),
   _pmp(),
   _pa(),
   _s(),
   _init_s_done(false)
{
}
template<class T, class M>
inline
problem_mixed_basic<T,M>::problem_mixed_basic (
  const form_basic<T,M>& a,
  const form_basic<T,M>& b,
  const form_basic<T,M>& c,
  const solver_option&   sopt)
 : _a(a),
   _b(b),
   _c(c),
   _sopt(),
   _mp(),
   _pmp(),
   _pa(),
   _s(),
   _init_s_done(false)
{
}
template<class T, class M>
inline
bool
problem_mixed_basic<T,M>::initialized() const
{
  return _s.initialized();
}
template<class T, class M>
inline
const solver_option&
problem_mixed_basic<T,M>::option() const
{
  return _s.option();
}
template<class T, class M>
inline
void
problem_mixed_basic<T,M>::set_metric (const form& mp)
{
  _mp = mp;
}
template<class T, class M>
inline
void
problem_mixed_basic<T,M>::set_preconditionner (const problem& pmp)
{
  _pmp = pmp;
}
template<class T, class M>
inline
void
problem_mixed_basic<T,M>::set_inner_problem (const problem& pa)
{
  _pa = pa;
}
template<class T, class M>
inline
void
problem_mixed_basic<T,M>::_init_s() const
{
  if (_init_s_done) return;
  _init_s_done = true;
  if (_mp.uu().dis_nnz() == 0) {
    // default mp metric is the Qh L2 scalar product:
    const space_basic<T,M> Qh = _b.get_second_space();
    test_basic<T,M,details::vf_tag_01> p (Qh);
    test_basic<T,M,details::vf_tag_10> q (Qh);
    _mp = integrate (p*q);
  }
  if (_c.uu().dis_nnz() == 0) {
    _s = solver_abtb_basic<T,M> (_a.uu(), _b.uu(),          _mp.uu(), _sopt);
  } else {
    _s = solver_abtb_basic<T,M> (_a.uu(), _b.uu(), _c.uu(), _mp.uu(), _sopt);
  }
  if (_pmp.initialized()) { _s.set_preconditioner (_pmp.get_solver()); }
  if ( _pa.initialized()) { _s.set_inner_solver   ( _pa.get_solver()); }
}
template<class T, class M>
inline
void
problem_mixed_basic<T,M>::solve (
  const field_basic<T,M>& lh,
  const field_basic<T,M>& kh, 
        field_basic<T,M>& uh,
        field_basic<T,M>& ph) const
{
  _init_s();
  vec<T,M> f = lh.u() - _a.ub()*uh.b() - _b.bu().trans_mult(ph.b());
  vec<T,M> g;
  if (_c.uu().dis_nnz() == 0) {
    g = kh.u() - _b.ub()*uh.b();
  } else {
    g = kh.u() - _b.ub()*uh.b() + _c.ub()*ph.b();
  }
  _s.solve (f, g, uh.set_u(), ph.set_u());
}

} // namespace rheolef
#endif // _RHEOLEF_PROBLEM_MIXED_H
