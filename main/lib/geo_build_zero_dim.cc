///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is sequential in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// used by space IR = space(zero,"P1");
//   zero.geo = 0D, 1 node, as in file:
//	mesh
//	4
//	header
//	 dimension 0
//	 nodes     1
//	end header
//
// NOTE:
// - the only node is attributed to iproc=0
//   used by field_concat.cc & form_concat.cc
//   and this attribution convention
//   is assumed in vec_concat.cc & csr_concat.cc
//
// TODO:
// - build it directly, without any file
// - for space IR^n : use a "n" parameter
//
#include "rheolef/geo.h"

namespace rheolef {

static std::string zero_full_name() {
  std::string full_name = std::string(_RHEOLEF_PKGDATADIR) + "/zero-1";
  if (file_exists (full_name + ".geo")) return full_name;
  // second chance: regression tests running with non-installed lib
  full_name = std::string(_RHEOLEF_ABS_TOP_SRCDIR) + "/main/lib/zero-1";
  if (file_exists (full_name + ".geo")) return full_name;
  fatal_macro ("cannot find \"zero-1.geo\"");
  return std::string();
}
#define _RHEOLEF_zero_dimension(M)				\
template <class T>						\
geo_basic<T,M>::geo_basic (					\
  details::zero_dimension,					\
  const communicator& comm)					\
: base (0)							\
{								\
  bool prev_verbose = iorheo::getverbose(std::clog);		\
  std::clog << noverbose;					\
  base::operator= (geo_load<T,M>(zero_full_name()));		\
  if (prev_verbose) std::clog << verbose;			\
}
_RHEOLEF_zero_dimension(sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_zero_dimension(distributed)
#endif // _RHEOLEF_HAVE_MPI
#undef _RHEOLEF_zero_dimension
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M)                             \
template 							\
geo_basic<T,M>::geo_basic (					\
  details::zero_dimension,					\
  const communicator& comm);

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI
#undef _RHEOLEF_instanciation


} // namespace rheolef
