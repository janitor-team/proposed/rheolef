#ifndef _RHEOLEF_FIELD_EXPR_TERMINAL_H
#define _RHEOLEF_FIELD_EXPR_TERMINAL_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// terminals (leaves) for the non-linear expression tree
//
// 0) utilities
//    0.1) concepts
//    0.2) terminal wrapper for exprs
//    0.2) utility for evaluation on sides
// 1) class-function
//    1.1) base class for the class-function family
//    1.2) general function or class-function
//    1.3) normal to a surface
//    1.4) h_local
//    1.5) penalty
// 2) field and such
//    2.1) field
//    2.2) jump of a field
// 3) convected field, as compose(uh,X) where X is a characteristic
//
#include "rheolef/field.h"
#include "rheolef/field_wdof_sliced.h"
#include "rheolef/field_expr_utilities.h"
#include "rheolef/expression.h"
#include "rheolef/form.h"
#include "rheolef/basis_on_pointset.h"
#include "rheolef/test.h"
#include "rheolef/characteristic.h"

namespace rheolef { 
// -------------------------------------------------------------------
// 0) utilities
// -------------------------------------------------------------------
namespace details {

#ifdef TO_CLEAN
// -------------------------------------------------------------------
// 0.1) concepts
// -------------------------------------------------------------------
// Define a trait type for detecting field expression valid arguments
// -> constant and field_convertible are valid terminals:
template<class Expr, class Sfinae = void> struct is_field_expr_v2_nonlinear_arg : std::false_type {};
template<class Expr>                      struct is_field_expr_v2_nonlinear_arg <Expr, typename std::enable_if<
    						 is_field_expr_v2_constant<Expr>::value>::type> : std::true_type {};
template<class Expr>                      struct is_field_expr_v2_nonlinear_arg <Expr, typename std::enable_if<
    						      has_field_rdof_interface<Expr>::value
                                                 >::type> : std::true_type {};

// Define a trait type for detecting linear & homogeneous expressions
// these expressions should act homogeneously in the same finite element space
template<class Expr>                   struct is_field_expr_affine_homogeneous <Expr, typename std::enable_if<
                                              is_field_expr_v2_constant<Expr>::value>::type> : std::true_type {};
template<class Expr>                   struct is_field_expr_affine_homogeneous <Expr, typename std::enable_if<
    			                              has_field_rdof_interface<Expr>::value
                                              >::type> : std::true_type {};
#endif // TO_CLEAN
// -------------------------------------------------------------------
// 0.2) terminal wrapper for exprs
// -------------------------------------------------------------------
template <class Expr, class Sfinae = void>
struct field_expr_v2_nonlinear_terminal_wrapper_traits
{ 
  // catch-all case: non-terminals are not wrapped
  typedef Expr   type;
};
// -------------------------------------------------------------------
// 0.3) utility for evaluation on sides
// -------------------------------------------------------------------
template<class T, class M>
const geo_element& global_get_side (const geo_basic<T,M>& omega, const geo_element& L, const side_information_type& sid);

} // namespace details
// ---------------------------------------------------------------------------
// 1. field-function
// ---------------------------------------------------------------------------
// 1.1. base class for the field-function family
// ---------------------------------------------------------------------------
namespace details {

template<class T>
class field_expr_v2_nonlinear_terminal_function_base_rep {
public:
// typedefs:

  typedef geo_element::size_type                                size_type;
  typedef rheo_default_memory_model                             memory_type; // TODO: deduce it
  typedef T                                                     scalar_type;
  typedef T                                                     float_type;

// allocators:

  field_expr_v2_nonlinear_terminal_function_base_rep ();
  field_expr_v2_nonlinear_terminal_function_base_rep (const field_expr_v2_nonlinear_terminal_function_base_rep<T>&);

// accessors:

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  template<class M>
  const geo_element&
  get_side (
    const geo_basic<float_type,M>& omega_K,
    const geo_element&             K,
    const side_information_type&   sid) const;

// data:
protected:
  mutable piola_on_pointset<float_type>     _pops;
};
// ---------------------------------------------------------------------------
// 1.2) general function or class-function
// ---------------------------------------------------------------------------
template<class Function>
class field_expr_v2_nonlinear_terminal_function_rep 
 : field_expr_v2_nonlinear_terminal_function_base_rep
     <typename float_traits<typename details::function_traits<Function>::result_type>::type>
{
public:
// typedefs:

  typedef geo_element::size_type                                size_type;
  typedef rheo_default_memory_model                             memory_type; // TODO: how to deduce it ?
  typedef typename details::function_traits<Function>::copiable_type  function_type;
  typedef typename details::function_traits<Function>::result_type   result_type;
  typedef typename details::function_traits<Function>::template arg<0>::type argument_type;
  typedef typename scalar_traits<result_type>::type             scalar_type;
  typedef typename  float_traits<result_type>::type             float_type;
  typedef result_type                                           value_type;
  typedef field_expr_v2_nonlinear_terminal_function_base_rep<float_type>     base;

// alocators:

  field_expr_v2_nonlinear_terminal_function_rep(const Function& f);

// accessors:

  static const space_constant::valued_type valued_hint = space_constant::valued_tag_traits<result_type>::value;

  space_constant::valued_type valued_tag() const {
      return space_constant::valued_tag_traits<result_type>::value; }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::initialize (pops, iopt); }

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::initialize (Xh, pops, iopt); }

  void evaluate (
    const geo_basic<float_type,memory_type>&           omega_K,
    const geo_element&                                 K,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&       value) const
  {
    const Eigen::Matrix<piola<float_type>,Eigen::Dynamic,1>& piola = base::_pops.get_piola (omega_K, K);
    reference_element hat_K = K.variant();
    size_type loc_nnod = piola.size();
    value.resize (loc_nnod);
    for (size_type loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
      const point_basic<float_type>& xi = piola[loc_inod].F;
      value[loc_inod] = _f (xi);
    }
  }
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&           omega_L,
    const geo_element&                                 L,
    const side_information_type&                       sid,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&       value) const
    	// TODO QUESTION_SIDE: is there some rotation and orientation to apply to the value order ?
    	{ evaluate (omega_L, base::get_side(omega_L,L,sid), value); }

  template<class Value>
  bool valued_check() const {
    static const bool status = is_equal<Value,result_type>::value;
    check_macro (status, "unexpected result_type");
    return status;
  }
// data:
protected:
  function_type  _f;
// working area:
public:
  mutable std::array<
    Eigen::Matrix<scalar_type,Eigen::Dynamic,1>
   ,reference_element::max_variant>                           	_scalar_val;
  mutable std::array<
    Eigen::Matrix<point_basic<scalar_type>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_vector_val;
  mutable std::array<
    Eigen::Matrix<tensor_basic<scalar_type>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_tensor_val;
  mutable std::array<
    Eigen::Matrix<tensor3_basic<scalar_type>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_tensor3_val;
  mutable std::array<
    Eigen::Matrix<tensor4_basic<scalar_type>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_tensor4_val;
};

template<class Function>
field_expr_v2_nonlinear_terminal_function_rep<Function>::field_expr_v2_nonlinear_terminal_function_rep (const Function& f)
  : base(),
    _f(f),
    _scalar_val(),
    _vector_val(),
    _tensor_val(),
    _tensor3_val(),
    _tensor4_val()
{
}

template<class Function>
class field_expr_v2_nonlinear_terminal_function : public smart_pointer<field_expr_v2_nonlinear_terminal_function_rep<Function> >
{
public:
// typedefs:

  typedef field_expr_v2_nonlinear_terminal_function_rep<Function> rep;
  typedef smart_pointer<rep>                    base;
  typedef typename rep::size_type               size_type;
  typedef typename rep::memory_type             memory_type;
  typedef typename rep::result_type             result_type;
  typedef typename rep::argument_type           argument_type;
  typedef typename rep::value_type              value_type;
  typedef typename rep::scalar_type             scalar_type;
  typedef typename rep::float_type              float_type;
  static const space_constant::valued_type valued_hint = rep::valued_hint;

// alocators:

  explicit field_expr_v2_nonlinear_terminal_function (const Function& f) 
    : base(new_macro(rep(f))) {}

  template<class TrueFunction, 
           class Sfinae = typename std::enable_if<std::is_function<TrueFunction>::value, TrueFunction>::type>
  explicit field_expr_v2_nonlinear_terminal_function (TrueFunction f) 
    : base(new_macro(rep(std::ptr_fun(f)))) {} 

  template <class Constant, 
            class Sfinae = typename std::enable_if <is_field_expr_v2_constant<Constant>::value, Constant>::type>
  explicit field_expr_v2_nonlinear_terminal_function (const Constant& c) 
    : base(new_macro(rep(f_constant<point_basic<float_type>,result_type>(c)))) {}

// accessors:

  space_constant::valued_type valued_tag() const { return base::data().valued_tag(); }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::data().initialize (pops, iopt); }

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
	{ base::data().initialize (Xh, pops, iopt); }

  void evaluate (
    const geo_basic<float_type,memory_type>&           omega_K,
    const geo_element&                                 K,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&       value) const
	{ return base::data().evaluate (omega_K, K, value); }

  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&           omega_L,
    const geo_element&                                 L,
    const side_information_type&                       sid,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&       value) const
  	{ return base::data().evaluate_on_side (omega_L, L, sid, value); }

  template<class Value>
  bool valued_check() const {
  	return base::data().template valued_check<Value>(); }
};
// concept::
template<class F> struct is_field_expr_v2_nonlinear_arg     <field_expr_v2_nonlinear_terminal_function<F> > : std::true_type {};
//template<class F> struct has_field_lazy_interface           <field_expr_v2_nonlinear_terminal_function<F> > : std::true_type {};
template<class F> struct is_field_expr_v2_nonlinear_arg     <F,
  typename std::enable_if<
    std::conjunction<
       std::negation<has_field_rdof_interface<F>>  // filter: has op()(i_com,j_cmp) inherited from field_rdof
      ,is_field_function<F>
    >::value
  >::type
> : std::true_type {};

// wrapper:
template <class Expr>
struct field_expr_v2_nonlinear_terminal_wrapper_traits <Expr,
  typename std::enable_if<
    std::conjunction<
       std::negation<has_field_rdof_interface<Expr>>  // filter: has op()(i_com,j_cmp) inherited from field_rdof
      ,is_field_function<Expr>
    >::value
  >::type
>
{
  typedef field_expr_v2_nonlinear_terminal_function<Expr>   type;
};
// wrapper for Expr=constant (used by nary compose to wrap constant args)
template <class Expr>
struct field_expr_v2_nonlinear_terminal_wrapper_traits <Expr,
  typename std::enable_if<
    is_field_expr_v2_constant<Expr>::value
  >::type
>
{
  typedef typename promote<Expr,Float>::type                 float_type; // promote int to Float, at least
  typedef f_constant<point_basic<float_type>,Expr>           fun_t;
  typedef field_expr_v2_nonlinear_terminal_function<fun_t>   type;
};
// ---------------------------------------------------------------------------
// 1.3) normal to a surface
// ---------------------------------------------------------------------------
// the special class-function, used in nonlinear field expressions:
template<class T>
struct normal_pseudo_function : std::unary_function <point_basic<T>, point_basic<T> > {
  point_basic<T> operator() (const point_basic<T>&) const {
    fatal_macro ("special normal() class-function should not be directly evaluated");
    return point_basic<T>();
  }
};
template<class T>
class field_expr_v2_nonlinear_terminal_function_rep<normal_pseudo_function<T> >
 : field_expr_v2_nonlinear_terminal_function_base_rep<T> {
public:
// typedefs:

  typedef field_expr_v2_nonlinear_terminal_function_base_rep<T> base;
  typedef geo_element::size_type                                size_type;
  typedef rheo_default_memory_model                             memory_type; // TODO: deduce it
  typedef normal_pseudo_function<T>                             function_type;
  typedef point_basic<T>                                        result_type;
  typedef point_basic<T>                                        argument_type;
  typedef T                                                     scalar_type;
  typedef T                                                     float_type;
  typedef result_type                                           value_type;

// alocators:

  field_expr_v2_nonlinear_terminal_function_rep (const function_type&);
  field_expr_v2_nonlinear_terminal_function_rep (const field_expr_v2_nonlinear_terminal_function_rep<normal_pseudo_function<T> >&);

// accessors:

  static const space_constant::valued_type valued_hint = space_constant::vector;

  space_constant::valued_type valued_tag() const { return space_constant::vector; }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void evaluate (
    const geo_basic<float_type,memory_type>&        omega_K,
    const geo_element&                              K,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&    value) const;

  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&        omega_L,
    const geo_element&                              L,
    const side_information_type&                    sid,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&    value) const;

  void evaluate_internal(
    const geo_basic<float_type,memory_type>&        omega_K,
    const geo_element&                              K,
    const T&                                        sign,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&    value) const;

  template<class Value>
  bool valued_check() const {
    static const bool status = details::is_equal<Value,result_type>::value;
    check_macro (status, "unexpected result_type");
    return status;
  }
// data:
  mutable bool _is_on_interface, _is_inside_on_local_sides; // normal sign fix
};

} // namespace details

// the normal() pseudo-function
template<class T>
inline
details::field_expr_v2_nonlinear_terminal_function <details::normal_pseudo_function<T> >
normal_basic()
{
  return details::field_expr_v2_nonlinear_terminal_function
	   <details::normal_pseudo_function<T> >
           (details::normal_pseudo_function<T>());
}
//! @brief normal: see the @ref expression_3 page for the full documentation
inline
details::field_expr_v2_nonlinear_terminal_function <details::normal_pseudo_function<Float> >
normal()
{
  return normal_basic<Float>();
}
// ---------------------------------------------------------------------------
// 1.4) h_local
// ---------------------------------------------------------------------------
namespace details {

// the special class-function, used in nonlinear field expressions:
template<class T>
struct h_local_pseudo_function : std::unary_function <point_basic<T>, T> {
  T operator() (const point_basic<T>&) const {
    fatal_macro ("special h_local() class-function should not be directly evaluated");
    return 0;
  }
};

template<class T>
class field_expr_v2_nonlinear_terminal_function_rep<h_local_pseudo_function<T> >
 : field_expr_v2_nonlinear_terminal_function_base_rep<T> {
public:
// typedefs:

  typedef field_expr_v2_nonlinear_terminal_function_base_rep<T> base;
  typedef geo_element::size_type                                size_type;
  typedef rheo_default_memory_model                             memory_type; // TODO: deduce it
  typedef h_local_pseudo_function<T>                            function_type;
  typedef T                                                     result_type;
  typedef point_basic<T>                                        argument_type;
  typedef T                                                     scalar_type;
  typedef T                                                     float_type;
  typedef result_type                                           value_type;

// alocators:

  field_expr_v2_nonlinear_terminal_function_rep (const function_type&);
  field_expr_v2_nonlinear_terminal_function_rep (const field_expr_v2_nonlinear_terminal_function_rep<h_local_pseudo_function<T> >&);

// accessors:

  static const space_constant::valued_type valued_hint = space_constant::scalar;

  space_constant::valued_type valued_tag() const { return space_constant::scalar; }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>& value) const;

  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&     omega_L,
    const geo_element&                           L,
    const side_information_type&                 sid,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>& value) const;

  template<class Value>
  bool valued_check() const {
    static const bool status = details::is_equal<Value,result_type>::value;
    check_macro (status, "unexpected result_type");
    return status;
  }
};

} // namespace details

// the h_local() pseudo-function
template<class T>
inline
details::field_expr_v2_nonlinear_terminal_function <details::h_local_pseudo_function<T> >
h_local_basic()
{
  return details::field_expr_v2_nonlinear_terminal_function
	   <details::h_local_pseudo_function<T> >
           (details::h_local_pseudo_function<T>());
}
//! @brief h_local: see the @ref expression_3 page for the full documentation
inline
details::field_expr_v2_nonlinear_terminal_function <details::h_local_pseudo_function<Float> >
h_local()
{
  return h_local_basic<Float>();
}
// ---------------------------------------------------------------------------
// 1.5) penalty
// ---------------------------------------------------------------------------
// the special class-function, used in nonlinear field expressions:
namespace details {

template<class T>
struct penalty_pseudo_function : std::unary_function <point_basic<T>, T> {
  T operator() (const point_basic<T>&) const {
    fatal_macro ("special penalty() class-function should not be directly evaluated");
    return 0;
  }
};

template<class T>
class field_expr_v2_nonlinear_terminal_function_rep<penalty_pseudo_function<T> >
 : field_expr_v2_nonlinear_terminal_function_base_rep<T> {
public:
// typedefs:

  typedef field_expr_v2_nonlinear_terminal_function_base_rep<T> base;
  typedef geo_element::size_type                                size_type;
  typedef rheo_default_memory_model                             memory_type; // TODO: deduce it
  typedef penalty_pseudo_function<T>                            function_type;
  typedef T                                                     result_type;
  typedef point_basic<T>                                        argument_type;
  typedef T                                                     scalar_type;
  typedef T                                                     float_type;
  typedef result_type                                           value_type;

// alocators:

  field_expr_v2_nonlinear_terminal_function_rep (const function_type&);
  field_expr_v2_nonlinear_terminal_function_rep (const field_expr_v2_nonlinear_terminal_function_rep<penalty_pseudo_function<T> >&);

// accessors:

  static const space_constant::valued_type valued_hint = space_constant::scalar;

  space_constant::valued_type valued_tag() const { return space_constant::scalar; }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>& value) const;

  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&     omega_L,
    const geo_element&                           L,
    const side_information_type&                 sid,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>& value) const;

  template<class Value>
  bool valued_check() const {
    static const bool status = details::is_equal<Value,result_type>::value;
    check_macro (status, "unexpected result_type");
    return status;
  }
protected:
// internal:
  T evaluate_measure (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K) const;

  void evaluate_internal(
    const geo_basic<float_type,memory_type>&        omega_K,
    const geo_element&                              K,
    const geo_element&                              L,
    Eigen::Matrix<result_type,Eigen::Dynamic,1>&    value) const;
};

} // namespace details

// the penalty() pseudo-function
template<class T>
inline
details::field_expr_v2_nonlinear_terminal_function <details::penalty_pseudo_function<T> >
penalty_basic()
{
  return details::field_expr_v2_nonlinear_terminal_function
	   <details::penalty_pseudo_function<T> >
           (details::penalty_pseudo_function<T>());
}
//! @brief penalty(): see the @ref expression_3 page for the full documentation
inline
details::field_expr_v2_nonlinear_terminal_function <details::penalty_pseudo_function<Float> >
penalty()
{
  return penalty_basic<Float>();
}

// ---------------------------------------------------------------------------
// 2) field and such
// ---------------------------------------------------------------------------
// 2.1) field
// ---------------------------------------------------------------------------
namespace details {

template<class T, class M, details::differentiate_option::type Diff>
class field_expr_v2_nonlinear_terminal_field_rep {
public:
// typedefs:

  using size_type   = geo_element::size_type;
  using memory_type = M;
  using scalar_type = T;
  using float_type  = typename float_traits<T>::type;
  using result_type = typename std::conditional<
             Diff == details::differentiate_option::divergence
            ,T				  // TODO: support also div(tensor) -> vector
            ,undeterminated_basic<T>
           >::type;
  using value_type  = result_type;

// allocators:

  template <class Expr,
            class Sfinae = typename std::enable_if<details::has_field_rdof_interface<Expr>::value>::type>
  explicit field_expr_v2_nonlinear_terminal_field_rep (const Expr& expr, const differentiate_option& gopt);

#ifdef TO_CLEAN
// --------------------------------------------
// field_lazy interface:
// --------------------------------------------

  const geo_type&   get_geo()   const { return _uh.get_geo(); }
  const band_type&  get_band()  const { return _uh.get_band(); }
  const space_type& get_space() const { return _uh.get_space(); }
  bool is_on_band () const { return _expr.is_on_band(); }
  void initialize (const geo_type& omega_K) { _uh.initialize (omega_K); }
#endif // TO_CLEAN

// --------------------------------------------
// accessors for the affine & homogeneous case:
// --------------------------------------------

  bool have_homogeneous_space (space_basic<scalar_type,memory_type>& Vh) const {
    Vh = _uh.get_space();
    return true;
  }
  // minimal forward iterator interface: 
  using const_iterator = typename field_basic<T,M>::const_iterator;
  const_iterator begin_dof() const { return _uh.begin_dof(); }

// --------------------------------------------
// accessors for the general nonlinear case:
// --------------------------------------------

  static const space_constant::valued_type valued_hint = space_constant::last_valued;

  space_constant::valued_type valued_tag() const;

  void initialize (
    const piola_on_pointset<T>&                           pops,
    const integrate_option&                               iopt);

  void initialize (
	const space_basic<T,M>&                           Xh,
        const piola_on_pointset<float_type>&              pops,
	const integrate_option&                           iopt);

  const geo_element& get_side (const geo_element& K, const side_information_type& sid) const;

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const;

  template<class Value>
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&     omega_L,
    const geo_element&                           L, 
    const side_information_type&                 sid,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const;

  template<class Value>
  bool valued_check() const;

// data:
  differentiate_option               _gopt;
  field_basic<T,M>          _uh;
  test_basic<T,M,details::vf_tag_01> _u_test;
  mutable bool                       _use_dom2bgd;
  mutable bool                       _use_bgd2dom;
  mutable bool                       _have_dg_on_sides;
  mutable reference_element          _tilde_L;
// working data:
  mutable basis_on_pointset<T>       _piola_on_geo_basis; // for DF, for Hdiv RT ect
  mutable std::vector<size_type>     _dis_inod_geo;       // idem
  bool                               _need_vector_piola;
public:
  mutable std::array<
    Eigen::Matrix<T,Eigen::Dynamic,1>
   ,reference_element::max_variant>                           	_scalar_val;
  mutable std::array<
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_vector_val;
  mutable std::array<
    Eigen::Matrix<tensor_basic<T>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_tensor_val;
  mutable std::array<
    Eigen::Matrix<tensor3_basic<T>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_tensor3_val;
  mutable std::array<
    Eigen::Matrix<tensor4_basic<T>,Eigen::Dynamic,1>
   ,reference_element::max_variant>          			_tensor4_val;
};
// inlined:
template<class T, class M, details::differentiate_option::type Diff>
template <class Expr, class Sfinae>
inline
field_expr_v2_nonlinear_terminal_field_rep<T,M,Diff>::field_expr_v2_nonlinear_terminal_field_rep (const Expr& expr, const differentiate_option& gopt)
  : _gopt(gopt),
    _uh(expr),
    _u_test(_uh.get_space()),
    _use_dom2bgd(false),
    _use_bgd2dom(false), 
    _have_dg_on_sides(false),
    _tilde_L(),
    _piola_on_geo_basis(),
    _dis_inod_geo(),
    _need_vector_piola(false),
    _scalar_val(),
    _vector_val(),
    _tensor_val(),
    _tensor3_val(),
    _tensor4_val()
{
    // e.g. Hdiv RTk: use DF for piola vector/tensor transformation
    _need_vector_piola
      =   _uh.valued_tag() != space_constant::scalar &&
        ! _uh.get_space().get_constitution().is_hierarchical();
}
template<class T, class M, details::differentiate_option::type Diff>
template<class Value>
inline
bool
field_expr_v2_nonlinear_terminal_field_rep<T,M,Diff>::valued_check() const
{
  switch (Diff) {
    case details::differentiate_option::none: {
      space_constant::valued_type valued_tag = space_constant::valued_tag_traits<Value>::value;
      bool status = (_uh.valued_tag() == valued_tag) ||
                    (_uh.valued_tag() == space_constant::unsymmetric_tensor &&
                     valued_tag       == space_constant::tensor);
      check_macro (status,
             "unexpected "<< _uh.valued()
          << "-valued field while a " << space_constant::valued_name(valued_tag)
          << "-valued one is expected in expression");
      return status;
    }
    case details::differentiate_option::gradient: {
#ifdef TODO
      base::_fops.template grad_valued_check<Value>(); 
#endif // TODO
      return true;
    }
    case details::differentiate_option::divergence: {
#ifdef TODO
      base::_fops.template div_valued_check<Value>();
#endif // TODO
      return true;
    }
    case details::differentiate_option::curl: {
#ifdef TODO
      base::_fops.template curl_valued_check<Value>();
#endif // TODO
      return true;
    }
  }
}
template<class T, class M, details::differentiate_option::type Diff>
class field_expr_v2_nonlinear_terminal_field : public smart_pointer<field_expr_v2_nonlinear_terminal_field_rep<T,M,Diff> >
{
public:
// typedefs:

  using rep         = field_expr_v2_nonlinear_terminal_field_rep<T,M,Diff>;
  using base        = smart_pointer<rep>;
  using size_type   = typename rep::size_type;
  using memory_type = typename rep::memory_type;
  using result_type = typename rep::result_type;
  using float_type  = typename rep::float_type;
  using scalar_type = typename rep::scalar_type;
  using value_type  = typename rep::value_type;

  static const space_constant::valued_type valued_hint = rep::valued_hint;

// alocators:

  template <class Expr,
            class Sfinae = typename std::enable_if<details::has_field_rdof_interface<Expr>::value>::type>
  explicit field_expr_v2_nonlinear_terminal_field (const Expr& expr, const differentiate_option& gopt = differentiate_option()) 
    : base(new_macro(rep(expr,gopt))) {} 

#ifdef TO_CLEAN
// --------------------------------------------
// field_lazy interface:
// --------------------------------------------

  const geo_type&   get_geo()   const { return base::data().get_geo(); }
  const band_type&  get_band()  const { return base::data().get_band(); }
  const space_type& get_space() const { return base::data().get_space(); }
  bool is_on_band () const { return base::data().is_on_band(); }
  void initialize (const geo_type& omega_K) { base::data().initialize (omega_K); }
#endif // TO_CLEAN

// --------------------------------------------
// accessors for the affine & homogeneous case:
// --------------------------------------------

  bool have_homogeneous_space (space_basic<scalar_type,memory_type>& Vh) const {
    return base::data().have_homogeneous_space (Vh); }
  // minimal forward iterator interface: 
  using const_iterator = typename rep::const_iterator;
  const_iterator begin_dof() const { return base::data().begin_dof(); }

// --------------------------------------------
// accessors for the general nonlinear case:
// --------------------------------------------

  space_constant::valued_type valued_tag() const { return base::data().valued_tag(); }
  const geo_basic<T,M>& get_geo() const { return base::data().get_geo(); }
  const field_basic<T,M>& expr() const  { return base::data()._uh; }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::data().initialize (pops, iopt); }

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::data().initialize (Xh, pops, iopt); }

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const
	{ base::data().evaluate (omega_K, K, value); } 

  template<class Value>
  void evaluate_on_side (
    const geo_basic<float_type,memory_type>&     omega_L,
    const geo_element&                           L, 
    const side_information_type&                 sid,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const
	{ base::data().evaluate_on_side (omega_L, L, sid, value); }

  template<class Value>
  bool valued_check() const {
  	return base::data().template valued_check<Value>();
  }
};
// concepts:
template<class T, class M, details::differentiate_option::type Diff>
struct is_field_expr_v2_nonlinear_arg     <field_expr_v2_nonlinear_terminal_field<T,M,Diff> > : std::true_type {};
//template<class T, class M, details::differentiate_option::type Diff>
//struct has_field_lazy_interface           <field_expr_v2_nonlinear_terminal_field<T,M,Diff> > : std::true_type {};

// field terminal is affine-homogeneous whentere is no differentiation
// e.g. when diff=grad then P1 transforms to (P0)^d and then non-homogeneous space ?
// TODO: field yh_P0_d = grad(xh_P1); => space should be computed on the fly ?

template<class T, class M>
struct is_field_expr_affine_homogeneous <field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::none>>: std::true_type {};

// wrapper:
template <class Expr>
struct field_expr_v2_nonlinear_terminal_wrapper_traits <Expr,
  typename std::enable_if<
    has_field_rdof_interface<Expr>::value
  >::type
>
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  typedef field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::none>   type;
};

} // namespace details 

//! @brief grad(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::gradient
  >
>::type
grad (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::gradient> (expr);
}
//! @brief grad_s(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::gradient
  >
>::type
grad_s (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  static details::differentiate_option gopt;
  gopt.surfacic = true;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::gradient> (expr, gopt);
}
//! @brief grad_h(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::gradient
  >
>::type
grad_h (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  static details::differentiate_option gopt;
  gopt.broken = true;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::gradient> (expr, gopt);
}
//! @brief D(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::gradient
  >
>::type
D (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  details::differentiate_option gopt;
  gopt.symmetrized = true;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::gradient> (expr, gopt);
}
//! @brief Ds(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::gradient
  >
>::type
Ds (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  details::differentiate_option gopt;
  gopt.symmetrized = true;
  gopt.surfacic = true;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::gradient> (expr, gopt);
}
//! @brief Dh(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::gradient
  >
>::type
Dh (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  details::differentiate_option gopt;
  gopt.symmetrized = true;
  gopt.broken = true;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::gradient> (expr, gopt);
}
//! @brief div(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::divergence
  >
>::type
div (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::divergence> (expr);
}
//! @brief div_s(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::divergence
  >
>::type
div_s (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  static details::differentiate_option   gopt;
  gopt.surfacic = true;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::divergence> (expr, gopt);
}
//! @brief div_h(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::divergence
  >
>::type
div_h (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  static details::differentiate_option   gopt;
  gopt.broken = true;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::divergence> (expr, gopt);
}
//! @brief curl(uh): see the @ref expression_3 page for the full documentation
template<class Expr>
inline
typename
std::enable_if<
  details::has_field_rdof_interface<Expr>::value
 ,details::field_expr_v2_nonlinear_terminal_field<
    typename Expr::scalar_type
   ,typename Expr::memory_type
   ,details::differentiate_option::curl
  >
>::type
curl (const Expr& expr)
{
  typedef typename Expr::scalar_type T;
  typedef typename Expr::memory_type M;
  return details::field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::curl> (expr);
}
// TODO: bcurl(uh) = Batchelor curl ?
// TODO: curl_s(uh) curl_h(uh)...?

// ----------------------------------------------------------------------------
// 2.2) jump of a field
// ----------------------------------------------------------------------------
namespace details {

template<class T, class M>
class field_expr_v2_nonlinear_terminal_field_dg_rep {
public:
// typedefs:

  typedef geo_element::size_type          size_type;
  typedef M                               memory_type;
  typedef undeterminated_basic<T>         result_type;
  typedef result_type                     value_type;
  typedef T                               scalar_type;
  typedef typename float_traits<T>::type  float_type;

// alocators:

  template <class Expr,
            class Sfinae = typename std::enable_if<details::has_field_rdof_interface<Expr>::value>::type>
  explicit field_expr_v2_nonlinear_terminal_field_dg_rep(const Expr& expr, const float_type& c0, const float_type& c1)
   : _expr0(expr), _expr1(expr),
     _c0(c0), _c1(c1)
   {}

// accessors:

  static const space_constant::valued_type valued_hint = space_constant::last_valued;

  space_constant::valued_type valued_tag() const { return _expr0.valued_tag(); }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const;

  template<class Value>
  bool valued_check() const {
    space_constant::valued_type valued_tag = space_constant::valued_tag_traits<Value>::value;
    bool status = _expr0.valued_tag() == valued_tag;
    check_macro (status,
 	              "unexpected "<<  space_constant::valued_name(_expr0.valued_tag())
        << "-valued field while a " << space_constant::valued_name(valued_tag)
        << "-valued one is expected in expression");
    return status;
  }
// data:
protected:
  field_expr_v2_nonlinear_terminal_field<T,M,details::differentiate_option::none> _expr0, _expr1;
  float_type                     _c0,    _c1;
};

template<class T, class M>
class field_expr_v2_nonlinear_terminal_field_dg : public smart_pointer<field_expr_v2_nonlinear_terminal_field_dg_rep<T,M> >
{
public:
// typedefs:

  typedef field_expr_v2_nonlinear_terminal_field_dg_rep<T,M> rep;
  typedef smart_pointer<rep>                    base;
  typedef typename rep::size_type               size_type;
  typedef typename rep::memory_type             memory_type;
  typedef typename rep::result_type             result_type;
  typedef typename rep::float_type              float_type;
  typedef typename rep::scalar_type             scalar_type;
  typedef typename rep::value_type              value_type;
  static const space_constant::valued_type valued_hint = rep::valued_hint;

// alocators:

  template <class Expr,
            class Sfinae = typename std::enable_if<details::has_field_rdof_interface<Expr>::value>::type>
  explicit field_expr_v2_nonlinear_terminal_field_dg(const Expr& expr, const float_type& c0, const float_type& c1)
    : base(new_macro(rep(expr,c0,c1))) {} 

// accessors:

  space_constant::valued_type valued_tag() const { return base::data().valued_tag(); }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::data().initialize (pops, iopt); }

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::data().initialize (Xh, pops, iopt); }

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const
  	{ base::data().evaluate (omega_K, K, value); } 

  template<class Value>
  bool valued_check() const 
  	{ return base::data().template valued_check<Value>(); }
};
template<class T, class M> struct is_field_expr_v2_nonlinear_arg  <field_expr_v2_nonlinear_terminal_field_dg<T,M> > : std::true_type {};
//template<class T, class M> struct has_field_lazy_interface        <field_expr_v2_nonlinear_terminal_field_dg<T,M> > : std::true_type {};

} // namespace details

#define _RHEOLEF_make_field_expr_v2_nonlinear_terminal_field_dg(op,c0,c1)			\
template<class Expr>					\
inline							\
typename						\
std::enable_if<						\
  details::has_field_rdof_interface<Expr>::value			\
 ,details::field_expr_v2_nonlinear_terminal_field_dg<	\
    typename Expr::scalar_type				\
   ,typename Expr::memory_type				\
  >							\
>::type							\
op (const Expr& expr)					\
{							\
  return details::field_expr_v2_nonlinear_terminal_field_dg		\
	  <typename Expr::scalar_type ,typename Expr::memory_type>	\
	  (expr, c0, c1); 						\
}

_RHEOLEF_make_field_expr_v2_nonlinear_terminal_field_dg (jump,    1,    -1)
_RHEOLEF_make_field_expr_v2_nonlinear_terminal_field_dg (average, 0.5,  0.5)
_RHEOLEF_make_field_expr_v2_nonlinear_terminal_field_dg (inner,   1,    0)
_RHEOLEF_make_field_expr_v2_nonlinear_terminal_field_dg (outer,   0,    1)
#undef _RHEOLEF_make_field_expr_v2_nonlinear_terminal_field_dg 


// ---------------------------------------------------------------------------
// 3) convected field, as compose(uh,X) where X is a characteristic
// ---------------------------------------------------------------------------
namespace details {

template<class T, class M>
class field_expr_v2_nonlinear_terminal_field_o_characteristic_rep {
public:
// typedefs:

  typedef geo_element::size_type          size_type;
  typedef M                               memory_type;
  typedef undeterminated_basic<T>         result_type;
  typedef result_type                     value_type;
  typedef T                               scalar_type;
  typedef typename float_traits<T>::type  float_type;

// alocators:

  field_expr_v2_nonlinear_terminal_field_o_characteristic_rep (const field_o_characteristic<T,M>& uoX);
  field_expr_v2_nonlinear_terminal_field_o_characteristic_rep (const field_basic<T,M>& uh, const characteristic_basic<T,M>& X);

// accessors:

  static const space_constant::valued_type valued_hint = space_constant::last_valued;

  space_constant::valued_type valued_tag() const { return _uh.valued_tag(); }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt);

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const;

  template<class Value>
  bool valued_check() const {
    space_constant::valued_type valued_tag = space_constant::valued_tag_traits<Value>::value;
    bool status = (_uh.valued_tag() == valued_tag);
    check_macro (status, 
        "unexpected "<<_uh.valued()
        << "-valued field while a " << space_constant::valued_name(valued_tag)
        << "-valued one is expected in expression");
    return status;
  }

// internal:
  template<class Result>
  void _check () const;

// data:
  field_basic<T,M>                               _uh;
  characteristic_basic<T,M>                      _X;
  mutable fem_on_pointset<T>                     _fops;
  mutable disarray<T,M>                          _scalar_val;
  mutable disarray<point_basic<T>,M>             _vector_val;
  mutable disarray<tensor_basic<T>,M>            _tensor_val;
  mutable disarray<tensor3_basic<T>,M>           _tensor3_val;
  mutable disarray<tensor4_basic<T>,M>           _tensor4_val;
  mutable size_type                              _start_q;
};
template<class T, class M>
field_expr_v2_nonlinear_terminal_field_o_characteristic_rep<T,M>::field_expr_v2_nonlinear_terminal_field_o_characteristic_rep (
    const field_basic<T,M>&          uh,
    const characteristic_basic<T,M>& X)
  : _uh (uh), 
    _X (X),
    _fops(),
    _scalar_val(),
    _vector_val(),
    _tensor_val(),
    _tensor3_val(),
    _tensor4_val(),
    _start_q(0)
{
}
template<class T, class M>
field_expr_v2_nonlinear_terminal_field_o_characteristic_rep<T,M>::field_expr_v2_nonlinear_terminal_field_o_characteristic_rep (
    const field_o_characteristic<T,M>& uoX)
  : _uh (uoX.get_field()), 
    _X (uoX.get_characteristic()),
    _fops(),
    _scalar_val(),
    _vector_val(),
    _tensor_val(),
    _tensor3_val(),
    _tensor4_val(),
    _start_q(0)
{
}

template<class T, class M>
class field_expr_v2_nonlinear_terminal_field_o_characteristic : 
  public smart_pointer<field_expr_v2_nonlinear_terminal_field_o_characteristic_rep<T,M> >
{
public:
// typedefs:

  typedef field_expr_v2_nonlinear_terminal_field_o_characteristic_rep<T,M>    rep;
  typedef smart_pointer<rep>                    base;
  typedef typename rep::size_type               size_type;
  typedef typename rep::memory_type             memory_type;
  typedef typename rep::result_type             result_type;
  typedef typename rep::float_type              float_type;
  typedef typename rep::scalar_type             scalar_type;
  typedef typename rep::value_type              value_type;
  static const space_constant::valued_type valued_hint = rep::valued_hint;

// alocators:

  field_expr_v2_nonlinear_terminal_field_o_characteristic (const field_o_characteristic<T,M>& uoX)
    : base(new_macro(rep(uoX))) {} 

  field_expr_v2_nonlinear_terminal_field_o_characteristic (const field_basic<T,M>& uh, const characteristic_basic<T,M>& X)
    : base(new_macro(rep(uh,X))) {} 

// accessors:

  space_constant::valued_type valued_tag() const { return base::data().valued_tag(); }

  void initialize (
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::data().initialize (pops, iopt); }

  void initialize (
    const space_basic<float_type,memory_type>&        Xh,
    const piola_on_pointset<float_type>&              pops,
    const integrate_option&                           iopt)
  	{ base::data().initialize (Xh, pops, iopt); }

  template<class Value>
  void evaluate (
    const geo_basic<float_type,memory_type>&     omega_K,
    const geo_element&                           K,
    Eigen::Matrix<Value,Eigen::Dynamic,1>&       value) const
  	{ base::data().evaluate (omega_K, K, value); }

  template<class Value>
  bool valued_check() const {
  	return base::data().template valued_check<Value>();
  }
};
template<class T, class M> struct is_field_expr_v2_nonlinear_arg <field_expr_v2_nonlinear_terminal_field_o_characteristic<T,M> > : std::true_type {};
//template<class T, class M> struct has_field_lazy_interface       <field_expr_v2_nonlinear_terminal_field_o_characteristic<T,M> > : std::true_type {};

} // namespace details

// compose a field with a characteristic
template<class T, class M>
inline
details::field_expr_v2_nonlinear_terminal_field_o_characteristic<T,M>
compose (const field_basic<T,M>& uh, const characteristic_basic<T,M>& X)
{
  return details::field_expr_v2_nonlinear_terminal_field_o_characteristic<T,M> (uh,X);
}

} // namespace rheolef
#endif // _RHEOLEF_FIELD_EXPR_TERMINAL_H
