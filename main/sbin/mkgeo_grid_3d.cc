///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
# include <rheolef/compiler.h>
#include <cstring>
using namespace rheolef;
using namespace std;
// 
// generate a mesh for a cube
// ----------------------------
# define TETRA 'T'
# define PENTA 'P'
# define HEXA  'H'

Float A = 0;
Float B = 1;
Float C = 0;
Float D = 1;
Float F = 0;
Float G = 1;

// default tetra
int type = TETRA;
    
// default nb edge on x axis
size_t nx = 10;
int has_reset_nx = 0;

// default nb edge on y axis
size_t ny = 10;
int has_reset_ny = 0;

// default nb edge on z axis
size_t nz = 10;

# define no(i,j,k) ((i)+(nx+1)*((j)+(ny+1)*(k)))

void usage (const char* prog)
{
        cerr << "uniform grid nx*ny*nz for the [a,b]x[c,d]x[f,g] parallelotope" << endl
             << "usage: " << endl
             << prog << " "
             << "[-T|-P|-H]"
             << "[nx=10][ny=nx][nz=ny] "
	     << "[-[no]boundary] " 
	     << "[-[no]sides] " 
	     << "[-[no]region] " 
	     << "[-[no]corner] " 
             << "[-v4] " 
             << "[-a float="<< A <<"] "
             << "[-b float="<< B <<"] "
             << "[-c float="<< C <<"] "
             << "[-d float="<< D <<"] "
             << "[-f float="<< F <<"] "
             << "[-g float="<< G <<"] "
             << endl
             << "example: " << endl
             << prog << " 10 " << endl
             << prog << " 10 20 " << endl
             << prog << " 10 20 15" << endl
             ;
        exit (1);
}
void print_tetra (int p0, int p1, int p2, int p3)
{
      cout << "T\t"
           << p0 << " " << p1 << " " 
           << p2 << " " << p3 << endl;
}
void print_penta (int type, int p0, int p1, int p2, int p3, int p4, int p5)
{
    if (type == PENTA)
      cout << "P\t"
           << p0 << " " << p1 << " " 
           << p2 << " " << p3 << " " 
           << p4 << " " << p5 << endl;
    else {
	print_tetra (p0, p1, p2, p3);
	print_tetra (p4, p1, p3, p5);
	print_tetra (p5, p1, p3, p2);
    }
}
void print_triangle (int p0, int p1, int p2) {
      cout << "t\t" << p0 << " " << p1 << " " << p2 << endl;
}
void print_rectangle (int p0, int p1, int p2, int p3) {
      cout << "q\t" << p0 << " " << p1 << " " << p2 << " " << p3 << endl;
}
void print_hexa (int type, int p0, int p1, int p2, int p3, int p4, int p5, int p6, int p7)
{
    if (type == HEXA)
      cout << "H\t"
           << p0 << " " << p1 << " " 
           << p2 << " " << p3 << " " 
           << p4 << " " << p5 << " " 
           << p6 << " " << p7 << endl;
    else if (type == PENTA) {
	    print_penta (type, p0, p1, p2, p4, p5, p6);
	    print_penta (type, p7, p6, p4, p3, p2, p0);
    } else {
	    print_tetra (p1, p2, p0, p5);
	    print_tetra (p6, p5, p4, p2);
	    print_tetra (p4, p2, p5, p0);
	    
	    print_tetra (p7, p6, p4, p3);
	    print_tetra (p0, p4, p2, p3);
	    print_tetra (p6, p2, p4, p3);
    }
}
void print_domain (const char* name, int nface) {
    cout << "domain"        << endl
         << name            << endl
         << "1 2 " << nface << endl; 
}
void put_bottom() {
    for (size_t i = 0; i < nx; i++) {
        for (size_t j = 0; j < ny; j++) {
	    if (type != HEXA) {
		print_triangle (no(i,j,0), no(i+1,j+1,0), no(i+1,j,0));
		print_triangle (no(i,j,0), no(i,j+1,0), no(i+1,j+1,0));
	    } else {
		print_rectangle (no(i,j,0), no(i,j+1,0), no(i+1,j+1,0), no(i+1,j,0));
	    }
        }
    }
}
void put_top() {
    for (size_t i = 0; i < nx; i++) {
        for (size_t j = 0; j < ny; j++) {
	    if (type != HEXA) {
	        print_triangle(no(i,j,nz), no(i+1,j,nz), no(i+1,j+1,nz));
	        print_triangle(no(i,j,nz), no(i+1,j+1,nz), no(i,j+1,nz));
	    } else {
		print_rectangle (no(i,j,nz), no(i+1,j,nz), no(i+1,j+1,nz), no(i,j+1,nz));
	    }
	}
    }
}
void put_left() {
    for (size_t i = 0; i < nx; i++) {
        for (size_t k = 0; k < nz; k++) {
	    if (type == TETRA) {
	        print_triangle(no(i,0,k), no(i+1,0,k), no(i+1,0,k+1));
	        print_triangle(no(i,0,k), no(i+1,0,k+1), no(i,0,k+1));
	    } else {
		print_rectangle (no(i,0,k), no(i+1,0,k), no(i+1,0,k+1), no(i,0,k+1));
	    }
	}
    }
}
void put_front() {
    for (size_t j = 0; j < ny; j++) {
        for (size_t k = 0; k < nz; k++)  {
	    if (type == TETRA) {
	        print_triangle(no(nx,j,k), no(nx,j+1,k), no(nx,j,k+1)); 
	        print_triangle(no(nx,j+1,k), no(nx,j+1,k+1), no(nx,j,k+1));
	    } else {
		print_rectangle (no(nx,j,k), no(nx,j+1,k), no(nx,j+1,k+1), no(nx,j,k+1));
	    }
	}
    }
}
void put_right() {
    for (size_t i = 0; i < nx; i++) {
        for (size_t k = 0; k < nz; k++) {
	    if (type == TETRA) {
	        print_triangle(no(i,ny,k), no(i+1,ny,k+1), no(i+1,ny,k)); 
	        print_triangle(no(i,ny,k), no(i,  ny,k+1), no(i+1,ny,k+1));
	    } else {
		print_rectangle (no(i,ny,k), no(i,ny,k+1), no(i+1,ny,k+1), no(i+1,ny,k));
	    }
	}
    }
}
void put_interface() {
    for (size_t i = 0; i < nx; i++) {
        for (size_t k = 0; k < nz; k++) {
	    if (type == TETRA) {
	        print_triangle(no(i,ny/2,k), no(i+1,ny/2,k+1), no(i+1,ny/2,k)); 
	        print_triangle(no(i,ny/2,k), no(i,  ny/2,k+1), no(i+1,ny/2,k+1));
	    } else {
		print_rectangle (no(i,ny/2,k), no(i,ny/2,k+1), no(i+1,ny/2,k+1), no(i+1,ny/2,k));
	    }
	}
    }
}
void put_back() {
    for (size_t j = 0; j < ny; j++) {
        for (size_t k = 0; k < nz; k++)  {
	    if (type == TETRA) {
	        print_triangle(no(0,j,k), no(0,j,k+1), no(0,j+1,k));
	        print_triangle(no(0,j+1,k), no(0,j,k+1), no(0,j+1,k+1));
	    } else {
		print_rectangle (no(0,j,k), no(0,j,k+1), no(0,j+1,k+1), no(0,j+1,k));
            }
        }
    }
}
int
main (int argc, char**argv)
{
    char* prog = argv[0];
    bool sides    = true;
    bool boundary = true;
    bool region   = false;
    bool corner   = false;
    bool v4       = false;
    if (argc == 1) usage(prog);

    // machine-dependent precision
    int digits10 = numeric_limits<Float>::digits10;
    cout << setprecision(digits10);

    // parse command line 
    for (int i = 1; i < argc; i++) {

    	switch (argv[i][0]) {

          case '-' : {
	         if (strcmp ("-sides", argv[i]) == 0)      { sides = true; }
	    else if (strcmp ("-nosides", argv[i]) == 0)    { sides = false; }
	    else if (strcmp ("-boundary", argv[i]) == 0)   { boundary = true; }
	    else if (strcmp ("-noboundary", argv[i]) == 0) { boundary = false; }
	    else if (strcmp ("-region", argv[i]) == 0)     { region = true; }
	    else if (strcmp ("-noregion", argv[i]) == 0)   { region = false; }
	    else if (strcmp ("-corner", argv[i]) == 0)     { corner = true; }
	    else if (strcmp ("-nocorner", argv[i]) == 0)   { corner = false; }

	    else if (strcmp (argv[i], "-v4") == 0)       { v4 = true; } 
	    else switch (argv[i][1]) {
	        case TETRA : 
	        case PENTA :
	        case HEXA  : {
		    type = argv[i][1];
		    break;
		}
		default : {
		    int j = i+1;
		    if (j >= argc) usage(prog);
	            switch (argv[i][1]) {
	            case 'a'   : A = atof(argv[j]);
			     break;
	            case 'b'   : B = atof(argv[j]);
			     break;
	            case 'c'   : C = atof(argv[j]);
			     break;
	            case 'd'   : D = atof(argv[j]);
			     break;
	            case 'f'   : F = atof(argv[j]);
			     break;
	            case 'g'   : G = atof(argv[j]);
			     break;
	            default    : usage(prog);
			     break;
	    	    }
		    ++i;
		    break;
		}
	    }
	    break;
	  }
	  default : {
	    if (has_reset_ny) {
		nz = atoi(argv[i]);
	    } else if (has_reset_nx) {
		nz = ny = atoi(argv[i]);
		has_reset_ny = 1;
	    } else {
		nz = ny = nx = atoi(argv[i]);
		has_reset_nx = 1;
	    }
	    break;
	  }
	}
    }
    if (A >= B || C >= D || F >= G) {
        cerr << prog << ": [a,b]x[c,d]x[f,g] may be non-empty.\n";
        exit (1);
    }
    if (region && nx % 2 != 0) {
        cerr << prog << ": region: nx may be an even number.\n";
        exit (1);
    }
    int np = (nx+1)*(ny+1)*(nz+1);
    int ne;
    if (type == TETRA)
        ne = 6*nx*ny*nz;
    else if (type == PENTA)
        ne = 2*nx*ny*nz;
    else
        ne = nx*ny*nz;
    
    // header
    cout << "#!geo" << endl
         << "mesh" << endl;
    if (!v4) {
      cout << "1 3 " << np << " " << ne << endl;
    } else {
      cout << "4" << endl
           << "header" << endl
           << " dimension 3" << endl
           << " nodes     " << np << endl;
      if (type == TETRA) {
        cout << " tetrahedra " << ne << endl;
      } else if (type == PENTA) {
        cout << " prisms    " << ne << endl;
      } else {
        cout << " hexahedra " << ne << endl;
      }
      cout << "end header" << endl;
    }
    cout << endl;
 
    // geometry
    for (size_t k = 0; k <= nz; k++)
      for (size_t j = 0; j <= ny; j++)
        for (size_t i = 0; i <= nx; i++)
	   cout << A+(B-A)*i/Float(int(nx)) << " "
	        << C+(D-C)*j/Float(int(ny)) << " "
	        << F+(G-F)*k/Float(int(nz)) << endl;
    // connectivity
    for (size_t i = 0; i < nx; i++) 
      for (size_t j = 0; j < ny; j++) 
        for (size_t k = 0; k < nz; k++) 
	      print_hexa (type, 
		no(i,j,k),   no(i+1,j,k),   no(i+1,j+1,k),   no(i,j+1,k),
		no(i,j,k+1), no(i+1,j,k+1), no(i+1,j+1,k+1), no(i,j+1,k+1));
    cout << endl;

    // domains = 6 sides
    if (boundary) {
        if (type == HEXA)  print_domain("boundary", 2*(nx*ny + nx*nz + ny*nz));
        if (type == PENTA) print_domain("boundary", 4*nx*ny + 2*(nx*nz + ny*nz));
        if (type == TETRA) print_domain("boundary", 4*(nx*ny + nx*nz + ny*nz));
        put_bottom();
        put_top();
        put_left();
        put_front();
        put_right();
        put_back();
    }
    if (sides) {
        print_domain("bottom", (type != HEXA) ? 2*nx*ny : nx*ny);
        put_bottom();
        cout << endl;

        print_domain("top",    (type != HEXA) ? 2*nx*ny : nx*ny);
        put_top();
        cout << endl;

        print_domain("left",   (type == TETRA) ? 2*nx*nz : nx*nz);
        put_left();
        cout << endl;

        print_domain("front",  (type == TETRA) ? 2*ny*nz : ny*nz);
        put_front();
        cout << endl;

        print_domain("right",  (type == TETRA) ? 2*nx*nz : nx*nz);
        put_right();
        cout << endl;

        print_domain("back",   (type == TETRA) ? 2*ny*nz : ny*nz);
        put_back();
        cout << endl;
    }
    if (region) {
	    int nvol;
	    if (type == TETRA)
	        nvol = 6*nx*ny*nz/2;
	    else if (type == PENTA)
	        nvol = 2*nx*ny*nz/2;
	    else
	        nvol = nx*ny*nz/2;
	
	    cout << "\ndomain\nwest\n1 3 " << nvol << "\n" ;
	    for (size_t i = 0; i < nx; i++) 
	      for (size_t j = 0; j < ny/2; j++) 
	        for (size_t k = 0; k < nz; k++) 
		      print_hexa (type, 
			no(i,j,k),   no(i+1,j,k),   no(i+1,j+1,k),   no(i,j+1,k),
			no(i,j,k+1), no(i+1,j,k+1), no(i+1,j+1,k+1), no(i,j+1,k+1));
	    cout << endl;
	
	    cout << "\ndomain\neast\n1 3 " << nvol << "\n" ;
	    for (size_t i = 0; i < nx; i++) 
	      for (size_t j = ny/2; j < ny; j++) 
	        for (size_t k = 0; k < nz; k++) 
		      print_hexa (type, 
			no(i,j,k),   no(i+1,j,k),   no(i+1,j+1,k),   no(i,j+1,k),
			no(i,j,k+1), no(i+1,j,k+1), no(i+1,j+1,k+1), no(i,j+1,k+1));
	    cout << endl;
            print_domain("interface", (type == TETRA) ? 2*nx*ny : nx*ny);
	    put_interface();
    }
    // corners = 4 0d-domains
    if (corner) {
      	    cout << "\ndomain\nleft_bottom_back\n1 0 1\n"   << no( 0, 0, 0) << endl
                 << "\ndomain\nleft_bottom_front\n1 0 1\n"  << no(nx, 0, 0) << endl
                 << "\ndomain\nright_bottom_front\n1 0 1\n" << no(nx,ny, 0) << endl
                 << "\ndomain\nright_bottom_back\n1 0 1\n"  << no(0, ny, 0) << endl
      	         << "\ndomain\nleft_top_back\n1 0 1\n"  << no( 0, 0,nz) << endl
                 << "\ndomain\nleft_top_front\n1 0 1\n" << no(nx, 0,nz) << endl
                 << "\ndomain\nright_top_front\n1 0 1\n"    << no(nx,ny,nz) << endl
                 << "\ndomain\nright_top_back\n1 0 1\n"     << no(0, ny,nz) << endl
		 << endl;
    }
}
