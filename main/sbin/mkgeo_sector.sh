#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
# author: Pierre.Saramito@imag.fr
# date: 2 february 2018

##
#@commandfile mkgeo_sector mesh of a sector
#@addindex command `mkgeo_sector`
#@addindex command `geo`
#@addindex command `bamg`
#@addindex mesh
#@addindex file format `.geo` mesh
#
#Synopsis
#--------
#
#    mkgeo_sector [options] n
#
#Example
#--------
#The following command build 2d unstructured mesh
#of the triangular region delimited by the three points (0,0), (1,0), (1,1):
#
#        mkgeo_sector
#        geo sector.geo
#
#Description
#-----------
#This command is convenient for building a mesh for
#a sector of a square pipe section, as it is a very classical benchmark in complex
#fluid flow problems.
#It calls `bamg` unstructured mesh generator.
#The mesh files goes on name.geo where *name*
#is the basename for the output (see option `-name` below).
#The three auxiliary files required for automatic mesh generation
#with Rheolef combined with `bamg` are also provided:
#
#        name.bamg
#        name.bamgcad
#        name.dmn
#
#The geometry
#------------
#The geometry is the triangular sector (0,0), (a,0), (a,b).
#By default a=b=1.
#
#`-a` *float* \n
#`-b` *float*
#>	These options control the geometry parameters.
#
#The discretization
#------------------
#The optional *n* argument is an integer
#that specifies the subdivision in each direction.
#By default n=10.
#
#Boundary domains
#----------------
#The boundary sides are represented by domains: `axis`, `boundary`,
#and `bisector`.
#
#Others options
#--------------
#`-name` *string*
#>       Set the basename for the output files.
#>       By default, the basename is `sector`.
#`-[no]clean`
#>	Clear temporary files (this is the default).
#`-[no]verbose`
#>      In verbose mode, print to stderr all commands and logs.
#
#Implementation
#--------------
#@showfromfile

n=""
n_default=10
a=1
b=1
name="sector"
clean=true
verbose=false

usage="usage: mkgeo_contraction 
	[n=$n_default]
	[-a float=$a]
	[-b float=$b]
	[-name string=$name]
	[-[no]clean]
	[-[no]verbose]
"

while test $# -ne 0; do
  case $1 in
  [0-9]*) if test x$n = x""; then n=$1; else m=$1; fi;;
  -a) a="$2"; shift;;
  -b) b="$2"; shift;;
  -name)      name=$2; shift;;
  -clean)     clean=true;;
  -noclean)   clean=false;;
  -verbose)   verbose=true;;
  -noverbose) verbose=false;;
  -h) echo ${usage} >&2; exit 0;;
  *)  echo ${usage} >&2; exit 1;;
  esac
  shift
done
if test x"$n" = x""; then
  n=$n_default
fi

to_clean=""

h=`echo $n | awk '{printf("%.15g\n", 1./$1)}'`
cat > $name.bamgcad << EOF1
	MeshVersionFormatted
           0
         Dimension
           2
         Vertices
           3
           0  0     1
           $a 0     2
           $a $b    3
         Edges
           3
           1  2     101
           2  3     102
           3  1     103
         hVertices
           $h $h $h
EOF1
echo "! file $name.bamgcad created" 1>&2

cat > $name.dmn << EOF2
        EdgeDomainNames
          3
          axis
          boundary
          bisector
EOF2
echo "! file $name.dmn created" 1>&2

command="bamg -g $name.bamgcad -o $name.bamg"
if $verbose; then
  echo "! $command" 1>&2
  command="$command 1>&2"
else 
  command="($command 2>&1) > $name.bamglog"
  to_clean="$to_clean $name.bamglog"
fi
eval $command
if test $? -ne 0; then
  echo "$0: command failed"
  if $verbose; then true; else cat $name.bamglog; fi
  exit 1
fi
echo "! file $name.bamg created" 1>&2

command="bamg2geo $name.bamg $name.dmn > $name.geo"
if $verbose; then
  echo "! $command" 1>&2
else 
  command="$command 2> $name.bamglog"
  to_clean="$to_clean $name.bamglog"
fi
eval $command
if test $? -ne 0; then
  echo "$0: command failed"
  if $verbose; then true; else cat $name.bamglog; fi
  exit 1
fi
echo "! file $name.geo created" 1>&2

if $clean; then
  command="rm -f $to_clean"
  $verbose && echo "! $command" 1>&2
  eval $command
fi
