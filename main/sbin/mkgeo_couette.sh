#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# author: Pierre.Saramito@imag.fr
# date: 2 december 2019

##
#@commandfile mkgeo_couette mesh of the Couette geometry
#@addindex command `mkgeo_contraction`
#@addindex command `geo`
#@addindex command `bamg`
#@addindex mesh
#@addindex file format `.geo` mesh
#
#Synopsis
#--------
#
#    mkgeo_couette n
#
#Example
#------- 
#The following command build a triangle based 2d unstructured mesh
#of the Couette geometry
#
#        mkgeo_couette 10
#        geo couette-10.geo
#
#Description
#-----------
#This command is useful when testing fluid mechanics programs on simple geometries.
#It calls the `bamg` as unstructured mesh generator.
#
#The geometry
#------------
#The geometry is a unit disk with a circular hole with diameter 1/2.
#
#Boundary domains
#----------------
#The boundary domains are `interior` and `exterior`.
#
#Implementation
#--------------
#@showfromfile

if test $# -eq 0; then
  echo "usage: mkgeo_couette <n>" >&2
  exit 1
fi
n=$1
nn=`expr $n + $n`
radius_ratio=0.5
basename="couette"
name=${basename}-${n}
bamgcad=${name}.bamgcad
echo "! file \"$bamgcad\" created." >&2

cat > $bamgcad << EOF1
MeshVersionFormatted
0

Dimension
2

Vertices
$nn
EOF1


echo | awk -v n=$n -v radius_ratio=${radius_ratio} 'BEGIN {
  pi = 3.14159265358979323846
  r  = radius_ratio
  for (i = 0; i < n; i++)
    printf ("%25.16g %25.16g     1\n", cos(2*i*pi/n), sin(2*i*pi/n))
  for (i = 0; i < n; i++)
    printf ("%25.16g %25.16g     1\n", r*cos(2*i*pi/n), r*sin(2*i*pi/n))
}' >> $bamgcad

cat >> $bamgcad << EOF2
Edges
$nn
EOF2

echo | awk -v n=$n 'BEGIN {
  for (i = 0; i < n; i++)
    printf ("%5d %5d     1\n", i+1, ((i+1)%n) + 1)
  for (i = 0; i < n; i++)
    printf ("%5d %5d     2\n", n + ((i+1)%n) + 1, n+i+1)
}' >> $bamgcad

cat >> $bamgcad << EOF3
SubDomain 1
2 1 1 0
EOF3


bamg=$name.bamg
command="bamg -g $bamgcad -o $bamg"
echo "! $command"
eval $command

dmn=$basename.dmn
echo "! file \"$dmn\" created." >&2
cat > $dmn << EOF4
EdgeDomainNames
2
exterior
interior
EOF4

geo=$name.geo
command="bamg2geo $bamg $dmn > $geo"
echo "! $command"
eval $command

