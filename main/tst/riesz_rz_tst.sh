#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"1"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

APP="P1 P2"
status=0

run "../sbin/mkgeo_grid_2d 10 -v4 -rz 2>/dev/null | ../bin/geo -upgrade - > mesh-2d.geo 2>/dev/null"
if test $? -ne 0; then status=1; exit 1; fi

for app in $APP; do
 loop_mpirun "./riesz_rz_tst mesh-2d $app >/dev/null  2>/dev/null"
 if test $? -ne 0; then status=1; fi
done

run "/bin/rm -f mesh-2d.geo"

exit $status
