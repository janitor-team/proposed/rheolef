///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// bug rapporte par Mahamar Dicko et Julien De marchi <julien.de-marchi@inria.fr>
// 	uh[0] = vh[1]; 
// => was a simple shallow copy of proxies, without data assignement (fixed)
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float u0 (const point& x) { return 3; }
Float u1 (const point& x) { return 5; }
point u  (const point& x) { return point(3,5); }
// ---------------------------------------------------------
int check_field_wdof_sliced (const geo& omega, Float tol) {
// ---------------------------------------------------------
  space Xh (omega, "P1");
  space Vh (omega, "P1", "vector");
  space Th (omega, "P1", "tensor");
  integrate_option iopt;
  iopt.set_order(1);
  field uh = lazy_interpolate(Vh,u);
  field tau_h (Th,0);
  tau_h(0,1) = uh[0]; // old bug fix: tau_h(0,1) = field(uh[0]); or 1.0*uh[0];
  Float err = fabs(tau_h.max() - 3);
  derr << "sliced(1): err = " << err << endl;
  field vh(Vh,0);
  vh[0] = 1*uh[0];                 // convert rdof_unary -> wdof_sliced: check for rdof_unary::begin_dof
  vh[1] = lazy_interpolate(Xh,u1); // convert lazy       -> wdof_sliced: check for sliced::dis_idof_entry
  field zh = uh-vh;
  err = max(err,fabs(zh.max_abs()));
  derr << "sliced(2): err = " << err << endl;
  // do the same with a space product: herarchical space case
  field uh2(Xh*Xh,0);
  uh2[0] = 3;
  uh2[1] = 5;
  field wh(uh2.get_space(),0);
  wh[0] = lazy_interpolate(Xh,u0); // convert lazy -> wdof_sliced: check for sliced::dis_idof_entry
  wh[1] = lazy_interpolate(Xh,u1);
  err = max(err,fabs(field(uh2-wh).max_abs()));
  derr << "sliced(3): err = " << err << endl;
  return (err < tol) ? 0 : 1;
}
// -----------------------------------------------------------
auto write_access (field& uh) { return uh["left"]; }
int check_field_wdof_indirect (const geo& omega, Float tol) {
// -----------------------------------------------------------
  space Xh (omega, "P1");
  integrate_option iopt;
  iopt.set_order(1);
  field phi_h(Xh,0), psi_h(Xh,1);
  phi_h["left"] = psi_h["left"];
  Float err = 0;
  err = max (err, fabs(phi_h.max()-1));
  err = max (err, fabs(integrate(omega["left"], phi_h,iopt)-1));
  err = max (err, fabs(integrate(omega["right"],phi_h,iopt)-0));
  derr << "indirect(1): err = " << err << endl;
  // redo the same with the copy cstor instread of op=
  field zeta_h(Xh,0);
  auto gh = zeta_h["left"]; // call: field_indirect gh(zeta_h, "left"); via the move cstor ?
  gh = psi_h["left"];
  err = max (err, fabs(zeta_h.max()-1));
  err = max (err, fabs(integrate(omega["left"], zeta_h,iopt)-1));
  err = max (err, fabs(integrate(omega["right"],zeta_h,iopt)-0));
  derr << "indirect(2): err = " << err << endl;
  // redo the same with a function that returns a reference : via the move cstor ?
  field kappa_h(Xh,0);
  auto rh = write_access (kappa_h);
  rh = -(-psi_h["left"]); // convert rdof_unary -> wdof_indirect: check for rdof_unary::begin_dof
  err = max (err, fabs(kappa_h.max()-1));
  err = max (err, fabs(integrate(omega["left"], kappa_h,iopt)-1));
  err = max (err, fabs(integrate(omega["right"],kappa_h,iopt)-0));
  derr << "indirect(3): err = " << err << endl;
#ifdef DUMP
  derr << catchmark("psi") << psi_h;
  derr << catchmark("phi") << phi_h;
  derr << catchmark("zeta") << zeta_h;
#endif // DUMP
  return (err < tol) ? 0 : 1;
}
// ---------------------------------------------------------
int check_field_wdof_both (const geo& omega, Float tol) {
// ---------------------------------------------------------
  space Vh (omega, "P1", "vector");
  integrate_option iopt;
  iopt.set_order(1);
  field uh(Vh,0);
  uh[0]["left"] = 1;
  point e0 (1,0);
  Float err = 0;
  err = max (err, fabs(uh.max()-1));
  err = max (err, fabs(integrate(omega["left"], dot(uh,e0),iopt)-1));
  derr << "field_both(1): err = " << err << endl;
  uh = 0;
  uh["left"][0] = 1; // swap indexes
  err = max (err, fabs(uh.max()-1));
  err = max (err, fabs(integrate(omega["left"], dot(uh,e0),iopt)-1));
  derr << "field_both(2): err = " << err << endl;
  return (err < tol) ? 0 : 1;
}
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  Float tol = (argc > 2) ? atof(argv[2]) : 1e-10;
  int status = 0;
  status |= check_field_wdof_sliced   (omega, tol);
  status |= check_field_wdof_indirect (omega, tol);
  status |= check_field_wdof_both     (omega, tol);
  return status;
}
