///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// draft-1 version of the geo_element class with arbitrarily order
// solution with std::vector inside each geo_element
//
// avantage : 
//  - souplesse : gere la memoire en tas dans geo (efficace)
//    mais aussi peut creer des elements temporaires, si besoin
//  - inconvenient : tableau de geo_element, qui contiennent des tableaux
//    alloues dynamiquement => la memoire n'est pas contigue globalement
//
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI

#include "rheolef/hack_array.h"
#include "rheolef/geo_element.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;

int main(int argc, char**argv)
{
  environment distributed (argc, argv);
  check_macro (communicator().size() == 2, "expect nproc = 2");
  size_t iproc = (argc > 1) ? atoi(argv[1]) : 0;
  size_t n, order;
  din >> n >> order;
  distributor ownership (n, communicator(), distributor::decide);
  geo_element::parameter_type param (reference_element::e, order); // edge, order=read
  hack_array<geo_element_hack> ge_e (ownership, param);
  din >> ge_e;
  set<size_t> set_ext_idx;
  size_t jproc = (communicator().rank() == 0) ? 1 : 0;
  for (size_t dis_i = ownership.first_index(jproc), dis_n = ownership.last_index(jproc); dis_i < dis_n; dis_i++) {
    set_ext_idx.insert (dis_i);
  }
  ge_e.set_dis_indexes (set_ext_idx);
  if (size_t(communicator().rank()) == iproc) {
    cout << ge_e.dis_size() << " " << order << endl;
    for (size_t dis_i = 0, dis_n = ge_e.dis_size(); dis_i < dis_n; dis_i++) {
      cout << ge_e.dis_at (dis_i) << endl;
    }
  }
}
#endif // _RHEOLEF_HAVE_MPI
