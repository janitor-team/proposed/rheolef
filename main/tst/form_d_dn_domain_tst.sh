#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

echo "      not yet (skipped)"
exit 0

status=0

# ------------------------------------
# test 1: Omega=]0,1[^d
# ------------------------------------
# mesh				domain where n={1,0,0}
L="
line-dom-v2 			right
carre-bamg-splitbedge-v2.geo 	right
cube-gmsh-v2.geo	        front
"
tol="1e-10"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  dom=`echo $L | gawk '{print $2}'`
  L=`echo $L | gawk '{for (i=3; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3; do
    loop_mpirun "./form_d_dn_tst $SRCDIR/$geo $Pk $dom $tol >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# ------------------------------------
# test 2: Omega=ball(0,1)
# ------------------------------------
# TODO: Pk isoparametric
# mesh				err_P1
L="
circle-10-fix-P1.geo 		0.05
sphere_p1-v2.geo 		0.3
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  tol=`echo $L | gawk '{print $2}'`
  L=`echo $L | gawk '{for (i=3; i <= NF; i++) print $i}'`
  for Pk in P1; do
    loop_mpirun "./form_d_dn_tst $SRCDIR/$geo $Pk boundary $tol -ball >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

exit $status
