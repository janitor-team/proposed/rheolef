///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// integrate order was not always well computed automatically
// with specific order : computation is good
// without: automatic default order computation was wrong
// because expr.n_derivative() was buggy 
//
// when expr=expr1+expr2 : compute 
// n_derivative(expr)= n_derivative(expr1)+ n_derivative(expr2)
// while the good computation is
// n_derivative(expr)=min(n_derivative(expr1),n_derivative(expr2))
// => now test it carrefully
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P2";
  Float tol = 1e-12;
  space Xh (omega, approx, "vector");
  integrate_option iopt;
  iopt.set_order (2*Xh.degree()-2);
  size_t dim = Xh.get_geo().dimension();
  tensor I = tensor::eye(dim);
  trial v(Xh); test w(Xh);
  form a1 = integrate (2*ddot(D(v)-1./dim*div(v)*I, D(w)-1./dim*div(w)*I));
  form a2 = integrate (2*ddot(D(v)-1./dim*div(v)*I, D(w)-1./dim*div(w)*I), iopt);
  Float err = form(a1-a2).uu().max_abs();
  derr << "err=" << err << endl;
  return (err < tol) ? 0 : 1;
}
