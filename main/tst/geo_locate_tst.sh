#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
SRCDIR=${SRCDIR-"."}
GEODIR=$SRCDIR
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

run "../sbin/mkgeo_grid_1d -e 10 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-1d.geo 2>/dev/null"
run "../sbin/mkgeo_grid_2d -t 10 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-2d.geo 2>/dev/null"
run "../sbin/mkgeo_grid_2d -q 10 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-2d-q.geo 2>/dev/null"
run "../sbin/mkgeo_grid_3d -T  6 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-3d.geo 2>/dev/null"
run "../sbin/mkgeo_grid_3d -H  6 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-3d-H.geo 2>/dev/null"
run "../sbin/mkgeo_grid_3d -P  6 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-3d-P.geo 2>/dev/null"

loop_mpirun "./geo_locate_tst 0.51 < mesh-1d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.5  < mesh-1d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0    < mesh-1d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 1    < mesh-1d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi

# outsider test:
# loop_mpirun "./geo_locate_tst -1   < mesh-1d.geo 2>/dev/null >/dev/null"
# if test $? -eq 0; then status=1; fi


loop_mpirun "./geo_locate_tst 0.51 0.52 < mesh-2d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.5  0.5  < mesh-2d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0    0    < mesh-2d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 1    1    < mesh-2d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0    1    < mesh-2d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 1    0    < mesh-2d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.51 0.52 0.53 < mesh-3d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.5  0.5  0.5  < mesh-3d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0    0    0    < mesh-3d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 1    1    1    < mesh-3d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 1    0    0    < mesh-3d.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi


loop_mpirun "./geo_locate_tst 0.51 0.52 < mesh-2d-q.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.5  0.5  < mesh-2d-q.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.51 0.52 0.53 < mesh-3d-H.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.5  0.5  0.5  < mesh-3d-H.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.51 0.52 0.53 < mesh-3d-P.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 0.5  0.5  0.5  < mesh-3d-P.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi
loop_mpirun "./geo_locate_tst 1    1    1    < mesh-3d-P.geo 2>/dev/null >/dev/null"
if test $? -ne 0; then status=1; fi

run "rm -f mesh-1d.geo mesh-2d.geo mesh-2d-q.geo mesh-3d.geo mesh-3d-H.geo mesh-3d-P.geo"

exit $status
