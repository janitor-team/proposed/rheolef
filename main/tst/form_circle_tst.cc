///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute:
//  \int_\Omega weight(x) dx = mes(\Omega) with weight
//
// check value on the unit ball C(0,1) in IR^d
//
#include "rheolef/rheolef.h"
using namespace rheolef;
using namespace std;

/*
  I_{m,n}
  = int_Omega x^m y^n dx
  = (int_{r=0}^1 r^{m+n+1} dr)*(int_0^{2*pi} cos(theta)^m sin(theta)^n d theta)
  et :
   int_{r=0}^1 r^{m+n+1} dr = 1/(m+n+2)
  => I_{m,n}   = J_{m,n}/(m+n+2)
  J_{m,n} = int_0^{2*pi} cos(theta)^m sin(theta)^n d theta
	cf Bro-1985, p. 557 & 561 : recurrence
  J_{m,n} = 0 si m ou n impair
  J_{2p,2q} = (2q-1)/(2p+2q)*J_{2p,2q-2}
  J_{2p,0} = (2p-1)/(2p)*J_{2p-2,0}
  J_{0,0} = 2*pi
                  (2p)!
  => J_{2p,0} = ------------ * (2*pi)        pour q = 0
	        (2^p * p!)^2
                 (2p)! (2q-1)! pi
  => J_{2p,2q} = --------------------------  pour q >=1
                 4^(p+q-1) p! (q-1)! (p+q)!
*/
Float factorial (size_t n)
{
  Float res = 1;
  for (size_t i = 1; i <= n; i++) res *= i;
  return res;
}
Float
integrate_2d_mass_xy (size_t nx, size_t ny, bool is_boundary)
{
  static Float pi = acos(-1.0);
  if ((nx % 2 == 1) || (ny % 2 == 1)) return 0;
  size_t p = nx/2;
  size_t q = ny/2;
  Float value;
  if (q == 0) {
    value = 2*pi*factorial(2*p)/sqr(pow(2.0,p)*factorial(p));
  } else {
    value = pi*factorial(2*p)*factorial(2*q-1)/(pow(4.0,p+q-1)*factorial(p)*factorial(q-1)*factorial(p+q));
  }
  if (!is_boundary) value = value/(nx+ny+2); // integrate also in r : interior of the circle
  return value;
}
// handle also grad_grad form:
Float
integrate_xy (size_t d, string name, size_t nx, size_t ny, size_t mx, size_t my, bool is_boundary)
{
  if (d == 2) { // circle
    if (name == "mass") return integrate_2d_mass_xy (nx+mx, ny+my, is_boundary);
    check_macro (name == "grad_grad", "unexpected form `"<<name<<"'");
    if (is_boundary) return 0;
    // here: form="grad_grad" on a filled circle
    Float value = 0;
    if (nx+mx >= 2) value += nx*mx*integrate_2d_mass_xy (nx+mx-2, ny+my,   is_boundary);
    if (ny+my >= 2) value += ny*my*integrate_2d_mass_xy (nx+mx,   ny+my-2, is_boundary);
    return value;
  } else { // sphere: only mass and nx=ny=0 yet
    check_macro (name == "mass", "form `"<<name<<"' not yet");
    check_macro (nx + ny + mx + my == 0, "non null nx etc: not yet");
    static Float pi = acos(-1.0);
    Float value = 4*pi;
    return value;
  }
}
void usage()
{
      derr << "usage: form_mass_circle_tst"
	   << " -|mesh[.geo]"
	   << " {-Igeodir}*"
 	   << " [-form name=mass]"
 	   << " -app approx"
	   << " [-nx int]"
	   << " [-ny int]"
	   << " [-nz int]"
	   << " [-mx int]"
	   << " [-my int]"
	   << " [-mz int]"
 	   << " [-tol float]"
	   << endl
           << "example:" << endl
           << "  mkgeo_ball -order 3 -t 10 > ball.geo" << endl
           << "  form_mass_circle_tst ball -app P3 -nx 3" << endl;
      exit (1);
}
struct weight {
  Float operator() (const point& x) const {
    if (nx == 0 && ny == 0) return 1;
    Float factx, facty;
    if (nx % 2 == 0) factx = pow(fabs(x[0]),nx);
    else             factx = x[0]*pow(fabs(x[0]),nx-1);
    if (ny % 2 == 0) facty = pow(fabs(x[1]),ny);
    else             facty = x[1]*pow(fabs(x[1]),ny-1);
    return factx*facty;
  }
  weight(size_t nx1=0, size_t ny1=0) : nx(nx1), ny(ny1) {}
  size_t nx, ny;
};
int main(int argc, char**argv)
{
    //
    // load geometry and options
    //
    environment rheolef (argc,argv);
    geo omega;  
    string form_name = "mass";
    string approx1 = "P1";
    string approx2 = "";
    size_t nx = 0;
    size_t ny = 0;
    size_t nz = 0;
    size_t mx = 0;
    size_t my = 0;
    size_t mz = 0;
    bool mesh_done = false;
    Float tol = 1e-10;

    if (argc <= 1) usage() ;

    for (int i = 1; i < argc; i++ ) {

      if (argv [i][0] == '-' && argv [i][1] == 'I') { append_dir_to_rheo_path (argv[i]+2) ; }
      else if (strcmp(argv[i], "-form") == 0) { form_name = argv[++i]; }
      else if (strcmp(argv[i], "-app") == 0) { approx1 = argv[++i]; }
      else if (strcmp(argv[i], "-tol") == 0) { tol = atof(argv[++i]); }
      else if (strcmp(argv[i], "-nx") == 0) { nx = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-ny") == 0) { ny = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-nz") == 0) { nz = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-mx") == 0) { mx = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-my") == 0) { my = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-mz") == 0) { mz = atoi(argv[++i]); }
      else if (strcmp(argv[i], "-") == 0) {
	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << "! load geo on stdin" << endl ;
	  din >> omega ;
	  mesh_done = true ;
      } else {
	  // input geo on file
	  if (mesh_done) usage() ;
	  //derr << "! load " << argv[i] << endl ;
	  omega = geo(argv[i]);
	  mesh_done = true ;
      }
    }
    warning_macro ("form = " << form_name);
    warning_macro ("nx = " << nx);
    warning_macro ("ny = " << ny);
    warning_macro ("mx = " << mx);
    warning_macro ("my = " << my);
    if (!mesh_done) usage() ;
    if (approx2 == "") {
	approx2 = approx1;
    }
    space V1(omega, approx1);
    space V2(omega, approx2);
    field w1h = lazy_interpolate (V1, weight(nx,ny));
    field w2h = lazy_interpolate (V2, weight(mx,my));
    form  a(V1,V2,form_name);
#ifdef TO_CLEAN
    odiststream out; out.open("a","mm"); out << a.uu;
    odiststream zzz; zzz.open("w1h","field"); zzz << w1h;
    form  mass(V1,V2,"mass");
    solver sm (mass.uu);
    field aw1h (V2);
    aw1h.u = sm.solve ((a*w1h).u);
    aw1h["boundary"] = -2;
    odiststream yyy; yyy.open("aw1h","field"); yyy << aw1h;
#endif // TO_CLEAN
    Float mes_omega = a(w1h, w2h);
    dout << setprecision(numeric_limits<Float>::digits10)
         << "mes(omega," << approx1 << "," << approx2 << ") = " << mes_omega << endl;
    int status = 0;
    // TODO: ny+my
    bool is_boundary = (omega.dimension() > omega.map_dimension());
    Float expect = integrate_xy (omega.dimension(),form_name,nx, ny, mx, my, is_boundary);
    dout << "exact  = " << expect << endl
	 << "error  = " << fabs(mes_omega - expect) << endl;
    return 0;
}
