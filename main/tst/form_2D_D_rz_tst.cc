///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute:
//  \int_\Omega 2 D(u):D(v) r dr dz
//
// check value on the unit square [0,1]^2
//
// component 0 : we check the u_r component : u_z = 0
//               and u_r takes monomial values : 1, r, ...
// component 1 : u_r = 0 and u_z is monomial
//
#include "rheolef/rheolef.h"
using namespace rheolef;
using namespace std;

typedef Float (*function_type)(const point&);

struct list_type {
	string        name;
	function_type function;
};
Float monom_1  (const point& x) { return 1; }
Float monom_r  (const point& x) { return x[0]; }
Float monom_z  (const point& x) { return x[1]; }
Float monom_r2 (const point& x) { return x[0]*x[0]; }
Float monom_rz (const point& x) { return x[0]*x[1]; }
Float monom_z2 (const point& x) { return x[1]*x[1]; }

// axisymmetric rz

list_type rz_monom_list [] = {
	//   monom
	// id   fct    
	{"1",	monom_1},
	{"r",	monom_r},
	{"z",	monom_z},
	{"r2",	monom_r2},
	{"rz",	monom_rz},
	{"z2",	monom_z2}
};
const unsigned int rz_monom_max = sizeof(rz_monom_list)/sizeof(list_type);

typedef Float T;

const Float infty = -1;

Float  expect_table [2][2] [rz_monom_max][rz_monom_max] = {
  {
    // a([ur,0],[vr,0])
    {//         1       r       z      r2      rz       z2

	{   infty, 	2,  infty,	1,	1,  infty},
	{	2, 	2,	1,	2,	1, T(2)/3},
	{   infty, 	1,  infty,    0.5,	1,  infty},

	{	1, 	2,    0.5,    2.5,	1,  T(1)/3},
	{	1, 	1,	1,	1, T(11)/12, T(5)/6},
	{   infty, T(2)/3,  infty, T(1)/3, T(5)/6,  infty}
    },
    // a([ur,0],[0,vz])
    {//vz=      1       r       z      r2      rz       z2
	{	0, 	0,	0,	0,	0,	0},
	{	0, 	0,	0,	0,	0,	0},
	{	0, 	0.5,	0,	T(2)/3,	0.25,	0},
	{	0, 	0,	0,	0,	0,	0},
	{	0, 	T(1)/3,	0,	0.5,	T(1)/6,	0},
	{	0, 	0.5,	0,	T(2)/3,	T(1)/3,	0}
    }
  },
  {
    // a([0,uz],[vr,0]) : not yet
    {
	{-1, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0}
    },
    // a([0,uz],[0,vz]) : not yet
    {
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0},
	{0, 	0,	0,	0,	0,	0}
    }
  }
};
void usage()
{
      derr << "form_2D_D_tst: usage: form_2D_D_tst"
	   << " {-Igeodir}*"
	   << " -|mesh[.geo]"
	   << " [-approx string]"
	   << " [-u-component int]"
	   << " [-v-component int]"
	   << " [-u-monom string]"
	   << " [-v-monom string]"
	   << endl
           << "example:\n"
           << "  form_2D_D_tst square -u-monom rz -v-monom r -u-component 0 -v-component 0\n";
      exit (1);
}
int main(int argc, char**argv)
{
    environment rheolef (argc, argv);
    //
    // load geometry and options
    //
    geo omega;  
    string sys_coord_name = "rz";
    string approx = "P2";
    bool mesh_done = false;
    size_t i_comp_u = 0;
    size_t i_comp_v = 0;
    string        u_monom_id = "";
    function_type u_monom_function = 0;
    unsigned int  u_monom_idx = 0;
    string        v_monom_id = "";
    function_type v_monom_function = 0;
    unsigned int  v_monom_idx = 0;

    if (argc <= 1) usage() ;

    for (int i = 1; i < argc; i++ ) {

      if (argv [i][0] == '-' && argv [i][1] == 'I')
	  append_dir_to_rheo_path (argv[i]+2) ;
      else if (strcmp(argv[i], "-approx") == 0)
	  approx = argv[++i];
      else if (strcmp(argv[i], "-u-component") == 0)
	  i_comp_u = atoi(argv[++i]);
      else if (strcmp(argv[i], "-v-component") == 0)
	  i_comp_v = atoi(argv[++i]);
      else if (strcmp(argv[i], "-u-monom") == 0) {
	  u_monom_id = argv[++i];
          for (unsigned int i = 0; i < rz_monom_max; i++) {
            if (u_monom_id == rz_monom_list [i].name) {
	      u_monom_function = rz_monom_list [i].function;
	      u_monom_idx = i;
            }
          }
      } else if (strcmp(argv[i], "-v-monom") == 0) {
	  v_monom_id = argv[++i];
          for (unsigned int i = 0; i < rz_monom_max; i++) {
            if (v_monom_id == rz_monom_list [i].name) {
	      v_monom_function = rz_monom_list [i].function;
	      v_monom_idx = i;
            }
          }
      } else if (strcmp(argv[i], "-") == 0) {
	  // input geo on standard input
	  if (mesh_done) usage() ;
	  derr << " ! load geo on stdin" << endl ;
	  din >> omega ;
	  mesh_done = true ;
      } else {
	  // input geo on file
	  if (mesh_done) usage() ;
	  derr << " ! load " << argv[i] << endl ;
	  omega = geo(argv[i]);
	  mesh_done = true ;
      }
    }
    if (!mesh_done) {
	derr << "form_2D_D_tst: no mesh specified" << endl;
	usage() ;
    }
    omega.set_coordinate_system (sys_coord_name);
    if (!u_monom_function) {
	error_macro("invalid u-monom identifier: " << u_monom_id);
    }
    if (!v_monom_function) {
	error_macro("invalid v-monom identifier: " << v_monom_id);
    }
    derr << "syscoord = " << omega.coordinate_system() << endl
         << "u_monom = " << u_monom_id << endl
         << "v_monom = " << v_monom_id << endl;
    space V0h(omega, approx);
    space Vh (omega, approx, "vector");
    field u_monom (Vh, Float(0));
    u_monom[i_comp_u] = lazy_interpolate (V0h, u_monom_function);
    field v_monom (Vh, Float(0));
    v_monom[i_comp_v] = lazy_interpolate (V0h, v_monom_function);
    form  a (Vh,Vh,"2D_D");
    Float result = a(u_monom, v_monom);
    string u_vec_monom_id;
    string v_vec_monom_id;
    if (i_comp_u == 0) u_vec_monom_id = u_monom_id + ",0";
    else               u_vec_monom_id = "0," + u_monom_id;
    if (i_comp_v == 0) v_vec_monom_id = v_monom_id + ",0";
    else               v_vec_monom_id = "0," + v_monom_id;
    derr << setprecision(numeric_limits<Float>::digits10)
         << "a(omega," << approx << "[" << u_vec_monom_id << "]," << approx << "[" << v_vec_monom_id << "]) = " << result << endl;
    Float expect = expect_table [i_comp_u][i_comp_v][u_monom_idx][v_monom_idx];
    Float tol = 1e-10;

    if (expect == infty) {
        derr << "ok (expect infty)" << endl;
        return 0;
    } else if (fabs(result - expect) <= tol) {
        derr << "ok" << endl;
        return 0;
    } else {
        derr << "but it was expected " << expect << endl
	     << "and error = " << fabs(result - expect) << endl
	     << "and tol = " << tol << endl;
        return 1;
    }
}
