///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// mkgeo_grid -e 100 -a -2 -b 2 > line2.geo
// quadrature_equispaced_tst line2 10 equispaced 
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float heaviside (const Float& x) { return (x <= 0) ? 0 : 1; }
struct phi {
  Float operator() (const point& x) const { return norm(x-x0)-r0; }
  phi() : r0(1), x0(0) {}
  Float r0; point x0;
};
struct id_ball {
  Float operator() (const point& x) const { return heaviside(-_phi(x)); }
  id_ball() : _phi() {}
  phi _phi;
};
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  const Float pi = acos(Float(-1));
  geo omega (argv[1]);
  size_t order  = (argc > 2) ? atoi(argv[2]) : 1;
  string family = (argc > 3) ?      argv[3] : "equispaced";
  Float tol     = (argc > 4) ? atof(argv[4]) : 1e100;
  size_t d = omega.dimension();
  quadrature_option qopt;
  qopt.set_family(family);
  qopt.set_order (order);
  Float mh = integrate (omega, id_ball(), qopt);
  Float m  = (d == 1) ? 2 : (d == 2) ? pi : 4*pi/3;
  Float err = fabs(mh-m)/m;
  dout << "# family = " << family << endl
       << "# m  = " << m << endl
       << "# mh = " << mh << endl
       << "# order |mh-m|/m " << endl
       << order << " " << err << endl;
  return (err < tol) ? 0 : 1;
}
