#!geo

mesh
2 1 11 10

0
0.1
0.2
0.3
0.4
0.5
0.6
0.7
0.8
0.9
1

e 0 1
e 1 2
e 2 3
e 3 4
e 4 5
e 5 6
e 6 7
e 7 8
e 8 9
e 9 10

domain
left
1 0 1
0

domain
right
1 0 1
10

domain
boundary
1 0 2
0
10

