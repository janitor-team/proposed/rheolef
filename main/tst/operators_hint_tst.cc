///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// type deduction for : a1*a2 -> r
// used in expresion, to deduce types at compile time 
// => avoid run-time switches that slow down codes

#include "rheolef.h"

// --------------------------------------------------
// pretty printer, for debug
// --------------------------------------------------

using namespace rheolef;
using namespace std;

template<class T> struct type_name { static const char* value; };

template<class T>
const char* 
type_name<T>::value = "!";

template<class Op, class T1, class T2, class R>
struct type_name<details::binop_error<Op,T1,T2,R> > { static const char* value; };

template<class Op, class T1, class T2, class R>
const char* 
type_name<details::binop_error<Op,T1,T2,R> >::value = "E";

template<class T1, class T2>
struct type_name<promote_not_specialized_for_this_case<T1,T2> > { static const char* value; };

template<class T1, class T2>
const char* 
type_name<promote_not_specialized_for_this_case<T1,T2> >::value = "?";

#define typ_val(typ,str) \
template<>        struct type_name<typ>      { static const char* value; }; \
const char* type_name<typ>::value = str;

typ_val(Float,"s")
typ_val(point,"v")
typ_val(tensor,"t")
typ_val(tensor3,"t3")
typ_val(tensor4,"t4")
typ_val(undeterminated_basic<Float>,"U")
typ_val(details::plus,"+")
typ_val(details::minus,"-")
typ_val(details::multiplies,"*")
typ_val(details::divides,"/")
typ_val(details::dot_,".")
typ_val(details::ddot_,":")
typ_val(details::pow_,"^")
#undef typ_val

template <class Op, class A1, class A2>
inline
void show_result () {
  typedef typename details::generic_binary_traits<Op>::template hint<A1,A2,undeterminated_basic<Float> >::result_type R;
  dout << type_name<A1>::value
       << type_name<Op>::value
       << type_name<A2>::value
       << " -> " << type_name<R>::value
       << endl;
}
template <class Op, class A2, class R>
inline
void show_arg1 () {
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,A2,R>::first_argument_type  a1;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,A2,R>::second_argument_type a2;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,A2,R>::result_type          r;
  dout
       << "a"
       << type_name<Op>::value
       << type_name<a2>::value
       << "=" << type_name<r>::value
       << " => a=" << type_name<a1>::value
       << endl;
}
template <class Op, class A1, class R>
inline
void show_arg2 () {
  typedef typename details::generic_binary_traits<Op>::template hint<A1,undeterminated_basic<Float>,R>::first_argument_type  a1;
  typedef typename details::generic_binary_traits<Op>::template hint<A1,undeterminated_basic<Float>,R>::second_argument_type a2;
  typedef typename details::generic_binary_traits<Op>::template hint<A1,undeterminated_basic<Float>,R>::result_type          r;
  dout << type_name<a1>::value 
       << type_name<Op>::value 
       << "b="
       << type_name<r>::value
       << " => b=" << type_name<a2>::value
       << endl;
}
template <class Op, class R>
inline
void show_from_result () {
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,undeterminated_basic<Float>,R>::first_argument_type  a1;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,undeterminated_basic<Float>,R>::second_argument_type a2;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,undeterminated_basic<Float>,R>::result_type r;
  dout << "a" 
       << type_name<Op>::value 
       << "b="   << type_name<r>::value
       << " => a=" << type_name<a1>::value
       << ", b="   << type_name<a2>::value
       << endl;
}
template <class Op, class A1>
inline
void show_from_arg1 () {
  typedef typename details::generic_binary_traits<Op>::template hint<A1,undeterminated_basic<Float>,undeterminated_basic<Float> >::first_argument_type  a1;
  typedef typename details::generic_binary_traits<Op>::template hint<A1,undeterminated_basic<Float>,undeterminated_basic<Float> >::second_argument_type a2;
  typedef typename details::generic_binary_traits<Op>::template hint<A1,undeterminated_basic<Float>,undeterminated_basic<Float> >::result_type          r;
  dout << type_name<a1>::value
       << type_name<Op>::value 
       << "b=r"
       << " => b="   << type_name<a2>::value
       << ", r="   << type_name<r>::value
       << endl;
}
template <class Op, class A2>
inline
void show_from_arg2 () {
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,A2,undeterminated_basic<Float> >::first_argument_type  a1;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,A2,undeterminated_basic<Float> >::second_argument_type a2;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,A2,undeterminated_basic<Float> >::result_type          r;
  dout << "a"
       << type_name<Op>::value 
       << type_name<a2>::value
       << "=r => a="   << type_name<a1>::value
       << ", r="   << type_name<r>::value
       << endl;
}
template <class Op>
inline
void show_from_none () {
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,undeterminated_basic<Float>,undeterminated_basic<Float> >::first_argument_type  a1;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,undeterminated_basic<Float>,undeterminated_basic<Float> >::second_argument_type a2;
  typedef typename details::generic_binary_traits<Op>::template hint<undeterminated_basic<Float>,undeterminated_basic<Float>,undeterminated_basic<Float> >::result_type          r;
  dout << "a"
       << type_name<Op>::value 
       << "b=r => a="   << type_name<a1>::value
       << ", b="   << type_name<a2>::value
       << ", r="   << type_name<r>::value
       << endl;
}
template <class Op>
inline
void show_deduce() {
  // 1) deduction du resultat quand on connait les deux args
  show_result<Op,Float,Float>(); 
  show_result<Op,Float,point>(); 
  show_result<Op,Float,tensor>(); 
  show_result<Op,Float,tensor3>(); 
  show_result<Op,Float,tensor4>(); 
  show_result<Op,point,Float>(); 
  show_result<Op,point,point>(); 
  show_result<Op,point,tensor>(); 
  show_result<Op,point,tensor3>(); 
  show_result<Op,point,tensor4>(); 
  show_result<Op,tensor,Float>(); 
  show_result<Op,tensor,point>(); 
  show_result<Op,tensor,tensor>(); 
  show_result<Op,tensor,tensor3>(); 
  show_result<Op,tensor,tensor4>(); 
  show_result<Op,tensor3,Float>(); 
  show_result<Op,tensor3,point>(); 
  show_result<Op,tensor3,tensor>(); 
  show_result<Op,tensor3,tensor3>(); 
  show_result<Op,tensor3,tensor4>(); 
  show_result<Op,tensor4,Float>(); 
  show_result<Op,tensor4,point>(); 
  show_result<Op,tensor4,tensor>(); 
  show_result<Op,tensor4,tensor3>(); 
  show_result<Op,tensor4,tensor4>(); 
  dout << endl;

  // 2) deduction du 1er arg quand on connait le 2nd et le resultat
  show_arg1<Op,Float,Float>(); 
  show_arg1<Op,Float,point>(); 
  show_arg1<Op,Float,tensor>(); 
  show_arg1<Op,Float,tensor3>(); 
  show_arg1<Op,Float,tensor4>(); 
  show_arg1<Op,point,Float>(); 
  show_arg1<Op,point,point>(); 
  show_arg1<Op,point,tensor>(); 
  show_arg1<Op,point,tensor3>(); 
  show_arg1<Op,point,tensor4>(); 
  show_arg1<Op,tensor,Float>(); 
  show_arg1<Op,tensor,point>(); 
  show_arg1<Op,tensor,tensor>(); 
  show_arg1<Op,tensor,tensor3>(); 
  show_arg1<Op,tensor,tensor4>(); 
  show_arg1<Op,tensor3,Float>(); 
  show_arg1<Op,tensor3,point>(); 
  show_arg1<Op,tensor3,tensor>(); 
  show_arg1<Op,tensor3,tensor3>(); 
  show_arg1<Op,tensor3,tensor4>(); 
  show_arg1<Op,tensor4,Float>(); 
  show_arg1<Op,tensor4,point>(); 
  show_arg1<Op,tensor4,tensor>(); 
  show_arg1<Op,tensor4,tensor3>(); 
  show_arg1<Op,tensor4,tensor4>(); 
  dout << endl;

  // 3) deduction du 2nd arg quand on connait le 1er et le resultat
  show_arg2<Op,Float,Float>(); 
  show_arg2<Op,Float,point>(); 
  show_arg2<Op,Float,tensor>(); 
  show_arg2<Op,Float,tensor3>(); 
  show_arg2<Op,Float,tensor4>(); 
  show_arg2<Op,point,Float>(); 
  show_arg2<Op,point,point>(); 
  show_arg2<Op,point,tensor>(); 
  show_arg2<Op,point,tensor3>(); 
  show_arg2<Op,point,tensor4>(); 
  show_arg2<Op,tensor,Float>(); 
  show_arg2<Op,tensor,point>(); 
  show_arg2<Op,tensor,tensor>();
  show_arg2<Op,tensor,tensor3>();
  show_arg2<Op,tensor,tensor4>();
  show_arg2<Op,tensor3,Float>(); 
  show_arg2<Op,tensor3,point>(); 
  show_arg2<Op,tensor3,tensor>();
  show_arg2<Op,tensor3,tensor3>();
  show_arg2<Op,tensor3,tensor4>();
  show_arg2<Op,tensor4,Float>(); 
  show_arg2<Op,tensor4,point>(); 
  show_arg2<Op,tensor4,tensor>();
  show_arg2<Op,tensor4,tensor3>();
  show_arg2<Op,tensor4,tensor4>();
  dout << endl;

  // 4) deduction de a1 & a2 quand on connait le resultat
  show_from_result<Op,Float>(); 
  show_from_result<Op,point>(); 
  show_from_result<Op,tensor>(); 
  show_from_result<Op,tensor3>(); 
  show_from_result<Op,tensor4>(); 
  dout << endl;

  // 5) deduction de a2 & r quand on connait a1
  show_from_arg1<Op,Float>(); 
  show_from_arg1<Op,point>(); 
  show_from_arg1<Op,tensor>(); 
  show_from_arg1<Op,tensor3>(); 
  show_from_arg1<Op,tensor4>(); 
  dout << endl;

  // 6) deduction de a1 & r quand on connait a2
  show_from_arg2<Op,Float>(); 
  show_from_arg2<Op,point>(); 
  show_from_arg2<Op,tensor>(); 
  show_from_arg2<Op,tensor3>(); 
  show_from_arg2<Op,tensor4>(); 
  dout << endl;

  // 7) deduction de a1, a2 & r quand ne connait rien
  show_from_none<Op>(); 
  dout << endl;
}
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  show_deduce<details::plus>();
  show_deduce<details::minus>();
  show_deduce<details::multiplies>();
  show_deduce<details::divides>();
  show_deduce<details::dot_>();
  show_deduce<details::ddot_>();
  show_deduce<details::pow_>();
}
