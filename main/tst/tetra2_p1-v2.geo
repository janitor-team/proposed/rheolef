#!geo

mesh
4
header
 dimension 3
 nodes	5
 tetrahedra	2
 triangles	7
 edges	9
end header

0 0 0
1 0 0
0 1 0
0 0 1
0 -1 0

T	0 1 2 3
T	0 4 1 3

t	0 2 1
t	0 3 2
t	0 1 3
t	1 2 3
t	0 1 4
t	0 4 3
t	4 1 3
e	0 1
e	1 2
e	2 0
e	0 3
e	1 3
e	2 3
e	0 4
e	4 1
e	4 3
