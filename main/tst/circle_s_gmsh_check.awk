#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# a way to compute 2*pi with gmsh
#	gmsh -1 -order 3 circle_s_gmsh.mshcad -o aa.msh
#	gawk -f circle_s_p3_check.awk < aa.msh
# => check convergence with mesh size & order
# ---------------------------------------------------
# functions
# ---------------------------------------------------
function max(x,y)
{
  if (x > y) return x; else return y;
}
function fabs(x,y)
{
  return max(x,-x);
}
# quadrature: Gauss, size=6, exact up to order=11
function set_quadrature()
{
  nquad = 6;
  w[1] = 0.085662246189585; s[1] = 0.0337652428984239;
  w[2] = 0.180380786524069; s[2] = 0.169395306766868;
  w[3] = 0.233956967286345; s[3] = 0.380690406958402;
  w[4] = 0.233956967286345; s[4] = 0.619309593041598;
  w[5] = 0.180380786524069; s[5] = 0.830604693233132;
  w[6] = 0.085662246189585; s[6] = 0.966234757101576;
}
# basis & derivatives: order 3, Lagrange, equispaced point set
function compute_phi(s)
{
  nbasis = order+1;
  if (order == 1) {
    phi[1] = -s+1.0;
    phi[2] =  s;
  } else if (order == 2) {
    phi[1] = -3.0*s+2.0*(s*s)+1.0;
    phi[2] = -s+2.0*(s*s);
    phi[3] = -4.0*(s*s)+4.0*s;
  } else if (order == 3) {
    phi[1] =  -(11.0/2.0)*s-(9.0/2.0)*(s*s*s)+9.0*(s*s)+1.0;
    phi[2] =  s-(9.0/2.0)*(s*s)+(9.0/2.0)*(s*s*s);
    phi[3] =  (27.0/2.0)*(s*s*s)+9.0*s-(45.0/2.0)*(s*s);
    phi[4] =  18.0*(s*s)-(9.0/2.0)*s-(27.0/2.0)*(s*s*s);
  } else if (order == 4) {
    phi[1] =  (32.0/3.0)*((s*s)*(s*s))+(70.0/3.0)*(s*s)-(25.0/3.0)*s-(80.0/3.0)*(s*s*s)+1.0;
    phi[2] =  -16.0*(s*s*s)+(22.0/3.0)*(s*s)-s+(32.0/3.0)*((s*s)*(s*s));
    phi[3] =  -(208.0/3.0)*(s*s)+96.0*(s*s*s)+16.0*s-(128.0/3.0)*((s*s)*(s*s));
    phi[4] =  64.0*((s*s)*(s*s))-128.0*(s*s*s)+76.0*(s*s)-12.0*s;
    phi[5] =  (16.0/3.0)*s+(224.0/3.0)*(s*s*s)-(112.0/3.0)*(s*s)-(128.0/3.0)*((s*s)*(s*s));
  } else if (order == 5) {
    phi[1] =  -(137.0/12.0)*s-(2125.0/24.0)*(s*s*s)+(375.0/8.0)*(s*s)-(625.0/24.0)*(s*(s*s)*(s*s))+(625.0/8.0)*((s*s)*(s*s))+1.0;
    phi[2] =  -(125.0/12.0)*(s*s)+(875.0/24.0)*(s*s*s)+s+(625.0/24.0)*(s*(s*s)*(s*s))-(625.0/12.0)*((s*s)*(s*s));
    phi[3] =  25.0*s+(3125.0/24.0)*(s*(s*s)*(s*s))-(4375.0/12.0)*((s*s)*(s*s))+(8875.0/24.0)*(s*s*s)-(1925.0/12.0)*(s*s);
    phi[4] =  (2675.0/12.0)*(s*s)-(7375.0/12.0)*(s*s*s)-(3125.0/12.0)*(s*(s*s)*(s*s))-25.0*s+(8125.0/12.0)*((s*s)*(s*s));
    phi[5] =  (3125.0/12.0)*(s*(s*s)*(s*s))-625.0*((s*s)*(s*s))+(50.0/3.0)*s+(6125.0/12.0)*(s*s*s)-(325.0/2.0)*(s*s);
    phi[6] =  -(3125.0/24.0)*(s*(s*s)*(s*s))+(6875.0/24.0)*((s*s)*(s*s))+(1525.0/24.0)*(s*s)-(5125.0/24.0)*(s*s*s)-(25.0/4.0)*s;
  }
}
function compute_dphi_ds(s)
{
  nbasis = order+1;
  if (order == 1) {
    dphi_ds[1] =  -1.0;
    dphi_ds[2] =   1.0;
  } else if (order == 2) {
    dphi_ds[1] =  4.0*s-3.0;
    dphi_ds[2] =  4.0*s-1.0;
    dphi_ds[3] = -8.0*s+4.0;
  } else if (order == 3) {
    dphi_ds[1] =  18.0*s-(27.0/2.0)*(s*s)-(11.0/2.0);
    dphi_ds[2] =  (27.0/2.0)*(s*s)-9.0*s+1.0;
    dphi_ds[3] = -45.0*s+(81.0/2.0)*(s*s)+9.0;
    dphi_ds[4] =  36.0*s-(81.0/2.0)*(s*s)-(9.0/2.0);
  } else if (order == 4) {
    dphi_ds[1] =  -80.0*(s*s)+(128.0/3.0)*(s*s*s)+(140.0/3.0)*s-(25.0/3.0);
    dphi_ds[2] =  (44.0/3.0)*s+(128.0/3.0)*(s*s*s)-48.0*(s*s)-1.0;
    dphi_ds[3] =  -(416.0/3.0)*s+288.0*(s*s)-(512.0/3.0)*(s*s*s)+16.0;
    dphi_ds[4] =  152.0*s+256.0*(s*s*s)-384.0*(s*s)-12.0;
    dphi_ds[5] =  -(224.0/3.0)*s+224.0*(s*s)-(512.0/3.0)*(s*s*s)+(16.0/3.0);
  } else if (order == 5) {
    dphi_ds[1] =  (625.0/2.0)*(s*s*s)-(2125.0/8.0)*(s*s)+(375.0/4.0)*s-(3125.0/24.0)*((s*s)*(s*s))-(137.0/12.0);
    dphi_ds[2] =  (3125.0/24.0)*((s*s)*(s*s))-(125.0/6.0)*s-(625.0/3.0)*(s*s*s)+(875.0/8.0)*(s*s)+1.0;
    dphi_ds[3] =  (15625.0/24.0)*((s*s)*(s*s))-(1925.0/6.0)*s+(8875.0/8.0)*(s*s)-(4375.0/3.0)*(s*s*s)+25.0;
    dphi_ds[4] =  (8125.0/3.0)*(s*s*s)+(2675.0/6.0)*s-(7375.0/4.0)*(s*s)-(15625.0/12.0)*((s*s)*(s*s))-25.0;
    dphi_ds[5] =  (15625.0/12.0)*((s*s)*(s*s))-2500.0*(s*s*s)+(6125.0/4.0)*(s*s)-325.0*s+(50.0/3.0);
    dphi_ds[6] =  (1525.0/12.0)*s+(6875.0/6.0)*(s*s*s)-(5125.0/8.0)*(s*s)-(15625.0/24.0)*((s*s)*(s*s))-(25.0/4.0);
  }
}
function init_iseq2imsh()
{
  iseq2imsh[1] = 1;
  iseq2imsh[order+1] = 2;
  for (i = 2; i <= order; i++) {
    iseq2imsh[i] = i+1;
  }
  for (i = 1; i <= order+1; i++) {
    imsh2iseq[iseq2imsh[i]] = i;
  }
}
function check_basis()
{
  status = 0;
  for (jseq = 1; jseq <= order+1; jseq++) {
    sj = (1.0*(jseq-1))/(1.0*order);
    #j = jseq;
    j = iseq2imsh[jseq];
    compute_phi(sj);
    for (i = 1; i <= order+1; i++) {
      phi_i_at_sj = phi[i];
      delta_ij = ((i == j) ? 1 : 0);
      if (fabs(phi_i_at_sj - delta_ij) > machine_epsilon) {
        print "FATAL: invalid basis: phi(",i,j,")=phi",i,"(",sj,")=",phi[i] > "/dev/stderr"; 
	status = 1;
      }
    }
  }
  if (status != 0) {
	exit(1);
  }
}
# basis & derivatives: evaluation on quadrature point set
function compute_phi_at_quad()
{
  for (q = 1; q <= nquad; q++) {
   compute_phi(s[q]);
   for (i = 1; i <= nbasis; i++) {
     phi_at_quad[i,q] = phi[i]; 
   }
  }
}
function compute_dphi_ds_at_quad()
{
  for (q = 1; q <= nquad; q++) {
   compute_dphi_ds(s[q]);
   for (i = 1; i <= nbasis; i++) {
     dphi_ds_at_quad[i,q] = dphi_ds[i]; 
   }
  }
}
# piola: jacobian at q-th quadrature point in elementb elt: 
function piola_x(q, elt)
{
   xq = 0;
   for (i = 1; i <= nbasis; i++) {
     xq += x[elt[i]]*phi_at_quad[i,q]; 
   }
   return xq;
}
function piola_y(q, elt)
{
   yq = 0;
   for (i = 1; i <= nbasis; i++) {
     yq += y[elt[i]]*phi_at_quad[i,q]; 
   }
   return yq;
}
function piola_jacobian(q, elt)
{
   dx_ds = 0;
   dy_ds = 0;
   for (i = 1; i <= nbasis; i++) {
     dx_ds += x[elt[i]]*dphi_ds_at_quad[i,q]; 
     dy_ds += y[elt[i]]*dphi_ds_at_quad[i,q]; 
   }
   Jq = sqrt(dx_ds*dx_ds + dy_ds*dy_ds);
   return Jq;
}
# max interpolation error at quadrature point set
function compute_max_interpolation_error_at_quad(elt)
{
  err = 0;
  for (q = 1; q <= nquad; q++) {
    xq = piola_x(q, elt);
    yq = piola_y(q, elt);
    r = xq*xq + yq*yq;
    err += max(err, fabs(1-r));
  }
  return err;
}
function compute_length(elt,nx,ny)
{
  value = 0;
  for (q = 1; q <= nquad; q++) {
    xq = piola_x(q, elt);
    yq = piola_y(q, elt);
    value += (xq**nx)*(yq**ny)*w[q]*piola_jacobian(q, elt);
  }
  return value;
}
function check_equispaced(elt)
{
   xprec = x[elt[iseq2imsh[1]]];
   yprec = y[elt[iseq2imsh[1]]];
   variation = 0;
   for (i = 2; i <= order+1; i++) {
     xi = x[elt[iseq2imsh[i]]];
     yi = y[elt[iseq2imsh[i]]];
     d = sqrt((xi-xprec)**2 + (yi-yprec)**2);
     xprec = xi;
     yprec = yi;
     if (i == 2) {
       d0 = d;
     } else {
       variation = max(variation, fabs(d-d0));
     }
   }
   return variation;
}
#  I_{m,n}
#  = int_Omega x^m y^n dx
#  = (int_{r=0}^1 r^{m+n+1} dr)*(int_0^{2*pi} cos(theta)^m sin(theta)^n d theta)
#  et :
#   int_{r=0}^1 r^{m+n+1} dr = 1/(m+n+2)
#  => I_{m,n}   = J_{m,n}/(m+n+2)
#  J_{m,n} = int_0^{2*pi} cos(theta)^m sin(theta)^n d theta
#	cf Bro-1985, p. 557 & 561 : recurrence
#  J_{m,n} = 0 si m ou n impair
#  J_{2p,2q} = (2q-1)/(2p+2q)*J_{2p,2q-2}
#  J_{2p,0} = (2p-1)/(2p)*J_{2p-2,0}
#  J_{0,0} = 2*pi
#                  (2p)!
#  => J_{2p,0} = ------------ * (2*pi)        pour q = 0
#	        (2^p * p!)^2
#                 (2p)! (2q-1)! pi
#  => J_{2p,2q} = --------------------------  pour q >=1
#                 4^(p+q-1) p! (q-1)! (p+q)!
function factorial(n)
{
  res = 1.0;
  for (i = 1; i <= n; i++) res = res*i;
  return res;
}
function integrate_xy(m,n)
{
  if ((m % 2 == 1) || (n % 2 == 1)) return 0;
  p = m/2;
  q = n/2;
  pi = 3.14159265358979323846;
  if (q == 0) {
    deno = (2.0**p)*factorial(p);
    value = 2*pi*factorial(2*p)/(deno*deno);
  } else {
    value = pi*factorial(2*p)*factorial(2*q-1)/(4**(p+q-1)*factorial(p)*factorial(q-1)*factorial(p+q));
  }
  return value;
}
# ---------------------------------------------------
# initialization: input parameters:
#     order (required)
#     nx    = 0 by default
#     ny    = 0 by default
# ---------------------------------------------------
BEGIN {
        if (order <= 0) { print "FATAL: usage: gawk -v order=value thisfile.awk" > "/dev/stderr"; exit(1); }
        machine_epsilon = 1e-11;
        pi = 3.14159265358979323846;
        init_iseq2imsh();
        check_basis();
   	set_quadrature();
        compute_phi_at_quad();
        compute_dphi_ds_at_quad();
	state = 0;
        perimeter = 0;
        error_linf = 0;
        node_error = 0;
        equispaced_error = 0;
  }
# ---------------------------------------------------
# line loop
# ---------------------------------------------------
($1 == "$Nodes" && state == 0) {
	state = 1;
	next;
	}
($1 != "" && state == 1) {
	nnod = $1;
	inod = 0;
	state = 2;
	next;
	}
($1 != "" && state == 2) {
	x[$1] = $2;
	y[$1] = $3;
        r = $2*$2 + $3*$3;
        node_error = max(node_error, fabs(1-r));
	inod++;
	if (inod == nnod) state = 3;
	next; 
	}
($1 == "$Elements" && state == 3) {
	nelt = $1;
	ielt = 0;
	state = 4;
	next;
	}
($1 != "" && state == 4) {
	nelt = $1;
	ielt = 0;
	state = 5;
	next;
    }
($1 != "" && state == 5) {
        type = $2;
             if (type ==  1) { elt_order = 1; }
        else if (type ==  8) { elt_order = 2; }
        else if (type == 26) { elt_order = 3; }
        else if (type == 27) { elt_order = 4; }
        else if (type == 28) { elt_order = 5; }
        else                 { print "FATAL: invalid gmsh type ",type > "/dev/stderr"; exit(1); }
        if (elt_order != order) { print "invalid element order ",elt_order,": expect ", order > "/dev/stderr"; exit(1); }
        start = $3 + 4;
	for (i = start; i <= NF; i++) {
	  elt[i-start+1] = $i;
	}
        elt_length = compute_length(elt,nx,ny);
        perimeter += elt_length;
        elt_error = compute_max_interpolation_error_at_quad(elt);
        error_linf = max(error_linf, elt_error);
	ielt++;
        elt_variation = check_equispaced(elt);
        equispaced_error = max(equispaced_error, elt_variation);
	if (ielt == nelt) state = 6;
	next;
    }
# ---------------------------------------------------
# epilogue
# ---------------------------------------------------
END {
	exact_perimeter = integrate_xy(nx,ny);
        error_perimeter = fabs(perimeter -  exact_perimeter);
        printf("measure        = %.16g\n", perimeter);
        printf("exact_measure  = %.16g\n", exact_perimeter);
        printf("error_measure  = %.16g\n", error_perimeter);
        printf("error_linf     = %.16g\n", error_linf);
        if (node_error > machine_epsilon) {
          printf("FATAL: node_error       = %.16g\n", node_error) > "/dev/stderr";
          exit (1);
        }
        if (equispaced_error > machine_epsilon) {
          printf("FATAL: equispaced_error  = %.16g\n", equispaced_error) > "/dev/stderr";
          exit (1);
        }
    }
