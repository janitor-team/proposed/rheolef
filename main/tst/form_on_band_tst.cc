///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// test des formes grad_grad_s et mass_s
//
#include "rheolef.h"
using namespace std;
using namespace rheolef;
// --------------------------------------------------------------------------------
// definition de phi = fct level set
// --------------------------------------------------------------------------------
point my_origin (0.5,0.5,0);
point my_normal (0,1,0);
Float phi (const point& x) { return dot(my_normal, x-my_origin); }
// --------------------------------------------------------------------------------
// test
// --------------------------------------------------------------------------------
string i_test = "1";
Float u (const point& x) {
  if (i_test == "1") return 1;
  if (i_test == "x") return x[0];
  if (i_test == "y") return x[1];
  if (i_test == "z") return x[2];
  if (i_test == "x2") return sqr(x[0]);
  if (i_test == "y2") return sqr(x[1]);
  if (i_test == "z2") return sqr(x[2]);
  if (i_test == "xy") return x[0]*x[1];
  if (i_test == "xz") return x[0]*x[2];
  if (i_test == "yz") return x[1]*x[2];
  error_macro ("invalid test " << i_test);
  return 0;
}
// --------------------------------------------------------------------------------
// utilities
// --------------------------------------------------------------------------------
void save (const form& a, string name = "a") {
  string filename = "matrix-" + name + ".mtx";
  odiststream out (filename.c_str());
  out.os() << setprecision(15);
  out << a.uu();
}
void save (const field& u, string name = "u") {
  string filename = "vector-" + name + ".mtx";
  odiststream out (filename.c_str());
  out << u.u();
}
void 
usage () {
  derr << "usage: grad_grad_tst " 
       << "mesh.geo " 
       << "-normal x y z" 
       << "-origin x y z" 
       << "-a val " 
       << "-m val " 
       << "-test poly " 
       << "[-dump] " 
       << endl;
  exit (1);
}
int main (int argc, char**argv)
{
  environment rheolef (argc,argv);
  Float tol = 1e-7;
  Float a_valid = 0;
  Float m_valid = -1;
  bool do_dump = false;
  int status = 0;
  string filename;
  level_set_option opts;
  for (int i = 1; i < argc; i++) {
    if (strcmp (argv[i], "-test") == 0)   {
      if (i+1 == argc) usage();
      i_test = argv[++i];
    } else if (strcmp (argv[i], "-a") == 0)   {
      if (i+1 == argc || !is_float(argv[i+1])) usage();
      a_valid = to_float (argv[++i]);
    } else if (strcmp (argv[i], "-m") == 0)   {
      if (i+1 == argc || !is_float(argv[i+1])) usage();
      m_valid = to_float (argv[++i]);
    } else if ((strcmp (argv[i], "-origin") == 0) || (strcmp (argv[i], "-normal") == 0))   {
      point x;
      size_t io = i;
      if (i+1 == argc || !is_float(argv[i+1])) {
        warning_macro ("invalid argument to `" << argv[i] << "'");
        usage();
      }
      x[0] = to_float (argv[++i]);
      if (i+1 < argc && is_float(argv[i+1])) {
        x[1] = to_float (argv[++i]);
        if (i+1 < argc && is_float(argv[i+1])) {
          x[2] = to_float (argv[++i]);
        }	
      }	
      if (strcmp (argv[io], "-origin") == 0)   {
        my_origin = x;
      } else {
	my_normal = x;
      }
    } else if (strcmp (argv[i], "-epsilon") == 0)   {
      if (i+1 == argc || !is_float(argv[i+1])) {
        warning_macro ("invalid argument to `" << argv[i] << "'");
        usage();
      }
      opts.epsilon = to_float (argv[++i]);
      dis_warning_macro ("epsilon="<<opts.epsilon);
    } else if (strcmp (argv[i], "-dump") == 0)   {
      do_dump = true;
    } else {
      // input on file
      filename = argv[i];
    }
  }
  if (filename == "") usage();
  geo lambda(filename);
  space Xh (lambda, "P1");
  field phi_h_lambda = lazy_interpolate(Xh, phi);
  band gh (phi_h_lambda, opts);
  space Bh (gh.band(), "P1");
  field phi_h = lazy_interpolate(Bh, phi);
  if (do_dump) save(phi_h, "phi");
  field uh = lazy_interpolate (Bh, u);
  if (do_dump) save(uh, "u");
  // ------------------------------------------------------
  // test mass
  // ------------------------------------------------------
  trial u (Bh); test v (Bh);
  form m = integrate (gh, u*v);
  if (do_dump) save(m, "m");
  Float m_tst = m(uh,uh);
  derr.os() << setprecision(15);
  derr << "m(uh,uh)=" << m_tst << endl;
  if (fabs(m_tst - m_valid) >= tol) {
    derr << "ERROR: m(uh,uh)=" << m_tst << " != " << m_valid << endl;
    status = 1;
  }
  // ------------------------------------------------------
  // test grad_grad
  // ------------------------------------------------------
  form a = integrate (gh, dot(grad_s(u),grad_s(v)));
  if (do_dump) save(a, "a");
  Float a_tst = a(uh,uh);
  derr << "a(uh,uh)=" << a_tst << endl;
  if (fabs(a_tst - a_valid) >= tol) {
    derr << "ERROR: a(uh,uh)=" << a_tst << " != " << a_valid << endl;
    status = 1;
  }
  if (do_dump) {
    gh.level_set().save();
    gh.band().save();
  }
  return status; 
}
