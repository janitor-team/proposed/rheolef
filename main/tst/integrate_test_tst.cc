///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;

// --------------------------------------------------------------------------
// some simple mass check
// --------------------------------------------------------------------------
int check_field_meas_omega (geo omega, string approx, Float eps) {
  space Vh (omega, approx);
  test v(Vh);
  field one (Vh, 1.);
  field lh = integrate (v);
  Float meas_omega = dual(lh,one);
  Float meas_omega_expected = 1;
  Float err = fabs(meas_omega - meas_omega_expected);
  derr << "field_meas_omega = " << meas_omega << endl
       << "field_meas_omega_expected = " << meas_omega_expected << endl
       << "err = " << err << endl;
  return (err < eps) ? 0 : 1;
}
int check_field_meas_bdry_omega (geo omega, string approx, Float eps) {
  space Vh (omega, approx);
  test v(Vh);
  field lh = integrate ("boundary",v);
  field one (Vh, 1.);
  Float meas_bdry_omega = dual(lh,one);
  Float meas_bdry_omega_expected = 4;
  Float err = fabs(meas_bdry_omega - meas_bdry_omega_expected);
  derr << "field_meas_bdry_omega = " << meas_bdry_omega << endl
       << "field_meas_bdry_omega_expected = " << meas_bdry_omega_expected << endl
       << "err = " << err << endl;
  return (err < eps) ? 0 : 1;
}
int check_form_meas_omega (geo omega, string approx, Float eps) {
  space Vh (omega, approx);
  trial u(Vh); test v(Vh);
  form m = integrate (u*v);
  field one (Vh, 1.);
  Float meas_omega = m(one,one);
  Float meas_omega_expected = 1;
  Float err = fabs(meas_omega - meas_omega_expected);
  derr << "form_meas_omega = " << meas_omega << endl
       << "form_meas_omega_expected = " << meas_omega_expected << endl
       << "err = " << err << endl;
  return (err < eps) ? 0 : 1;
}
int check_form_meas_bdry_omega (geo omega, string approx, Float eps) {
  space Vh (omega, approx);
  trial u(Vh); test v(Vh);
  form m = integrate ("boundary", u*v);
  field one (Vh, 1.);
  Float meas_bdry_omega = m(one,one);
  Float meas_bdry_omega_expected = 4;
  Float err = fabs(meas_bdry_omega - meas_bdry_omega_expected);
  derr << "form_meas_bdry_omega = " << meas_bdry_omega << endl
       << "form_meas_bdry_omega_expected = " << meas_bdry_omega_expected << endl
       << "err = " << err << endl;
  return (err < eps) ? 0 : 1;
}
// --------------------------------------------------------------------------
// grad
// --------------------------------------------------------------------------
struct poly {
  Float operator() (const point& x) const { return pow(x[0],k); }
  poly (size_t k1) : k(k1) {}
  size_t k;
};
struct grad_poly {
  point operator() (const point& x) const { return k*point(pow(x[0],k-1)); }
  grad_poly (size_t k1) : k(k1) {}
  size_t k;
};
int check_field_grad (geo omega, string approx, Float eps) {
  space Vh (omega, approx);
  size_t k = Vh.degree();
  if (k == 0) return 0;
  space Th (omega, "P"+std::to_string(k-1)+"d", "vector");
  trial u(Vh), sigma(Th);
  test  v(Vh), tau  (Th);
  integrate_option iopt; iopt.invert = true;
  form m = integrate (dot(sigma,tau));
  form inv_m = integrate (dot(sigma,tau), iopt);
  form g = integrate (dot(grad(u),tau));
  size_t p = k;
  field uh = lazy_interpolate (Vh, poly(p));
  field lh = g*uh;
  field grad_uh = inv_m*lh;
  field pi_h_grad_u = lazy_interpolate (Th, grad_poly(p));
  Float field_grad_err = field(pi_h_grad_u - grad_uh).max_abs();
  derr << "field_grad_err = " << field_grad_err << endl;
  derr << "eps            = " << eps << endl;
  return (field_grad_err < eps) ? 0 : 1;
}
// --------------------------------------------------------------------------
int main(int argc, char**argv) {
// --------------------------------------------------------------------------
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1";
  Float eps = 1e6*std::numeric_limits<Float>::epsilon();
  int status = 0;
  status += check_field_meas_omega      (omega, approx, eps);
  status += check_field_meas_bdry_omega (omega, approx, eps);
  status += check_form_meas_omega       (omega, approx, eps);
  status += check_form_meas_bdry_omega  (omega, approx, eps);
  status += check_field_grad            (omega, approx, eps);
  return status;
}
