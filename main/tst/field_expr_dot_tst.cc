///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check field expr with normal() operator
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#define BUG_FIELD_COMPONENT_ACCESS
#ifdef BUG_FIELD_COMPONENT_ACCESS
point u0(const point& x) { return point(1,0,0); }
#endif // BUG_FIELD_COMPONENT_ACCESS
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  Float prec = (argc > 3) ? atof(argv[3]) : 1e-10;
  space X0h (omega, "P1");
  space Xh  (omega, "P1", "vector");
  space W0h (omega["boundary"], "P0");
  field uh (Xh, 0);
#ifndef BUG_FIELD_COMPONENT_ACCESS
  uh[0] = 1;
#else // BUG_FIELD_COMPONENT_ACCESS
  uh = lazy_interpolate (Xh, u0);
#endif // BUG_FIELD_COMPONENT_ACCESS
dis_warning_macro ("normal...");
  field zh = lazy_interpolate (W0h, dot(uh, normal()));
dis_warning_macro ("normal done");
  dout << round (zh, prec);
}
