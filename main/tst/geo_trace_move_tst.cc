///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//  given x and v, search K in mesh such that y=x+v in K
//  and when x+v goes outside, ray trace the y point on the boundary
//
//  the y as argument is used for non-regression test purpose
//
#include "rheolef/geo.h"
#include "rheolef/rheostream.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;
void usage() {
      cerr << "prog: usage:" << endl
           << "prog "
           << "[-x [x0 [x1 [x2]]]] "
           << "[-v [v0 [v1 [v2]]]] "
           << "[-y [y0 [y1 [y2]]]] "
           << "< file.geo "
           << endl;
      exit (1);
}
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  Float tol = 1e-7;
  point x, v, y_exact;
  for (int i = 1; i < argc; i++) {
    if ((strcmp (argv[i], "-x") == 0) || 
        (strcmp (argv[i], "-v") == 0) ||
        (strcmp (argv[i], "-y") == 0))  {

      point p;
      size_t io = i;
      if (i+1 == argc || !is_float(argv[i+1])) {
	warning_macro ("invalid argument to `" << argv[i] << "'");
        usage();
      }
      p[0] = to_float (argv[++i]);
      if (i+1 < argc && is_float(argv[i+1])) {
        p[1] = to_float (argv[++i]);
        if (i+1 < argc && is_float(argv[i+1])) {
	  p[2] = to_float (argv[++i]);
        }	
      }	
      if (strcmp (argv[io], "-x") == 0) {
        x = p;
      } else if (strcmp (argv[io], "-v") == 0) {
        v = p;
      } else {
        y_exact = p;
      }
    } else {
      usage();
    }
  }
  geo omega;
  din >> omega;
  point y;
  size_t dis_ie = omega.dis_trace_move (x,v,y);
  Float err = dist(y,y_exact);
  size_t d = omega.dimension();
  dout << "y      = " << ptos(y,d) << endl
       << "dis_ie = " << dis_ie << endl
       << "err    = " << err << endl;
  return (err < tol) ? 0 : 1;
}
