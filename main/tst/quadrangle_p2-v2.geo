#!geo

mesh
4
header
 dimension 2
 order     2
 nodes	9
 quadrangles	1
 edges	4
end header

0 0
1 0
1 1
0 1
0.75 0.125
1.25 0.625
0.75 1.125
0.25 0.625
1 0.75

q	0 1 2 3

e	0 1
e	1 2
e	2 3
e	3 0
