///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// check compose(f,field_expr) for f as a true function
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float g  (const point& x) { return sin(x[0]+x[1]+0.5); }
Float f  (const Float& u) { return u*(1-u); }
#ifdef TODO
Float h  (const Float& u, Float v) { return v*(1-u); }
#endif // TODO
struct h : std::binary_function<Float,Float,Float> {
  Float operator() (const Float& u, const Float& v) const { return v*(1-u); }
};
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]); 
  string dom_name = (argc > 2) ? argv[2] : "right";
  Float prec = (argc > 3) ? atof(argv[3]) : 1e-10;
  space Vh (omega, "P1");
  space Wh (omega[dom_name], "P1");
  field fh = lazy_interpolate(Vh, g);
  field gh = lazy_interpolate(Wh, g);
  field zh = lazy_interpolate (Wh, atan2(gh,fh[dom_name]/2) + max(abs(fh[dom_name]), Float(0)) + compose(h(), fh[dom_name]+2, Float(0)));
  dout << round (zh, prec);
}
