///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct f1 {
  Float operator() (const point& x) const { return sqrt(fabs(x[0])); }
};
struct f2 {
  f2(size_t d) : _v() { _v[d-1] = -1; }
  point operator() (const point& x) const { return _v; }
  point _v;
};
struct rotation {
  rotation(size_t d1) : d(d1) {}
  point operator() (const point& x) const {
    // return (d==1) ? point(x[0]) : point(x[1], -x[0]); } // PB
    // return (d==1) ? point(x[0]) : point(x[0], 5); } // PB
    return (d==1) ? point(x[0]) : point(1, x[0]); }
  size_t d;
}; 
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega(argv[1]);
  size_t d = omega.dimension();
  omega.boundary(); // add boundary...
  space Xh (omega, argv[2]);
  Float tol = (argc > 3) ? atof(argv[3]) : 1e-10;
  test v(Xh);
  quadrature_option qopt;
  qopt.set_family (quadrature_option::gauss);
  qopt.set_order  (2*Xh.degree()-1);
  Float err = 0;

  // scalar fct:
  field l1h_old = riesz (Xh, f1(), qopt);
  field l1h_new = integrate (omega, f1()*v, qopt);
#ifdef TO_CLEAN
  derr << "l1h(old)="<<l1h_old; 
  derr << "l1h(new)="<<l1h_new;
#endif // TO_CLEAN
  field e1h = l1h_new-l1h_old;
  Float err1 = e1h.max_abs();
  derr << "err1 = " << err1 << endl;
  err = std::max(err,err1);

  // vector fct:
  space Xvh (omega, argv[2], "vector");
  test vv(Xvh);
  field l2h_old = riesz (Xvh, f2(d), qopt);
  field l2h_new = integrate (omega, dot(f2(d),vv), qopt);
#ifdef TO_CLEAN
  derr << "l2h(old)="<<l2h_old; 
  derr << "l2h(new)="<<l2h_new;
#endif // TO_CLEAN
  field e2h = l2h_new-l2h_old;
  Float err2 = e2h.max_abs();
  derr << "err2 = " << err2 << endl;
  err = std::max(err,err2);

  // cte vector fct:
  point f3 (0,0,0);
  f3[d-1] = -1;
  field l3h_old = riesz (Xvh, f3, qopt);
  field l3h_new = integrate (omega, dot(f3,vv), qopt);
#ifdef TO_CLEAN
  derr << "l3h(old)="<<l3h_old;
  derr << "l3h(new)="<<l3h_new;
#endif // TO_CLEAN
  field e3h = l3h_new-l3h_old;
  Float err3 = e3h.max_abs();
  derr << "err3 = " << err3 << endl;
  err = std::max(err,err3); 

  // characteristic fct: not supported for P3 and more and on non-simplex meshes 
  // not robust in 3D when nproc > 1
  communicator comm;
  size_t nproc = comm.size();
#ifdef _RHEOLEF_HAVE_CGAL
  bool tst_charact
   = ( d == 1 ||
      (d == 2 && Xh.degree() <= 2 && omega.sizes().ownership_by_variant[reference_element::q].dis_size() == 0));
#else
  bool tst_charact = false;
#endif // _RHEOLEF_HAVE_CGAL

#ifdef TODO
  // TODO: when d==3: characteristic method calls now nearest that is not implemented in 3D
  // => fatal(../../../rheolef/nfem/plib/geo_nearest.cc,323): unsupported dimension d=3
      (d == 3 && Xh.degree() <= 1 && nproc == 1 &&
	  omega.sizes().ownership_by_variant[reference_element::P].dis_size() == 0 &&
	  omega.sizes().ownership_by_variant[reference_element::H].dis_size() == 0));
#endif //
  warning_macro ("tst_charact="<<tst_charact);

#if (__GNUC__ > 4) || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7) // TODO_GXX48
  if (tst_charact) {
    // scalar-valued convected field
    field uh = lazy_interpolate (Xvh, f2(d));
    Float delta_t = 0.1;
    characteristic X (-delta_t*uh);
    field phi_h = lazy_interpolate (Xh, f1());
    quadrature_option qloba;
    qloba.set_family(quadrature_option::gauss_lobatto);
    qloba.set_order(Xh.degree());
    field l4h_old = riesz (Xh, compose(phi_h, X),   qloba);
    field l4h_new = integrate (compose(phi_h, X)*v, qloba);
#ifdef TO_CLEAN
    derr << "l4h(old)="<<l4h_old;
    derr << "l4h(new)="<<l4h_new;
#endif // TO_CLEAN
    field e4h = l4h_new-l4h_old;
    Float err4 = e4h.max_abs();
    derr << "err4 = " << err4 << endl;
    err = std::max(err,err4); 
  }
  if (tst_charact) {

    // vector-valued convected field
    field uh = lazy_interpolate (Xvh, rotation(d));
    Float delta_t = 0.1;
    characteristic X (-delta_t*uh);
    field phi_h = lazy_interpolate (Xvh, rotation(d)); 
    quadrature_option qloba;
    qloba.set_family(quadrature_option::gauss_lobatto);
    qloba.set_order(Xvh.degree());
    field l5h_old = riesz (Xvh, compose(phi_h, X), qloba);
    field l5h_new = integrate (dot(compose(phi_h, X),vv), qloba);
#ifdef TO_CLEAN
    derr << "l5h(old)="<<l5h_old;
    derr << "l5h(new)="<<l5h_new;
#endif // TO_CLEAN
    field e5h = l5h_new-l5h_old;
    Float err5 = e5h.max_abs();
    derr << "err5 = " << err5 << endl;
    err = std::max(err,err5); 
  }
#endif // TODO_GXX48

  // scalar-field * scalar-test
  field f6h = lazy_interpolate (Xh, f1());
  field l6h_old = riesz (Xh, f6h, qopt);
  field l6h_new = integrate (omega, f6h*v, qopt);
#ifdef TO_CLEAN
  derr << "l6h(old)="<<l6h_old; 
  derr << "l6h(new)="<<l6h_new;
#endif // TO_CLEAN
  field e6h = l6h_new-l6h_old;
  Float err6 = e6h.max_abs();
  derr << "err6 = " << err6 << endl;
  err = std::max(err,err6);

#ifdef TODO
  // vector-field . grad(test)
  field c7h = lazy_interpolate (Xh, f1());
  field f7h = lazy_interpolate (Xvh, f2());
  field l7h_new = integrate (omega, dot(f7h,c7h*grad(v)), qopt);
#ifdef TO_CLEAN
  derr << "l7h(new)="<<l7h_new;
#endif // TO_CLEAN

#endif // TODO

  return (err < tol) ? 0 : 1;
}
