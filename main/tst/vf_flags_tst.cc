///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
/// =========================================================================
#include "rheolef/config.h"
#include "rheolef/linalg.h"
// TODO: is_symmetric<expr> ?
// -----------------------------------
// vf_tag output:
// -----------------------------------
namespace rheolef {
namespace details {
// TODO: vf_tag<"00"> ?
struct                                 vf_tag_nonlinear {};
typedef std::pair<std::false_type,std::false_type>  vf_tag_00;
typedef std::pair<std::false_type,std::true_type >  vf_tag_01;
typedef std::pair<std::true_type, std::true_type >  vf_tag_11;
typedef std::pair<std::true_type, std::false_type>  vf_tag_10;

template<class G>
struct vf_tag_name            { static constexpr const char* value = "nonlinear"; };
template<>
struct vf_tag_name<vf_tag_00> { static constexpr const char* value = "00"; }; // constant
template<>
struct vf_tag_name<vf_tag_01> { static constexpr const char* value = "01"; }; // test-linear 
template<>
struct vf_tag_name<vf_tag_10> { static constexpr const char* value = "10"; }; // trial-linear
template<>
struct vf_tag_name<vf_tag_11> { static constexpr const char* value = "11"; }; // bilinear

} // namespace details
// -----------------------------------
// test & trial
// -----------------------------------
template <class T, class M, class G>
struct test_basic {
  typedef G                    vf_tag_t;
  static constexpr const char* vf_tag_name = details::vf_tag_name<vf_tag_t>::value;
  static const char*       get_vf_tag_name() { return vf_tag_name; }
};

template <class T, class M, class G>
std::ostream& 
operator<< (std::ostream& out, const test_basic<T,M,G>& x)
{
  out << "test[" << test_basic<T,M,G>::vf_tag_name << "]";
  return out;
}
typedef test_basic<Float,rheo_default_memory_model,details::vf_tag_01> test;
typedef test_basic<Float,rheo_default_memory_model,details::vf_tag_10> trial;

// -----------------------------------
// exprs
// -----------------------------------
template <class Op, class G>
struct expr {
  typedef G                    vf_tag_t;
  static constexpr const char* vf_tag_name = details::vf_tag_name<vf_tag_t>::value;
  static const char*       get_vf_tag_name() { return vf_tag_name; }
};
template <class Op, class G>
std::ostream& 
operator<< (std::ostream& out, const expr<Op,G>& x)
{
  out << "expr[" << expr<Op,G>::vf_tag_name << "]";
  return out;
}
// -----------------------------------
//  mult
// -----------------------------------
namespace details {
// tag de linearite par rapport a une variable
template <class Op, class G1, class G2>
struct op_vf_tag                                      { typedef vf_tag_nonlinear type; };

struct mult {};
template<> struct op_vf_tag<mult,vf_tag_00,vf_tag_00> { typedef vf_tag_00 type; };

template<> struct op_vf_tag<mult,vf_tag_00,vf_tag_01> { typedef vf_tag_01 type; };
template<> struct op_vf_tag<mult,vf_tag_01,vf_tag_00> { typedef vf_tag_01 type; };

template<> struct op_vf_tag<mult,vf_tag_00,vf_tag_10> { typedef vf_tag_10 type; };
template<> struct op_vf_tag<mult,vf_tag_10,vf_tag_00> { typedef vf_tag_10 type; };

template<> struct op_vf_tag<mult,vf_tag_01,vf_tag_10> { typedef vf_tag_11 type; };
template<> struct op_vf_tag<mult,vf_tag_10,vf_tag_01> { typedef vf_tag_11 type; };
template<> struct op_vf_tag<mult,vf_tag_00,vf_tag_11> { typedef vf_tag_11 type; };
template<> struct op_vf_tag<mult,vf_tag_11,vf_tag_00> { typedef vf_tag_11 type; };

} // namespace details

// on a le droit de multiplier u*u mais ce n'est plus lineaire => tag_nonlinear
// tag pourrait valoir "01n": 0=cte, 1=lin, n=nonlin ?
template <class T, class M, class G1, class G2>
expr<details::mult,typename details::op_vf_tag<details::mult,G1,G2>::type>
operator* (const test_basic<T,M,G1>& x, test_basic<T,M,G2>& y) 
{
  return expr<details::mult,typename details::op_vf_tag<details::mult,G1,G2>::type>();
}
// -----------------------------------
// add
// -----------------------------------
namespace details {
struct add{};
template<class Tag> struct op_vf_tag<add,Tag,Tag> { typedef Tag type; };
} // namespace details

// on a le droit d'ajouter u+u*v ou bien u+c mais ce n'est plus (bi-)lineaire => tag_nonlinear
template <class T, class M, class G1, class G2>
expr<details::add,typename details::op_vf_tag<details::add,G1,G2>::type>
operator+ (const test_basic<T,M,G1>& x, test_basic<T,M,G2>& y) 
{
  return expr<details::add,typename details::op_vf_tag<details::add,G1,G2>::type>();
}
} // namespace rheolef

using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  trial u;
  test  v; 
  cout << "u=" << u << endl;
  cout << "v=" << v << endl;

  cout << "u+u=" << u+u << endl;
  cout << "v+v=" << v+v << endl;
  cout << "u+v=" << u+v << endl;
  cout << "v+u=" << v+u << endl;

  cout << "u*u=" << u*u << endl;
  cout << "v*v=" << v*v << endl;
  cout << "u*v=" << u*v << endl;
  cout << "v*u=" << v*u << endl;
#ifdef TODO
#endif // TODO
}
