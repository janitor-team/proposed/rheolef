///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  string approx = argv[2];
  string dom_name = (argc > 3) ? argv[3] : "right";
  Float prec = (argc > 4) ? atof(argv[4]) : 1e-10;
  space Wh (omega[dom_name], approx);
  field gh (Wh);
  din >> gh; // re-use Wh instead of creating a new space & geo
  dout << round (gh, prec);
  check_macro (gh.get_space().get_geo().data().variant() == geo_abstract_base_rep<Float>::geo_domain,
	"field input may re-use the space here");
}
