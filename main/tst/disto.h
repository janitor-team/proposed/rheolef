#ifndef _RHEOLEF_DISTO_H
#define _RHEOLEF_DISTO_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/field_functor.h"
namespace rheolef {
// P_order piola-triangle transformation here
template <class T>
struct disto_t : field_functor<disto_t<T>,point_basic<T> > {
  point_basic<T> operator() (const point_basic<T>& x) const {
    point_basic<T> Fx;
    Fx[0] = x[0] + a*pow(x[0]+  x[1], order);
    Fx[1] = x[1] + b*pow(x[0]+2*x[1], order);
    return Fx;
  }
  disto_t(size_t order1) : order(order1), a(T(1)/T(10*order)), b(T(1)/T(7*order)) {}
  size_t order; T a, b;
};
} // namespace rheolef
#endif // _RHEOLEF_DISTO_H
