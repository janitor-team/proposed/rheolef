#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
x=$1
n=20
gmsh -3 ${x}_gmsh.mshcad -o ${x}-${n}.msh 2>&1 |tee ${x}.log
if test $? -ne 0 || test `grep -i error ${x}.log | wc -l` -ne 0; then
  grep -i error ${x}.log 
  echo "gmsh failed"
  exit 1
fi
../sbin/msh2geo < ${x}-${n}.msh | tee ${x}-${n}-v1.geo | ../bin/geo -upgrade -geo - > ${x}-${n}-v2.geo
if test $? -ne 0; then
  echo "msh2geo failed"
  exit 1
fi
../bin/geo ${x}-${n}-v2.geo
