///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//  geo & domain : specifications
// 
// author: Pierre.Saramito@imag.fr
//
// date: 2 fevrier 2011
//
#include "rheolef/smart_pointer.h"
using namespace std;
using namespace rheolef;
struct point {};
struct geo_element {};

/*
  on introduit tout d'abord la sous-classe masque
  qui va permettre de definir les domaines :
  un domaine sera un masque (vue partielle) d'un mailage via
  un ensemble reduit d'elements definis par une renumerotation "num" :
*/
struct domain_indirect_rep {
  vector<size_t>	     _num;
  vector<bool>		     _orient;
  size_t		     _dim;
  string		     _name;
};
struct domain_indirect : smart_pointer<domain_indirect_rep> {};

/*
  geo: niveau abstraction :
  on souhaite que geo et domain s'utilisent de la meme facon,
  du coup on declare une classe abstraite, qui efinit l'interface 
  commun aux deux classes.
  le point de depart est le constructeur de space :

  	space (const geo& omega, "P1")
        : space_rep (omega.data(), "P1") {}

  	space_rep (const geo_abs_rep& omega, "P1") 
        { code }

  ou geo_abs_rep est une classe de base virtuelle pure 
  qui contient l'interface commun, minimum necessaire a la
  construction de la classe space_rep
*/
struct geo_abs_rep {
  geo_abs_rep () {}
  virtual ~geo_abs_rep () {}
// interface "geo" abstrait :
  virtual string name () const = 0;
  virtual const point& vertex (size_t iv) const = 0;
  virtual const geo_element& subgeo (size_t d,size_t ie) const = 0;
  virtual domain_indirect operator[] (string dom_name) const = 0; 
};
/*
  geo : conteneur
  la lecture d'un fichier .geo construit une classe derivee :
  qui est massive (coords, connectivite) :
*/
struct geo_rep : geo_abs_rep {
// data:
  size_t		     _dim;
  string		     _name;
  vector<geo_element>        _subgeo[4];
  vector<point>              _vertex;
  vector<domain_indirect>    _domains;
// allocator:
  geo_rep () : _dim(0), _name(), _subgeo(), _vertex(), _domains() {}
  geo_rep (string file_name) : _dim(0), _name(), _subgeo(), _vertex(), _domains() { /* code a developper */ }
  geo_rep (const struct geo_domain_rep& dom); // defini + loin
  ~geo_rep () {}
// interface "geo" concret :
  virtual string name () const { return _name; }
  const point& vertex (size_t iv) const { return _vertex[iv]; }
  const geo_element& subgeo (size_t d,size_t ie) const { return _subgeo[d][ie]; }
  domain_indirect operator[] (string dom_name) const;
};
domain_indirect
geo_rep::operator[] (string dom_name) const
{
   for (size_t i = 0; i < _domains.size(); i++) 
    if (_domains[i].data()._name == dom_name) return _domains[i];
   return domain_indirect(); // erreur
}

/*
  geo : couche smart_pointer
   pour gerer la memoire en souplesse
*/

struct geo : smart_pointer<geo_abs_rep> {
  typedef smart_pointer<geo_abs_rep> base;
// allocator:
  geo () : smart_pointer<geo_abs_rep>(new geo_rep) {}
  geo (string filename) : smart_pointer<geo_abs_rep>(new geo_rep(filename)) {}
  geo (const domain_indirect& indirect); // defini + loin
// interface "geo" concret :
  const point& vertex (size_t iv) const { return data().vertex(iv); }
  const geo_element& subgeo (size_t d,size_t ie) const { return data().subgeo(d,ie);}
  geo operator[] (string dom_name) const { return geo(data()[dom_name]); }
};
/*
  domain:
  la classe "domain_indirect" ne contient que les numeros des elements :
  pour acceder aux elements et aux coords des sommets,
  il faut disposer de la classe "geo_abs_rep" associee, appellee alors
  "background geo" (concretement : geo_rep ou geo_domain_rep) :
*/
struct geo_domain_rep : geo_abs_rep {
// data:
  domain_indirect  _indirect;
  geo          _bg; // background geo
// allocator:
  geo_domain_rep (const domain_indirect& indirect, const geo& bg) :
	_indirect(indirect), _bg(bg) {}
// interface "geo" concret :
  virtual string name () const { return _indirect.data()._name; }
  const point& vertex (size_t iv) const { return _bg.vertex(iv); }
  const geo_element& subgeo (size_t d,size_t ie) const {
	if (d == _indirect.data()._dim) return _bg.subgeo(d,_indirect.data()._num[ie]);
	else                        return _bg.subgeo(d,ie); }
  domain_indirect operator[] (string dom_name) const; // impl + loin
};
// constructeur d'un domaine avec l'interface "geo" :
geo::geo (const domain_indirect& indirect)
: smart_pointer<geo_abs_rep>(new geo_domain_rep(indirect, *this)) {}

/*
  point delicat :
  les domaines conduisent a deux types de "geo" :
   1) le plus simple est 
	geo gamma = omega["gamma"];
      qui est represente' par un geo_domain_rep : la numerotation
      des sommets suit celle du maillage "background".
      Ce type de "geo" peut construire des espace pour assembler
      des matrices de masse pour la condition de robin, par ex.
      Ce cas est traite' par le constructeur de geo_domain_rep precedent :

  	geo_domain_rep (const domain_indirect& indirect, const geo& bg);
 
   2) lorsqu'on veut une matrice de masse avec une numerotation
      compactee, ou` seuls les sommets intervenant dans le domaine
      interviennent, pour que par exemple la matrice de masse
      sur une surface soit inversible, il faut renumeroter
      les sommets, et donc egalement toutes les faces et aretes.
      Dans ce cas, le domain_tst construit precedement 
      est convertis en geo_tst :

        geo_rep (const geo_domain_rep& dom);

      c'est l'implementation qui suit :
*/
geo_rep::geo_rep (const geo_domain_rep& dom)
: _dim  (dom._indirect.data()._dim),
  _name (dom._indirect.data()._name + "@" + dom._bg.data().name()),
  _subgeo(),
  _vertex(),
  _domains()
{
  // a faire ici:
  // 1) _vertex : copie paresseuse de la table des sommets
  // 2) _subgeo : renum compactee de ts les elements, y compris sommets
  // 3) _domains : intersection des domaines de _bg avec le dom._indirect :
  //     on ne garde que les
}
/*
  Enfin, un domaine d'un maillage peut avoir des sous-domaines,
  par exemple les bords du bord. Il s'agit de faire l'intersection
  du domaine designe' par _indirect avec celui donne' par dom_name
  et qui designe un domain dans _bg, le maillage de fond.
  Le resultat est un nouveau masque de _bg.
*/
domain_indirect 
geo_domain_rep::operator[] (string dom_name) const { 
  // ici, extraction de sous-sous-domaines.
  return domain_indirect();
}

int main() {
}
