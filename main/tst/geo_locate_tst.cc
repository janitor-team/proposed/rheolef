///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//  given x, search K in mesh such that x in K
//
// usage: prog x0 x1 x2 < mesh.geo
#include "rheolef/geo.h"
#include "rheolef/geo_locate.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  geo_basic<Float> omega;
  din >> omega;
  size_t d = omega.dimension();

  point_basic<Float> x;
  x[0] = (d >= 1 && argc > 1) ? atof(argv[1]) : 0.51;
  x[1] = (d >= 2 && argc > 2) ? atof(argv[2]) : 0.52;
  x[2] = (d >= 3 && argc > 3) ? atof(argv[3]) : 0.53;

  size_t dis_ie = omega.dis_locate (x); 
  dout << dis_ie << endl;
  return (dis_ie != numeric_limits<size_t>::max()) ? 0 : 1;
}
