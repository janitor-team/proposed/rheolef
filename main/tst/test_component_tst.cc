///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// tests for dirichlet_hdg.cc
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float tol = 1e3*numeric_limits<Float>::epsilon();
int test_vector (geo omega, string approx) {
  space Xh (omega, approx, "vector");
  trial u(Xh); test  v(Xh);
  form a0 = integrate (u[0]*v[0] + u[1]*v[1]);
  form a1 = integrate (dot(u,v));
  Float err = (a0 - a1).uu().max_abs();
  derr << "err_vector = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_subcomp (geo omega, string approx) {
  space Xh (omega, approx, "vector");
  space Yh = Xh*Xh;
  trial u(Yh); test  v(Yh);
  form a0 = integrate (u[1][0]*v[1][0] + u[1][1]*v[1][1]);
  form a1 = integrate (dot(u[1],v[1]));
  Float err = (a0 - a1).uu().max_abs();
  derr << "err_subcomp = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_mixt (geo omega, string approx) {
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh;
  trial x(Yh), sigma(Th), u(Xh);
  test  y(Yh), tau(Th),   v(Xh);
  form A0 = integrate (dot(x[0],y[0]) + x[1]*div_h(y[0]) + y[1]*div_h(x[0]));
  form a  = integrate (dot(sigma,tau));
  form b  = integrate (v*div_h(sigma));
  form A1 = {{a, trans(b)},
             {b,   0     }};
  Float err = (A0 - A1).uu().max_abs();
  derr << "err_mixt = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_mixt2 (geo omega, string approx) {
  Float alpha = 1;
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh(omega["internal_sides"]);
  trial x(Yh), lambda(Mh), sigma(Th), u(Xh);
  test  y(Yh), mu(Mh),     tau(Th),   v(Xh);
  form A0 = integrate (dot(x[0],y[0]) + x[1]*div_h(y[0]) + y[1]*div_h(x[0])
                       - 2*alpha*on_local_sides(x[1]*y[1]));
  form a  = integrate (dot(sigma,tau));
  form b  = integrate (v*div_h(sigma));
  form c  = integrate ("internal_sides", 2*average(u)*average(v) + 0.5*jump(u)*jump(v))
          + integrate ("boundary", u*v);
  form A1 = {{a,   trans(b)},
             {b, -2*alpha*c}};
  Float err = (A0 - A1).uu().max_abs();
  derr << "err_mixt2 = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_mixt3 (geo omega, string approx) {
  Float alpha = -7*sqrt(2);
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh (omega["internal_sides"], approx);
  trial x(Yh), lambda(Mh), sigma(Th), u(Xh);
  test  y(Yh), mu(Mh),     tau(Th),   v(Xh);
  form B0 = integrate ("internal_sides",
	     (-dot(jump(x[0]),normal()) + 2*alpha*average(x[1]))*mu);
  form b0 = integrate ("internal_sides", -dot(jump(sigma),normal())*mu);
  form b1 = integrate ("internal_sides", 2*alpha*average(u)*mu);
  form B1 = {b0, b1};

#ifdef TO_CLEAN
  odiststream out ("aa","m",io::nogz);
  out << matlab
      << setbasename("B0") << B0.uu()
      << setbasename("b0") << b0.uu()
      << setbasename("b1") << b1.uu()
      << "n0=size(b0)(2);" << endl
      << "n1=size(b1)(2);" << endl
      << "err0=norm(B0(:,   1:n0   )-b0)" << endl
      << "err1=norm(B0(:,n0+1:n0+n1)-b1)" << endl
      << "BB0=full(B0)" << endl
      << "BB1=full([b0,b1])" << endl
      << "err=norm(BB0-BB1)" << endl
      ;
#endif // TO_CLEAN

  Float err = (B0 - B1).uu().max_abs();
  derr << "err_mixt3 = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_mixt3a (geo omega, string approx) {
  Float alpha = 1;
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh (omega["internal_sides"], approx);
  trial x(Yh), lambda(Mh), sigma(Th), u(Xh);
  test  y(Yh), mu(Mh),     tau(Th),   v(Xh);
  form B0 = integrate ("internal_sides",
	     (-dot(jump(x[0]),normal()))*mu);
  form b0 = integrate ("internal_sides", -dot(jump(sigma),normal())*mu);
  form b1 (Xh,Mh);
  form B1 = {b0, b1};
#ifdef TO_CLEAN
  odiststream out ("a0","m",io::nogz);
  out << matlab
      << setbasename("B0") << B0.uu()
      << setbasename("b0") << b0.uu()
      << setbasename("b1") << b1.uu()
      << "n0=size(b0)(2);" << endl
      << "n1=size(b1)(2);" << endl
      << "err0=norm(B0(:,   1:n0   )-b0)" << endl
      << "err1=norm(B0(:,n0+1:n0+n1)-b1)" << endl
      << "B1=[b0,b1];" << endl
      << "err=norm(B0-B1)" << endl
      << "BB0=full(B0(:,   1:n0   ))" << endl
      << "bb0=full(b0)" << endl
      << "BB1=full(B0(:,n0+1:n0+n1))" << endl
      << "bb1=full(b1)" << endl
      ;
#endif // TO_CLEAN

  Float err = (B0 - B1).uu().max_abs();
  derr << "err_mixt3a = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_mixt3b (geo omega, string approx) {
  Float alpha = 1;
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh (omega["internal_sides"], approx);
  trial x(Yh), lambda(Mh), sigma(Th), u(Xh);
  test  y(Yh), mu(Mh),     tau(Th),   v(Xh);
  form B0 = integrate ("internal_sides",
	     (2*alpha*average(x[1]))*mu);
  form b0 (Th,Mh);
  form b1 = integrate ("internal_sides", 2*alpha*average(u)*mu);
  form B1 = {b0, b1};
#ifdef TO_CLEAN
  odiststream out ("a1","m",io::nogz);
  out << matlab
      << setbasename("B0") << B0.uu()
      << setbasename("b0") << b0.uu()
      << setbasename("b1") << b1.uu()
      << "n0=size(b0)(2);" << endl
      << "n1=size(b1)(2);" << endl
      << "err0=norm(B0(:,   1:n0   )-b0)" << endl
      << "err1=norm(B0(:,n0+1:n0+n1)-b1)" << endl
      << "B1=[b0,b1];" << endl
      << "err=norm(B0-B1)" << endl
      << "BB0=full(B0(:,   1:n0   ))" << endl
      << "bb0=full(b0)" << endl
      << "BB1=full(B0(:,n0+1:n0+n1))" << endl
      << "bb1=full(b1)" << endl
      ;
#endif // TO_CLEAN

  Float err = (B0 - B1).uu().max_abs();
  derr << "err_mixt3b = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_dg_bdr (geo omega, string approx) {
  space Th (omega, approx, "vector"),
        Xh (omega, approx),
        Yh = Th*Xh,
        Mh (omega["sides"], approx);
  Mh.block("boundary");
  trial x(Yh), lambda(Mh);
  test  y(Yh), mu(Mh);
  field lh = integrate("boundary", -dot(x[0],normal()) + x[1]);
  Float p  = integrate(omega["boundary"]);
  Float err = dual(lh,1) - p;
  derr << "err_dg_bdr = " << err << endl;
  return (err <= tol) ? 0: 1; 
}
int test_mixt4 (geo omega, string approx) {
  // from dirichlet_hdg2.cc
  space Th (omega, approx, "vector"),
        Xh (omega, "P0"),
        Mh (omega["sides"], approx),
        Yh = Xh*Mh;
  trial sigma(Th); test y(Yh);
  // DVT_INTEGRATE_HETEROGENEOUS_MAP_DIMENSION: allows one to suppress "omega" here:
  form b = integrate (omega, 0*div_h(sigma)*y[0])
         - integrate ("internal_sides", dot(jump(sigma),normal())*y[1]);
  test v(Xh), mu (Mh);
  form b0 = integrate (omega, 0*div_h(sigma)*v);
  form b1 = - integrate ("internal_sides", dot(jump(sigma),normal())*mu);
  // DVT_FORM_CONCAT_VERTICAL : comment concatener 2 formes verticalement ?
  form ct = {trans(b0), trans(b1)};
  form c  = trans(ct);
  form b_err = b-c;
  Float err_b = b_err.uu().max_abs();
  // BUG_SPACE_PRODUCT_HETEROGENEOUS_MAP_DIMENSION : not zero !
  derr << "err_b = " << err_b << endl;
#ifdef TO_CLEAN
  odiststream out ("b","m",io::nogz);
  out << matlab
      << setbasename("b0") << b0.uu()
      << setbasename("b1") << b1.uu()
      << setbasename("c") << c.uu()
      << setbasename("b") << b.uu()
      << "bb0=full(b0)" << endl
      << "bb1=full(b1)" << endl
      << "cc=full(c)" << endl
      << "bb=full(b)" << endl
      << "err=norm(b-c)" << endl
      ;
#endif // TO_CLEAN
  return (err_b <= tol) ? 0: 1; 
}
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1d";
  int status = 0;
#ifdef TODO
  // vector-valued are no more hierachical spaces
  status += test_vector (omega, approx);
  status += test_subcomp(omega, approx);
#endif // TODO
  status += test_mixt   (omega, approx);
  status += test_mixt2  (omega, approx);
  status += test_mixt3  (omega, approx);
  status += test_mixt3a (omega, approx);
  status += test_mixt3b (omega, approx);
  status += test_dg_bdr (omega, approx);
  status += test_mixt4  (omega, approx);
  return status;
}
