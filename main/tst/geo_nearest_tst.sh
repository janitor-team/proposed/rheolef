#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
SRCDIR=${SRCDIR-"."}
GEODIR=$SRCDIR
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

# NOTE: no use of mpirun_loop, as dis_locate(x) only available when NPROC==1

status=0

run "../sbin/mkgeo_grid_2d -t 10 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-2d.geo 2>/dev/null"
run "../sbin/mkgeo_grid_2d -q 10 -v4 2>/dev/null | ../bin/geo -upgrade - > mesh-2d-q.geo 2>/dev/null"

for geo in mesh-2d.geo mesh-2d-q.geo; do
  run "./geo_nearest_tst 1.2 1.1 1.0 1.0 < $geo 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
  run "./geo_nearest_tst 1.2 0.9 1.0 0.9 < $geo 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
  run "./geo_nearest_tst 0.9 1.1 0.9 1.0 < $geo 2>/dev/null >/dev/null"
  if test $? -ne 0; then status=1; fi
done

exit $status
