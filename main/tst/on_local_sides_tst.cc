///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "jump_dg_tst.h"
// ----------------------------------------------------------------------------
size_t test_form (geo omega, string approx, Float tol) {
// ----------------------------------------------------------------------------
  space Xh (omega, approx);
  trial u(Xh); test  v(Xh);
  form a0 = integrate (on_local_sides(u*v));
  form a1 = integrate ("internal_sides", 2*average(u)*average(v) + 0.5*jump(u)*jump(v))
          + integrate ("boundary", u*v);
  Float form_err = (a0 - a1).uu().max_abs();
  derr << "form_err = " << form_err << endl;
  return (form_err <= tol) ? 0: 1; 
}
size_t test_field (geo omega, string approx, Float tol) {
  space Xh (omega, approx);
  test v(Xh);
  field u0 = integrate (on_local_sides(v));
  field u1 = integrate ("internal_sides", 2*average(v))
           + integrate ("boundary", v);
  Float field_err = field(u0 - u1).u().max_abs();
  derr << "field_err = " << field_err << endl;
  return (field_err <= tol) ? 0: 1; 
}
// ----------------------------------------------------------------------------
size_t test_meas_sides (geo omega, string approx, Float tol) {
// ----------------------------------------------------------------------------
  // TODO: add a true check on values
  space Xh (omega, approx);
  space Mh (omega["sides"], approx);
  test v (Xh);
  field lambda_h (Mh,1.);
  field l1h = integrate(on_local_sides(lambda_h*v));
  field l2h = integrate("boundary", v);
  field one(Xh,1.);
  Float meas_sides2   = dual (l1h, one); // internal sides are counted twice
  Float meas_boundary = dual (l2h, one);
  trial lambda (Mh); test mu (Mh);
  form m = integrate(lambda*mu);
  Float meas_sides = m (lambda_h, lambda_h); // internal sides are counted one time
  Float meas_sides_err = (meas_sides2 - meas_boundary) - 2*(meas_sides - meas_boundary);
  derr << "meas_boundary  = " << meas_boundary << endl
       << "meas_sides2    = " << meas_sides2 << endl
       << "meas_sides     = " << meas_sides << endl
       << "meas_sides_err = " << meas_sides_err << endl;
  return (meas_sides_err <= tol) ? 0: 1; 
}
// ----------------------------------------------------------------------------
size_t test_interface (geo omega, string approx, Float eps, interface_side check) {
// ----------------------------------------------------------------------------
  if (!omega.have_domain_indirect("interface")) return 0;
  size_t d = omega.dimension();
  space Mh (omega["sides"], approx);
  size_t k = Mh.degree();
  space Xh (omega, approx),
        Xh_west (omega["west"], approx),
        Xh_east (omega["east"], approx);
  quadrature_option qopt;
  qopt.set_family(quadrature_option::gauss);
  qopt.set_order(2*k+1);
  field lambda_h (Mh , 0);
  lambda_h["interface"] = 1;
  field uh (Xh, 0.);
  if (check == west_side || check == both_side) {
    uh["west"] = lazy_interpolate (Xh_west, p_west(d,k));
  }
  if (check == east_side || check == both_side) {
    uh["east"] = lazy_interpolate (Xh_east, p_east(d,k));
  }
  trial lambda(Mh); test v(Xh);
  form  m   = integrate (omega, on_local_sides(lambda*v), qopt);
  field lh  = integrate (omega, on_local_sides(lambda*uh), qopt);
  form  m2  = integrate (omega["internal_sides"], 2*lambda*average(v), qopt);
  field lh2 = integrate (omega["internal_sides"], 2*lambda*average(uh), qopt);
  Float value_interface_m        = m (lambda_h, uh);
  Float value_interface_m2       = m2(lambda_h, uh);
  Float value_interface_l        = dual (lh,  lambda_h);
  Float value_interface_l2       = dual (lh2, lambda_h);
  Float value_interface_expected = 0;
  if (check == west_side || check == both_side) {
    value_interface_expected += p_west::integrate_interface(d,k);
  }
  if (check == east_side || check == both_side) {
    value_interface_expected += p_east::integrate_interface(d,k);
  }
  Float error_interface          = fabs(value_interface_m  - value_interface_expected)
                                 + fabs(value_interface_m2 - value_interface_expected)
                                 + fabs(value_interface_l  - value_interface_expected)
                                 + fabs(value_interface_l2 - value_interface_expected);
  derr << setprecision(16)
       << "interface side(s) check  = " << side_name[check] << endl
       << "space                    = " << Mh.name() << endl
       << "value_interface_m        = " << value_interface_m << endl
       << "value_interface_l        = " << value_interface_l << endl
       << "value_interface_m2       = " << value_interface_m2 << endl
       << "value_interface_l2       = " << value_interface_l2 << endl
       << "value_interface_expected = " << value_interface_expected << endl
       << "error_interface          = " << error_interface << endl
       << endl;
  return (error_interface < eps)  ? 0 : 1;
}
// ----------------------------------------------------------------------------
int main(int argc, char**argv) {
// ----------------------------------------------------------------------------
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1d";
  Float eps  = 1e3*numeric_limits<Float>::epsilon();
  Float eps2 = sqrt(numeric_limits<Float>::epsilon());
  size_t status = 0;
  status += test_form       (omega, approx, eps);
  status += test_field      (omega, approx, eps);
  status += test_meas_sides (omega, approx, eps2);
  status += test_interface  (omega, approx, eps, west_side);
  status += test_interface  (omega, approx, eps, east_side);
  status += test_interface  (omega, approx, eps, both_side);
  return status;
}
