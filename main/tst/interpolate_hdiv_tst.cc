///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// 
// check the O(h^{k+1}) convergence of RTkd in L2 and Hdiv norms
//  1) for a polynomial function, on a single-element mesh
//  2) for a non-polynomial function
//
// author: Pierre.Saramito@imag.fr
//
// date: 25 april 2019
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
// -----------------------------------------------------------------------------
// 1) polynomial function
// -----------------------------------------------------------------------------
#include "interpolate_RTk_polynom.icc"
int test_polynomial (geo omega, space Vh, Float tol) {
  size_t d = omega.dimension();
  size_t k = Vh.degree() - 1;
  if (d != 2 || k >= 2) return 0;
  size_t n_tri = omega.sizes().ownership_by_variant [reference_element::t].dis_size();
  size_t n_qua = omega.sizes().ownership_by_variant [reference_element::q].dis_size();
  if (n_tri != 0 && n_qua != 0) return 0;
  char hat_K_name = (n_tri != 0) ? 't' : 'q';
  size_t ndx = psi::n_index (hat_K_name,k);
  derr << "approx     " << Vh.name() << endl
       << "omega      " << omega.name() << endl
       << "ndx        " << ndx << endl;
  int status = 0;
  for (size_t idx = 0; idx < ndx; ++idx) {
    field pi_h_psi = lazy_interpolate(Vh, psi(hat_K_name,idx));
#ifdef TO_CLEAN
    odiststream out (omega.name() + "-" + std::to_string(idx), "field", io::nogz);
    out << pi_h_psi;
#endif // TO_CLEAN
    integrate_option iopt;
    iopt.set_order(2*(k+1)+2);
    Float err_l2     = sqrt(integrate (omega, norm2(  psi(hat_K_name,idx) -       pi_h_psi),  iopt));
    Float err_div_l2 = sqrt(integrate (omega, sqr(div_psi(hat_K_name,idx) - div_h(pi_h_psi)), iopt));
    derr << "idx        " << idx << endl
         << "err_l2     " << err_l2 << endl
         << "err_div_l2 " << err_div_l2 << endl;
    status |= (err_l2 <= tol && err_div_l2 <= tol) ? 0 : 1;
  }
  return status;
}
// -----------------------------------------------------------------------------
// 2) non-polynomial function
// -----------------------------------------------------------------------------
#ifdef TO_CLEAN // moved in "interpolate_RTk_polynom.icc"
struct u_exact {
  point operator() (const point& x) const {
    switch (d) {
      case 2:  return point(cos(pi*(x[0]+2*x[1])),
                            sin(pi*(x[0]-2*x[1])));
      default: return point(cos(pi*(x[0]+2*x[1]+x[2])),
                            sin(pi*(x[0]-2*x[1]-x[2])),
                            sin(pi*(x[0]+2*x[1]-x[2])));
    }
  }
  Float div (const point& x) const {
    switch (d) {
      case 2:  return -pi*(    sin(pi*( x[0]+2*x[1]))
                           + 2*cos(pi*(-x[0]+2*x[1])));
      default: return -pi*(    sin(pi*( x[0]+2*x[1]+x[2]))
                           + 2*cos(pi*(-x[0]+2*x[1]+x[2]))
                           +   cos(pi*(-x[0]-2*x[1]+x[2])));
    }
  }
  u_exact(size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; Float pi;
};
struct div_u {
  Float operator() (const point& x) const { return _u.div(x); }
  div_u(size_t d) : _u(d) {}
  u_exact _u;
};
#endif // TO_CLEAN
int test_non_polynomial (geo omega, space Vh, Float np_err_l2_valid, Float np_err_div_l2_valid) {
  size_t d = omega.dimension();
  size_t k = Vh.degree() - 1;
  // -------------------------
  // 1. Lagrange interpolation
  // -------------------------
  field pi_h_u     = lazy_interpolate(Vh, u_exact(d));
#undef DUMP
#ifdef DUMP
  odiststream out (omega.name() + "-np", "field", io::nogz);
  out << pi_h_u;
#endif // DUMP
  integrate_option iopt;
  iopt.set_order(4*(k+4)); // TODO: adjust more optimaly when RTk cvgce will be fixed
  Float np_err_l2     = sqrt(integrate (omega, norm2(u_exact(d) - pi_h_u), iopt));
  Float np_err_div_l2 = sqrt(integrate (omega, sqr(div_u(d) - div_h(pi_h_u)), iopt));
  // -------------------------
  // 2. L2 projection
  // -------------------------
  trial u (Vh); test v (Vh);
  integrate_option inv_opt; 
  inv_opt.invert = true;
  form inv_mu = integrate (dot(u,v), inv_opt);
  field lh = integrate (dot(u_exact(d),v));
  field prl2_u = inv_mu*lh;
  Float np_err_prl2_l2     = sqrt(integrate (omega, norm2(u_exact(d) - prl2_u), iopt));
  Float np_err_prl2_div_l2 = sqrt(integrate (omega, sqr(div_u(d) - div_h(prl2_u)), iopt));
  // -------------------------
  // Linf norm of: pl2(div(u)) - div_h(pi_h(u)) :
  // -------------------------
  space Qh (omega, "P"+std::to_string(k)+"d");
  trial p (Qh); test q (Qh);
  form inv_mp = integrate (p*q, inv_opt);
  field prl2_div_u   = inv_mp*integrate(div_u(d)*q);
  field div_h_pi_h_u = lazy_interpolate(Qh, div_h(pi_h_u));
  field div_pi_h_sw = prl2_div_u - div_h_pi_h_u;
  Float np_err_div_pi_h_sw_linf = div_pi_h_sw.max_abs();
  field div_h_prl2_u = inv_mp*integrate(div_h(prl2_u)*q);
  field div_prl2_sw = prl2_div_u - div_h_prl2_u;
  Float np_err_div_prl2_sw_linf = div_prl2_sw.max_abs();
#ifdef DUMP
  odiststream prl2_div_u_out (omega.name() + "-np-prl2-div-u", "field", io::nogz);
  prl2_div_u_out << prl2_div_u;
  odiststream div_h_pi_h_u_out (omega.name() + "-np-div-h-pi-h-u", "field", io::nogz);
  div_h_pi_h_u_out << div_h_pi_h_u;
  odiststream div_pi_h_sw_out (omega.name() + "-np-div-pi-h-sw", "field", io::nogz);
  div_pi_h_sw_out << div_pi_h_sw;
#endif // DUMP
  // -------------------------
  // P(k+1)^d re-interpolation
  // -------------------------
  space Vh1 (omega, "P"+std::to_string(k+1)+"d", "vector");
  space Qh1 (omega, "P"+std::to_string(k+1)+"d");
  field pi_Vh1_pi_h_u     = lazy_interpolate (Vh1, pi_h_u);
  field pi_Qh1_div_pi_h_u = lazy_interpolate (Qh1, div_h(pi_h_u));
#ifndef DUMP
  odiststream pi_h_u1_out (omega.name() + "-np-pi-h-u1", "field", io::nogz);
  pi_h_u1_out << pi_Vh1_pi_h_u;
#endif // DUMP
  field pi_Vh1_u     = lazy_interpolate (Vh1, u_exact(d));
  field pi_Qh1_div_u = lazy_interpolate (Qh1, div_u(d));
  Float np_err_linf     = field(pi_Vh1_pi_h_u - pi_Vh1_u).max_abs();
  Float np_err_div_linf = field(pi_Qh1_div_pi_h_u - pi_Qh1_div_u).max_abs();
  // -------------------------
  // output
  // -------------------------
  derr << "approx                  " << Vh.name() << endl
       << "omega                   " << omega.name() << endl
       << "np_err_l2               " << np_err_l2 << endl
       << "np_err_div_l2           " << np_err_div_l2 << endl
       << "np_err_linf             " << np_err_linf << endl
       << "np_err_div_linf         " << np_err_div_linf << endl
       << "np_err_prl2_l2          " << np_err_prl2_l2 << endl
       << "np_err_prl2_div_l2      " << np_err_prl2_div_l2 << endl
       << "np_err_div_pi_h_sw_linf " << np_err_div_pi_h_sw_linf << endl
       << "np_err_div_prl2_sw_linf " << np_err_div_prl2_sw_linf << endl;

  // TODO: np_err_div_eh_linf should be zero up to machine precision
  return (np_err_l2 <= np_err_l2_valid && np_err_div_l2 <= np_err_div_l2_valid) ? 0 : 1;
}
// -----------------------------------------------------------------------------
int main(int argc, char**argv) {
// -----------------------------------------------------------------------------
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  space Vh (omega, argv[2]);
  Float err_l2_valid     = (argc > 3) ? atof(argv[3]) : 0;
  Float err_div_l2_valid = (argc > 4) ? atof(argv[4]) : 0;
  bool np                = (argc > 5) &&     argv[5] == string("-np");
  Float tol = sqrt(std::numeric_limits<Float>::epsilon());
  int status = 0;
  if (!np) {
    status |= test_polynomial   (omega, Vh, tol);
  }
  status |= test_non_polynomial (omega, Vh, err_l2_valid, err_div_l2_valid);
  return status;
}
