#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/main/tst"}
DATADIR=${SRCDIR}
BINDIR="../bin"
SBINDIR="../sbin"
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

epsilon="1e-10"
qorder="6"
# =========================================================
# 1) surface mesh
# =========================================================
# -------------------------------------------------------
# 1.1) div_s(n) = curvature = d-1 on a circle or sphere
# -------------------------------------------------------
# geo			err_P1   err_P2   err_P3
L="
circle_s-40-fix		$epsilon $epsilon $epsilon
sphere_s-10-fix		$epsilon $epsilon $epsilon
sphere_s_q-10-fix	$epsilon $epsilon $epsilon
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_div_s_tst $DATADIR/${geo}-${Pk} -normal $Pk $qorder $err >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# ---------------------------------------------------------
# 1.2) compare div_s(u) and div_s(uh) for a polynomial u
# ---------------------------------------------------------
# geo			err_P1 err_P2 err_P3
L="
sphere_s-10-fix		0.2    0.03   0.003
sphere_s_q-10-fix	0.3    0.03   0.004
"
while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1 P2 P3; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_div_s_tst $DATADIR/${geo}-${Pk} -polynom $Pk $qorder $err >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done
# =========================================================
# 2) banded level set method: volume mesh, for a sphere
# =========================================================
qorder="6"
run "$SBINDIR/mkgeo_grid_2d -v4 -t 20 -a -2 -b 2 -c -2 -d 2 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-20.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_2d -v4 -t 41 -a -2 -b 2 -c -2 -d 2 2>/dev/null | $BINDIR/geo -upgrade - > mesh-2d-41.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_3d -v4 -T 20 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2  2>/dev/null| $BINDIR/geo -upgrade - > mesh-3d-20.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_3d -v4 -T 21 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2  2>/dev/null| $BINDIR/geo -upgrade - > mesh-3d-21.geo 2>/dev/null"

# -------------------------------------------------------
# 1.1) div_s(n) = curvature = d-1 on a circle or sphere
# -------------------------------------------------------
# geo			err_P1 
L="
mesh-2d-41		0.08
mesh-3d-20		0.53  
mesh-3d-21		0.6   
"
if test x"${QD_EXT}" == x""; then
  # add 20x20 mesh that need some additional constraints with Lagrange multiplier:
  # the linear system is symmetric but undefinite
  # and, when using Float=float128, the direct solver ldlt is unable to solve it
L="
mesh-2d-20		0.1
$L
"
else
  echo "      mesh-2d-20: not yet (skiped when QD lib is active, since undefinite matrix solver is missing)"
fi

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for Pk in P1; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./form_div_s_tst ${geo} -normal $Pk $qorder $err >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

run "rm -f mesh-2d-20.geo mesh-2d-41.geo mesh-3d-20.geo mesh-3d-21.geo"

exit $status
