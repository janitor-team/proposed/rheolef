#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
e=${1-"t"}
kmax=3;
L="1 2 4 8 16 32 64 128 256 512 1024 2048 4096"
k=0
while test $k -le $kmax; do
  echo "# RT${k}d $e"
  echo "# n err_linf err_div_linf"
  for n in $L; do
    h=`echo $n | gawk '{printf("%20.16g",1.0/$1)}'`
    case $e in
      t) cat > tmp.geo << EOF_T
#!geo
mesh
4
header
 dimension 2
 nodes  3
 triangles      1
 edges  3
end header
0  0
$h 0
0  $h
t       0 1 2
e       0 1
e       1 2
e       2 0
EOF_T
         ;;
      q) cat > tmp.geo << EOF_H
#!geo
mesh
4
header
 dimension 2
 nodes  4
 quadrangles    1
 edges  4
end header
0  0
$h 0
0  $h
$h $h
q       0 1 3 2
e       0 1
e       1 3
e       3 2
e       2 0
EOF_H
      ;;
    esac
    command="./interpolate_hdiv_tst tmp.geo RT${k}d >/dev/null 2>tmp.txt"
    #echo "! $command" 1>&2
    eval $command
    err_linf=`grep np_err_linf tmp.txt | gawk '{print $2}'`
    err_div_linf=`grep np_err_div_linf tmp.txt | gawk '{print $2}'`
    echo "$n $err_linf $err_div_linf"
  done
  echo; echo
  k=`expr $k + 1`
done
