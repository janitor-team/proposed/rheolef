///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// solver_abtb::operator= was buggy (core dump)
//
#include "rheolef.h"
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) { 
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, "P2", "vector");
  Xh.block ("boundary");
  space Qh (omega, "P1");
  trial u(Xh), p(Qh);
  test  v(Xh), q(Qh);
  form a  = integrate (2*ddot(D(u),D(v))),
       b  = integrate (q*div(u)),
       mp = integrate (p*q);
#undef  BUG_SOLVER_COPY
#ifndef BUG_SOLVER_COPY
  solver_abtb stokes (a.uu(), b.uu(), mp.uu());
#else // BUG_SOLVER_COPY
  solver_abtb stokes;
  solver_option opt, sub_opt;
  opt.iterative     = true;
  sub_opt.iterative = false;
  stokes = solver_abtb (a.uu(), b.uu(), mp.uu(), opt, sub_opt);
#endif // BUG_SOLVER_COPY
  field uh (Xh,0), ph(Qh, 0);
  uh[0]["top"] = 1; // driven cavity
  stokes.solve (- a.ub()*uh.b(), -b.ub()*uh.b(),
                uh.set_u(),      ph.set_u());
  dout << catchmark("u") << uh
       << catchmark("p") << ph;
}
