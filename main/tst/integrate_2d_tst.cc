///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;

// utility
struct my_norm2_fun : unary_function<point,Float> {
  Float operator() (const point& v) const { return dot(v,v); }
};
// --------------------------------------------------------------------
// vector test
// --------------------------------------------------------------------
struct vf2 {
  point operator() (const point& x) const { return point(x[0],x[1]); }
};
// --------------------------------------------------------------------
int main(int argc, char**argv) {
    environment rheolef(argc, argv);
    geo omega(argv[1]);
    cout << setprecision(15);

    quadrature_option qopt;
    qopt.set_family(quadrature_option::gauss);
    qopt.set_order(2);

    // -------------------------------------------
    // short notation
    // -------------------------------------------
    Float I_vf2 = integrate (omega, compose(my_norm2_fun(), vf2()), qopt);
    cout << "I_vf2 = " << I_vf2 << endl;
    check_macro ((I_vf2 - 2./3.) < 1e-7, "unexpected");
}
