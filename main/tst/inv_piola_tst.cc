///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check inverse piola transformation for non-linear elements (quadrangles, etc)
//
#include "rheolef.h"
#include "rheolef/inv_piola.h"
using namespace rheolef;
using namespace std;
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  point hat_x;
  Float  eps      = std::numeric_limits<Float>::epsilon();
  geo omega (argv[1]);
         hat_x[0] = (argc > 2) ? atof(argv[2]) : 0.9;
         hat_x[1] = (argc > 3) ? atof(argv[3]) : 0.9;
  Float  tol      = (argc > 4) ? atof(argv[4]) : 1e5*eps;
  size_t max_iter = (argc > 5) ? atoi(argv[5]) : 500;
  inv_piola<Float> F;
  const geo& omega_const = omega;
  const geo_element& K = omega_const[0];
  std::vector<size_t> dis_inod;
  omega.dis_inod (K, dis_inod);
  F.reset (omega, K, dis_inod);
  point x = F.residue (hat_x);
  derr << "# inv_piola problem by Newton:" << endl
       << "# K     = " << K.name() << endl
       << "# hat_x = " << hat_x << endl
       << "# x     = " << x << endl
       << "# tol   = " << tol << endl
       << "# max_iter = " << max_iter << endl;
  F.set_x (x);
  point hat_x1 = F.initial();
  int status = damped_newton (F, hat_x1, tol, max_iter, &derr);
  derr << "# hat_x1 = " << hat_x1 << endl;
  return status && (norm(hat_x-hat_x1) < eps);
}
