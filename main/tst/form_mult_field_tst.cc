///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
size_t N;
Float f (const point& x) { return 1+(0.5/N)*(x[0]*(1-x[0])+x[1]*(1-x[1])+x[2]*(1-x[2])); }
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  Float prec = (argc > 3) ? atof(argv[3]) : 1e-10;
  N = omega.dimension();
  if (argc > 4) {
    warning_macro ("block " << argv[4]);
    Xh.block(argv[4]);
  } else {
    if (omega.dimension() >= 3) {
      Xh.block("back");
      Xh.block("front");
    }
    if (omega.dimension() >= 2) {
      Xh.block("top");
      Xh.block("bottom");
    }
    Xh.block("left");
    Xh.block("right");
  }
  field fh = lazy_interpolate(Xh, f);
  form m (Xh, Xh, "mass");
  field gh = m*fh;
  dout << round (gh, prec);
}
