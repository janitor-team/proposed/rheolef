///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
struct u_exact {
  point operator() (const point& x) const {
    point u;
    if (d > 0) u[0] = pow(x[0],a);
    if (d > 1) u[1] = pow(x[1],a);
    if (d > 2) u[2] = pow(x[2],a);
    return u;
  }
  u_exact (size_t d1, size_t a1) : d(d1), a(a1) {}
  size_t d, a;
};
// ----------------------------------------------------------------------------
int test_interpolate (geo omega, string approx, Float tol) {
// ----------------------------------------------------------------------------
  size_t d = omega.dimension();
  space Xh (omega, approx);
  size_t k = Xh.degree() - 1;
  field uh = lazy_interpolate (Xh, u_exact(d,k));
  string nodal_approx = "P"+std::to_string(k)+"d";
  space Yh (omega, nodal_approx, "vector");
  field vh = lazy_interpolate (Yh, uh);
  quadrature_option qopt;
  qopt.set_order (2*k+1);
  Float err_l1 = integrate (omega,norm(vh-u_exact(d,k)),qopt);
  derr << "interpolate: err_l1="<<err_l1<<endl;
  return err_l1 < tol ? 0 : 1; 
}
// ----------------------------------------------------------------------------
int test_mass (geo omega, string approx, Float tol) {
// ----------------------------------------------------------------------------
  size_t d = omega.dimension();
  space Xh (omega, approx);
  size_t k = Xh.degree() - 1;
  trial u(Xh); test v(Xh);
  // uh = L2 proj of u_exact on RTkd:
  integrate_option iopt;
  iopt.invert = true;
  form inv_mrt = integrate (dot(u,v), iopt);
  field lh = integrate (dot(u_exact(d,k),v));
  field uh = inv_mrt*lh;
  // u0h = L2 proj of uh on Pkd:
  space V0h (omega, "P"+std::to_string(k)+"d");
  trial u0(V0h); test v0(V0h);
  form inv_m0 = integrate (u0*v0, iopt);
  point e0 (1,0), e1(0,1), e2(0,0,1);
  field l0h, l1h, l2h;
  field u0h, u1h, u2h;
  if (d > 0) l0h = integrate (dot(uh,e0)*v0);
  if (d > 1) l1h = integrate (dot(uh,e1)*v0);
  if (d > 2) l2h = integrate (dot(uh,e2)*v0);
  if (d > 0) u0h = inv_m0*l0h;
  if (d > 1) u1h = inv_m0*l1h;
  if (d > 2) u2h = inv_m0*l2h;
  space Vh (omega, "P"+std::to_string(k)+"d", "vector");
  field uh_Pkd (Vh);
  if (d > 0) uh_Pkd[0] = u0h;
  if (d > 1) uh_Pkd[1] = u1h;
  if (d > 2) uh_Pkd[2] = u2h;
  quadrature_option qopt;
  qopt.set_order (2*k+1);
  Float err_l1 = integrate (omega,norm(u_exact(d,k)-uh_Pkd),qopt);
  derr << "mass: err_l1="<<err_l1<<endl;
  return err_l1 < tol ? 0 : 1; 
}
// ----------------------------------------------------------------------------
// test div
// ----------------------------------------------------------------------------
// div and interpolate should commute between L2->Pk and Hdiv->RTk
struct u_sin {
  point operator() (const point& x) const { return point(pow(x[0],a),pow(x[1],a)); }
  u_sin (Float a1) : a(a1) {}
  Float a;
};
struct div_u_sin {
  Float operator() (const point& x) const { return a*pow(x[0],a-1) + a*pow(x[1],a-1); }
  div_u_sin (Float a1) : a(a1) {}
  Float a;
};
int test_div (geo omega, string approx, Float tol) {
  space Th (omega, approx);
  size_t k = Th.degree()-1;
  space Xh (omega, "P"+std::to_string(k)+"d");
  field     sigma_h = lazy_interpolate (Th,    u_sin(k));
  field div_sigma_h = lazy_interpolate (Xh,div_u_sin(k));
  test vs (Xh);
  field lh1 = integrate(div_h(sigma_h)*vs);
  field lh2 = integrate(div_sigma_h*vs);
  field err_h = lh1 - lh2;
  Float err_div_linf = err_h.max_abs();
  derr << "err_div_linf = "<<err_div_linf << endl;
  return err_div_linf < tol ? 0 : 1; 
}
// ----------------------------------------------------------------------------
// test sides
// ----------------------------------------------------------------------------
int test_sides (geo omega, string approx, Float tol) {
warning_macro("sides...");
  space Tht (omega, approx);
  field sigma_ht (Tht,1.);
  size_t k = Tht.degree();
  space Xh (omega, "P"+std::to_string(k)+"d");
  space Th (omega, "P"+std::to_string(k)+"d", "vector");
  field uh (Xh, 0.), sigma_h (Th, 0.);
  test  tau_t  (Tht);
  Float alpha = 1;
  // BUG: fatal(../../../rheolef/nfem/plib/test.cc,176): unexpected non-boundary side K.dis_ie=e1
  field lht1 = integrate ("internal_sides", 
                 (2*dot(average(sigma_h),normal()) - alpha*jump(uh))
                *dot(average(tau_t),normal()))
	     ;
#ifdef TODO // BUG_RT_BDR
warning_macro("lht(2)...");
  // BUG: fatal(../../../rheolef/nfem/gbasis/basis_on_pointset.cc,593): index loc_idof=2 out of range [0:2[
  field lht2 = integrate ("boundary", 
                 // (dot(sigma_h,normal()) - alpha*uh)
		alpha
                *dot(tau_t,normal()))
	     ;
#endif // TODO
dis_warning_macro("sides: error not tested, due to BUG_RT_BDR");
warning_macro("sides done");
  return 0;
}
// ----------------------------------------------------------------------------
// test sides2 : (sigma.n)(tau.n) on local sides
// ----------------------------------------------------------------------------
int test_sides2_aux (geo omega, space Th1, space Th2, Float tol) {
warning_macro ("test_sides2_aux(0) Th1="<<Th1.name()<<", Th2="<<Th2.name()<<"...");
  trial sigma (Th1); test tau (Th2);
  form a1 = integrate (omega, on_local_sides(
                         dot(sigma,normal())*dot(tau,normal())));
warning_macro ("test_sides2_aux(1)...");
  form a2 =
#ifdef TODO
	    integrate ("internal_sides",
                         dot(average(sigma),normal())*dot(average(tau),normal())
                       + 0.25*dot(jump(sigma),normal())*dot(jump(tau),normal()))
          +
#endif // TODO
	    // BUG_RT_BDR
	    integrate ("boundary", dot(sigma,normal())*dot(tau,normal()))
	;
warning_macro ("test_sides2_aux(2)...");
  form err_a = a1-a2;
  Float err_sides2 = err_a.uu().max_abs();
  derr << "err_sides2 = " << err_sides2 << endl;
warning_macro ("test_sides2_aux(f)...");
  return 0;
}
int test_sides2_mixt (geo omega, space Th1, space Vh2, Float tol) {
warning_macro ("test_sides2_mixt(0) Th1="<<Th1.name()<<", Vh2="<<Vh2.name()<<"...");
  trial sigma (Th1); test v (Vh2);
warning_macro ("test_sides2_mixt(1)...");
  field lh2 = integrate ("boundary", dot(sigma,normal()));
warning_macro ("test_sides2_mixt(2)...");
  field lh1 = integrate (omega, on_local_sides(dot(sigma,normal())));
warning_macro ("test_sides2_mixt(3)...");
  form a2 =
#ifdef TODO
	    integrate ("internal_sides",
                         dot(average(sigma),normal())*average(v)
                       + 0.25*dot(jump(sigma),normal())*jump(v))
          +
#endif // TODO
	    // BUG_RT_BDR
	    integrate ("boundary", dot(sigma,normal())*v)
	;
warning_macro ("test_sides2_mixt(4)...");
  form a1 = integrate (omega, on_local_sides(dot(sigma,normal())*v));
  form err_a = a1-a2;
  Float err_sides2 = err_a.uu().max_abs();
  derr << "err_sides2 = " << err_sides2 << endl;
warning_macro ("test_sides2_mixt(f)...");
  return 0;
}
int test_sides2 (geo omega, string approx, Float tol) {
  space Tht (omega, approx);
  size_t k = Tht.degree() - 1;
  space Vh (omega, "P"+std::to_string(k)+"d");
  space Th (omega, "P"+std::to_string(k)+"d", "vector");
  int status = 0;
  status += test_sides2_mixt(omega, Tht, Vh,  tol);
#ifdef TODO
  status += test_sides2_aux (omega, Tht, Tht, tol);
#endif // TODO
  return status;
}
// ----------------------------------------------------------------------------
int main(int argc, char**argv) {
// ----------------------------------------------------------------------------
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ?      argv[2]  : "RT1d";
  Float  tol    = (argc > 3) ? atof(argv[3]) : 1e-10;
  Float  eps    = 1e5*numeric_limits<Float>::epsilon();
  int status = 0;
  status += test_interpolate (omega, approx, tol);
  status += test_mass        (omega, approx, tol);
  status += test_div         (omega, approx, eps);
  status += test_sides       (omega, approx, tol);
#ifdef TODO // BUG_RT_BDR
  status += test_sides2      (omega, approx, eps);
#endif // TODO
  return status;
}
