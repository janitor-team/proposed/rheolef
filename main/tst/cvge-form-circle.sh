#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
if test $# -eq 0; then
  echo "usage: $0 form Pk [nx=0] [ny=0] [mx=0] [my=0]" 
  echo "example: $0 mass P3 3"
  echo "example: $0 grad_grad P3 1 0 1 0"
  exit 0
fi
name="form_circle_tst"
form=${1-"mass"}
Pk=${2-"P1"}
nx=${3-"0"}
ny=${4-"0"}
mx=${5-"0"}
my=${6-"0"}
order=`echo $Pk | sed -e 's/P//'`
e="t"
np=2
case $e in
 e|t|q) 
	   #n_set="2 4 8 16 32 64 128"
	   n_set="8 16 32 64"
	   ;;
 ut|uq|utq) n_set="  10 20 40 80";;
 *) case $Pk in
    P1) n_set="5 10 15 20";;
    *)  n_set="3 5 7 10";
    esac
esac
verbose=true
#RUN="mpirun -np $np"
RUN=""
# ------------------------------------------
# utility
# ------------------------------------------
my_eval () {
  command="$*"
  if test "$verbose" = true; then echo "! $command" 1>&2; fi
  eval $command
  if test $? -ne 0; then
    echo "$0: error on command: $command"
    exit 1
  fi
}
# ------------------------------------------
# plot
# ------------------------------------------
basename="cvge-$name-$Pk-nx${nx}-ny${ny}-mx${mx}-my${my}-$e"
gdat="$basename.gdat"
plot="$basename.plot"
cat << EOF > $plot
set logscale
set size square

a = 1.0
c = 1.0
f(h) = c*h**a
fit f(x) "$gdat" using (1./\$1):(\$2) via a, c
titre = sprintf("pente = %g", a)

plot \
	"$gdat" u (1./\$1):(\$2) t "$Pk($e) err" w lp, \
	f(x) t titre

print "pente = ", a

pause -1 "<retour>"
EOF

# ------------------------------------------
# pente file = silent plot
# ------------------------------------------
tmp_pente="tmp.plot"
cat << EOF2 > $tmp_pente
set logscale
set size square

a = 1.0
c = 1.0
f(h) = c*h**a
fit f(x) "$gdat" using (1./\$1):(\$2) via a, c
titre = sprintf("pente = %g", a)

print "pente = ", a
EOF2

# ------------------------------------------
# gdat
# ------------------------------------------
geo_upgrade="../../main/bin/geo -upgrade -"
rm -f $gdat
echo "# cvgce: $form $Pk nx=$nx ny=${ny} mx=${mx} my=${my} $e"    | tee -a $gdat
echo "# n err" | tee -a $gdat
for n in $n_set; do
  geo="tmp-$n.geo"
  log="tmp-$n.log"
  my_eval "mkgeo_ball -$e $n -order $order 2>/dev/null > $geo"
  my_eval "$RUN ./${name} -form $form $geo -app ${Pk} -nx $nx -ny ${ny} -mx $mx -my $my 2>/dev/null > $log"
  err=`cat $log | grep error | gawk '{print $3}'`
  echo "$n $err" | tee -a $gdat
  rm -f $geo $log
done

# ------------------------------------------
# pente
# ------------------------------------------
log="tmp.log"
rm -f $log
(gnuplot $tmp_pente 2>&1) > $log
pente=`cat $log | grep pente | gawk '{printf ("%8.2f\n",$3)}'`


echo "# $name $Pk : pente"
echo "# elt	pente"
echo "$e	$pente"

gnuplot $plot
echo "! file \"$gdat\" created" 1>&2
echo "! file \"$plot\" created" 1>&2
