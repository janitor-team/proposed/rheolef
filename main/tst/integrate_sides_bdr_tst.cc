///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float f (const point& x) { return 1; }
Float negative (Float x) {
  return (x < -numeric_limits<Float>::epsilon()) ? 1 : 0; }
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Mh (omega["sides"], argv[2]);
  point u = omega.dimension() == 1 ? point(1) : point(1,0);
  test  mu(Mh);
  field kh1 = integrate("boundary", mu);
  field kh2 = integrate("boundary", f*mu);
  field kh3 = integrate("boundary", dot(u,normal())*mu);
  field kh4 = integrate("boundary", compose(negative,dot(u,normal()))*mu);
}
