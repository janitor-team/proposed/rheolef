//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// the branch unix command
// author: Pierre.Saramito@imag.fr
//

namespace rheolef {
/**
@commandfile branch handle a family of fields

@addindex command `branch`
@addindex command `gnuplot`
@addindex command `paraview`
@addindex file format `.branch` family of fields 
@addindex animation
@addindex continuation methods
@addindex time-dependent problems

Synopsis
========

    branch [options] file[.branch[.gz]]

Examples
========
Run an animation:

        branch file.branch
 
It uses either `gnuplot`, for 1d geometries,
or `paraview`, otherwise.

Next, let us extract the 17-th indexed and save it in `.field` file format.
Indexes started at 0:

        branch file.branch -extract 17 -branch > file-17.field


Description
===========

Read and visualize or output a branch of finite element fields from file.

Input file specification
========================

@addindex RHEOPATH environment variable

filename
>	Specifies the name of the file containing
>      	the input field.

`-`
>	Read field on standard input instead on a file.

`-I`@e dir \\
`-I` *dir* 
>       Add *dir* to the Rheolef file search path.
>       This option is useful e.g. when the mesh .geo and the .field files are in
>       different directories.
>       This mechanism initializes a search path given by the environment variable `RHEOPATH`.
>       If the environment variable `RHEOPATH` is not set, the default value is the current directory.

`-name`
>       When the field comes from standard input, the file base name
>       is not known and is set to "output" by default.
>       This option allows one to change this default.
>       Useful when dealing with output formats (graphic, format conversion)
>       that creates auxiliary files, based on this name.

Input format options
====================

@addindex command `bamg`
@addindex file format `.vtk` mesh

`-if` *format* \n
`-input-format` *format*
>       Load a mesh in the prescribed file *format*.
>       Supported input file formats are: `.branch` and `.vtk`.

Render specification
====================

`-gnuplot`
>       Run a 1d animation using `gnuplot`.

@addindex python
@addindex file `.py` python

`-paraview`
>       Run 2d and 3d animations using `paraview`.
>       Generate a collections of `.vtk` files and a
>       main `.py` python one, then execute the python file.

Rendering options
=================

`-skipvtk`
>       Do not regenerate the collection of `.vtk` files when 
>       using the `paraview` render.
>       Only generate the main `.py` python file and execute it.
>       Assume that all the `.vtk` files was already created
>       with the `-vtk` option or with 
>       `-paraview` one combined with `-noclean`.

`-color` \n
`-gray` \n
`-black-and-white` \n
`-bw`
>       Use (color/gray scale/black and white) rendering.
>       Color rendering is the default.
`-[no]showlabel`
>       Show or hide title, color bar and various annotations.
>       Default is to show labels.
`-label` *string*
>       Set the label to show for the represented value.
>       This supersedes the default value.
`-[no]elevation`
>       For a two dimensional field, represent values as elevation in the third dimension.
>	The default is no elevation.
`-[no]fill`
>       Isoline intervals are filled with color.
>       This is the default.
`-[no]volume`
>       For 3D data, render values using a colored translucid volume.
>       This option requires the `paraview` code.
`-scale` *float*
>       Applies a multiplicative factor to the field.
>       This is useful e.g. in conjunction with the `-elevation` option.
>       The default value is 1.
`-[no]stereo`
>       Rendering mode suitable for red-blue anaglyph 3D stereoscopic glasses.
>       This option is only available with `paraview`.
`-[no]cut`
>       Cut by a specified plane.
>       The cutting plane is specified by its origin point and normal vector.
>       This option requires `paraview`.
`-origin` *float* [*float* [*float*]]
>       Set the origin of the cutting plane.
>       Default is (0.5, 0.5, 0.5).
`-normal` *float* [*float* [*float*]]
>       Set the normal of the cutting plane.
>       Default is (1, 0, 0).
`-isovalue` [*float*] \n
`-iso`      [*float*]
>       Draw 2d isoline or 3d isosurface.
>       When the optional float is not provided,
>       a median value is used.
>       This option requires the `paraview` code.
`-noisovalue`
>       Do not draw isosurface.
>       This is the default.
`-n-iso` *int*
>       For 2D visualizations, the isovalue table contains
>       regularly spaced values from fmin to fmax, the bounds
>       of the field.

@addindex vorticity
@addindex stream function
`-n-iso-negative` *int*
>       The isovalue table is split into negatives and positives values.
>       Assume there is n_iso=15 isolines: if 4 is requested by this option,
>       then, there will be 4 negatives isolines, regularly spaced
>       from fmin to 0 and 11=15-4 positive isolines, regularly spaced
>       from 0 to fmax.
>       This option is useful when plotting e.g. vorticity or stream
>       functions, where the sign of the field is representative.

Output file specification
=========================

`-vtk`
>       Generate a collection of `.vtk` files for `paraview`.

`-branch`
>       Output on stdout in `.branch` format.

`-extract` *int* \n
`-index`   *int*
>	Extract the i-th record in the file.
>       The output is a field or multi-field file format.
>       Indexes started at 0.

`-toc`
>	Print the table of contents (toc) to standard output and exit.
>	Each index value is followed by the associated value (e.g. the time
>       or a physical parameter).

`-ndigit` *int*
>       Number of digits used to print floating point values
>       when using the `-branch` option.
>       Note that the default value depends upon the machine precision associated to the
>       `Float` type, as defined by the `configure` script during
>       the installation of the library (see @ref configuration_page).
>	When `Float` is `double`, then 16 digits are used by default.
>	This default value can be changed by this option, e.g.
>	for the portability of non-regression tests.

@addindex video file format
@addindex file format `.avi` image
@addindex file format `.jpg` image
@addindex file format `.png` image
@addindex file format `.tif` image
@addindex file format `.bmp` image

`-image-format` *string*
>       For image or video capture.
>       The supported argument are .`avi`, .`jpg`, .`png`, .`tif` and .`bmp`.
        This option should be combined with the `paraview` render.
        The output file is  basename.`avi` where *basename*
        is the name of the mesh, or can be set with the `-name` option.
`-resolution` *int* *int*
>       For the resolution of an image or a video capture.
>       The argument is a couple of sizes, separated by a white space.
>       This option can be used together with the `-image-format`
>       for any of the bitmap image formats.
>       This option requires the `paraview` render.

Others options
==============

`-umin` *float* \n
`-umax` *float*
>	Set the solution range for the `gnuplot` driver.
> 	By default this range is computed from the first field
>       of the branch, and this could be problematic when this
>       field is initialy zero.

`-subdivide *int*
>       When using a high order geometry, the number of points per edge used to draw
>       a curved element. Default value is the mesh order.

@addindex topography

`-topography` filename[.field[.gz]]
>       Performs a tridimensional elevation view based
>	on the topographic data.

@addindex projection
@addindex approximation P1
`-proj` *approx*
`-proj`
>       Convert all selected fields to approximation *approx* by using a L2 projection.
>       When argument is omitted, `P1` approximation is assumed.
`-lumped-proj`
>       Force `P1` approximation for L2 projection and use a lumped mass matrix for it.
`-round` [*float*]
>       Round the input up to the specified precision.
>       This option, combined with `-field`, leads to a round filter.
>       Useful for non-regression test purpose, in order
> 	to compare numerical results between files with a limited precision,
>       since the full double precision is machine-dependent.

`-[no]verbose`
>       Print messages related to graphic files created and
>       command system calls (this is the default).

`-[no]clean
>       Clear temporary graphic files (this is the default).
    
`-[no]execute`
>       Execute graphic command (this is the default).
>       The `-noexecute` variant is useful
>       in conjunction with the `-verbose` and `-noclean` options
>       in order to modify some render options by hand.

Example of file format conversion
=================================

For conversion from the `.vtk` legacy ascii file format to the `.branch` one,
simply writes:

        branch -if vtk -branch - < input.vtk > output.branch

The branch file format
======================

@addindex file format `.field`

The `.branch` file format bases on the `.field` one
(see @ref field_1 ):

        example        | general format
        -------------------------------------------------
        #!branch       | #!branch
        branch         | branch
        1 1 11         | <version> <nfield=1> <nvalue=N>
        time u         | <key> <field name>
                       |
        #time 3.14     | #<key> <key value 1>
        #u             | #<field name>
        field          | <field 1>
        .....          | ....
                       |
        .....          | ....
        #time 6.28     | #<key> <key value N>
        #u             | #<field name>
        field          | <field N>
        .....          |  ....

The key is here `time`, but could be any string without spaces,
such as `t` or `lambda`.
Labels appears all along the file to facilitate direct jumps and field and step skips.

The previous example contained one `field` at each time step.
The format supports several fields, such as (t,u(t),p(t)), where u could
be a multi-component field (e.g. a vector):

        #!branch
        branch
        1 2 11
        time u p
         
        #time 3.14
        #u
        ...
        #p
        ...
        #time 6.28
        ...

Implementation
==============
@showfromfile
*/
} // namespace rheolef

//
// TODO: filter options
// ------------
//  -extract N
// ------------
//  -mask  u
//  -unmask  u
//  -mask-all
//  -unmask-all
//
//  -select u <==> -mask-all -unmask u
//
//  -show-names
// ------------
//  -norm {-l2|-linf|-h1}
//	-> normes in direct via gnuplot via branch (geo with dim=0)
//
//  Q. how to combine and synchronize images u(t) and |u(t)| ?
//     -> field n(t,x), mesh with dim=0, one point p0, value n(t,p0) = |u(t)| 
// ------------
// -energy-norm
//  sqrt(|u^{n+1}-u^n|/(t^{n+1}-t^n)) : mesure a(u,u), l'energie (norme l2 pour u)
// ------------
// graphic:
//   -height      : with -topography, interprets scalar as height 
//   -yield value : mask all scalars lesser than
// menu:
//   stop/start
//   start number
//   transparency : of the height
//   edit color map for height : can put uniform color
//   stop/start

// -------------------------------------------------------------
// program
// -------------------------------------------------------------
#include <rheolef.h>
#include <rheolef/iofem.h>
using namespace rheolef;
using namespace std;

void usage()
{
      cerr << "branch: usage: branch "
	   << "-|file[.branch[.gz]]"
	   << "[-toc] "
	   << "[-Idir|-I dir] "
           << "[-name string] "
	   << "[-index int|-extract int] "
           << "[-[catch]mark string] "
	   << "[-ndigit int] "
	   << "[-proj [string]] "
           << "[-lumped-proj] "
           << "[-if {branch,vtk}] "
	   << "[-paraview|-gnuplot] "
	   << "[-vtk|-branch] "
	   << "[-[no]verbose|-[no]clean|-[no]execute] "
           << "[-color|-gray|-black-and-white|-bw] "
	   << "[-[no]elevation] "
	   << "[-[no]volume] "
	   << "[-[no]fill] "
           << "[-[no]showlabel] "
           << "[-label string] "
           << "[-[no]stereo] "
           << "[-[no]cut] "
           << "[-normal x [y [z]]] "
           << "[-origin x [y [z]]] "
           << "[-scale float] "
           << "[-iso[value] float|-noiso[value]] "
           << "[-n-iso int] "
           << "[-n-iso-negative int] "
	   << "[-topography filename] "
           << "[-umin float] "
           << "[-umax float] "
           << "[-subdivide int] "
	   << endl;
      exit (1);
}

typedef field::size_type size_type;

typedef enum {
	text_render,
	paraview_render,
	vtk_render,
	plotmtv_render,
	gnuplot_render,
	toc_render
} render_type;

struct reuse_proj_form_type {
  vector<form_basic<Float,sequential>> m, p;
  vector<bool>                         done;
  reuse_proj_form_type() : m(), p(), done() {}
  size_t size() const { return m.size(); }
  void resize(size_t n) { m.resize(n); p.resize(n); done.resize(n,false); }
};

field_basic<Float,sequential>
proj (
        size_t                               extract_id,
	const field_basic<Float,sequential>& uh,
	bool                                 do_lumped_mass,
        string	            		     use_proj_approx,
        reuse_proj_form_type                 reuse)
{
  const space_basic<Float,sequential>& Uh = uh.get_space();
  size_t k = Uh.degree();
  if (k == 0) k++;
  std::string approx = (do_lumped_mass || use_proj_approx == "") ? "P1" : use_proj_approx;
  space_basic<Float,sequential> Vh (uh.get_geo(), approx, uh.valued());
  test_basic<Float,sequential,details::vf_tag_10> u (Uh), v (Vh);
  test_basic<Float,sequential,details::vf_tag_01> vt (Vh);
  integrate_option fopt;
  fopt.lump = do_lumped_mass;
  if (!reuse.done[extract_id] || !(reuse.m[extract_id].get_geo() == uh.get_geo())) { 
    switch (Uh.valued_tag()) {
      case space_constant::scalar:
	  reuse.m[extract_id] = integrate(v*vt, fopt);
	  reuse.p[extract_id] = integrate(u*vt);
	  break;
      case space_constant::vector:
	  reuse.m[extract_id] = integrate(dot(v,vt), fopt);
	  reuse.p[extract_id] = integrate(dot(u,vt));
	  break;
      case space_constant::tensor:
      case space_constant::unsymmetric_tensor:
	  reuse.m[extract_id] = integrate(ddot(v,vt), fopt);
	  reuse.p[extract_id] = integrate(ddot(u,vt));
	  break;
      default:
	  error_macro ("proj: unexpected valued field: " << Uh.valued());
    }
    reuse.done[extract_id] = true;
  }
  solver_basic<Float,sequential> sm (reuse.m[extract_id].uu());
  field_basic<Float,sequential> vh (Vh);
  vh.set_u() = sm.solve((reuse.p[extract_id]*uh).u());
  return vh;
}
void
extract (
	idiststream&                  in,
	odiststream&                  out,
	bool                          do_proj,
	bool                          do_lumped_mass,
        string			      use_proj_approx,
	size_type                     extract_id,
        const Float&                  scale_value,
        reuse_proj_form_type          reuse)
{
    branch_basic<Float,sequential> event;
    in  >> event.header();
    out << event.header();
    if (reuse.size() == 0) reuse.resize(event.size());
    for (size_t id = 0; (in >> catchmark (event.parameter_name())) && (id < extract_id); ++id)
      true;
    check_macro (in.good(), "extract: index="<<extract_id<<" not found"); 

    Float t;
    in >> t;
    if (!in) return;
    event.set_parameter (t);
    for (size_t i = 0; in && i < event.size(); i++) {
       in >> catchmark (event[i].first) >> event[i].second;
    }
    field_basic<Float,sequential> uh = event[0].second;
    if (do_proj) {
      uh = proj (extract_id, uh, do_lumped_mass, use_proj_approx, reuse);
    }
    if (scale_value != Float(1)) {
      uh *= scale_value;
    }
    out << event(t,uh);
    out << event.finalize();
}

void
put (
	idiststream&                  in,
	odiststream&                  out,
	bool                          do_proj,
	bool                          do_lumped_mass,
        string			      use_proj_approx,
	bool                          def_fill_opt,
	size_type                     extract_id,
	const Float&                  scale_value,
	const std::pair<Float,Float>& u_range,
	render_type		      render,
        reuse_proj_form_type          reuse)
{
    if (extract_id != numeric_limits<size_type>::max()) {
      extract (in, out, do_proj, do_lumped_mass, use_proj_approx, extract_id, scale_value, reuse);
      return;
    }
    Float t = 0;
    branch_basic<Float,sequential> event;
    if (u_range.first  !=  std::numeric_limits<Float>::max() ||
        u_range.second != -std::numeric_limits<Float>::max()) {
      event.set_range (u_range);
    }
    in  >> event.header();
    if (reuse.size() == 0) reuse.resize(event.size());
    if (render == toc_render) {
      in.is() >> noverbose;
      out << setprecision(numeric_limits<Float>::digits10)
          << "# i " << event.parameter_name() << endl;
      Float param;
      for (size_t n = 0; in >> catchmark (event.parameter_name()) >> param; ++n) {
        out << n << " " << param << endl;
      }
      return;
    }
    field_basic<Float,sequential> uh;
    size_type n = 0;
    while (in >> event) {
      for (size_t i = 0; i < event.size(); i++) {
        uh = event[i].second;
        if (n == 0) {
          // default 1D graphic render is gnuplot
          if (uh.get_geo().dimension() == 1 && render == paraview_render) {
            dout.os() << gnuplot;
          }
          // default 3D is iso+cut and nofill; default 2D is nocut and fill...
          if (uh.get_geo().map_dimension() == 3) {
#ifdef TODO
            if (!def_plane_cut_opt)  dout.os() << cut;
#endif // TODO
            if (!def_fill_opt)       dout.os() << nofill;
          } else {
#ifdef TODO
            if (!def_plane_cut_opt)  dout.os() << nocut;
#endif // TODO
            if (!def_fill_opt)       dout.os() << fill;
          }
          out << event.header();
        }
        if (do_proj) {
          uh = proj (i, uh, do_lumped_mass, use_proj_approx, reuse);
        }
        if (scale_value != Float(1)) {
          uh *= scale_value;
        }
	event[i].second = uh;
      }
      out << event;
      n++;
    }
    out << event.finalize();
}
void set_input_format (idiststream& in, std::string input_format)
{
       if (input_format == "vtk")    in.is() >> vtk;
  else if (input_format == "branch") in.is() >> rheo;
  else {
      std::cerr << "branch: invalid input format \""<<input_format<<"\"" << std::endl;
      usage();
  }
}
int main(int argc, char**argv)
{
    environment distributed(argc, argv);
    if (argc <= 1) usage();
    clog << verbose;
    dout.os() << noelevation;
    bool on_stdin = false;
    bool do_proj = false;
    string use_proj_approx = "";
    bool do_lumped_mass = false;
    reuse_proj_form_type reuse;
    bool do_cut  = false;
    bool def_fill_opt = false;
    int digits10 = numeric_limits<Float>::digits10;
    render_type render = paraview_render; dout.os() << paraview;
    size_type extract_id = numeric_limits<size_type>::max();
    Float scale_value = 1;
    string file_name, name, input_format = "branch";
    std::pair<Float,Float> u_range;
    u_range.first  =  std::numeric_limits<Float>::max();
    u_range.second = -std::numeric_limits<Float>::max();
    dout.os() << showlabel;
    // this normal is not so bad for the dirichlet.cc demo on the cube:
    cout << setnormal(point(-0.015940197423022637, -0.9771157601293953, -0.21211011624358989));
    cout << setorigin(point(std::numeric_limits<Float>::max()));

    for (int i = 1; i < argc; i++) {

        if      (strcmp (argv[i], "-ndigit") == 0)    { digits10 = atoi(argv[++i]); }
        else if (strcmp (argv[i], "-toc") == 0)       { render = toc_render; }
        else if (strcmp (argv[i], "-index") == 0 || strcmp (argv[i], "-extract") == 0)
                                                      { extract_id = atoi(argv[++i]); render = text_render; dout.os() << rheo; }
        else if (strcmp (argv[i], "-branch") == 0)    { render = text_render; dout.os() << rheo; }
        else if (strcmp (argv[i], "-vtk") == 0)       { render = vtk_render; dout.os() << vtk; }
        else if (strcmp (argv[i], "-gnuplot") == 0)   { render = gnuplot_render; dout.os() << gnuplot; }
        else if (strcmp (argv[i], "-paraview") == 0)  { render = paraview_render; dout.os() << paraview; }
        else if (strcmp (argv[i], "-skipvtk") == 0)   { dout.os() << skipvtk; }
        else if (strcmp (argv[i], "-proj") == 0)      { do_proj = true; 
                                                        if (i+1 < argc && argv[i+1][0] != '-') {
                                                          use_proj_approx = argv[++i];
                                                        }
                                                      }
        else if (strcmp (argv[i], "-lumped-proj") == 0){ do_proj = do_lumped_mass = true; use_proj_approx = "P1"; }
        else if (strcmp (argv[i], "-elevation") == 0) { dout.os() << elevation; }
        else if (strcmp (argv[i], "-noelevation") == 0) { dout.os() << noelevation; }
        else if (strcmp (argv[i], "-color") == 0)           { dout.os() << color; }
        else if (strcmp (argv[i], "-gray") == 0)            { dout.os() << gray; }
        else if (strcmp (argv[i], "-black-and-white") == 0) { dout.os() << black_and_white; }
        else if (strcmp (argv[i], "-bw") == 0)              { dout.os() << black_and_white; }
        else if (strcmp (argv[i], "-showlabel") == 0)       { dout.os() << showlabel; }
        else if (strcmp (argv[i], "-noshowlabel") == 0)     { dout.os() << noshowlabel; }
	else if (strcmp (argv[i], "-fill") == 0)            { dout.os() << fill;   def_fill_opt = true; }
        else if (strcmp (argv[i], "-nofill") == 0)          { dout.os() << nofill; def_fill_opt = true; }
        else if (strcmp (argv[i], "-stereo") == 0)          { dout.os() << stereo;
                                                              if (render != paraview_render) {
                                                                dout.os() << paraview;
                                                                render  = paraview_render;
                                                              }
                                                            }
        else if (strcmp (argv[i], "-nostereo") == 0)        { dout.os() << nostereo; }
        else if (strcmp (argv[i], "-volume") == 0)          { dout.os() << paraview << volume;
                                                              render  = paraview_render; }
        else if (strcmp (argv[i], "-novolume") == 0)        { dout.os() << novolume; }
        else if (strcmp (argv[i], "-cut") == 0)             { do_cut = true; }
        else if (strcmp (argv[i], "-nocut") == 0)           { do_cut = false; }
        else if (strcmp (argv[i], "-umin") == 0)   {
            if (i+1 == argc || !is_float(argv[i+1])) usage();
            u_range.first = to_float (argv[++i]);
        } else if (strcmp (argv[i], "-umax") == 0)   {
            if (i+1 == argc || !is_float(argv[i+1])) usage();
            u_range.second = to_float (argv[++i]);
        } else if (strcmp (argv[i], "-scale") == 0)   {
            if (i+1 == argc || !is_float(argv[i+1])) usage();
            scale_value = to_float (argv[++i]);
            dout.os() << setvectorscale (scale_value);
        } else if (strcmp (argv[i], "-noisovalue") == 0)  {
	    dout.os() << noiso; 
        } else if (strcmp (argv[i], "-isovalue") == 0 || strcmp (argv[i], "-iso") == 0)   {

            dout.os() << iso;
            if (i+1 < argc && is_float(argv[i+1])) {
              Float iso_value = to_float (argv[++i]);
              dout.os() << setisovalue(iso_value);
            }
        } else if (strcmp (argv[i], "-n-iso") == 0)   {

            if (i+1 == argc || !isdigit(argv[i+1][0])) usage();
            size_t idx = atoi (argv[++i]);
            dout.os() << setn_isovalue(idx);

        } else if (strcmp (argv[i], "-n-iso-negative") == 0)   {

            if (i+1 == argc || !isdigit(argv[i+1][0])) usage();
            size_t idx = atoi (argv[++i]);
            dout.os() << setn_isovalue_negative(idx);

        } else if (strcmp (argv[i], "-subdivide") == 0) {
            if (i == argc-1) { cerr << "branch -subdivide: option argument missing" << endl; usage(); }
            size_t nsub = atoi(argv[++i]);
            dout.os() << setsubdivide (nsub);
        } else if (strcmp (argv[i], "-topography") == 0)   {

            if (i+1 == argc) usage();
	    idiststream zin (argv[++i]);
            field_basic<Float,sequential> z;
            zin >> z;
            dout.os() << settopography(z);
	}
        else if (strcmp (argv[i], "-I") == 0)         {
            if (i+1 == argc) { cerr << "geo -I: option argument missing" << endl; usage(); }
            append_dir_to_rheo_path (argv[++i]);
        }
        else if (argv [i][0] == '-' && argv [i][1] == 'I')  { append_dir_to_rheo_path (argv[i]+2); }
        else if (strcmp (argv[i], "-noclean") == 0)   clog << noclean;
        else if (strcmp (argv[i], "-clean") == 0)     clog << clean;
        else if (strcmp (argv[i], "-noexecute") == 0) clog << noexecute;
        else if (strcmp (argv[i], "-execute") == 0)   clog << execute;
        else if (strcmp (argv[i], "-verbose") == 0)   clog << verbose;
        else if (strcmp (argv[i], "-noverbose") == 0) clog << noverbose;
        else if ((strcmp(argv[i], "-origin") == 0) || (strcmp (argv[i], "-normal") == 0))   {

            point x;
            unsigned int io = i;
            if (i+1 == argc || !is_float(argv[i+1])) {
                warning_macro ("invalid argument to `" << argv[i] << "'");
                usage();
            }
            x[0] = to_float (argv[++i]);
            if (i+1 < argc && is_float(argv[i+1])) {
                x[1] = to_float (argv[++i]);
                if (i+1 < argc && is_float(argv[i+1])) {
                    x[2] = to_float (argv[++i]);
                }
            }
            if (strcmp (argv[io], "-origin") == 0)   {
                cout << setorigin(x);
            } else {
                cout << setnormal(x);
            }
        } else if (strcmp (argv[i], "-image-format") == 0) {
            if (i == argc-1) {
                cerr << "field -image-format: option argument missing" << endl;
                usage();
            }
            string format = argv[++i];
            dout.os() << setimage_format(format);
        }
        else if (strcmp (argv[i], "-resolution") == 0) {
            if (i == argc-1 || !isdigit(argv[i+1][0])) { std::cerr << "geo -resolution: option argument missing" << std::endl; usage(); }
            size_t nx = atoi(argv[++i]);
            size_t ny = (i < argc-1 && isdigit(argv[i+1][0])) ? atoi(argv[++i]) : nx;
            dout.os() << setresolution(point_basic<size_t>(nx,ny));
        }
	else if (argv [i][0] == '-' && argv [i][1] == 'I') {

	    append_dir_to_rheo_path (argv[i]+2);
        }
        else if (strcmp (argv[i], "-name") == 0)            {
            if (i+1 == argc) { std::cerr << "field -name: option argument missing" << std::endl; usage(); }
            name = argv[++i];
        }
        else if (strcmp (argv[i], "-label") == 0)            {
            if (i+1 == argc) { std::cerr << "field -label: option argument missing" << std::endl; usage(); }
            string label = argv[++i];
            dout.os() << setlabel(label);
        }
        else if (strcmp (argv[i], "-catchmark") == 0 || strcmp (argv[i], "-mark") == 0)            {
            if (i+1 == argc) { std::cerr << "field -mark: option argument missing" << std::endl; usage(); }
            string mark = argv[++i];
            dout.os() << setmark(mark);
        }
        else if (strcmp (argv[i], "-if") == 0 ||
                 strcmp (argv[i], "-input-format") == 0) {
            if (i == argc-1) { std::cerr << "branch "<<argv[i]<<": option argument missing" << std::endl; usage(); }
            input_format = argv[++i];
        }
	else if (strcmp (argv [i], "-") == 0) {
	    
	    on_stdin = true;
            dout.os() << setbasename("output") << reader_on_stdin;
	    file_name = "output";
	}
	else if (argv [i][0] == '-') {
	    cerr << "branch: invalid option `" << argv[i] << "'" << endl;
	    usage();
	}
	else {

            // input on file
            string dir_name = get_dirname(argv[i]);
	    prepend_dir_to_rheo_path (dir_name);
            file_name = get_basename(delete_suffix (delete_suffix (argv[i], "gz"), "branch"));
        }
    }
    if (!on_stdin && file_name == "") {
	cerr << "branch: no input specified" << endl;
	usage();
    }
    string basename = (name != "") ? name : file_name;
    dout.os() << setbasename(basename)
              << setprecision(digits10);

    if (on_stdin) {
        set_input_format (din, input_format);
        put(din,dout, do_proj, do_lumped_mass, use_proj_approx, def_fill_opt, extract_id, scale_value, u_range, render, reuse);
    } else {
        idiststream in (file_name, "branch");
        check_macro(in.good(), "\"" << file_name << "[.branch[.gz]]\" not found.");
        set_input_format (in, input_format);
        put(in, dout, do_proj, do_lumped_mass, use_proj_approx, def_fill_opt, extract_id, scale_value, u_range, render, reuse);
    }
}
