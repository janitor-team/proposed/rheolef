#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
config2=$1
file=$2
if test x"$file" = x""; then
  echo "usage: $0 file" 1>&2
  exit 1
fi

# CONFIG_FILES=$file CONFIG_HEADERS= CONFIG_COMMANDS= /bin/bash ./config.status

mv $file $file.tmp

. $config2

sed -e "s%@RHEOLEF_SHLIBPATH_VAR@%${RHEOLEF_SHLIBPATH_VAR}%" \
    -e "s%@RHEOLEF_HARDCODE_LIBDIR_FLAG_SPEC@%${RHEOLEF_HARDCODE_LIBDIR_FLAG_SPEC}%" \
  < $file.tmp > $file

rm -f $file.tmp

