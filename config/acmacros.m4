dnl
dnl This file is part of Rheolef.
dnl
dnl Copyright (C) 2000-2009 Pierre Saramito 
dnl
dnl Rheolef is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl Rheolef is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with Rheolef; if not, write to the Free Software
dnl Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
dnl 

# Checks whether a compiler accepts a given flag
# ----------------------------------------------

# $1 = compiler name
# $2 = flag
# $3 = make macro containing flags for that compiler

# Note: changes AC_LANG()

AC_DEFUN([CHECK_COMPILE_FLAG],
	[AC_MSG_CHECKING(whether the $1 compiler accepts $2)
	check_save_flags="$$3"
	AC_LANG_PUSH($1)
	$3="$$3 $2"

	# The program needs to contain something for the test source
	# file to be created by autoconf.

	# Some options really need to be linked (not only compiled) to
	# check whether they work.

	AC_LINK_IFELSE([ifelse($1,Fortran 77,
[       program x
       end],
			[AC_LANG_PROGRAM])],
		check_flag_ok=yes,
		check_flag_ok=no)
	AC_MSG_RESULT($check_flag_ok)
	if test "$check_flag_ok" = no;
	then
		$3="$check_save_flags"
	fi

	AC_LANG_POP($1)
])
