#ifndef _RHEO_NUMERIC_FLAGS_H
#define _RHEO_NUMERIC_FLAGS_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// set some flags for floats
//
// author: Pierre.Saramito@imag.fr
//
// date: 25 january 2000
//
template <class T>
class numeric_flags {
public:

    // Cast to double before printing.
    // Usefull for non-regression test purpose, since
    // bigfloat does not know about 
    // ios::scientific and ios::fixed.
    // thus text output could differ.

    static bool output_as_double() { return false; }
    static void output_as_double(bool)   {}
};
#endif // _RHEO_NUMERIC_FLAGS_H
