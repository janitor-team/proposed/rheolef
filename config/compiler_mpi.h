# ifndef _RHEOLEF_COMPILER_MPI_H
# define _RHEOLEF_COMPILER_MPI_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/compiler.h"

#ifdef _RHEOLEF_HAVE_MPI
// -----------------------------------------------------------------------
#  pragma GCC diagnostic push
#  pragma GCC diagnostic ignored "-Weffc++" // MPI C++ leads to warns
#  pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#  pragma GCC diagnostic ignored "-Wparentheses"
#  include <mpi.h>
#  ifdef _RHEOLEF_HAVE_BOOST_MPI_HPP
#    include <boost/mpi.hpp>
     namespace rheolef {
       namespace mpi = boost::mpi;
     } // namespace rheolef
#  else
#    error "boost.mpi library expected"
#  endif // _RHEOLEF_HAVE_BOOST_MPI_HPP
#  pragma GCC diagnostic pop
// -----------------------------------------------------------------------
#endif // _RHEOLEF_HAVE_MPI
#endif // _RHEOLEF_COMPILER_MPI_H
