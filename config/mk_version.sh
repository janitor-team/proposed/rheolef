#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
# protability problem with echo '\\' in Makefile

srcdir=$1
top_srcdir=$2
VERSION=$3
MAIN=$4
EXAMPLEDIR=$5

LASTUPDATE="`/bin/sh ${top_srcdir}/config/mdate-sh ${srcdir}/${MAIN}.tex`"

cat > vtmp.tex << EOF
\\newcommand{\\LASTUPDATE}{${LASTUPDATE}}
\\newcommand{\\VERSION}{${VERSION}}
\\newcommand{\\EXAMPLEDIR}{${EXAMPLEDIR}}
EOF

if cmp -s vtmp.tex version.tex; then
  true
else
   echo "Updating version.tex"
   cp vtmp.tex version.tex
fi

/bin/rm -f vtmp.tex

