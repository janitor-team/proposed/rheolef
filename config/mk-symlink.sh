#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
# usage: 
#   mk-symlink.sh top_srcdir top_builddir srcdir {headers}*
#
# TODO: use relative paths when given paths are relative ?
#
here=`pwd` # absolute reference
builddir=$here
rel_top_srcdir=$1
cd ${rel_top_srcdir}; top_srcdir=`pwd`; cd $here
rel_top_builddir=$2
cd ${rel_top_builddir}; top_builddir=`pwd`; cd $here
rel_srcdir=$3
cd ${rel_srcdir}; srcdir=`pwd`; cd $here
shift
shift
shift
pkginclude_HEADERS=$*

echo "linking exported headers...."

mkinstalldirs="${top_srcdir}/config/install-sh -d"
/bin/sh ${mkinstalldirs} ${top_builddir}/include/rheolef

echo "cd ${top_builddir}/include/rheolef"
cd ${top_builddir}/include/rheolef

for h in ${pkginclude_HEADERS}; do
      echo "/bin/rm -f ${top_builddir}/include/rheolef/$h"
      /bin/rm -f ${top_builddir}/include/rheolef/$h
      if test -f ${builddir}/$h; then
        echo "ln -s ${builddir}/$h $h"
        ln -s ${builddir}/$h $h
      elif test -f ${srcdir}/$h; then
        echo "ln -s ${srcdir}/$h $h"
        ln -s ${srcdir}/$h $h
      else
        echo "file not found: $h"; exit 1
      fi
done
echo "linking done."

