#ifndef _RHEOLEF_COMPILER_EIGEN_H
#define _RHEOLEF_COMPILER_EIGEN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// just include eigen Dense & Tensor without any warning
// author: Pierre.Saramito@imag.fr
// date: 22 january 2019

#include "rheolef/compiler.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wignored-attributes"
#pragma GCC diagnostic ignored "-Wuninitialized"
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/CXX11/Tensor>
#pragma GCC diagnostic pop

namespace rheolef { namespace details {

// c(j,k) = a(i,j,k)*b(i)
template<class T1, class T2, class T3>
void
contract0_tensor3_vector (
  const Eigen::Tensor<T1,3>&                             a,
  const Eigen::Matrix<T2,Eigen::Dynamic,1>&              b,
        Eigen::Matrix<T3,Eigen::Dynamic,Eigen::Dynamic>& c)
{
  // TODO: c = b.transpose()*a; ==> need compat between Eigen::Matrix and Tensor 
  size_t ni = a.dimension(0);
  size_t nj = a.dimension(1);
  size_t nk = a.dimension(2);
  c.resize (nj, nk);
  for (size_t j = 0; j < nj; ++j) {
  for (size_t k = 0; k < nk; ++k) {
    T3 sum = 0;
    for (size_t i = 0; i < ni; ++i) {
      sum += b[i]*a(i,j,k);
    }
    c(j,k) = sum;
  }}
}

}} // namespace rheolef::details
#endif // _RHEOLEF_COMPILER_EIGEN_H
