#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# --------------------------------------------------------------------------
## Generated automatically from config.mk.in by configure.

# libtool does not support yet parallel make (make -j)
.NOTPARALLEL:

# customize compiler and options here:
CXX                   = @CXX@
CXXFLAGS              = @CXXFLAGS@ $(NO_OPTIMIZE_CXXFLAGS)
CC                    = @CC@
CFLAGS                = @CFLAGS@
CPP	              = @CPP@
CXXCPP                = @CXXCPP@
CPPFLAGS              = @CPPFLAGS@
LDFLAGS               = @LDFLAGS@ @EXTRA_LDFLAGS@
LIBS                  = @LIBS@

# on x86: force compilation with ieee norm
CXXFLAGS_IEEE         = @CXXFLAGS_IEEE@

# max optimization: not used in makefiles
OPTIMIZE_CFLAGS	      = @OPTIMIZE_CFLAGS@

# for debug mode with dmalloc
#DMALLOC_OPTIONS="debug=0x4000503,log=a.log"  # runtime : too laxist: do not detect some memory bugs
#DMALLOC_OPTIONS="debug=0x14f47d83,log=a.log" # null-free-forbiden : too strict
#DMALLOC_OPTIONS="debug=0x4f46d03,log=a.log"  # high : too strict
#DMALLOC_OPTIONS="debug=0x4f40d03,log=a.log"  # medium
#DMALLOC_OPTIONS="debug=0x4e40503,log=a.log"  # low
 DMALLOC_OPTIONS="debug=0x24f47d03,log=a.log" # null-free-allowed : ok, but skit/ptst2/solver_tst too slow

# additional libraries
ALLOCA                = @ALLOCA@
INCLUDES_BLAS         = @INCLUDES_BLAS@
LDADD_BLAS            = @LDADD_BLAS@
INCLUDES_SUITESPARSE_AMD = @INCLUDES_SUITESPARSE_AMD@
LDADD_SUITESPARSE_AMD    = @LDADD_SUITESPARSE_AMD@
INCLUDES_CHOLMOD      = @INCLUDES_CHOLMOD@
LDADD_CHOLMOD         = @LDADD_CHOLMOD@ $(LDADD_BLAS)
INCLUDES_UMFPACK      = @INCLUDES_UMFPACK@
LDADD_UMFPACK         = @LDADD_UMFPACK@ $(LDADD_BLAS)
INCLUDES_EIGEN        = @INCLUDES_EIGEN@
LDADD_EIGEN           = @LDADD_EIGEN@
INCLUDES_BOOST        = @INCLUDES_BOOST@
LDADD_BOOST           = @LDADD_BOOST@
INCLUDES_BOOST_IO     = $(INCLUDES_BOOST)
LDADD_BOOST_IO        = $(LDADD_BOOST) -lboost_iostreams
INCLUDES_GINAC 	      = @INCLUDES_GINAC@
LDADD_GINAC 	      = @LDADD_GINAC@
INCLUDES_FLOAT128     = @INCLUDES_FLOAT128@
LDADD_FLOAT128	      = @LDADD_FLOAT128@
QD_EXT                = @QD_EXT@
INCLUDES_CLN          = @INCLUDES_CLN@
LDADD_CLN             = @LDADD_CLN@
INCLUDES_FLOAT 	      = ${INCLUDES_BIGFLOAT} ${INCLUDES_FLOAT128}
LDADD_FLOAT    	      = ${LDADD_BIGFLOAT}    ${LDADD_FLOAT128}
INCLUDES_ZLIB 	      =
LDADD_ZLIB    	      = # -lz
INCLUDES_CGAL         = @INCLUDES_CGAL@
INCLUDES_CGAL_INTERN  = @INCLUDES_CGAL_INTERN@
LDADD_CGAL            = @LDADD_CGAL@


# debug tools
INCLUDES_DMALLOC      = @INCLUDES_DMALLOCXX@ @INCLUDES_DMALLOC@
LDADD_DMALLOC	      = @LDADD_DMALLOC@
LDADD_DMALLOCXX	      = @LDADD_DMALLOCXX@ 

# distributed tools
USE_DISTRIBUTED	      = @USE_DISTRIBUTED@
MPIRUN		      = @MPIRUN@
NPROC_MAX	      = 4
INCLUDES_MPI          = @INCLUDES_MPI@
LDADD_MPI             = @LDADD_MPI@
INCLUDES_BOOST_MPI    = $(INCLUDES_BOOST)
LDADD_BOOST_MPI       = @LDADD_BOOST_MPI@
INCLUDES_SCOTCH       = @INCLUDES_SCOTCH@
LDADD_SCOTCH          = @LDADD_SCOTCH@ $(LDADD_ZLIB)
INCLUDES_PARMETIS     = @INCLUDES_PARMETIS@
LDADD_PARMETIS        = @LDADD_PARMETIS@
INCLUDES_PARTITIONNER = $(INCLUDES_SCOTCH) $(INCLUDES_PARMETIS)
LDADD_PARTITIONNER    = $(LDADD_SCOTCH)    $(LDADD_PARMETIS)
INCLUDES_SCALAPACK    = @INCLUDES_SCALAPACK@
LDADD_SCALAPACK       = @LDADD_SCALAPACK@
# trilinos also requieres: mpi
INCLUDES_TRILINOS     = @INCLUDES_TRILINOS@
LDADD_TRILINOS        = @LDADD_TRILINOS@

# mumps/seq requieres: blas
# mumps/mpi requieres: blas, scotch, scalapack, blacs, mpi
INCLUDES_MUMPS        = @INCLUDES_MUMPS@
LDADD_MUMPS           = @LDADD_MUMPS@ $(LDADD_SCALAPACK)

# pastix/seq requieres: blas
# pastix/mpi requieres: blas, scotch, mpi
INCLUDES_SOLVER       = $(INCLUDES_UMFPACK) $(INCLUDES_CHOLMOD) $(INCLUDES_MUMPS) $(INCLUDES_TRILINOS) $(INCLUDES_PASTIX) $(INCLUDES_SUITESPARSE_AMD) $(INCLUDES_BLAS)
LDADD_SOLVER          = $(LDADD_UMFPACK)    $(LDADD_CHOLMOD)    $(LDADD_MUMPS)    $(LDADD_TRILINOS)    $(LDADD_PASTIX)    $(LDADD_SUITESPARSE_AMD)    $(LDADD_BLAS)

# dox documention for refman:
DOXYGEN   = @DOXYGEN@
SRC2MAN_FLAGS = @SRC2MAN_FLAGS@
MANDB     = @MANDB@

# pdf documention for usrman:
PDFLATEX  = @PDFLATEX@
BIBTEX	  = @BIBTEX@
MAKEINDEX = @MAKEINDEX@
GS	  = @GS@
FIG2DEV   = @FIG2DEV@
GNUPLOT   = @GNUPLOT@

# maintainers tools:
MKDIRHIER = mkdir -p
FLEX	  = @FLEX@
FLEXLEXER_H = @FLEXLEXER_H@
BISON	  = @BISON@

# avoid printing directories when GNU make
no_print_directory_option = @no_print_directory_option@
MAKEFLAGS = ${no_print_directory_option}
AM_INSTALL_PROGRAM_FLAGS = @INSTALL_PROGRAM_FLAGS@

# where to install web site
WEB_DIR = $(HOME)/public_html/$(PACKAGE)

# latex environment
HAVE_LATEX_HYPEREF   = @HAVE_LATEX_HYPEREF@

INSTALL = ${top_srcdir}/config/install-sh -c

-include @RECOGNIZED_CXX@
-include ${top_srcdir}/config/compiler.mk
-include $(CVSMKROOT)/git.mk

