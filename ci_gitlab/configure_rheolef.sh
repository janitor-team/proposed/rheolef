#!bin/bash

# --- configure rheolef software ---
#
# Usage :
# > export CI_PROJECT_DIR=<path-to-rheolef-repository>
# 
# > sh configure_rheolef.sh
#
#   Use absolute path or path relative to $CI_PROJECT_DIR
# - export for CI_PROJECT_DIR, IMAGE_NAME and ctest_build_model is not needed when this script is called by gitlab-ci.
#

: ${CI_PROJECT_DIR:?"Please set environment variable CI_PROJECT_DIR with 'siconos' repository (absolute) path."}
: ${IMAGE_NAME:?"Please set environment variable IMAGE_NAME. It will be used to name cdash build site."}

# --- Run ctest for Siconos ---
# configure, build, test
mkdir ${CI_PROJECT_DIR}/build
rsync -r ${CI_PROJECT_DIR}/* ${CI_PROJECT_DIR}/build/
cd ${CI_PROJECT_DIR}/build
./bootstrap
./configure --prefix=${CI_PROJECT_DIR}/install-rheolef
