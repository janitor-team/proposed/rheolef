## process this file with automake to produce Makefile.in
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
include ${top_builddir}/config/config.mk

#------------------------------------------------------------------------------
# the file list
#------------------------------------------------------------------------------
docdir = $(prefix)/share/doc/@doc_dir@

# les fichiers *.css et *.js et le .gif
# doivent etre copie dans html/ apres run de doxygen
HTML_SRC = 				\
	rheodoxy.css 			\
	rheo_navtree_hacks.js		\
	citing-rheolef.gif		\
	citing-rheolef_bib.html

PNG_SRC = 				\
	new.jpg				\
	synthese-fig.png		\
	dirichlet-2d-P2.png		\
	dirichlet-3d-mayavi-mix-fig.png	 \
	mosolov-sector-P2-Bi-0.5-n-1.png \
	oldroyd-contraction3-zr-We-0.7.png \
	xsquare-Re-1000-geo.png 	\
	xsquare-Re-1000-phi.png 	\
	embankment-adapt-2d-P2-fig.png 	\
	incompressible-elasticity-cube-fig.png

DOX_SRC = 				\
	main_menu.xml			\
	header.html			\
	footer.html			\
	main_page.dox			\
	citing-rheolef.dox		\
	section_features.dox		\
	section_gallery.dox		\
	section_documentation.dox	\
	section_usersguide.dox		\
	section_download.dox		\
	section_binaries.dox		\
	section_sources.dox		\
	section_forum.md		

# TODO: citing-rheolef.dox a generer automatiquement depuis citing-rheolef.html
# ajouter aussi citing-rheolef_bib.html dans le repertoire html/
EXTRA_DIST =				\
	Makefile.am			\
	section_dox_make.sh 		\
	section_example_dox_make.sh	\
	patch_html.sh			\
	Doxyfile-html.in 		\
	$(HTML_SRC)			\
	$(DOX_SRC)			\
	$(PNG_SRC)			

CVSIGNORE = Makefile.in
WCIGNORE  = 				\
	$(CVSIGNORE) 			\
	$(PNG_SRC)			\
	citing-rheolef.dox		\
	Doxyfile-html.in

LICENSEIGNORE = 			\
	footer.html			\
	$(PNG_SRC)			\
	$(HTML_SRC)			\
	$(DOX_SRC)			

#------------------------------------------------------------------------------
# automatically generated files
#------------------------------------------------------------------------------
AUTO_GENERATED = 				\
	$(srcdir)/section_auto_authors.md	\
	$(srcdir)/section_auto_configure.md	\
	$(srcdir)/section_auto_command.dox	\
	$(srcdir)/section_auto_class.dox	\
	$(srcdir)/section_auto_function.dox	\
	$(srcdir)/section_auto_linalgclass.dox	\
	$(srcdir)/section_auto_linalgfunction.dox \
	$(srcdir)/section_auto_femclass.dox	\
	$(srcdir)/section_auto_utilclass.dox	\
	$(srcdir)/section_auto_internalclass.dox	\
	$(srcdir)/section_auto_internalfunction.dox	\
	$(srcdir)/section_auto_example.dox

# non-recursively scan .md .dox .h .cc .icc .sh .in in these directories 
# REFMAN_SCAN_SUBDIRS is defined in configure.ac
REFMAN_SCAN_SUBDIRS = @REFMAN_SCAN_SUBDIRS@

# some .dox files are automatically generated in a builddir,
# so add both src and bluild dirs:
DOX_INPUT =                                             \
	${addprefix ${abs_top_srcdir}/,   ${REFMAN_SCAN_SUBDIRS}}

MAN_INPUT =                                             \
	${addprefix ${abs_top_builddir}/, ${REFMAN_SCAN_SUBDIRS}}

# exclude some boring source files:
DOX_EXCLUDE = 					\
	@top_srcdir@/Makefile.in		\
	@top_srcdir@/*/Makefile.in		\
	@top_srcdir@/*/*/Makefile.in 		\
	@top_srcdir@/config/*.sh		\
	@top_srcdir@/config/*.cc		\
	@top_srcdir@/config/compiler.h		\
	@top_srcdir@/config/dmalloc_return.h 	\
	@top_srcdir@/config/numeric_flags.h 	\
	@top_srcdir@/config/numeric_limits.h 	\
	@top_srcdir@/config/config.mk.in 	\
	@top_srcdir@/config/rheolef.mk.in 	\
	@top_srcdir@/config/acconfig.h.in 	\
	@top_srcdir@/config/src2man.sh.in 	\
	@top_srcdir@/config/Doxyfile-man.in 	\
        @top_srcdir@/util/bamg/*.h		\
        @top_srcdir@/util/bamg/*.hpp		\
        @top_srcdir@/util/bamg/*.cpp		\
        @top_srcdir@/fem/quadrature/*_tst.*	\
        @top_srcdir@/fem/geo_element/*_tst.*	\
        @top_srcdir@/fem/lib/*.sh		\
        @top_srcdir@/fem/lib/*_lex.cc 		\
        @top_srcdir@/fem/lib/*_yacc.cc 		\
        @top_srcdir@/fem/lib/*_tst.*		\
        @top_srcdir@/linalg/lib/promote.h 	\
        @top_srcdir@/main/lib/*.sh		\
        @top_srcdir@/main/lib/*_lex.cc 		\
        @top_srcdir@/main/lib/*_yacc.cc 	\
        @top_srcdir@/main/sbin/mkgeo_grid_*d.cc	\
        @top_srcdir@/doc/examples/*.sh		\
        @top_srcdir@/doc/web/*.in		\
        @top_srcdir@/doc/web/*.sh

show-doxy-example-files:
	@cd ../examples; $(MAKE) -s Makefile 1>&2
	@cd ../examples; $(MAKE) -s show-doxy-example-files

show-non-doxy-example-files:
	@cd ../examples; $(MAKE) -s Makefile 1>&2
	@cd ../examples; $(MAKE) -s show-non-doxy-example-files | \
          sed -e 's, , ../../../rheolef/doc/examples/,g' -e 's,^, ../../../rheolef/doc/examples/,g'

Doxyfile-html: $(srcdir)/Doxyfile-html.in $(srcdir)/Makefile.am
	predefined_multi_line=`grep '#define' ${top_builddir}/config/config.h | awk '{print $$2}'`; \
	predefined=`echo $$predefined_multi_line`; \
	non_example_files=`$(MAKE) -s show-non-doxy-example-files`; \
	exclude=`echo ${DOX_EXCLUDE} $$non_example_files`; \
	sed \
            -e "s,@EXCLUDE@,$$exclude," \
            -e "s,@INPUT@,${DOX_INPUT}," \
            -e "s,@IMAGE_PATH@,${DOX_INPUT}," \
            -e "s,@PREDEFINED@,$$predefined," \
            -e "s,@SRCDIR@,$(srcdir)," \
            -e "s,@TOP_SRCDIR@,$(top_srcdir)," \
            -e "s,@TOP_BUILDDIR@,$(top_builddir)," \
            -e "s,\@VERSION\@,$(VERSION)," \
            < $(srcdir)/Doxyfile-html.in > Doxyfile-html

$(srcdir)/section_auto_authors.md: ${top_srcdir}/AUTHORS
	echo "@page authors_page Authors and copying" > $@
	echo "@par" >> $@
	echo >> $@
	echo "This software is distributed under the <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">Gnu General Public license</a>." >> $@
	echo >> $@
	cat ${top_srcdir}/AUTHORS >> $@
$(srcdir)/section_auto_configure.md: ${top_srcdir}/configure.ac 
	echo "@page configuration_page Configuration" > $@
	grep -e   '^dnl #' ${top_srcdir}/configure.ac | \
	  sed  -e 's/^dnl #//'  >> $@
$(srcdir)/section_auto_command.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 1 $(MAN_INPUT) > $@
$(srcdir)/section_auto_class.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 2 $(MAN_INPUT) > $@
$(srcdir)/section_auto_function.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 3 $(MAN_INPUT) > $@
$(srcdir)/section_auto_linalgclass.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 4 $(MAN_INPUT) > $@
$(srcdir)/section_auto_linalgfunction.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 5 $(MAN_INPUT) > $@
$(srcdir)/section_auto_femclass.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 6 $(MAN_INPUT) > $@
$(srcdir)/section_auto_utilclass.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 7 $(MAN_INPUT) > $@
$(srcdir)/section_auto_internalclass.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 8 $(MAN_INPUT) > $@
$(srcdir)/section_auto_internalfunction.dox: section_dox_make.sh $(AUTO_DOX_DEPEND)
	 bash $(srcdir)/section_dox_make.sh 9 $(MAN_INPUT) > $@
$(srcdir)/section_auto_example.dox: section_example_dox_make.sh $(AUTO_DOX_DEPEND)
	@cd ../examples; $(MAKE) -s Makefile 1>&2
	@examples=`$(MAKE) -s show-doxy-example-files` ; \
	 bash $(srcdir)/section_example_dox_make.sh $(top_srcdir)/doc/examples $$examples > $@


#------------------------------------------------------------------------------
# rules that run doxygen
#------------------------------------------------------------------------------
include $(abs_top_builddir)/doc/web/depend.mk

if HAVE_DOX_DOCUMENTATION
dvi-local: stamp-html
else
dvi-local:
endif

stamp-html: Doxyfile-html $(AUTO_GENERATED) $(HTML_DEPEND)
	touch stamp-html
	if test x"${DOXYGEN}" != x""; then 	\
	  rm -f depend.mk && $(MAKE) depend.mk;	\
	  rm -rf html doxygen.log; 		\
	  here=`pwd`; cd $(abs_top_srcdir); TOP_SRCDIR=`pwd`; cd $$here; \
	  export SRC2DOX_FILTER_TOP_SRCDIR="$$TOP_SRCDIR"; \
	  echo "${DOXYGEN} Doxyfile-html"; 		      \
		${DOXYGEN} Doxyfile-html || true; 	      \
	  if test -d html; then			\
	    for x in $(HTML_SRC); do 		\
              cp $(srcdir)/$$x html/$$x;	\
	    done; 				\
	    mkdir -p html/images;		\
	    for x in $(PNG_SRC); do 		\
              cp $(srcdir)/$$x html/images/$$x; \
	    done; 				\
	  fi;					\
	  bash $(srcdir)/patch_html.sh html;	\
          grep 'unable to resolve reference to' doxygen.log | \
            sed -e "s,${abs_top_srcdir},," | sort -u || true; \
          n_err=`grep 'unable to resolve reference to' doxygen.log | wc -l`; \
          test "$$n_err" -eq 0;                 \
	fi

$(abs_top_builddir)/doc/web/depend.mk:
	echo "" > depend.mk;						\
	echo "AUTO_DOX_DEPEND = \\" >> depend.mk;			\
	for suffix in am; do 				    		\
	  find $(DOX_INPUT) -name "*.$${suffix}" | sed -e 's,$$, \\,' >> depend.mk; \
	done;								\
	echo "" >> depend.mk;						\
	echo "HTML_DEPEND = \\" >> depend.mk;				\
	for suffix in h cc icc sh dox; do 		    		\
	  find $(DOX_INPUT) -name "*.$${suffix}" | sed -e 's,$$, \\,' >> depend.mk; \
	done;								\
	echo "" >> depend.mk

install-data-local: stamp-html
	rm -rf $(DESTDIR)$(docdir)/html
	if test -d html; then cp -r html $(DESTDIR)$(docdir); fi

clean-local:
	rm -rf Doxyfile-html html doxygen.log stamp-html xml rheolef.tag depend.mk $(AUTO_GENERATED)

