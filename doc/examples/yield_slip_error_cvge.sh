#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
#
# utility
# ------------------------------------------
#verbose=false
verbose=true
my_eval () {
  command="$*"
  if test "$verbose" = true; then echo "! $command" 1>&2; fi
  eval $command
  if test $? -ne 0; then
    echo "$0: error on command: $command"
    exit 1
  fi
}
# ------------------------------------------
k=${1-"1"}
S="0.25"
n="0.5"
Cf="1"
r="1"
sym="symmetric"
eps="1e-14"
method="damped_newton"
#method="augmented_lagrangian"
L="3 5 10 20 30 40 50"
echo "# yield_slip: convergence with h=1/n"
echo "# approx = P$k"
echo "# method = $method"
echo "# S      = $S"
echo "# n      = $n"
echo "# Cf     = $Cf"
echo "# r      = $r"
echo "# symmetry = $sym"
echo "# tol    = $eps"
echo "# n err_linf err_l2 err_h1 err_b err_lb"
for m in $L; do
  geo="circle-$m.geo"
  my_eval "mkgeo_ball -t $m -order $k > $geo"
  if test $method = "augmented_lagrangian"; then
    my_eval "./yield_slip_augmented_lagrangian   $geo P$k $S $n $Cf 2>/dev/null | ./yield_slip_error 1e10 > tmp.log 2>/dev/null"
  else
    my_eval "./yield_slip_damped_newton          $geo P$k $S $n $Cf $r -$sym 2>/dev/null | ./yield_slip_error 1e10 > tmp.log 2>/dev/null"
  fi
  err_linf=`grep err_linf tmp.log | gawk '{print $3}'`
  err_l2=`grep err_l2     tmp.log | gawk '{print $3}'`
  err_h1=`grep err_h1     tmp.log | gawk '{print $3}'`
  err_b=`grep err_b       tmp.log | gawk '{print $3}'`
  err_lb=`grep err_lb     tmp.log | gawk '{print $3}'`
  echo "$m $err_linf $err_l2 $err_h1 $err_b $err_lb"
done
