#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
ROUNDER="$BINDIR/field - -field -round"
NPROC_MAX=${NPROC_MAX-"6"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "    not yet (skiped)"
#exit 0

status=0
# --------------------------------------------------------------------
# run tests on abrupt contraction P1b-P1 stokes
# --------------------------------------------------------------------
# note: velocity sufers from variation of rounding effects with mpi
# and matrix products; stream function psi is more stable,
# thus check psi_max :
psi_max="2.72867435237797e-05"; # diag() was buggy
psi_max="2.43408263903391e-05"; # diag(ab) inversion
psi_max="1.96922159307387e-05"; # true block-diag inv of ab
tol="1e-6";
loop_mpirun "./stokes_contraction_bubble ${SRCDIR}/contraction-small.geo 2>/dev/null | RHEOPATH=${SRCDIR} ./streamf_contraction 2>/dev/null | RHEOPATH=${SRCDIR} $BINDIR/field -max - 2>/dev/null > tmp.log && cat tmp.log | gawk '{ err=int((\$1-${psi_max})/${tol}); have_value=1;} END { exit (have_value==1 && err==0 ?0:1);}'"
if test $? -ne 0; then status=1; fi

exit $status
