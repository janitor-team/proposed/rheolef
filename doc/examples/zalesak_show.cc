///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "zalesak.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ?      argv[2]  : "P1d";
  size_t n_max  = (argc > 3) ? atoi(argv[3]) : 24;
  space Xh (omega, approx);
  Float tf = 4*acos(Float(-1)), delta_t = tf/n_max;
  branch event ("t","phi");
  for (size_t n = 0; n <= n_max; n++) {
    Float t = n*delta_t;
    field pi_h_phi = lazy_interpolate (Xh, phi_exact(t));
    dout << event (t, pi_h_phi);
  }
}
