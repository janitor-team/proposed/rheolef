#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

#echo "      not yet (skipped)"
#exit 0

eps="1e-11"
# err_linf
# geo			P1d	P2d	P3d
L="
mkgeo_grid_1d  10	0.08	$eps 	$eps
mkgeo_grid_1d 100	8e-4	$eps	$eps
mkgeo_grid_2d  10	0.2	$eps	$eps
mkgeo_grid_3d   6	0.6	$eps    $eps
"

geo="tmp.geo"

while test "$L" != ""; do

  mkgeo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  n=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  run "${SBINDIR}/$mkgeo $n -region -v4 2>/dev/null | ${BINDIR}/geo -upgrade - > $geo 2>/dev/null"
  if test $? -ne 0; then status=1; fi

  for Pk in P1d P2d P3d; do
    err=`echo $L | gawk '{print $1}'`
    L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
    loop_mpirun "./transmission_dg $geo $Pk 2>/dev/null | ./transmission_error $err >/dev/null 2>/dev/null"
    if test $? -ne 0; then status=1; fi
  done
done

run "rm -f $geo"

exit $status
