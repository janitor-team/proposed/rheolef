///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile neumann-laplace.cc The Poisson problem with Neumann boundary conditions
#include "rheolef.h"
using namespace rheolef;
using namespace std;
size_t d;
Float f (const point& x) { return 1; }
Float g (const point& x) { return -0.5/d; }
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  d = omega.dimension();
  space Xh (omega, argv[2]);
  trial u (Xh); test v (Xh);
  form  a  = integrate (dot(grad(u),grad(v)));
  field b  = integrate(v);
  field lh = integrate (f*v) + integrate ("boundary", g*v); 
  form  A = {{   a,      b},
             { trans(b), 0}};
  field Bh = {   lh,     0};
  field Uh (Bh.get_space(), 0); 
  A.set_symmetry(true);
  problem p (A);
  p.solve (Bh, Uh);
  dout << Uh[0];
}
