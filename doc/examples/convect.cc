///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile convect.cc Convection-diffusion equation by the method of characteristics
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "rotating-hill.h"
int main (int argc, char **argv) {
  environment rheolef (argc,argv);
  geo omega (argv[1]);
  string approx = (argc > 2) ? argv[2] : "P1";
  Float  nu     = (argc > 3) ? atof(argv[3]) : 1e-2;
  size_t n_max  = (argc > 4) ? atoi(argv[4]) : 50;
  size_t d = omega.dimension();
  Float delta_t = 2*acos(-1.)/n_max;
  space Vh (omega, approx, "vector");
  field uh = lazy_interpolate (Vh, u(d));
  space Xh (omega, approx);
  Xh.block ("boundary");
  field phi_h = lazy_interpolate (Xh, phi(d,nu,0));
  characteristic X (-delta_t*uh);
  integrate_option iopt;
  iopt.set_family (integrate_option::gauss_lobatto);
  iopt.set_order (Xh.degree());
  trial phi (Xh); test psi (Xh);
  branch event ("t","phi");
  dout << catchmark("nu") << nu << endl
       << event (0, phi_h);
  for (size_t n = 1; n <= n_max; n++) {
    Float t = n*delta_t;
    Float c1 = 1 + delta_t*phi::sigma(d,nu,t);
    Float c2 = delta_t*nu;
    form  a  = integrate (c1*phi*psi + c2*dot(grad(phi),grad(psi)), iopt);
    field lh = integrate (compose(phi_h, X)*psi, iopt);
    problem p (a);
    p.solve (lh, phi_h);
    dout << event (t, phi_h);
  }
}
