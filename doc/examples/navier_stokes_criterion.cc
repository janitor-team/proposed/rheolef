///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// zcat square-4.field.gz | ./navier_stokes_criterion | geo -
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "navier_stokes_criterion.icc"
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  adapt_option options;
  options.err             = (argc > 1) ? atof(argv[1]) : 1e-4;
  options.hmin            = (argc > 2) ? atof(argv[2]) : 0.004;
  options.additional      = "-RelError";
  options.hmax            = 0.1;
  Float Re;
  field uh;
  din >> catchmark("Re") >> Re
      >> catchmark("u")  >> uh;
  field ch = navier_stokes_criterion (Re, uh);
  dout << adapt (ch, options);
}
