///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile helmholtz_band_iterative.cc The Helmholtz problem on a surface by the banded level-set method -- iterative solver
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "sphere.icc"
int main (int argc, char**argv) {
  environment rheolef(argc, argv);
  geo lambda (argv[1]);
  size_t d = lambda.dimension();
  space Xh (lambda, "P1");
  field phi_h = lazy_interpolate(Xh, phi);
  band gamma_h (phi_h);
  space Bh (gamma_h.band(), "P1");
  trial u (Bh); test v (Bh);
  form  a  = lazy_integrate (gamma_h, u*v + dot(grad_s(u),grad_s(v)));
  field lh = lazy_integrate (gamma_h, f(d)*v);
  field uh (Bh,0);
  solver_option sopt;
  sopt.max_iter = 10000;
  sopt.tol = 1e-10;
  minres (a.uu(), uh.set_u(), lh.u(), eye(), sopt);
  dout << catchmark("phi") << phi_h
       << catchmark("u")   << uh;
}
