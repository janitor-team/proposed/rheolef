///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile rotating-hill.h Convection-diffusion equation -- the rotating hill benchmark
struct u {
  point operator() (const point & x) const {
    return (d == 1) ? point(u0) : point(x[1], -x[0]); }
  u (size_t d1) : d(d1), u0 (0.5/acos(Float(-1))) {}
  protected: size_t d; Float u0;
};
struct phi {
  Float operator() (const point& x) const {
    return exp(-4*nu*(t/t0) - dist2(x,x0t())/(t0+4*nu*t)); }
  phi (size_t d1, Float nu1, Float t1=0) : d(d1), nu(nu1), t(t1),
    u0 (0.5/acos(Float(-1))), x0(-0.5,0) {}
  static Float sigma(size_t d, Float nu1, Float t=0) {
    return 4*nu1/t0 - 2*d*nu1/(t0 + 4*nu1*t); }
  point x0t() const {
    if (d == 1) return point(x0[0] + u0*t);
    return point( x0[0]*cos(t) + x0[1]*sin(t),
                 -x0[0]*sin(t) + x0[1]*cos(t)); }
  point d_x0t_dt() const {
    if (d == 1) return point(u0);
    return point(-x0[0]*sin(t) + x0[1]*cos(t),
                 -x0[0]*cos(t) - x0[1]*sin(t)); }
  protected: size_t d; Float nu, t, u0; point x0;
  static constexpr Float t0 = 0.2;
};
