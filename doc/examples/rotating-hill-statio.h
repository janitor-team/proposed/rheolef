///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rotating-hill.h"
struct f: phi { // - d phi/dt
  Float operator() (const point& x) const {
    return (4*nu/t0 - 4*nu*dist2(x,x0t())/sqr(t0+4*nu*t)
     -2*dot(d_x0t_dt(),x-x0t())/(t0+4*nu*t))*phi::operator()(x); }
  f (size_t d1, Float nu1, Float t1=0) : phi(d1,nu1,t1) {}
};
