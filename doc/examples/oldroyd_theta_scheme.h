///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile oldroyd_theta_scheme.h The Oldroyd problem by the theta-scheme -- class header
template<class Problem>
struct oldroyd_theta_scheme {
  oldroyd_theta_scheme();
  void initial (const geo& omega, field& tau_h, field& uh, field& ph,
		string restart);
  bool solve (field& tau_h, field& uh, field& ph);
protected:
  void step      (const field& tau_h0, const field& uh0, const field& ph0,
                  field& tau_h, field& uh, field& ph) const;
  void sub_step1 (const field& tau_h0, const field& uh0, const field& ph0,
                  field& tau_h, field& uh, field& ph) const;
  void sub_step2 (const field& uh0, const field& tau_h1, const field& uh1,
                  field& tau_h, field& uh) const;
  Float residue  (field& tau_h, field& uh, field& ph) const;
  void reset (const geo& omega);
  void update_transport_stress (const field& uh) const;
public:
  Float We, alpha, a, Re, delta_t, tol;
  size_t max_iter;
protected:
  space Th, Xh, Qh;
  form b, c, d, mt, inv_mt, mu, mp;
  mutable form th;
  mutable field thb;
  Float theta, lambda, eta, nu, c1, c2, c3, c4, c5;
  problem_mixed stokes;
};
#include "oldroyd_theta_scheme1.h"
#include "oldroyd_theta_scheme2.h"
#include "oldroyd_theta_scheme3.h"
