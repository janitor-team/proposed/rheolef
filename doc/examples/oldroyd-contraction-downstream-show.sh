#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
input=${1-"aa.field"}
geo=`field -get-geo $input 2>/dev/null`
sys_coord=`geo -sys-coord $geo 2>/dev/null | gawk '{print $3}'`
cartesian=`if test $sys_coord = "cartesian"; then echo 1; else echo 0; fi`
L2=`geo $geo -xmax | gawk '{print $3}'`
L=`echo $L2 | gawk '{print $1/2.}'`
echo "L=$L"
echo "sys_coord=$sys_coord cartesian=$cartesian L2=$L2 L=$L"
output="output.plot"
echo "alpha = 8./9."  > $output.plot
grep '#We' $input | sed -e '#We/We =/' >> $output.plot

cat $input | field - -name output-tau00 -domain downstream -gnuplot -elevation -noclean -noexecute -comp 00
cat $input | field - -name output-tau01 -domain downstream -gnuplot -elevation -noclean -noexecute -comp 01
cat $input | field - -name output-u0    -domain downstream -gnuplot -elevation -noclean -noexecute -catchmark u -comp 0
cat $input | field - -catchmark u -field | ./streamf_contraction2 | \
             field - -name output-psi   -domain downstream -gnuplot -elevation -noclean -noexecute 
cat $input | field - -catchmark u -field | ./streamf_contraction2 | \
             field - -name output-psi-up -domain upstream -gnuplot -elevation -noclean -noexecute 

cat $input | field - -name output-tau00-cut -cut -normal -1 0 -origin $L 0 -gnuplot -elevation -noclean -noexecute -comp 00
cat $input | field - -name output-tau01-cut -cut -normal -1 0 -origin $L 0 -gnuplot -elevation -noclean -noexecute -comp 01
cat $input | field - -name output-u0-cut    -cut -normal -1 0 -origin $L 0 -gnuplot -elevation -noclean -noexecute -catchmark u -comp 0

cat >> $output << EOF

cartesian = $cartesian
c = 4.
gu = (cartesian ? 3./c : 8/c**2)
gd = (cartesian ? 3.   : 8.)

tau00_exact(y) = 2*alpha*We*(gd*y)**2
tau01_exact(y) = - alpha*gd*y
u0_exact(y)    = 0.5*gd*(1-y**2)
psi_exact(y)   = (cartesian ? 0.5*gd*(y - y**3/3 - 2./3) : 0.25*gd*(y**2*(1-y**2/2) - 0.5) )
psi_up_exact(y)= (cartesian ? 0.5*gu*c*((y/c)*(1- (y/c)**2/3) - 2./3) : 0.25*(gu*c**2)*((y/c)**2*(1-(y/c)**2/2) - 0.5) )

set size square
set colors classic
set sample 1000
set xrange [0:1]
plot \
"output-tau00.gdat" \
  u 2:3 \
  t 'tau00(y)' \
  with lines lw 2, \
"output-tau00-cut.gdat" \
  u 1:2 \
  t 'tau00-cut(y)' \
  with lines lw 2 lc 3, \
tau00_exact(x) \
  w l lc 0 dt 2

pause -1 "<return>"

plot \
"output-tau01.gdat" \
  u 2:3 \
  t 'tau01(y)' \
  with lines lw 2, \
"output-tau01-cut.gdat" \
  u 1:2 \
  t 'tau01-cut(y)' \
  with lines lw 2 lc 3, \
tau01_exact(x) \
  w l lc 0 dt 2

pause -1 "<return>"

plot \
"output-u0.gdat" \
  u 2:3 \
  t 'u0(y)' \
  with lines lw 2, \
"output-u0-cut.gdat" \
  u 1:2 \
  t 'u0-cut(y)' \
  with lines lw 2 lc 3, \
u0_exact(x) \
  w l lc 0 dt 2

pause -1 "<return>"

plot \
"output-psi.gdat" \
  u 2:3 \
  t 'psi(y)' \
  with lines lw 2, \
psi_exact(x) \
  w l lc 0 dt 2

pause -1 "<return>"

set xrange [0:c]
plot \
  "output-psi-up.gdat" \
  u 2:3 \
  t 'psi-up(y)' \
  with lines lw 2, \
psi_up_exact(x) \
  w l lc 0 dt 2

pause -1 "<return>"
EOF

gnuplot $output

