///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile combustion_error.cc The combustion problem -- error analysis
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "combustion_exact.icc"
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  bool is_upper = (argc > 1) && (argv[1][0] == '1');
  bool is_crit  = (argc > 1) && (argv[1][0] == 'c');
  Float tol     = (argc > 2) ? atof(argv[2]) : 1e-15;
  Float lambda_h;
  field uh;
  din >> catchmark("lambda") >> lambda_h
      >> catchmark("u")      >> uh;
  Float lambda = (is_crit ? lambda_c() : lambda_h);
  const geo& omega = uh.get_geo();
  const space& Xh = uh.get_space();
  field pi_h_u = lazy_interpolate (Xh, u_exact(lambda,is_upper));
  field eh = pi_h_u - uh;
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*Xh.degree()+1);
  Float err_l2
   = sqrt(integrate(omega, norm2(uh - u_exact(lambda,is_upper)), iopt));
  Float err_h1
   = sqrt(integrate(omega, norm2(grad(uh)-grad_u(lambda,is_upper)), iopt));
  Float err_linf = eh.max_abs();
  dout << "err_linf = " << err_linf << endl
       << "err_l2   = " << err_l2   << endl
       << "err_h1   = " << err_h1   << endl;
  return (err_h1 < tol) ? 0 : 1;
}
