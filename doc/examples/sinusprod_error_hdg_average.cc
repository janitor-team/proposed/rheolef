///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile sinusprod_error_hdg_average.cc The sinus product function -- error analysis for the hybrid discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  Float err_linf_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  field uh, bar_uh; 
  din >> catchmark("u")     >> uh
      >> catchmark("bar_u") >> bar_uh;
  space Xh = uh.get_space();
  space Zh = bar_uh.get_space();
  geo omega = Zh.get_geo();
  size_t d = omega.dimension();
  size_t k = Xh.degree();
  trial zeta(Zh); test xi(Zh);
  integrate_option iopt;
  iopt.invert = true;
  form inv_mz = integrate (zeta*xi, iopt);
  iopt.set_order(2*(k+2)+3);
  field lh    = integrate (u_exact(d)*xi, iopt);
  field bar_pi_h_u = inv_mz*lh;
  field eh = lazy_interpolate (Zh, bar_uh-bar_pi_h_u);
  Float err_linf = eh.max_abs();
  iopt.set_order(2);
  Float err_l2 = sqrt(integrate (omega, sqr(bar_uh-bar_pi_h_u), iopt));
  derr << "err_l2   = " << err_l2 << endl
       << "err_linf = " << err_linf << endl;
  return (err_linf <= err_linf_expected) ? 0 : 1;
}
