#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
DATADIR=${TOP_SRCDIR}/main/tst
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# geo         err_h1
L="
mesh-3d-20     3.7
mesh-3d-21     3.9
"

run "$SBINDIR/mkgeo_grid_3d -v4 -T 20 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2  2>/dev/null| $BINDIR/geo -upgrade - > mesh-3d-20.geo 2>/dev/null"
run "$SBINDIR/mkgeo_grid_3d -v4 -T 21 -a -2 -b 2 -c -2 -d 2 -f -2 -g 2  2>/dev/null| $BINDIR/geo -upgrade - > mesh-3d-21.geo 2>/dev/null"

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  err=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  loop_mpirun "./laplace_band $geo 2>/dev/null | ./laplace_band_error $err >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done

run "rm -f mesh-3d-20.geo mesh-3d-21.geo"

exit $status
