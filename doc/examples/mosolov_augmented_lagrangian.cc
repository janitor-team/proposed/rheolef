///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile mosolov_augmented_lagrangian.cc The Mossolov problem by the augmented Lagrangian method
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "mosolov_augmented_lagrangian.h"
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  mosolov_augmented_lagrangian pb;
  geo omega (argv[1]);
  string approx  = (argc > 2) ?      argv[2]  : "P1";
  pb.Bi          = (argc > 3) ? atof(argv[3]) : 0.2;
  pb.n           = (argc > 4) ? atof(argv[4]) : 1;
  size_t n_adapt = (argc > 5) ? atoi(argv[5]) : 0;
  pb.max_iter    = (argc > 6) ? atoi(argv[6]) : 10000;
  pb.err         = (argc > 7) ? atof(argv[7]) : 1e-4;
  pb.r           = 100;
  pb.tol         = 1e-10;
  pb.hmin        = 1e-4;
  pb.hmax        = 1e-1;
  pb.ratio       = 3;
  pb.additional  = "-AbsError";
  field sigma_h, uh;
  for (size_t i = 0; true; i++) {
    pb.reset (omega, approx);
    pb.initial (sigma_h, uh);
    int status = pb.solve (sigma_h, uh);
    odiststream out (omega.name(), "field");
    pb.put (out, sigma_h, uh);
    if (i == n_adapt) break;
    space T0h (sigma_h.get_geo(), "P"+to_string(sigma_h.get_space().degree())+"d");
    field ch = lazy_interpolate (T0h, sqrt(abs(dot(sigma_h, grad(uh)))));
    omega = adapt (ch, pb);
    omega.save();
  }
}
