#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skiped)"
#exit 0

status=0

run "$SBINDIR/mkgeo_grid_2d -t 10 -v4 2>/dev/null | $BINDIR/geo -upgrade - > mesh-t.geo 2>/dev/null"

# geo   k       err_p_l2  	err_p_div_l2
L="
t	0	5e-2		7e-2
t	1	1e-3		3e-2
t	2	4e-5		4e-5
t	3	4e-7		8e-7
"

while test "$L" != ""; do
  e=`echo $L | gawk '{print $1}'`
  k=`echo $L | gawk '{print $2}'`
  err_p_l2=`echo $L | gawk '{print $3}'`
  err_p_div_l2=`echo $L | gawk '{print $4}'`
  L=`echo $L | gawk '{for (i=5; i <= NF; i++) print $i}'`
  loop_mpirun "./commute_rtd mesh-$e $k 2>/dev/null | ./commute_rtd_error $err_p_l2 $err_p_div_l2 >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done

run "rm -f mesh-t.geo"

exit $status
