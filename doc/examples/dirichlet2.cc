///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile dirichlet2.cc The Poisson problem with homogeneous boundary conditions -- variable right-hand-side
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod_laplace.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo omega (argv[1]);
  size_t d = omega.dimension();
  space Xh (omega, argv[2]);
  Xh.block ("boundary");
  trial u (Xh); test v (Xh);
  form a = lazy_integrate (dot(grad(u),grad(v)));
  field lh = lazy_integrate (f(d)*v);
  field uh (Xh);
  uh ["boundary"] = 0;
  problem p (a);
  p.solve (lh, uh);
  dout << catchmark("u") << uh;
}
