///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile zalesak_dg.cc The Zalesak slotted disk benchmark by the discontinuous Galerkin method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "zalesak_circle.h"
#include "bdf.icc"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  size_t n_max = (argc > 3) ?   atoi(argv[3]) : 1000;
  size_t strip = (argc > 4) ? string(argv[4]) == "true" : false;
  size_t p     = (argc > 5) ?   atoi(argv[5]) : min(Xh.degree()+1,bdf::pmax);
  Float tf = u::period(), delta_t = tf/n_max;
  trial phi (Xh); test xi (Xh);
  form m  = integrate (phi*xi),
       a0 = integrate (dot(u(),grad_h(phi))*xi)
          + integrate ("boundary", max(0, -dot(u(),normal()))*phi*xi)
          + integrate ("internal_sides", 
	 	- dot(u(),normal())*jump(phi)*average(xi)
                + 0.5*abs(dot(u(),normal()))*jump(phi)*jump(xi));
  problem pb;
  branch event ("t","phi");
  vector<field> phi_h (p+1);
  phi_h[0] = phi_h[1] = lazy_interpolate (Xh, phi0());
  dout << event (0, phi_h[0]); 
  for (size_t n = 1; n <= n_max; n++) {
    Float t = n*delta_t;
    if (n % 10 == 0) derr << "[" << n << "]";
    size_t pn = min(n,p);
    field rhs(Xh, 0);
    for (size_t i = 1; i <= pn; i++)
      rhs += (bdf::alpha[pn][i]/delta_t)*phi_h[i];
    field lh = integrate(rhs*xi) 
             + integrate("boundary", max(0,-dot(u(),normal()))*phi_exact(t)*xi);
    if (pn <= p) {
      form an = a0 + (bdf::alpha[pn][0]/delta_t)*m;
      pb = problem (an);
    }
    pb.solve (lh, phi_h[0]);
    check_macro (phi_h[0].max_abs() < 100, "BDF failed -- HINT: decrease delta_t");
    if (!strip || n == n_max) dout << event (t, phi_h[0]);
    for (size_t i = min(p,pn+1); i >= 1; i--)
      phi_h[i] = phi_h[i-1];
  }
  derr << endl;
}
