///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile elasticity_criterion.icc The elasticity problem -- adaptive mesh criterion
field elasticity_criterion (Float lambda, const field& uh) {
  string grad_approx = "P" + to_string(uh.get_space().degree()-1) + "d";
  space Xh  (uh.get_geo(), grad_approx);
  if (grad_approx == "P0d") return lazy_interpolate (Xh, norm(uh));
  space T0h  (uh.get_geo(), grad_approx);
  size_t d = uh.get_geo().dimension();
  tensor I = tensor::eye (d);
  return lazy_interpolate (T0h, sqrt(2*norm2(D(uh)) + lambda*sqr(div(uh))));
}
