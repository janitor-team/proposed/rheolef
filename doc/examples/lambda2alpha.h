///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile lambda2alpha.h The combustion problem -- inversion of the parameter function
#include "lambda_c.h"
Float lambda2alpha (Float lambda, bool up = false) {
  static const Float ac = alpha_c();
  Float tol = 1e2*numeric_limits<Float>::epsilon();
  size_t max_iter = 1000;
  Float a_min = up ?  ac : 0;
  Float a_max = up ? 100 : ac;
  for (size_t k = 0; abs(a_max - a_min) > tol; ++k) {
    Float a1      = (a_max + a_min)/2;
    Float lambda1 = 8*sqr(a1/cosh(a1));
    if ((up && lambda > lambda1) || (!up && lambda < lambda1))
          { a_max = a1; }
    else  { a_min = a1; }
    check_macro (k < max_iter, "lambda2alpha: max_iter=" << k
                       << " reached and err=" << a_max - a_min);
  }
  return(a_max + a_min)/2;
};
