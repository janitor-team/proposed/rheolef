#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
ROUNDER="$BINDIR/field - -field -round"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

tol="1e-7"

status=0
# --------------------------------------------------------------------
# do meshes
# --------------------------------------------------------------------
run "${SBINDIR}/mkgeo_grid_2d -v4 10 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-2d.geo 2>/dev/null"
if test $? -ne 0; then exit 1; fi

run "${SBINDIR}/mkgeo_grid_3d -v4 3 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-3d.geo 2>/dev/null"
if test $? -ne 0; then exit 1; fi

if false; then 
#TODO: stokes axi
run "${SBINDIR}/mkgeo_grid_2d -v4 10 -rz 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-rz.geo 2>/dev/null"
if test $? -ne 0; then exit 1; fi
fi
# --------------------------------------------------------------------
# run tests
# --------------------------------------------------------------------
# 2D
loop_mpirun "./stokes_cavity mesh-2d 2>/dev/null | \$RUN ./vorticity 2>/dev/null | $ROUNDER $tol 2>/dev/null | diff $SRCDIR/stokes-cavity-vorticity-mesh-2d.field - >/dev/null"
if test $? -ne 0; then status=1; fi

# 3D
loop_mpirun "./stokes_cavity mesh-3d 2>/dev/null | \$RUN ./vorticity 2>/dev/null | $ROUNDER $tol 2>/dev/null | bash ${TOP_SRCDIR}/main/tst/field_new2old.sh 3 mesh-3d P1d vector none | diff -Bw $SRCDIR/stokes-cavity-vorticity-mesh-3d.field - >/dev/null"
if test $? -ne 0; then status=1; fi
# --------------------------------------------------------------------
# clean
# --------------------------------------------------------------------
run "/bin/rm -f mesh-2d.geo mesh-3d.geo mesh-rz.geo"

exit $status
