///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile embankment.cc The elasticity problem for the embankment geometry
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "embankment.icc"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  geo  omega (argv[1]);
  space Xh = embankment_space (omega, argv[2]);
  Float lambda = (argc > 3) ? atof(argv[3]) : 1;
  size_t d = omega.dimension();
  point f (0,0,0);
  f[d-1] = -1;
  trial u (Xh); test v (Xh);
  form  a  = integrate (lambda*div(u)*div(v) + 2*ddot(D(u),D(v)));
  field lh = integrate (dot(f,v));
  field uh (Xh, 0);
  problem p (a);
  p.solve (lh, uh);
  dout << catchmark("inv_lambda") << 1/lambda << endl
       << catchmark("u")          << uh;
}
