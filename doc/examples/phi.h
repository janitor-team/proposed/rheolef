///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile phi.h The Mossolov problem -- the phi function
struct phi {
  phi (Float n1=2, Float c1=1, Float r1=0) : n(n1), c(c1), r(r1) {}
  Float operator() (const Float& x) const {
    if (x <= 0) return 0;
    if (n == 1) return x/(c+r);
    if (r == 0) return pow(x/c,1/n);
    Float y = x/(c+r);
    const Float tol = numeric_limits<Float>::epsilon();
    for (size_t i = 0; true; ++i) {
      Float ry = f(y)-x;
      Float dy = -ry/df_dy(y);
      if (fabs(ry) <= tol && fabs(dy) <= tol) break; 
      if (i >= max_iter) break;
      if (y+dy > 0) {
        y += dy; 
      } else {
        y /= 2;
        check_macro (1+y != y, "phi: machine precision problem");
      }
    }
    return y;
  }
  Float derivative (const Float& x) const {
    Float phi_x = operator()(x);
    return 1/(r + n*c*pow(phi_x,-1+n));
  }
protected:
  Float f(Float y) const     { return c*pow(y,n) + r*y; }
  Float df_dy(Float y) const { return n*c*pow(y,-1+n) + r; }
  Float n,c,r;
  static const size_t max_iter = 100;
};
