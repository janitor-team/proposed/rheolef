///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile combustion_newton.cc The combustion problem by the Newton method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "combustion.h"
int main(int argc, char**argv) { 
  environment rheolef (argc,argv);
  geo omega (argv[1]);
  Float eps = numeric_limits<Float>::epsilon();
  string approx   = (argc > 2) ?      argv[2]  : "P1";
  Float  lambda   = (argc > 3) ? atof(argv[3]) : 0.1;
  Float  tol      = (argc > 4) ? atof(argv[4]) : eps;
  size_t max_iter = (argc > 5) ? atoi(argv[5]) : 100;
  combustion F (omega, approx);
  F.set_parameter (lambda);
  field uh = F.initial ();
  Float residue = tol;
  size_t n_iter = max_iter;
  damped_newton (F, uh, residue, n_iter, &derr);
  F.put (dout, uh);
  return (residue <= sqrt(tol)) ? 0 : 1;
}
