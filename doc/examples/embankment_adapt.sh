#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${TOP_SRCDIR}/main/tst"
ROUNDER="../../main/bin/field -field -round"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0

L="
$GEODIR/carre-dom-v2
"

for fullgeo in $L; do
 geo=`basename $fullgeo`
 dir=`dirname $fullgeo`
 d=`grep dimension $fullgeo.geo | gawk '{print $2}'`
 for Pk in P1 P2 P3; do
   loop_mpirun "./embankment_adapt $fullgeo $Pk 0 0 2>/dev/null && RHEOPATH=.:${dir} $ROUNDER 1e-7 $geo.field.gz 2>/dev/null | bash ${TOP_SRCDIR}/main/tst/field_new2old.sh $d $geo $Pk vector none 2>/dev/null | diff -w -B $SRCDIR/embankment-$geo-$Pk.field - >/dev/null"
   if test $? -ne 0; then status=1; fi
 done
 run "rm -f $geo.field.gz"
done

exit $status
