///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile sinusprod_dirichlet.h The sinus product function -- right-hand-side and boundary condition for the Poisson problem
#include "sinusprod.h"
struct f {
  Float operator() (const point& x) const { return d*sqr(pi)*_u(x); }
  f(size_t d1) : d(d1), _u(d1), pi(acos(Float(-1))) {}
  size_t d;
  u_exact _u; Float pi;
};
struct g {
  Float operator() (const point& x) const { return _u(x); }
  g(size_t d) : _u(d) {}
  u_exact _u;
};
