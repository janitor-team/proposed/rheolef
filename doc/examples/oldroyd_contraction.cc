///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile oldroyd_contraction.cc The Oldroyd problem on the contraction benchmark
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "oldroyd_theta_scheme.h"
#include "oldroyd_contraction.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  cin >> noverbose;
  oldroyd_theta_scheme<oldroyd_contraction> pb;
  geo omega (argv[1]);
  Float We_incr     = (argc > 2) ? atof(argv[2]) : 0.1;
  Float We_max      = (argc > 3) ? atof(argv[3]) : 0.1;
  Float delta_t0    = (argc > 4) ? atof(argv[4]) : 0.005;
  string restart    = (argc > 5) ?      argv[5]  : "";
  pb.tol            = 1e-3;
  pb.max_iter       = 50000;
  pb.alpha          = 8./9;
  pb.a              = 1;
  pb.delta_t        = delta_t0;
  Float delta_t_min = 1e-5;
  Float We_incr_min = 1e-5;
  dout << catchmark("alpha") << pb.alpha << endl
       << catchmark("a")     << pb.a     << endl;
  branch even ("We", "tau", "u", "p");
  field tau_h, uh, ph;
  pb.initial (omega, tau_h, uh, ph, restart);
  dout << even (pb.We, tau_h, uh, ph);
  bool ok = true;
  do {
    if (ok) pb.We += We_incr;
    derr << "# We = " << pb.We << " delta_t = " << pb.delta_t << endl;
    field tau_h0 = tau_h, uh0 = uh, ph0 = ph;
    ok = pb.solve (tau_h, uh, ph);
    if (ok) {
      dout << even (pb.We, tau_h, uh, ph);
    } else {
      pb.delta_t /= 2;
      tau_h = tau_h0; uh = uh0; ph = ph0;
      if (pb.delta_t < delta_t_min) {
        derr << "# solve failed: decreases We_incr and retry..." << endl;
  	pb.delta_t = delta_t0;
	We_incr /= 2;
	pb.We -= We_incr;
        if (We_incr < We_incr_min) break;
      } else {
        derr << "# solve failed: decreases delta_t and retry..." << endl;
      }
    }
    derr << endl << endl;
  } while (true);
}
