///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
point u_exact   (const point& x) { return point(-x[1],x[0]); }
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-10;
  field uh, ph;
  din >> catchmark("u") >> uh
      >> catchmark("p") >> ph;
  const space& Xh = uh.get_space(),
               Qh = ph.get_space();
  const geo& omega = Xh.get_geo();
  size_t k = Xh.degree();
  Float meas_omega = integrate(omega);
  field pi_h_u = lazy_interpolate (Xh,u_exact),
        pi_h_p (Qh, 0.);
  trial u (Xh), p (Qh);
  test  v (Xh), q (Qh);
  form m  = integrate (dot(u,v)),
       a  = integrate (ddot(grad(u),grad(v)));
  integrate_option iopt;
  iopt.set_order (2*(k+2)+3);
  Float err_l2_u = sqrt(integrate (omega, norm2(uh-pi_h_u), iopt)/meas_omega);
  derr << "err_l2_u = " << err_l2_u << endl;
  return (err_l2_u < tol) ? 0 : 1;
}
