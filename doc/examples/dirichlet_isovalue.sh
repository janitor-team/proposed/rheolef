#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
# check isovalue extraction
#
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
GEODIR="${SRCDIR}"
ROUNDER="../../main/bin/field - -field -round"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

# BUG_VTK_FIELD_ISOVALUE
# Error: Could not import vtkCommonComputationalGeometry
echo "      not yet (skiped)"
exit 0
if test x"${QD_EXT}" != x""; then
  echo "      not yet (skiped when QD lib is active)"
  exit 0
fi

status=0

L="
$SRCDIR/square-10-bdry
$SRCDIR/cube-5-bdry
"

for fullgeo in $L; do
 geo=`basename $fullgeo`
 dir=`dirname $fullgeo`
 for Pk in P1; do
   loop_mpirun_raw "RHEOPATH=${GEODIR} \$RUN ./dirichlet $fullgeo $Pk 2>/dev/null | RHEOPATH=$GEODIR ../../main/bin/field - -isovalue 0.01 -field 2>/dev/null | RHEOPATH=$GEODIR ../../main/bin/geo -geo -round - 2>/dev/null | diff -w -B $SRCDIR/dirichlet-isovalue-$geo-$Pk.geo.valid - >/dev/null"
   if test $? -ne 0; then status=1; fi
 done
done

exit $status
