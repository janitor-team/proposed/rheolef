///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile navier_stokes_upw_dg.h The Navier-Stokes equations with the discontinuous Galerkin method and upwinding -- class header
#include "navier_stokes_dg.h"
struct navier_stokes_upw_dg: navier_stokes_dg {
  typedef Float                        float_type;
  typedef navier_stokes_dg::value_type value_type;
  navier_stokes_upw_dg (Float Re, const geo& omega, string approx);
  value_type residue     (const value_type& uh) const;
  void update_derivative (const value_type& uh) const;
};
#include "navier_stokes_upw_dg.icc"
