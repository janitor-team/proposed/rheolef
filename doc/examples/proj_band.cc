///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile proj_band.cc The banded level set method - projection on the surface
#include "rheolef.h"
using namespace std;
using namespace rheolef;
int main (int argc, char**argv) {
  environment rheolef (argc, argv);
  field phi_h;
  din >> catchmark("phi") >> phi_h;
  const space& Xh = phi_h.get_space();
  band gamma_h (phi_h);
  space Bh (gamma_h.band(), "P1");
  field uh(Bh);
  din >> catchmark("u") >> uh;
  space Wh (gamma_h.level_set(), "P1");
  gamma_h.level_set().save();
  dout << lazy_interpolate (Wh, uh);
}
