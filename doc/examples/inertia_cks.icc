///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile inertia_cks.icc The inertia term of the Navier-Stokes equation with the discontinuous Galerkin method -- Cockburn, Kanschat & Schotzau variant
form inertia (field w, trial u, test v,
  integrate_option iopt = integrate_option())
{
  return
    integrate (- dot(trans(grad_h(v))*w,u) - 0.5*div_h(v)*dot(u,w), iopt)
  + integrate ("internal_sides",
           dot(average(u),normal())*dot(jump(v),average(w))
     + 0.5*dot(jump(v),normal())
           *(dot(average(u),average(w)) + 0.25*dot(jump(u),jump(w))), iopt)
  + integrate ("boundary", 0.5*dot(v,normal())*dot(u,w), iopt);
}
field inertia_fix_rhs (test v, 
  integrate_option iopt = integrate_option())
{
  return integrate("boundary", -dot(g(),normal())*dot(g(),v), iopt);
}

