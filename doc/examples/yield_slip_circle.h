///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile yield_slip_circle.h The yield slip problem on a circle -- exact solution
struct u {
  Float operator() (const point& x) const { return (1-norm2(x))/4 + us; }
  u (Float S, Float n, Float Cf) : us(pow(max(Float(0),(0.5-S)/Cf), 1/n)) {}
  protected: Float us;
};
struct grad_u {
  point operator() (const point& x) const { return -x/2; }
  grad_u (Float S, Float n, Float Cf) {}
};
struct lambda {
  Float operator() (const point& x) const { return 1./2; }
  lambda (Float S, Float n, Float Cf) {}
};
