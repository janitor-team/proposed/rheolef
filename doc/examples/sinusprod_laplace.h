///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile sinusprod_laplace.h The sinus product function -- right-hand-side and boundary condition for the Poisson problem with Neumann boundary condition
#include "sinusprod.h"
struct f {
  Float operator() (const point& x) const { return d*sqr(pi)*_u(x); }
  f(size_t d1) : d(d1), _u(d1), pi(acos(Float(-1))) {}
  size_t d;
  u_exact _u; Float pi;
};
struct g {
  Float operator() (const point& x) const {
    switch (d) {
     case 0:  return 0;
     case 1:  return -pi;
     case 2:  return -pi*(sin(pi*x[0]) + sin(pi*x[1]));
     default: return -pi*( sin(pi*x[0])*sin(pi*x[1])
                         + sin(pi*x[1])*sin(pi*x[2])
                         + sin(pi*x[2])*sin(pi*x[0])); 
   }
  }
  g(size_t d1) : d(d1), pi(acos(Float(-1))) {}
  size_t d; Float pi;
};
