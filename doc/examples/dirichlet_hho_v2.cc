///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile dirichlet_hho.cc The Poisson problem by the hybrid high order method
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "sinusprod_dirichlet.h"
#include "diffusion_isotropic.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  string Pkd = (argc > 2) ? argv[2] : "P1d",
         Pld = (argc > 3) ? argv[3] :  Pkd;
  space  Xh (omega, Pld),
         Mh (omega["sides"], Pkd);
  Mh.block("boundary");
  size_t k = Xh.degree(), l = Mh.degree(), d = omega.dimension();
  Float beta = (argc > 4) ? atof(argv[4]) : 10*(k+1)*(k+d)/Float(d);
  check_macro(l == k-1 || l == k || l == k+1,
    "invalid (k,l) = ("<<k<<","<<l<<")");
  space Xhs(omega, "P"+to_string(k+1)+"d"),
        Zh (omega, "P0"),
        Mht(omega, "trace_n(RT"+to_string(k)+"d)");
  space Yh = Xh*Xhs*Xh*Mht*Xhs*Zh*Xh*Mht*Zh;
  trial x(Yh), lambda(Mh);
  test  y(Yh), mu(Mh);
  auto u = x[0], us = x[1], ut = x[2], deltat = x[3], vs = x[4];
  auto w = y[0], ws = y[1], wt = y[2],   phit = y[3], ys = y[4];
  auto zeta1 = x[5], vt = x[6], gammat = x[7], zeta2 = x[8];
  auto   xi1 = y[5], yt = y[6], kappat = y[7],   xi2 = y[8];
  integrate_option iopt;
  iopt.invert = true;
  auto gamma = pow(h_local(),2);
  auto a_expr =   dot(grad_h(us),A(d)*grad_h(ws))
                + dot(grad_h(us)-grad_h(u),A(d)*grad_h(ys))
                + dot(grad_h(ws)-grad_h(w),A(d)*grad_h(vs))
                + (us-u)*xi1 + (ws-w)*zeta1 - gamma*zeta1*xi1
                + (us-ut)*yt + (ws-wt)*vt
                + vs*xi2 + ys*zeta2 - gamma*zeta2*xi2
                + on_local_sides(
                    beta*pow(h_local(),-1.)*deltat*phit
                  + u*dot(A(d)*grad_h(ys),normal())
                  + w*dot(A(d)*grad_h(vs),normal())
                  + (u+us-ut-deltat)*kappat
                  + (w+ws-wt-phit  )*gammat);
  form a = integrate(omega, a_expr);
  form inv_a = integrate(omega, a_expr, iopt);
  form b = integrate(omega,on_local_sides(-mu*(
                  dot(A(d)*grad_h(vs),normal()) + gammat)));
  field lh = integrate (omega, f(d)*w);
  field lambda_h(Mh,0);
  form s = -b*inv_a*trans(b);
  s.set_symmetric_definite_positive();
  field rhs = -b*(inv_a*lh);
  problem p (s);
  p.solve (rhs, lambda_h);
  field xh = inv_a*(lh - b.trans_mult(lambda_h));
  dout << catchmark("beta")   << beta << endl
       << catchmark("u")      << xh[0]
       << catchmark("us")     << xh[1]
       << catchmark("ut")     << xh[2]
       << catchmark("delta")  << xh[3]
       << catchmark("vs")     << xh[4]
       << catchmark("zeta1")   << xh[5]
       << catchmark("vt")     << xh[6]
       << catchmark("gammat") << xh[7]
       << catchmark("zeta2")  << xh[8]
       << catchmark("lambda") << lambda_h;
}
