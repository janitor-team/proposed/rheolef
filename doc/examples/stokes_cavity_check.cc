///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "cavity.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-10;
  field uh;
warning_macro("get field...");
  din >> catchmark("u") >> uh;
warning_macro("get field done");
  const space& Xh = uh.get_space();
  const geo& omega = Xh.get_geo();
  space Qh (omega, "P1");
  field ph (Qh);
  din >> catchmark("p") >> ph;
  trial u (Xh), p (Qh); test v (Xh), q (Qh);
  form a  = integrate (2*ddot(D(u),D(v)));
  form b  = -integrate (div(u)*q);
  form mp = integrate (p*q);
  field m_ru = a*uh + b.trans_mult(ph);
  field m_rp = b*uh;
warning_macro("field_indirect...");
  m_ru["top"] = 0; m_ru["bottom"] = 0;
warning_macro("field_indirect done");
  if (Xh.get_geo().dimension() == 3) {
    m_ru[1]["left"] = 0; m_ru[1]["right"] = 0;
    m_ru["front"]   = 0; m_ru["back"]     = 0;
  } else {
    m_ru["left"] = 0; m_ru["right"] = 0;
  }
  field rp (Qh);
  problem pmp (mp);
  pmp.solve (m_rp, rp);
  Float res_u = m_ru.u().max_abs();
  Float res_p = sqrt(mp(rp,rp));
  Float res = max(res_u, res_p);
  Float p_constant = mp(ph, field(Qh,1.));
  derr << "check: residue(uh) = " << res_u << endl
       << "check: residue(ph) = " << res_p << endl
       << "check: residue     = " << res << endl
       << "m(p,1)             = " << p_constant << endl;
  return (res <= tol) ? 0 : 1;
}
