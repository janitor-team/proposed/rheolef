///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile burgers_diffusion_error.cc The diffusive Burgers equation -- error analysis
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "burgers_diffusion_exact.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  Float err_expected = (argc > 1) ? atof(argv[1]) : 1;
  Float epsilon;
  din >> catchmark("epsilon") >> epsilon;
  branch even("t","u");
  Float t=0; field uh;
  Float err_linf_l2 = 0,
        err_l2_l2 = 0,
        err_linf_linf = 0,
        meas_omega = 0;
  size_t n = 0;
  bool have_meas_omega = false;
  dout << "# t err_l2(t) err_linf(t)" << endl;
  while (din >> even(t,uh)) {
    const geo& omega = uh.get_geo();
    if (!have_meas_omega) {
      meas_omega = integrate(omega);
      have_meas_omega = true;
    }
    integrate_option iopt;
    iopt.set_order (2*uh.get_space().degree()+1);
    field pi_h_u = lazy_interpolate (uh.get_space(), u_exact(epsilon,t));
    Float err_linf = field(uh - pi_h_u).max_abs();
    Float err_l2 = sqrt(integrate (omega, sqr(uh - u_exact(epsilon,t)), iopt)/meas_omega);
    err_linf_linf = max(err_linf_linf, err_linf);
    err_linf_l2 = max(err_linf_l2, err_l2);
    err_l2_l2 += sqr(err_l2);
    dout << t << " " << err_l2 << " " << err_linf << endl;
    ++n;
  }
  err_l2_l2 = sqrt(err_l2_l2/n);
  dout << "# err_l2_l2     = " << err_l2_l2 << endl
       << "# err_linf_l2   = " << err_linf_l2 << endl
       << "# err_linf_linf = " << err_linf_linf << endl;
  return (err_linf_l2 <= err_expected) ? 0 : 1;
}
