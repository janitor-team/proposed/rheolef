///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile navier_stokes_taylor_error_dg.cc The Navier-Stokes equations for the Taylor benchmark -- error analysis
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "taylor_exact.h"
int main(int argc, char**argv) {
  environment rheolef(argc, argv);
  Float err_u_linf_expected = (argc > 1) ? atof(argv[1]) : 1e+38;
  Float err_p_linf_expected = (argc > 2) ? atof(argv[2]) : err_u_linf_expected;
  bool have_kinetic_energy  = (argc > 3);
  bool dump                 = (argc > 4);
  Float Re;
  field uh; 
  din >> catchmark("Re") >> Re 
      >> catchmark("u") >> uh;
  space Xh  = uh.get_space();
  size_t k  = Xh.degree();
  geo omega = Xh.get_geo();
  string approx = "P"+to_string(k)+"d";
  space Qh (omega, approx);
  field ph(Qh); 
  din >> catchmark("p") >> ph;
  size_t d = omega.dimension();
  integrate_option iopt;
  iopt.set_family(integrate_option::gauss);
  iopt.set_order(2*k+1);
#ifdef TODO
  Float p_moy = integrate (omega, ph, iopt);
  ph = ph-p_moy;
#else // TODO
  trial p (Qh); test q (Qh);
  form mp = integrate(p*q);
  Float p_moy = mp (ph, field(Qh,1));
  ph = ph-p_moy;
#endif // TODO
  string high_approx = "P"+to_string(k+1)+"d";
  space Xh1 (omega, high_approx, "vector"),
        Qh1 (omega, high_approx);
  field euh = lazy_interpolate (Xh1, uh-u_exact());
  field eph = lazy_interpolate (Qh1, ph-p_exact(Re,have_kinetic_energy));
  Float err_u_l2 = sqrt(integrate (omega, norm2(uh-u_exact()), iopt));
  Float err_u_linf = euh.max_abs();
  Float err_u_h1 = sqrt(integrate (omega, norm2(grad_h(euh)), iopt)
                    + integrate (omega.sides(), (1/h_local())*norm2(jump(euh)), iopt));
  Float err_p_l2 = sqrt(integrate (omega, sqr(ph-p_exact(Re,have_kinetic_energy)), iopt));
  Float err_p_linf = eph.max_abs();
  derr << "err_u_l2   = " << err_u_l2 << endl
       << "err_u_linf = " << err_u_linf << endl
       << "err_u_h1   = " << err_u_h1 << endl
       << "err_p_l2   = " << err_p_l2 << endl
       << "err_p_linf = " << err_p_linf << endl;
  if (dump) {
    dout << catchmark("uh")  << uh 
         << catchmark("u")   << lazy_interpolate (Xh, u_exact())
         << catchmark("eu") << euh 
         << catchmark("ph")  << ph 
         << catchmark("p")   << lazy_interpolate (Qh, p_exact(Re,have_kinetic_energy))
         << catchmark("ep") << eph;
  }
  return ((err_u_linf <= err_u_linf_expected) && (err_p_linf <= err_p_linf_expected)) ? 0 : 1;
}
