///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef.h"
using namespace rheolef;
using namespace std;
Float negative (Float x) {
  return (x < -1e5*numeric_limits<Float>::epsilon()) ? 1 : 0; }
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]),
        Mh (omega["sides"], argv[2]);
  Float sigma = (argc > 4) ? atof(argv[4]) : 3;
  point u (1,0,0);
  trial phi(Xh), lambda(Mh);
  test  psi(Xh), mu(Mh);
  integrate_option iopt;
  iopt.invert = true;
  form inv_a = integrate (phi*(sigma*psi - dot(u,grad_h(psi)))
		+ on_local_sides(abs(dot(u,normal()))*phi*psi), iopt);
  form b1 = integrate ("internal_sides", (dot(u,normal())*jump(phi)
               - 2*abs(dot(u,normal()))*average(phi))*mu)
          - integrate ("boundary", 2*max(0, -dot(u,normal()))*phi*mu);
  form b2 = integrate ("internal_sides", average(phi)*mu)
          + integrate ("boundary",
	      (1-compose(negative,dot(u,normal())))*phi*mu);
  form c  = integrate ("sides", lambda*mu);
  field lh(Xh, 0);
  field kh = integrate("boundary", -compose(negative,dot(u,normal()))*mu);
  form s = c + b2*inv_a*trans(b1);
  field rh = b2*(inv_a*lh) - kh;
  field lambda_h(Mh);
  problem ps (s);
  ps.solve (rh, lambda_h);
  field phi_h = inv_a*(lh - b1.trans_mult(lambda_h));
  dout << catchmark("sigma")  << sigma << endl
       << catchmark("phi")    << phi_h
       << catchmark("lambda") << lambda_h;
#ifdef TO_CLEAN
  form a = integrate (phi*(sigma*psi - dot(u,grad_h(psi)))
		+ on_local_sides(abs(dot(u,normal()))*phi*psi));
  odiststream out ("aa","m",io::nogz);
  out << matlab
      << setbasename("a") << a.uu()
      << setbasename("inv_a") << inv_a.uu()
      << setbasename("b1") << b1.uu()
      << setbasename("b2") << b2.uu()
      << setbasename("c") << c.uu()
      << setbasename("s") << s.uu()
      << "lh=" << lh.u() << endl
      << "kh=" << kh.u() << endl
      << "rh=" << rh.u() << endl
      << "eig_a=eig(a)" << endl
      << "eig_c=eig(c)" << endl
      << "eig_s=eig(s)" << endl
      ;
#endif // TO_CLEAN
}
