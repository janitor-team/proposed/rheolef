#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/doc/examples"}
SBINDIR=${SBINDIR-"../../main/sbin"}
BINDIR=${BINDIR-"../../main/bin"}
NPROC_MAX=${NPROC_MAX-"1"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

status=0

# mesh          approx  err_l2
L="
mesh-2d-t       P1d      0.006
mesh-2d-t       P2d      0.002
mesh-2d-t       P3d      0.0003
"

run "${SBINDIR}/mkgeo_grid_2d -v4 -t 10 -a -0.5 -b 0.5 -c -0.5 -d 0.5 -boundary  2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-2d-t.geo 2>/dev/null"
if test $? -ne 0; then status=1; fi

while test "$L" != ""; do
  geo=`echo $L | gawk '{print $1}'`
  approx=`echo $L | gawk '{print $2}'`
  err_l2=`echo $L | gawk '{print $3}'`
  L=`echo $L | gawk '{for (i=4; i <= NF; i++) print $i}'`
  loop_mpirun "./diffusion_transport_tensor_dg $geo $approx 2>/dev/null | \$RUN ./diffusion_transport_tensor_error_dg $err_l2 >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1; fi
done

run "rm -f mesh-2d-t.geo"

exit $status
