///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile mosolov_residue.cc The Mossolov problem -- residue analysis
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "vector_projection.h"
int main(int argc, char**argv) {
  environment rheolef(argc,argv);
  Float tol = (argc > 1) ? atof(argv[1]) : 1e-12;
  Float Bi, n, r;
  field uh, sigma_h;
  din >> catchmark("Bi")    >> Bi
      >> catchmark("n")     >> n
      >> catchmark("r")     >> r
      >> catchmark("sigma") >> sigma_h
      >> catchmark("u")     >> uh;
  space Th = sigma_h.get_space();
  space Xh = uh.get_space();
  trial u (Xh), sigma (Th);
  test  v (Xh), tau   (Th);
  form m  = integrate(u*v);
  form a  = integrate(dot(grad(u),grad(v)));
  form b  = integrate(dot(grad(u),tau));
  form mt = integrate(dot(sigma,tau));
  integrate_option iopt;
  iopt.invert = true;
  form inv_mt = integrate(dot(sigma,tau), iopt);
  field lh = integrate(2*v);
  field grad_uh = inv_mt*(b*uh);
  auto c = compose (vector_projection(Bi,n), norm(sigma_h));
  field r_sigma_h = lazy_interpolate(Th, grad_uh - c*sigma_h);
  field mr_uh = lh - b.trans_mult(sigma_h-r*grad_uh) - r*(a*uh);
  mr_uh["boundary"] = 0;
  field r_uh (Xh);
  problem pm (m);
  pm.solve (mr_uh, r_uh);
  Float residue_uh      = sqrt(m(r_uh,r_uh));
  Float residue_sigma_h = sqrt(mt(r_sigma_h,r_sigma_h));
  derr << "norm_linf residue(uh)      = " << r_uh.max_abs() << endl
       << "norm_l2   residue(uh)      = " << residue_uh << endl
       << "norm_linf residue(sigma_h) = " << r_sigma_h.max_abs() << endl
       << "norm_l2   residue(sigma_h) = " << residue_sigma_h << endl;
  Float residue = max(residue_uh, residue_sigma_h);
  return (residue <= tol) ? 0 : 1;
}
