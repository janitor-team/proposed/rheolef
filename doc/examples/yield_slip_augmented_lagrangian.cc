///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile yield_slip_augmented_lagrangian.cc The yield slip problem by the augmented Lagrangian method
#include "rheolef.h"
using namespace std;
using namespace rheolef;
#include "yield_slip_augmented_lagrangian.icc"
#include "poisson_robin.icc"
int main(int argc, char**argv) {
  environment rheolef (argc,argv);
  dlog << noverbose;
  geo omega (argv[1]);
  string approx   = (argc > 2) ?      argv[2]  : "P1";
  Float  S        = (argc > 3) ? atof(argv[3]) : 0.6;
  Float  n        = (argc > 4) ? atof(argv[4]) : 1;
  Float  Cf       = (argc > 5) ? atof(argv[5]) : 1;
  Float  r        = (argc > 6) ? atof(argv[6]) : 1;
  Float  tol      = 1e3*numeric_limits<Float>::epsilon();
  size_t max_iter = 100000;
  space Xh (omega, approx);
  test  v (Xh);
  field lh = integrate(v);
  field uh = poisson_robin (Cf, omega["boundary"], lh);
  space Wh (omega["boundary"], Xh.get_approx());
  field lambda_h = Cf*uh["boundary"];
  int status = yield_slip_augmented_lagrangian(S, n, Cf, omega["boundary"],
		lh, lambda_h, uh, tol, max_iter, r);
  dout << setprecision(numeric_limits<Float>::digits10)
       << catchmark("S")  << S  << endl
       << catchmark("n")  << n  << endl
       << catchmark("Cf") << Cf << endl
       << catchmark("r")  << r  << endl
       << catchmark("u")  << uh
       << catchmark("lambda") << lambda_h;
  return status;
}
