///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile p_laplacian_circle.h The p-Laplacian problem on a circular geometry -- exact solution
struct u_exact {
  Float operator() (const point& x) const {
    return (1 - pow(norm2(x), p/(2*p-2)))/((p/(p-1))*pow(2.,1/(p-1)));
  }
  u_exact (Float q) : p(q) {}
  protected: Float p;
};
struct grad_u {
  point operator() (const point& x) const {
    return - (pow(norm2(x), p/(2*p-2) - 1)/pow(2.,1/(p-1)))*x;
  }
  grad_u (Float q) : p(q) {}
  protected: Float p;
};
