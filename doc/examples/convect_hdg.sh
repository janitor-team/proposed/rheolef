#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR="${TOP_SRCDIR}/doc/examples"
GEODIR="${TOP_SRCDIR}/main/tst"
SBINDIR="../../main/sbin"
BINDIR="../../main/bin"
NPROC_MAX=${NPROC_MAX-"8"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skipped)"
#exit 0

status=0

run "${SBINDIR}/mkgeo_grid_1d 500 -a -1 -b 1 -v4 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-1d.geo 2>/dev/null"
run "${SBINDIR}/mkgeo_grid_2d  80 -a -1 -b 1 -c -1 -d 1 -v4 2>/dev/null | ${BINDIR}/geo -upgrade - > mesh-2d.geo 2>/dev/null"

#	P1d	P2d	P3d / 1d 2d
L="	2e-5	3e-8	5e-11
	2e-3	2e-5	4e-7
"

for d in 1 2; do
 for k in 1 2 3; do
  tol=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  loop_mpirun "./convect_hdg mesh-${d}d.geo P${k}d > tmp.field 2>/dev/null && \$RUN ./convect_hdg_error $tol < tmp.field >/dev/null 2>/dev/null"
  if test $? -ne 0; then status=1;  fi
 done
done

run "/bin/rm -f mesh-1d.geo mesh-2d.geo mesh-3d.geo tmp.field"

exit $status
