///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile harten_show.cc The Burgers problem: the Harten exact solution -- visualization
#include "rheolef.h"
using namespace rheolef;
using namespace std;
#include "harten.h"
int main(int argc, char**argv) {
  environment rheolef (argc, argv);
  geo omega (argv[1]);
  space Xh (omega, argv[2]);
  size_t nmax = (argc > 3) ? atoi(argv[3]) : 1000;
  Float tf    = (argc > 4) ? atof(argv[4]) : 2.5;
  Float a     = (argc > 5) ? atof(argv[5]) : 1;
  Float b     = (argc > 6) ? atof(argv[6]) : 0.5;
  Float c     = (argc > 7) ? atof(argv[7]) : 0;
  branch even("t","u");
  for (size_t n = 0; n <= nmax; ++n) {
    Float t = n*tf/nmax;
    field pi_h_u = lazy_interpolate (Xh, harten(t,a,b,c));
    dout << even(t,pi_h_u);
  }
}
