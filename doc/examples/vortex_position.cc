///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile vortex_position.cc The stream function minima and its position
#include "rheolef.h"
using namespace rheolef;
int main (int argc, char** argv) {
  environment rheolef (argc, argv);
  check_macro (communicator().size() == 1, "please, use sequentially");
  field psi_h;
  din >> psi_h; 
  size_t idof_min = 0;
  Float psi_min = std::numeric_limits<Float>::max();
  for (size_t idof = 0, ndof = psi_h.ndof(); idof < ndof; idof++) {
    if (psi_h.dof(idof) >= psi_min) continue;
    psi_min = psi_h.dof(idof);
    idof_min = idof;
  }
  const disarray<point>& xdof = psi_h.get_space().get_xdofs();
  point xmin = xdof [idof_min];
  dout << "xc\t\tyc\t\tpsi" << std::endl
       << xmin[0] << "\t" << xmin[1] << "\t" << psi_min << std::endl;
}
