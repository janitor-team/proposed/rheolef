///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//! @examplefile burgers_diffusion_exact.h The diffusive Burgers equation -- its exact solution
struct u_exact {
  Float operator() (const point& x) const { 
    return 1 - tanh((x[0]-x0-t)/(2*epsilon)); }
  u_exact (Float e1, Float t1=0) : epsilon(e1), t(t1), x0(-0.5) {}
  Float M() const { return 0; }
  Float epsilon, t, x0;
};
using u_init = u_exact;
using g = u_exact;
