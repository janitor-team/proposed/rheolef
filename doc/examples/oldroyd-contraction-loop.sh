#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
name=${1-"contraction-small3-zr"}
We=0
np=8
delta_We=0.01
delta_t=0.005
max_iter=50000
tol=1e-3

i=0
status=0
if test $We = "0"; then
  echo "# ----------------------"
  echo "# We=$We i=$i"
  echo "# ----------------------"
  command="oldroyd_contraction $name $We > $name-We-0.field"
  echo "# $command"
  eval   "$command"
  status=$?
fi
while true; do
  #echo "# begin loop: oldroyd_contraction status=$status"
  if test $status -eq 0; then
    #echo "# status ok => incr We..."
    We_prec=$We
    We=`echo $We | gawk -v delta_We=$delta_We '{print $1 + delta_We }'`
  else
    echo "# oldroyd_contraction exits with status=$status"
    delta_t=`echo $delta_t | gawk '{print 0.5*$1 }'`
    echo "# decreasing delta_t=$delta_t and restart"
    # TODO: stop when delta_t too small
  fi
  i=`expr $i + 1`
  echo ; echo
  echo "# ----------------------"
  echo "# We=$We i=$i"
  echo "# ----------------------"
  command="mpirun -np $np oldroyd_contraction $name $We $delta_t $max_iter $tol $name-We-${We_prec}.field > $name-We-${We}.field"
  echo "# $command"
  eval   "$command"
  status=$?
  #echo "# end loop: oldroyd_contraction status=$status"
done
