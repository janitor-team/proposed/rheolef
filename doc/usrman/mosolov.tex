%\chapter{Flow in a pipe section}
% ---------------------------------- 
\subsection{Problem statement}
% ---------------------------------- 
\pbindex{Mosolov}%
\cindex{geometry!pipe}%
  Viscoplastic fluids develops an yield stress behavior
  (see e.g. \citealp{Sar-2016-cfma,SarWac-2017}).
  \citet{MosMia-1965,MosMia-1966,MosMia-1967}
  first investigated the flow of a viscoplastic in a pipe
  with an arbitrarily cross section.
  Its numerical investigation by augmented Lagrangian methods
  was first performed by \citet{SarRoq-2001,RoqSar-2008}.
  The Mosolov problem \citep{SarRoq-2001} writes:\\
  \mbox{} \ \ $(P)$: find $\boldsymbol{\sigma}$ and $u$, defined in $\Omega$, such that
  \begin{subequations}
  \begin{eqnarray}
      {\rm div}\,\boldsymbol{\sigma}
	=
	- f
        \ \mbox{ in } \Omega
	\phantom{aaaaaaaaaaaaaaaaaaaaaa}
   	\label{eq-mosolov-1}
 	\\
      u = 0
        \ \mbox{ on } \partial\Omega
	\phantom{aaaaaaaaaaaaaaaaaaaaaa}
 	\\
      \left.\begin{array}{ll}
       |\boldsymbol{\sigma}| \leq Bi
	 & \mbox{ when } \bnabla u = 0
   	\label{eq-mosolov-2}
	 \\
       \boldsymbol{\sigma} = |\bnabla u|^{-1+n}\bnabla u
	 + Bi\Frac{\bnabla u}{|\bnabla u|}
	 & \mbox{ otherwise }
     \end{array}\right\}
        \ \mbox{ in } \Omega
   	\label{eq-mosolov-3}
  \end{eqnarray}
  \end{subequations}
\pbindex{Bingham}%
  where $Bi\geq 0$ is the Bingham number and $n>0$ is a power-law index.
  The computational domain $\Omega$ represents the cross-section
  of the pipe.
\pbindex{Herschel-Bulkley}%
  In the bidimensional case $d=2$ and when $f$ is constant, this problem
  describes the stationary flow of an Herschel-Bulkley fluid in a 
  general pipe section $\Omega$.
  Let $Ox_2$ be the axis of the pipe and $Ox_0x_1$ the plane of the
  section $\Omega$.
  The vector-valued field $\boldsymbol{\sigma}$ represents
  the shear stress components $(\sigma_{0,1},\sigma_{0,2})$
  while $u$ is the axial component of velocity along $Ox_3$.
  When $Bi=0$, the problem reduces to the nonlinear $p$-Laplacian problem.
  When $n=1$ the fluid is a Bingham fluid.
  When $n=1$ and $Bi=0$, the problem reduces to the linear Poisson
  one with $\boldsymbol{\sigma}=\bnabla u$.

% ------------------------------------------
\subsection{The augmented Lagrangian algorithm}
% ------------------------------------------
\cindex{method!augmented Lagrangian}%
  This problem writes as a minimization of an energy: \\
  \begin{subequations}
  \begin{equation}
      u
 	=
      \argmin_{v\in W^{1,p}_0(\Omega)}
	J(v)
    \label{eq-mosolov-pb-min-1}
  \end{equation}
  where
  \begin{equation}
      J(v)
	=
      \Frac{1}{1+n} 
      \int_\Omega
  	|\bnabla v|^{1+n}
  	\, {\rm d}x
      +
      Bi
      \int_\Omega
  	|\bnabla v|
  	\, {\rm d}x
      -
      \int_\Omega
  	f\, v
  	\, {\rm d}x
    \label{eq-mosolov-pb-min-2}
  \end{equation}
  \end{subequations}
  This problem is solved by using
  an augmented Lagrangian algorithm.
  The auxiliary variable 
  $\boldsymbol{\gamma}=\bnabla u$
  is introduced together with the Lagrangian multiplier
  $\boldsymbol{\sigma}$ associated to the constraint
  $ \bnabla u-\boldsymbol{\gamma} =0$.
  For all $r >0$, let:
  $$
      L((v,\boldsymbol{\gamma});\boldsymbol{\sigma})
	=
      \Frac{1}{1+n} 
      \int_\Omega
  	|\boldsymbol{\gamma}|^{1+n}
  	\, {\rm d}x
      +
      Bi
      \int_\Omega
  	|\boldsymbol{\gamma}|
  	\, {\rm d}x
      -
      \int_\Omega
  	f\, v
  	\, {\rm d}x
      +
      \int_\Omega
        \boldsymbol{\sigma}
        .(\bnabla u-\boldsymbol{\gamma})
  	\, {\rm d}x
      +
      \Frac{r}{2} 
      \int_\Omega
        |\bnabla u-\boldsymbol{\gamma}|^2
  	\, {\rm d}x
  $$
  An Uzawa-like minimization algorithm writes:\\
  \begin{itemize}
  \item $k=0$: let $\lambda^{(0)}$ and $\gamma^{(0)}$ arbitrarily chosen.
  \item $k\geq 0$: let $\lambda^{(k)}$ and $\gamma^{(k)}$ being known.
  \begin{eqnarray*}
    u^{(k+1)} &:=& \argmin_{v\in W^{1,p}(\Omega)}
		 L((v,\boldsymbol{\gamma}^{(k)});\boldsymbol{\sigma}^{(k)})
	\\
    \boldsymbol{\gamma}^{(k+1)} &:=& \argmin_{\boldsymbol{\delta}\in L^{2}(\Omega)^d}
		L((u^{(k+1)},\boldsymbol{\delta});\boldsymbol{\sigma}^{(k)})
	\\
     \boldsymbol{\sigma}^{(k+1)}
	&:=&
        \boldsymbol{\sigma}^{(k)}
	+ \rho \left( \bnabla u^{(k+1)} - \boldsymbol{\gamma}^{(k+1)} \right)
        \ \mbox{ in } \Omega
  \end{eqnarray*}
  \end{itemize}
  The descent step $\rho$ is chosen as $\rho=r$
  for sufficiently large $r$.
  The Lagrangian $L$ is quadratic in $u$ and thus the computation
  of $u^{(k+1)}$ reduces to a linear problem.
  The non-linearity is treated when computing $\boldsymbol{\gamma}^{(k+1)}$.
  This operation is performed point-by-point in $\Omega$
  by minimizing:
  \[
	\boldsymbol{\gamma} := \argmin_{\boldsymbol{\delta}\in\mathbb{R}^d}
		\Frac{|\boldsymbol{\delta}|^{1+n}}{1+n}
		+ \Frac{r|\boldsymbol{\delta}|^{2}}{2}
		+ Bi|\boldsymbol{\delta}|
		- \boldsymbol{\xi}.\boldsymbol{\delta}
  \]
  where $\boldsymbol{\xi}=\boldsymbol{\sigma}^{(k)}+r\,\bnabla u^{(k+1)}$ is given.
  This problem is convex and its solution is unique.
  The solution has the form:
  \begin{equation}
     \boldsymbol{\gamma}
	=
	P_{n,r}(\boldsymbol{\xi})
	\eqdef
	\left\{ \begin{array}{cl}
          0
	    & \mbox{ when } |\boldsymbol{\xi}| \leq S \\
	 \phi_{n,r}(|\boldsymbol{\xi}|-S)
	   \Frac{\boldsymbol{\xi}}{|\boldsymbol{\xi}|}
	    & \mbox{ otherwise}
	\end{array}\right.
	\label{eq-mosolov-projection}
  \end{equation}
  where $\phi_{n,r}(x)=f^{-1}_{n,r}(x)$
  has been introduced in~\eqref{eq-def-proj}
  page~\pageref{eq-def-proj} in the
  context of the yield slip problem together with the
  scalar projector.

% -----------------------------------
\myexamplelicense{vector_projection.h}
% -----------------------------------

  Finally, the Uzawa-like minimization algorithm \citep{SarRoq-2001} writes:\\
  \begin{itemize}
  \item $k=0$: let $\boldsymbol{\sigma}^{(0)}$ and $\boldsymbol{\gamma}^{(0)}$ arbitrarily chosen.
  \item $k\geq 0$: let $\boldsymbol{\sigma}^{(k)}$ and $\boldsymbol{\gamma}^{(k)}$ being known, find $u^{(k+1)}$ such that
  \begin{eqnarray*}
      -r \Delta u^{(k+1)}
	&=&
	f
	+
	{\rm div}
	\left(
		\boldsymbol{\sigma}^{(k)} - r \boldsymbol{\gamma}^{(k)} 
	\right)
        \ \mbox{ in } \Omega \\
      u^{(k+1)}&=& 0
        \ \mbox{ on } \partial\Omega
  \end{eqnarray*}
  and then compute explicitly $\boldsymbol{\gamma}^{(k+1)}$
  and $\boldsymbol{\sigma}^{(k+1)}$:
  \begin{eqnarray}
    \boldsymbol{\gamma}^{(k+1)}
	&:=&
        P_{n,r}
        \left(
            \boldsymbol{\sigma}^{(k)}+r\bnabla u^{(k+1)}
        \right)
	\label{eq-mosolov-al-projection}
	\\
     \boldsymbol{\sigma}^{(k+1)}
	&:=&
        \boldsymbol{\sigma}^{(k)}
	+ r \left( \bnabla u^{(k+1)} - \boldsymbol{\gamma}^{(k+1)} \right)
	\nonumber
  \end{eqnarray}
  \end{itemize}
Here $r>0$ is a numerical parameter.
This algorithm reduces the nonlinear problem to a sequence of
linear and completely standard Poisson problems and some explicit
computations.
For convenience, this algorithm is implemented as the \code{solve}
function member of a class:

% ----------------------------------------------
\myexamplelicense{mosolov_augmented_lagrangian1.icc}
% ----------------------------------------------

For convenience, the order of the update of the three variables
$u$, $\boldsymbol{\gamma}$ and $\boldsymbol{\sigma}$ has been rotated:
by this way, the algorithm starts with initial values for
$u$ and $\boldsymbol{\sigma}$.
instead of $\boldsymbol{\gamma}$ and $\boldsymbol{\sigma}$.
Observe that the projection step~\eqref{eq-mosolov-al-projection}
is implemented by using the {\tt interpolate} operator:
this projection step interprets as point-wise at Lagrange nodes
instead as a numerical resolution of the element-wise minimization problem.
Note that, for the lowest order $k=1$, these two approaches are 
strictly equivalent, while, when $k\geq 2$, the numerical solution
obtained by this algorithm
is no more solution of the discrete version of the saddle-point problem
for the Lagrangian $L$.
Nevertheless, the numerical solution is founded to converge to the
exact solution of the initial problem \eqref{eq-mosolov-1}-\eqref{eq-mosolov-3}:
this will be checked here in a forthcoming section, dedicated to
the error analysis.
Observe also that the stopping criterion for breaking the loop
bases on the max of the relative error for the $\boldsymbol{\sigma}_h$ variable.
For this algorithm, this stopping criterion guaranties that all
residual terms of the initial problem are also converging to zero,
as it will be checked here.
Moreover, this stopping criterion is very fast to compute while
the full set of residual terms of the initial problem would take
more computational time inside the loop.

The class declaration contains all model parameters, loop controls,
form and space variables, together with some pre- and
post-treatments:

% ----------------------------------------------
\myexamplelicense{mosolov_augmented_lagrangian.h}

\myexamplelicense{mosolov_augmented_lagrangian2.icc}

\myexamplelicense{mosolov_augmented_lagrangian.cc}
% ----------------------------------------------

The main program read parameters from the command 
line and performs an optional mesh adaptation loop.
This implementation supports any $n>0$,
any continuous piecewise polynomial $P_k$, $k\geq 1$
and also isoparametric approximations for curved boundaries.

% ------------------------------
\subsubsection*{Running the program}
% ------------------------------
  \begin{figure}[htb]
    \begin{center}
      \begin{tabular}{cc}
        \includegraphics[height=7cm]{mosolov-square-30-Bi-0,4-n-1.png} &
        \includegraphics[height=7cm]{mosolov-square-Bi-0,4-cut-bisector.pdf}
      \end{tabular}
    \end{center}
    \caption{The augmented Lagrangian algorithm on the Mosolov
	problem with $Bi=0.4$ and $n=1$:
	(left) the velocity field in elevation view ($h=1/30$);
	(right) velocity cut along the first bisector for various $h$.
    }
    \label{fig-mosolov-u}
  \end{figure}
  Compile the program as usual:
\begin{verbatim}
  make mosolov_augmented_lagrangian
  mkgeo_grid -a -1 -b 1 -c -1 -d 1 -t 10 > square.geo
  ./mosolov_augmented_lagrangian square.geo P1 0.4 1
  field -mark u square.field -elevation
\end{verbatim}
Observe on Fig.~\ref{fig-mosolov-u}.left the central region where the velocity is constant.
A cut of the velocity field along the first bisector is obtained by:
\begin{verbatim}
  field -mark u square.field -cut -origin 0 0 -normal 1 1 -gnuplot
\end{verbatim}
Observe on Fig.~\ref{fig-mosolov-u}.right
the small regions with zero velocity, near the outer corner of the square pipe section.
This region is really small but exists
% It is difficult to obtain its reliable
% prediction without a massive mesh refinement,
% at the price of very long computational time.
This question will be revisited in the next section
dedicated to auto-adaptive mesh refinement.

\begin{figure}[htb]
    \begin{center}
      \includegraphics[height=7cm]{mosolov-augmented-lagrangian-square-P1-Bi-0,4-cvg.pdf}
    \end{center}
   \caption{The augmented Lagrangian algorithm on the Mosolov
	problem with $Bi=0.4$ and $n=1$:
 	residue versus iteration $k$ for various $h$.
   }
   \label{fig-mosolov-convergence}
\end{figure}
% -------------------------------------
\myexamplenoinput{mosolov_residue.cc}
% -------------------------------------
The file \file{mosolov_residue.cc} implement the computation of the full set 
of residual terms of the initial problem.
This file it is not listed here but is available in the \Rheolef\  example directory.
The computation of residual terms is obtained by:
\begin{verbatim}
  make mosolov_residue
  zcat square.field.gz | ./mosolov_residue
\end{verbatim}
Observe that the residual terms of problem~\eqref{eq-mosolov-1}-\eqref{eq-mosolov-3}
are of about $10^{-10}$, as required by the stopping criterion.
Fig.~\ref{fig-mosolov-convergence} plots the max of the relative error 
for the $\boldsymbol{\sigma}_h$ variable: this quantity is used as stopping criterion.
Observe that it behaves asymptotically as $1/k$ for large iteration number $k$.
\cindex{method!newton}%
Note that these convergence properties could be dramatically improved
by using a Newton method, as shown by \citet{Sar-2016}.
% This implementation will be presented in the \Rheolef\  documentation
% in a future release.

\cindex{distributed computation}%
Finally, computation can be performed for any $n>0$, any polynomial order $k\geq 1$
and in a distributed environment for enhancing performances on larger meshes:
\begin{verbatim}
  mkgeo_grid -a -1 -b 1 -c -1 -d 1 -t 40 > square-40.geo
  mpirun -np 8 ./mosolov_augmented_lagrangian square-40.geo P2 0.4 0.5
  field -mark u square-40.field -elevation
\end{verbatim}
\pindex{mpirun}%
The computation could take about ten minutes.
The \code{mpirun -np 8} prefix is optional and you should omit it
if you are running a sequential installation of \Rheolef.

% ------------------------------
\subsection{Mesh adaptation}
% ------------------------------
\begin{figure}[htb]
  \begin{center}
  \begin{tabular}{cc}
    \includegraphics[height=8.0cm]{{mosolov-sector-P2-Bi-0.5-n-1}.png} &
    \includegraphics[height=8.0cm]{{mosolov-sector-P2-Bi-0.5-n-1-zoom}.png} \\
    \includegraphics[height=8.0cm]{{mosolov-sector-P2-Bi-0-5-n-1-cut-bisector}.pdf}
  \end{tabular}
  \end{center}
  \caption{Auto-adaptive meshes for the Mosolov problem with $Bi=0.5$ and $n=1$
	and the $P_2$ element for the velocity:
 	(top) Yielded regions in gray and nine isovalues of the velocity
	       inside $]0,u_{\rm max}[$ with $u_{\rm max}=0.169865455242435$.
 	       The auto-adaptive mesh is mirrored.
 	(top-right) Zoom in the outer corner of the square section.
	(bottom) Velocity cut along the first bisector.
  }
  \label{fig-mosolov-adapt}
\end{figure}

\cindex{mesh!adaptation}%
An important improvement can be obtained by using mesh adaptation,
as shown in \citep{SarRoq-2001,RoqSar-2003,RoqSar-2008}:
with a well chosen criterion, rigid regions, where the velocity is constant,
can be accurately determined with reasonable mesh sizes.
This is especially true for obtaining an accurate determination of the shape
of the small regions with zero velocity in the outer corner
of the pipe section.
In order to reduce the computational time, we can reduce
the pipe section flow to only a sector, thanks to symmetries
(see Fig.~\ref{fig-mosolov-adapt}):
\pindex{mkgeo_sector}%
\begin{verbatim}
  mkgeo_sector
  geo sector.geo
\end{verbatim}
Then, the computation is run by indicating an adaptation
loop with ten successive meshes:
\begin{verbatim}
  make mosolov_augmented_lagrangian
  mpirun -np 8 ./mosolov_augmented_lagrangian sector P2 0.5 1 10
\end{verbatim}
\pindex{mpirun}%
This computation could take about one hour.
The \code{mpirun -np 8} prefix is optional and you should omit it
if you are running a sequential installation of \Rheolef.

\begin{figure}[htb]
  \begin{center}
  \begin{tabular}{cc}
    \includegraphics[height=6.0cm]{{mosolov-sector-P2-Bi-0-5-n-1-geo-adapt-loop-size}.pdf} &
    \includegraphics[height=6.0cm]{{mosolov-sector-P2-Bi-0-5-n-1-geo-adapt-loop-hmin}.pdf}
  \end{tabular}
  \end{center}
  \caption{Auto-adaptive meshes for the Mosolov problem:
	evolution of the mesh size (left) and the minimal edge length
	during the adaptation loop.
  }
  \label{fig-mosolov-adapt-loop}
\end{figure}
Fig.~\ref{fig-mosolov-adapt-loop} shows the evolution of the
mesh size and the minimal edge length during the adaptation loop:
observe the convergence of the meshes to an optimal one.

% -----------------------------------
\myexamplelicense{mosolov_yield_surface.cc}
% -----------------------------------
\cindex{interface!yield surface}%
The yield surface is
the zero isosurface of the level set function
\mbox{$\phi(\boldsymbol{x}=| \boldsymbol{\sigma}_h(\boldsymbol{x}| - Bi$}.
Its visualization is easy to obtain by the following commands:
\pindexopt{field}{-n-iso}%
\pindexopt{field}{-n-iso-negative}%
\pindexopt{field}{-proj}%
\begin{verbatim}
  make mosolov_yield_surface
  zcat sector-010.field.gz | ./mosolov_yield_surface | \
        field - -proj P1 -n-iso 10 -n-iso-negative 5
\end{verbatim}
The unyielded zones, associated to negative values, appear in cold colors.
Conversely, the yielded ones are represented by warm colors.
% -------------------------------
\myexamplenoinput{mkview_mosolov}%
% -------------------------------
A combined representation of the solution
can be obtained by the following command:
\pindex{paraview}%
\begin{verbatim}
  bash mkview_mosolov sector-010.field.gz
\end{verbatim}
The shell script \code{mkview_mosolov}
invokes \code{mosolov_yield_surface}
and then directly builds the view shown on Fig.~\ref{fig-mosolov-adapt}
in the \code{paraview} graphic render.
Please, click to deselect the \fbox{show box} option for completing the view.
A cut of the velocity field along the first bisector is obtained by:
\begin{verbatim}
  field -mark u sector-010.field.gz -mark u -domain bisector -elevation -gnuplot
\end{verbatim}
Observe on Fig.~\ref{fig-mosolov-adapt}.bottom
the good capture of the small regions with zero velocity, near the outer corner of the square pipe section.
When compared with Fig.~\ref{fig-mosolov-u}.right,
the benefict of mesh adaptation appears clearly.

% ------------------------------
\subsection{Error analysis}
% ------------------------------
\label{ex-vp-poiseuille-rz}%
Theoretical error bounds for this problem
can be found in \citep{RoqMicSar-2000}. 
In order to study the error between the numerical solution and the exact solution
of the Mosolov problem, let us investigate a case for which
the exact solution can be explicitly expressed.
We consider the special case of a flow of a viscoplastic fluid in a circular pipe.
The pressure is expressed by $p(z) = -fz$.
The velocity has only one nonzero component along the $0z$ axis,
denoted as $u(r)$ for simplicity.
Conversely,
the symmetric tensor $\boldsymbol{\sigma}$ has only one non-zero $rz$ component,
denoted as $\sigma(r)$.
Thanks to the expression of the tensor-divergence operator 
in axisymmetric coordinates~\citep[p.~588]{BirArmHas-vol1-1987},
the problem reduces to~:

(P) : find $\sigma(r)$ and $u(r)$, defined in $]-R,R[$ such that
\begin{eqnarray*}
        \left\{
        \begin{array}{ll}
        \sigma(r)  = K |u'(r)|^{-1+n} \, u'(r)
      	+ \sigma_0 \Frac{u'(r)}{|u'(r)|},
      	& \mbox{ when } u'(r) \neq 0
      	\\
        |\sigma(r)| \leq \sigma_0
      	& \mbox{ otherwise }
        \end{array}
        \right.
		\\
     \begin{array}{rcll}
	-
	\Frac{1}{r}(r\sigma)'
        -
        f
	&=& 
	0
  	& \mbox{ in } \ ]-R,R[
		\\
        u(-R)=u(R) 
        &=&
        0
    \end{array}
\end{eqnarray*}
Let $\Sigma=fR/2$ be a representative stress
and $U$ a representative velocity such that $K(U/R)^n=\Sigma$.
Then, we consider the following change of unknown:
\[	
	r=R\tilde{r}, \ \ 
	u=U\tilde{u}, \ \ 
	\sigma=\Sigma\tilde{\sigma}
\]
The system reduces to a problem with only two 
parameters $n$ and the Bingham number $Bi=2\sigma_0/(fR)$,
that measures the ratio between the yield stress and the load.
Since there is no more ambiguity, we omit the tildes~:

(P) : find $\sigma$ and $u$, defined in $]-1,1[$ such that
\begin{eqnarray*}
        \left\{
        \begin{array}{ll}
        \sigma(r)  = |u'(r)|^{-1+n} \, u'(r)
      	+ Bi \Frac{u'(r)}{|u'(r)|},
      	& \mbox{ when } u'(r) \neq 0
      	\\
        |\sigma(r)| \leq Bi
      	& \mbox{ otherwise }
        \end{array}
        \right.
		\\
     \begin{array}{rcll}
	-
	\frac{1}{r}(r\sigma)'
        -
        2
	&=& 
	0
  	& \mbox{ in } \ ]-1,1[
		\\
        u(-1)=u(1) 
        &=&
        0
    \end{array}
\end{eqnarray*}
Remark that the solution is even~: $u(-r)=u(r)$.
We get $\sigma(r) = -r$ and the yield stress criterion
leads to $u(x)=0$ when $|r|\leq Bi$:
the load is weaker than the yield stress
and the flow is null.
When $Bi >1$ the solution is $u=0$.
Otherwise, when $|r|> Bi$, we get
\mbox{$ |u'(r)|^n + Bi = |r|$} and finally,
with the boundary conditions and the continuity 
at $r=\pm Bi$~:
\[
	u(r)
	=
	\Frac{
		(1-Bi)^{1+\frac{1}{n}}
		-
		\max(0,\,|r|-Bi)^{1+\frac{1}{n}}
	}{1+\frac{1}{n}} 
\]
When $n=1$, the second derivative of the solution is discontinuous
at $r=Bi$ and its third derivative is not square integrable.
For any $n>0$, an inspection of the integrability of the square of the
solution derivatives shows that $u\in H^{1+1/n}(]-1,1[,\,r\,{\rm d}r)$ at the best. 

\myexamplelicense{mosolov_exact_circle.h}
\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[height=6cm]{mosolov-circle-Bi-0,2-n-0,5-error-u-h1.pdf} &
      \includegraphics[height=6cm]{mosolov-circle-Bi-0,2-n-0,5-error-u-linf.pdf} \\
      \includegraphics[height=6cm]{mosolov-circle-Bi-0,2-n-0,5-error-sigma-l2.pdf} &
      \includegraphics[height=6cm]{mosolov-circle-Bi-0,2-n-0,5-error-sigma-linf.pdf}
    \end{tabular}
  \end{center}
  \caption{The augmented Lagrangian algorithm on the Mosolov
	problem with $Bi=0.2$ and $n=1/2$:
 	error versus mesh paremeter $h$ for various polynomial order $k$.
  }
  \label{fig-mosolov-error}
\end{figure}

% -------------------------------------
\myexamplenoinput{mosolov_error.cc}
% -------------------------------------
When computing on a circular pipe section, the exact solution is known
and it is also possible to compute the error: this is implemented in
the file \file{mosolov_error.cc}.
This file it is not listed here but is available in the \Rheolef\  example directory.
The error analysis is obtained by:
\begin{verbatim}
  make mosolov_error
  mkgeo_ball -order 2 -t 10 > circle-P2-10.geo
  ./mosolov_augmented_lagrangian circle-P2-10.geo P2 0.2 0.5
  zcat circle-P2-10.field | ./mosolov_error
\end{verbatim}
\cindex{approximation!high-order}%
\cindex{approximation!isoparametric}%
Note that we use an high order isoparametric approximation of the flow domain
for tacking into account the the curved boundaries.
Observe on Fig.~\ref{fig-mosolov-error} that 
both the error in $H^1$ norm for the velocity $u$
behaves as $\mathcal{O}(h^{s})$ with $s=\min(k,2)$.
This is optimal, as, from interpolation theory \citep[p.~109]{BreSco-2002-2ed}, we have:
\[
    \|u-\pi_h(u)\|_{1,2,\Omega}
    \leq
    C h^{s} |u|_{s+1,2,\Omega}
\]
with $s=\min(k,1/n)$ when $u\in H^{1+1/n}(\Omega)$.
Then, the convergence rate of the error in~$H^1$ norm
versus the mesh size is bounded by~$1/n$ for any polynomial order~$k$.
% TODO: Nevertheless, when $k=3$, the convergence is no more
%       improved as, when $n=1/2$,
%       $u\not\in H^{3+\varepsilon}(\Omega)$ for any $\varepsilon>0$.
The rate for the error in $L^\infty$ norm for
for the velocity~$u$ 
is $\min(k+1,1/n)$, for any $k\geq 1$ and $n>0$.
For the stress $\boldsymbol{\sigma}$,
the error in $L^2$ norm 
behaves as $\mathcal{O}(h)$ for any polynomial order
and, in $L^\infty$ norm, the rate is weaker, of about~$3/4$.
Finally, for $n=1/2$ there is no advantage of
using polynomial order more than $k=2$ with quasi-uniform meshes.
Conversely, for $n=1$, we obtains that there is no advantage of
using polynomial order more than $k=1$ with quasi-uniform meshes.
\cindex{mesh!adaptation}%
This limitation can be circumvented by combining mesh adaptation
with high order polynomials \citep{RoqMicSar-2000,SarRoq-2001}. 

% ------------------------------
\subsection{Error analysis for the yield surface}
% ------------------------------
The limit contour separating the rigid zones are expressed as
a level set of the stress norm:
\[
   \Gamma_{h} = \{ \boldsymbol{x}\in\Omega \ ; \ 
	| \boldsymbol{\sigma}_h(\boldsymbol{x})| = Bi \}
\]
This contour is called the \emph{yield surface} and its exact value is known
from the exact solution $\boldsymbol{\sigma}(\boldsymbol{x})=\boldsymbol{x}$
in the circle:
\[
   \Gamma = \{ \boldsymbol{x}\in\Omega \ ; \ |\boldsymbol{x}| = Bi \}
\]
Recall the stress $\boldsymbol{\sigma}_h$ converges in $L^\infty$ norm,
thus, there is some hope that its point-wise values converge.
As these point-wise values appears in the definition of the yield surface,
this suggests that $\Gamma_h$ could converge to $\Gamma$ with mesh refinement:
our aim is to check this conjecture.
Let us introduce the area between $\Gamma_h$ and $\Gamma$ as a
$L^1$ measure of the distance between them:
\[
   {\rm dist}(\Gamma,\Gamma_h)
   =
   \int_\Omega
     \delta\left(| \boldsymbol{\sigma}_h|-Bi, \ |\boldsymbol{\sigma}| - Bi \right)
     \,{\rm d}x
\]
where
\[
   \delta(\phi,\psi) 
   =
   \left\{ \begin{array}{ll}
	0 	& \mbox{when } \phi\psi\geq 0 \\
	1 	& \mbox{otherwise}
   \end{array}\right.
\]
% -----------------------------------
\myexamplelicense{mosolov_error_yield_surface.cc}
% -----------------------------------

\begin{figure}[htb]
  \begin{center}
  \begin{tabular}{cc}
    \includegraphics[height=6.0cm]{mosolov-circle-Bi-0,2-n-0,5-error-ys.pdf} &
    \includegraphics[height=6.0cm]{mosolov-circle-P1-Bi-0,2-n-0,5-ys.pdf} 
  \end{tabular}
  \end{center}
  \caption{The augmented Lagrangian algorithm on the Mosolov
	problem with $Bi=0.2$ and $n=1/2$:
 	(left) convergence of the yield surface versus mesh parameter $h$ for various polynomial order~$k$~;
 	(right) visualization of the yield surface ($P_1$ approximation).
  }
  \label{fig-mosolov-error-yield-surface}
\end{figure}

The computation of the error for the yield surface prediction writes:
\begin{verbatim}
  make mosolov_error_yield_surface
  zcat circle-P2-10.field.gz | ./mosolov_error_yield_surface
\end{verbatim}
Fig.~\ref{fig-mosolov-error-yield-surface}.right shows the result:
the yield surface error converges as $\mathcal{O}(h)$ for any $k\geq 0$.

Recall that the yield surface is
the zero isosurface of the level set function
\mbox{$\phi(\boldsymbol{x}=| \boldsymbol{\sigma}_h(\boldsymbol{x}| - Bi$}.
Its visualization, shown on Fig.~\ref{fig-mosolov-error-yield-surface}.left,
is provided by the following commands:
\begin{verbatim}
  make mosolov_yield_surface
  zcat circle-P2-10.field.gz | ./mosolov_yield_surface | \
        field - -proj P1 -n-iso 10 -n-iso-negative 5
\end{verbatim}

