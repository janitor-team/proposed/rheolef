set terminal cairolatex pdf color standalone
set output 'p-laplacian-newton-p=1,5-n.tex'

set size square 
set colors classic
set log y
set xrange [0:100]
set xtics (0,50,100)
set yrange [1e-15:1e5]
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}" 1 )
set xlabel '[c]{\large $n$}'
set  label '[l]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph 0.01,0.92
set  label '[c]{$p=3/2$}' at graph 0.3,0.50

plot \
'p-laplacian-newton-p=1.5-n=10.gdat' \
	t '[r]{$h=1/10$}' w lp lw 2, \
'p-laplacian-newton-p=1.5-n=20.gdat' \
	t '[r]{$h=1/20$}' w lp lw 2 lc '#008800', \
'p-laplacian-newton-p=1.5-n=30.gdat' \
	t '[r]{$h=1/30$}' w lp lw 2, \
'p-laplacian-newton-p=1.5-n=40.gdat' \
	t '[r]{$h=1/40$}' w lp lw 2, \
'p-laplacian-newton-p=1.5-n=50.gdat' \
	t '[r]{$h=1/50$}' w lp lw 2

#pause -1 "<retour>"
