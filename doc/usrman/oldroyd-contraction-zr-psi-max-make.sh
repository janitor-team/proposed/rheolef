#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
input=${1-"contraction1-zr.branch"}
for input in $*; do
  echo "# $input"
  echo "# We psi_max"
  branch -toc $input 2>/dev/null| tail --lines=+2 > tmp-toc.txt
  n=`cat tmp-toc.txt | wc -l`
  We_list=`cat tmp-toc.txt | gawk '{print $2}'`
  i=0
  while test $i -lt $n; do
    We=`echo $We_list | gawk '{print $1}'`
    We_list=`echo $We_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
    psi_max=`branch $input -extract $i 2>/dev/null | field - -mark u -field 2>/dev/null | ./streamf_contraction 2>/dev/null | field - -max 2>/dev/null`
    echo "$We $psi_max"
    i=`expr $i + 1`
  done
  echo; echo
done
