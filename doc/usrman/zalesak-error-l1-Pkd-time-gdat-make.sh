#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
if test $# -eq 0; then
  branch_list=`ls square-100-P2d-*.branch.gz`
else
  branch_list=$*
fi

r=20
echo "# n_max err_linf_l1 v_max"
for x in ${branch_list}; do
  n_max=`echo $x | sed -e 's/.*d-//' -e 's/\..*//'`
  #echo "$x : $n_max"
  zcat $x | ./zalesak_dg_error $r > tmp.txt 2>/dev/null
  err_linf_l1=`grep err_linf_l1 tmp.txt | awk '{print $4}'`
  zcat $x | ./zalesak_dg_mass  $r > tmp.txt 2>/dev/null
  v_max=`grep v_max tmp.txt | awk '{print $4}'`
  echo "$n_max $err_linf_l1 $v_max"
done
