set terminal cairolatex pdf color standalone
set output "oldroyd-contraction3-zr-tau11-axis.tex"

set size square
set colors classic
set key bottom
set xrange [-4:4]
set yrange [-2.5:1]
set xtics 2
set ytics 1
set xlabel '[c]{$z$}'
set  label '[l]{$\tau_{rr}(z,0)$}' at graph 0.03, 0.93

plot \
'oldroyd-contraction3-zr-tau11-axis.gdat' \
  i 7 \
  u 1:3 \
  t '[r]{$We=0.7$}' \
  w l lw 4 lc 1, \
'oldroyd-contraction3-zr-tau11-axis.gdat' \
  i 5 \
  u 1:3 \
  t '[r]{$We=0.5$}' \
  w l lw 4 lc rgb '#008800', \
'oldroyd-contraction3-zr-tau11-axis.gdat' \
  i 3 \
  u 1:3 \
  t '[r]{$We=0.3$}' \
  w l lw 4 lc 3, \
'oldroyd-contraction3-zr-tau11-axis.gdat' \
  i 0 \
  u 1:3 \
  t '[r]{$We=0\phantom{.0}$}' \
  w l lw 4 lc 4

#pause -1
