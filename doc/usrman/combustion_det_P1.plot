set terminal cairolatex pdf color standalone
set output "combustion_det_P1.tex"

#set title "det(J(lambda))/det(J(0)) vs lambda_c_h - lambda"
set size square
set colors classic
set key left
set log xy
f(m,e,b) = m*(b**e)

set xrange [1e-8:10]
set yrange [1e-5:10]
graph_ratio = 9.0/6.0

set key right bottom
set xlabel '[c]{\large $\lambda_{c,h}-\lambda$}'
set  label '[l]{\large $\displaystyle\frac{\textrm{det}\left(\frac{\partial F}{\partial u}(\lambda,u_h(\lambda))\right)}{\textrm{det}\left(\frac{\partial F}{\partial u}(0,u_h(0))\right)}$}' at graph 0.05,0.85

set xtics (\
        "[c]{$10^{-8}$}" 1e-8, \
        "[c]{$10^{-6}$}" 1e-6, \
        "[c]{$10^{-4}$}" 1e-4, \
        "[c]{$10^{-2}$}" 1e-2, \
        "[c]{$1$}" 1)
set ytics (\
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$10^{-4}$}" 1e-4, \
        "[r]{$10^{-3}$}" 1e-3, \
        "[r]{$10^{-2}$}" 1e-2, \
        "[r]{$10^{-1}$}" 1e-1, \
        "[r]{$1$}"       1 )

# triangle a droite
slope_A = graph_ratio*0.5
xA =  0.1
yA =  0.15
dxA = 0.10
dyA = dxA*slope_A
set label "[l]{\\scriptsize $1/2$}" at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead



lambda_c     = 3.51383071912516
lambda_c_10  = 3.56659177249115
lambda_c_20  = 3.52687692663115
lambda_c_40  = 3.51708339820236
lambda_c_80  = 3.51464333904495
lambda_c_160 = 3.51403384294798

det_0_10  = f(0.582076609134674,0,2)
det_0_20  = f(0.677626357803442,0,2)
det_0_40  = f(0.918354961579942,0,2)
det_0_80  = f(0.843375835458491,0,2)
det_0_160 = f(0.711282799835262,0,2)

a = 0.503284 #       +/- 0.0002632    (0.05231%)
c = 0.526748 #       +/- 0.0001297    (0.02462%)
det(x)=c*x**a
#a=1
#b=1
#fit d(x) 'combustion_det_P1.gdat' i 0 u (abs(lambda_c_10 -$1)):(f($2, $3 -   34,$4)/det_0_10) via a,c

plot \
'combustion_det_P1.gdat' i 0 u (abs(lambda_c_10 -$1)):(f($2, $3 -   34,$4)/det_0_10)  t '[r]{$h=1/10$}'  w lp, \
'combustion_det_P1.gdat' i 1 u (abs(lambda_c_20 -$1)):(f($2, $3 -   87,$4)/det_0_20)  t '[r]{$h=1/20$}'  w lp lc rgb '#008800', \
'combustion_det_P1.gdat' i 2 u (abs(lambda_c_40 -$1)):(f($2, $3 -  213,$4)/det_0_40)  t '[r]{$h=1/40$}'  w lp, \
'combustion_det_P1.gdat' i 3 u (abs(lambda_c_80 -$1)):(f($2, $3 -  506,$4)/det_0_80)  t '[r]{$h=1/80$}'  w lp, \
'combustion_det_P1.gdat' i 4 u (abs(lambda_c_160-$1)):(f($2, $3 - 1172,$4)/det_0_160) t '[r]{$h=1/160$}' w lp pt 6


#pause -1 "<retour>"
