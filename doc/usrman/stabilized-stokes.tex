\subsection{The $P_1b-P_1$ element for the Stokes problem}
\label{sec-p1bp1}
\pbindex{Stokes}

\subsubsection*{Formulation and approximation}

\apindex{P1}%
\apindex{bubble}%

\apindex{P2-P1, Taylor-Hood}%
\apindex{P1b-P1}%
  Let us go back to the Stokes problem. In section~\ref{sec-stokes},
  page~\pageref{sec-stokes}, the Taylor-Hood finite element was 
  considered.
  Here, we turn to the mini-element
  proposed by \citet*{ArnBreFor-1984},
  also well-known as the \emph{P1-bubble} element.
  This element is generally less precise than the Taylor-Hood one, but
  becomes popular, mostly because it is easy to implement in two and three
  dimensions and furnishes a $P_1$ approximation of the velocity field.
  Moreover, this problem develops some links with
  stabilization technique and will presents some new \Rheolef\  features.
 
  We consider a mesh ${\cal T}_h$ of $\Omega \subset \mathbb{R}^d$, $d=2,3$
  composed only of simplicial elements: triangles when $d=2$ and 
  tetrahedra when $d=3$.
  The following finite dimensional spaces are introduced:
  \begin{eqnarray*}
      {\bf X}^{(1)}_h &=& \{ {\bf v} \in (H^1(\Omega))^d; \
          {\bf v}_{/K} \in (P_1)^d, \
          \forall K \in {\cal T}_h \}, \\
      {\bf B}_h &=& \{ \bbeta \in (C^0(\bar{\Omega}))^d; \
		\bbeta_{/K} \in B(K)^d,  \forall K \in {\cal T}_h \} \\
      {\bf X}_h &=& {\bf X}^{(1)}_h \oplus {\bf B}_h\\
      {\bf V}_h(\alpha) &=& X_h \cap {\bf V}(\alpha), \\
       Q_h &=& \{ q \in L^2(\Omega))\cap C^0(\bar{\Omega}); \
          q_{/K} \in P_1, \
          \forall K \in {\cal T}_h \},
  \end{eqnarray*}
  where $B(K)={\rm vect}(\lambda_1 \times \ldots \times \lambda_{d+1})$
  and $\lambda_i$ are the barycentric coordinates of the simplex $K$.
  The $B(K)$ space is related to the \emph{bubble} local space.
  The approximate problem is similar to \eqref{eq-fvh-stokes},
  page~\pageref{eq-fvh-stokes}, up to the choice of finite dimensional spaces.

  Remark that the velocity field splits in two terms:
  ${\bf u}_h={\bf u}^{(1)}_h + {\bf u}^{(b)}_h$, where
  ${\bf u}^{(1)}_h \in {\bf X}^{(1)}_h$ is continuous and piecewise linear,
  and ${\bf u}^{(b)}_h \in {\bf B}_h$ is the bubble term.

\cindex{geometry!contraction}%
\cindex{benchmark!flow in an abrupt contraction}%
\cindex{boundary condition!mixed}%
  We consider the abrupt contraction geometry:
  \[
     \Omega = ]\!-\!L_{u},0[\times ]0,c[ \ \cup\ [0,L_d[\times ]0,1[
  \]
  where $c\geq 1$ stands for the contraction ratio, and $L_u,L_d>0$, are the
  upstream and downstream tube lengths.
  The boundary conditions on ${\bf u}=(u_0,u_1)$ for this test case are:
  \begin{eqnarray*}
	u_0 &=& u_{\rm poiseuille} 
		\ \mbox{ and } \ 
		u_1 = 0 
		\ \mbox{ on } \Gamma_{\rm upstream} \\
	{\bf u} &=& 0 \ \mbox{ on } \Gamma_{\rm wall} \\
	\Frac{\partial u_0}{\partial x_1} &=& 0
		\ \mbox{ and } \ 
		u_1 = 0 \ \mbox{ on } \Gamma_{\rm axis} \\
	\Frac{\partial {\bf u}}{\partial n} &=& 0 \ \mbox{ on } \Gamma_{\rm downstream}
  \end{eqnarray*}
  where
  \begin{eqnarray*}
		\Gamma_{\rm upstream} &=& \{-L_u\}\times ]0,c[ \\
		\Gamma_{\rm downstream} &=& \{L_d\}\times ]0,1[ \\
		\Gamma_{\rm axis} &=&  ]\!-\!L_u,L_d[ \times \{0\} \\
		\Gamma_{\rm wall} &=& 
			]\!-\!L_u,0[ \times \{c\} 
			\ \cup \ \{0\} \times ]1,c[
			\ \cup \ ]0,L_d[ \times \{1\} 
  \end{eqnarray*}
\apindex{P2-P1, Taylor-Hood}%
  The matrix structure is similar to those of the Taylor-Hood
  element (see section~\ref{sec-stokes}, page~\pageref{sec-stokes}).
  Since ${\bf X}_h = {\bf X}^{(1)}_h \oplus {\bf B}_h$,
  any element $u_h\in X_h$ can be written as a sum
  $u_h=u_{1,h}+u_{b,h}$ where $u_{1,h}\in {\bf X}^{(1)}_h$ and $u_{b,h}\in {\bf B}_h$.
  Remark that
  \[
	a(u_{1,h},v_{b,h}) = 0
	,\ \ \forall u_{1,h}\in {\bf X}^{(1)}_h
	,\ \ \forall v_{b,h}\in {\bf B}_h
	.
  \]
  Thus, the form $a(.,.)$ defined over ${\bf X}_h\times {\bf X}_h$ writes
  simply as the sum of the forms $a_1(.,.)$ and $a_b(.,.)$,
  defined over ${\bf X}^{(1)}_h\times {\bf X}^{(1)}_h$
  and ${\bf B}_h\times {\bf B}_h$ respectively.
  Finally, the form $b(.,.)$ defined over ${\bf X}_h\times Q_h$ writes
  as the sum of the forms $b_1(.,.)$ and $b_b(.,.)$,
  defined over ${\bf X}^{(1)}_h\times Q_h$
  and ${\bf B}_h\times Q_h$ respectively.
  Then, the linear system admits the following block structure~:
  \[
    \left(
	\begin{array}{ccc}
	   A_1 & 0   & B_1^T \\
	     0 & A_b & B_b^T \\
	   B_1 & B_b &  0 
	\end{array}
    \right)
    \left(
	\begin{array}{c}
	 U_1 \\ U_b \\ P
	\end{array}
    \right)
    =
    \left(
	\begin{array}{c}
	 L_1 \\ L_b \\ L_p
	\end{array}
    \right)
  \]
  An alternative and popular implementation
  of this element eliminates the unknowns related to the
  bubble components (see e.g.~\citealp{Abd-1987-these}, page~24).
  Remark that, on any element $K \in {\cal T}_h$,
  any bubble function $v_K$ that belongs to $B(K)$
  vanishes on the boundary of $K$ and have a compact support in $K$.
  Thus, the $A_b$ matrix is block-diagonal.
  Moreover, $A_b$ is invertible and $U_b$ writes~:
  \[
	U_b = A_b^{-1}(B_b^{T}p - L_b)
  \]
  As $A_b$ is block-diagonal, its inverse can be efficiently inverted
  at the element level during the assembly process.
  Then, $U_b$ can be easily eliminated from the system that reduces to: 
  \[
     \left( \begin{array}{cc} 
	A_1 & B_1^T \\
        B_1 & -C 
     \end{array} \right)
     \left( \begin{array}{l} 
        U_1 \\ P
     \end{array} \right)
	=
     \left( \begin{array}{l} 
        L_1 \\ \tilde{L}_p
     \end{array} \right)
  \]
  where $\tilde{L}_p=L_p-A_b^{-1}L_p$
  and $C=B_bA_b^{-1}B_b^T$.
  Remarks that the matrix structure is similar to those of the nearly incompressible
  elasticity (see~\ref{sec-incompressible-elasticity}, page~\ref{sec-incompressible-elasticity}).
\pbindex{stabilized Stokes}%
  This reduced matrix formulation of the $P_1b-P1$ element
  is similar to the direct $P_1-P_1$ stabilized element,
  proposed by \citet*{BrePit-1984}.

% TODO: test resol iterative de 3D de P1bP1 !
% ---------------------------------------
\myexamplelicense{stokes_contraction_bubble.cc}
% ---------------------------------------

% ======================
\subsubsection*{Comments}
% ======================

\cindex{matrix!bloc-diagonal!inverse}%
\clindex{integrate_option}%
\cindex{form!product}%
\findex{integrate}%
First, $A_b^{-1}$ is computed as:
\begin{lstlisting}[numbers=none,frame=none]
  integrate_option iopt;
  iopt.invert = true;
  form inv_ab = integrate (2*ddot(D(ub),D(vb)), iopt);
\end{lstlisting}
Note the usage of the optional parameter \code{iopt} to the \code{integrate} function.
As the form is bloc-diagonal, its inverse is computed element-by-element during the assembly process.
Next, the $C=B_b A_b^{-1} B_b^T$ form is simply computed as:
\begin{lstlisting}[numbers=none,frame=none]
  form c = bb*inv_ab*trans(bb);
\end{lstlisting}

The file \file{contraction.h} contains code for the velocity and stream function boundary
conditions.

% -------------------------
\myexamplelicense{contraction.h}
% -------------------------

Without loss of generality, we assume that the half width of the downstream channel is assumed to be equal to one.
The Poiseuille velocity upstream boundary condition \verb+u_upsteam+ is then scaled such that the
downstream average velocity is equal to one.
By this way, the flow rate in the half upstream and downstream channel are also equal to one.
The stream function is defined up to a constant:
we assume that it is equal to $-1$ on the axis of symmetry:
by this way, it is equal to zero on the wall.
  
\cindex{geometry!axisymmetric}%
\cindex{coordinate system!axisymmetric}%
The file \file{contraction.h} also contains a treatment of the axisymmetric variant of the geometry:
this case will be presented in the next paragraph.
Note also the automatic computation of the geometric
coordinate system and contraction ratio $c$ from the input mesh, as:
\begin{lstlisting}[numbers=none,frame=none]
  c = omega.xmax()[1];
  string sys_coord = omega.coordinate_system_name();
\end{lstlisting}
\cindex{boundary condition!Poiseuille flow}%
These parameters are transmitted via a base class
to the class-function that computes the 
Poiseuille upstream flow boundary condition.

% ===================================
\subsubsection*{How to run the program}
% ===================================

  \begin{figure}[htb]
     \begin{center}
       \begin{tabular}{ll}
          \includegraphics[height=4.5cm]{contraction-geo.pdf} 
		&\\
          \includegraphics[height=4.5cm]{contraction-bubble-psi.pdf} 
		& $\psi_{\rm max}=1.109\times 10^{-3}$ \\
          \includegraphics[height=4.5cm]{contraction-P2-psi.pdf}
		& $\psi_{\rm max}=1.118\times 10^{-3}$
       \end{tabular}
     \end{center}
     \caption{Solution of the Stokes problem in the abrupt contraction:
       (top) the mesh;
       (center) the $P_1$ stream function associated to the $P_1b-P_1$ element;
       (bottom) the $P_2$ stream function associated to the $P_2-P_1$ Taylor-Hood element.
       }
     \label{fig-contraction-bubble}
  \end{figure}

\exindex{contraction.mshcad}%
\cindex{directory of example files}%
  The boundary conditions in this example are related to an abrupt
  contraction geometry with a free surface.
  The corresponding mesh \file{contraction.geo} can be easily build from the
  geometry description file \reffile{contraction.mshcad}, which
  is provided in the example directory of the \Rheolef\  distribution.
  The building mesh procedure is presented with details
  in appendix~\ref{sec-how-to-mesh}, page~\ref{sec-how-to-mesh}.
\pindex{gmsh}
\cindex{mesh!generation}
\pindex{msh2geo}
\fiindex{\filesuffix{.msh} gmsh mesh}
\fiindex{\filesuffix{.mshcad} gmsh geometry}
\fiindex{\filesuffix{.geo} mesh}
\begin{verbatim}
  gmsh -2 contraction.mshcad -format msh2 -o contraction.msh
  msh2geo contraction.msh > contraction.geo
  geo contraction.geo
\end{verbatim}
  The mesh is represented on Fig.~\ref{fig-contraction-bubble}.top.
  Then, the computation and the visualization writes:
\begin{verbatim}
  make stokes_contraction_bubble
  ./stokes_contraction_bubble contraction.geo > contraction-P1.field
  field contraction-P1.field -velocity
\end{verbatim}
  The visualization of the velocity field brings few informations
  about the properties of the flow.
  The stream function is more relevant for stationary flow visualization.

\clearpage
\cindex{stream function}%
% ---------------------------------
\myexamplelicense{streamf_contraction.cc}
% ---------------------------------

\apindex{P2-P1, Taylor-Hood}%
\findex{integrate}%
Note the usage of the optional parameter \code{iopt} to the \code{integrate} function.
\begin{lstlisting}[numbers=none,frame=none]
  iopt.ignore_sys_coord = true;
\end{lstlisting}
\cindex{geometry!axisymmetric}
\cindex{coordinate system!axisymmetric}%
\cindex{form!{${\rm bcurl}({\bf u}).\boldsymbol{\xi}$}}%
In the axisymmetric coordinate system, there is a specific definition
of the stream function, together with the use of a variant of the ${\bf curl}$
operator, denoted as ${\bf bcurl}$ in \Rheolef.
\begin{lstlisting}[numbers=none,frame=none]
  field lh = integrate (dot(uh,bcurl(xi)));
\end{lstlisting}
The axisymmetric case will be presented in the next section.
By this way, our code is able to deal with both Cartesian and axisymmetric
geometries.

The stream function $\psi$ (see also section~\ref{sec-streamf}) is computed and visualized as:
\pindexopt{field}{-bw}%
\pindexopt{field}{-n-iso}%
\pindexopt{field}{-n-iso-negative}%
\begin{verbatim}
  make streamf_contraction
  ./streamf_contraction < contraction-P1.field > contraction-P1-psi.field
  field contraction-P1-psi.field
  field contraction-P1-psi.field -n-iso 15 -n-iso-negative 10 -bw
\end{verbatim}
\cindex{vortex}%
The $P_1$ stream function is represented on Fig.~\ref{fig-contraction-bubble}.center.
The stream function is zero along the wall and the line separating the main flow and
the vortex located in the outer corner of the contraction.
Thus, the isoline associated to the zero value separates the main flow from the vortex.
In order to observe this vortex, an extra \code{-n-iso-negative 10} option is added:
ten isolines are drawn for negatives values of $\psi$, associated to the main flow,
and \code{n_iso-10} for the positives values, associated to the vortex.

\myexamplenoinput{stokes_contraction.cc}%
\cindex{directory of example files}%
  A similar computation based on the Taylor-Hood $P_2-P_1$ element is implemented
  in \code{stokes_contraction.cc}. The code is similar, up to the boundary conditions,
  to \code{stokes_cavity.cc} (see page~\pageref{cavity.h}):
  thus it is not listed here but is available in the \Rheolef\  example directory.
\begin{verbatim}
  make stokes_contraction
  ./stokes_contraction contraction.geo > contraction-P2.field
  field contraction-P2.field -velocity
  ./streamf_contraction < contraction-P2.field > contraction-P2-psi.field
  field contraction-P2-psi.field -n-iso-negative 10 -bw
\end{verbatim}
  The associated $P_2$ stream function is represented on Fig.~\ref{fig-contraction-bubble}.bottom.
  Observe that the two solutions are similar and that the vortex activity, defined as $\psi_{\max}$,
  is accurately computed with the two methods (see also~\citealp{Sar-1990-these}, Fig.~5.11.a, page~143).
\pindexopt{field}{-max}%
\begin{verbatim}
  field contraction-P1-psi.field -max
  field contraction-P2-psi.field -max
\end{verbatim}
  Recall that the stream function is negative in the main flow and positive in the vortex 
  located in the outer corner of the contraction.
  Nevertheless, the Taylor-Hood based solution is more accurate~:
  this is perceptible on the graphic, in the region where the upstream vortex reaches the boundary.

