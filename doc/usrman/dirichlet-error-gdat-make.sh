#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
e=${1-"e"}
prog="dirichlet2"
case $e in
 e)   kmax=4; L="3 4 8 16 32 64 128 256 512 1024";;
 t|q) kmax=4; L="1 2 4 8 16 32 64 128 256 512";;
 *)   kmax=4; L="5 10 20 40";;
esac
k=1
echo "# prog = $prog"
while test $k -le $kmax; do
    echo "# P${k}d $e"
    echo "# 1/h err_u_l2 err_u_linf err_u_h1 cpu"
    for m in $L; do
      command="mkgeo_grid -$e $m > tmp.geo"
      #echo "! $command" 1>&2
      eval $command
      command="/usr/bin/time --format='cpu = %U' ./$prog tmp.geo P${k} 2>cpu.txt | ./sinusprod_error >tmp.txt 2>/dev/null"
      #echo "! $command" 1>&2
      eval $command
      err_u_l2=` grep err_u_l2 tmp.txt | gawk '{print $2}'`
      err_u_linf=` grep err_u_linf tmp.txt | gawk '{print $2}'`
      err_u_h1=` grep err_u_h1 tmp.txt | gawk '{print $2}'`
      cpu=` grep cpu cpu.txt | gawk '{print $3}'`
      echo "$m $err_u_l2 $err_u_linf $err_u_h1 $cpu"
    done
    echo; echo
    k=`expr $k + 1`
done
rm -f tmp.geo cpu.txt
