\subsection{Nonlinear scalar hyperbolic problems with diffusion}
% ------------------------------------
%\subsubsection*{General problem setting}
% ------------------------------------
A time-dependent nonlinear second order problem
with nonlinear first order dominated terms
problem writes:

\ \ $(P)$: \emph{find $u$, defined in $]0,T[ \times \Omega$, such that}
\begin{subequations}
\begin{eqnarray}
  \Frac{\partial u}{\partial t}
  +
  {\rm div}\,{\bf f}(u)
  -
  \varepsilon\Delta u
  &=& 0 
  \ \mbox{ in } ]0,T[\times \Omega
	\label{eq-dg-hyp-diff-eqn}
  	\\
  u(t\!=\!0) &=& u_0 
  \ \mbox{ in } \Omega 
	\label{eq-dg-hyp-diff-ini}
	\\
  u &=& g
  \mbox{ on } ]0,T[\times \partial\Omega
	\label{eq-dg-hyp-diff-bc}
\end{eqnarray}
\end{subequations}
where $\varepsilon>0$,
$T>0$, $\Omega\subset \mathbb{R}^d$, $d=1,2,3$
and the initial condition ${\bf u}_0$ being known.
The function
\mbox{$
  {\bf f}: \mathbb{R} \longrightarrow \mathbb{R}^d
$} is also known and supposed to be continuously differentiable.
The initial data $u_0$, defined in $\Omega$,
and the boundary one, $g$, defined on $\partial\Omega$ are given. 

Comparing
\eqref{eq-dg-hyp-diff-eqn}-\eqref{eq-dg-hyp-diff-bc}
with the non-diffusive case
\eqref{eq-dg-hyp-eqn}-\eqref{eq-dg-hyp-bc}
               page~\pageref{eq-dg-hyp-bc},
the second order term has been added in
\eqref{eq-dg-hyp-diff-eqn}
and the upstream boundary condition
has been replaced by a Dirichlet one
\eqref{eq-dg-hyp-diff-bc}.

% ---------------------------------------------------------
\subsection{Example: the Burgers equation with diffusion}
% ---------------------------------------------------------
\subsubsection*{Problem statement and its exact solution}
% ---------------------------------------------------------
\begin{figure}[htb]
     \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=6cm]{burgers_diffusion_exact.pdf} &
       \end{tabular}
     \end{center}
     \caption{An exact solution for the Burgers equation with diffusion
	($\varepsilon=10^{-1}$, $x_0=-1/2$).
     }
   \label{fig-burgers-diffusion-exact}
\end{figure}

Our model problem in this chapter is the
one-dimensional Burgers equation.
It was introduced
in section~\ref{sec-burger-dg},
page~\pageref{sec-burger-dg}
with the choice
\mbox{$
  {\bf f}(u) = u^2/2
$},
for all $u\in\mathbb{R}$.
Equation \eqref{eq-dg-hyp-diff-eqn} admits an exact solution
% see FerHu-2011
\begin{eqnarray}
  u(t,x) &=& 1 - \tanh\left(
	\Frac{x-x_0-t}{2\varepsilon}
  \right)
\end{eqnarray}
% ---------------------------------------------------------
\myexamplelicense{burgers_diffusion_exact.h}
% ---------------------------------------------------------
The solution is represents on
Fig.~\ref{fig-burgers-diffusion-exact}.
Here $x_0$ represent the position of the front at $t=0$
and $\varepsilon$ is a characteristic width of the front.
The initial and boundary condition are chosen 
such that $u(t,x)$ is the solution of
\eqref{eq-dg-hyp-diff-eqn}-\eqref{eq-dg-hyp-diff-bc}.

\begin{figure}[htb]
       \begin{tabular}{cc}
          \includegraphics[height=6cm]{burgers_diffusion_semiimplicit_error-h-50-dt.pdf} &
          \includegraphics[height=6cm]{burgers_diffusion_rk_semiimplicit_error-dt.pdf}
       \end{tabular}
     \begin{center}
     \end{center}
     \caption{Convergence of the first order semi-implicit scheme for the Burgers equation
		with diffusion ($\epsilon=0.1$, $T=1$).
		(a) first order semi-implicit scheme ;
		(b) Runge-Kutta semi-implicit scheme with $p=3$.
     }
   \label{fig-burgers-diffusion-semiimplicit}
\end{figure}
Fig.~\ref{fig-burgers-diffusion-semiimplicit}.a plots the error versus $\Delta t$
for the semi-implicit scheme when $k=1$ and~$2$, and for $h=2/50$.
The time step for which the error becomes independent upon $\Delta t$
and depends only upon $h$ is of about $\Delta t=10^{-3}$ when
$k=1$ and of about $10^{-5}$ when $k=2$.
This approach is clearly inefficient for high order polynomial $k$
and a hiher order time scheme is required.

Fig.~\ref{fig-burgers-diffusion-semiimplicit}.b plots the error versus $\Delta t$
for the Runge-Kutta semi-implicit scheme with $p=3$, $k=1$ and $h=2/200$.
The scheme is clearly only first-order, which is still unexpected.
More work is required...




% ------------------------------------------------
\subsubsection*{Space discretization}
% ------------------------------------------------
The \emph{discontinuous} finite element space is defined by:
\[ 
   X_h = \{ v_h \in L^2(\Omega); v_{h|K}\in P_k, \ \forall K \in \mathcal{T}_h \}
\]
where $k\geq 1$ is the polynomial degree.
\cindex{broken Sobolev space {$H^1(\mathcal{T}_h)$}}%
As elements of $X_h$ do not belongs to $H^1(\Omega)$, due to discontinuities
at inter-elements, we introduce the broken Sobolev space:
\[
   H^1(\mathcal{T}_h) = \{ v \in L^2(\Omega); \ v_{|K}\in H^1(K), \ \forall K\in\mathcal{T}_h \}
\]
such that $X_h\subset H^1(\mathcal{T}_h)$.
% TODO: au lieu de recopier ah et lh, donner les refs dans la doc rheolef2-dg
As for the Dirichlet problem,
introduce the folowing bilinear form $a_h(.,.)$ and linear for $l_h(.)$,
defined for all $u,v\in H^1(\mathcal{T}_h)$ by
(see e.g. \citealp[p.~125~and~127]{PieErn-2012}, eqn. (4.12)):
\cindex{form!{$\jump{u}\jump{v}$}}%
\cindex{form!{$\jump{u}\average{\nabla_h v.{\bf n}}$}}%
\begin{eqnarray}
  a_{h}(u,v)
    &=&
    \int_\Omega
	\nabla_h u .\nabla_h v
	\,{\rm d}x
    +
    \sum_{S \in \mathscr{S}_h}
    \int_S
        \left(
	      \kappa_s
              \, \jump{u} \, \jump{v}
              - 
              \average{\nabla_h u.{\bf n}}
              \, \jump{v}
              -
              \jump{u}
              \, \average{\nabla_h v.{\bf n}}
        \right)
        \, {\rm d}s
	\label{eq-dg-hyp-diff-ah}
    \\
  \ell_{h}(v)
    &=&
    \int_{\partial\Omega}
        \left(
	    \kappa_s 
	    \, g 
            \, v 
          -
	    g
            \,\nabla_h v.{\bf n}
        \right)
	\,{\rm d}s
	\label{eq-dg-hyp-diff-lh}
\end{eqnarray}

The semi-discrete problem writes in variational
form~\citep[p.~100]{PieErn-2012}:

\ \ $(P)_h$: \emph{find $u_h\in C^1([0,T],X_h)$ such that}
\begin{eqnarray*}
  \int_\Omega
    \Frac{\partial u_h}{\partial t}
    v_h
    \,{\rm d}x
  \int_\Omega
    G_h(u_h)
    \,v_h
    \,{\rm d}x
  +
  \varepsilon\,
  a_{h}(u_h,v_h)
  &=& 
  \varepsilon\,
  \ell_{h}(v_h)
  ,\ \ \forall v_h\in X_h
  \\
  u_h(t\!=\!0) &=& \pi_h(u_0)
\end{eqnarray*}
where $G_h$ has been
introduced in~\eqref{eq-dg-hyp-def-Gh},
       page~\pageref{eq-dg-hyp-def-Gh}.

% ----------------------------------------------------------
%\subsubsection*{Explicit high order time discretization}
% ----------------------------------------------------------
%\cindex{method!Runge-Kutta scheme}%
%
%An alternative to the semi-implicit first order scheme
%is the higher order Runge-Kutta one, as introduced in
% section~\ref{sec-hyperbolic-runge-kutta}
%page~\pageref{sec-hyperbolic-runge-kutta}.
%Problem presents as a set of nonlinear ordinary differential equations
%
%\ \ $(P)_h$: \emph{find $u_h\in C^1([0,T],X_h)$ such that}
%\begin{eqnarray*}
%    \Frac{\partial u_h}{\partial t}
%    +
%    \tilde{G}_h(u_h)
%    &=&
%    0
%\end{eqnarray*}
%where $\tilde{G}_h$ is defined for all $u_h\in X_h$ by
%\begin{eqnarray*}
%  \int_\Omega
%    \tilde{G}_h(u_h)
%    \, v_h
%    \,{\rm d}x
%  &=&
%  \int_\Omega
%    G_h(u_h)
%    \, v_h
%    \,{\rm d}x
%  +
%  \varepsilon
%  ( a_h (u_h,v_h) - \ell_h (v_h) )
%  ,\ \ \forall v_h\in X_h
%\end{eqnarray*}
%This explicit time discretization suffers from a
%stringent time-step restriction for stability
%when diffusion is no more dominated by convection.
%For instance, the scheme is stable when using
%$\varepsilon=10^{-2}$ and $h=2/100$:
%\begin{verbatim}
%  make ./burgers_diffusion_rk_dg
%  mkgeo_grid -e 100 -a -1 -b 1 > line.geo
%  ./burgers_diffusion_rk_dg line P1d 0.01 1000 > line.branch
%\end{verbatim}
%but becomes instable when $\varepsilon=10^{-1}$:
%\begin{verbatim}
%  ./burgers_diffusion_rk_dg line P1d 0.1  1000 > line.branch
%\end{verbatim}
% ----------------------------------------------------------
%\subsubsection*{Semi-implicit first order time discretization}
% ----------------------------------------------------------
%Let $\Delta t>0$ be the time step.
%We consider a first-order semi-implicit scheme.
%\begin{eqnarray*}
%  \int_\Omega
%    \Frac{u_h^{n+1} - u_h^n}{\Delta t}
%    v_h
%    \,{\rm d}x
%  +
%  \int_\Omega
%    G_h\left(u_h^n\right)
%    \,v_h
%    \,{\rm d}x
%  +
%  \varepsilon\,
%  a_{h}\left(u_h^{n+1},v_h\right)
%  &=& 
%  \varepsilon\,
%  \ell_{h}(v_h)
%  ,\ \ \forall v_h\in X_h
%  \\
%  u_h^0 &=& \pi_h(u_0)
%\end{eqnarray*}
%Observe that the second order diffusion terms are treated implicitly
%while first-order terms are explicit.
%At each time step, the problem writes:
%\begin{eqnarray*}
%  c_{h}\left(u_h^{n+1},v_h\right)
%  &=&
%  k_{h}(v_h)
%  ,\ \ \forall v_h\in X_h
%\end{eqnarray*}
%where
%\begin{eqnarray*}
%  c_{h}\left(u_h,v_h\right)
%  &=&
%  \Frac{1}{\Delta t}
%  \int_\Omega
%    u_h
%    v_h
%    \,{\rm d}x
%  +
%  \varepsilon\,
%  a_{h}\left(u_h,v_h\right)
%  ,\ \ \forall u_h,v_h\in X_h
%  \\
%  k_{h}(v_h)
%  &=& 
%  \varepsilon\,
%  \ell_{h}(v_h)
%  +
%  \Frac{1}{\Delta t}
%  \int_\Omega
%    u_h^n
%    v_h
%    \,{\rm d}x
%  -
%  \int_\Omega
%    G_h\left(u_h^n\right)
%    \,v_h
%    \,{\rm d}x
%\end{eqnarray*}
%This scheme is more stable but requires very small time
%step to be precise.
%The best solution is to use semi-implicit high order time scheme.
% ----------------------------------------------------------
\subsubsection*{Time discretization}
% ----------------------------------------------------------
\cindex{method!Runge-Kutta scheme}%
Explicit Runge-Kutta scheme is possible for this problem
but it leads to an excessive Courant-Friedrichs-Levy
condition for the time step $\Delta t$, that is required to
be lower than an upper bound that varies in $\mathcal{O}(h^2)$.
The idea here is to continue to explicit the first order 
nonlinear terms and implicit the linear second order terms.
Semi-implicit second order Runge-Kutta scheme was 
first introduced
by \citet*{AscRuuSpi-1997}
and then extended to third and fourth order
by \citet*{CalFruNov-2001}.
\citet*{WanShuZha-2015,WanShuZha-2015-b}
applied it in the context of the discontinuous Galerkin method.
The finite dimensional problem can be rewritten as

\ \ $(P)_h$: \emph{find $u_h\in C^1([0,T],X_h)$ such that}
\begin{eqnarray*}
    \Frac{\partial u_h}{\partial t}
    +
    G_h(t,u_h)
    +
    A_h(t,u_h)
    &=&
    0
    , \ \ \forall t \in ]0,T[
    \\
    u_h(t\!=\!0) = \pi_h(u_0)
\end{eqnarray*}
where $G_h$ has been
introduced in~\eqref{eq-dg-hyp-def-Gh},
       page~\pageref{eq-dg-hyp-def-Gh}
and $A_h$ denotes the diffusive term.
The semi-implicit Runge-Kutta scheme with $p \geq 0$ intermediate steps
writes at time step $t_n$:
\begin{subequations}
\begin{eqnarray}
   u_h^{n,0} 
	&=&
	u_h^n
	\label{eq-dg-hyp-rk-semiimplicit-0}
	\\
   u_h^{n,i}
	&=&
	u_h^n
	-
        \Delta t 
	\sum_{j=1}^{i}
	    \alpha_{i,j} 
	    A_h \left(t_{n,j},\, u_h^{n,j} \right)
	-
        \Delta t 
	\sum_{j=0}^{i-1}
	    \tilde{\alpha}_{i,j} 
	    G_h \left(t_{n,j},\, u_h^{n,j} \right)
	, \ \ i=1,\ldots,p
	\label{eq-dg-hyp-rk-semiimplicit-i}
	\\
   u_h^{n+1}
	&=&
	u_h^n
	-
        \Delta t 
	\sum_{i=1}^{p}
	    \beta_{i} 
	    A_h \left(t_{n,i},\, u_h^{n,i} \right)
	-
        \Delta t 
	\sum_{i=0}^{p}
	    \tilde{\beta}_{i} 
	    G_h \left(t_{n,i},\, u_h^{n,i} \right)
	\label{eq-dg-hyp-rk-semiimplicit-final}
\end{eqnarray}
\end{subequations}
where $\left(u_h^{n,i}\right)_{1\leq i\leq p}$ are the $p \geq 1$ intermediate states,
\mbox{$
	t_{n,i}=t_n + \gamma_i\Delta t
$},
\mbox{$
	\gamma_i
	= \sum_{j=1}^i \alpha_{i,j}
	= \sum_{j=0}^{i-1} \tilde{\alpha}_{i,j}
$},
and
$ (\alpha_{i,j})_{0\leq i,j\leq p} $,
$ (\tilde{\alpha}_{i,j})_{0\leq i,j\leq p} $,
$ (\beta_{i})_{0\leq i\leq p} $
and
$ (\tilde{\beta}_{i})_{0\leq i\leq p} $
are the coefficients of the scheme \citep{AscRuuSpi-1997,CalFruNov-2001,WanShuZha-2015}.
At each time step, have to solve $p$ linear systems.
From~\eqref{eq-dg-hyp-rk-semiimplicit-i} we get for all $i=1,\ldots,p$:
\begin{eqnarray*}
   \left(
     I
     +
     \Delta t 
     \,\alpha_{i,i} 
     A_h \left(t_{n,i}\right)
   \right)
   u_h^{n,i}
	&=&
	u_h^n
	-
        \Delta t 
	\sum_{j=1}^{i-1}
	    \alpha_{i,j} 
	    A_h \left(t_{n,j},\, u_h^{n,j} \right)
	-
        \Delta t 
	\sum_{j=0}^{i-1}
	    \tilde{\alpha}_{i,j} 
	    G_h \left(t_{n,j},\, u_h^{n,j} \right)
\end{eqnarray*}
Note that when the matrix coefficients of $A_h(t,.)$
are indepencdent of $t$, the matrix involved on the
right-hand-side of the previous equation can be
factored one time for all.

% ---------------------------------------------------------
\myexamplenoinput{runge_kutta_semiimplicit.icc}

\myexamplelicense{burgers_diffusion_dg.cc}

\myexamplelicense{burgers_diffusion_operators.icc}
% ---------------------------------------------------------
The included file~\file{runge_kutta_semiimplicit.icc} is not shown here
but is available in the example directory.

\subsubsection*{Running the program}

\begin{figure}[htb]
     \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=6cm]{burgers-diffusion-rk-semiimplicit-line2-400-eps-1e-3.pdf} &
          \includegraphics[height=6cm]{burgers-diffusion-rk-semiimplicit-limiter-line2-400-eps-1e-3.pdf} 
       \end{tabular}
     \end{center}
     \caption{Burgers equation with a small diffusion ($\varepsilon=10^{-3}$).
	Third order in time semi-implicit scheme with $P_{1d}$ element.
	(left) without limiter ;
	(right) with limiter.
     }
   \label{fig-burgers-diffusion-small}
\end{figure}

Running the program writes with $h=2/400$ and $\varepsilon=10^{-2}$
writes:
\pindexopt{branch}{-gnuplot}%
\pindexopt{branch}{-umin}%
\pindexopt{branch}{-umax}%
\begin{verbatim}
  make burgers_diffusion_dg
  mkgeo_grid -e 400 -a -1 -b 1 > line.geo
  mpirun -np 8 ./burgers_diffusion_dg line P1d 0.01 1000 1 3 > line.branch
  branch -gnuplot line.branch -umin -0.1 -umax 2.1
\end{verbatim}
It could take about one minute, depending on your computer.
Decreasing $\varepsilon=10^{-3}$ leads to a sharper solution:
\begin{verbatim}
  mpirun -np 8 ./burgers_diffusion_dg line P1d 0.001 1000 1 3 > line.branch
  branch -gnuplot line.branch -umin -0.1 -umax 2.1
\end{verbatim}
As mentioned by \citet{WanShuZha-2015}, the time step should be chosen
smaller when $\varepsilon$ decreases.
%We use here $1000$ time steps with $\varepsilon=10^{-3}$.
The result is shown on Fig.~\ref{fig-burgers-diffusion-small}.left.
Observe the oscillations near the smoothed shock when there is
no limiter while the value goes outside $[0,2]$.
Conversely, with a limiter
(see Fig.~\ref{fig-burgers-diffusion-small}.right)
the approximate solution
is decreasing and there are no more oscillations:
the values remains in the range $[0:2]$.
