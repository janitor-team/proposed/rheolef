%
% This file is part of Rheolef.
%
% Copyright (C) 2000-2009 Pierre Saramito 
%
% Rheolef is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% Rheolef is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Rheolef; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
% -------------------------------------------------------------------
% ---------------------------------------------
\subsection{\new Example: the Leveque vortex-in-box}
% ---------------------------------------------
\label{sec-leveque-dg}%
\apindex{discontinuous}%
\cindex{problem!transport equation!evolution}%
\cindex{benchmark!Leveque vortex-in-box}%

The \citet[p.~653]{Lev-1996} vortex-in-box
is also another widely used benchmark.
This example tests the ability of the scheme to accurately resolve thin filaments on the scale of the mesh
which can occur in swirling and stretching flows.
An initial shape is deformed by a non-constant velocity field given by:
\begin{empheq}{align*}
  \boldsymbol{u}(t,\boldsymbol{x})
  &=
  \left\{ \begin{array}{ll}
    \cos(\pi t/t_f)
    \left( \begin{array}{r}
       \sin^2( \pi x_0)
       \sin  (2\pi x_1)
       \\
      -\sin  (2\pi x_0)
       \sin^2( \pi x_1)
    \end{array} \right)
    & \mbox{ when } d=2
    \medskip \\
    \cos(\pi t/t_f)
    \left( \begin{array}{r}
       2
       \sin^2( \pi x_0)
       \sin  (2\pi x_1)
       \sin  (2\pi x_2)
       \\
       -
       \sin  (2\pi x_0)
       \sin^2( \pi x_1)
       \sin  (2\pi x_2)
       \\
       -
       \sin^2( \pi x_0)
       \sin  (2\pi x_1)
       \sin  (2\pi x_2)
    \end{array} \right)
    & \mbox{ when } d=3
  \end{array} \right.
\end{empheq}
At the half-period~$t=t_f/2$,
the initial data is quite deformed and
the flow slows down and reverses direction in such a way
that the initial data should be recovered at time~$t_f$.
This gives a very useful test problem since we then know the true solution at time~$t_f$
even though the flow field has a quite complicated structure.
Here we use~$t_f=8$ when~$d=2$ and~$t_f=3$ when~$d=3$.
%
The initial shape is a circle 
of radius~$0.15$ placed at~$(0.5,0.75)$ when~$d=2$
and a sphere
of radius~$0.15$ placed at~$(0.35,0.35,0.35)$ when~$d=3$.
The tridimensional flow field was first proposed by \citet[p.~662]{Lev-1996}
and the corresponding initial shape described by \citet[p.~112]{EnrFedFerMit-2002}.
%
The computational domain is~\mbox{$\Omega=]0,1[^d$} and note that
the flow field~$\boldsymbol{u}$ vanishes on~$\partial\Omega$.

% ---------------------------------------
\myexamplenoinput{leveque.h}

\myexamplelicense{leveque_dg.cc}
% ---------------------------------------

\subsubsection*{Comments}
Numerical computations are performed on the time interval~$[0,T]$
with a time step~$\Delta t=T/n_{max}$ where~$n_{max}$ is the number of time steps.
%
Observe that~$\boldsymbol{u}$ is here time-dependent.
Then~$a_n$, as given by~\eqref{eq-transport-bdf-an},
is also dependent upon~$n$, 
and~$a_n$ can no more be factored one time for all,
as it was done for the Zalesak case.
The file \file{leveque.h} included here implements both
the~\code{u} and~\code{phi0} class-functions for the flow field
and the initial data~$\phi_0$ associated to the initial circular shape, respectively.
This file it is not listed here but is available in the \Rheolef\  example directory.
%
Recall that~$\boldsymbol{u}$ vanishes on~$\partial\Omega$.
Then, all boundary terms for~$a_n$ and~$\ell_n$,
involved in~\eqref{eq-transport-bdf-an}-\eqref{eq-transport-bdf-ln},
are here omitted.

\begin{figure}[htb]
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[width=0.45\textwidth]{leveque-100-P2d-6000-half-subdiv3.jpg} &
      \includegraphics[width=0.45\textwidth]{leveque-100-P2d-6000-last.jpg} 
    \end{tabular}
  \end{center}
  \caption{Leveque vortex-in-box:
    approximation with~$h=1/100$, $k=2$, $p=3$ and~$\Delta t=4\pi/6000$.
    (left) solution at half period~: the deformation is maximal~;
    (right) after a full period~: comparison with the initial shape, in red.
  }
  \label{fig-leveque-visu}
\end{figure}

% ------------------------------
\subsubsection*{How to run the program}
% ------------------------------
Compilation and run writes:
\begin{verbatim}
  mkgeo_ugrid -t 100 > square.geo
  make leveque_dg
  mirun -np 8 ./leveque_dg square P2d 6000 > square-P2d.branch
\end{verbatim}
\pindex{mpirun}%
The computation could take about two hours, depending upon your computer:
a prompt shows the advancement for the~6000 time steps.
The \code{mpirun -np 8} prefix is optional and you should omit it
if you are running a sequential installation of \Rheolef.
\pindexopt{branch}{-iso}%
\pindexopt{branch}{-bw}%
Then, the visualization writes:
\begin{verbatim}
  branch square-P2d.branch -iso 0 -bw
\end{verbatim}
As for the Zalesak case, the solution is represented 
by an animation with \code{paraview}.
The video could be directly observed from the link (7 Mo):
\begin{center}
  \url{http://www-ljk.imag.fr/membres/Pierre.Saramito/rheolef/leveque-100-P2d-6000.mp4}
\end{center}
Fig.~\ref{fig-leveque-visu}.left represents the solution when~$h=1/100$
and~$6000$ time steps at the half-period~$t=t_f/2$,
when the deformation is maximal.
Conversely, Fig.~\ref{fig-leveque-visu}.right represents it
when~$t=t_f$, when the shape recovers its initial position.
Observe the good correspondence between the final and the initial shapes,
which is represented in red.
