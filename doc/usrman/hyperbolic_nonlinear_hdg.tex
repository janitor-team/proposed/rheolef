% --------------------------------------------
\subsection{Nonlinear scalar hyperbolic problems revisited}
% --------------------------------------------
\label{sec-hyperbolic-hdg}%
\apindex{discontinuous!hybrid}%
\cindex{problem!hyperbolic nonlinear equation}

The aim of this paragraph is to study the
hybrid discontinuous Galerkin discretization
of scalar nonlinear hyperbolic equations.
This section reuses the general framework already
introduced in section~\ref{sec-hyperbolic-hdg}.

The stationary nonlinear hyperbolic problem writes:

\ \ $(P)$: \emph{find $u$, defined in $\Omega$, such that}
\begin{subequations}
\begin{eqnarray}
  {\rm div}\,\boldsymbol{f}(u)
  +
  \sigma u
  &=&
  f
  \ \mbox{ in } \Omega
	\label{eq-hdg-hyp-eqn}
  	\\
  \boldsymbol{f}(u).\boldsymbol{n} &=& \Phi(\boldsymbol{n};\,u,g)
  \mbox{ on } \partial\Omega
	\label{eq-hdg-hyp-bc}
\end{eqnarray}
\end{subequations}
with the notations of section~\ref{sec-hyperbolic-hdg}
and where $\sigma\geq 0$ is an optional source term parameter 
and $f$ is a right-hand side.
For simplicity, we consider here the stationary problem:
its extension to the non-stationary case will be considered later.
It could be done either as section~\ref{sec-hyperbolic-hdg},
with Runge-Kutta schemes, or, and it will be a new feature, implicitely,
based on BDF schemes and a Newton algorithm.

Multiplying \eqref{eq-hdg-hyp-eqn} by a test-function $v$
and integrating on $K\in\mathscr{T}_h$, we get:
\begin{eqnarray}
  \int_K
    \left(
      \sigma u v
      -
      \boldsymbol{f}(u).\nabla v
    \right) \,{\rm d}x
  +
  \int_{\partial K}
    \boldsymbol{f}(u).\boldsymbol{n}
    \, v
    \,{\rm d}s
  &=&
  \int_K
    f \, v
    \,{\rm d}x
  \label{eq-hdg-hyp-integ-part} 
\end{eqnarray}
By definition of the Godunov flux $\Phi$ associated to $\boldsymbol{f}$,
a boundary condition similar to \eqref{eq-hdg-hyp-bc} 
applies on $\partial K$:
\begin{eqnarray*}
  \boldsymbol{f}(u).\boldsymbol{n} &=& \Phi(\boldsymbol{n};\,u,u_{ext})
  \mbox{ on } \partial K
\end{eqnarray*}
where $u_{ext}$ denotes the external trace of $u$ on the boundary sides of $K$.

On all internal sides, let $\lambda=\average{u}$.
The $\lambda$ notation is now considered as an indpendent variable
and the constraint $\lambda-\average{u}=0$ will be imposed
by using a Lagrange multiplier.
We extends $\lambda$ on the boundary side by using 
the boundary condition \eqref{eq-hdg-hyp-bc}.
For instance, for a transport problem,
we get $\lambda=g$ on the inflow boundary domain
and, on the rest of the boundary, $\lambda$, is the inner trace of $u$.
Let $S=\partial K_-\cap K_+$ be any internal side with orientation
such that the normal on $S$ is $\boldsymbol{n}=\boldsymbol{n}_-=-\boldsymbol{n}_+$
(See Fig.~\ref{fig-dg-neighbor},
 page~\pageref{fig-dg-neighbor}).
Let $u_-$ and $u_+$ denotes the two corresponding traces of $u$ on $S$.
Then
\mbox{$\lambda=(u_-+u_+)/2$}, or equivalently
\mbox{$u_+=2\lambda-u_-$},
and thus the Godunov flux
$\Phi(\boldsymbol{n};u_-,u_+)$
can be expressed equivalently in terms of $\lambda$ and $u_-$.

Going back to an element $K\in\mathscr{T}_h$ with
$u_{ext}$ as the external trace of $u$ on $\partial K$,
we have \mbox{$u_{ext}=2\lambda-u$}
and \eqref{eq-hdg-hyp-integ-part} becomes:
\begin{eqnarray*}
  \int_K
    \left(
      \sigma u v
      -
      \boldsymbol{f}(u).\nabla v
    \right)
    \,{\rm d}x
  +
  \int_{\partial K}
    \Phi(\boldsymbol{n};\,u,\,2\lambda\!-\!u)
    \, v
    \,{\rm d}s
  &=&
  \int_K
    f \, v
    \,{\rm d}x
\end{eqnarray*}
Observe that, as $\lambda$ is now an independent variable,
the previous equation dependends only upon the value of $u$
inside the element $K$ and no more upon its external trace.
So, the problem has the desired block-diagonal structure at the
element level.


The discontinuous finite element space are defined by:
\begin{eqnarray*}
   X_h &=& \{ \varphi_h \in L^2(\Omega); \ \varphi_{h|K}\in P_k, \ \forall K \in \mathscr{T}_h \} \\
   M_h &=& \{ \mu \in L^2(\mathscr{S}_h); \ \mu_{h,S}\in P_k, \forall S \in \mathscr{S}_h \}
\end{eqnarray*}
where $k\geq 0$ is the polynomial degree
and $\mathscr{S}_h$ denotes the set of all sides in the mesh $\mathscr{T}_h$.
Remark that on any internal side,
a function $\varphi_h\in M_h$ is bi-valued while a 
function $\mu_h\in M_h$ has only one value.

After summation of the previous equation over all $K\in\mathscr{T}_h$,
the discrete variational formulation of the problem writes:

\ \ $(FV)_h$: \emph{find $u_h\in X_h$ and $\lambda_h \in M_h$ such that}
\begin{eqnarray*}
  \int_\Omega
    \left(
      \sigma u_h v_h
      -
      \boldsymbol{f}(u_h).\nabla_h v_h
    \right)
    \,{\rm d}x
  +
  \sum_{K\in\mathscr{T}_h}
  \int_{\partial K}
    \Phi(\boldsymbol{n};\,u_h,\,2\lambda_h\!-\!u_h)
    \, v_h
    \,{\rm d}s
  &=&
  \int_\Omega
    f \, v_h
    \,{\rm d}x
  \\
  \int_{\mathscr{S}_h^{(i)}}
    (\average{u_h}-\lambda_h)
    \,\mu_h
    \,{\rm d}s
  +
  \int_{\partial\Omega}
    (\boldsymbol{f}(\lambda_h).\boldsymbol{n} - \Phi(\boldsymbol{n};\,\lambda_h,g))
    \,\mu_h
    \,{\rm d}s
  &=&
  0
\end{eqnarray*}
for all $v_h\in X_h$ and $\mu_h \in M_h$.
The second equation expresses that,
on all internal sides, we impose $\lambda_h=\average{u_h}$
by using the Lagrange multiplier $\mu_h$.
This multiplier extends on the boundary for imposing
the boundary condition \eqref{eq-hdg-hyp-bc} for the 
Lagrange multiplier $\lambda_h$.

\red{TODO: with this boundary condition treatment, $\lambda_h$ is undefined on the outflow boundary part.}

In the next section, we turn to the concrete example
of the linear scalar transport equation.
