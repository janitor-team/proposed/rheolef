set terminal cairolatex pdf color standalone
set output "oldroyd-contraction-zr-psi-max.tex"

set size square
set colors classic
set key bottom
set xlabel '[c]{$We$}'
set  label '[l]{$\psi_{max}$}' at graph 0.03, 0.93
set xrange [0:0.7]
set yrange [0:0.02]
set xtics 0.1
set ytics 0.01

plot \
'oldroyd-contraction-zr-psi-max.gdat' \
  i 2 \
  notitle \
  w lp lw 4 lc 0

#pause -1
