set terminal cairolatex pdf color standalone
set output "yield-slip-n-0,5-S-0,6.tex"

set logscale y
set colors classic
set size square
set xrange [0:12]
set xtics 2
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$10^{0}$}" 1 )
set xlabel "[c]{$n$}"
set  label "[r]{\\Large $\\|r_h\\|_{L^2(\\partial\\Omega)}$}" at graph -0.05,0.5

plot [0:11][1e-16:1e1] \
"yield-slip-n-10-n-0.5-S-0.6.gdat" title "[r]{$10\\times 10$}" w lp lw 2, \
"yield-slip-n-20-n-0.5-S-0.6.gdat" title "[r]{$20\\times 20$}" w lp lw 2, \
"yield-slip-n-30-n-0.5-S-0.6.gdat" title "[r]{$30\\times 30$}" w lp lw 2, \
"yield-slip-n-40-n-0.5-S-0.6.gdat" title "[r]{$40\\times 40$}" w lp lw 2, \
"yield-slip-n-50-n-0.5-S-0.6.gdat" title "[r]{$50\\times 50$}" w lp lw 2

#pause -1 "<retour>"
