set terminal cairolatex pdf color standalone
set output "p-laplacian-rate-log.tex"

set size square 1.0
set colors classic

set xtics (\
	"[c]{$10^{-3}$}" 1e-3, \
	"[c]{$10^{-2}$}" 1e-2, \
	"[c]{$10^{-1}$}" 1e-1, \
	"[c]{$10^{0}$}" 1 )
set ytics (0,1,2)

set xlabel "[c]{$|p-2|$}"
set  label "[r]{\\Large $\\bar{v}$}" at graph -0.08,0.95

set logscale x
plot [1e-3:1][0:3] \
  "p-laplacian-rate.gdat" i 0 u (2-$1):2 \
	title "[r]{computation: $p<2$}" w l dt 1 lc rgb '#008800' lw 4, \
  "p-laplacian-rate.gdat" i 1 u ($1-2):2 \
	title "[r]{computation: $p>2$}" w l dt 1 lc 1 lw 4, \
  -log10(x) \
	title '[r]{fit: $-\log_{10}\,|p-2|$}' w l dt 3 lw 1 lc 0

#pause -1 "<retour>"
