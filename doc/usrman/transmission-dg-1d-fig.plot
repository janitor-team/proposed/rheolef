set terminal cairolatex pdf color standalone
set output "transmission-dg-1d-fig.tex"

set colors classic
set xtics (0, 0.5, 1)
set ytics (0, 1, 2, 3)
epsilon = 0.01

a1 = - 0.25*(1.0 + 3.0*epsilon)/(1.0 + epsilon)
b1 = a1
b2 = -b1 - 0.5

u1(x) = -(x*x/2 + a1*x)/epsilon
u2(x) = -(x*x/2 + b1*x + b2)

u(x) = ((x < 0.5) ? u1(x) : u2(x))

plot [0:1][0:3.5]  \
        "transmission-dg-line-10.gdat" title '[r]{$h=1/10$}'    w lp dt 1 lw 2 lc rgb '#008800' pt 7 ps 0.2, \
	"transmission-dg-line-6.gdat"  title '[r]{$h=1/6\ \,$}' w lp dt 1 lw 4 lc 1 pt 7 ps 0.2, \
	u(x) title '[r]{exact}'                                 w l       lw 2 lc 0 dt 1

#\
	"transmission-dg-line-14.gdat" title '[r]{$h=1/14$}'    w lp dt 4 lw 3 lc 3 pt 7

# pause -1 "<return>"

