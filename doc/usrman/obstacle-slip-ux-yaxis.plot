set terminal cairolatex pdf color standalone
set output "obstacle-slip-ux-yaxis.tex"

set size square
set key bottom left

set xrange [-2:0]
set yrange [1:2]
set xtics 1
set ytics 1
set xlabel '[c]{$1-u_0(x_1)$}'
set  label '[r]{$x_1$}' at graph -0.03, 0.5
plot \
"obstacle-slip-ux-yaxis.gdat" \
  u (1-$3):2 \
  t 'cylinder' \
  with l lw 2 lc rgb '#000000', \
"obstacle-zr-slip-ux-yaxis.gdat" \
  u (1-$3):2 \
  t 'sphere' \
  with l lw 2 lc rgb '#ff0000'

#pause -1 "<return>"
