set terminal cairolatex pdf color standalone
set output "harten-Pk-err.tex"

a0  = 0.848211  #      +/- 0.005758     (0.6788%)
b0  = 0.186869  #      +/- 0.01273      (6.813%)
log10_err_p0(log10_h) = a0*log10_h + b0
#fit log10_err_p0(x) "harten-P0-err.gdat" u (log10(2./$1)):(log10($3)) via a0,b0
err_p0(h) = 10.0**log10_err_p0(log10(h))

set size square 
set colors classic
set log
set key bottom
set xrange [1e-3:1e-1]
set yrange [1e-3:1e-1]
graph_ratio = 2.0/2.0
set xtics (\
	'[c]{$10^{-3}$}' 1e-3, \
	'[c]{$10^{-2}$}' 1e-2, \
	'[c]{$10^{-1}$}' 1e-1)
set ytics (\
	'[r]{$10^{-3}$}' 1e-3, \
	'[r]{$10^{-2}$}' 1e-2, \
	'[r]{$10^{-1}$}' 1e-1)
set xlabel '[c]{$h$}'
set  label '[l]{$\|u-u_h\|_{L^{\infty}(0,T;L^1)}$}' at graph 0.02, 0.95
set  label '[l]{(b) $T=5/2$}' at graph 0.08, 0.85

# triangle a droite
slope_A = graph_ratio*a0
xA =  0.20
yA =  0.46
dxA = 0.10
dyA = dxA*slope_A
set label sprintf("[l]{\\scriptsize $%.2g$}",a0) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
slope_B = graph_ratio*1.0
xB =  0.20
yB =  0.09
dxB = 0.10
dyB = dxB*slope_B
set label "[l]{\\scriptsize $1$}" at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

plot \
  "harten-P0-err.gdat" \
        using (2./$1):3 \
	t '[r]{$k=0$}' \
	w lp lt 1 lw 2 lc 0, \
  "harten-P1d-err.gdat" \
        using (2./$1):3 \
	t '[r]{$k=1$}' \
	w lp lt 1 lw 2 lc 1

