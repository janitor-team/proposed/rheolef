%------------------------------------
\subsection{\new Stress diffusion}
%------------------------------------
\label{sec-stres-diffusion}%
\apindex{discontinuous}%
The tensor diffusion variant of a viscoelastic model introduces
an additional second order diffusive term in the differential
equation for~$\boldsymbol{\sigma}$, see section~\ref{sec-oldroyd-transport}.
It develops nice theoretical properties, see e.g.~\cite{LukMizNecRen-2017,MalPruSkrSul-2018}.
%
Let us consider 
the following variant of the tensor transport problem previously introduced
in section~\ref{sec-oldroyd-transport},
 page~\pageref{sec-oldroyd-transport},
by adding a diffusion term:

\ \ $(P)$: \emph{find $\boldsymbol{\sigma}$, defined in $]0,T[\times \Omega$, such that}
\begin{eqnarray*}
  \Frac{\mathscr{D}_a\boldsymbol{\sigma}}{\mathscr{D}t}
  + \nu \boldsymbol{\sigma}
  - \varepsilon\Delta\boldsymbol{\sigma}
  &=& \boldsymbol{\chi} \ \mbox{ in } ]0,T[\times \Omega \\
  \boldsymbol{\sigma} &=& \boldsymbol{\sigma}_\Gamma \ \mbox{ on } ]0,T[\times \partial\Omega_{-} \\
  \boldsymbol{\sigma}(0) &=& \boldsymbol{\sigma}_0 \ \mbox{ in } \Omega
\end{eqnarray*}
where~$\varepsilon\geq 0$ is the diffusion parameter.
Observe that when~$\varepsilon=0$, this problem reduces to
the usual one, without diffusion, as introduced in section~\ref{sec-oldroyd-transport}.

The steady version of the tensor transport problem writes:

\ \ $(S)$: \emph{find $\boldsymbol{\sigma}$, defined in $\Omega$, such that}
\begin{eqnarray*}
  \left(
            {\bf u}.\nabla)\boldsymbol{\sigma}
            +
            \boldsymbol{\sigma} \boldsymbol{g}_a({\bf u}
            +
            \boldsymbol{g}_a^T({\bf u}) \boldsymbol{\sigma}
  \right)
  + \nu \boldsymbol{\sigma}
  - \varepsilon\Delta\boldsymbol{\sigma}
  &=& \boldsymbol{\chi} \ \mbox{ in } \Omega \\
  \boldsymbol{\sigma} &=& \boldsymbol{\sigma}_\Gamma \ \mbox{ on } \partial\Omega_{-}
\end{eqnarray*}
We introduce the forms defined
for all $\boldsymbol{\sigma},\boldsymbol{\tau}\in H^1(\Omega)^{d\times d}_s$ by 
\begin{equation*}
  \begin{array}{rcccl}
  a(\boldsymbol{\sigma},\boldsymbol{\tau}) &=& 
	{\displaystyle
          \int_\Omega 
	    \left(
              ({\bf u}.\nabla) \boldsymbol{\sigma}
              +
              \boldsymbol{\sigma} \boldsymbol{g}_a({\bf u})
              +
              \boldsymbol{g}_a^T({\bf u}) \boldsymbol{\sigma}
	      +
	      \nu \,
              \boldsymbol{\sigma}
	    \right)
            \!:\! \boldsymbol{\tau}
	    \, {\rm d}x
        }
	&+&
	{\displaystyle
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n}\right)
	    \boldsymbol{\sigma} \!:\! \boldsymbol{\tau} \, {\rm d}s
        }
	\\
        && +\ 
	{\displaystyle
          \int_\Omega 
            \varepsilon
            \nabla \boldsymbol{\sigma}
            \!:\!
            \nabla \boldsymbol{\tau}
	    \, {\rm d}x
        }
        &&
	\\
  l(\boldsymbol{\tau}) &=& 
	{\displaystyle
  	  \int_\Omega 
	    \boldsymbol{\chi} \!:\! \boldsymbol{\tau} \, {\rm d}x
        }
        &+&
	{\displaystyle
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n}\right)
	    \boldsymbol{\sigma}_\Gamma \!:\! \boldsymbol{\tau} \, {\rm d}s
        }
 \end{array}
\end{equation*}
Then, the variational formulation of the steady problem
writes:

\ \ $(FV)$: \emph{find $\boldsymbol{\sigma}\in X$ such that}
\begin{eqnarray*}
  a(\boldsymbol{\sigma},\boldsymbol{\tau}) &=& l(\boldsymbol{\tau})
  , \ \forall \boldsymbol{\tau}\in H^1(\Omega)^{d\times d}_s
\end{eqnarray*}
The discontinuous finite element space is then defined by:
\[ 
   X_h = \{ \boldsymbol{\tau}_h \in H^1(\Omega)^{d\times d}_s; \boldsymbol{\tau}_{h|K}\in P_k, \ \forall K \in \mathcal{T}_h \}
\]
where $k\geq 0$ is the polynomial degree.
The discrete version $a_h$ of the bilinear form $a$
is defined for all $\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h\in X_h$ by:
\begin{eqnarray*}
  a_h(\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
    	&=& 
    	t_h(\boldsymbol{u};\,\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
	+
        \varepsilon
    	d_h(\boldsymbol{u};\,\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
	+
	\nu
        \int_\Omega
	    \boldsymbol{\sigma}_h
            \!:\!\boldsymbol{\tau}_h
	    \, {\rm d}x
	\nonumber
  \\
  t_h(\boldsymbol{u};\,\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
  	&=& 
        \int_\Omega
	    \left(
              ({\bf u}.\nabla_h) \boldsymbol{\sigma}_h
              +
              \boldsymbol{\sigma} \boldsymbol{g}_a({\bf u})
              +
              \boldsymbol{g}_a^T({\bf u}) \boldsymbol{\sigma}
	    \right)
            \!:\!\boldsymbol{\tau}_h
	    \, {\rm d}x
        +
	  \int_{\partial\Omega}
	    \max\left(0, -{\bf u}.{\bf n} \right)
	    \boldsymbol{\sigma}_h \!:\! \boldsymbol{\tau}_h
	    \, {\rm d}s
	\nonumber
	\\
	&& + \ 
        \sum_{S \in \mathscr{S}_h^{(i)}}
          \int_S
	    \left(
	      -\ 
	      {\bf u}.{\bf n} \,
	      \jump{\boldsymbol{\sigma}_h} \!:\! \average{\boldsymbol{\tau}_h}
              +
	      \Frac{\alpha}{2}
	      |{\bf u}.{\bf n}| \,
	      \jump{\boldsymbol{\sigma}_h} \!:\! \jump{\boldsymbol{\tau}_h}
            \right)
	    \, {\rm d}s
	\nonumber
  \\
  d_h(\boldsymbol{u};\,\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h)
  	&=& 
        \int_\Omega
            \nabla_h \boldsymbol{\sigma}_h
            \!:\!
            \nabla_h \boldsymbol{\tau}_h
	    \, {\rm d}x
        \nonumber
        \\
        &&\ 
	+
        \sum_{S \in \mathscr{S}_h}
          \int_S
            \left(
            \kappa_s 
            \jump{\boldsymbol{\sigma}_h}
            \!:\!
            \jump{\boldsymbol{\tau}_h}
            - 
            \jump{\boldsymbol{\sigma}_h}
            \!:\!
            \average{\nabla_h\boldsymbol{\tau}_h\boldsymbol{n}}
            - 
            \jump{\boldsymbol{\tau}_h}
            \!:\!
            \average{\nabla_h\boldsymbol{\sigma}_h\boldsymbol{n}}
            \right)
	    \, {\rm d}s
	\nonumber
  \\
  l_h(\boldsymbol{\sigma}_h)
  	&=& 
        \int_\Omega
	  \boldsymbol{\chi} \!:\! \boldsymbol{\tau}
          \, {\rm d}x
        +
	\int_{\partial\Omega}
	  \max\left(0, -{\bf u}.{\bf n}\right)
	  \boldsymbol{\sigma}_\Gamma \!:\! \boldsymbol{\tau}
          \, {\rm d}s
        \\
        &&
        +\ 
        \varepsilon
	\int_{\partial\Omega}
          \left(
            \kappa_s
	    \boldsymbol{\sigma}_\Gamma \!:\! \boldsymbol{\tau}
            -
	    \boldsymbol{\sigma}_\Gamma \!:\! (\nabla_h\boldsymbol{\tau}\boldsymbol{n})
            +
	    \boldsymbol{\tau} \!:\! (\nabla\boldsymbol{\sigma}_\Gamma\boldsymbol{n})
          \right)
          \, {\rm d}s
\end{eqnarray*}
where
$\boldsymbol{\sigma}_\Gamma$ is given
and
\mbox{$
  \kappa_s = \beta \,\varpi_s
$}
is the stabilisation parameter,
with
\mbox{$
  \beta = (k+1)(k+d)/d
$}
and~$\varpi_s$ is the penalisation, see
 section~\ref{dirichlet-dg},
page~\pageref{dirichlet-dg}.

\ \ $(FV)_h$: \emph{find $\boldsymbol{\sigma}_h\in X_h$ such that}
\begin{eqnarray*}
  a_h(\boldsymbol{\sigma}_h,\boldsymbol{\tau}_h) &=& l_h(\boldsymbol{\tau}_h), \ \forall \boldsymbol{\tau}_h\in X_h
\end{eqnarray*}

The following code implement this problem in the \Rheolef\  environment.

% ---------------------------------------
\myexamplelicense{diffusion_transport_tensor_dg.cc}
% ---------------------------------------

% ---------------------------------------
\subsubsection*{Running the program}
% ---------------------------------------
Let $d=2$ and $\Omega = ]-1/2,\,1/2[^2$.
We consider the rotating field ${\bf u}=(-x_2,x_1)$.
A particular solution of the
time-dependent problem with zero right-hand side
is given by:
\[
       \boldsymbol{\sigma}(x,t)
        =
        \Frac{1}{2} \,
	\exp\left\{
		-\nu t
		-\Frac{(x_1-x_{1,c}(t))^2 + (x_2-x_{2,c}(t))^2}
		      {r_0^2}	
		\right\}
	\times 
        \left( \begin{array}{cc}
                1+\cos(2t) & \sin(2t) \\
                \sin(2t) & 1-\cos(2t)
        \end{array} \right)
\]
where
$
	x_{1,c}(t) = \bar{x}_{1,c} \cos(t) - \bar{x}_{2,c} \sin(t) 
$
and
$
	x_{2,c}(t) = \bar{x}_{1,c} \sin(t) + \bar{x}_{2,c} \cos(t)
$
with $r_0>0$ and $(\bar{x}_{1,c},\,\bar{x}_{2,c})\in\mathbb{R}^2$.
The form~$_h$ is chosen with $\boldsymbol{\sigma}_\Gamma=\boldsymbol{\sigma}$.
%
% ---------------------------------------
\myexamplenoinput{diffusion_transport_tensor_exact.icc}
% ---------------------------------------
This exact solution is implemented in the file \file{diffusion_transport_tensor_exact.icc}.
This file it is not listed here but is available in the \Rheolef\  example directory.
%
For the steady problem, the right-hand side could be chosen
as $\boldsymbol{\chi}=-\Frac{\partial \boldsymbol{\sigma}}{\partial t}$ and then $t=t_0$ is fixed.
%
The numerical tests correspond to
$\nu\!=\!3$,
$r_0\!=\!1/10$,
$(\bar{x}_{1,c},\,\bar{x}_{2,c})=(1/4,\,0)$
and a fixed time $t_0=\pi/8$.
\begin{verbatim}
  make diffusion_transport_tensor_dg
  mkgeo_grid -t 80 -a -0.5 -b 0.5 -c -0.5 -d 0.5 > square2.geo
  ./diffusion_transport_tensor_dg square2 P1d > square2.field
  field square2.field -comp 00 -elevation
  field square2.field -comp 01 -elevation
\end{verbatim}
The computation could also be performed with
any \code{Pkd}, with $k\geq 0$.
% ---------------------------------------
\subsubsection*{Error analysis}
% ---------------------------------------

  \begin{figure}[htb]
     \begin{center}
          \includegraphics[height=7cm]{diffusion-transport-tensor-err.pdf}
     \end{center}
     \caption{Diffusion-diffusion tensor problem: convergence versus mesh size.}
     \label{fig-diffusion-transport-tensor-err}
  \end{figure}
% ---------------------------------------
\myexamplenoinput{diffusion_transport_tensor_error_dg.cc}
% ---------------------------------------
The file \file{diffusion_transport_tensor_error_dg.cc} implement the computation of the error
between the approximate solution $\boldsymbol{\sigma}_h$ and the exact one $\boldsymbol{\sigma}$.
This file it is not listed here but is available in the \Rheolef\  example directory.
The computation of the error is obtained by:
\begin{verbatim}
  make diffusion_transport_tensor_error_dg
  ./diffusion_transport_tensor_error_dg < square2.field
\end{verbatim}
The error is plotted on Fig.~\ref{fig-diffusion-transport-tensor-err} for various
mesh size~$h$ and polynomial order~$k$: observe the optimality of the
convergence properties.
% For $k=4$ and on the finest mesh, the error saturates at about $10^{-8}$,
% due to finite machine precision effects.

