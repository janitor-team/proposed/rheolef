set terminal cairolatex pdf color standalone
set output "dirichlet-hho-k-k-t-us-cpu.tex"
gdat     = 'dirichlet-hho-k-k-t.gdat'
std      = 'dirichlet2-t.gdat'

set log xy
set size square
set colors classic
set xrange [1e-2:1e4]
set yrange [1e-10:1e2]
set xlabel '[c]{$t_{cpu} (sec)$}'
set  label '[l]{\large $\|u_h^* - u\|_{0,2,\Omega}$}' at graph 0.05, 0.92
set ytics (\
        '[r]{$1$}'        1,     \
        '[r]{$10^{-5}$}'  1e-5,  \
        '[r]{$10^{-10}$}' 1e-10)

plot \
gdat \
 i 0 u 9:6 \
 t '[r]{$k=0$}' \
 w lp lw 2 pt 1 ps 0.5 lc '#ff0000' dt 1, \
std \
 i 0 u 5:2 \
 notitle \
 w lp lw 2 pt 1 ps 0.5 lc '#ff0000' dt 2, \
gdat \
 i 1 u 9:6 \
 t '[r]{$1$}' \
 w lp lw 2 pt 2 ps 0.5 lc '#008800' dt 1, \
std \
 i 1 u 5:2 \
 notitle \
 w lp lw 2 pt 2 ps 0.5 lc '#008800' dt 2, \
gdat \
 i 2 u 9:6 \
 t '[r]{$2$}' \
 w lp lw 2 pt 3 ps 0.5 lc '#0000ff' dt 1, \
std \
 i 2 u 5:2 \
 notitle \
 w lp lw 2 pt 3 ps 0.5 lc '#0000ff' dt 2, \
gdat \
 i 3 u 9:6 \
 t '[r]{$3$}' \
 w lp lw 2 pt 4 ps 0.5 lc '#000000' dt 1, \
std \
 i 3 u 5:2 \
 notitle \
 w lp lw 2 pt 4 ps 0.5 lc '#000000' dt 2

#pause -1
