\subsection{The heat equation}
\label{sec-heat}
\pbindex{heat}%
\cindex{method!Euler implicit scheme}%

\subsubsection*{Formulation}
% -----------------------
Let $T>0$, $\Omega \subset \mathbb{R}^d$, $d=1,2,3$
and $f$ defined in $\Omega$.
The heat problem writes:
\begin{quote}
	$(P)$: {\em find $u$, defined in $\Omega \times ]0,T[$, such that}
\end{quote}
\begin{eqnarray*}
	\Frac{\partial u}{\partial t} - \Delta u &=& f
		\ \mbox{ in } \Omega \times ]0,T[,
		\\
	u(0) &=& 0
		\ \mbox{ in } \Omega,
		\\
	u(t) &=& 0
		\ \mbox{ on } \partial\Omega \times ]0,T[.
\end{eqnarray*}
where $f$ is a known function.
In the present example, we consider $f=1$.

\subsubsection*{Approximation}
% ------------------------------
Let $\Delta t>0$ and
$t_n=n\Delta t$, $n\geq 0$.
The problem is approximated with respect to time
by the following first-order implicit Euler scheme:
\[
	\Frac{u^{n+1}-u^n}{\Delta t} - \Delta u^{n+1} = f(t_{n+1})
		\ \mbox{ in } \Omega
\]
where $u^n\approx u(n\Delta  t)$ and $u^{(0)}=0$.
The variational formulation of the time-discretized 
problem writes:

  {\it $(VF)_n$: Let $u^n$ being known, find $u^{n+1}\in H^1_0(\Omega)$ such that}
  $$
      a\,(u^{n+1},\,v) = l^{(n)}(v)
      ,\ \forall v \in H^1_0(\Omega).
  $$
\pbindex{Poisson}%
where
\begin{eqnarray*}
  a(u,v)     &=& \int_\Omega \left(uv + \Delta t \,\nabla u.\nabla v\right) \,v \,{\rm d}x
	\\
  l^{(n)}(v) &=& \int_\Omega \left(u^n+\Delta t\,f(t_{n+1})\right) \,v \,{\rm d}x
\end{eqnarray*}
This is a Poisson-like problem.
The discretization with respect to space of this problem
is similar to those presented
in section~\ref{sec-dirichlet}, page~\pageref{sec-dirichlet}.

% ----------------------------------
\myexamplelicense{heat.cc}
% ----------------------------------

\subsubsection*{Comments}
% --------------------
\clindex{branch}%
Note the use of the \code{branch} class:
\begin{lstlisting}[numbers=none,frame=none]
  branch event ("t","u");
\end{lstlisting}
this is a wrapper class that is used here to print
the branch of solution $(t_n,u^n)_{n\geq 0}$, 
on the standard output in the \filesuffix{.branch} file format.
\findex{catchmark}%
An instruction as:
\begin{lstlisting}[numbers=none,frame=none]
  dout << event (t,uh);
\end{lstlisting}
is equivalent to the formatted output
\begin{lstlisting}[numbers=none,frame=none]
  dout << catchmark("t") << t << endl
       << catchmark("u") << uh;
\end{lstlisting}

\subsubsection*{How to run the program}
% ----------------------------------

 \begin{figure}[htb]
     \begin{center}
          \includegraphics[scale=0.6]{heat-fig.png}
     \end{center}
     \caption{Animation of the solution of the heat problem.}
     \label{fig-heat}
  \end{figure}

  We assume that the previous code is contained in
  the file \reffile{heat.cc}.
  Then, compile the program as usual (see page~\pageref{makefile}):
\begin{verbatim}
  make heat
\end{verbatim}
\fiindex{\filesuffix{.branch} family of fields}
  For a one dimensional problem, enter the commands:
\begin{verbatim}
  mkgeo_grid -e 100 > line.geo
  ./heat line.geo > line.branch
\end{verbatim}
  The previous commands solve the problem for the corresponding mesh
  and write the solution in the field-family file format \filesuffix{.branch}.
  For a bidimensional one:
\begin{verbatim}
  mkgeo_grid -t 10 > square.geo
  ./heat square.geo > square.branch
\end{verbatim}
  For a tridimensional one:
\begin{verbatim}
  mkgeo_grid -T 10 > box.geo
  ./heat box.geo > box.branch
\end{verbatim}

\subsubsection*{How to run the animation}
% ----------------------------------
\label{sec-animation}
\cindex{visualization!animation}%
\pindex{gnuplot}%
\begin{verbatim}
  branch line.branch
\end{verbatim}
A \code{gnuplot} window appears.
Enter \code{q} to exit the window.
For a bidimensional case, simply enter:
\pindex{paraview}%
\pindexopt{field}{-elevation}%
\begin{verbatim}
  branch square.branch -elevation
\end{verbatim}
A window appears, that looks like a video player.
Then, click on the video \fbox{play} button, at the top of the window.

\fiindex{\filesuffix{.avi} avi file (video)}
To generate an animation file,
go to the \verb+File->save animation+ menu and
enter as file name \code{square} and as file type \code{avi}.
The animation file \code{square.avi}
can now be started from any video player, such as \code{vlc}:
\pindex{vlc}%
\begin{verbatim}
  vlc square.avi
\end{verbatim}
For the tridimensional case, the animation feature is similar:
\pindexopt{branch}{-volume}%
\begin{verbatim}
  branch box.branch 
  branch box.branch -volume
\end{verbatim}
