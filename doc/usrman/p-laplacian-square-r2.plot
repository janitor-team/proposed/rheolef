set terminal cairolatex pdf color standalone
set output "p-laplacian-square-r2.tex"

set logscale y
set colors classic
set size square 

set xrange [0:200]
set xtics (0,100,200)
set yrange [1e-15:1e5]
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}" 1 )
set xlabel '[c]{\large $n$}'
set  label '[l]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph 0.01,0.92

plot \
  "p-laplacian-square-p=1.15.gdat"  \
	title "[r]{$p=1.15$}" w l lt 1 lc 1 lw 2, \
  "p-laplacian-square-p=1.25.gdat"  \
	title "[r]{$p=1.25$}" w l lt 1 lc rgb '#008800' lw 2, \
  "p-laplacian-square-p=1.5.gdat" \
	title "[r]{$p=1.50$}" w l lt 1 lc 3 lw 2

#pause -1 "<retour>"
