set terminal cairolatex pdf color standalone
set output 'yield-slip-50-S-0,6-n-cut.tex'

set size square
set colors classic
set xrange [0:1]
set yrange [-1e-10:0.04]
set xtics 0.5
set ytics 0.01
set xlabel '[c]{\large $x_0$}'
set  label '[l]{\large $u(x_0,1)$}' at graph 0.03,0.92

plot \
'yield-slip-50-n-1,5-S-0,6-cut.gdat' u 1:3 t '[r]{$n=1.5$}' w l lc 1 lt 1 lw 3, \
'yield-slip-50-n-1-S-0,6-cut.gdat'   u 1:3 t '[r]{$n=1.0$}' w l lc rgb "#008800" lt 1 lw 3, \
'yield-slip-50-n-0,5-S-0,6-cut.gdat' u 1:3 t '[r]{$n=0.5$}' w l lc 3 lt 1 lw 3

#pause -1 "<retour>"
