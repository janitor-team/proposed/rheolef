set terminal cairolatex pdf color standalone
set output "p-laplacian-damped-newton-n=50-P1-p1.tex"

set logscale y
set colors classic
set size square 
set xtics (0,25,50)
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$10^{0}$}" 1 )
set xlabel '[c]{$n$}'
set  label '[l]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph 0.02,0.92
set  label '[r]{$h=1/50, k=1$}' at graph 0.98,0.23

plot [0:50][1e-15:1e5] \
"p-laplacian-damped-newton-p=1.5-n=50-P1.gdat" title "[r]{$p=1.5$}" w lp lw 2, \
"p-laplacian-damped-newton-p=1.4-n=50-P1.gdat" title "[r]{$p=1.4$}" w lp lw 2 lc '#008800', \
"p-laplacian-damped-newton-p=1.3-n=50-P1.gdat" title "[r]{$p=1.3$}" w lp lw 2,\
"p-laplacian-damped-newton-p=1.2-n=50-P1.gdat" title "[r]{$p=1.2$}" w lp lw 2

#pause -1 "<retour>"
