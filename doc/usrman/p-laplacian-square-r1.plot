set terminal cairolatex pdf color standalone
set output "p-laplacian-square-r1.tex"

set logscale y
set colors classic
set size square 
set xtics (0,250,500)
set ytics (\
        "[r]{$10^{-15}$}" 1e-15, \
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}" 1e-5, \
        "[r]{$1$}" 1 )
set xlabel '[c]{\large $n$}'
set  label '[l]{$\left\|r_h^{(n)}\right\|_{-1,h}$}' at graph 0.02,0.92

plot [0:500][1e-15:1e5] \
  "p-laplacian-square-p=2.95.gdat" \
	title "[r]{$p=2.95$}" w l lt 1 lc 1 lw 2, \
  "p-laplacian-square-p=2.9.gdat" \
	title "[r]{$p=2.90$}" w l lt 1 lc rgb '#008800' lw 2, \
  "p-laplacian-square-p=2.5.gdat"  \
	title "[r]{$p=2.50$}" w l lt 1 lc 3 lw 2

#pause -1 "<retour>"

