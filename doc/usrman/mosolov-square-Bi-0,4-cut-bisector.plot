set terminal cairolatex pdf color standalone
set output "mosolov-square-Bi-0,4-cut-bisector.tex"

# field -catchmark u square-20.field -cut -origin 0 0 -normal 1 1 -noclean
# field -catchmark u square-10.field -cut -origin 0 0 -normal 1 1 -noclean
 
set multiplot # for the zoom
set size square 
set colors classic
set xrange [0:]
set yrange [0:0.3]
s_zoom = 1.30
set xtics (0, s_zoom, '[c]{$\sqrt{2}$}' sqrt(2))
set ytics 0.1
set xlabel '[c]{$\sqrt{x^2+y^2}$}'
set  label '[l]{$u(x,y)$}' at graph 0.04, 0.92
plot \
"mosolov-square-10-Bi-0,4-cut-bisector.gdat" \
  u ($1/sqrt(2.)):2 \
  t '[r]{$h=1/10$}' \
  w lp lw 1 ps 0.5 lc 1 dt 2, \
"mosolov-square-20-Bi-0,4-cut-bisector.gdat" \
  u ($1/sqrt(2.)):2 \
  t '[r]{$h=1/20$}' \
  w lp lw 1 ps 0.5 lc rgb "#008800", \
"mosolov-square-30-Bi-0,4-cut-bisector.gdat" \
  u ($1/sqrt(2.)):2 \
  t '[r]{$h=1/30$}' \
  w lp lw 1 ps 0.5 lc 3, \
"mosolov-square-40-Bi-0,4-cut-bisector.gdat" \
  u ($1/sqrt(2.)):2 \
  t '[r]{$h=1/40$}' \
  w lp lw 1 ps 0.5 lc 4, \
"mosolov-square-50-Bi-0,4-cut-bisector.gdat" \
  u ($1/sqrt(2.)):2 \
  t '[r]{$h=1/50$}' \
  w lp lw 1 pt 6 ps 0.5 lc 0

set origin 0.22, 0.15
unset size
unset xlabel
unset label
set size 0.42
set xrange [s_zoom:sqrt(2)]
set yrange [0:0.01]
set xtics (s_zoom, '[c]{$\sqrt{2}$}' sqrt(2))
set ytics 0.01
set nokey
replot

#pause -1 "<return>"
