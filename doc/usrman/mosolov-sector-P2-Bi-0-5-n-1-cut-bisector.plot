set terminal cairolatex pdf color standalone
set output "mosolov-sector-P2-Bi-0-5-n-1-cut-bisector.tex"

umax = 0.169865455242435

set size square
set colors classic
# for several plots on the same graph: use multiplot in addition to replot 
set multiplot layout 1,1

set xrange [0:sqrt(2)]
set yrange [-0.01:0.20]
s_zoom = 1.30
set xtics (0, 1, s_zoom, '[c]{$\sqrt{2}$}' sqrt(2))
set ytics (0, 0.1, 0.2, '[r]{$u_\textrm{max}$}' umax)
set xlabel '[c]{$\sqrt{x^2+y^2}$}'
set  label '[l]{$u(x,y)$}' at graph 0.03, 0.93

set arrow from 0,0 to sqrt(2),0 nohead dt 3 lc 0
plot \
'mosolov-sector-P2-Bi-0-5-n-1-cut-bisector.gdat' \
  u (sqrt(2)*$1):3 notitle with lp lc 1 lw 1

set origin 0.24, 0.19
unset size
unset xlabel
unset label
unset arrow
set size 0.4
set xrange [s_zoom:sqrt(2)]
set yrange [-0.001:0.006]
set xtics (s_zoom, '[c]{$\sqrt{2}$}' sqrt(2))
set ytics 0.006
set nokey
set arrow from s_zoom,0 to sqrt(2),0 nohead dt 3 lc 0
replot

unset multiplot
