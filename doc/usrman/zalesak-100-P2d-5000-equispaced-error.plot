set terminal cairolatex pdf color standalone
set output "zalesak-100-P2d-5000-equispaced-error.tex"

gdat = 'zalesak-100-P2d-5000-equispaced.gdat' 

set size square
set xrange [0:50]
set yrange [0:1e-3]
set xlabel '[c]{$r$}'
set  label '[l]{$E_h^{(n_{\max})}$}' at graph 0.09, 0.9
set xtics 10
set ytics (\
    '[r]{$10^{-3}$}'             1e-3, \
    '[r]{$5\!\times\! 10^{-4}$}' 5e-4, \
    '[r]{$0$}'                   0)

plot \
gdat \
  every 2 u 1:2 \
  not 'err-tf-l1' \
  w lp pt 7 ps 0.25 lw 2 lc 0

# gdat \
  u 1:3 \
  t 'v-tf' \
  w lp

#pause -1
