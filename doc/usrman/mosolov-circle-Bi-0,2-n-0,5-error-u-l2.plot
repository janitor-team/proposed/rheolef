set terminal cairolatex pdf color standalone
set output "mosolov-circle-Bi-0,2-n-0,5-error-u-l2.tex"

set size square
set log xy
set colors classic
set xrange [1e-2:1e-1]
set yrange [1e-4:1]
graph_ratio_xy = 1./4.

set xlabel '[c]{\Large $h$}'
set  label '[l]{\Large $\|u-u_h\|_{0,\infty,\Omega}$}' at graph 0.05, 0.95

## triangle a droite
#slope_A = graph_ratio_xy*1.0
#xA =  0.35
#yA =  0.47
#dxA = 0.15
#dyA = dxA*slope_A
#set label "[l]{\\scriptsize $1=k$}" at graph xA+dxA+0.02, yA+0.5*dyA right
#set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
#set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
#set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
slope_B = graph_ratio_xy*2.0
xB =  0.35
yB =  0.09
dxB = 0.15
dyB = dxB*slope_B
set label "[l]{\\scriptsize $2$}" at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

plot \
'mosolov-circle-Bi-0,2-n-0,5-error.gdat' \
  i 0 \
  u (1/$1):3 \
  t '[r]{$k=1$}' \
  w lp lw 4 lc 1, \
'mosolov-circle-Bi-0,2-n-0,5-error.gdat' \
  i 1 \
  u (1/$1):3 \
  t '[r]{$k=2$}' \
  w lp lw 4 lc rgb "#008800", \
'mosolov-circle-Bi-0,2-n-0,5-error.gdat' \
  i 2 \
  u (1/$1):3 \
  t '[r]{$k=3$}' \
  w lp lw 4 lc 3

#pause -1 "<return>"
