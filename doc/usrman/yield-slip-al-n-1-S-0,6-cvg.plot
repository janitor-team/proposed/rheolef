set terminal cairolatex pdf color standalone
set output "yield-slip-al-n-1-S-0,6-cvg.tex"

set logscale xy
set colors classic
set size square 
set xrange [1:1e4]
set yrange [1e-10:1e2]
graph_ratio_xy = 4./12.
set xtics (\
        "[c]{$10^{4}$}" 1e4, \
        "[c]{$10^{2}$}" 1e2, \
        "[c]{$1$}"      1 )
set ytics (\
        "[r]{$10^{-10}$}" 1e-10, \
        "[r]{$10^{-5}$}"  1e-5, \
        "[r]{$1$}"        1 )
set xlabel "[c]{iteration $k$}"
set  label "[l]{residue}" at graph 0.03,0.93

# triangle a gauche
alpha_D = -1.0
slope_D = graph_ratio_xy*alpha_D
xD =  0.32
yD =  0.57
dxD = 0.15
dyD = dxD*slope_D
set label sprintf("[r]{\\scriptsize $%g$}",alpha_D) at graph xD-0.02, yD+0.5*dyD right
set arrow from graph xD,     yD     to graph xD,     yD+dyD nohead
set arrow from graph xD,     yD+dyD to graph xD+dxD, yD+dyD nohead
set arrow from graph xD+dxD, yD+dyD to graph xD,     yD     nohead

plot \
"yield-slip-al-n-10-n-1-S-0.6.gdat" title "[r]{$h=1/10$}" w l lw 4 lc 1, \
"yield-slip-al-n-20-n-1-S-0.6.gdat" title "[r]{$1/20$}" w l lw 4 lc rgb '#008800', \
"yield-slip-al-n-30-n-1-S-0.6.gdat" title "[r]{$1/30$}" w l lw 4 lc 3, \
"yield-slip-al-n-40-n-1-S-0.6.gdat" title "[r]{$1/40$}" w l lw 4 lc 4, \
"yield-slip-al-n-50-n-1-S-0.6.gdat" title "[r]{$1/50$}" w l lw 4 lc 0

#"yield-slip-al-n-50-P3-n-0,5-S-0.6.gdat" t "..." w l lw 4 lc 0

#pause -1 "<retour>"
