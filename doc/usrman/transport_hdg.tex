% -----------------------------------------------------------------------
\subsection{The transport equation}
% -----------------------------------------------------------------------
\apindex{discontinuous!hybrid}%
\cindex{problem!transport equation!steady}

The steady scalar transport problem 
has already been studied
in section~\ref{sec-transport-dg},
  page~\pageref{sec-transport-dg}:

\ \ $(P)$: \emph{find $\phi$, defined in $\Omega$, such that}
\begin{eqnarray*}
  {\rm div}(\boldsymbol{u}\nabla \phi) + \sigma \phi &=& f 
	\ \ \mbox{ in } \ \Omega
	\\
  \phi &=& u_\Gamma
	\ \mbox{ on } \ \partial \Omega_-
\end{eqnarray*}
and $\boldsymbol{u}$ is assumed to be a divergence-free given vector.
See also section~\ref{sec-transport-dg} for notations and definitions.
This problem falls into the framework of the previous section
with the choices
\eqref{eq-dg-hyp-transport-f}-\eqref{eq-dg-hyp-transport-phi}
for $\boldsymbol{f}$ and $\Phi$, respectively.
Then, the discrete variational problem becomes:

\ \ $(FV)_h$: \emph{find $\phi_h\in X_h$ and $\lambda_h \in M_h$ such that}
\begin{eqnarray*} 
  &&
  \int_\Omega
    \phi_h
   \left(
      \sigma \varphi_h
      -
      \boldsymbol{u}.\nabla_h \varphi_h
    \right)
    \,{\rm d}x
  +
  \sum_{K\in\mathscr{T}_h}
  \int_{\partial K}
    |\boldsymbol{u}.\boldsymbol{n}| 
    \, \phi_h
    \, \varphi_h
    \,{\rm d}s
  \\
  &&
  -
  \int_{\mathscr{S}_h^{(i)}}
  \left(
      2|\boldsymbol{u}.\boldsymbol{n}|
      \, \average{\varphi_h}
      -
      (\boldsymbol{u}.\boldsymbol{n})
      \, \jump{\varphi_h}
    \right)
    \lambda_h
    \,{\rm d}s
  -
  \int_{\partial\Omega}
    2\max\left(0,
      -
      \boldsymbol{u}.\boldsymbol{n}
    \right)
    \, \varphi_h
    \lambda_h
    \,{\rm d}s
  =
  \int_\Omega
    f \, \varphi_h
    \,{\rm d}x
  \\
  &&
  \int_{\mathscr{S}_h^{(i)}}
    (\average{\phi_h}-\lambda_h)
    \,\mu_h
    \,{\rm d}s
  +
  \int_{\partial\Omega}
    \left(
      \mathbb{I}_{]-\infty,0[}(\boldsymbol{u}.\boldsymbol{n})
      (g-\lambda_h)
      +
      \mathbb{I}_{[0,\infty[}(\boldsymbol{u}.\boldsymbol{n})
      (\phi_h-\lambda_h)
       \right)
    \,\mu_h
    \,{\rm d}s
  =
  0
\end{eqnarray*}
for all $\varphi_h\in X_h$ and $\mu_h \in M_h$.
The boundary condition for $\lambda_h$ has been
re-expressed explicited as $\lambda_h=g$ on the upstream boundary domain
and as $\lambda_h=\phi_h$, the inner trace, on the rest of the 
boundary domain.
Here, $\mathbb{I}_A$ denotes the indicator function on any set $A$.

Let us introduce the following linear and bilinear forms:
\begin{eqnarray*}
  a_h(\phi,\varphi)
  &=&
  \int_\Omega
    \phi
    \left(
      \sigma
      \varphi
      -
      \boldsymbol{u}.\nabla_h\varphi
    \right)
    \, {\rm d}x
  +
  \sum_{K\in \mathscr{T}_h}
  \int_{\partial K}
    |\boldsymbol{u}.\boldsymbol{n}|
    \,\phi
    \,\varphi
    \, {\rm d}s
  \\
  b_{1,h}(\phi,\mu)
  &=&
  \int_{\mathscr{S}_h^{(i)}}
    \left(
      (\boldsymbol{u}.\boldsymbol{n})
      \,\jump{\phi}
      -
      2 |\boldsymbol{u}.\boldsymbol{n}|
      \,\average{\phi}
    \right)
    \,\mu
    \, {\rm d}s
  -
  \int_{\partial\Omega}
    2\max\left(0,\, -\boldsymbol{u}.\boldsymbol{n} \right)
    \,\phi
    \,\mu
    \, {\rm d}s
  \\
  b_{2,h}(\phi,\mu)
  &=&
  \int_{\mathscr{S}_h^{(i)}}
    \average{\phi}
    \,\mu
    \, {\rm d}s
  +
  \int_{\partial\Omega}
    \mathbb{I}_{[0,\infty[}(\boldsymbol{u}.\boldsymbol{n})
    \,\phi
    \,\mu
    \, {\rm d}s
  \\
  c_{h}(\lambda,\mu)
  &=&
  \int_{\mathscr{S}_h}
    \lambda
    \,\mu
    \, {\rm d}s
  \\
  \ell_h(\varphi)
  &=&
  \int_\Omega
    f
    \,\varphi
    \, {\rm d}x
  \\
  k_h(\mu)
  &=&
  -
  \int_{\partial\Omega}
    \mathbb{I}_{]-\infty,0[}(\boldsymbol{u}.\boldsymbol{n})
    \,g
    \,\mu
    \, {\rm d}s
\end{eqnarray*}
The discrete variational formulation of the problem writes:

\ \ $(FV)_h$: \emph{find $\phi_h\in X_h$ and $\lambda_h\in M_h$, such that}
\begin{eqnarray*}
  a_h(\phi_h,\varphi_h)
  +
  b_{1,h}(\varphi_h,\lambda_h)
  &=&
  \ell_h(\varphi_h)
  \\
  b_{2,h}(\phi_h,\mu_h)
  -\ \ 
  c_{h}(\lambda_h,\mu_h)
  &=&
  k_h(\mu_h)
\end{eqnarray*}
for all $\varphi_h\in X_h$ and $\mu_h\in M_h$.
This is an unsymmetrix linear system with the following mixted matrix structure:
\begin{eqnarray*}
  &&
  \left( \begin{array}{cc}
    A   & B_1^T \\
    B_2 & -C
  \end{array} \right)
  \left( \begin{array}{c}
    \phi_h \\
    \lambda_h
  \end{array} \right) 
  \ =\ 
  \left( \begin{array}{c}
    \ell_h \\
    k_h
  \end{array} \right) 
\end{eqnarray*}
A carreful study shows that the $A$ matrix
has a block-diagonal structure at the element level
and can be thus easily inverted.
The matrix structure writes equivalently:
\begin{eqnarray*}
  &\Longleftrightarrow&
  \left\{ \begin{array}{rcl}
	A\phi_h   + B_1^T\lambda_h &=& \ell_h \\
	B_2\phi_h - C\lambda_h     &=& k_h
  \end{array} \right.
  \\
  &\Longleftrightarrow&
  \left\{ \begin{array}{l}
	\phi_h = A^{-1}(\ell_h-B_1^T\lambda_h) \\
	(C+B_2A^{-1}B_1^T)\lambda_h = B_2A^{-1}\ell_h - k_h
  \end{array} \right.
\end{eqnarray*}
The second equation is solved first:
the only remaining unknown is the Lagrange multiplier $\lambda_h$.
Then, the variable $\phi_h$
is simply obtained by a direct computation.

\myexamplelicense{transport_hdg.cc}

% ------------------------------
\subsubsection*{Comments}
% ------------------------------
The data are $f=0$, $g=1$ and $\boldsymbol{u}=(1,0,0)$,
and then the exact solution is known:
$\phi(x)=\exp(-\sigma x_0)$.
The numerical tests are running with $\sigma=3$ by default.
The one-dimensional case writes:
\begin{verbatim}
  make transport_hdg
  mkgeo_grid -e 10 > line.geo
  ./transport_hdg line P0  | field -
  ./transport_hdg line P1d | field -
  ./transport_hdg line P2d | field -
\end{verbatim}
