set terminal cairolatex pdf color standalone
set output "combustion_error_h1.tex"

set size square
set colors classic
set log xy
set xrange[1e-3:1]
set yrange[1e-8:1]
set key left at graph 0.05,0.96
set xlabel '[c]{\large $h$}'
set  label '[r]{\large $\|\nabla(u_h-u)\|_{0,2,\Omega}$}' at graph -0.02,0.82

set xtics (\
        "[c]{$10^{-3}$}" 1e-3, \
        "[c]{$10^{-2}$}" 1e-2, \
        "[c]{$10^{-1}$}" 1e-1, \
        "[c]{$1$}" 1)
set ytics (\
        "[r]{$10^{-8}$}" 1e-8, \
        "[r]{$10^{-4}$}" 1e-4, \
        "[r]{$1$}"       1 )

graph_ratio = 3.0/8.0

# triangle a droite
slope_A = graph_ratio*1.0
xA =  0.27
yA =  0.76
dxA = 0.10
dyA = dxA*slope_A
set label "[l]{\\scriptsize $1$}" at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
slope_B = graph_ratio*2.0
xB =  0.27
yB =  0.41
dxB = 0.10
dyB = dxB*slope_B
set label "[l]{\\scriptsize $2$}" at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
slope_C = graph_ratio*3.0
xC =  0.37
yC =  0.20
dxC = 0.10
dyC = dxC*slope_C
set label "[l]{\\scriptsize $3=k$}" at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

plot \
'combustion_error.gdat' i 0 u (1./$1):4 t '[r]{$k=1$}' w lp lw 2 lt 1 lc 1, \
'combustion_error.gdat' i 1 u (1./$1):4 t '[r]{$k=2$}' w lp lw 2 lt 1 lc 3, \
'combustion_error.gdat' i 2 u (1./$1):4 t '[r]{$k=3$}' w lp lw 2 lt 1 lc 4

#pause -1 "<retour>"
