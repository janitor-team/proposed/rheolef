set terminal cairolatex pdf color standalone
set output "convect_cvge_Pk_err_l2_l2.tex"

set size square
set colors classic
set log xy
set xrange [4e-4/3:4e-0/3]
set yrange [5e-4:5]
graph_ratio = 4.0/4.0

# omega=]-4:4[ and [0:T]=[0:2*pi] => factor 8*2*pi = 16*pi for the L2(L2) norm
#scale = 1./(16*pi)
scale = 1

#set label '[r]{$\|\phi_h-\pi_h(\phi)\|_{L^2(L^2)}$}' at graph -0.05,0.9
set xlabel '[r]{$h$}'
set label  '[l]{\small $\Delta t=2\pi/50$}'  at graph 0.02,0.24
set label  '[l]{\small $\Delta t=2\pi/100$}' at graph 0.02,0.17
set label  '[l]{\small $\Delta t=2\pi/200$}' at graph 0.02,0.10

# triangle a gauche, pente +1
slope_A = graph_ratio*(2.0)
xA =  0.45
yA =  0.30
dxA = 0.07
dyA = dxA*slope_A
set label "[r]{\\scriptsize $2$}" at graph xA-0.02, yA+0.5*dyA right
set arrow from graph xA,     yA     to graph xA,     yA+dyA nohead
set arrow from graph xA,     yA+dyA to graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead


# triangle a droite, pente +2
slope_B = graph_ratio*2.0
xB =  0.85
yB =  0.35
dxB = 0.07
dyB = dxB*slope_B
set label "[l]{\\scriptsize $2$}" at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

plot \
"convect_cvge_Pk.gdat" \
	i 0 u (8.0/$1):(scale*$2) \
	not '[r]{$P_1$$}' \
	w lp lt 1 pt 1 lc 1, \
"convect_cvge_Pk.gdat" \
	i 1 u (8.0/$1):(scale*$2) \
	not '[r]{$P_1$, $\Delta t=2\pi/100$}' \
	w lp lt 1 pt 2 lc 1, \
"convect_cvge_Pk.gdat" \
	i 2 u (8.0/$1):(scale*$2) \
	t '[c]{$P_1$}' \
	w lp lt 1 pt 3 lc 1, \
"convect_cvge_Pk.gdat" \
	i 3 u (8.0/$1):(scale*$2) \
	not "P2 err_l2_l2 dt=50" \
	w lp lt 1 pt 1 lc 3, \
"convect_cvge_Pk.gdat" \
	i 4 u (8.0/$1):(scale*$2) \
	not "P2 err_l2_l2 dt=100" \
	w lp lt 1 pt 2 lc 3, \
"convect_cvge_Pk.gdat" \
	i 5 u (8.0/$1):(scale*$2) \
	t '[c]{$P_2$}' \
	w lp lt 1 pt 3 lc 3

#pause -1 "<retour>"
