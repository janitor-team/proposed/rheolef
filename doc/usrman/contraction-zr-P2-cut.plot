set terminal cairolatex pdf color standalone
set output "contraction-zr-P2-cut.tex"

set size square
set colors classic
set xtics (-8,-4,0,2)
set ytics (0,1,2,3,4)
set xlabel '[c]{\Large $z$}'
set  label '[l]{\Large $u_0(z,0)$}' at graph 0.05, 0.95
plot [-8:2][0:5] \
     "contraction-zr-P2-cut.gdat" u 1:($2) title "axisymetric" with lines lc 0 dt 1 lw 3, \
     "contraction-P2-cut.gdat"    u 1:($2) title "cartesian"   with lines lc 1 dt 1 lw 3
#pause -1 "<return>"
