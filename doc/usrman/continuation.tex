%---------------------------------------------
%\chapter{Continuation and bifurcation methods}
%---------------------------------------------
\label{chap-continuation}%
This chapter is an introduction to continuation
and bifurcation methods with \Rheolef.
We consider a model nonlinear problem that 
depends upon a parameter.
This problem is inspired from application to combustion.
Solutions exists for a limited range of this parameter
and there is a limit point: beyond this limit point
there is no more solution.
Our first aim is to compute the branch of solutions
until this limit point, thanks to the continuation algorithm.
Moreover, the limit point is a turning point for the branch of
solutions: there exists a second branch of solutions,
continuing the first one.
Our second aim is to compute the second branch of solutions
after this limit point with the Keller continuation algorithm.
For simplicity in this presentation, the discretization is in one dimension:
the extension to high order space dimension is immediate.

%-------------------------------------------------
\subsection{Problem statement and the Newton method}
%-------------------------------------------------
\label{sec-continuation-dirichlet}%
\pbindex{combustion}%

Let us consider the following model problem
(see~\citealp[p.~59]{Pau-1997} or~\citealp[p.~2]{CroRap-1990}),
defined for all $\lambda\in\mathbb{R}$:

\ \ $(P)$: {\em find $u$, defined in $\Omega$ such that}
\begin{eqnarray*}
	-\Delta u + \lambda \exp(u) &=& 0
	\ \mbox{ in }\Omega
	\\
	u = 0 
	\ \mbox{ on }\partial\Omega
\end{eqnarray*}
\cindex{method!newton}%
In order to apply a Newton method to the whole problem, let us introduce:
\[
	F(\lambda,u) = -\Delta u + \lambda \exp(u)
\]
Then, the G\^ateau derivative at any $(\lambda,u)\in \mathbb{R}\times H^1_0(\Omega)$ is given by:
\[
	\Frac{\partial F}{\partial u}(\lambda,u).(v) = -\Delta v + \lambda \exp(u) v
	, \ \forall v \in H^1_0(\Omega)
\]
% ---------------------------------------
\myexamplelicense{combustion.h}

\myexamplelicense{combustion1.icc}

\myexamplelicense{combustion2.icc}

\myexamplelicense{combustion_newton.cc}
% ---------------------------------------

\bigskip

Let us choose $\alpha=1/2$
and $\lambda=8(\alpha/\cosh(\alpha))^2 \approx 1.57289546593186$.
Compilation and run are:
\begin{verbatim}
  make combustion_newton
  mkgeo_grid -e 10 > line-10.geo
  ./combustion_newton line-10 P1 1.57289546593186 > line-10.field
  field line-10.field
\end{verbatim}
%------------------------------------------------------
\subsection{Error analysis and multiplicity of solutions}
%------------------------------------------------------
In one dimension, when $\Omega=]0,1[$, the problem can be solved 
explicitly~\citep[p.~59]{Pau-1997}:
\begin{itemize}
\item when $\lambda \leq 0$ the solution is parameterized by
$\alpha\in ]0,\pi/2[$ and:
\begin{eqnarray*}
  \lambda &=& -\Frac{8\alpha^2}{\cos^2(\alpha)} \\
  u(x) &=& 2\log\left(\Frac{\cos(\alpha)}{\cos(\alpha(1-2x))}\right)
	, \ x\in]0,1[
\end{eqnarray*}
\item when $0\leq \lambda \leq \lambda_c$ there is two  solutions.
The smallest one is parameterized by
$\alpha\in ]0,\alpha_c]$ and:
and the largest by $\alpha\in ]\alpha_c,+\infty[$ with:
\begin{eqnarray*}
  \lambda &=& \Frac{8\alpha^2}{\cosh^2(\alpha)} \\
  u(x) &=& 2\log\left(\Frac{\cosh(\alpha)}{\cosh(\alpha(1-2x))}\right)
	, \ x\in]0,1[
\end{eqnarray*}
\item when $\lambda > \lambda_c$ there is no more solution.
\end{itemize}
The critical parameter value $\lambda_c=8\alpha_c^2/\cosh^2(\alpha_c)$
where $\alpha_c$ is the unique positive solution
to $\tanh(\alpha)=1/\alpha$.
\cindex{method!newton}%
The following code compute $\alpha_c$ and $\lambda_c$ by using a Newton method.

% ---------------------------------------
\myexamplelicense{lambda_c.h}

\myexamplelicense{lambda_c.cc}
% ---------------------------------------

Compilation and run write:
\begin{verbatim}
  make lambda_c
  ./lambda_c
\end{verbatim}
and then
$\alpha_c  \approx 1.19967864025773$
and
$\lambda_c \approx 3.51383071912516$.
%
The exact solution and its gradient at the limit point are computed by the following functions:

% ---------------------------------------
\myexamplelicense{combustion_exact.icc}
% ---------------------------------------

The \code{lambda2alpha} function converts $\lambda$ into $\alpha$.
When $0<\lambda<\lambda_c$,
there is two solutions to the equation
\mbox{$
  8\left(\alpha/\cosh(\alpha)\right)^2 = \lambda
$}
and thus we
specify with the Boolean \code{is_upper} which one is expected.
Then $\alpha$ is computed by a dichotomy algorithm.

% ---------------------------------------
\myexamplelicense{lambda2alpha.h}
% ---------------------------------------

Finally, the errors in various norms are available:

% ---------------------------------------
\myexamplelicense{combustion_error.cc}
% ---------------------------------------

The computation of the error writes:
\begin{verbatim}
  make combustion_error
  ./combustion_error < line-10.field
\end{verbatim}
The solution is represented on Fig.~\ref{fig-combustion-crit}.
\begin{figure}[htb]
     \begin{tabular}{cc}
          \includegraphics[height=6cm]{combustion-a-0,5.pdf}  &
          \includegraphics[height=6cm]{combustion-ac.pdf}
     \end{tabular}
     \caption{Combustion problem:
        (left) $\alpha=1/2$.
        (right) near $\alpha_c$.
        }
   \label{fig-combustion-crit}
\end{figure}
Then we consider the vicinity of $\lambda_c \approx 3.51383071912516$.
\begin{verbatim}
  ./combustion_newton line-10  P1 3.55 > line-10.field
  field line-10.field
  mkgeo_grid -e 100 > line-100.geo
  ./combustion_newton line-100 P1 3.51 > line-100.field
\end{verbatim}
The Newton method fails
when the parameter $\lambda$ is greater to some critical value
$\lambda_{c,h}$ that depends upon the mesh size.
Moreover, while the approximate solution is close to the exact one
for moderate $\lambda=1.57289546593186$,
the convergence seems to be slower at the vicinity of $\lambda_c$.

In the next section, we compute accurately $\lambda_{c,h}$ for each
mesh and observe the convergence of $\lambda_{c,h}$ to $\lambda_c$
and the convergence of the associated approximate solution to the
exact one.

%-------------------------------------------------
\subsection{The Euler-Newton continuation algorithm}
%-------------------------------------------------
\cindex{method!continuation}%
The Euler-Newton continuation algorithm writes~\citep[p.~176]{Pau-1997}:

{\bf algorithm~1} (\emph{continuation})
\begin{itemize}
\item $n=0$:     Let $(\lambda_0,u_0)$ be given. Compute
      \[
	\dot{u}_0 = -
                    \left( \Frac{\partial F}{\partial u}      (\lambda_0,u_0)\right)^{-1}
                           \Frac{\partial F}{\partial \lambda}(\lambda_0,u_0)
      \]
\item $n\geq 0$: Let $(\lambda_n,u_n)$ and $\dot{u}_n$ being known.
      \begin{itemize}
      \item[1)] First choose a step $\Delta \lambda_n$ and set $\lambda_{n+1}=\lambda_n+\Delta\lambda_n$.
      \item[2)] Then, perform a prediction by computing
      \[
	w_0 = u_n - \Delta \lambda_n
                    \left( \Frac{\partial F}{\partial u}      (\lambda_n,u_n)\right)^{-1}
                           \Frac{\partial F}{\partial \lambda}(\lambda_n,u_n)
      \]
      \item[3)] Then, perform a correction step: for all $k\geq 0$, with $w_{k}$ being known, compute
      \[
	w_{k+1} = w_k -
                    \left( \Frac{\partial F}{\partial u}(\lambda_{n+1},w_k)\right)^{-1}
                                                       F(\lambda_{n+1},w_k)
      \]
      At convergence of the correction loop, set $u_{n+1}=w_\infty$.
      \item[4)] Finally, compute
      \[
	\dot{u}_{n+1} = -
                    \left( \Frac{\partial F}{\partial u}      (\lambda_{n+1},u_{n+1})\right)^{-1}
                           \Frac{\partial F}{\partial \lambda}(\lambda_{n+1},u_{n+1})
      \]
      \end{itemize}
\end{itemize}
The step $\Delta\lambda_n$ can be chosen
from a guest $\Delta\lambda_*=\Delta\lambda_{n-1}$
by adjusting the contraction ratio $\kappa(\Delta\lambda_*)$
\cindex{method!newton}%
of the Newton method.
Computing the two first iterates $w_{0,*}$ and $w_{1,*}$ with
the guest step $\Delta\lambda_*$ and $\lambda_*=\lambda_n+\Delta\lambda_*$
we have:
\[
	\kappa(\Delta\lambda_*)
	=
	\Frac{ \left\|
                    \left( \Frac{\partial F}{\partial u}(\lambda_{*},w_{1,*})\right)^{-1}
                                                       F(\lambda_{*},w_{1,*})
	       \right\|
	}{     \left\|
                    \left( \Frac{\partial F}{\partial u}(\lambda_{*},w_{0,*})\right)^{-1}
                                                       F(\lambda_{*},w_{0,*})
	       \right\|
	}
\]
As the Newton method is expected to converge quadratically
for small enough step, we get a practical
expression for $\Delta\lambda_n$~\citep[p.~185]{Pau-1997}:
\[
	\Frac{\kappa_0}{\Delta\lambda_n}
	\approx
	\Frac{\kappa(\Delta\lambda_*)}{\Delta\lambda_*^2}
\]
where $\kappa_0\in ]0,1[$ is the chosen reference for the contraction ratio,
for instance $\kappa_0=1/2$.

% ---------------------------------------
%\myexamplelicense{solver_damped_newton.h} 
%\myexamplelicense{continuation.h} 
%\myexamplelicense{continuation_opt.h} 
%\myexamplelicense{continuation_step.h} 
\myexamplelicense{combustion_continuation.cc}
% ---------------------------------------

\bigskip

\pindexopt{branch}{-toc}%
Then, the program is compiled and run as:
\begin{verbatim}
  make combustion_continuation
  mkgeo_grid -e 10 > line-10.geo
  ./combustion_continuation line-10 > line-10.branch
  branch line-10.branch -toc
\end{verbatim}
The last command lists all the computations preformed by the continuation algorithm.
\begin{figure}[htb]
     \begin{center}
          \includegraphics[height=6cm]{combustion_umax.pdf}
     \end{center}
     \caption{Combustion problem:
        $\|u_h\|_{0,\infty,\Omega}$ vs $\lambda$ when $h=1/10$ and $1/160$.
        }
   \label{fig-combustion-umax}
\end{figure}
The last recorded computation is associated to the \emph{limit point}
denoted and denoted as $\lambda_{c,h}$, says at index $21$:
\pindexopt{field}{-max}%
\pindexopt{branch}{-extract}%
\pindexopt{branch}{-branch}%
Let us visualize the solution $u_h$ at the limit point and compute its maximum $\|u_h\|_{0,\infty,\Omega}=u_h(1/2)$:
\begin{verbatim}
  branch line-10.branch -extract 21 -branch | field -
  branch line-10.branch -extract 21 -branch | field -max -
\end{verbatim}
Fig.~\ref{fig-combustion-umax} plots
$\|u_h\|_{0,\infty,\Omega}$ versus $\lambda$ for various meshes.

Fig.~\ref{fig-combustion-cv} plots its convergence to $\lambda_c$
and also the convergence of the corresponding appoximate solution $u_h$ to the
exact one $u$, associated to $\lambda_c$.
\begin{figure}[htb]
     \begin{tabular}{cc}
          \includegraphics[height=6cm]{lambda_c_cv.pdf} &
          \includegraphics[height=6cm]{combustion_error_h1.pdf}
     \end{tabular}
     \caption{Combustion problem:
        (left) convergence of $|\lambda_{c,h}-\lambda_c|$ vs $h$ ;
        (right) convergence of $|u_h-u|$ vs $h$ at the limit point.
        }
   \label{fig-combustion-cv}
\end{figure}
\cindex{error analysis}%
\cindex{convergence!error!versus mesh}%
Observe that $|\lambda_{c,h}-\lambda_c|$ converges to zero as $\mathcal{O}(h^{2k})$
while $|u_h-u|=\mathcal{O}(h^{k})$.
When $k=3$, the convergence of $\lambda_{c,h}$ slows down around $10^{-9}$:
this is due to the stopping criterion of the Newton method that detects
automatically the propagation of rounding effects in the resolution of linear systems.
\begin{figure}[htb]
     \begin{center}
          \includegraphics[height=6cm]{combustion_det_P1.pdf}
     \end{center}
     \caption{Combustion problem:
	\mbox{${\rm det}\left(\Frac{\partial F}{\partial u}(\lambda,u_h)\right)$}
	versus $\lambda_{c,h}-\lambda$ for various $h$ and $k=1$.
        }
   \label{fig-combustion-det}
\end{figure}
Observe on Fig.~\ref{fig-combustion-det} the plot of the determinant of
the Jacobean
\mbox{$\Frac{\partial F}{\partial u}(\lambda,u_h(\lambda))$}
versus $\lambda_{c,h}-\lambda$:
it tends to zero at the vicinity of $\lambda=\lambda_c$ and thus,
the Newton method reaches increasing difficulties to solve the problem.
Note that the determinant has been normalized by its value when $\lambda=0$, i.e. 
\mbox{
  	${\rm det}\left(\Frac{\partial F}{\partial u}(0,u_h(0))\right)
$}: all the curves tend to superpose on a unique master curve and the
asymptotic behavior is independent of $h$.
More precisely, Fig.~\ref{fig-combustion-det} suggests a $1/2$ slope in
logarithmic scale and thus
\[
	\Frac{{\rm det}\left(\Frac{\partial F}{\partial u}(\lambda,u_h(\lambda))\right)}
	     {{\rm det}\left(\Frac{\partial F}{\partial u}(0,u_h(0))\right)}
	\approx 
	C 
        \, (\lambda_{c,h}-\lambda)^{1/2}
\]
where $C\approx 0.52$ when $k=1$.
This behavior explains that the Newton method is unable to
reach the limit point $\lambda_c$.
%-----------------------------------------------------
\subsection{Beyond the limit point~: the Keller algorithm}
%-----------------------------------------------------
Note that the continuation method stops to the limit point (see Fig.~\ref{fig-combustion-umax})
while the the branch continues: the limit point is a turning point for the branch of
solutions. Especially, for each $\lambda\in ]0,\lambda_c[$ there are two solutions
and only one has been computed.
Keller proposed a method to follow the branch beyond a turning point
and this method is presented here.
The main idea is to parameterize the branch $(\lambda,u(\lambda))$
by a curvilinear abscissa $s$ as $(\lambda(s),u(s))$.
In order to have the count of unknown and equations we add a 
normalization equation:
\begin{eqnarray*}
	\mathcal{N}(s,\lambda(s),u(s)) &=& 0 \\
	  F(\lambda(s),u(s)) &=& 0
\end{eqnarray*}
where $\mathcal{N}$ is a given normalization function.
For the normalization function, Keller proposed 
to choose, when $(s,\lambda,u)$ is at the vicinity of $(s_n,\lambda(s_n),u(s_n))$
the one following orthogonal norms:
\begin{subequations}
  \label{eq-continuation-normalization}%
  \begin{itemize}
    \item The {\bf orthogonal} norm:  
    \begin{eqnarray}
	\mathcal{N}_n(s,\,\chi\!=\!(\lambda,u))
         &=&
	  (\chi'(s_n),\, \chi-\chi(s_n))
	- (s-s_n)
        \nonumber \\
	&=& 
	  \lambda'(s_n)\,(\lambda-\lambda(s_n))
	+ (u'(s_n),\, u-u(s_n))_V	
	- (s-s_n)
	\label{eq-continuation-normalization-orthogonal}
    \end{eqnarray}
    \item The {\bf spherical} norm:  
    \begin{eqnarray}
	\widetilde{\mathcal{N}}_n(s,\,\chi\!=\!(\lambda,u)) 
	&=&
	  \|\chi-\chi_n\|^2	
	- |s-s_n|^2
        \nonumber \\
	&=&
	  |\lambda-\lambda_n|^2
	+ \|u-u_n\|_V^2	
	- |s-s_n|^2
	\hspace{8em}
	\label{eq-continuation-normalization-spherical}
    \end{eqnarray}
  \end{itemize}
\end{subequations}
The orthogonal norm induces a pseudo curvilinear arc-length $s$,
measured on the tangent at $s=s_n$.
The spherical norm measures is simply a distance between $(s,\chi)$ and $(s_n,\chi_n)$
(see also~\citealp[pp.~179-180]{Pau-1997} and the corresponding Fig.~5.2).
We add the subscript $n$ to $\mathcal{N}$ in order to
emphasize that $\mathcal{N}$ depends upon 
both $(\lambda(s_n),u(s_n))$ and $(\lambda'(s_n),u'(s_n))$
For any $s\in\mathbb{R}$
and \mbox{$\chi=(\lambda,u)\in \mathbb{R}\times V$}
we introduce:
\[
    \mathcal{F}_n(s,\chi) 
	=
	\left(\begin{array}{r}
		\mathcal{N}_n(s,\chi) \\
	          F(\chi) 
	\end{array}\right)
    \ \mbox{ and } \  
    \widetilde{\mathcal{F}}_n(s,\chi) 
	=
	\left(\begin{array}{r}
		\widetilde{\mathcal{N}}_n(s,\chi) \\
	          F(\chi) 
        \end{array}\right)
\]
\begin{subequations}
\label{eq-continuation}
Then, the Keller problem with the orthogonal norm reduces to find,
for any $s\in\mathbb{R}$, $\chi(s)\in\mathbb{R}\times V$ such that
\begin{equation}
	\mathcal{F}_n(s,\chi(s)) = 0
	\label{eq-continuation-orthogonal}
\end{equation}
Conversely, the Keller problem with the spherical norm reduces to find,
for any $s\in\mathbb{R}$, $\chi(s)\in\mathbb{R}\times V$ such that
\begin{equation}
	\widetilde{\mathcal{F}}_n(s,\chi(s)) = 0
	\label{eq-continuation-spherical}
\end{equation}
\end{subequations}
Both problems falls into the framework of the previous paragraph,
when $F$ is replaced by either $\mathcal{F}_n$ or $\widetilde{\mathcal{F}}_n$.
Then, for any $s$ and $\chi=(\lambda,u)$, the partial derivatives are:
\begin{eqnarray*}
        \Frac{\partial \mathcal{F}_n}{\partial s} (s,\chi) 
	    &=&
	    \left(\begin{array}{c}
		   -1 \\
	           0
	    \end{array}\right)
	    \\
	\Frac{\partial\mathcal{F}_n}{\partial\chi} (s,\chi)
        &=&
	\left(\begin{array}{r}
	     \Frac{\partial \mathcal{N}_n}{\partial\chi} (s,\chi) \\
		\mbox{} \\
	                                  F'(s,\chi)
	\end{array}\right)
        =
	\left(\begin{array}{cc}
	     \lambda'(s_n) & u'(s_n) \\
		\mbox{} \\
	     \Frac{\partial F}{\partial\lambda} (\lambda,u) &
	     \Frac{\partial F}{\partial u}      (\lambda,u)
	\end{array}\right)
\end{eqnarray*}
and
\begin{eqnarray*}
        \Frac{\partial \widetilde{\mathcal{F}}_n}{\partial s} (s,\chi) 
	    &=&
	    \left(\begin{array}{c}
		-2(s-s_n) \\
	           0
	    \end{array}\right)
	    \\
        \Frac{\partial \widetilde{\mathcal{F}}_n}{\partial \chi} (s,\chi) 
	    &=&
	    \left(\begin{array}{c}
		2(\chi-\chi_n)^T \\
		\\
		F'(\chi)
	    \end{array}\right)
	    \ = \ 
	    \left(\begin{array}{cc}
		2(\lambda-\lambda_n) & 2(u-u_n)^T \\
		& \\
		\Frac{\partial F}{\partial \lambda} (\lambda,u) &
		\Frac{\partial F}{\partial u}       (\lambda,u)
	    \end{array}\right)
\end{eqnarray*}
Let us focus on the orthogonal norm case, as the spherical one is similar.
The continuation algorithm
of the previous paragraph is able to follows the branch
of solution beyond the limit point and explore the second
part of the branch.
Let us compute $\lambda'(s_n)$ and $u'(s_n)$.
By differentiating~\eqref{eq-continuation} with respect to $s$,
we get:
\[
	\Frac{\partial\mathcal{F}_n}{\partial s}  (s,\chi(s))
	+
	\Frac{\partial\mathcal{F}_n}{\partial\chi}(s,\chi(s)).(\chi'(s)) 
	= 	
	0
\]
that writes equivalently
\begin{eqnarray*}
     \Frac{\partial \mathcal{N}_n}{\partial s}   (s,\chi(s))
   + \Frac{\partial \mathcal{N}_n}{\partial\chi} (s,\chi(s)).(\chi'(s))
  &=& 0
   \\
     F'(s,\chi(s)).(\chi'(s))
  &=& 0
\end{eqnarray*}
Using the expression~\ref{eq-continuation-normalization}
for $\mathcal{N}_n$ we obtain:
\begin{eqnarray}
   - 1
   + \lambda'(s_n)\lambda'(s)
   + (u'(s_n),u'(s))
  &=& 0
  \label{eq-continuation-diff1}
   \\
     \Frac{\partial F}{\partial\lambda} (\lambda(s),u(s))\,\lambda'(s)
   + \Frac{\partial F}{\partial u}      (\lambda(s),u(s)).(u'(s))
  &=& 0
  \label{eq-continuation-diff2}
\end{eqnarray}
Here $(.,.)$ denotes the scalar product of the $V$ space for $u$.
Let us choose $s=s_n$, for any $n\geq 0$: we obtain
\begin{eqnarray*}
     |\lambda'(s_n)|^2
   + \|u'(s_n)\|^2 &=& 1 \\
     \Frac{\partial F}{\partial\lambda} (\lambda_n,u_n)\,\lambda'(s_n)
   + \Frac{\partial F}{\partial u}      (\lambda_n,u_n).(u'(s_n))
   &=& 0
\end{eqnarray*}
where we use the notations
$\lambda_n=\lambda(s_n)$
and $u_n=u(s_n)$,
and
where $\|.\|$ denotes the norm of the $V$ space.
Thus 
\begin{eqnarray*}
  \chi'(s_n)
  &=&
    \left( \begin{array}{c}
	   \lambda'(s_n) \\
	   u'(s_n)
    \end{array}\right)
    \\
  &=&
    \Frac{1}
	 {\left(
          1 
          +
          \left\|
              \left(\Frac{\partial F}{\partial u}      (\lambda_n,u_n)\right)^{-1}
                    \Frac{\partial F}{\partial\lambda} (\lambda_n,u_n)
	  \right\|^2
          \right)^{1/2}
         }
    \left( \begin{array}{c}
	    1 \\
	    - \left(\Frac{\partial F}{\partial u}      (\lambda_n,u_n)\right)^{-1}
                    \Frac{\partial F}{\partial\lambda} (\lambda_n,u_n)
    \end{array}\right)
\end{eqnarray*}
The previous relation requires 
\mbox{$
	\Frac{\partial F}{\partial u}      (\lambda_n,u_n)
$} to be nonsingular, e.g. the computation is not possible at
a singular point $(\lambda_n,u_n)$.

For a singular point, suppose that $n\geq 1$
and that both 
$(\lambda_{n},u_{n})$,
$(\lambda_{n-1},u_{n-1})$ and
$(\dot{\lambda}_{n-1},\dot{u}_{n-1})$ are known. 
By differentiating~\eqref{eq-continuation} at step $n-1$
we get the equivalent of
\eqref{eq-continuation-diff1}-\eqref{eq-continuation-diff2}
at step $n-1$ that is then evaluated for $s=s_n$.
We get:
\begin{eqnarray*}
     \lambda'(s_{n-1})
     \lambda'(s_n)
   + (u'(s_{n-1}), u'(s_n)) &=& 1 \\
     \Frac{\partial F}{\partial\lambda} (\lambda_{n},u_{n})\,\lambda'(s_n)
   + \Frac{\partial F}{\partial u}      (\lambda_{n},u_{n}).(u'(s_n))
   &=& 0
\end{eqnarray*}
that writes equivalently
\begin{eqnarray}
   \left( \begin{array}{cc}
     \lambda'(s_{n-1}) &
     u'(s_{n-1})
	\\
     \Frac{\partial F}{\partial\lambda} (\lambda_{n},u_{n}) &
     \Frac{\partial F}{\partial u}      (\lambda_{n},u_{n})
   \end{array}\right)
   \left( \begin{array}{c}
     \lambda'(s_n) \\
     u'(s_n)
   \end{array}\right)
   =
   \left( \begin{array}{c}
     1 \\
     0
   \end{array}\right)
   \label{eq-continuation-tangent}
\end{eqnarray}
The matrix involved in the left hand side is exactly the Jacobean
of $\mathcal{F}_{n-1}$ evaluated at point $\chi_n=(\lambda_n,u_n)$.
This Jacobean is expected to be nonsingular at a simple limit point.

Thus, at the first step, we suppose that the initial point $(\lambda_0,u_0)$
is non-singular and we compute $(\dot{\lambda}_0,\dot{u}_0)$.
Then, at the begining of the $n$-th step, $n\geq 1$, of the Keller continuation algorithm,
we suppose that both $(\lambda_{n-1},u_{n-1})$ and $(\dot{\lambda}_{n-1},\dot{u}_{n-1})$ are known.
We consider the problem
\mbox{$
	\mathcal{F}_{n-1}(s,\chi(s))=0
$}.
Here, $\mathcal{F}_{n-1}(s,\chi)$ is completely defined at the vicinity of
$(s_{n-1},\lambda_{n-1},u_{n-1})$.
The step control procedure furnishes as usual a parameter step $\Delta s_{n-1}$ and we set
$s_{n}=s_{n-1}+\Delta s_{n-1}$.
The Newton method is performed and we get $(\lambda_{n},u_{n})$.
Finaly, we compute $\dot{\lambda}_n$ and $\dot{u}_n$ from \eqref{eq-continuation-tangent}.

Recall that the function $\mathcal{F}_{n-1}$ depends upon $n$
and should be \emph{refreshed} at the  begining of each iteration
by using the values $(\dot{\lambda}_{n-1},\dot{u}_{n-1})$.

The Keller continuation algorithm writes:

{\bf algorithm~2} (\emph{Keller continuation})
\begin{itemize}
\item $n=0$:     Let $(s_0,\,\chi_0=(\lambda_0,u_0))$ be given.
      The recurrence requires also $\dot{\chi}_0$ and
      its orientation $\varepsilon_0 \in\{-1,+1\}$:
      they could either be given or computed.
      When computing $(\dot{\chi}_0,\varepsilon_0)$, the present algorithm supposes
      that $(\lambda_0,u_0)$ is a regular point: on a singular point,
      e.g. a bifurcation one, there a several possible directions, and one should be chosen.
      Then, choose $\varepsilon=\pm 1$ and compute $\dot{\chi}_0$ in three steps:
      \begin{eqnarray*}
	\Frac{{\rm d}u}{{\rm d}\lambda}(\lambda_0)
		&=& 
		- \left( \Frac{\partial F}{\partial u}      (\lambda_0,u_0)\right)^{-1}
                         \Frac{\partial F}{\partial \lambda}(\lambda_0,u_0)
		\\
	c &=& \left(1 + \left\| \Frac{{\rm d}u}{{\rm d}\lambda}(\lambda_0) \right\| \right)^{-1/2}
		\\
        \dot{\chi}_0 
		&\stackrel{def}{=}& (\dot{\lambda}_0, \, \dot{u}_0)^T
		\ = \ c \, \left( 1 , \ \Frac{{\rm d}u}{{\rm d}\lambda}(\lambda_0) \right)^T
      \end{eqnarray*}
\item $n\geq 0$: Let $(s_n,\,\chi_n\!=\!(\lambda_n,u_n))$, 
        $\dot{\chi}_n = (\dot{\lambda}_n, \dot{u}_n)$ and $\varepsilon_n$ being known.
      \begin{itemize}
      \item[1)] First choose a step $\Delta s_n$ and set $s_{n+1}=s_n+\Delta s_n$,
		as in the classical continuation algorithm
      \item[2)] Then, perform a prediction, as usual:
        \begin{eqnarray*}
	  y_0 &=& \chi_n + \varepsilon_n \Delta s_n \dot{\chi}_n
        \end{eqnarray*}
      \item[3)] Also as usual, do a correction loop:
                for all $k\geq 0$, $y_k$ being known,
                compute
        \begin{eqnarray*}
	  y_{k+1}
            &=&
            y_k
            - 
            \left( \Frac{\partial \mathcal{F}_n}{\partial \chi}(s_{n+1},y_{k}) \right)^{-1}
                                  \mathcal{F}_n                (s_{n+1},y_{k})
        \end{eqnarray*}
        At convergence of the correction loop, set $\chi_{n+1} = y_\infty$.
	This Newton correction loop can be replaced by a damped Newton one.
%ICI
      \item[4)] \red{Check}~: if $n\geq 1$, compute the following angle cosinus:
      \begin{eqnarray*}
        c_1 &=&
            (\chi_{n+1}-\chi_n,\, \chi_n-\chi_{n-1})
	  \\
	  &=& 
            (\lambda_{n+1}-\lambda_n)(\lambda_n-\lambda_{n-1})
	    +
            (u_{n+1}-u_n,\, u_n-u_{n-1})_V
	  \\
        c_2 &=&
            (\dot{\chi}_n,\, \chi_{n+1}-\chi_n)
	  \\
	  &=& 
            (\dot{\lambda}_n,\, \lambda_{n+1}-\lambda_n)
	    +
            (\dot{u}_n,\, u_{n+1}-u_n)_V
      \end{eqnarray*}
      When either $c_1 \leq 0$ or $c_2 \leq 0$, then decreases $\Delta s_n$ and go back at step~1.
      Otherwise the computation of $\chi_{n+1}$ is validated.
      \item[5)] Finally, compute $\dot{\chi}_{n+1} = (\dot{\lambda}_{n+1}, \dot{u}_{n+1})$ as:
        \begin{eqnarray*}
            \dot{\chi}_{n+1}
		&=&
		-
        	\left( \Frac{\partial \mathcal{F}_n}{\partial \chi}(s_{n+1},\chi_{n+1}) \right)^{-1}
                     \ \Frac{\partial \mathcal{F}_n}{\partial s}   (s_{n+1},\chi_{n+1})
        \end{eqnarray*}
      If $\varepsilon_n\ (\dot{\chi}_{n+1},\, \dot{\chi}_{n}) \geq 0$
      then set $\varepsilon_{n+1} =  \varepsilon_{n}$
      else $\varepsilon_{n+1} = -\varepsilon_{n}$.
      \end{itemize}
\end{itemize}
The Keller algorithm with the spherical norm is simply obtained
by replacing $\mathcal{F}_n$
by $\widetilde{\mathcal{F}}_n$
%
Both algorithm variants still require to save $\dot{\chi}_n$ and $\varepsilon_n$
at each step for restarting nicely with the same sequence of computation.
The only drawback is that, at a restart of the algorithm,
we skip the first \red{Check} step, and its possible 
to go back at the first iterate if $\Delta s_0$ is too large.
A possible remedy is, when restarting, to furnish two previous iterates,
namely $\chi_{-1}$ and $\chi_0$, together with $\dot{\chi}_0$:
$\chi_{-1}$ is used only for the \red{Check} of a possible change of direction.

\citet{How-2009-cont} suggested that the
Keller algorithm with spherical norm is more robust
that its variant with orthogonal one.
%
\citet{How-2009-cont} reported that
\begin{quote}
\begin{em}
	[...] the spherical constraint $N_2$ is
	\red{much more efficient} than the orthogonal constraint $N_1$,
	as $N_2$ required only one step of length 0.01 to exceed the target
	value, while $N_1$ required 25, with 8 additional convergence failures.

	[...] spherical constraint was seen to be \red{more efficient} than an orthogonal constraint in a
	region of high curvature of the solution manifold, while both constraints performed 
	similarly in regions of low or moderate curvature.
\end{em}
\end{quote}

% ---------------------------------------
%\myexamplelicense{keller.h} 
\myexamplelicense{combustion_keller.cc}
% ---------------------------------------

\begin{figure}[htb]
     \begin{center}
       \begin{tabular}{cc}
          \includegraphics[height=6cm]{combustion_arc_umax.pdf} &
          \includegraphics[height=6cm]{combustion_upper.pdf}
       \end{tabular}
     \end{center}
     \caption{Combustion problem:
        (left) $\|u_h\|_{0,\infty,\Omega}$ vs $\lambda$ when $h=1/10$ and $1/80$.
	The full branch of solutions when $\lambda\geq 0$, with the limit point and the upper part of the branch.
	(right) solution of the upper branch when $\lambda=10^{-2}$.
        }
   \label{fig-combustion-limit-point}
\end{figure}

\subsubsection*{How to run the program}
% -------------------------------------
Enter the following unix commands:
\begin{verbatim}
  make combustion_keller combustion_keller_post combustion_error
  mkgeo_grid -e 20 > line-20.geo
  ./combustion_keller line-20 > line-20.branch
  ./combustion_keller_post < line-20.branch
\end{verbatim}
The last command scans the file \code{line-20.branch} containing
the branch of solutions $(\lambda(s),u(s))$ and compute some useful
informations for graphical representations.
Observe on Fig.~\ref{fig-combustion-limit-point}.left
the full branch of solutions when $\lambda\geq 0$,
with the limit point and the upper part of the branch.
Compare it with Fig.~\ref{fig-combustion-umax}, where the continuation
algorithm was limited to the lower part part of the branch.
Next, in order to vizualise the last computed solution, enter:
\pindexopt{branch}{-toc}%
\pindexopt{branch}{-extract}%
\pindexopt{branch}{-branch}%
\begin{verbatim}
  branch line-20.branch -toc
  branch line-20.branch -extract 41 -branch | ./combustion_error 1
\end{verbatim}
Please, replace $41$ by the last computed index on the branch.
Fig.~\ref{fig-combustion-limit-point}.right represents this solution
and compares it ith the exact one.
Observe the excellent agreement.

% ---------------------------------------
\myexamplelicense{combustion_keller_post.cc}
% ---------------------------------------

