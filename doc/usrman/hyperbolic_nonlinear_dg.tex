% --------------------------------------------
\subsection{Abstract setting}
% --------------------------------------------
\label{sec-hyperbolic-dg}%
\cindex{problem!hyperbolic nonlinear equation}
The aim of this paragraph is to study the
discontinuous Galerkin discretization of scalar nonlinear hyperbolic equations.
This section presents the general framework and discretization tools
while the next section illustrates the method for the Burgers equation.

% ------------------------------------
\subsubsection*{Problem statement}
% ------------------------------------
A time-dependent nonlinear hyperbolic problem writes in general
form~\citep[p.~99]{PieErn-2012}:

\ \ $(P)$: \emph{find $u$, defined in $]0,T[ \times \Omega$, such that}
\begin{subequations}
\begin{eqnarray}
  \Frac{\partial u}{\partial t}
  +
  {\rm div}\,{\bf f}(u)
  &=& 0 
  \ \mbox{ in } ]0,T[\times \Omega
	\label{eq-dg-hyp-eqn}
  	\\
  u(t\!=\!0) &=& u_0 
  \ \mbox{ in } \Omega 
	\label{eq-dg-hyp-ini}
	\\
  {\bf f}(u).{\bf n} &=& \Phi({\bf n};\,u,g)
  \mbox{ on } ]0,T[\times \partial\Omega
	\label{eq-dg-hyp-bc}
\end{eqnarray}
where $T>0$, $\Omega\subset \mathbb{R}^d$, $d=1,2,3$
and the initial condition ${\bf u}_0$ being known.
As usual, ${\bf n}$ denotes the outward unit normal
on the boundary $\partial\Omega$.
The function
\mbox{$
  {\bf f}: \mathbb{R} \longrightarrow \mathbb{R}^d
$} is also known and supposed to be continuously differentiable.
The initial data $u_0$, defined in $\Omega$,
and the boundary one, $g$, defined on $\partial\Omega$ are given. 
The function $\Phi$, called the Godunov flux associated to ${\bf f}$, is defined,
for all $\boldsymbol{\nu}\in\mathbb{R}^d$
and $a, b  \in\mathbb{R}$, by
\begin{equation}
  \Phi(\boldsymbol{\nu};\,a,b)
  =
  \left\{
    \begin{array}{ll}
      {\displaystyle \min_{v\in [a,b]}}
  	{\bf f}(v).\boldsymbol{\nu} 
      & \mbox{ when } a \leq b \\
      {\displaystyle \max_{v\in [b,a]}}
  	{\bf f}(v).\boldsymbol{\nu} 
      & \mbox{ otherwise }
    \end{array}
  \right.
  \label{eq-dg-hyp-flux-godunov}
\end{equation}
\end{subequations}
% Observe that the boundary condition is neither obvious nor explicit
% in the case of a nonlinear problem given by a general ${\bf f}$ function.

Note that, with this general formalism,
the linear transport problem considered in
the previous section~\ref{sec-transport-dg}
corresponds to (see e.g.~\citealp[p.~104]{PieErn-2012}:
\begin{subequations}
\begin{eqnarray}
  \boldsymbol{f}(u) &=& \boldsymbol{a}u
	\label{eq-dg-hyp-transport-f}
	\\
  \Phi(\boldsymbol{n};\,u,v) &=& 
  	\boldsymbol{a}.\boldsymbol{n}
	\Frac{u+v}{2}
	+
  	\Frac{|\boldsymbol{a}.\boldsymbol{n}|}{2}
	(u-v)
	\label{eq-dg-hyp-transport-phi}
\end{eqnarray}
\end{subequations}
% ------------------------------------
\subsubsection*{Space discretization}
% ------------------------------------
In this section, we consider first the semi-discretization 
with respect to space while the problem remains continuous
with respect to time.
The semi-discrete problem writes in variational
form~\citep[p.~100]{PieErn-2012}:

\ \ $(P)_h$: \emph{find $u_h\in C^1([0,T],X_h)$ such that}
\begin{eqnarray*}
  \int_\Omega
    \Frac{\partial u_h}{\partial t}
    v_h
    \,{\rm d}x
  -
  \int_\Omega
    {\bf f}(u_h)
    .\nabla_h v_h
    \,{\rm d}x
  +
  \sum_{S\in \mathcal{S}_h^{(i)}}
  \int_S
    \Phi({\bf n};\,u_h^-,u_h^+)
    \jump{v_h}
    \,{\rm d}s
  +
  \int_{\partial\Omega}
    \Phi({\bf n};\,u_h,g)
    v_h
    \,{\rm d}s
  &=& 0,
  \ \ \forall v_h\in X_h
  \\
  u_h(t\!=\!0) &=& \pi_h(u_0)
\end{eqnarray*}
where $\pi_h$ denotes the Lagrange
interpolation operator on $X_h$
and others notations has been introduced in the
previous section.

For convenience, we introduce the
discrete operator $G_h$, defined for all 
$u_h,v_h\in X_h$ by
\begin{eqnarray}
  \int_\Omega
    G_h(u_h)
    v_h
    \,{\rm d}x
  =
  -
  \int_\Omega
    {\bf f}(u_h)
    .\nabla_h v_h
    \,{\rm d}x
  +
  \sum_{S\in \mathcal{S}_h^{(i)}}
    \int_S
    \Phi({\bf n};\,u_h^-,u_h^+)
    \jump{v_h}
    \,{\rm d}s
  +
  \int_{\partial\Omega}
    \Phi({\bf n};\,u_h,g)
    v_h
    \,{\rm d}s
  \label{eq-dg-hyp-def-Gh}
\end{eqnarray}
For a given $u_h\in X_h$, we also define the linear form $g_h$ as
\begin{eqnarray*}
  g(v_h)
  &=&
  \int_\Omega
    G_h(u_h)
    v_h
    \,{\rm d}x
\end{eqnarray*}
As the matrix $M$, representing the $L^2$ scalar product in $X_h$,
is block-diagonal, it can be easily inverted
at the element level, and for a given $u_h\in X_h$,
we have $G(u_h)=M^{-1}g_h$.
%
Then, the problem
writes equivalently as a set of coupled
nonlinear ordinary differential equations.

\ \ $(P)_h$: \emph{find $u_h\in C^1([0,T],X_h)$ such that}
\begin{eqnarray*}
    \Frac{\partial u_h}{\partial t}
    +
    G_h(u_h)
    &=&
    0
\end{eqnarray*}

% ---------------------------------------
\subsubsection*{Time discretization}
% ---------------------------------------
\label{sec-hyperbolic-runge-kutta}%
\cindex{method!Runge-Kutta scheme}%

% TODO: pas clair: Gh ici et lh(vh) dans le code
%       et on ne sais pas d'ou vient inv_m*lh
Let $\Delta t>0$ be the time step.
The previous nonlinear ordinary differential equations
are discretized by using a specific explicit Runge-Kutta
with intermediates states~\citep{ShuOsh-1988,GotShu-1998,GotShuTad-2001}.
This specific variant of the usual Runge-Kutta scheme,
called \emph{strong stability preserving},
is suitable for avoiding possible spurious oscillations of the approximate
solution when the exact solution has a poor regularity.
Let $u_h^n$ denotes the approximation of $u_h$ at
time $t_n=n\Delta t$, $n\geq 0$.
Then $u_h^{n+1}$ is defined by 
recurrence:
\begin{eqnarray*}
  u_h^{n,0} &=& u_h^n	
    \\
  u_h^{n,i} &=& 
	\sum_{j=0}^{i-1}
	   \alpha_{i,j} u_h^{n,j}
           -
	   \Delta t\,\beta_{i,j} G_h\left(u_h^{n,j}\right)
	, \ \ 1\leq i\leq p
    \\
  u_h^{n+1} &=& u_h^{n,p}	
\end{eqnarray*}
where the coefficients satisfy $\alpha_{i,j}\geq 0$
and $\beta_{i,j}\geq 0$
for all $1\leq i\leq p$ and $0\leq j\leq i-1$,
and 
\mbox{$
  \sum_{j=0}^{i-1} \alpha_{i,j}=1
$}
for all $1\leq i\leq p$.
\cindex{method!Euler explicit scheme}%
Note that when $p=1$ this scheme coincides with the usual
explicit Euler scheme.
For a general $p\geq 1$ order scheme, there are $p-1$ intermediate states
$u_h^{n,i}$, $i=1\ldots p-1$.
%
% ---------------------------------------
\myexamplenoinput{runge_kutta_ssp.icc}
% ---------------------------------------
Computation of the coefficients 
$\alpha_{i,j}$
and $\beta_{i,j}$
can be founded in~\citep{ShuOsh-1988,GotShu-1998,GotShuTad-2001}
and are grouped in file~\file{runge_kutta_ssp.icc} 
of the examples directory for convenience.

