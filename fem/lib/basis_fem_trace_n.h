#ifndef _RHEOLEF_BASIS_FEM_TRACE_N_H
#define _RHEOLEF_BASIS_FEM_TRACE_N_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
/*Class:sherwin
NAME: @code{trace_n(RTkd)} - normal trace of a basis on element boundaries
@cindex  polynomial basis
@clindex space
@clindex basis
@clindex reference_element
SYNOPSIS:
  space Mh (omega,"trace_n(RT1d)");
DESCRIPTION:
  @noindent
  This polynomial @code{basis} is used by the postprocessing stage of the
  hybrid high order (HHO) finite element method.
  It is indicated in the @code{space} (see @ref{space class}) by
  inserting the "trace_n(.)" operator around
  an H(div) element characterisation, e.g. Raviart-Thomas
  (see @ref{basis class}).

AUTHOR: Pierre.Saramito@imag.fr
DATE:   23 march 2020
End:
*/
#include "rheolef/basis.h"
namespace rheolef {

template<class T>
class basis_fem_trace_n: public basis_rep<T> {
public:

// typedefs:

  typedef basis_rep<T>                 base;
  typedef reference_element::size_type size_type;
  typedef T                            value_type;

// allocators:

  basis_fem_trace_n (const basis_basic<T>& vec_basis);
 ~basis_fem_trace_n();

// accessors:

  std::string family_name()  const { return _vec_basis.family_name(); }
  size_type   family_index() const { return _vec_basis.family_index(); }
  size_type   degree()       const { return _vec_basis.family_index(); }
  bool is_nodal() const            { return _tr_n_basis.is_nodal(); }
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
  hat_node (reference_element hat_K) const;

  size_type local_ndof_on_side (
        reference_element            hat_K,
        const side_information_type& sid) const;
  void local_idof_on_side (
        reference_element            hat_K,
        const side_information_type& sid,
        Eigen::Matrix<size_type,Eigen::Dynamic,1>& loc_idof) const;

// evaluation of all basis functions at hat_x:

  void evaluate_on_side (
    reference_element            hat_K,
    const side_information_type& sid,
    const point_basic<T>&        hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const;

// internals:

  // accessors to subgeo ndof & nnod by dimension (not available for basis_trace_n)
  virtual size_type first_sid_inod (reference_element hat_K, size_type loc_isid) const {
        return _first_sid_inod [hat_K.variant()][loc_isid];
  }
  virtual size_type first_sid_idof (reference_element hat_K, size_type loc_isid) const {
        return _first_sid_idof [hat_K.variant()][loc_isid];
  }
  void _initialize_cstor_sizes() const;
  void _initialize_data (reference_element hat_K) const;
  void _compute_dofs (
    reference_element     hat_K, 
    const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod, // scalar-valued case
          Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const;

  // overload visualization:
  virtual void put_scalar_valued (std::ostream& os, reference_element hat_K) const;

protected:
// data:
  basis_basic<T>   _vec_basis, 
                   _tr_n_basis;

  mutable std::array<
             Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>,
             reference_element::max_variant>        _hat_node;

  // nodes and dofs are organized by side=0..6 of subgeos, for each hat_K:
  mutable std::array<
	    std::array<size_type,reference_element::max_side_by_variant+1>,
            reference_element::max_variant>
	  _first_sid_inod, _first_sid_idof;

  // working array:
  mutable Eigen::Matrix<T,Eigen::Dynamic,1> _sid_value;
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_FEM_TRACE_N_H
