///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "basis_fem_Pk_bernstein.h"
#include "basis_fem_Pk_lagrange.h"
#include "piola_fem_lagrange.h"
#include "rheolef/rheostream.h"
#include "equispaced.icc"
#include "warburton.icc"
#include "eigen_util.h"
#include "basis_on_pointset_evaluate.icc"

namespace rheolef {
using namespace std;

// =========================================================================
// basis members
// =========================================================================
template<class T>
basis_fem_Pk_bernstein<T>::~basis_fem_Pk_bernstein()
{
}
template<class T>
basis_fem_Pk_bernstein<T>::basis_fem_Pk_bernstein (size_type degree, const basis_option& sopt) 
  : basis_rep<T> (sopt),
    _raw_basis("B"+std::to_string(degree)),
    _hat_node(),
    _vdm(),
    _inv_vdm()
{
  // Bezier nodes are equispaced => forced
  base::_sopt.set_node(basis_option::equispaced);
  base::_name = base::standard_naming (family_name(), degree, base::_sopt);
  _initialize_cstor_sizes();

  // piola FEM transformation:
  typedef piola_fem_lagrange<T> piola_fem_type;
  base::_piola_fem.piola_fem<T>::base::operator= (new_macro(piola_fem_type));
}
template<class T>
void
basis_fem_Pk_bernstein<T>::_initialize_cstor_sizes() const
{
  basis_fem_Pk_lagrange<T>::initialize_local_first (
    degree(),
    base::is_continuous(),
    base::_ndof_on_subgeo_internal,
    base::_ndof_on_subgeo,
    base::_nnod_on_subgeo_internal,
    base::_nnod_on_subgeo,
    base::_first_idof_by_dimension_internal,
    base::_first_idof_by_dimension,
    base::_first_inod_by_dimension_internal,
    base::_first_inod_by_dimension);
}
template<class T>
const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>&
basis_fem_Pk_bernstein<T>::hat_node (reference_element hat_K) const
{
  base::_initialize_data_guard (hat_K);
  return _hat_node [hat_K.variant()];
}
template<class T>
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_fem_Pk_bernstein<T>::vdm (reference_element hat_K) const
{
  base::_initialize_data_guard (hat_K);
  return _vdm [hat_K.variant()];
}
template<class T>
const Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic>&
basis_fem_Pk_bernstein<T>::inv_vdm (reference_element hat_K) const
{
  base::_initialize_data_guard (hat_K);
  return _inv_vdm [hat_K.variant()];
}
template<class T>
void
basis_fem_Pk_bernstein<T>::_initialize_data (reference_element hat_K) const
{
  // initialization is similar to Pk-Lagrange
  size_type k = degree();
  size_type variant = hat_K.variant();

  // nodes:
  switch (base::_sopt.get_node()) {
    case basis_option::equispaced:
          pointset_lagrange_equispaced (hat_K, k, _hat_node[variant]);
          break;
    case basis_option::warburton:
          pointset_lagrange_warburton  (hat_K, k, _hat_node[variant]); break;
    default: error_macro ("unsupported node set: "<<base::_sopt.get_node_name());
  }
  // vdm:
  details::basis_on_pointset_evaluate (_raw_basis, hat_K, _hat_node[variant], _vdm[variant]);
  check_macro (invert(_vdm[variant], _inv_vdm[variant]),
        "unisolvence failed for " << base::name() <<"(" << hat_K.name() << ") basis");
}
// evaluation of all basis functions at hat_x:
template<class T>
void
basis_fem_Pk_bernstein<T>::evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const
{
  base::_initialize_data_guard (hat_K);
  _raw_basis.evaluate (hat_K, hat_x, value);
}
// evaluate the gradient:
template<class T>
void
basis_fem_Pk_bernstein<T>::grad_evaluate (
  reference_element           hat_K,
  const point_basic<T>&       hat_x,
  Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const 
{
  base::_initialize_data_guard (hat_K);
  _raw_basis.grad_evaluate (hat_K, hat_x, value);
}
// dofs for a scalar-valued function
template<class T>
void
basis_fem_Pk_bernstein<T>::_compute_dofs (
  reference_element     hat_K,
  const Eigen::Matrix<T,Eigen::Dynamic,1>&   f_xnod, 
        Eigen::Matrix<T,Eigen::Dynamic,1>&   dof) const
{
  base::_initialize_data_guard (hat_K);
  dof = _inv_vdm[hat_K.variant()]*f_xnod;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template class basis_fem_Pk_bernstein<T>;

_RHEOLEF_instanciation(Float)

}// namespace rheolef
