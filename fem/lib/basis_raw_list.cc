///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// TODO: how to a user's defined basis to the list ?
#include "rheolef/basis_raw.h"
#include "basis_raw_monomial.h"
#include "basis_raw_bernstein.h"
#include "basis_raw_dubiner.h"
namespace rheolef {
using namespace std;

template<class T>
basis_raw_rep<T>*
basis_raw_rep<T>::make_ptr (std::string name)
{
  if (name == "") return 0;
  if (name[0] == 'M') return new_macro(basis_raw_monomial<T>(name));
  if (name[0] == 'B') return new_macro(basis_raw_bernstein<T>(name));
  if (name[0] == 'D') return new_macro(basis_raw_dubiner<T>(name));
  error_macro ("undefined raw basis `" << name << "'");
  return 0;
}
// instanciation in library:
template basis_raw_rep<Float>* basis_raw_rep<Float>::make_ptr (string);

} // namespace rheolef
