///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// P1qd approximation: linear polynoms in quadrangles or hexahedra
// used for Stokes : incompressible velocity-pressure element P2-P1q
//                   P1qd is globally discontinuous (see P1qd_numbering)
// see Brezzi-Fortin 1991, page 266.
#include "basis_symbolic.h"
using namespace rheolef;
using namespace std;
using namespace GiNaC;

class P1qd_symbolic : public basis_symbolic_nodal
{
public:
    P1qd_symbolic ();
};
P1qd_symbolic::P1qd_symbolic ()
: basis_symbolic_nodal("P1qd",1)
{
  on('q') << node ( 0.5,  0.5)
          << node ( 0.5, -0.5)
          << node (-0.5, -0.5)
          << poly (1)
          << poly (x)
          << poly (y)
	  << end;
  on('H') << node ( 0.5,  0.5,  0.5)
          << node ( 0.5,  0.5, -0.5)
          << node ( 0.5, -0.5, -0.5)
          << node (-0.5, -0.5, -0.5)
          << poly (1)
          << poly (x)
          << poly (y)
          << poly (z)
          << end;
}
int main (int argc, char **argv) {
	P1qd_symbolic P1qd;
	P1qd.put_cxx_main (argc,argv);
}
