#ifndef _RHEO_BASIS_SYMBOLIC_H
#define _RHEO_BASIS_SYMBOLIC_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include <ginac/ginac.h>
#include "rheolef/point.h"
#include "rheolef/reference_element.h"
namespace rheolef { 

class basis_symbols {
public:

// allocator:

	basis_symbols() : x("x"), y("y"), z("z") {}

// data:
	GiNaC::symbol x, y, z;
};
class basis_symbolic_nodal_on_geo: public basis_symbols {
public:

// typedefs:

	typedef std::vector<int>::size_type size_type;
	typedef GiNaC::ex                   polynom_type;
	typedef GiNaC::ex                   value_type;
	struct  end_type {};

// allocators:

	basis_symbolic_nodal_on_geo() 
	 : basis_symbols(), _name("unnamed"), 
	   _hat_K(), _node(0), _poly(0), 
	   _basis(), _grad_basis() {}

// accessors:

	size_type size() const { return _node.size(); }
	size_type dimension() const { return _hat_K.dimension(); }
	const reference_element& hat_K() const { return _hat_K; }
	std::string name() const { return _name; }
	const point_basic<GiNaC::ex>& node (size_type i) const { return _node[i]; }
	const polynom_type& polynom (size_type i) const { return _poly[i]; }

// modifiers:

	void set_name(std::string str) { _name = str; }
	void set_hat_K(reference_element::variant_type t) {
		_hat_K.set_variant(t); }
	point_basic<GiNaC::ex>& node (size_type i) { return _node[i]; }
	polynom_type& polynom (size_type i) { return _poly[i]; }
	
	void resize(size_type n) { 
		_node.resize(n);  _poly.resize(n);
		_basis.resize(n); _grad_basis.resize(n); }

	void add_polynom (const polynom_type& p) { _poly.push_back(p); }
	basis_symbolic_nodal_on_geo& operator<< (const polynom_type& p) {
		add_polynom (p); return *this; }

	void add_node (const point_basic<GiNaC::ex>& x) { _node.push_back(x); }
	void add_node (const Float& x0, const Float& x1=0, const Float& x2=0) { 
		add_node(point_basic<GiNaC::ex>(x0,x1,x2)); } 
	basis_symbolic_nodal_on_geo& operator<< (const point_basic<GiNaC::ex>& x) {
		add_node (x); return *this; }
	
	void make_node_basis ();
	basis_symbolic_nodal_on_geo& operator<< (end_type(*)()) {
		make_node_basis(); return *this; }

// method:

	value_type eval (const polynom_type& p,
		const point_basic<polynom_type>& x, size_type d = 3) const;
	GiNaC::matrix vandermonde_matrix (
		const std::vector<polynom_type>& p, size_type d = 3) const; 

// outputs:

	void put_cxx_header (std::ostream& out) const;
	void put_cxx_body   (std::ostream& out) const;

// utility:

	polynom_type indexed_symbol (const polynom_type& expr0) const;

// data:

protected:
	std::string                       	_name;
	reference_element                       _hat_K;
	std::vector<point_basic<GiNaC::ex> >	_node;
	std::vector<polynom_type>  		_poly;
	std::vector<polynom_type>  		_basis;
	std::vector<point_basic<polynom_type> > _grad_basis;
};
class basis_symbolic_nodal 
 : public basis_symbols, public std::vector<basis_symbolic_nodal_on_geo> {
public:

// typedefs:

	typedef basis_symbolic_nodal_on_geo::size_type size_type;
	typedef basis_symbolic_nodal_on_geo::polynom_type polynom_type;
	typedef basis_symbolic_nodal_on_geo::end_type end_type;

// allocators:

	basis_symbolic_nodal(std::string nam, size_type deg)
	: basis_symbols(),
	  std::vector<basis_symbolic_nodal_on_geo>(reference_element::max_variant),
	  _name(nam),
	  _family_name(),
	  _degree(deg),
	  _have_index_parameter(false),
	  _have_continuous_feature(false)
	  {
	      GiNaC::Digits = 2*std::numeric_limits<Float>::digits10;
	      for (size_type i = 0; i < reference_element::max_variant; i++) {
	        operator[](i).x = x;
	        operator[](i).y = y;
	        operator[](i).z = z;
		operator[](i).set_hat_K(reference_element::variant_type(i));
		operator[](i).set_name(nam);
	      }
	  }

// accessors:

	size_type degree() const { return _degree; }
	bool have_index_parameter() const { return _have_index_parameter; }
	bool have_continuous_feature() const { return _have_continuous_feature; }

	const basis_symbolic_nodal_on_geo& on(
	    reference_element::variant_type t) const {
	    return operator[] (t);
	}
	basis_symbolic_nodal_on_geo& on(
	    reference_element::variant_type t) {
	    return operator[] (t);
	}
	basis_symbolic_nodal_on_geo& on(char t) {
	    reference_element hat_K;
	    hat_K.set_name(t);
	    return operator[] (hat_K.variant()); }

	std::string name() const { return _name; }
	std::string family_name() const { return _family_name; }

// modifiers:

	void set_name(std::string str) { _name = str; }
	void set_degree_parameter (bool dp = true) { _have_index_parameter = dp; }
	void set_continuous_feature (bool cf = true) { _have_continuous_feature = cf; }

// syntax helpers:

	static polynom_type poly (const polynom_type& p) { return p; } 
	static point_basic<GiNaC::ex> node (const point_basic<GiNaC::ex>& x) {
		return x; }
	static point_basic<GiNaC::ex> node (
		const GiNaC::ex& x0, const GiNaC::ex& x1=0, const GiNaC::ex& x2=0) { 
		return point_basic<GiNaC::ex>(x0,x1,x2); } 
	static end_type end () { return end_type(); } 

// outputs:

	void put_cxx_header(std::ostream& out) const;
	void put_cxx_body  (std::ostream& out) const;
	void put_cxx_main (int argc, char **argv) const;

// data:
protected:
	std::string   _name;
	std::string   _family_name;
	size_type     _degree;
	bool	      _have_index_parameter;
	bool	      _have_continuous_feature;
};
}// namespace rheolef
#endif // _RHEO_BASIS_SYMBOLIC_H
