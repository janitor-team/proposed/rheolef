///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// P1 approximation
//
#include "basis_symbolic.h"
using namespace rheolef;
using namespace std;

class P1_symbolic : public basis_symbolic_nodal
{
public:
    P1_symbolic ();
};
P1_symbolic::P1_symbolic ()
: basis_symbolic_nodal("P1",1)
{
  basis_symbolic_nodal::_family_name = "P";
  basis_symbolic_nodal::set_degree_parameter();
  basis_symbolic_nodal::set_continuous_feature();
  on('p') << node(0) << poly (1) << end;
  on('e') << node (0)
	  << node (1)
	  << poly (1)
	  << poly (x)
	  << end;
  on('t') << node (0, 0)
          << node (1, 0)
          << node (0, 1)
          << poly (1)
          << poly (x)
          << poly (y)
	  << end;
  on('q') << node (-1, -1)
          << node ( 1, -1)
          << node ( 1,  1)
          << node (-1,  1)
          << poly (1)
          << poly (x)
          << poly (y)
          << poly (x*y)
	  << end;
  on('T') << node (0, 0, 0)
          << node (1, 0, 0)
          << node (0, 1, 0)
          << node (0, 0, 1)
          << poly (1)
          << poly (x)
          << poly (y)
          << poly (z)
          << end;
  on('P') << node(0, 0,-1)
          << node(1, 0,-1)
          << node(0, 1,-1)
          << node(0, 0, 1)
          << node(1, 0, 1)
          << node(0, 1, 1)
          << poly (1)
          << poly (x)
          << poly (y)
          << poly (z)
          << poly (x*z)
          << poly (y*z)
          << end;
  on('H') << node(-1,-1,-1)
          << node( 1,-1,-1)
          << node( 1, 1,-1)
          << node(-1, 1,-1)
          << node(-1,-1, 1)
          << node( 1,-1, 1)
          << node( 1, 1, 1)
          << node(-1, 1, 1)
          << poly (1)
          << poly (x)
          << poly (y)
          << poly (z)
          << poly (x*y)
          << poly (y*z)
          << poly (z*x)
          << poly (x*y*z)
          << end;
}
int main (int argc, char **argv) {
	P1_symbolic P1;
	P1.put_cxx_main (argc,argv);
}
