#ifndef _RHEOLEF_BASIS_GET_H
#define _RHEOLEF_BASIS_GET_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// basis name: parser utility
//
#include <cstring>
#include "rheolef/basis_option.h"

namespace rheolef {

struct family_index_option_type {
  family_index_option_type (std::string f="", size_t k=0, basis_option o=basis_option()) 
    : family(f), index(k), option(o) {}
  std::string       family;
  size_t            index;
  basis_option      option;
};

void basis_parse_from_string (const std::string& str, family_index_option_type& fio);

} // namespace rheolef
#endif // _RHEOLEF_BASIS_GET_H
