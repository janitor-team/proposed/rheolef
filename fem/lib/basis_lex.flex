%option noyywrap
%{
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito 
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// =========================================================================
//
// lexer for basis specification by string
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 october 2017
//
%}
%%
d	       	    { yylval.string_value = insert(yytext); return D; }
bubble 	       	    { yylval.string_value = insert(yytext); return FAMILY_NO_INDEX; }
P1qd	       	    { yylval.string_value = insert(yytext); return FAMILY_NO_INDEX; }
empty	       	    { yylval.string_value = insert(yytext); return FAMILY_NO_INDEX; }
scalar              { yylval.string_value = insert(yytext); return VALUED; }
vector              { yylval.string_value = insert(yytext); return VALUED; }
tensor              { yylval.string_value = insert(yytext); return VALUED; }
unsymmetric_tensor  { yylval.string_value = insert(yytext); return VALUED; }
tensor3             { yylval.string_value = insert(yytext); return VALUED; }
tensor4             { yylval.string_value = insert(yytext); return VALUED; }
trace_n             { yylval.string_value = insert(yytext); return TRACE; }
cartesian      	    { yylval.string_value = insert(yytext); return COORDINATE; }
rz	       	    { yylval.string_value = insert(yytext); return COORDINATE; }
zr	       	    { yylval.string_value = insert(yytext); return COORDINATE; }
[a-zA-Z][a-zA-Z\_]* { yylval.string_value = insert(yytext); return IDENTIFIER; }
[0-9][0-9]* 	    { yylval.string_value = insert(yytext); return INTEGER; }
.        	    { return *yytext ;  /* default */ }
%%
