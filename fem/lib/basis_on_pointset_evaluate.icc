#ifndef _RHEO_BASIS_ON_POINTSET_EVALUATE_H
#define _RHEO_BASIS_ON_POINTSET_EVALUATE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// basis evaluated on lattice of quadrature formulae: auxilliaries

#include "rheolef/basis.h"
namespace rheolef { namespace details {

// vdm = (phi_j(xi))_{i,j} 
// i.e. the full basis at xi is the i-th row
template<class Basis, class T, class Value>
void basis_on_pointset_evaluate (
  const Basis&                                          b,
  const reference_element&                              hat_K,
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_x,
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&   vdm)
{
  size_t loc_ndof = b.ndof(hat_K);
  size_t loc_nnod = hat_x.size();
  vdm.resize (loc_nnod, loc_ndof);
  Eigen::Matrix<Value,Eigen::Dynamic,1> vdm_row;
  vdm_row.resize (loc_ndof);
  for (size_t loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
    b.evaluate (hat_K, hat_x[loc_inod], vdm_row);
    vdm.row (loc_inod) = vdm_row.transpose(); // TODO: how to avoid a copy: support matrix::row arg ?
  }
}
template<class Basis, class T, class Value>
void basis_on_pointset_grad_evaluate (
  const Basis&                                          b,
  const reference_element&                              hat_K,
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_x,
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&   vdm_grad)
{
  size_t loc_ndof = b.ndof(hat_K);
  size_t loc_nnod = hat_x.size();
  vdm_grad.resize (loc_nnod, loc_ndof);
  Eigen::Matrix<Value,Eigen::Dynamic,1> vdm_grad_row;
  vdm_grad_row.resize (loc_ndof);
  for (size_t loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
    b.grad_evaluate (hat_K, hat_x[loc_inod], vdm_grad_row);
    vdm_grad.row (loc_inod) = vdm_grad_row.transpose(); // TODO: how to avoid a copy: support matrix::row arg ?
  }
}
template<class Basis, class T, class Value>
void basis_on_pointset_evaluate_on_side (
  const Basis&                                          b,
  const reference_element&                              tilde_K,
  const side_information_type&                          sid,
  const Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& hat_x,
  Eigen::Matrix<Value,Eigen::Dynamic,Eigen::Dynamic>&   vdm)
{
  size_t d = tilde_K.dimension();
  check_macro (d > 0, "invalid zero dimension");
  size_t loc_ndof = b.ndof(tilde_K);
  size_t loc_nnod = hat_x.size();
  vdm.resize (loc_nnod, loc_ndof);
  Eigen::Matrix<Value,Eigen::Dynamic,1> vdm_row;
  vdm_row.resize (loc_ndof);
  for (size_t loc_inod = 0; loc_inod < loc_nnod; ++loc_inod) {
    b.evaluate_on_side (tilde_K, sid, hat_x[loc_inod], vdm_row);
    vdm.row (loc_inod) = vdm_row.transpose(); // TODO: how to avoid a copy: support matrix::row arg ?
  }
}

}}// namespace rheolef::details
#endif // _RHEO_BASIS_ON_POINTSET_EVALUATE_H
