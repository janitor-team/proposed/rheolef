#ifndef _RHEOLEF_BASIS_RAW_DUBINER_H
#define _RHEOLEF_BASIS_RAW_DUBINER_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// Duniner initial (raw) basis for fem construction
//
// author: Pierre.Saramito@imag.fr
//
// date: 11 september 2017
//
#include "rheolef/basis_raw.h"
#include "rheolef/ad3.h"
namespace rheolef {

template<class T>
class basis_raw_dubiner: public basis_raw_rep<T> {
public:

// typedefs:

  typedef basis_raw_rep<T>             base;
  typedef typename base::size_type     size_type;
  typedef T                            value_type;

// allocators:

  basis_raw_dubiner (std::string name);
  ~basis_raw_dubiner();

// accessors:

  std::string family_name() const { return "D"; }
  size_type ndof (reference_element hat_K) const;
  bool is_hierarchical() const { return true; }

// evaluation of all basis functions at hat_x:

  void evaluate (
    reference_element                 hat_K,
    const point_basic<T>&             hat_x,
    Eigen::Matrix<T,Eigen::Dynamic,1>& value) const;

  // evaluate the gradient:
  void grad_evaluate (
    reference_element           hat_K,
    const point_basic<T>&       hat_x,
    Eigen::Matrix<point_basic<T>,Eigen::Dynamic,1>& value) const;

protected:
// internals:

  void _initialize (reference_element hat_K) const;

// data:

  mutable std::array<std::vector<size_type>,
             reference_element::max_variant>     _inod2ideg;

  mutable std::array<std::vector<ad3_basic<T> >,
             reference_element::max_variant>     _value_ad;
};

} // namespace rheolef
#endif // _RHEOLEF_BASIS_RAW_DUBINER_H
