#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "basis_interpolate_RTk_h_t.tex"

set size square
set key bottom
set log xy
set xrange [1e-3:1]
set yrange [1e-12:1e3]
graph_ratio_xy = 3./15.
set xtics (\
   '[c]{$1$}'        1, \
   '[c]{$10^{-1}$}'  1e-1, \
   '[c]{$10^{-2}$}'  1e-2, \
   '[c]{$10^{-3}$}'  1e-3)
set ytics (\
   '[r]{$1$}'        1, \
   '[r]{$10^{-5}$}'  1e-5, \
   '[r]{$10^{-10}$}' 1e-10)
set xlabel '[c]{$h$}' 
set  label '[l]{$\|u-\pi_h(u)\|_{L^2}\ $}' at graph 0.08, 0.9
set  label '[l]{$RT_k$, $K=t$}'            at graph 0.62, 0.30

# triangle a droite
rate_A  = 1.0
slope_A = graph_ratio_xy*rate_A
xA =  0.19
yA =  0.64
dxA = 0.10
dyA = dxA*slope_A
set label sprintf('[l]{\scriptsize $%g=k+1$}',rate_A) at graph xA+dxA+0.02, yA+0.5*dyA right
set arrow from graph xA,     yA to     graph xA+dxA, yA     nohead
set arrow from graph xA+dxA, yA to     graph xA+dxA, yA+dyA nohead
set arrow from graph xA+dxA, yA+dyA to graph xA,     yA     nohead

# triangle a droite
rate_B  = 2.0
slope_B = graph_ratio_xy*rate_B
xB =  0.19
yB =  0.49
dxB = 0.10
dyB = dxB*slope_B
set label sprintf('[l]{\scriptsize $%g$}',rate_B) at graph xB+dxB+0.02, yB+0.5*dyB right
set arrow from graph xB,     yB to     graph xB+dxB, yB     nohead
set arrow from graph xB+dxB, yB to     graph xB+dxB, yB+dyB nohead
set arrow from graph xB+dxB, yB+dyB to graph xB,     yB     nohead

# triangle a droite
rate_C  = 3.0
slope_C = graph_ratio_xy*rate_C
xC =  0.19
yC =  0.31
dxC = 0.10
dyC = dxC*slope_C
set label sprintf('[l]{\scriptsize $%g$}',rate_C) at graph xC+dxC+0.02, yC+0.5*dyC right
set arrow from graph xC,     yC to     graph xC+dxC, yC     nohead
set arrow from graph xC+dxC, yC to     graph xC+dxC, yC+dyC nohead
set arrow from graph xC+dxC, yC+dyC to graph xC,     yC     nohead

# triangle a droite
rate_D  = 4.0
slope_D = graph_ratio_xy*rate_D
xD =  0.19
yD =  0.12
dxD = 0.10
dyD = dxD*slope_D
set label sprintf('[l]{\scriptsize $%g$}',rate_D) at graph xD+dxD+0.02, yD+0.5*dyD right
set arrow from graph xD,     yD to     graph xD+dxD, yD     nohead
set arrow from graph xD+dxD, yD to     graph xD+dxD, yD+dyD nohead
set arrow from graph xD+dxD, yD+dyD to graph xD,     yD     nohead

plot \
'basis_interpolate_RTk_h_t.gdat' \
  i 0 \
  u (1/$1):2 \
  t '[r]{$k=0$}' \
  w lp lc rgb '#ff0000' dt 1 pt 2 ps 0.5, \
'basis_interpolate_RTk_h_t.gdat' \
  i 1 \
  u (1/$1):2 \
  t '[r]{$1$}' \
  w lp lc rgb '#008800' dt 1 pt 2 ps 0.5, \
'basis_interpolate_RTk_h_t.gdat' \
  i 2 \
  u (1/$1):2 \
  t '[r]{$2$}' \
  w lp lc rgb '#0000ff' dt 1 pt 1 ps 0.5, \
'basis_interpolate_RTk_h_t.gdat' \
  i 3 \
  u (1/$1):2 \
  t '[r]{$3$}' \
  w lp lc rgb '#ff8800' dt 1 pt 1 ps 0.5

#pause -1
