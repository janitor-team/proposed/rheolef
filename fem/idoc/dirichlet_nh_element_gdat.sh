#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
family="B"

kmin=3
kmax=30

tmp="/tmp/tmp$$"
echo "# dirichlet_nh_element_tst"
echo "# family ${family}k"
for K in e t q; do
  for node in warburton; do
    for raw_poly in dubiner; do
      echo "# K=$K node=$node raw_poly=$raw_poly"
      echo "# k err_l2 err_linf"
      k=3
      while test $k -le $kmax; do
        command="./dirichlet_nh_element_tst ${family}${k} $K $node $raw_poly"
        #echo "! $command"
        eval "$command > $tmp.txt 2>/dev/null"
	err_l2=`grep err_l2 $tmp.txt | gawk '{print $2}'`
	err_linf=`grep err_linf $tmp.txt | gawk '{print $2}'`	
	echo "$k $err_l2 $err_linf"
        k=`expr $k + 1`
      done
      echo; echo
    done
  done
done

rm -f $tmp.txt

