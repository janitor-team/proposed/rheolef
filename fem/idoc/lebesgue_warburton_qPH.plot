#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal cairolatex pdf color standalone
set output "lebesgue_warburton_qPH.tex"
  
set size square
set log y
set colors classic
set key left at graph 0, 0.70
set xrange [0:15]
set yrange [1:1e3]
set xtics 5
set ytics add (\
   '[r]{$1$}'       1, \
   '[r]{$10$}'      10, \
   '[r]{$10^2$}'    1e2, \
   '[r]{$10^{3}$}'  1e3)
set xlabel '[c]{$k$}'
set  label '[l]{$\Lambda_k$}' at graph 0.03, 0.92
set  label '[l]{Warburton}' at graph 0.1, 0.75

plot \
'lebesgue.gdat' \
  i 3 \
  u 1:3 \
  t "quadrangle" \
  w lp, \
'lebesgue.gdat' \
  i 4 \
  u 1:3 \
  t "prism" \
  w lp, \
'lebesgue.gdat' \
  i 5 \
  u 1:3 \
  t "hexahedron" \
  w lp

#pause -1
