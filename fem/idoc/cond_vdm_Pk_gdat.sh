#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2018 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
node=${1-"equispaced"}
poly=${2-"monomial"}
kmax=15
tmp=/tmp/cond-vdm-$$"
echo "# node=$node"
echo "# poly=$poly"
for K in e t T q P H; do
  k=0
  echo "# K=$K"
  echo "# k cond_vdm"
  while test $k -le $kmax; do
    ./vdm_tst $K $k $node $poly 2>/dev/null > $tmp.txt
    cond_vdm=`grep cond $tmp.txt | gawk '{print $2}'`
    echo "$k $cond_vdm"
    k=`expr $k + 1`
  done
  echo ; echo
done
rm -f $tmp.txt
