///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the degree of a basis
// for non-regression test purpose
//
// 	./sherwin_degree_tst t 5
// 	./sherwin_degree_tst t 5 dump | maxima
//
// author: Pierre.Saramito@imag.fr
//
// date: 24 september 2017
//
// note: use ginac, that is not able to simplify rational expressions
//	into polynomials, so use a hack to evaluate the degree
//
#include "rheolef/basis.h"
#include "ginac/ginac.h"
using namespace rheolef;
using namespace std;
using namespace GiNaC;
#define SHERWIN_USE_GINAC
#include "sherwin.icc"

size_t
my_degree (
 const point_basic<symbol>& hat_x,
 reference_element      hat_K,
 ex                     p,
 ex&                    new_p)
{
  switch (hat_K.variant()) {
    case reference_element::p: {
      new_p = p;
      return 0;
    }
    case reference_element::e: {
      new_p = p;
      return p.degree(hat_x[0]);
    }
    case reference_element::t: {
      size_t deg_x = p.degree(hat_x[0]);
      size_t deg = 0;
      new_p = 0;
      ex tmp = p;
      for (size_t i = 0; i <= deg_x; ++i) {
        // ginac is unable to simplify (y-1)^2/(1-y) and such, so hack...
        ex ci_y = tmp.subs(hat_x[0] == 0);
        ci_y = ci_y.normal();
        // cerr << "  coef["<<i<<"] = " <<ci_y << endl;
        ex num = ci_y.numer().normal().collect(hat_x[1]);
        ex den = ci_y.denom().normal().collect(hat_x[1]);
        size_t deg_y_num = num.degree(hat_x[1]);
        size_t deg_y_den = den.degree(hat_x[1]);
        // cerr << "  nume["<<i<<"] = " <<num << " : deg="<< deg_y_num << endl;
        // cerr << "  deno["<<i<<"] = " <<den << " : deg="<< deg_y_den << endl;
        size_t deg_y = deg_y_num - deg_y_den; // hope that division is valid
        deg = max(deg, i + deg_y);
        new_p += ci_y*pow(hat_x[0],i);
	tmp = tmp.diff(hat_x[0],1)/(i+1);
        // cerr << "  DIFF["<<i<<"] = " << tmp << endl;
      }
      return deg;
    }
    default: error_macro ("unsupported element: "<<hat_K.name()); return 0;
  }
}
int main(int argc, char**argv) {
  char   t      = (argc > 1) ?      argv[1][0] : 't';
  size_t degree = (argc > 2) ? atoi(argv[2])   :  0;
  bool   dump   = (argc > 3);
  reference_element hat_K;
  hat_K.set_name(t);
  point_basic<symbol> hat_x = {symbol("x"), symbol("y"), symbol("z")};
  ex alpha = 1, beta = 1;
  std::vector<ex> jacobi_x, jacobi_y, jacobi_z, value;
  eval_sherwin_basis (hat_x, hat_K, degree, alpha, beta, jacobi_x, jacobi_y, jacobi_z, value);
  size_t deg_max = 0;
  for (size_t i = 0; i < value.size(); ++i) {
    ex new_p;
    size_t deg = my_degree(hat_x,hat_K,value[i],new_p);
    deg_max = max(deg_max,deg);
    if (dump) cout << "value[" << i << "] : factor(" << value[i] << "); /* deg="<<deg<< " */" << endl;
  }
  if (deg_max != degree) {
    cerr << "error: deg_max = " << deg_max << " != " << degree << endl;
  } else {
    cerr << "degree checked" << endl;
  }
  return (degree == deg_max) ? 0 : 1;
}
