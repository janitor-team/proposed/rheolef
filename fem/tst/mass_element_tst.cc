///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute the element mass matrix condition number
// for non-regression test purpose
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 september 2017
//
#include "rheolef/basis.h"
#include "rheolef/quadrature.h"
#include "rheolef/eigen_util.h"
#include "form_element.h"
using namespace rheolef;
using namespace std;
using namespace Eigen;

Float one_sf (const point& x) { return 1; }
struct one_vf {
  point operator() (const point& x) const { return point(c,c,c); }
  one_vf (size_t d) : c(1/sqrt(Float(d))) {}
  Float c;
};

template <class Basis, class BuildMass>
void 
show_mass_generic (
  const Basis&                  b,
  reference_element             hat_K,
  SparseMatrix<Float,RowMajor>& mass,
  bool                          dump,
  BuildMass                     build_mass_f)
{
  build_mass_f (b, hat_K, mass);
  Matrix<Float,Dynamic,Dynamic> full_mass = mass;
  if (dump) {
    ofstream out ("mass.mtx");
    put_matrix_market (out, mass);
  }
  cout << setprecision(numeric_limits<Float>::digits10)
       << "approx   " << b.name() << endl
       << "element  " << hat_K.name() << endl
       << "det      " << full_mass.determinant() << endl
       << "cond     " << cond(full_mass) << endl
       << "size     " << mass.rows() << endl
       << "nnz      " << mass.nonZeros()<< endl
       << "fill     " << 1.*mass.nonZeros()/sqr(mass.rows())<< endl
    ;
}
void 
show_mass_raw (
  const basis_raw_basic<Float>& b,
  reference_element hat_K,
  bool              dump)
{
  SparseMatrix<Float,RowMajor> mass;
  show_mass_generic (b, hat_K, mass, dump, build_mass_generic<Float,Float,basis_raw_basic<Float>>);
}
void 
show_mass_fem (
  const basis_basic<Float>&   b,
  reference_element hat_K,
  bool              dump)
{
  SparseMatrix<Float,RowMajor> mass;
  show_mass_generic (b, hat_K, mass, dump, build_mass<Float,basis_basic<Float>>);
  Matrix<Float,Dynamic,1> meas_hat_K;
  Matrix<Float,Dynamic,1> one;
  switch (b.valued_tag()) {
    case space_constant::scalar: b.compute_dof (hat_K, one_sf, one); break;
    case space_constant::vector: b.compute_dof (hat_K, one_vf(hat_K.dimension()), one); break;
    default: error_macro("tensorial interpolate: not yet");
  }
  meas_hat_K = one.transpose()*mass*one;
  cout << setprecision(numeric_limits<Float>::digits10)
       << "node     " << b.option().get_node_name() << endl
       << "raw_poly " << b.option().get_raw_polynomial_name() << endl
       << "meas_K   " << meas_hat_K(0,0) << endl
    ;
}
int main(int argc, char**argv) {
  string approx = (argc > 1) ? argv[1]    : "P3";
  char   t      = (argc > 2) ? argv[2][0] : 't';
  bool   dump   = (argc > 3);
  reference_element hat_K;
  hat_K.set_name(t);
  if (approx[0] == 'D') {
    basis_raw b (approx);
    show_mass_raw (b, hat_K, dump);
  } else {
    basis b (approx);
    show_mass_fem (b, hat_K, dump);
  }
}
