#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"${TOP_SRCDIR}/fem/lib"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"
. "${TOP_SRCDIR}/config/float_compare.sh"

status=0

K=t
k=10
# Pk(t) cond(vdm):
#	equispaced	warburton           (lines)
#    \	monomial	bernstein dubiner  (column)
cond_valid_list="
	1.1139e+10	3411.7	  261.88
	4.2891e+09	1754.1	   82.204
"


for node in equispaced warburton; do
for poly in monomial bernstein dubiner; do
  cond_valid=`echo $cond_valid_list | gawk '{print $1}'`
  cond_valid_list=`echo $cond_valid_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
  command="./vdm_tst P${k}[$node,$poly] $K"
  echo "      $command"
  cond=`eval "$command 2>/dev/null| grep cond | gawk '{print \\$2}'"`
  if float_differ "$cond" "$cond_valid" 1e-4; then
    echo "      ** FAILED"; status=1
    echo "      -> ($cond - $cond_valid)/$cond_valid"
  fi
done; done

exit $status
