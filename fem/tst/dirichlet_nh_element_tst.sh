#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"$TOP_SRCDIR/fem/lib"}
DATADIR=$TOP_SRCDIR/fem/lib
NPROC_MAX=${NPROC_MAX-"3"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

#echo "      not yet (skipped)"
#exit 0


status=0

k_list="03      06       12	18"
# hat_K:
L="
e	7e-3	1e-4	3e-11	7e-9
	7e-3	1e-4	2e-11	1e-11
t	3e-1	4e-2	2e-7	2e-9
	3e-1	2e-3	1e-8	1e-11
q	7e-1	7e-3	3e-7	none
	7e-1	4e-3	4e-8	none
T	none	4e-3	none	none
	none	2e-3	none	none
P	6e-1	1e-2	none	none
	6e-1	3e-3	none	none
H	8e-1	2e-2	none	none
	8e-1	6e-3	none	none
"

# check for various raw_polynomial basis and one node pointset:


raw_poly="dubiner"
while test "$L" != ""; do
  K=`echo $L | gawk '{print $1}'`
  L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
  for node in equispaced warburton; do
    for k in $k_list; do
      err_linf_valid=`echo $L | gawk '{print $1}'`
      L=`echo $L | gawk '{for (i=2; i <= NF; i++) print $i}'`
      if test "$err_linf_valid" = none; then continue; fi
      run "./dirichlet_nh_element_tst P${k}[$node,$raw_poly] $K 0 $err_linf_valid 2>/dev/null >/dev/null"
      if test $? -ne 0; then status=1; fi
    done
  done
done

exit $status
