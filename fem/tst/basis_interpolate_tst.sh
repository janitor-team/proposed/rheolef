#!/bin/bash
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# -------------------------------------------------------------------------
TOP_SRCDIR=${TOP_SRCDIR-"../../../rheolef"}
SRCDIR=${SRCDIR-"${TOP_SRCDIR}/fem/lib"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"
. "${TOP_SRCDIR}/config/float_compare.sh"

status=0
k=5
poly="dubiner"
# Pk(K) err_l2:
#     	equispaced  warburton
err_l2_valid_list="
	0.000350678 0.000323061
	0.000411257 0.000392035
	0.000471243 0.000506902
	0.000311466 0.000239287
	0.000222153 0.000203472
	0.00119354  0.000808494
"

for K in e t T q P H; do
  for node in equispaced warburton; do
    err_l2_valid=`echo $err_l2_valid_list | gawk '{print $1}'`
    err_l2_valid_list=`echo $err_l2_valid_list | gawk '{for (i=2; i <= NF; i++) print $i}'`
    command="./basis_interpolate_tst P${k}[$node,$poly] $K"
    echo "      $command"
    err_l2=`eval "$command 2>/dev/null| grep err_l2 | gawk '{print \\$2}'"`
    #echo "          err_l2 : \"$err_l2\" ?= \"$err_l2_valid"\"
    if float_differ "$err_l2" "$err_l2_valid" 1e-3; then
      echo "      ** FAILED"; status=1
      echo "      -> err_l2 ($err_l2 - $err_l2_valid)/$err_l2_valid"
    fi
  done
done

exit $status
