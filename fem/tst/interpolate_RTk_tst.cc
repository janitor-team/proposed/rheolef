///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// check the RT1 basis interpolatation on triangle
//
// author: Pierre.Saramito@imag.fr
//
// date: 29 april 2019
//
#include "rheolef/basis.h"
#include "field_element.h"
using namespace rheolef;
using namespace std;
#include "interpolate_RTk_polynom.icc"

int main(int argc, char**argv) {
  char hat_K_name = (argc > 1) ?      argv[1][0] : 't';
  size_t idx      = (argc > 2) ? atoi(argv[2]) : 5;
  size_t nsub     = (argc > 3) ? atoi(argv[3]) : 10;
  Float tol       = (argc > 4) ? atof(argv[4]) : sqrt(std::numeric_limits<Float>::epsilon());
  bool do_gdat = true;
  basis b ("RT1");
  reference_element hat_K;
  hat_K.set_name(hat_K_name);
  size_t d = hat_K.dimension();
  space_element Vk (b, hat_K);
  field_element psi_k (Vk);
  psi_k.interpolate (psi(hat_K.name(),idx));
  cerr << "basis      " << b.name() << endl
       << "hat_K      " << hat_K.name() << endl
       << "basis_ndof " << b.ndof(hat_K) << endl
       << "psi index  " << idx << endl
       << "ndof       " << psi_k.ndof() << endl
      ;
  for (size_t idof = 0, ndof = psi_k.ndof(); idof < ndof; ++idof) {
    cerr << psi_k.dof(idof) << endl;
  }
  Float err_linf = 0;
  Float err_div_linf = 0;
  if (do_gdat) {
    cout << "# x y psi_k_x psi_k_y psi_x psi_y div_psi_k div_psi" << endl;
  }
  for (size_t j = 0; j <= nsub; j++) {
    for (size_t i1 = 0; i1 <= nsub; i1++) {
      size_t i = (hat_K.name() == 't') ? std::min(i1, nsub-j) : i1;
      point hat_x (Float(int(i))/Float(int(nsub)), Float(int(j))/Float(int(nsub)));
      point value = psi_k.template evaluate<point> (hat_x);
      tensor grad_value = psi_k.template grad_evaluate<tensor> (hat_x);
      Float div_value = tr(grad_value); 
      point exact = psi(hat_K.name(),idx) (hat_x);
      Float div_exact = psi(hat_K.name(),idx).div (hat_x);
      if (do_gdat) {
        cout << hat_x[0]  << " " << hat_x[1] << " " 
             << value[0]  << " " << value[1] << " "
             << exact[0]  << " " << exact[1] << " "
             << div_value << " " << div_exact << endl;
      }
      err_linf = max(err_linf, abs(value[0]-exact[0]));
      err_linf = max(err_linf, abs(value[1]-exact[1]));
      err_div_linf = max(err_div_linf, abs(div_value-div_exact));
    }
    if (do_gdat) cout << endl;
  }
  cerr << "err_linf      " << err_linf << endl
       << "err_div_linf  " << err_linf << endl;
  return (err_linf <= tol && err_div_linf <= tol) ? 0 : 1;
}
