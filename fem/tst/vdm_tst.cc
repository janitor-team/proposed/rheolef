///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute the Vandermonde condition number, via octave
//
// author: Pierre.Saramito@imag.fr
//
// date: 2 september 2017
//
#include "rheolef/basis.h"
#include "rheolef/eigen_util.h"
using namespace rheolef;
using namespace std;
using namespace Eigen;

int main(int argc, char**argv) {
  string approx = (argc > 1) ?      argv[1]    : "D3";
  char   t      = (argc > 2) ?      argv[2][0] : 't';
  bool   dump   = (argc > 3);
  basis b (approx);
  reference_element hat_K;
  hat_K.set_name(t);
  const Matrix<Float,Dynamic,Dynamic>& vdm     = b.vdm     (hat_K);
  const Matrix<Float,Dynamic,Dynamic>& inv_vdm = b.inv_vdm (hat_K);
  Eigen::SparseMatrix<Float,Eigen::RowMajor> sp_vdm;
  eigen_dense2sparse (vdm, sp_vdm);

  cout << setprecision(numeric_limits<Float>::digits10)
       << "approx  " << b.name() << endl
       << "element " << hat_K.name() << endl
       << "node    " << b.option().get_node_name() << endl
       << "size    " << vdm.rows() << endl
       << "cond    " << cond(vdm) << endl
       << "det     " << vdm.determinant() << endl
       << "nnz     " << sp_vdm.nonZeros()<< endl
       << "fill    " << 1.*sp_vdm.nonZeros()/sqr(vdm.rows())<< endl
    ;
  if (!dump) return 0;
  ofstream out ("vdm.mm");
  put_matrix_market (out, sp_vdm);
}
