///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// compute the lebesgue constant
//
// author: Pierre.Saramito@imag.fr
//
// date: 4 september 2017
//
#include "rheolef/basis.h"
using namespace rheolef;
using namespace std;

template<class T>
T
lebesgue_constant (
    const basis_basic<T>&		     b,
    reference_element                        hat_K,
    size_t                                   n_approx)
{
  typedef reference_element::size_type  size_type;
  Eigen::Matrix<T,Eigen::Dynamic,1> phi;
  T Lambda = 0;
  switch (hat_K.variant()) {
    case reference_element::p: {
      return Lambda;
    } 
    case reference_element::e: {
      for (size_type i = 0; i <= n_approx; i++) { 
        point_basic<T> hat_x (T(int(i))/T(int(n_approx)));
        b.evaluate (hat_K, hat_x, phi);
        Lambda = max(Lambda, phi.template lpNorm<1>());
      }
      return Lambda;
    }
    case reference_element::t: {
      for (size_type j = 0; j <= n_approx; j++) { 
        for (size_type i = 0; i+j <= n_approx; i++) { 
          point_basic<T> hat_x (T(int(i))/T(int(n_approx)),
                                T(int(j))/T(int(n_approx)));
          b.evaluate (hat_K, hat_x, phi);
          Lambda = max(Lambda, phi.template lpNorm<1>());
        }
      }
      return Lambda;
    } 
    case reference_element::T: {
      for (size_type k = 0; k <= n_approx; k++) {
        for (size_type j = 0; j+k <= n_approx; j++) {
          for (size_type i = 0; i+j+k <= n_approx; i++) { 
            point_basic<T> hat_x (T(int(i))/T(int(n_approx)),
                                  T(int(j))/T(int(n_approx)),
                                  T(int(k))/T(int(n_approx)));
            b.evaluate (hat_K, hat_x, phi);
            Lambda = max(Lambda, phi.template lpNorm<1>());
          }
        }
      }
      return Lambda;
    }
    case reference_element::q: {
      for (size_type j = 0; j <= n_approx; j++) { 
        for (size_type i = 0; i <= n_approx; i++) { 
          point_basic<T> hat_x (2*T(int(i))/T(int(n_approx))-1,
                                2*T(int(j))/T(int(n_approx))-1);
          b.evaluate (hat_K, hat_x, phi);
          Lambda = max(Lambda, phi.template lpNorm<1>());
        }
      }
      return Lambda;
    } 
    case reference_element::P: {
      for (size_type k = 0; k <= n_approx; k++) { 
        for (size_type j = 0; j <= n_approx; j++) { 
          for (size_type i = 0; i+j <= n_approx; i++) { 
            point_basic<T> hat_x (T(int(i))/T(int(n_approx)),
                                  T(int(j))/T(int(n_approx)),
                                2*T(int(k))/T(int(n_approx))-1);
            b.evaluate (hat_K, hat_x, phi);
            Lambda = max(Lambda, phi.template lpNorm<1>());
          }
        }
      }
      return Lambda;
    } 
    case reference_element::H: {
      for (size_type k = 0; k <= n_approx; k++) { 
        for (size_type j = 0; j <= n_approx; j++) { 
          for (size_type i = 0; i <= n_approx; i++) { 
            point_basic<T> hat_x (2*T(int(i))/T(int(n_approx))-1,
                                  2*T(int(j))/T(int(n_approx))-1,
                                  2*T(int(k))/T(int(n_approx))-1);
            b.evaluate (hat_K, hat_x, phi);
            Lambda = max(Lambda, phi.template lpNorm<1>());
          }
        }
      }
      return Lambda;
    } 
    default: error_macro ("unexpected element type `"<<hat_K.name()<<"'");
      return 0;
  }
}
int main(int argc, char**argv) {
  string approx = (argc > 1) ?      argv[1]    : "P3";
  char   t      = (argc > 2) ?      argv[2][0] : 't';
  size_t nsub   = (argc > 3) ? atoi(argv[3]) : 200;
  basis b (approx);
  reference_element hat_K;
  hat_K.set_name(t);
  Float Lambda = lebesgue_constant<Float> (b, hat_K, nsub);
  cout << setprecision(numeric_limits<Float>::digits10)
       << "approx   " << b.name() << endl
       << "element  " << hat_K.name() << endl
       << "node     " << b.option().get_node_name() << endl
       << "raw_poly " << b.option().get_raw_polynomial_name() << endl
       << "nsub     " << nsub << endl
       << "lebesgue " << Lambda << endl
    ;
}
