///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/tensor3.h"
using namespace rheolef;
using namespace std; 
int main () {
    Float tol = sqrt(std::numeric_limits<Float>::epsilon());
    // check for Float*tensor3 -> tensor3 : it was a bug
    tensor3 a, c;
    for (size_t i = 0; i < 3; ++i)
    for (size_t j = 0; j < 3; ++j)
    for (size_t k = 0; k < 3; ++k) {
      a (i,j,k) = i+2*j+3*k;
      c (i,j,k) = 5.*a(i,j,k);
    }
    cout << "a=" << a << endl;
    tensor3 b = Float(5)*a;
    cout << "3*a=" << b << endl;
    tensor3 e = b - c;
    cout << "e=" << e << endl;
    Float err = 0;
    for (size_t i = 0; i < 3; ++i)
    for (size_t j = 0; j < 3; ++j)
    for (size_t k = 0; k < 3; ++k) {
      err += fabs(e(i,j,k));
    }
    return (err < tol) ? 0 : 1;
}
