///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/tensor3.h"
namespace rheolef {
using namespace std;

template<class T>
tensor3_basic<T>&
tensor3_basic<T>::operator= (const T& val)
{ 
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
      _x[i][j][k] = val;
    return *this;
}
template<class T>
tensor3_basic<T>&
tensor3_basic<T>::operator= (const tensor3_basic<T>& a)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
      _x[i][j][k] = a._x[i][j][k];
    return *this;
}
// algebra
template <class T>
tensor_basic<T>
tensor3_basic<T>::operator* (const point_basic<T>& v) const
{
    tensor_basic<T> b;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type z = 0; z < 3; z++)
      b(i,j) += _x[i][j][z]*v[z];
    return b;
}
template <class T>
tensor3_basic<T>
tensor3_basic<T>::operator* (const tensor_basic<T>& t) const
{
    tensor3_basic<T> b;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
    for (size_type z = 0; z < 3; z++)
      b(i,j,k) += _x[i][j][z]*t(z,k);
    return b;
}
template <class T>
tensor3_basic<T>
tensor3_basic<T>::operator+ (const tensor3_basic<T>& b) const
{
    tensor3_basic<T> c;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
      c(i,j,k) = _x[i][j][k] + b(i,j,k);
    return c;
}
template <class T>
tensor3_basic<T>
tensor3_basic<T>::operator- (const tensor3_basic<T>& b) const
{
    tensor3_basic<T> c;
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
      c(i,j,k) = _x[i][j][k] - b(i,j,k);
    return c;
}
template<class T>
T
dddot (const tensor3_basic<T>& a, const tensor3_basic<T> & b)
{
    T r = 0;
    typedef typename tensor_basic<T>::size_type size_type;
    for (size_type i = 0; i < 3; i++) 
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
        r += a(i,j,k) * b(i,j,k);
    return r;
}
template<class T>
tensor3_basic<T>&
tensor3_basic<T>::operator+= (const tensor3_basic<T>& b)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
        _x[i][j][k] += b._x[i][j][k];
    return *this;
}
template<class T>
tensor3_basic<T>&
tensor3_basic<T>::operator-= (const tensor3_basic<T>& b)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
        _x[i][j][k] -= b._x[i][j][k];
    return *this;
}
template <class T>
tensor3_basic<T>&
tensor3_basic<T>::operator*= (const T& fact)
{
    for (size_type i = 0; i < 3; i++)
    for (size_type j = 0; j < 3; j++)
    for (size_type k = 0; k < 3; k++)
      _x[i][j][k] *= fact;
    return *this;
}
// ----------------------------------------------------------------------------
// io
// ----------------------------------------------------------------------------
// output
template<class T>
ostream& 
tensor3_basic<T>::put (ostream& out, size_type d) const
{
    switch (d) {
    case 0 : return out;
    case 1 : return out << _x[0][0];
    case 2 : return out << "[[" << _x[0][0][0] << ", " << _x[0][0][1] << ";"   << endl 
	                << "  " << _x[0][1][0] << ", " << _x[0][1][1] << "],"  << endl
		        << " [" << _x[1][0][0] << ", " << _x[1][0][1] << ";"   << endl 
	                << "  " << _x[1][1][0] << ", " << _x[1][1][1] << "]]";

    default: return out << "[[" << _x[0][0][0] << ", " << _x[0][0][1] << ", " << _x[0][0][2] << ";"   << endl 
                        << "  " << _x[0][1][0] << ", " << _x[0][1][1] << ", " << _x[0][1][2] << ";"   << endl 
                        << "  " << _x[0][2][0] << ", " << _x[0][2][1] << ", " << _x[0][2][2] << "],"  << endl 
                        << " [" << _x[1][0][0] << ", " << _x[1][0][1] << ", " << _x[1][0][2] << ";"   << endl 
                        << "  " << _x[1][1][0] << ", " << _x[1][1][1] << ", " << _x[1][1][2] << ";"   << endl 
                        << "  " << _x[1][2][0] << ", " << _x[1][2][1] << ", " << _x[1][2][2] << "],"  << endl 
                        << " [" << _x[2][0][0] << ", " << _x[2][0][1] << ", " << _x[2][0][2] << ";"   << endl 
                        << "  " << _x[2][1][0] << ", " << _x[2][1][1] << ", " << _x[2][1][2] << ";"   << endl 
                        << "  " << _x[2][2][0] << ", " << _x[2][2][1] << ", " << _x[2][2][2] << "]]";
    }
}
template<class T>
istream&
tensor3_basic<T>::get (istream& in)
{
    // TODO: as output, aka octave-like
    fatal_macro ("tensor3::get: not yet");
    return in;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template class tensor3_basic<T>;						\
template T dddot (const tensor3_basic<T>&, const tensor3_basic<T>&);		\

_RHEOLEF_instanciation(Float)

}// namespace rheolef
