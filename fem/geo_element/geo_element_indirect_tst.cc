///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// draft-1 version of the geo_element class with arbitrarily order
// solution with std::vector inside each geo_element
//
// avantage : 
//  - souplesse : gere la memoire en tas dans geo (efficace)
//    mais aussi peut creer des elements temporaires, si besoin
//  - inconvenient : tableau de geo_element, qui contiennent des tableaux
//    alloues dynamiquement => la memoire n'est pas contigue globalement
//
#include "rheolef/geo_element_indirect.h"
using namespace rheolef;
using namespace std;

int main(int argc, char**argv)
{
  clog << "sizeof(size_t) = "
       << sizeof(size_t) << endl;
  clog << "sizeof(geo_element_indirect) = "
       << sizeof(geo_element_indirect) << endl;
  geo_element_indirect x;
  clog << "initial          : "; x.dump(clog); clog << endl;
  clog << "set shift 0      : "; x.set_shift(0); x.dump(clog); clog << endl;
  cout << "shift = " << x.shift() << endl << flush;
  clog << "set shift 7      : "; x.set_shift(7); x.dump(clog); clog << endl;
  cout << "shift = " << x.shift() << endl << flush;
  clog << "set orient -1    : "; x.set_orientation(-1); x.dump(clog); clog << endl;
  cout << "orient = " << x.orientation() << endl << flush;
  clog << "set orient  1    : "; x.set_orientation(1); x.dump(clog); clog << endl;
  cout << "orient = " << x.orientation() << endl << flush;
  clog << "set index 0      : "; x.set_index(0); x.dump(clog); clog << endl;
  cout << "index = " << x.index() << endl << flush;
  clog << "set index 8      : "; x.set_index(8); x.dump(clog); clog << endl;
  cout << "index = " << x.index() << endl << flush;
}
