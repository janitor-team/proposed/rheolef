///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/quadrature.h"
#include "rheolef/reference_element_face_transformation.h"
#include "rheolef/rheostream.h"
#include <cctype> // for std::isdigit
namespace rheolef {
using namespace std;

// ----------------------------------------------------------------------------
// quadrature options
// ----------------------------------------------------------------------------
static const char* static_family_name[quadrature_option::max_family+1] = {
	"gauss"           ,
	"gauss_lobatto"   ,
	"gauss_radau"	  ,
	"middle_edge"     ,
	"superconvergent" ,
	"equispaced"      ,
	"undefined"
};
static 
quadrature_option::family_type
family_name2type (string name)
{
  for (size_t i =  0; i < quadrature_option::max_family; ++i) {
    if (static_family_name[i] == name) {
      return (quadrature_option::family_type) (i);
    }
  }
  error_macro ("unexpected quadrature family name `" << name << "'");
  return quadrature_option::max_family;
}
void
quadrature_option::set_family (string name)
{
  set_family (family_name2type (name));
}
string
quadrature_option::get_family_name() const
{
  check_macro (_family >= 0 && _family <= max_family,
	"unexpected quadrature family number = " << _family);
  return static_family_name[_family];
}
string
quadrature_option::name() const
{
  if (get_family() == default_family && get_order() == default_order) {
    return "";
  } else {
    return get_family_name() + "(" + std::to_string(get_order()) + ")";
  }
#ifdef TO_CLEAN
  return get_family_name() + "(" + std::to_string(long(get_order())) + ")";
#endif // TO_CLEAN
}
void
quadrature_option::reset (const std::string& name)
{
  if (name == "") {
    // set options to default:
    set_family (default_family);
    set_order  (default_order);
    return;
  }
  size_type i = name.find ('(');
  string family = name.substr (0, i);
  size_type i1 = i+1, l1 = name.size()-i1-1;
  check_macro (name.size() > i1+1,
	"invalid quadrature name \""<<name<<"\"; expect e.g. \"gauss(3)\"");
  string order_str = name.substr (i1, l1);
  for (size_type i = 0; i < order_str.size(); ++i) {
    // accepts also e.g. "gauss(-1)" as default initialization
    check_macro (std::isdigit(order_str[i]) || order_str[i] == '-',
	"invalid quadrature name \""<<name<<"\"; expect e.g. \"gauss(3)\"");
  }
  size_type order = atoi(order_str.c_str());
  set_family (family);
  set_order  (order);
}
// ----------------------------------------------------------------------------
// quadrature on a specific reference element
// ----------------------------------------------------------------------------
template<class T>
void
quadrature_on_geo<T>::initialize (reference_element hat_K,  quadrature_option opt)
{
    base::resize (0);
    switch (hat_K.variant()) {
      case reference_element::p:  init_point       (opt); break;
      case reference_element::e:  init_edge        (opt); break;
      case reference_element::t:  init_triangle    (opt); break;
      case reference_element::q:  init_square      (opt); break;
      case reference_element::T:  init_tetrahedron (opt); break;
      case reference_element::P:  init_prism       (opt); break;
      case reference_element::H:  init_hexahedron  (opt); break;
      default:
	fatal_macro ("quadrature formula on element type `"
	  << hat_K.name() << "' are not yet implemented.");
    }
}
template<class T>
void
quadrature_on_geo<T>::get_nodes (Eigen::Matrix<point_basic<T>, Eigen::Dynamic, 1>& node) const
{
  size_type nq = base::size();
  node.resize (nq);
  for (size_type q = 0; q < nq; ++q) {
    node[q] = base::operator[](q).x;
  }
}
// ----------------------------------------------------------------------------
// quadrature on the full set of reference elements
// ----------------------------------------------------------------------------
template<class T>
quadrature_rep<T>::quadrature_rep (quadrature_option opt)
 : _options(opt),
   _quad(),
   _initialized           (reference_element::max_variant, false)
{
}
template<class T>
quadrature_rep<T>::quadrature_rep (const std::string& name)
 : _options(name),
   _quad(),
   _initialized           (reference_element::max_variant, false)
{
}
template<class T>
quadrature_rep<T>::quadrature_rep (const quadrature_rep<T>& q)
 : _options               (q._options),
   _quad                  (q._quad),
   _initialized           (q._initialized)
{
}
template<class T>
const quadrature_rep<T>&
quadrature_rep<T>::operator= (const quadrature_rep<T>& q) {
       _options                = q._options;
       _quad                   = q._quad;
       _initialized            = q._initialized;
       return *this;
}
template<class T>
string
quadrature_rep<T>::name() const
{
  return _options.name();
}
template<class T>
void 
quadrature_rep<T>::_initialize (reference_element hat_K) const
{
    reference_element::variant_type K_type = hat_K.variant();
    _quad[K_type].initialize (hat_K, _options);
    _initialized [K_type] = true;
}
template<class T>
void
quadrature_rep<T>::get_nodes (reference_element hat_K, Eigen::Matrix<point_basic<T>, Eigen::Dynamic, 1>& node) const
{
  if (!_initialized [hat_K.variant()]) _initialize (hat_K);
  _quad [hat_K.variant()].get_nodes (node);
}
template<class T>
typename quadrature_rep<T>::const_iterator 
quadrature_rep<T>::begin (reference_element hat_K) const
{
    reference_element::variant_type K_type = hat_K.variant();
    if (!_initialized [K_type]) _initialize (hat_K);
    return _quad[K_type].begin();
}
template<class T>
typename quadrature_rep<T>::const_iterator 
quadrature_rep<T>::end (reference_element hat_K) const
{
    reference_element::variant_type K_type = hat_K.variant();
    if (!_initialized [K_type]) _initialize (hat_K);
    return _quad[K_type].end();
}
template<class T>
typename quadrature_rep<T>::size_type 
quadrature_rep<T>::size (reference_element hat_K) const
{
    reference_element::variant_type K_type = hat_K.variant();
    if (!_initialized [K_type]) _initialize (hat_K);
    return _quad[K_type].size();
}
template<class T>
const weighted_point<T>&
quadrature_rep<T>::operator() (reference_element hat_K, size_type q) const
{
    reference_element::variant_type K_type = hat_K.variant();
    if (!_initialized [K_type]) _initialize (hat_K);
    return _quad[K_type][q];
}
template<class T>
ostream&
operator<< (ostream& out, const quadrature_on_geo<T>& x)
{
  out << setprecision (numeric_limits<T>::digits10)
      << x.size() << endl;
  for (size_t r = 0; r < x.size(); r++)
    out << x[r].x << "\t" << x[r].w << endl;
  return out;
}
template<class T>
ostream&
operator<< (ostream& out, const quadrature_rep<T>& x)
{
  out << "quadrature" << endl
      << x._options.get_family_name() << " " << x._options.get_order()  << endl;
  for (size_t i = 0; i != size_t(reference_element::max_variant); i++) {
    reference_element::variant_type K_type = reference_element::variant_type(i);
    if (! x._initialized [K_type]) continue;
    reference_element hat_K (K_type);
    out << "reference_element " << hat_K.name() << endl
        << x._quad[K_type];
  }
  out << "end_quadrature" << endl;
  return out;
}
// ----------------------------------------------------------------------------
// quadrature : pointer interface
// ----------------------------------------------------------------------------
template <class T>
quadrature_rep<T>::~quadrature_rep()
{
  if (_options.name() != "") {
    persistent_table<quadrature<T>>::unload (name());
  }
}
template<class T>
void
quadrature<T>::reset (const std::string& name)
{
  if (name == "") {
    base::operator= (new_macro(quadrature_rep<T>()));
  } else {
    base::operator= (persistent_table<quadrature<T>>::load (name));
  }
}
template<class T>
const quadrature_option&
quadrature<T>::get_options() const
{
  return base::data().get_options();
}
template<class T>
quadrature<T>::quadrature (quadrature_option opt)
  : base(),
    persistent_table<quadrature<T>>()
{
  reset (opt.name());
}
template<class T>
quadrature<T>::quadrature (const std::string& name)
  : base(),
    persistent_table<quadrature<T>>()
{
  reset (name);
}
template<class T>
void
quadrature<T>::set_order  (size_type order)
{
  quadrature_option qopt = get_options();
  qopt.set_order (order);
  reset (qopt.name());
}
template<class T>
void
quadrature<T>::set_family (family_type ft)
{
  quadrature_option qopt = get_options();
  qopt.set_family (ft);
  reset (qopt.name());
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)               \
template class quadrature_on_geo<Float>;	\
template class quadrature_rep<Float>;		\
template class quadrature<T>;			\
template ostream& operator<< (ostream&, const quadrature_on_geo<T>&); 	\
template ostream& operator<< (ostream&, const quadrature_rep<T>&);

_RHEOLEF_instanciation(Float)

} // namespace rheolef
