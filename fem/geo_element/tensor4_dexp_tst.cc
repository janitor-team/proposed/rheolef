///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
#include "rheolef/tensor4.h"
using namespace rheolef;
using namespace std;

Float check(string name, const tensor& a, const tensor4& dexp_a_exact, size_t dim) 
{
  cout << name << "="<<endl; a.put(cout,dim); cout <<endl;
  cout << "exp_"<<name<<"_exact="<<endl; dexp_a_exact.put(cout,dim); cout <<endl;
  tensor4 dexp_a = dexp(a,dim);
  cout << "exp_"<<name<<"="<<endl; dexp_a.put(cout,dim); cout<<endl;
  Float err = norm(dexp_a_exact - dexp_a);
  cerr << "err="<<err<<endl<<endl;
  return err;
}
int main(int argc, char**argv) {
  Float tol = 1e-7, err = 0, e = exp(Float(1));

  // 1rst test (see tensor4_dexp_tst.mac)
  tensor a = {{1,1},{1,1}};
  tensor4 dexp_a = {{
	         // cc_00_ij
                 {{ 3.694528049465325, 3.194528049465325 },
                  { 3.194528049465325, 0.5               }},
	         // cc_01_ij
                 {{ 1.597264024732663, 4.194528049465325 },
                  { 4.194528049465325, 1.597264024732663 }}},
	         // cc_01_ij
                {{{ 1.597264024732663, 4.194528049465325 },
                  { 4.194528049465325, 1.597264024732663 }},
	         // cc_11_ij
                 {{ 0.5              , 3.194528049465325 },
                  { 3.194528049465325, 3.694528049465325 }}}};
  err += check ("a", a, dexp_a, 2);

  // 2nd test
  tensor b = {{1,1},{1,2}};
  tensor4 dexp_b = {{
                 // cc_00_ij
                 {{ 4.004622655150845,  4.630914073915026  },
                  { 4.630914073915026,  0.8445828090966658 }},
                 // cc_01_ij
                 {{ 2.315457036957513,  7.164662501205024  },
                  { 7.164662501205024,  3.160039846054179  }}},
                 // cc_10_ij
                {{{ 2.315457036957513,  7.164662501205024  },
                  { 7.164662501205024,  3.160039846054179  }},
                 // cc_11_ij
                 {{ 0.8445828090966656, 6.320079692108359 },
                  { 6.320079692108359,  9.480119538162537 }}}};
  err += check ("b", b, dexp_b, 2);

  // 3rd test (degenerate)
  tensor c = {{1,0},{0,1}};
  tensor4 dexp_c = {{
                 // cc_00_ij
                 {{ e,  0},
                  { 0,  0}},
                 // cc_01_ij
                 {{ 0,  e},
                  { e,  0}}},
                 // cc_10_ij
                {{{ 0,  e},
                  { e,  0}},
                 // cc_11_ij
                 {{ 0, 0},
                  { 0, e}}}};
  err += check ("c", c, dexp_c, 2);

  return (err < tol) ? 0 : 1;
}
