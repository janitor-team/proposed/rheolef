# ifndef _RHEO_BASIC_POINT_H
# define _RHEO_BASIC_POINT_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// author: Pierre.Saramito@imag.fr

namespace rheolef {
/**
@classfile point d-dimensional physical point or vector

Description
===========
The `point` defines a vertex or vector in the physical
d-dimensional space, d=1,2,3.
It is represented as an array of coordinates.
The coordinate index starts at zero and finishes at `d-1`,
e.g. `x[0]`, `x[1]` and `x[2]`.

The default constructor set all components to zero:

        point x;

and this default could be overridden:

        point x (1, 2, 3.14);

or alternatively:

        point x = {1, 2, 3.14};

The standard linear algebra for vectors is supported by the
`point` class.

Implementation
==============
@showfromfile
The `point` class is simply an alias to the `point_basic` class

@snippet point.h verbatim_point

The `point_basic` class is a template class
with the floating type as parameter:

@snippet point.h verbatim_point_basic
@snippet point.h verbatim_point_basic_cont
@par

These linear and nonlinear functions are
completed by some usual functions:

@snippet point.h verbatim_point_basic_cont2
*/
} // namespace rheolef

#include "rheolef/compiler_mpi.h"
#include <sstream>

namespace rheolef { 

// [verbatim_point_basic]
template <class T>
class point_basic {
public:

// typedefs:

  typedef size_t size_type;
  typedef T      element_type;
  typedef T      scalar_type;
  typedef T      float_type;

// allocators:

  explicit point_basic();
  explicit point_basic (const T& x0, const T& x1 = 0, const T& x2 = 0);
	
  template <class T1>
  point_basic<T>(const point_basic<T1>& p);

  template <class T1>
  point_basic<T>& operator= (const point_basic<T1>& p);

  point_basic (const std::initializer_list<T>& il);

// accessors:

  T& operator[](int i_coord)	          { return _x[i_coord%3]; }
  T& operator()(int i_coord)	          { return _x[i_coord%3]; }
  const T&  operator[](int i_coord) const { return _x[i_coord%3]; }
  const T&  operator()(int i_coord) const { return _x[i_coord%3]; }

// algebra:

  bool operator== (const point_basic<T>& v) const;
  bool operator!= (const point_basic<T>& v) const;
  point_basic<T> operator+ (const point_basic<T>& v) const;
  point_basic<T> operator- (const point_basic<T>& v) const;
  point_basic<T> operator- () const;
  point_basic<T>& operator+= (const point_basic<T>& v);
  point_basic<T>& operator-= (const point_basic<T>& v);
  point_basic<T>& operator*= (const T& a);
  point_basic<T>& operator/= (const T& a);

  template <class U>
  typename
  std::enable_if<
    details::is_rheolef_arithmetic<U>::value
    ,point_basic<T>
  >::type
  operator* (const U& a) const;
  point_basic<T> operator/ (const T& a) const;
  point_basic<T> operator/ (point_basic<T> v) const;

// i/o:

  std::istream& get (std::istream& s, int d = 3);
  std::ostream& put (std::ostream& s, int d = 3) const;
// [verbatim_point_basic]

// interface for CGAL library inter-operability:

  const T& x() const { return _x[0]; }
  const T& y() const { return _x[1]; }
  const T& z() const { return _x[2]; }
  T& x(){ return _x[0]; }
  T& y(){ return _x[1]; }
  T& z(){ return _x[2]; }

// data:
// protected:
  T _x[3];
// internal:
  static T _my_abs(const T& x) { return (x > T(0)) ? x : -x; }
// [verbatim_point_basic_cont]
};
// [verbatim_point_basic_cont]

// [verbatim_point]
typedef point_basic<Float> point;
// [verbatim_point]

// [verbatim_point_basic_cont2]
template<class T>
std::istream& operator >> (std::istream& s, point_basic<T>& p);

template<class T>
std::ostream& operator << (std::ostream& s, const point_basic<T>& p);

template <class T, class U>
typename
std::enable_if<
  details::is_rheolef_arithmetic<U>::value
 ,point_basic<T>
>::type
operator* (const U& a, const point_basic<T>& u);

template<class T>
point_basic<T>
vect (const point_basic<T>& v, const point_basic<T>& w);

// metrics:
template<class T>
T dot (const point_basic<T>& x, const point_basic<T>& y);

template<class T>
T norm2 (const point_basic<T>& x);

template<class T>
T norm (const point_basic<T>& x);

template<class T>
T dist2 (const point_basic<T>& x,  const point_basic<T>& y);

template<class T>
T dist (const point_basic<T>& x,  const point_basic<T>& y);

template<class T>
T dist_infty (const point_basic<T>& x,  const point_basic<T>& y);

template <class T>
T vect2d (const point_basic<T>& v, const point_basic<T>& w);

template <class T>
T mixt (const point_basic<T>& u, const point_basic<T>& v, const point_basic<T>& w);

// robust(exact) floating point predicates: return the sign of the value as (0, > 0, < 0)
// formally: orient2d(a,b,x) = vect2d(a-x,b-x)
template <class T>
int sign_orient2d (
  const point_basic<T>& a,
  const point_basic<T>& b,
  const point_basic<T>& c);

template <class T>
int sign_orient3d (
  const point_basic<T>& a,
  const point_basic<T>& b,
  const point_basic<T>& c,
  const point_basic<T>& d);

// compute also the value:
template <class T>
T orient2d(
  const point_basic<T>& a,
  const point_basic<T>& b,
  const point_basic<T>& c);

// formally: orient3d(a,b,c,x) = mixt3d(a-x,b-x,c-x)
template <class T>
T orient3d(
  const point_basic<T>& a,
  const point_basic<T>& b,
  const point_basic<T>& c,
  const point_basic<T>& d);

template <class T>
std::string ptos (const point_basic<T>& x, int d = 3);

// ccomparators: lexicographic order
template<class T, size_t d>
bool lexicographically_less (const point_basic<T>& a, const point_basic<T>& b);
// [verbatim_point_basic_cont2]
// -------------------------------------------------------------------------------------
// inline'd
// -------------------------------------------------------------------------------------
template <class T, class U>
inline
typename
std::enable_if<
  details::is_rheolef_arithmetic<U>::value
 ,point_basic<T>
>::type
operator* (const U& a, const point_basic<T>& u)
{
  return point_basic<T> (a*u[0], a*u[1], a*u[2]);
}
template<class T>
inline
point_basic<T>
vect (const point_basic<T>& v, const point_basic<T>& w)
{
  return point_basic<T> (
	v[1]*w[2]-v[2]*w[1],
	v[2]*w[0]-v[0]*w[2],
	v[0]*w[1]-v[1]*w[0]);
}
// metrics:
template<class T>
inline
T dot (const point_basic<T>& x, const point_basic<T>& y)
{
  return x[0]*y[0]+x[1]*y[1]+x[2]*y[2]; 
}
template<class T>
inline
T norm2 (const point_basic<T>& x)
{
  return dot(x,x);
}
template<class T>
inline
T norm (const point_basic<T>& x)
{
  return sqrt(norm2(x));
}
template<class T>
inline
T dist2 (const point_basic<T>& x,  const point_basic<T>& y)
{
  return norm2(x-y);
}
template<class T>
inline
T dist (const point_basic<T>& x,  const point_basic<T>& y)
{
  return norm(x-y);
}
template<class T>
inline
T dist_infty (const point_basic<T>& x,  const point_basic<T>& y)
{
  return max(point_basic<T>::_my_abs(x[0]-y[0]),
         max(point_basic<T>::_my_abs(x[1]-y[1]),
             point_basic<T>::_my_abs(x[2]-y[2])));
}
// ccomparators: lexicographic order
template<class T, size_t d>
inline
bool
lexicographically_less (const point_basic<T>& a, const point_basic<T>& b)
{
  for (typename point_basic<T>::size_type i = 0; i < d; i++) {
    if (a[i] < b[i]) return true;
    if (a[i] > b[i]) return false;
  }
  return false; // equality
}
/// @brief helper for point_basic<T> & tensor_basic<T>: get basic T type
template<class T> struct scalar_traits                  { typedef T type; };
template<class T> struct scalar_traits<point_basic<T> > { typedef T type; };
template<class T> struct  float_traits<point_basic<T> > { typedef typename float_traits<T>::type type; };

template<class T>
point_basic<T>::point_basic() {
  _x[0] = T();
  _x[1] = T();
  _x[2] = T();
} 
template<class T>
point_basic<T>::point_basic (
  	const T& x0, 
	const T& x1, 
	const T& x2)
{
  _x[0] = x0;
  _x[1] = x1;
  _x[2] = x2;
}
template<class T>
template<class T1>
point_basic<T>::point_basic (const point_basic<T1>& p)
{
  _x[0] = p._x[0];
  _x[1] = p._x[1];
  _x[2] = p._x[2];
}
template<class T>
template <class T1>
point_basic<T>&
point_basic<T>::operator= (const point_basic<T1>& p)
{
  _x[0] = p._x[0];
  _x[1] = p._x[1];
  _x[2] = p._x[2];
  return *this;
}
template<class T>
point_basic<T>::point_basic (const std::initializer_list<T>& il) : _x() {
    typedef typename std::initializer_list<T>::const_iterator const_iterator;
    check_macro (il.size() <= 3, "unexpected initializer list size=" << il.size() << " > 3");
    size_type i = 0;
    for (const_iterator iter = il.begin(); iter != il.end(); ++iter, ++i) {
      _x[i] = *iter;
    }
    for (i = il.size(); i < 3; ++i) {
      _x[i] = T();
    }
}
// input/output
template<class T>
std::istream&
point_basic<T>::get (std::istream& s, int d)
{
	    switch (d) {
	    case 0 : _x[0] = _x[1] = _x[2] = T(0); return s;
	    case 1 : _x[1] = _x[2] = T(0); return s >> _x[0];
	    case 2 : _x[2] = T(0); return s >> _x[0] >> _x[1];
	    default: return s >> _x[0] >> _x[1] >> _x[2];
	    }
}
template<class T>
inline
std::ostream&
point_basic<T>::put (std::ostream& s, int d) const
{
    switch (d) {
    case 0 : return s;
    case 1 : return s << _x[0];
    case 2 : return s << _x[0] << " " << _x[1];
    default: return s << _x[0] << " " << _x[1] << " " << _x[2];
    }
}
template<class T>
inline
std::istream& 
operator >> (std::istream& s, point_basic<T>& p)
{
    return s >> p[0] >> p[1] >> p[2];
}
template<class T>
inline
std::ostream& 
operator << (std::ostream& s, const point_basic<T>& p)
{
    return s << p[0] << " " << p[1] << " " << p[2];
}
template<class T>
std::string
ptos (const point_basic<T>& x, int d)
{
  std::ostringstream ostrstr;
  x.put (ostrstr, d);
  return ostrstr.str();
}
// ----------------------------------------------------------
// point extra: inlined
// ----------------------------------------------------------
#define def_point_function2(f,F)	\
template<class T>			\
inline					\
point_basic<T>				\
f (const point_basic<T>& x)		\
{					\
  point_basic<T> y;			\
  for (size_t i = 0; i < 3; i++)	\
    y[i] = F(x[i]);			\
  return y;				\
}

#define def_point_function(f)	def_point_function2(f,f)

def_point_function(sqr)
def_point_function(sqrt)
def_point_function(log)
def_point_function(log10)
def_point_function(exp)
#undef def_point_function2
#undef def_point_function

template <class T>
bool
point_basic<T>::operator== (const point_basic<T>& v) const
{
  return _x[0] == v[0] && _x[1] == v[1] && _x[2] == v[2];
}
template <class T>
bool
point_basic<T>::operator!= (const point_basic<T>& v) const
{
  return !operator==(v);
}
template <class T>
point_basic<T>&
point_basic<T>::operator+= (const point_basic<T>& v)
{
  _x[0] += v[0];
  _x[1] += v[1];
  _x[2] += v[2];
  return *this;
} 
template <class T>
point_basic<T>&
point_basic<T>::operator-= (const point_basic<T>& v)
{
  _x[0] -= v[0];
  _x[1] -= v[1]; 
  _x[2] -= v[2];
  return *this;
}
template <class T>
point_basic<T>&
point_basic<T>::operator*= (const T& a)
{
  _x[0] *= a; 
  _x[1] *= a; 
  _x[2] *= a;
  return *this;
} 
template <class T>
point_basic<T>&
point_basic<T>::operator/= (const T& a)
{
  _x[0] /= a; 
  _x[1] /= a; 
  _x[2] /= a;
  return *this;
}
template <class T>
point_basic<T>
point_basic<T>::operator+ (const point_basic<T>& v) const
{
  return point_basic<T> (_x[0]+v[0],
                         _x[1]+v[1],
                         _x[2]+v[2]);
}
template <class T>
point_basic<T>
point_basic<T>::operator- () const
{
  return point_basic<T> (-_x[0],
                         -_x[1],
                         -_x[2]);
}
template <class T>
point_basic<T>
point_basic<T>::operator- (const point_basic<T>& v) const
{
  return point_basic<T> (_x[0]-v[0],
                         _x[1]-v[1],
                         _x[2]-v[2]);
}
template<class T1, class T2>
inline
point_basic<T1>
operator/ (const T2& a, const point_basic<T1>& x)
{
  point_basic<T1> y;
  for (size_t i = 0; i < 3; i++)
    if (x[i] != 0) y[i] = a/x[i];
  return y;
}
template <class T>
template <class U>
typename
std::enable_if<
  details::is_rheolef_arithmetic<U>::value
 ,point_basic<T>
>::type
point_basic<T>::operator* (const U& a) const
{
  return point_basic<T> (_x[0]*a,
                         _x[1]*a,
                         _x[2]*a);
}
template <class T>
point_basic<T>
point_basic<T>::operator/ (const T& a) const
{
  return operator* (T(1)/T(a));
}
template <class T>
point_basic<T>
point_basic<T>::operator/ (point_basic<T> v) const
{
  return point_basic<T> (_x[0]/v[0],
                         _x[1]/v[1],
                         _x[2]/v[2]);
}
// vect2d() and mixt() are deduced from:
template <class T>
inline
T
vect2d (const point_basic<T>& v, const point_basic<T>& w)
{
  return orient2d (point_basic<T>(), v, w);
}
template <class T>
inline
T
mixt (const point_basic<T>& u, const point_basic<T>& v, const point_basic<T>& w)
{
  return orient3d (point_basic<T>(), u, v, w);
}

}// namespace rheolef
// -------------------------------------------------------------
// serialization
// -------------------------------------------------------------
#ifdef _RHEOLEF_HAVE_MPI
#include <boost/serialization/serialization.hpp>

namespace boost {
 namespace serialization {
  template<class Archive, class T>
  void serialize (Archive& ar, class rheolef::point_basic<T>& x, const unsigned int version) {
    ar & x[0];
    ar & x[1];
    ar & x[2];
  }
 } // namespace serialization
} // namespace boost

// Some serializable types, like geo_element, have a fixed amount of data stored at fixed field positions.
// When this is the case, boost::mpi can optimize their serialization and transmission to avoid extraneous copy operations.
// To enable this optimization, we specialize the type trait is_mpi_datatype, e.g.:
namespace boost {
 namespace mpi {
  // TODO: when point_basic<T> is not a simple type, such as T=bigfloat or T=gmp, etc
  template <>
  struct is_mpi_datatype<rheolef::point_basic<double> > : mpl::true_ { };
 } // namespace mpi
} // namespace boost
#endif // _RHEOLEF_HAVE_MPI

#endif /* _RHEO_BASIC_POINT_H */
