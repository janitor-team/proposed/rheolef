// file automaticaly generated by "cxx_reference_element.cc"
//
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// file automaticaly generated by "cxx_reference_element.cc"
const char
reference_element::_name [reference_element::max_variant] = {
	'p',
	'e',
	't',
	'q',
	'T',
	'P',
	'H'
};
const reference_element::size_type
reference_element::_dimension [reference_element::max_variant] = {
	0,
	1,
	2,
	2,
	3,
	3,
	3
};
const reference_element::size_type
reference_element::_first_variant_by_dimension [5] = {
	reference_element::p,
	reference_element::e,
	reference_element::t,
	reference_element::T,
	reference_element::max_variant
};
const reference_element::size_type
reference_element::_n_vertex [reference_element::max_variant] = {
	1,
	2,
	3,
	4,
	4,
	6,
	8
};
const reference_element::size_type
reference_element::_n_subgeo_by_variant [reference_element::max_variant] [reference_element::max_variant] = {
//  p  e  t  q  T  P  H
  { 1, 0, 0, 0, 0, 0, 0}, // p
  { 2, 1, 0, 0, 0, 0, 0}, // e
  { 3, 3, 1, 0, 0, 0, 0}, // t
  { 4, 4, 0, 1, 0, 0, 0}, // q
  { 4, 6, 4, 0, 1, 0, 0}, // T
  { 6, 9, 2, 3, 0, 1, 0}, // P
  { 8, 12, 0, 6, 0, 0, 1}  // H
};
static const Float
hat_K_measure [reference_element::max_variant] = {
	1,
	1,
	0.5,
	4,
	0.166666666666667,
	1,
	8
};
const reference_element::size_type
geo_element_T_fac2edg_idx [4][3] = {
	{2,1,0},
	{3,5,2},
	{0,4,3},
	{1,5,4} };

const reference_element::size_type
geo_element_P_fac2edg_idx [5][4] = {
	{2,1,0},
	{6,7,8},
	{0,4,6,3},
	{1,5,7,4},
	{3,8,5,2} };

const reference_element::size_type
geo_element_H_fac2edg_idx [6][4] = {
	{3,2,1,0},
	{4,11,7,3},
	{0,5,8,4},
	{8,9,10,11},
	{1,6,9,5},
	{2,7,10,6} };

const int
geo_element_T_fac2edg_orient [4][3] = {
	{-1,-1,-1},
	{1,-1,1},
	{1,1,-1},
	{1,1,-1} };

const int
geo_element_P_fac2edg_orient [5][4] = {
	{-1,-1,-1},
	{1,1,1},
	{1,1,-1,-1},
	{1,1,-1,-1},
	{1,-1,-1,1} };

const int
geo_element_H_fac2edg_orient [6][4] = {
	{-1,-1,-1,-1},
	{1,-1,-1,1},
	{1,1,-1,-1},
	{1,1,1,1},
	{1,1,-1,-1},
	{1,1,-1,-1} };

