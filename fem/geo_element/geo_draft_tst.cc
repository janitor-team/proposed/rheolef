///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// draft version of the geo class based on the hack_array class
// - geo::const_iterator  	begin(dim), end(dim)
// - accessor 			geo_element& K = geo_element(dim,ige);
//
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI

#include "rheolef/hack_array.h"
#include "rheolef/geo_element.h"
using namespace rheolef;
using namespace std;

template<class T, class Ref, class Ptr, class IteratorByVariant>
class geo_iterator {
  // see std::deque<T>::iterator
  typedef geo_iterator<T,Ref,Ptr,IteratorByVariant>                  _self;
  typedef geo_iterator<T,T&,T*,typename hack_array<T>::iterator>  _iterator;
public:

// typedefs

  typedef std::random_access_iterator_tag iterator_category;
  typedef T                               value_type;
  typedef Ptr                             pointer;
  typedef Ref                             reference;
  typedef typename T::size_type           size_type;
  typedef ptrdiff_t                       difference_type;

// allocators:

  geo_iterator (size_type map_dim, const IteratorByVariant& iter, const class geo_abstract_rep& omega);
  geo_iterator (const _iterator& y);

// accessors & modifiers:

  reference operator* () const { return *_iter; }
  pointer   operator->() const { return _iter.operator->(); }

  _self& operator++ () { 
    // TODO: simplifier avec variant_by_dim :
    _iter++;
    switch(_map_dim) {
      case 0:
      case 1:  break;
      case 2:  if (_iter == _last_t) { _iter = _first_q; }; break;
      case 3:
      default: if (_iter == _last_T) { _iter = _first_P; };
	       if (_iter == _last_P) { _iter = _first_H; };
               break;
    }
    return *this;
  }
  _self operator++ (int) { _self tmp = *this; operator++(); return tmp; }

protected:
  size_type         _map_dim;
  IteratorByVariant _iter;
  IteratorByVariant _last_t;
  IteratorByVariant _last_T;
  IteratorByVariant _last_P;
  IteratorByVariant _first_q;
  IteratorByVariant _first_P;
  IteratorByVariant _first_H;
};
class geo_abstract_rep {
public:

// typedefs

  typedef geo_element_hack::size_type                    size_type;
  typedef geo_element_hack::variant_type                 variant_type;
  typedef const geo_element&                          const_reference;
  typedef hack_array<geo_element_hack>::iterator       iterator_by_variant;
  typedef hack_array<geo_element_hack>::const_iterator const_iterator_by_variant;
  typedef geo_iterator<geo_element, geo_element&, geo_element*, iterator_by_variant>
          iterator; 
  typedef geo_iterator<geo_element, const geo_element&, const geo_element*, const_iterator_by_variant>
          const_iterator; 

  virtual ~geo_abstract_rep() {}

// accessors & modifiers:

  virtual size_type size (size_type map_dim) const = 0;
  virtual const_reference get_geo_element (size_type map_dim, size_type ige) const = 0;
  virtual const_iterator_by_variant begin_by_variant (variant_type variant) const = 0;
  virtual const_iterator_by_variant   end_by_variant (variant_type variant) const = 0;

  const_iterator begin (size_type dim) const {
  	const_iterator_by_variant iter = begin_by_variant (reference_element::first_variant_by_dimension(dim));
	return const_iterator (dim, iter, *this);
  }
  const_iterator end   (size_type dim) const {
  	const_iterator_by_variant iter = end_by_variant (reference_element::last_variant_by_dimension(dim));
	return const_iterator (dim, iter, *this);
  }
};
template<class T, class Ref, class Ptr, class IteratorByVariant>
geo_iterator<T,Ref,Ptr,IteratorByVariant>::geo_iterator (
  size_type                map_dim,
  const IteratorByVariant& iter,
  const geo_abstract_rep&  omega)
 : _map_dim(map_dim),
   _iter(iter),
   _last_t  (omega.end_by_variant   (reference_element::t)),
   _last_T  (omega.end_by_variant   (reference_element::T)),
   _last_P  (omega.end_by_variant   (reference_element::P)),
   _first_q (omega.begin_by_variant (reference_element::q)),
   _first_P (omega.begin_by_variant (reference_element::P)),
   _first_H (omega.begin_by_variant (reference_element::H))
{
}
template<class T, class Ref, class Ptr, class IteratorByVariant>
geo_iterator<T,Ref,Ptr,IteratorByVariant>::geo_iterator (const _iterator& y)
 : _map_dim(y._map_dim),
   _iter(y._iter),
   _last_t  (y._last_t),
   _last_T  (y._last_T),
   _last_P  (y._last_P),
   _first_q (y._first_q),
   _first_P (y._first_P),
   _first_H (y._first_H)
{
}
class geo_rep: public geo_abstract_rep {
public:
  typedef std::allocator<size_t> A;
  typedef geo_abstract_rep::const_iterator_by_variant  const_iterator_by_variant;
  typedef geo_abstract_rep::iterator_by_variant        iterator_by_variant;
  typedef geo_element::variant_type                 variant_type;
  typedef geo_element::parameter_type               parameter_type;

  geo_rep () 
   {
     for (size_type variant = 0; variant < reference_element::max_variant; variant++) {
       _geo_element [variant]
        = hack_array_mpi_rep<geo_element_hack,A>(distributor(), parameter_type(reference_element::H, 1));
     }
   }
  size_type size (size_type map_dim) const {
    switch(map_dim) {
    // TODO: simplifier avec une bcle sur variant_by_dim :
      case 0:  return _geo_element [reference_element::p].size();
      case 1:  return _geo_element [reference_element::e].size();
      case 2:  return _geo_element [reference_element::t].size()
                    + _geo_element [reference_element::q].size();
      case 3:
      default: return _geo_element [reference_element::T].size()
                    + _geo_element [reference_element::P].size()
                    + _geo_element [reference_element::H].size();
    }
  }
  const_reference get_geo_element (size_type map_dim, size_type ige) const {
    // TODO: simplifier avec variant_by_dim :
    switch(map_dim) {
      case 0:  return _geo_element [reference_element::p] [ige];
      case 1:  return _geo_element [reference_element::e] [ige];
      case 2: {
               size_t s = _geo_element [reference_element::t].size();
               if (ige < s)
                  return  _geo_element [reference_element::t] [ige];
               return     _geo_element [reference_element::q] [ige - s];
              }
      case 3:
      default:{
               size_t sT =  _geo_element [reference_element::T].size();
               if (ige < sT) 
                  return   _geo_element [reference_element::T] [ige];
               size_t sP = sT + _geo_element [reference_element::P].size();
               if (ige < sP)
                  return _geo_element [reference_element::P] [ige - sT];
               return    _geo_element [reference_element::H] [ige - sP];
              }
    }
  }
  const_iterator_by_variant begin_by_variant (variant_type variant) const {
    return _geo_element [variant].begin();
  }
  const_iterator_by_variant end_by_variant (variant_type variant) const {
    return _geo_element [variant].end();
  }
protected:
  hack_array_mpi_rep<geo_element_hack,A> _geo_element [reference_element::max_variant];
};
int main(int argc, char**argv)
{
  geo_rep omega;
}
#endif // _RHEOLEF_HAVE_MPI
