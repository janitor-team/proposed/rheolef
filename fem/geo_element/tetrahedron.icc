#ifndef _RHEO_TETRA_ICC
#define _RHEO_TETRA_ICC
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2018 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

/**
@femclassfile tetrahedron reference element
@addindex reference element
@addindex command `gmsh`
@addindex command `msh2geo`

Description
===========
The edge @ref reference_element_6 is 
@verbatim
        K = { 0 < x0 < 1 and 0 < x1 < 1-x0 and 0 < x2 < 1-x0-x1 }

                          x2
                        .
                      ,/
                     /
                   3        
                 ,/|`\
               ,/  |  `\
             ,/    '.   `\
           ,/       |     `\
         ,/         |       `\
        0-----------'.--------2 --> x1  
         `\.         |      ,/        
            `\.      |    ,/         
               `\.   '. ,/          
                  `\. |/           
                     `1           
                        `\.
                          ` x0
@endverbatim
The orientation is such that the trihedral (01, 02, 03) is direct,
and all faces, see from exterior, are in the direct sens. 
See

        P. L. Georges, 
        Generation automatique de maillages,
        page 24-, coll RMA, 16, Masson, 1994.

This three-dimensional @ref reference_element_6 is then transformed,
after the Piola geometrical application, as a tetrahedron
in a physical space, as a @ref geo_element_6.

Curved high order Pk tetrahedra (k >= 1) in 3D geometries are supported.
These tetrahedra have additional edge-nodes, face-nodes
and internal volume-nodes.
These nodes are numbered as: first vertex, then edge-node, following
the edge numbering order and orientation, then face-nodes following the face numbering
order and orientation, and finally the face internal nodes,
following the tetrahedron lattice.
See below for edges and faces numbering and orientation.
@verbatim
                   3                
                 ,/|`\
               ,/  |  `\
             ,7    '.   `9       
           ,/       |     `\
         ,/         8       `\
        0--------6--'.--------2
         `\.         |      ,/
            `\.      |    ,5  
               `4.   '. ,/   
                  `\. |/    
                     `1    
                  P2      
@endverbatim
Notice that the edge-nodes and face-nodes numbering slightly differ from
those used in the `gmsh` mesh generator when using high-order elements.
This difference is handled by the @ref msh2geo_1 mesh file converter.

Implementation
==============
@showfromfile
@snippet tetrahedron.icc verbatim_tetrahedron
*/

// [verbatim_tetrahedron]
const size_t dimension = 3;
const Float  measure = Float(1.)/Float(6.);
const size_t n_vertex = 4;
const point vertex [n_vertex] = {
	point( 0, 0, 0 ),
	point( 1, 0, 0 ),
	point( 0, 1, 0 ),
	point( 0, 0, 1 ) };
const size_t  n_face = 4;
const size_t face [n_face][3] = {
	{ 0, 2, 1 },
	{ 0, 3, 2 },
	{ 0, 1, 3 },
	{ 1, 2, 3 } };
const size_t  n_edge = 6;
const size_t edge [n_edge][2] = {
	{ 0, 1 },
	{ 1, 2 },
	{ 2, 0 },
	{ 0, 3 },
	{ 1, 3 },
	{ 2, 3 } };
// [verbatim_tetrahedron]

#endif // _RHEO_TETRA_ICC
