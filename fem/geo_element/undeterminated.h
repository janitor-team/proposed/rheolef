# ifndef _RHEO_UNDETERMINATED_H
# define _RHEO_UNDETERMINATED_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
#include "rheolef/point.h"
#include "rheolef/tensor.h"
#include "rheolef/tensor3.h"
#include "rheolef/tensor4.h"
#include "rheolef/promote.h"

namespace rheolef {
/// @brief helper for generic field value_type: T, point_basic<T> or tensor_basic<T>
template<class T>
struct undeterminated_basic {
  typedef T scalar_type;
  typedef T float_type;
};
template<class T> struct  scalar_traits<undeterminated_basic<T> > { typedef T                              type; };
template<class T> struct   float_traits<undeterminated_basic<T> > { typedef typename float_traits<T>::type type; };

template<class T> struct   is_undeterminated : std::false_type {};
template<class T> struct   is_undeterminated<undeterminated_basic<T> > : std::true_type {};

// promote: used to eliminate undeterminated_basic from generic expressions
template<class T1, class T2>
struct promote<undeterminated_basic<T1>, undeterminated_basic<T2> > {
  typedef undeterminated_basic<typename promote<T1,T2>::type> type;
};

// scalar:
template<class T1, class T2>
struct promote<T1, undeterminated_basic<T2> > {
  typedef typename promote<T1,T2>::type type;
};
template<class T1, class T2>
struct promote<undeterminated_basic<T1>, T2 > {
  typedef typename promote<T1,T2>::type type;
};

#define _RHEOLEF_tensor_promote(tensor) 			\
template<class T1, class T2>					\
struct promote<tensor##_basic<T1>, undeterminated_basic<T2> > {	\
  typedef tensor##_basic<typename promote<T1,T2>::type> type;	\
};								\
template<class T1, class T2>					\
struct promote<undeterminated_basic<T1>, tensor##_basic<T2> > {	\
  typedef tensor##_basic<typename promote<T1,T2>::type> type;	\
};								\
template<class T1, class T2>					\
struct promote<tensor##_basic<T1>, tensor##_basic<T2> > {	\
  typedef tensor##_basic<typename promote<T1,T2>::type> type;	\
};
_RHEOLEF_tensor_promote(point)
_RHEOLEF_tensor_promote(tensor)
_RHEOLEF_tensor_promote(tensor3)
_RHEOLEF_tensor_promote(tensor4)
#undef _RHEOLEF_tensor_promote

} // namespace rheolef
#endif // _RHEO_UNDETERMINATED_H
