///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// load a mesh and reprint it
// if input domains are formated in version=1, upgrade it in version=2
#include "rheolef/geo_element.h"
using namespace rheolef;
using namespace std;

void dump (const geo_element& K, size_t side_dim) {
    cout << " " << side_dim << "d sides:" << endl;
    for (size_t loc_isid = 0, loc_nsid = K.n_subgeo (side_dim); loc_isid < loc_nsid; loc_isid++) {
      cout << " ";
      for (size_t j = 0, n = K.subgeo_n_node(side_dim,loc_isid); j < n; j++) {
        cout << " " << K.subgeo_local_node (side_dim,loc_isid,j);
      }
      cout << endl;
    }
}
void dump (const geo_element& K) {
    cout << "P" << K.order() << "(" << K.name() << ") :" << endl;
    for (size_t d = 0; d <= K.dimension(); d++) {
      dump (K, d);
    }
    cout << endl;
}
int main(int argc, char**argv) {
    dump (geo_element_auto<>(reference_element::p,1));

    dump (geo_element_auto<>(reference_element::e,1));
    dump (geo_element_auto<>(reference_element::e,2));
    dump (geo_element_auto<>(reference_element::e,3));
    dump (geo_element_auto<>(reference_element::e,9));

    dump (geo_element_auto<>(reference_element::t,1));
    dump (geo_element_auto<>(reference_element::t,2));
    dump (geo_element_auto<>(reference_element::t,3));
    dump (geo_element_auto<>(reference_element::t,9));

    dump (geo_element_auto<>(reference_element::q,1));
    dump (geo_element_auto<>(reference_element::q,2));
    dump (geo_element_auto<>(reference_element::q,3));
    dump (geo_element_auto<>(reference_element::q,9));

    dump (geo_element_auto<>(reference_element::T,1));
    dump (geo_element_auto<>(reference_element::T,2));
    dump (geo_element_auto<>(reference_element::T,3));
    dump (geo_element_auto<>(reference_element::T,4));
    dump (geo_element_auto<>(reference_element::T,9));

    dump (geo_element_auto<>(reference_element::P,1));
    dump (geo_element_auto<>(reference_element::P,2));
    dump (geo_element_auto<>(reference_element::P,3));
#ifdef TODO
    dump (geo_element_auto<>(reference_element::P,9));
#endif // TODO

    dump (geo_element_auto<>(reference_element::H,1));
    dump (geo_element_auto<>(reference_element::H,2));
    dump (geo_element_auto<>(reference_element::H,3));
    dump (geo_element_auto<>(reference_element::H,9));
}
