# ifndef _RHEOLEF_TENSOR4_H
# define _RHEOLEF_TENSOR4_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@classfile tensor4 d-dimensional physical fourth-order tensor
@addindex tensor fourth-order

Description
===========
The `tensor4` class defines a `d^4` array with floating coefficients.
This class is suitable for defining fourth-order tensors,
i.e. @ref field_2 with `d^4` matrix values at each physical position.

It is represented as a fourth-dimensional array of coordinates.
The coordinate indexes start at zero and finishes at `d-1`,
e.g. `a(0,0,0,0)`, `a(0,0,0,1)`, ..., `a(2,2,2,2)`.

The default constructor set all components to zero:

        tensor4 a;

The standard linear algebra is supported.

Implementation
==============
@showfromfile
The `tensor4` class is simply an alias to the `tensor4_basic` class

@snippet tensor4.h verbatim_tensor4

The `tensor4_basic` class is a template class
with the floating type as parameter:

@snippet tensor4.h verbatim_tensor4_basic
@snippet tensor4.h verbatim_tensor4_basic_cont
@par

The norm  and contracted product with a
second-order tensor is provided, together with the
`dexp` fuinction, that represents the derivative of
the tensor matrix function.

@snippet tensor4.h verbatim_tensor4_basic_cont2
*/
} // namespace rheolef

#include "rheolef/point.h"
#include "rheolef/tensor.h"
namespace rheolef {

// [verbatim_tensor4_basic]
template<class T>
class tensor4_basic {
public:

  typedef size_t size_type;
  typedef T      element_type;
  typedef T      float_type;

// allocators:

  tensor4_basic ();
  explicit tensor4_basic (const T& init_val);
  tensor4_basic (const tensor4_basic<T>& a);
  static tensor4_basic<T> eye (size_type d = 3);

  tensor4_basic (const std::initializer_list<std::initializer_list<
		       std::initializer_list<std::initializer_list<T> > > >& il);

// affectation:

  tensor4_basic<T>& operator= (const tensor4_basic<T>& a);
  tensor4_basic<T>& operator= (const T& val);

// accessors:

  T&       operator()(size_type i, size_type j, size_type k, size_type l);
  const T& operator()(size_type i, size_type j, size_type k, size_type l) const;

  tensor_basic<T>&  operator()(size_type i, size_type j);
  const tensor_basic<T>& operator()(size_type i, size_type j) const;

// algebra:

  tensor4_basic<T>  operator* (const T& k) const;
  tensor4_basic<T>  operator/ (const T& k) const;
  tensor4_basic<T>  operator+ (const tensor4_basic<T>& b) const;
  tensor4_basic<T>  operator- (const tensor4_basic<T>& b) const;
  tensor4_basic<T>& operator+= (const tensor4_basic<T>&);
  tensor4_basic<T>& operator-= (const tensor4_basic<T>&);
  tensor4_basic<T>& operator*= (const T& k);
  tensor4_basic<T>& operator/= (const T& k) { return operator*= (1./k); }

// io:
  std::ostream& put (std::ostream& out, size_type d=3) const;
// [verbatim_tensor4_basic]

// data:
protected:
  tensor_basic<tensor_basic<T> > _x;
// [verbatim_tensor4_basic_cont]
};
// [verbatim_tensor4_basic_cont]

// [verbatim_tensor4]
typedef tensor4_basic<Float> tensor4;
// [verbatim_tensor4]

// [verbatim_tensor4_basic_cont2]
template <class T>
T norm (const tensor4_basic<T>& a) { return sqrt(norm2(a)); }

template <class T>
T norm2 (const tensor4_basic<T>&);

template <class T>
tensor_basic<T> ddot (const tensor4_basic<T>&, const tensor_basic<T>&);

template <class T>
tensor_basic<T> ddot (const tensor_basic<T>&, const tensor4_basic<T>&);

template <class T>
tensor4_basic<T> dexp (const tensor_basic<T>& a, size_t d = 3);
// [verbatim_tensor4_basic_cont2]
// -----------------------------------------------------------------------
// inlined
// -----------------------------------------------------------------------
template<class T> struct  float_traits<tensor4_basic<T> > { typedef typename float_traits<T>::type type; };
template<class T> struct scalar_traits<tensor4_basic<T> > { typedef T type; };

template<class T>
inline
tensor4_basic<T>::tensor4_basic()
 : _x (tensor_basic<T>(T()))
{ 
}
template<class T>
inline
tensor4_basic<T>::tensor4_basic(const T& init_val)
 : _x (tensor_basic<T>(init_val))
{ 
}
template<class T>
inline
tensor4_basic<T>::tensor4_basic (const tensor4_basic<T>& a)
 : _x (tensor_basic<T>(T()))
{
    operator= (a);
}
template<class T>
inline
tensor_basic<T>&
tensor4_basic<T>::operator() (size_type i, size_type j)  
{
    return _x(i,j);
}
template<class T>
inline
const tensor_basic<T>&
tensor4_basic<T>::operator() (size_type i, size_type j) const
{
    return _x(i,j);
}
template<class T>
inline
T&
tensor4_basic<T>::operator() (size_type i, size_type j, size_type k, size_type l)  
{
    return _x(i,j)(k,l);
}
template<class T>
inline
const T&
tensor4_basic<T>::operator() (size_type i, size_type j, size_type k, size_type l) const
{
    return _x(i,j)(k,l);
}
template <class T>
inline
tensor4_basic<T>
tensor4_basic<T>::operator* (const T& k) const
{
  tensor4_basic<T> b = *this;
  b *= k;
  return b;
}
template <class T, class U>
inline
typename
std::enable_if<
  details::is_rheolef_arithmetic<U>::value
 ,tensor4_basic<T>
>::type
operator* (const U& k, const tensor4_basic<T>& a)
{
  return a*k;
}
template <class T>
inline
tensor4_basic<T>
tensor4_basic<T>::operator/ (const T& k) const
{
  return operator* (1./k);
}
// inputs/outputs:
#ifdef TODO
template<class T>
inline
std::istream& operator>> (std::istream& in, tensor4_basic<T>& a)
{
    return a.get (in);
}
#endif // TODO
template<class T>
inline
std::ostream& operator<< (std::ostream& out, const tensor4_basic<T>& a)
{
    return a.put (out);
}

}// namespace rheolef
# endif /* _RHEOLEF_TENSOR4_H */
