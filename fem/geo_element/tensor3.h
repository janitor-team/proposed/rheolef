# ifndef _RHEOLEF_TENSOR3_H
# define _RHEOLEF_TENSOR3_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================

namespace rheolef {
/**
@classfile tensor3 d-dimensional physical third-order tensor
@addindex tensor third-order

Description
===========
The `tensor3` class defines a `d*d*d` array with floating coefficients.
This class is suitable for defining third-order tensors,
i.e. @ref field_2 with `d*d*d` matrix values at each physical position.

It is represented as a tridimensional array of coordinates.
The coordinate indexes start at zero and finishes at `d-1`,
e.g. `a(0,0,0)`, `a(0,0,1)`, ..., `a(2,2,2)`.

The default constructor set all components to zero:

        tensor3 a;

The linear algebra with scalars and
@ref tensor_2 is supported.

Implementation
==============
@showfromfile
The `tensor3` class is simply an alias to the `tensor3_basic` class

@snippet tensor3.h verbatim_tensor3

The `tensor3_basic` class is a template class
with the floating type as parameter:

@snippet tensor3.h verbatim_tensor3_basic
@snippet tensor3.h verbatim_tensor3_basic_cont
*/
} // namespace rheolef

#include "rheolef/point.h"
#include "rheolef/tensor.h"
namespace rheolef {

// [verbatim_tensor3_basic]
template<class T>
class tensor3_basic {
public:

  typedef size_t size_type;
  typedef T      element_type;
  typedef T      float_type;

// allocators:

  tensor3_basic (const T& init_val = 0);
  tensor3_basic (const tensor3_basic<T>& a);

// affectation:

  tensor3_basic<T>& operator= (const tensor3_basic<T>& a);
  tensor3_basic<T>& operator= (const T& val);

// accessors:

  T&       operator()(size_type i, size_type j, size_type k);
  const T& operator()(size_type i, size_type j, size_type k) const;

// algebra
  tensor3_basic<T>  operator*  (const T& k) const;
  tensor3_basic<T>  operator/  (const T& k) const;
  tensor_basic<T>   operator*  (const point_basic<T>& v) const;
  tensor3_basic<T>  operator*  (const tensor_basic<T>& b) const;
  tensor3_basic<T>  operator+  (const tensor3_basic<T>& b) const;
  tensor3_basic<T>  operator-  (const tensor3_basic<T>& b) const;
  tensor3_basic<T>& operator+= (const tensor3_basic<T>&);
  tensor3_basic<T>& operator-= (const tensor3_basic<T>&);
  tensor3_basic<T>& operator*= (const T& k);
  tensor3_basic<T>& operator/= (const T& k) { return operator*= (1./k); }

// inputs/outputs:

  std::ostream& put (std::ostream& s, size_type d = 3) const;
  std::istream& get (std::istream&);
// [verbatim_tensor3_basic]

// data:
protected:
  T _x [3][3][3];
// [verbatim_tensor3_basic_cont]
};
// [verbatim_tensor3_basic_cont]

// [verbatim_tensor3]
typedef tensor3_basic<Float> tensor3;
// [verbatim_tensor3]

// -----------------------------------------------------------------------
// inlined
// -----------------------------------------------------------------------
template<class T> struct  float_traits<tensor3_basic<T> > { typedef typename float_traits<T>::type type; };
template<class T> struct scalar_traits<tensor3_basic<T> > { typedef T type; };

template<class T>
inline
tensor3_basic<T>::tensor3_basic (const T& init_val)
{ 
    operator= (init_val);
}
template<class T>
inline
tensor3_basic<T>::tensor3_basic (const tensor3_basic<T>& a)
{
    operator= (a);
}
template<class T>
inline
T&
tensor3_basic<T>::operator() (size_type i, size_type j, size_type k)  
{
    return _x[i%3][j%3][k%3];
}
template<class T>
inline
const T&
tensor3_basic<T>::operator() (size_type i, size_type j, size_type k) const
{
    return _x[i%3][j%3][k%3];
}
template <class T>
inline
tensor3_basic<T>
tensor3_basic<T>::operator* (const T& k) const
{
  tensor3_basic<T> b = *this;
  b *= k;
  return b;
}
template <class T, class U>
inline
typename
std::enable_if<
  details::is_rheolef_arithmetic<U>::value
 ,tensor3_basic<T>
>::type
operator* (const U& k, const tensor3_basic<T>& a)
{
  return a*k;
}
template <class T>
inline
tensor3_basic<T>
tensor3_basic<T>::operator/ (const T& k) const
{
  return operator* (1./k);
}
template <class U>
U dddot (const tensor3_basic<U>&, const tensor3_basic<U>&);

template <class T>
inline
T
norm2 (const tensor3_basic<T>& a)
{
  return dddot(a,a);
}
template <class T>
inline
T
dist2 (const tensor3_basic<T>& a, const tensor3_basic<T>& b)
{
  return norm2(a-b);
}
template <class U>
inline
U
norm  (const tensor3_basic<U>& a)
{
  return sqrt(norm2(a));
}
template <class U>
inline
U
dist (const tensor3_basic<U>& a, const tensor3_basic<U>& b)
{
  return norm(a-b);
}

// inputs/outputs:
template<class T>
inline
std::istream& operator>> (std::istream& in, tensor3_basic<T>& a)
{
    return a.get (in);
}
template<class T>
inline
std::ostream& operator<< (std::ostream& out, const tensor3_basic<T>& a)
{
    return a.put (out);
}

}// namespace rheolef
# endif /* _RHEOLEF_TENSOR3_H */
