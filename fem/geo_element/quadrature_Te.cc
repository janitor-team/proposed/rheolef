///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/quadrature.h"
#include "rheolef/gauss_jacobi.h"
namespace rheolef {
using namespace std;

template<class T>
void
quadrature_on_geo<T>::init_tetrahedron  (quadrature_option opt)
{
  quadrature_option::family_type f = opt.get_family();
  // -------------------------------------------------------------------------
  // special case : equispaced, for irregular (e.g. Heaviside) functions
  // -------------------------------------------------------------------------
  if (f == quadrature_option::equispaced) {
    size_type r = opt.get_order();
    if (r == 0) {
      wx (x(1/T(3),1/T(3),1/(3)), T(1)/6);
    } else {
      size_type n = (r+1)*(r+2)*(r+3)/6;
      T w = (1/T(6))/T(int(n));
      for (size_type i = 0; i <= r; i++) {
        for (size_type j = 0; i+j <= r; j++) {
          for (size_type k = 0; i+j+k <= r; k++) {
            wx (x(T(int(i))/r,T(int(j))/r,T(int(k))/r), w);
          }
        }
      }
    }
    return;
  }
  // -------------------------------------------------------------------------
  // special case : superconvergent patch recovery nodes & weights
  // -------------------------------------------------------------------------
  if (f == quadrature_option::superconvergent) {
    switch (opt.get_order()) {
      case 0:
      case 1:
      case 2:
        f =  quadrature_option::gauss;	
        break;
      default:
	error_macro ("unsupported superconvergent("<<opt.get_order()<<")");
    }
  }
  // -------------------------------------------------------------------------
  // special case : Gauss-Lobatto points
  // e.g. when using special option in riesz() function
  // -------------------------------------------------------------------------
  if (f == quadrature_option::gauss_lobatto) {
    switch (opt.get_order()) {
     case 0 :
     case 1 :
	    // trapezes:
	    wx(x(T(0), T(0), T(0)), T(1)/24);
	    wx(x(T(1), T(0), T(0)), T(1)/24);
	    wx(x(T(0), T(1), T(0)), T(1)/24);
	    wx(x(T(0), T(0), T(1)), T(1)/24);
	    break;
     case 2 :
	    // TODO: simpson
	    // break;
     default:
	    error_macro ("unsupported Gauss-Lobatto("<<opt.get_order()<<")");
    }
    return;
  }
  // -------------------------------------------------------------------------
  // general case: Gauss
  // -------------------------------------------------------------------------
  check_macro (f == quadrature_option::gauss,
        "unsupported quadrature family \"" << opt.get_family_name() << "\"");
 
  switch (opt.get_order()) {
   case 0:
   case 1:
	// central point:
	// better than the general case
	// r=0 : 4 nodes
	// r=1 : 12 nodes
	// here: 1 node
	wx (x(0.25, 0.25, 0.25), T(1)/6);
	break;
   case 2: {
	// better than the general case
	// r=2 : 18 nodes
	// here: 4 nodes
	T a = (5 -   sqrt(T(5)))/20;
	T b = (5 + 3*sqrt(T(5)))/20;
	wx (x(a, a, a), T(1)/24);
	wx (x(a, a, b), T(1)/24);
	wx (x(a, b, a), T(1)/24);
	wx (x(b, a, a), T(1)/24);
	break;
   }
   case 3:
   case 4:
   case 5: {
	// better than the general case
	// r=3 : 36 nodes
	// r=4 : 48 nodes
	// r=5 : 80 nodes
	// here: 15 nodes
	T a  = 0.25;
	T b1 = (7 + sqrt(T(15)))/34;
	T b2 = (7 - sqrt(T(15)))/34;
	T c1 = (13 - 3*sqrt(T(15)))/34;
	T c2 = (13 + 3*sqrt(T(15)))/34;
	T w1 = (2665 - 14*sqrt(T(15)))/226800;
	T w2 = (2665 + 14*sqrt(T(15)))/226800;
	T d  = (5 - sqrt(T(15)))/20;
	T e  = (5 + sqrt(T(15)))/20;
	wx (x(a,  a,  a),  T(8)/405);
	wx (x(b1, b1, b1), w1);
	wx (x(b1, b1, c1), w1);
	wx (x(b1, c1, b1), w1);
	wx (x(c1, b1, b1), w1);
	wx (x(b2, b2, b2), w2);
	wx (x(b2, b2, c2), w2);
	wx (x(b2, c2, b2), w2);
	wx (x(c2, b2, b2), w2);
	wx (x(d,  d,  e),  T(5)/567);
	wx (x(d,  e,  d),  T(5)/567);
	wx (x(e,  d,  d),  T(5)/567);
	wx (x(d,  e,  e),  T(5)/567);
	wx (x(e,  d,  e),  T(5)/567);
	wx (x(e,  e,  d),  T(5)/567);
	break;
   }
   default: {
    // Gauss-Legendre quadrature formulae 
    //  where Legendre = Jacobi(alpha=0,beta=0) polynoms
    size_t r = opt.get_order();
    size_type n0 = n_node_gauss(r);
    size_type n1 = n_node_gauss(r+1);
    size_type n2 = n_node_gauss(r+2);
    vector<T> zeta0(n0), omega0(n0);
    vector<T> zeta1(n1), omega1(n1);
    vector<T> zeta2(n2), omega2(n2);
    gauss_jacobi (n0, 0, 0, zeta0.begin(), omega0.begin());
    gauss_jacobi (n1, 0, 0, zeta1.begin(), omega1.begin());
    gauss_jacobi (n2, 0, 0, zeta2.begin(), omega2.begin());

    for (size_type i = 0; i < n0; i++) {
      for (size_type j = 0; j < n1; j++) {
        for (size_type k = 0; k < n2; k++) {
          // we transform the cube into the tetra 
	  T eta_0 = (1+zeta0[i])*(1-zeta1[j])*(1-zeta2[k])/8;
	  T eta_1 =              (1+zeta1[j])*(1-zeta2[k])/4;
	  T eta_2 =                           (1+zeta2[k])/2;
	  T J     =              (1-zeta1[j])*sqr(1-zeta2[k])/64;
          wx (x(eta_0,eta_1,eta_2), J*omega0[i]*omega1[j]*omega2[k]);
        }
      }
    }
   }
  }
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T)                                             	\
template void quadrature_on_geo<T>::init_tetrahedron (quadrature_option);

_RHEOLEF_instanciation(Float)

}// namespace rheolef
