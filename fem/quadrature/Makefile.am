## process this file with automake to produce Makefile.in
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
include ../../config/am_options.mk
include ${top_builddir}/config/config.mk

# ---------------------------------------------------------------------------
# programing files
# ---------------------------------------------------------------------------

TEX_INCLUDED =						\
	jacobi.icc					\
	jacobi_roots.icc				\
	gamma.icc					\
	gauss_jacobi.icc				\
	gauss_radau_jacobi.icc				\
	gauss_lobatto_jacobi.icc			\
	gauss_chebyschev.icc				\
	gauss_radau_chebyschev.icc			\
	gauss_lobatto_chebyschev.icc			\
	gauss_legendre_check.icc			\
	gauss_chebyschev_check.icc			\
	gauss_jacobi_check.icc

pkginclude_HEADERS = 					\
	$(TEX_INCLUDED)					\
	$(TEX_INCLUDED:.icc=.h)				

check_PROGRAMS = 					\
	jacobi_roots_tst				\
	gauss_legendre_tst				\
	gauss_chebyschev_tst				\
	gauss_jacobi_tst				\
	gauss_radau_legendre_tst			\
	gauss_radau_chebyschev_tst			\
	gauss_radau_jacobi_tst				\
	gauss_lobatto_legendre_tst			\
	gauss_lobatto_chebyschev_tst			\
	gauss_lobatto_jacobi_tst

jacobi_roots_tst_SOURCES = jacobi_roots_tst.cc
gauss_legendre_tst_SOURCES = gauss_legendre_tst.cc
gauss_chebyschev_tst_SOURCES = gauss_chebyschev_tst.cc
gauss_jacobi_tst_SOURCES = gauss_jacobi_tst.cc
gauss_radau_legendre_tst_SOURCES = gauss_radau_legendre_tst.cc
gauss_radau_chebyschev_tst_SOURCES = gauss_radau_chebyschev_tst.cc
gauss_radau_jacobi_tst_SOURCES = gauss_radau_jacobi_tst.cc
gauss_lobatto_legendre_tst_SOURCES = gauss_lobatto_legendre_tst.cc
gauss_lobatto_chebyschev_tst_SOURCES = gauss_lobatto_chebyschev_tst.cc
gauss_lobatto_jacobi_tst_SOURCES = gauss_lobatto_jacobi_tst.cc

AM_CPPFLAGS = 						\
	$(INCLUDES_BOOST) 				\
	-I${top_builddir}/include			\
	$(INCLUDES_FLOAT)				\
	$(INCLUDES_DMALLOC)

LDADD = 							\
        $(LDADD_FLOAT)						\
	$(LDADD_DMALLOC)					\
	-lm

# automatically generated source file:

# ---------------------------------------------------------------------------
# test files
# ---------------------------------------------------------------------------

TESTS = 						\
	${check_PROGRAMS:=.sh}				\
	gauss_jacobi_large_tst.sh

TESTS_ENVIRONMENT = 					\
	SRCDIR="${srcdir}"; export SRCDIR;		\
	TOP_SRCDIR="${top_srcdir}"; export TOP_SRCDIR;	\
	DMALLOC_OPTIONS="${DMALLOC_OPTIONS}"; export DMALLOC_OPTIONS;	\
	bash

# ---------------------------------------------------------------------------
# documenting files
# ---------------------------------------------------------------------------

MAIN_TEX_SRC  = jacobi.tex
INC_TEX_SRC  = matvec.tex
PS_SRC = mbb-15.ps mbu-15.ps mub-15.ps muu-15.ps
FIG_SRC  = transform.fig numbering.fig
PLOT_SRC = cout-qr.plot
DATA_SRC = cout-qr.dat


$(MAIN_TEX_SRC:.tex=.dvi): 				\
	$(INC_TEX_SRC)					\
	$(PS_SRC)					\
	$(FIG_SRC:.fig=.tex)				\
	$(PLOT_SRC:.plot=.eps)				\
	$(TEX_INCLUDED:.icc=.lg)

# ---------------------------------------------------------------------------
# the file set
# ---------------------------------------------------------------------------
noinst_DATA =						\
	$(PS_SRC)					\
	$(FIG_SRC)					\
	$(DATA_SRC)					\
	subst.sed					

EXTRA_DIST = 						\
	Makefile.am					\
	$(INC_TEX_SRC)					\
	$(MAIN_TEX_SRC)					\
	spectral.bib					\
	$(PLOT_SRC)					\
	${TESTS}					\
	$(noinst_DATA)			

CVSIGNORE = 						\
	Makefile 					\
	Makefile.in					\
	stamp-symlink.in				

WCIGNORE      = $(noinst_DATA)			
LICENSEIGNORE = $(noinst_DATA)			

# ---------------------------------------------------------------------------
# latex rules
# ---------------------------------------------------------------------------

# depend upon lgrind that is debian/non-free
# => not in "make dvi" target, but still in local "make doc" target
#
#dvi-local: doc
doc: $(MAIN_TEX_SRC:.tex=.dvi) $(MAIN_TEX_SRC:.tex=.ps)

SUFFIXES = .lg .icc 

.tex.dvi:
	rm -f $*.aux $*.bbl
	@TEXINPUTS=":.:$(srcdir)/:$$TEXINPUTS";				\
	 BIBINPUTS="$(srcdir):$(top_srcdir)/doc";			\
	 export TEXINPUTS BIBINPUTS;					\
	latex $< ;							\
	 for f in `ls $(*F)*.aux`; do 				\
	    g=`expr $$f : '\(.*\)\.aux' `; 				\
	    echo "bibtex $$g"; 						\
	    bibtex $$g | tee $$g.biblog; 				\
	    if grep -i warning $$g.biblog >/dev/null; then exit 1; else true; fi; \
	    if grep -i error   $$g.biblog >/dev/null; then exit 1; else true; fi; \
	done;								\
	makeindex -o $*.cnd $*.cdx;					\
	makeindex -o $*.fnd $*.fdx;					\
	makeindex -o $*.gnd $*.gdx;					\
	latex $< ;							\
	latex $< ;							\
	latex $< 
	grep Warning $*.log || true 
	test `grep Warning $*.log | \
	      grep -v -i 'float' | \
	      grep -v -i 'has changed' | \
	      grep -v -i 'font shape' | \
	      wc -l` \
	     -eq 0

.plot.eps:
	GNUPLOT_LIB=$(srcdir) bash $(top_srcdir)/doc/pusrman/plot2pdf.sh -eps -gnuplot5 $<
	rm -f $(srcdir)/$*.glog
.fig.tex:
	fig2dev -L pstex_t -p $*.ps $< > $@
	fig2dev -L pstex            $< > $*.ps

jacobi.ps: jacobi.dvi
	if test x"${DVIPS}" != x""; then 			\
	  TEXINPUTS=":.:$(srcdir)/:$$TEXINPUTS" 		\
	  ${DVIPS} -t a4 `echo $< | sed 's,.*/,,'` -o $@;	\
	fi

CXX2LATEX = sed -e '/^\/\/\//d' | lgrind -i  -lC++ - | sed -f $(srcdir)/subst.sed

.icc.lg:
	cat $< | $(CXX2LATEX) > $@
.cc.lg:
	cat $< | $(CXX2LATEX) > $@
.h.lg:
	cat $< | $(CXX2LATEX) > $@

# ---------------------------------------------------------------------------
# other rules
# ---------------------------------------------------------------------------

CLEANFILES = 							\
	*.bak							\
	*.lg							\
	$(MAIN_TEX_SRC:.tex=.dvi)				\
	$(MAIN_TEX_SRC:.tex=.ps)				\
	$(MAIN_TEX_SRC:.tex=.log)				\
	$(MAIN_TEX_SRC:.tex=.aux)				\
	$(MAIN_TEX_SRC:.tex=.toc)				\
	$(MAIN_TEX_SRC:.tex=.lof)				\
	$(MAIN_TEX_SRC:.tex=.ilg)				\
	$(MAIN_TEX_SRC:.tex=.bbl)				\
	$(MAIN_TEX_SRC:.tex=.blg)				\
	$(MAIN_TEX_SRC:.tex=.biblog)				\
	$(MAIN_TEX_SRC:.tex=.cnd)				\
	$(MAIN_TEX_SRC:.tex=.ind)				\
	$(MAIN_TEX_SRC:.tex=.fnd)				\
	$(MAIN_TEX_SRC:.tex=.gnd)				\
	$(MAIN_TEX_SRC:.tex=.cdx)				\
	$(MAIN_TEX_SRC:.tex=.idx)				\
	$(MAIN_TEX_SRC:.tex=.fdx)				\
	$(MAIN_TEX_SRC:.tex=.gdx)				\
	$(PLOT_SRC:.plot=.tex)					\
	$(FIG_SRC:.fig=.tex)					\
	$(FIG_SRC:.fig=.ps)					\
	$(PLOT_SRC:.plot=.tex)					\
	stamp-symlink.in

symlink: 
	$(MKSYMLINK) $(pkginclude_HEADERS)

stamp-symlink.in: Makefile.am
	@$(MKSYMLINK) $(pkginclude_HEADERS)
	@touch stamp-symlink.in

BUILT_SOURCES =                                                 \
	stamp-symlink.in

