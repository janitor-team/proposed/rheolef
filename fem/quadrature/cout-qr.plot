#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
set terminal latex
set output "cout-qr.tex"

#fit:
a=2.03224086937104 
c=2.06507113100646e-06
f(x)=c*x**a

set size square
set logscale
set ytics (                             \
        "[r]{$10^{-3}$}"   1e-3,        \
        "[r]{$10^{-1}$}"   1e-1,        \
        "[r]{$10^{1}$}"   1e+1,        \
        "[r]{$10^{3}$}"   1e+3        \
        ) 
set xtics (                             \
        "{$10^{1}$}"   1e1,        \
        "{$10^{2}$}"   1e2,        \
        "{$10^{3}$}"   1e3,        \
        "{$10^{4}$}"   1e4        \
        ) 
set xlabel "{\\large $R$}"
set  label "[l]{\\large $T_{QR}(R)$}" at graph 0.1,0.8

set arrow from graph 0.65,0.45 to graph 0.85,0.45 nohead lt 1
set arrow from graph 0.85,0.45 to graph 0.85,0.65 nohead lt 1
set arrow from graph 0.85,0.65 to graph 0.65,0.45 nohead lt 1
set label "[l]{\\normalsize $2$}" at graph 0.87, 0.55

plot [10:1e+4][0.001:1e+3] 'cout-qr.dat' notitle w lp 1 8

#pause -1 "<retour>"


