<a href="http://www-ljk.imag.fr/membres/Pierre.Saramito">Pierre Saramito</a>
is the project leader, main developments and code maintainer.

Present and past contributors:
- <a href="http://www.mines-paristech.fr/Services/Annuaire/franck-pigeonneau">Franck Pigeonneau</a>:
  discontinuous Galerkin method for interface problems (phase field, level set).
- <a href="mailto:mahamar.dicko@gmail.com">Mahamar Dicko</a>:
  finite element methods for equations on surfaces.
- <a href="https://www-timc.imag.fr/ibrahim-cheddadi">Ibrahim Cheddadi</a>:
  discontinuous Galerkin method for tensor transport problems.
- <a href="https://www.rhu.edu.lb/directory-listing/abou-orom-lara-a">Lara Abouorm</a>:
  banded level set method for equations on surfaces.
- <a href="https://www-liphy.ujf-grenoble.fr/pagesperso/etienne">Jocelyn Etienne</a>:
  characteristic method for time-dependent problems.
- <a href="https://www.ifsttar.fr/menu-haut/annuaire/fiche-personnelle/personne/roquet-nicolas">Nicolas Roquet</a>:
  initial versions of the Stokes and Bingham flow solvers.


Auxilliary code included in Rheolef:
- For convenience, Rheolef includes the @ref bamg_1 mesh generator
  from Frédéric Hecht <Frederic.Hecht@inria.fr> (util/bamg/*).
- For convenience, Rheolef includes a `dmalloc` auxilliary file
  from Gray Watson (config/dmalloc_return.h ; util/dmallocxx)
