///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// assembly on process 0
// and send values to others via 
// data exchange
//
#include "rheolef/disarray.h"
#include "rheolef/environment.h"
using namespace rheolef;
int main(int argc, char**argv) {
    environment rheolef(argc, argv); 
    size_t n = (argc < 2) ? 101 : atoi(argv[1]);
    disarray<Float> x(n, 0.0);
    if (x.comm().rank() == 0) {
        for (size_t i = 0; i < x.dis_size(); i++) {
	    x.dis_entry (i) = 1000+i+0.5;
        }
    }
    x.dis_entry_assembly(); 
    dout << x.dis_size() << std::endl
          << x           << std::endl;
}
