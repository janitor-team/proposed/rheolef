///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// test of the cg iterative solver
//
#include "rheolef/linalg.h"
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) { 
  environment rheolef (argc, argv);

  // read the matrix
  csr<Float> a;
  din >> a;

  // set x(dis_i) = dis_i
  vec<Float> x_ex (a.col_ownership());
  for (size_t i = 0; i < x_ex.size(); i++) {
    x_ex [i] = 1 + i + x_ex.ownership().first_index();
  }
  // compute the rhs and solve
  vec<Float> b = a*x_ex ;
  solver_option sopt;
  sopt.tol = std::numeric_limits<Float>::epsilon();
  sopt.max_iter = 1000;
  vec<Float> x (a.col_ownership(), 0);
  int status = cg (a, x, b, eye(), sopt);

  // check the result
  Float err = sqrt(dot (x-x_ex,x-x_ex));
  warning_macro ("error=|x-x_ex|="<< setprecision(15) << err);

  return (err < 1e-10) ? 0 : 1;
}
