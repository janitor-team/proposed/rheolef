///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// test of vec cstor from initializer list (c++ 2011)
//
#include "rheolef/vec_concat.h"
#include "rheolef/csr_concat.h"
#include "rheolef/environment.h"
using namespace rheolef;
using namespace std;
int main (int argc, char **argv) { 
  environment rheolef (argc, argv);
  csr<Float> b;
  din >> b;
  b = trans(b);
  csr<Float> a = trans(b)*b;
  distributor ownership = b.row_ownership();
  size_t n = 4;
  vector<vec<Float> > c (n);
  for (size_t i = 0; i < n; i++) {
    c[i] = vec<Float>(ownership, Float(int(i+1)));
  }
  csr<Float> A = {{ a, trans(b), 0      },
                  { b,   0,    trans(c) },
                  { 0,   c,      0      }};
  dout << A;
}
