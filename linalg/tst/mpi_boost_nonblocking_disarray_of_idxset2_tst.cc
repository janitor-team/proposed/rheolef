///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI

#include "rheolef/communicator.h"
#include "rheolef/dis_macros.h"
#include <boost/mpi.hpp>
#include "rheolef/index_set.h"
using namespace std;
using namespace rheolef;
template<typename ForwardIterator>
std::pair<mpi::status, ForwardIterator> 
my_wait_any(ForwardIterator first, ForwardIterator last)
{
warning_macro ("wait_any...");
  using std::advance;

  BOOST_ASSERT(first != last);
  
  typedef typename std::iterator_traits<ForwardIterator>::difference_type
    difference_type;

  bool all_trivial_requests = true;
  difference_type n = 0;
  ForwardIterator current = first;
  while (true) {
    // Check if we have found a completed request. If so, return it.
    warning_macro ("wait_any[1]...");
    warning_macro ("wait_any[1] current="<<current.operator->());
    // plante ici:
    if (boost::optional<mpi::status> result = current->test()) {
      warning_macro ("wait_any: found");
      return std::make_pair(*result, current);
    }
    // Check if this request (and all others before it) are "trivial"
    // requests, e.g., they can be represented with a single
    // MPI_Request.
    warning_macro ("wait_any[2]");
    all_trivial_requests = 
      all_trivial_requests
      && !current->m_handler 
      && current->m_requests[1] == MPI_REQUEST_NULL;

    // Move to the next request.
    warning_macro ("wait_any[3]");
    ++n;
    if (++current == last) {
      // We have reached the end of the list. If all requests thus far
      // have been trivial, we can call MPI_Waitany directly, because
      // it may be more efficient than our busy-wait semantics.
      warning_macro ("wait_any[4.a] end of list");
      if (all_trivial_requests) {
        warning_macro ("wait_any[4.a.1] trivial");
        std::vector<MPI_Request> requests;
        requests.reserve(n);
        for (current = first; current != last; ++current)
          requests.push_back(current->m_requests[0]);

        // Let MPI wait until one of these operations completes.
        int index;
        mpi::status stat;
        BOOST_MPI_CHECK_RESULT(MPI_Waitany, 
                               (n, &requests[0], &index, &stat.m_status));

        // We don't have a notion of empty requests or status objects,
        // so this is an error.
        if (index == MPI_UNDEFINED)
          boost::throw_exception(mpi::exception("MPI_Waitany", MPI_ERR_REQUEST));

        // Find the iterator corresponding to the completed request.
        current = first;
        advance(current, index);
        current->m_requests[0] = requests[index];
        warning_macro ("wait_any: found2");
        return std::make_pair(stat, current);
      } else {
        warning_macro ("wait_any[4.b] non trivial");
      }
      // There are some nontrivial requests, so we must continue our
      // busy waiting loop.
      n = 0;
      current = first;
      all_trivial_requests = true;
    }
    warning_macro ("wait_any[4.c]");
  }
  // We cannot ever get here
  warning_macro ("should not append");
  BOOST_ASSERT(false);
}
int main(int argc, char** argv) {
  mpi::environment rheolef(argc, argv);
  mpi::communicator world;
  vector<index_set> msg(2);
  int tag = 1;
  vector<index_set> msg_out(2);
  size_t to_proc;
  if (world.rank() == 0) {
    to_proc = 1;
    msg_out[0] += 100;
    msg_out[0] += 200;
    msg_out[0] += 300;
    msg_out[1] += 110;
    msg_out[1] += 210;
    msg_out[1] += 310;
  } else {
    to_proc = 0;
    msg_out[0] += 101;
    msg_out[0] += 201;
    msg_out[0] += 301;
    msg_out[1] += 111;
    msg_out[1] += 211;
    msg_out[1] += 311;
  }
  std::vector<mpi::request> send_reqs(1);
  std::list<mpi::request>   recv_reqs;
  send_reqs[0]          = world.isend(to_proc, tag, msg_out.begin().operator->(), msg_out.size());
  mpi::request recv_req = world.irecv(to_proc, tag, msg.begin().operator->(),     msg.size());
  recv_reqs.push_back(recv_req);
  // wait on recvs:

#undef  HAVE_NO_BUG	// wait_all: ok
#define HAVE_NO_BUG2	// wait_all: ok aalso with the status vector third argument
#undef  HAVE_BUG1 	// wait_any: enter in an infinite loop...

#ifdef HAVE_NO_BUG
  mpi::wait_all(recv_reqs.begin(), recv_reqs.end());
#endif // HAVE_NO_BUG

#ifdef HAVE_NO_BUG2
  std::vector<mpi::status> recv_status(1);
  mpi::wait_all (recv_reqs.begin(), recv_reqs.end(), recv_status.begin());
  for (size_t i = 0; i < recv_status.size(); i++) {
    boost::optional<int> i_msg_size_opt = recv_status[i].count<index_set>();
    check_macro (i_msg_size_opt, "receive wait failed");
    size_t i_msg_size = (size_t)i_msg_size_opt.get();
    warning_macro ("msg_size["<<i<<"] = " << i_msg_size);
  }
#endif // HAVE_NO_BUG2

#ifdef HAVE_BUG1
  while (recv_reqs.size() != 0) {
    std::pair<mpi::status,list<mpi::request>::iterator> pair_status = my_wait_any (recv_reqs.begin(), recv_reqs.end());
    boost::optional<int> i_msg_size_opt = pair_status.first.count<index_set>();
    check_macro (i_msg_size_opt, "receive wait failed");
    size_t i_msg_size = (size_t)i_msg_size_opt.get();
    warning_macro ("msg_size = " << i_msg_size);
  } 
#endif // HAVE_BUG1
  // wait on sends:
  mpi::wait_all(send_reqs.begin(), send_reqs.end());
  // print:
  std::string filename = "mpi_boost_nonblocking_disarray_of_idxset2_tst-" + std::to_string(world.rank());
  std::ofstream out (filename.c_str());
  out << "proc "<<world.rank()<<" : msg[0]=" << msg[0] << std::endl
      << "proc "<<world.rank()<<" : msg[1]=" << msg[1] << std::endl;
  return 0;
}
#endif // _RHEOLEF_HAVE_MPI
