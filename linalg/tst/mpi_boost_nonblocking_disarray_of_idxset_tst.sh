#!/bin/sh
#
# This file is part of Rheolef.
#
# Copyright (C) 2000-2009 Pierre Saramito 
#
# Rheolef is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Rheolef is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rheolef; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# -------------------------------------------------------------------------
SRCDIR=${SRCDIR-"."}
TOP_SRCDIR=${TOP_SRCDIR-"../.."}
NPROC_MAX=${NPROC_MAX-"7"}
. "${TOP_SRCDIR}/config/loop_mpirun.sh"

echo "    skipped (super-setted by mpi_boost_nonblocking_disarray_of_idxset_tst.cc)"
exit 0

status=0

if test "$MPIRUN" = "" || test "$NPROC_MAX" -lt 2; then
  echo "      skipped (requiers np >= 2)"
else
  run "rm -f ./mpi_boost_nonblocking_disarray_of_idxset_tst-[01]"
  run "$MPIRUN -np 2 ./mpi_boost_nonblocking_disarray_of_idxset_tst 2>/dev/null && cat ./mpi_boost_nonblocking_disarray_of_idxset_tst-[01] | diff -w -B $SRCDIR/mpi_boost_nonblocking_disarray_of_idxset_tst.valid -"
  if test $? -ne 0; then status=1; fi
  run "rm -f ./mpi_boost_nonblocking_disarray_of_idxset_tst-[01]"
fi

exit $status
