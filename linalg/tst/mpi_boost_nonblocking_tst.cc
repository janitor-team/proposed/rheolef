///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// from mpi::boost tutorial: http://www.boost.org/doc/libs/1_45_0/doc/html/mpi/tutorial.html
// We have replaced calls to the communicator::send  and communicator::recv  members with similar calls to their non-blocking counterparts, communicator::isend  and communicator::irecv. The prefix i indicates that the operations return immediately with a mpi::request  object, which allows one to query the status of a communication request (see the test  method) or wait until it has completed (see the wait  method). Multiple requests can be completed at the same time with the wait_all operation.

#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include "boost/mpi.hpp"
#pragma GCC diagnostic pop

#include <list>
namespace mpi = boost::mpi;
using namespace std;
int main(int argc, char* argv[]) {
  //cerr << "init mpi..." << endl;
  mpi::environment rheolef(argc, argv);
  //cerr << "init mpi done" << endl;
  mpi::communicator comm;
  size_t n = comm.size();
  int tag = 0;
  vector<double> a_i(n);
  for (size_t j = 0; j < n; j++) {
	a_i[j] = comm.rank()+j+0.5;
  }
  if (comm.rank() == 0) {
    for (size_t j = 0; j < n; j++) {
	 cout << "a(0,"<<j<<") = " << a_i[j] << endl;
    }
    list<mpi::request> reqs;
    vector<double> buffer_aij (n*(n-1));
    for (size_t i = 1; i < n; i++) {
        mpi::request req = comm.irecv(i, tag, buffer_aij.begin().operator->() + (i-1)*n, n);
	reqs.push_back (req);
    }
    for (size_t idx = 1; idx < n; idx++) {
        std::pair<mpi::status, list<mpi::request>::iterator>
		pair_status = mpi::wait_any (reqs.begin(), reqs.end());
	boost::optional<int> i_msg_size_opt = pair_status.first.count<double>();
	if (!i_msg_size_opt) { cerr << "wait failed" << endl; mpi::environment::abort(1); }
    	int i = pair_status.first.source();
	size_t i_msg_size = (size_t)i_msg_size_opt.get();
        cerr << "proc " << comm.rank() << ": receive msg from iproc="<< i <<", size="<<i_msg_size << endl;
	if (i < 0) { cerr << "receive: source iproc = "<<i<<" < 0 !" << endl; mpi::environment::abort(1); }
        for (size_t j = 0; j < n; j++) {
	    cout << "a("<<i<<","<<j<<") = " << buffer_aij[(i-1)*n+j] << endl;
        }
        list<mpi::request>::iterator req_iter = pair_status.second;
        reqs.erase (req_iter);
    } 
  } else {
    mpi::request req = comm.isend(0, tag, a_i.begin().operator->(), a_i.size());
  }
}
#endif // _RHEOLEF_HAVE_MPI
