///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
//
// This code presents "mat-mat" expressions
//
// also print matlab code for non-regression test purpose, i.e.
//
// usage: 
//   gzip -d < in-animal-s2.hb.gz | blas3_tst | octave -q
//
//  machine-precision independent computation:
//   gzip -d < in-animal-s2.hb.gz | blas3_tst -ndigit 12 | octave -q
//
#include "rheolef/linalg.h"
using namespace rheolef;
using namespace std;
#include "rounder2.h"

bool do_trunc = false;

Float tol = 1e-7;

template<class M>
void check (const csr<Float,M>& b, const char* expr, int sub_prec = 1)
{
    static int i = 0;
    dout << "e1=" << expr    << ";\n";
    dout << setbasename("e2") << matlab << csr_apply (rounder_type<Float>(tol),b);

    if (!do_trunc) {
        dout << "error" << ++i << "=norm(e1-e2)\n\n";
    } else {
	if (sub_prec == 1) {
            dout << "error" << ++i << "=eps1*round(norm(e1-e2)/eps1)\n\n";
	} else {
	    dout << "eps2=eps1^(1.0/" << sub_prec << ");\n";
            dout << "error" << ++i << "=eps2*round(norm(e1-e2)/eps2)\n\n";
	}
    }
}
template<class M>
void
run ()
{
    dout << matlab;

    csr<Float,M> a;
    din.is() >> hb;
    din >> a;
    dout << matlab << setbasename("a") << csr_apply (rounder_type<Float>(tol),a);

    // transpose
    csr<Float,M> at = trans(a);
    check (at, "at=a'");

    // addition, substraction
    csr<Float,M> b = a;
    dout << matlab << setbasename("b") << csr_apply (rounder_type<Float>(tol),b);

    csr<Float,M> c;
    c = a+b;
    check (c, "c=a+b");

    c = a-b;
    check (c, "c=a-b");

   // multiplication
   csr<Float,M> c1;
   c1 = at*a;
   check (c1, "c1=a'*a", 2);

   csr<Float,M> c2 = a*at;
   check (c2, "c2=a*a'", 2);

   // note: here, need a copy
   c2 = (a*at)*c2;
   check (c2, "c2=(a*a')*c2", 8);

#ifdef TODO
    // diagonal part
    vec<Float,M> dv(a.ncol());
    int i = 0;
    for (vec<Float,M>::iterator iter = dv.begin(), last = dv.end(); iter != last; ++iter, ++i) {
      *iter = i;
    }
    dia<Float,M> d = diag(a);
    csr<Float,M> da (d);
    check (da, "d=diag(a);");

    // mult with diagonal  
    csr<Float,M> b;
    b = a*d;
    check (b, "b=a*d");
  
    b.left_mult(d);
    check (b, "b=d*b");

    b = d*a;
    check (b, "b=d*a");

    b *= d;
    check (b, "b=b*d");
#endif // TODO
}
// usage: prog -ndigit n {-seq|-dis}
int main (int argc, char* argv[])
{
    environment rheolef (argc, argv);
    if (argc >= 3) {
        // avoid machine-dependent output digits in non-regression mode:
	do_trunc = true;
        int digits10 = atoi(argv[2]);
        tol = pow(Float(10.0),-digits10);
	dout << "eps1=sqrt(10^(-" << digits10 << "));\n";
    }
    communicator comm;
    if ((argc > 3 && string(argv[3]) != "-seq") || comm.size() > 1) {
#ifdef _RHEOLEF_HAVE_MPI
      warning_macro ("distributed...");
      run<distributed>();
#endif // _RHEOLEF_HAVE_MPI
    } else {
      warning_macro ("sequential...");
      run<sequential>();
    }
}
