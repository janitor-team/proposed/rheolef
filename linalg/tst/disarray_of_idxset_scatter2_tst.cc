///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// BUG dans disarray<index_set>::scatter
//  dis_i=880 : x.dis_at(880) = 880 12   734 735 737 738 774 775 882 884 896 898 1114 1116
// est sur iproc=2 et demande' par jproc=0 et 1
//	jproc=0 : recupere x.dis_at(880) = 0
//	jproc=1 : recupere x.dis_at(880) = la bonne valeur
// => BUG !
//
// bizarrement, ce bug n'etait pas apparu jusqu'a present
// est-ce un probleme quand la taille est nulle ?
//
#include "rheolef/config.h"
#ifndef _RHEOLEF_HAVE_MPI
int main() { return 0; }
#else // _RHEOLEF_HAVE_MPI
#include "rheolef/disarray.h"
#include "rheolef/index_set.h"
#include "rheolef/environment.h"
using namespace rheolef;
int main(int argc, char**argv) {
    environment rheolef(argc, argv);
    std::string input = (argc > 1) ? argv[1] : "disarray_of_idxset_scatter2_tst.in";
    communicator comm;
    check_macro (comm.size() == 3, "expect np=3");
    std::ifstream in ((input + std::to_string(comm.rank())).c_str());
    size_t n;
    in >> n;
    distributor ownership (distributor::decide, comm, n);
    disarray<index_set> x (ownership);
    for (size_t i = 0; i < n; i++) {
      size_t dis_i;
      in >> dis_i >> x[i];
    }
    index_set ext_idx_set;
    in >> ext_idx_set;
    in.close();

    std::map<size_t,index_set> ext_idx_map;
    x.get_dis_entry (ext_idx_set, ext_idx_map);
    std::ofstream out (("disarray_of_idxset_scatter2_tst.out" + std::to_string(comm.rank())).c_str());
    for (std::map<size_t,index_set>::const_iterator iter = ext_idx_map.begin(), last = ext_idx_map.end();
	iter != last; iter++) {
	out << (*iter).first << " " << (*iter).second << std::endl;
    } 
    out.close();
}
#endif // _RHEOLEF_HAVE_MPI
