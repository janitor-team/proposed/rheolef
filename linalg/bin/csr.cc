///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// the csr unix command
// author: Pierre.Saramito@imag.fr
// date: 12 may 1997, updated 9 oct 2012

namespace rheolef {
/**
@commandfile csr sparse matrix manipulation 
@addindex command `csr`
@addindex file format `.mtx` sparse matrix

Synopsis
========

    csr [options] file[[.mtx].gz]
  
Description
===========

Plots or manipulates a sparse matrix.
Read from standard input, in the `.mtx` matrix-market format.
Write to standard output in either in `matlab` or `postscript` formats.

Examples
========
Enter commands as:
  
      csr -ps     matrix.mtx | gv -
      csr -matlab matrix.mtx > matrix.m

Output format options
=====================

`-ps`
>    	Print in the `.ps` postscript format.
>       This is the default.

`-mtx`
>   	Print in the `.mtx` format.
>	When the input was not sorted by increasing line and then
>	column order, then, the output is.

@addindex command `octave`
`-matlab`
>   	Print in the `matlab` sparse format (see e.g. `octave`(1)).

`-name` *string*
>    	Specifies a current name for the sparse `matlab` output option.
>	Default is `a`.

Limitations
===========

No render are yet used for the visualization of the sparse matrix:
future developments plane to use `gnuplot`.

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/linalg.h"
using namespace rheolef;
using namespace std;

// ------------------------------------------------------------------------
// print function
// ------------------------------------------------------------------------
template <class T>
struct log10_abs_or_zero {
    T operator()(T x) { return ((x == T(0.)) ? T(0.) : log10(xabs(x))); }
};
template <class T>
void
print_postscript (
  std::ostream& s, 
  const csr<T,sequential>& a)
{
       typedef typename csr<T,sequential>::size_type size_type;

    // scales
        bool logscale = iorheo::getlogscale(s);
        bool color    = iorheo::getcolor(s);

	// title of the drawing
	// const char title [256] = "Sparse Matrix";
	const char title [256] = "";

	// ptitle = position of title; 0 under the drawing, else above
	const  int ptitle = 0;
	
	// size   = size of the drawing (unit =cm) 
	const T size = 15;
	
	// units cm do dot conversion factor and a4 paper size
      	const T paperx = 21.0;
      	const T conv   = 2.54; // cm-per-inch
      	T u2dot        = 72.0/conv;

      	int maxdim = max(a.nrow(), a.ncol());
      	int m = 1 + maxdim;

	// left and right margins (drawing is centered)
	T lrmrgn = (paperx-size)/2.0;

	// bottom margin : 2 cm
	T botmrgn = 2;

	// scaling factor
      	T scfct = size*u2dot/m;

	// matrix frame line witdh
        const T frlw = 0.25;

	// font size for title (cm)
        const T fnstit = 0.5;
        int    ltit = strlen(title);

	// position of title : centered horizontally
	//                     at 1.0 cm vertically over the drawing
        const T ytitof = 1.0;
        T xtit = paperx/2.0;

        T ytit = botmrgn + size*int(a.nrow())/T(m) + T(ytitof);

	// almost exact bounding box
        T xl = lrmrgn*u2dot - scfct*frlw/2;
        T xr = (lrmrgn+size)*u2dot + scfct*frlw/2;
        T yb = botmrgn*u2dot - scfct*frlw/2;
        
	T yt = (botmrgn+size*int(a.nrow())/T(m))*u2dot + scfct*frlw/2.;
        
	if (ltit == 0)
            yt = yt + (ytitof+fnstit*0.70)*u2dot;

	// add some room to bounding box
        const T delt = 10.0;
        xl -= delt;
        xr += delt;
        yb -= delt;
        yt += delt;

	// correction for title under the drawing
        if (ptitle == 0 && ltit > 0) {
            ytit = botmrgn + fnstit*0.3;
            botmrgn = botmrgn + ytitof + fnstit*0.7;
	}
   
   // print header

	s << "%!PS-Adobe-2.0\n";
	s << "%%Creator: sparskit++ 1.0, 1997, Computer Modern Artists\n";
	s << "%%Title: sparskit++ CSR matrix\n";
	s << "%%BoundingBox: " << xl << " " << yb << " " << xr << " " << yt << std::endl;
	s << "%%EndComments\n";
	s << "/cm {72 mul 2.54 div} def\n";
	s << "/mc {72 div 2.54 mul} def\n";
	s << "/pnum { 72 div 2.54 mul 20 string\n";
	s << "cvs print ( ) print} def\n";
	s << "/Cshow {dup stringwidth pop -2 div 0 rmoveto show} def\n";

	// we leave margins etc. in cm so it is easy to modify them if
	// needed by editing the output file
	s << "gsave\n";
        if (ltit > 0)
            s << "/Helvetica findfont " << fnstit << " cm scalefont setfont \n";
        s << xtit << " cm " << ytit << " cm moveto\n";
        s << "(" << title << ") Cshow\n";

        s << lrmrgn << " cm " << botmrgn << " cm translate\n";
        s << size << " cm " << m << " div dup scale\n";

	// draw a frame around the matrix
	s << frlw << " setlinewidth\n";
	s << "newpath\n";
	s << "0 0 moveto\n";
        s <<  a.ncol()+1 << " " <<      0 << " lineto\n";
        s <<  a.ncol()+1 << " " << a.nrow()+1 << " lineto\n";
        s <<       0 << " " << a.nrow()+1 << " lineto\n";
	s << "closepath stroke\n";

	s << "1 1 translate\n";
	s << "0.8 setlinewidth\n";
	s << "/p {moveto 0 -.40 rmoveto \n";
	s << "           0  .80 rlineto stroke} def\n";

#ifdef TODO
	if (color) {

	    // the number of used colors
	    const int ncolor = 100;

	    T max_;
	    T min_;
	    T inf = std::numeric_limits<T>::max();
	    log10_abs_or_zero<T> filter;

	    if (logscale) {
		max_ = select_op_value (a, a+a.nnz(), -inf, std::greater<T>(), filter);
		min_ = select_op_value (a, a+a.nnz(),  inf, std::less<T>(),    filter);
	    } else {
	        max_ = select_max(a, a+a.nnz(), -inf);
	        min_ = select_min(a, a+a.nnz(),  inf);
	    }
	    palette tab = palette(min_, max_, ncolor);
	    tab.postscript_print_color_def (s);
	    
	    // color: plotting loop
            for (Index i = 0; i < a.nrow(); i++)
                for (Index k = ia [i]; k < ia [i+1]; k++) {

		    T value = a[k];
	    	    if (logscale) value = filter(value);
		    int ic = tab.color_index(value);
		    s << "c" << ic << " " 
		      << ja [k]    << " " 
		      << a.nrow()-i-1  << " p\n";
	    }
	} else { 
#endif // TODO
	    // black & white: plotting loop
            typename csr<T,sequential>::const_iterator dia_ia = a.begin();
            for (size_type i = 0, n = a.nrow(); i < n; i++) {
              for (typename csr<T,sequential>::const_data_iterator p = dia_ia[i]; p < dia_ia[i+1]; p++) {
                size_type j = (*p).first;
                s << j << " " << a.nrow()-i-1 << " p\n";
              }
            }
#ifdef TODO
	}
#endif // TODO
	s << "showpage\n";
}
// ------------------------------------------------------------------------
// unix command
// ------------------------------------------------------------------------
void usage()
{
      derr << "csr: usage: csr "
	   << "[-ps|-matlab] "
	   << "[-name string] "
	   << "file[.mtx]"
	   << endl;
      exit (1);
}
int main(int argc, char **argv)
{
    environment rheolef (argc, argv);
    check_macro (communicator().size() == 1, "csr: command may be used as mono-process only");
    if (argc <= 1) usage();
    bool have_file = false;
    typedef enum { mtx, ml, ps} out_format_type;
    out_format_type out_format = ps;
    string filename;
    string name = "a";
    for (int i = 1; i < argc; i++) {
	     if (strcmp (argv[i], "-ps") == 0)              out_format = ps;
	else if (strcmp (argv[i], "-matlab") == 0)          out_format = ml;
	else if (strcmp (argv[i], "-mtx") == 0)             out_format = mtx;
	else if (strcmp (argv[i], "-name") == 0)            {
            if (i == argc-1) { std::cerr << "csr -name: option argument missing" << std::endl; usage(); }
            name = argv[++i];
        }
	else if (strcmp (argv[i], "-") == 0)                filename   = "-";
        else if (argv[i][0] == '-')                         usage();
	else {
		filename = argv[i];
	}
    }
    csr<Float,sequential> a;
    if (filename == "" || filename == "-") {
      din >> a;
    } else {
      idiststream a_in (filename);
      a_in >> a;
    }
    if (out_format == ml) {
	dout << matlab << setbasename(name) << a;
    } else if (out_format == ps) {
        print_postscript (cout, a);
    } else if (out_format == mtx) {
        dout << a;
    }
#ifdef TODO
    //
    // set default options
    //
    cout << hb << setbasename("a");

    bool get_rhs  = false;

    //
    // get input options
    //
    for (int i = 1; i < argc; i++) {
	     if (strcmp (argv[i], "-transpose-in") == 0)    cin >> transpose;
	else if (strcmp (argv[i], "-notranspose-in") == 0)  cin >> notranspose;
        else if (strcmp (argv[i], "-input-hb") == 0)        cin >> hb;
        else if (strcmp (argv[i], "-input-mm") == 0)        cin >> matrix_market;
    }
    //
    // input matrix
    //
    csr<Float> a;
    cin >> a;
    //
    // get options
    //
    for (int i = 1; i < argc; i++) {
      
             if (strcmp (argv[i], "-hb") == 0)              cout << hb;
        else if (strcmp (argv[i], "-mm") == 0)              cout << matrix_market;
        else if (strcmp (argv[i], "-ml") == 0)              cout << ml;
        else if (strcmp (argv[i], "-matlab") == 0)          cout << matlab;
        else if (strcmp (argv[i], "-sparse-matlab") == 0)   cout << sparse_matlab;
        else if (strcmp (argv[i], "-ps") == 0)              cout << ps;
        else if (strcmp (argv[i], "-dump") == 0)            cout << dump;

	else if (strcmp (argv[i], "-tril") == 0 ||
		 strcmp (argv[i], "-triu") == 0)  {
	    int k = 0;
	    int io = i;
	    if (i+1 < argc) {
	        if (isdigit(argv[i+1][0]))
		    k = atoi(argv[++i]);
                else if (argv[i+1][0] == '-' && 
		    isdigit(argv[i+1][1]))
		    k = - atoi(argv[++i]+1);
            }
	    if (strcmp (argv[io], "-tril") == 0)            a = tril(a,k);
            else                                            a = triu(a,k);
        }
	else if (strcmp (argv[i], "-gibbs") == 0) {
	    						    permutation p = gibbs(a);
	    						    a = perm(a, p, p);   
	}
        else if (strcmp (argv[i], "-rhs") == 0) { 
							    cout << hb; 
							    get_rhs = true; 
	}
        else if (strcmp (argv[i], "-logscale") == 0)        cout << logscale;
        else if (strcmp (argv[i], "-nologscale") == 0)      cout << nologscale;
        else if (strcmp (argv[i], "-color") == 0)           cout << color;
        else if (strcmp (argv[i], "-black-and-white") == 0) cout << black_and_white;

	else if (strcmp (argv[i], "-transpose-out") == 0)   cout << transpose;
	else if (strcmp (argv[i], "-notranspose-out") == 0) cout << notranspose;
	else if (strcmp (argv[i], "-name") == 0) {
		if (++i >= argc) {
		    cerr << "-name: missing argument" << endl; 
		    usage();
		}
		cout << setbasename (argv[i]);
	}
	// skit input options
	else if (strcmp (argv[i], "-transpose-in") == 0)    ;
	else if (strcmp (argv[i], "-notranspose-in") == 0)  ;
        else if (strcmp (argv[i], "-input-hb") == 0)        ;
        else if (strcmp (argv[i], "-input-mm") == 0)        ;

        else {
	    cerr << "unexpected command-line argument `" << argv[i] << "'" << endl; 
	    usage();
        }
    }
    //
    // output matrix
    //
    if (!get_rhs) {
    
	// simple output
	cout << a;

    } else {
    
	unsigned int n = iorheo::getnrhs(cin);

	//      Here, we get the number of right-hand-sides.
	//      Next, get the type: has guess and/or exact solution:

        string rhs_type = iorheo::getrhstype(cin);
    
        //      and start to write.
        //      We specify first in header the number of right-hand-side,
        //      and then output the matrix.
    
        cout << hb << setnrhs(n) << setrhstype(rhs_type) << notranspose << a ;
    
        //      A first loop for optional rhs:

        for (unsigned int i = 0; i < n; i++) {
            vec<Float> b;
            cin  >> b;
            cout << b;
        }
        //
        //      Test if guess vectors is supplied:
        //
        if (rhs_type[1] == 'G') {
            vec<Float> x0;
            for (unsigned int i = 0; i < n; i++) {
                cin  >> x0;
                cout << x0;
            }
        }
        //
        //	    and finally, check if an exact solution vector is supplied
        //
        if (rhs_type[2] == 'X') {
            vec<Float> x;
            for (unsigned int i = 0; i < n; i++) {
                cin  >> x;
                cout << x;
            }
        }
    }
#endif // TODO
}
