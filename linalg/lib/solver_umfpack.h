#ifndef _RHEOLEF_SOLVER_UMFPACK_H
#define _RHEOLEF_SOLVER_UMFPACK_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// solver implementation: interface
//

#include "rheolef/config.h"

#ifdef _RHEOLEF_HAVE_UMFPACK

#include "rheolef/solver.h"
extern "C" {
#include <umfpack.h>
}
namespace rheolef {

// =======================================================================
// rep
// =======================================================================
template<class T, class M>
class solver_umfpack_rep : public solver_abstract_rep<T,M> {
public:
// typedef:

  typedef solver_abstract_rep<T,M>        base;
  typedef typename base::size_type        size_type;
  typedef typename base::determinant_type determinant_type;

// allocator:

  solver_umfpack_rep();
  explicit solver_umfpack_rep (const csr<T,M>& a, const solver_option& opt = solver_option());
  solver_abstract_rep<T,M>* clone() const;
  bool initialized() const { return true; }
  void update_values (const csr<T,M>& a);
  ~solver_umfpack_rep ();

// accessors:

  vec<T,M> trans_solve (const vec<T,M>& rhs) const;
  vec<T,M> solve       (const vec<T,M>& rhs) const;
  determinant_type det() const { return _det; }

protected:
// data:
  int                _n;
  void*              _numeric;
  int*               _ia_p;
  int*               _ja_p;
  T*                 _a_p; // TODO: only used when T=double yet
  determinant_type   _det;
  double             _control [UMFPACK_INFO];
  mutable double     _info    [UMFPACK_INFO];

// internal:
  void _set_control();
  void _destroy();
  void _solve (int transpose_flag, const vec<T,M>& b, vec<T,M>& x) const;
private:
  solver_umfpack_rep (const solver_umfpack_rep<T,M>&);
  solver_umfpack_rep<T,M>& operator= (const solver_umfpack_rep<T,M>&);
};
template <class T, class M>
inline
solver_abstract_rep<T,M>*
solver_umfpack_rep<T,M>::clone() const
{
  typedef solver_umfpack_rep<T,M> rep;
  return new_macro (rep(*this));
}

} // namespace rheolef
#endif // _RHEOLEF_HAVE_UMFPACK
#endif // _RHEOLEF_SOLVER_UMFPACK_H
