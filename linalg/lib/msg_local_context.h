#ifndef RHEO_MSG_LOCAL_CONTEXT_H
#define RHEO_MSG_LOCAL_CONTEXT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

# include "rheolef/msg_util.h"
namespace rheolef {

/*F:
NAME: msg_local_context -- receive pattern (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Computes the receive compresed message local pattern,
  i.e. the only part that does not requires communication.
  (see "msg_to_context"(5)).
ALGORITHM:
  msg_local_context

  "input": the index sets and the local processor index range
  |   idx(0:nidx-1), idy(0:nidx-1), istart, ilast
  "output": the send and receive local contexts (to, from)
  |   to_loc_idx(0:n_local-1), from_loc_idy(0:n_local-1)
  begin
  |     if n_local <> 0 then
  |       iloc := 0
  |       for k := 0 to nidx-1 do
  |         if idy(k) in (istart, ilast( then
  |           to_loc_idx(iloc)   := idy(k) - istart
  |           from_loc_idy(iloc) := idy(k)
  |           iloc := iloc + 1
  |         endif
  |       endfor
  |     endif
  end

COMPLEXITY:
  Memory and time complexity is O(receive_total_size).
SEE ALSO: "msg_to_context"(5)

METHODS: @msg_local_context 
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   23 march 1999
END:
*/

//<msg_local_context:
template <
    class InputIterator1,
    class InputIterator2,
    class Size,
    class OutputIterator1,
    class OutputIterator2>
void
msg_local_context (
    InputIterator1 		idx,		// nidx
    InputIterator1 		last_idx,	
    InputIterator2 		idy,		// nidx
    Size      			idy_maxval,
    Size      			istart,
    Size      			ilast,
    OutputIterator1    		to_loc_idx,		// n_local
    OutputIterator1    		last_to_loc_idx,
    OutputIterator2    		from_loc_idy)		// n_local
{
    if (to_loc_idx == last_to_loc_idx) {
        return;
    }
    while (idx != last_idx) {
	Size idx_i = *idx;
	if (idx_i >= istart && idx_i < ilast) {
	    Size idy_i = *idy;
            assert_macro (idy_i < idy_maxval, "Scattering past end of TO vector");
	    (*to_loc_idx++)   = idx_i - istart;
	    (*from_loc_idy++) = idy_i;
	}
        ++idx;
        ++idy;
    }
}
//>msg_local_context:
} // namespace rheolef
#endif // RHEO_MSG_LOCAL_CONTEXT_H
