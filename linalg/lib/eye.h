# ifndef _RHEOLEF_EYE_H
# define _RHEOLEF_EYE_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   28 january 1997

namespace rheolef {
/**
@linalgfunctionfile eye identity matrix and preconditionner

Description
===========
Following e.g. `octave`, `eye` denotes the identity matrix.
Here, `eye()` is a function call that returns the identity preconditionner
as a @ref solver_4.

The dimensions of `eye()` are determined by the context.
This class is useful in the context of preconditionner interfaces:
it allows one to specify a do-nothing preconditionner.
See e.g. @ref cg_5 for an example.
 
Implementation
==============
@showfromfile
*/
} // namespace rheolef

# include "rheolef/vec.h"
# include "rheolef/solver.h"

namespace rheolef { 

template<class T, class M = rheo_default_memory_model>
class solver_eye_rep: public solver_abstract_rep<T,M> {
public:
  explicit solver_eye_rep (const solver_option& opt = solver_option());
  solver_abstract_rep<T,M>* clone() const;
  bool good() const { return true; }
  void update_values (const csr<T,M>&) {}
  vec<T,M> operator*   (const vec<T,M>& x) const { return x; }
  vec<T,M> solve       (const vec<T,M>& x) const { return x; }
  vec<T,M> trans_solve (const vec<T,M>& x) const { return x; }
};

template <class T, class M = rheo_default_memory_model>
solver_basic<T,M> eye_basic();

//! @brief see the @ref eye_5 page for the full documentation
inline solver_basic<Float> eye() { return eye_basic<Float>(); }

template<class T, class M>
inline
solver_eye_rep<T,M>::solver_eye_rep (const solver_option& opt)
 : solver_abstract_rep<T,M>(opt)
{
}
template <class T, class M>
inline
solver_abstract_rep<T,M>*
solver_eye_rep<T,M>::clone() const
{
  typedef solver_eye_rep<T,M> rep;
  return new_macro (rep(*this));
}
template <class T, class M>
inline
solver_basic<T,M>
eye_basic ()
{
  using rep = solver_eye_rep<T,M>;
  solver_basic<T,M> p;
  p.solver_basic<T,M>::base::operator= (new_macro(rep()));
  return p;
}

}// namespace rheolef
# endif // _RHEOLEF_EYE_H
