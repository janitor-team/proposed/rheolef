# ifndef _RHEOLEF_LINALG_H
# define _RHEOLEF_LINALG_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

// include all for convenience
#include "rheolef/iorheo.h"
#include "rheolef/diststream.h"
#include "rheolef/communicator.h"
#include "rheolef/dis_cpu_time.h"
#include "rheolef/dis_memory_usage.h"
#include "rheolef/vec.h"
#include "rheolef/vec_expr_v2.h"
#include "rheolef/vec_concat.h"
#include "rheolef/vec_range.h"
#include "rheolef/csr.h"
#include "rheolef/csr_vec_expr.h"
#include "rheolef/csr_concat.h"
#include "rheolef/dis_inner_product.h"
#include "rheolef/solver.h"
#include "rheolef/solver_abtb.h"
#include "rheolef/cg.h"
#include "rheolef/gmres.h"
#include "rheolef/eye.h"
#include "rheolef/dia.h"
#include "rheolef/diag.h"
#include "rheolef/mic.h"
#include "rheolef/ilut.h"
#include "rheolef/environment.h"
#endif // _RHEOLEF_LINALG_H
