# ifndef _RHEOLEF_DISTSTREAM_H
# define _RHEOLEF_DISTSTREAM_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHORS: Pierre.Saramito@imag.fr
// DATE:   31 october 1997 ; 13 nov 1998

namespace rheolef {
/**
@classfile diststream i/o streams in distributed environment
@addindex RHEOPATH environment variable
@addindex file `.gz` gziped

Description
===========
The `idiststream` and `odiststream`
classes provide a stream input and output interface
for parallel and distributed codes.
The main difference 
Recall that, with the usual `std::istream` and `std::ostream`,
any i/o operation are executed on all processes:
e.g. the output is printed many times.
In contrast, `idiststream` and `odiststream`
manage nicely the distributed environment.

For small data, e.g. `int`, `double` or `string`,
a specific processor is selected for i/o operations.
For large data, the i/o operations are delegated
to a specific class member functions when available.

For convenience, the standard streams `cin`, `cout`, `clog` and `cerr`
are extended as `din`, `dout`, `dlog` and `derr`, respectively.

File suffixes
=============
Finally, optional suffix extension to the file name is 
automatically handled:

        odiststream foo("NAME", "suffix");
  
is similar

        ofstream foo("NAME.suffix").

Conversely,

 	irheostream foo("NAME","suffix");

is similar to
  
	ifstream foo("NAME.suffix").
 
File search
===========
Recursive search in a directory list is provided for finding the input file.
However, we look at a search path environment variable 
`RHEOPATH` in order to find `NAME` while the suffix is assumed.
Moreover, `gzip` compressed files, ending
with the `.gz` suffix are handled, and decompression is done
automatically on the fly in that case.

Compression
===========
File compression/decompresion is handled on the fly,
thanks to the `gzip` library.
The data compression is assumed by default for output:
it can be deactivated while opening a file by an optional argument:

        odiststream foo("NAME", "suffix", io::nogz);
  
Append mode
===========
An existing file, possibly compressed, can be reopen in `append` mode.
New results will be appended at the end of an existing file:

        odiststream foo("NAME", "suffix", io::app);

Flush
=====
The `flush` member function is nicely handled in compression mode:
this feature allows intermediate results to be available during long computations.
Buffers are flushed and data are available in the output file.

Implementation
==============
@showfromfile
@snippet diststream.h verbatim_idiststream
@snippet diststream.h verbatim_idiststream_cont
@snippet diststream.h verbatim_odiststream
@snippet diststream.h verbatim_odiststream_cont
*/
} // namespace rheolef

#include "rheolef/communicator.h"
#include "rheolef/dis_macros.h"
#include "rheolef/catchmark.h"

// to propagate operator<< from ostream& to odiststream&
// TODO: do better ? --> yes: dot it in this order in "linalg.h" & "rheolef.h"
#include <iomanip> // dout << std::setprecision()
#ifdef TO_CLEAN
#include "rheolef/iorheo.h" // dout << setbasename()
#include "rheolef/point.h" // dout << point()
#include "rheolef/tensor.h" 
#include "rheolef/tensor3.h" 
#include "rheolef/tensor4.h" 
#endif // TO_CLEAN

namespace rheolef {

//! @brief odiststream: see the @ref diststream_2 page for the full documentation
// [verbatim_odiststream]
class odiststream {
public:
  typedef std::size_t size_type;

// allocators/deallocators:

  odiststream();
  odiststream (std::string filename, std::string suffix = "",
              io::mode_type mode = io::out, const communicator& comm = communicator());
  odiststream (std::string filename,
              io::mode_type mode, const communicator& comm = communicator());
  odiststream (std::string filename, std::string suffix, const communicator& comm);
  odiststream (std::string filename, const communicator& comm);
  odiststream(std::ostream& os, const communicator& comm = communicator());
  ~odiststream();

// modifiers:

   void open (std::string filename, std::string suffix = "",
	     io::mode_type mode = io::out, const communicator& comm = communicator());
   void open (std::string filename, 
	     io::mode_type mode, const communicator& comm = communicator());
   void open (std::string filename, std::string suffix,
	     const communicator& comm);
   void open (std::string filename, const communicator& comm);
   void flush();
   void close();

// accessors:

   const communicator& comm() const { return _comm; }
   bool good() const;
   operator bool() const { return good(); }
   static size_type io_proc();
// [verbatim_odiststream]

// internals:

   std::ostream& os();
   bool nop();

protected:
// data:
   std::ostream* _ptr_os;
   bool          _use_alloc;
   communicator  _comm;
private:
   odiststream(const odiststream&);
   odiststream& operator= (const odiststream&);
// [verbatim_odiststream_cont]
};
// [verbatim_odiststream_cont]

// ------------------------------------------------------------------
// inlined
// ------------------------------------------------------------------
inline
odiststream::odiststream()
 : _ptr_os(0), _use_alloc(false), _comm()
{
}
inline
odiststream::odiststream (std::string filename, std::string suffix, io::mode_type mode, const communicator& comm)
 : _ptr_os(0), _use_alloc(false), _comm()
{
  open (filename, suffix, mode, comm);
}
inline
odiststream::odiststream (std::string filename, io::mode_type mode, const communicator& comm)
 : _ptr_os(0), _use_alloc(false), _comm()
{
  open (filename, mode, comm);
}
inline
odiststream::odiststream (std::string filename, std::string suffix, const communicator& comm)
 : _ptr_os(0), _use_alloc(false), _comm()
{
  open (filename, suffix, comm);
}
inline
odiststream::odiststream (std::string filename, const communicator& comm)
 : _ptr_os(0), _use_alloc(false), _comm()
{
  open (filename, comm);
}
inline
odiststream::odiststream(std::ostream& os, const communicator& comm)
 : _ptr_os(&os), _use_alloc(false), _comm(comm)
{
}
inline
void
odiststream::open (std::string filename, io::mode_type mode, const communicator& comm)
{
  open (filename, std::string(""), mode, comm);
}
inline
void 
odiststream::open (std::string filename, std::string suffix, const communicator& comm)
{
  open (filename, suffix, io::out, comm);
}
inline
void
odiststream::open (std::string filename, const communicator& comm)
{
  open (filename, std::string(""), io::out, comm);
}
inline
std::ostream&
odiststream::os() {
  check_macro (_ptr_os != 0, "try to use an uninitialized odiststream");
  return *_ptr_os;
}
#ifndef _RHEOLEF_HAVE_MPI
inline bool odiststream::nop() { return false; }
#else
inline bool odiststream::nop() { return size_type(_comm.rank()) != io_proc(); }
#endif //_RHEOLEF_HAVE_MPI

// ------------------------------------------------------------------
// standard i/o:
// ------------------------------------------------------------------
# define _RHEOLEF_define_sequential_odiststream_raw_macro(arg) 	\
    inline 						\
    odiststream&					\
    operator << (odiststream& s, arg) {			\
        if (s.nop()) return s;				\
	s.os() << x; 					\
	return s;					\
    }
# define _RHEOLEF_define_sequential_odiststream_macro(T) 	\
         _RHEOLEF_define_sequential_odiststream_raw_macro(const T& x)

_RHEOLEF_define_sequential_odiststream_macro(char)
_RHEOLEF_define_sequential_odiststream_macro(int)
_RHEOLEF_define_sequential_odiststream_macro(unsigned int)
_RHEOLEF_define_sequential_odiststream_macro(long int)
_RHEOLEF_define_sequential_odiststream_macro(long unsigned int)
_RHEOLEF_define_sequential_odiststream_macro(float)
_RHEOLEF_define_sequential_odiststream_macro(double)
_RHEOLEF_define_sequential_odiststream_macro(long double)
_RHEOLEF_define_sequential_odiststream_macro(char*const)
_RHEOLEF_define_sequential_odiststream_macro(std::string)
#ifdef _RHEOLEF_HAVE_FLOAT128
_RHEOLEF_define_sequential_odiststream_macro(boost::multiprecision::float128)
#endif // _RHEOLEF_HAVE_FLOAT128 
_RHEOLEF_define_sequential_odiststream_raw_macro(char *x)
_RHEOLEF_define_sequential_odiststream_raw_macro(std::ostream& (*x)(std::ostream&))
#undef _RHEOLEF_define_sequential_odiststream_macro

// output all small objects that have an
//   ostream& operator<< 
// IMPLEMENTATION NOTE:
//   the ostream& operator<< should have been already known here
//   thus, have to #include "point.h" at the top of this file
//   otherwise it fails: any other solution 
namespace details {
template<class T, class Sfinae = void>
struct is_omanip : std::false_type {};
// const T& version : for rheolef::setbasename & such
template<class T>
struct is_omanip <T,typename std::enable_if<
  std::is_pointer<decltype(static_cast<std::ostream& (*)(std::ostream&, const T&)>
     (operator<<))>::value>::type>
  : std::true_type {};
// T version : for std::setprecision & such, but fails (see below)
// note: could not be merged with the previous enable_if
//       otherwise it fails
template<class T>
struct is_omanip <T,typename std::enable_if<
  std::is_pointer<decltype(static_cast<std::ostream& (*)(std::ostream&, T)>
    (operator<<))>::value>::type>
  : std::true_type {};

// explicit version for std::setprecision (otherwise it fails)
// TODO: how to have a more implicit way ? for all std::iomanips ?
template<>
struct is_omanip <typename std::function<decltype(std::setprecision)>::result_type>
  : std::true_type {};

}// namespace details

template <class T>
typename std::enable_if<
  details::is_omanip<T>::value
 ,odiststream&
>::type
operator<< (odiststream& s, const T& x)
{
    if (s.nop()) return s;
    s.os() << x;
    return s;
}

// =============================================================================

//! @brief idiststream: see the @ref diststream_2 page for the full documentation
// [verbatim_idiststream]
class idiststream {
public:
  typedef std::size_t size_type;

// allocators/deallocators:
     
  idiststream();
  idiststream (std::istream& is, const communicator& comm = communicator());
  idiststream (std::string filename, std::string suffix = "",
             const communicator& comm = communicator());
  ~idiststream();

// modifiers:

  void open (std::string filename, std::string suffix = "",
             const communicator& comm = communicator());
  void close();

// accessors:

  const communicator& comm() const { return _comm; }
  bool good() const;
  operator bool() const { return good(); }
  static size_type io_proc();
// [verbatim_idiststream]

// internals:

  std::istream& is();
  bool nop();
  bool do_load();

protected:
// data:
  std::istream* _ptr_is;
  bool          _use_alloc;
  communicator  _comm;
private:
   idiststream(const idiststream&);
   idiststream& operator= (const idiststream&);
// [verbatim_idiststream_cont]
};
// [verbatim_idiststream_cont]
// ------------------------------------------------------------------
// inlined
// ------------------------------------------------------------------
inline
idiststream::idiststream()
 : _ptr_is(0), _use_alloc(false), _comm()
{
}
inline
idiststream::idiststream (std::istream& is, const communicator& comm)
 : _ptr_is(&is), _use_alloc(false), _comm(comm)
{
}
inline
idiststream::idiststream (std::string filename, std::string suffix, const communicator& comm)
 : _ptr_is(0), _use_alloc(false), _comm()
{
  open (filename, suffix, comm);
}
inline 
std::istream&
idiststream::is()
{
  check_macro (_ptr_is != 0, "try to use an uninitialized idiststream");
  return *_ptr_is;
}
#ifndef _RHEOLEF_HAVE_MPI
inline bool idiststream::nop()     { return false; }
inline bool idiststream::do_load() { return true; }
#else
inline bool idiststream::nop()     { return size_type(_comm.rank()) != io_proc(); }
inline bool idiststream::do_load() { return size_type(_comm.rank()) == io_proc(); }
#endif //_RHEOLEF_HAVE_MPI

// ------------------------------------------------------------------
// standard i/o:
// ------------------------------------------------------------------
#ifdef _RHEOLEF_HAVE_MPI
# define _RHEOLEF_define_sequential_idiststream_macro(T) \
inline							\
idiststream&						\
operator>> (idiststream& s, T& x)			\
{							\
  if (s.do_load()) { (s.is()) >> x; }			\
  mpi::broadcast (mpi::communicator(), x, s.io_proc());	\
  return s;						\
}
#else // _RHEOLEF_HAVE_MPI
# define _RHEOLEF_define_sequential_idiststream_macro(T) \
inline							\
idiststream&						\
operator>> (idiststream& s, T& x)			\
{							\
  (s.is()) >> x; 					\
  return s;						\
}
#endif // _RHEOLEF_HAVE_MPI

_RHEOLEF_define_sequential_idiststream_macro(char)
_RHEOLEF_define_sequential_idiststream_macro(int)
_RHEOLEF_define_sequential_idiststream_macro(long int)
_RHEOLEF_define_sequential_idiststream_macro(unsigned int)
_RHEOLEF_define_sequential_idiststream_macro(long unsigned int)
_RHEOLEF_define_sequential_idiststream_macro(float)
_RHEOLEF_define_sequential_idiststream_macro(double)
_RHEOLEF_define_sequential_idiststream_macro(long double)
_RHEOLEF_define_sequential_idiststream_macro(std::string)
#ifdef _RHEOLEF_HAVE_FLOAT128
_RHEOLEF_define_sequential_idiststream_macro(boost::multiprecision::float128)
#endif // _RHEOLEF_HAVE_FLOAT128 

#undef _RHEOLEF_define_sequential_idiststream_macro

// iomanips such as: ids << noverbose
inline							
idiststream&						
operator>> (idiststream& s, std::istream& (*x)(std::istream&))
{							
  s.is() >> x; 						
  return s;						
}

// predefined distributed streams
//! @var din
//! @brief see the @ref diststream_2 page for the full documentation
extern idiststream din;
//! @var dout
//! @brief see the @ref diststream_2 page for the full documentation
extern odiststream dout;
//! @var dlog
//! @brief see the @ref diststream_2 page for the full documentation
extern odiststream dlog;
//! @var derr
//! @brief see the @ref diststream_2 page for the full documentation
extern odiststream derr;

bool dis_scatch (idiststream& ips, const communicator& comm, std::string ch);

inline
bool dis_scatch (idiststream& ips, std::string ch)
{
  return dis_scatch (ips, ips.comm(), ch);
}
inline
idiststream&
operator>> (idiststream& ids, const catchmark& m)
{
  if (ids.nop()) return ids;
  ids.is() >> setmark(m.mark());
  std::string label = "#" + m.mark();
  if (!scatch(ids.is(),label)) {
    bool verbose  = iorheo::getverbose(ids.is());
    if (verbose) warning_macro ("catchmark: label `"<< label <<"' not found on input");
  }
  return ids;
}
inline
odiststream&
operator<< (odiststream& ods, const catchmark& m)
{
    if (ods.nop()) return ods;
    ods.os() << setmark(m.mark())
             << "#" << m.mark() << std::endl;
    return ods;
}
// system utilities:
int dis_system (const std::string& command, const communicator& comm = communicator());
bool dis_file_exists (const std::string& filename, const communicator& comm = communicator());


} // namespace rheolef
# endif // _RHEOLEF_DISTSTREAM_H
