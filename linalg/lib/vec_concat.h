#ifndef _RHEOLEF_VEC_CONCAT_H
#define _RHEOLEF_VEC_CONCAT_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// build vec from initializer lists
//
#include "rheolef/vec.h"

namespace rheolef { namespace details {

// ----------------------------------------------------------------------------
// 1) implementation
// ----------------------------------------------------------------------------

//! @brief scalar constraints are attributed to this process:
static int constraint_process_rank (const communicator& comm = communicator()) { return 0; }

template <class T, class M>
class vec_concat_value {
public:
// typedef:
   typedef enum { scalar, scalars, vector} variant_type;
// allocators:
   template <class U,
            class Sfinae
                  = typename std::enable_if<
                      is_rheolef_arithmetic<U>::value
                     ,void
                    >::type
           >
  vec_concat_value (const U& x)              : s(x), ss(),  v(),  variant(scalar) {}
  vec_concat_value (const std::vector<T>& x) : s(),  ss(x), v(),  variant(scalars) {}
  vec_concat_value (const vec<T,M>& x)       : s(),  ss(),  v(x), variant(vector) {}
// data:
public:
  T              s;
  std::vector<T> ss;
  vec<T,M>       v;
  variant_type variant;
};

template <class T, class M>
class vec_concat {
public:
// typedef:
  typedef typename vec<T,M>::size_type   size_type;
  typedef vec_concat_value<T,M>          value_type;

// allocators:

  vec_concat () : _l() {}
  vec<T,M> build_vec() const;

  vec_concat (const std::initializer_list<value_type>& il) : _l() {
    typedef typename std::initializer_list<value_type>::const_iterator const_iterator;
    for (const_iterator iter = il.begin(); iter != il.end(); ++iter) {
        _l.push_back(*iter);
    }
  }

// data:
protected:
  std::list<value_type> _l;
};
} // namespace details

// ----------------------------------------------------------------------------
// 2) interface with the vec<T,M> class
// ----------------------------------------------------------------------------
template <class T, class M>
inline
int
vec<T,M>::constraint_process_rank() const
{
  return details::constraint_process_rank (base::comm());
}
template <class T, class M>
inline
vec<T,M>::vec (const std::initializer_list<details::vec_concat_value<T,M> >& init_list)
{
  details::vec_concat<T,M> vc (init_list);
  vec<T,M>::operator= (vc.build_vec());
}
template <class T, class M>
inline
vec<T,M>&
vec<T,M>::operator= (const std::initializer_list<details::vec_concat_value<T,M> >& init_list)
{
  details::vec_concat<T,M> vc (init_list);
  vec<T,M>::operator= (vc.build_vec());
  return *this;
}

} // namespace rheolef
#endif // _RHEOLEF_VEC_CONCAT_H
