#ifndef _RHEOLEF_MIC_H
#define _RHEOLEF_MIC_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   28 february 2018

namespace rheolef {
/**
@linalgfunctionfile mic modified incomplete Cholesky factorization preconditionner 

Synopsis
========

        solver pa = mic(a);

Description
===========
`mic` is a function that returns the modified incomplete Cholesky
factorization preconditioner as a @ref solver_4.
The method is described in

        C-J. Lin and J. J. More,
        Incomplete Cholesky factorizations with limited memory,
        SIAM J. Sci. Comput. 21(1), pp. 24-45, 1999

It performs the following incomplete factorization:
`S P A P^T S` that approximates `L L^T`
where `L` is a lower triangular factor,
`S` is a diagonal scaling matrix,
and `P` is a fill-in reducing permutation as computed
by the `AMDcol` ordering method.

Options
=======
This preconditioner supports an option related to the shifting strategy: 
let `B = S P A P^T S` be the scaled matrix on which the factorization is carried out,
and `beta` be the minimum value of the diagonal.
If `beta > 0` then, the factorization is directly performed on the matrix `B`.
Otherwise, the factorization is performed on the shifted matrix
`B + (shift+|beta|I` where `shift` is the provided option.
The default value is `shift = 0.001`.
If the factorization fails, then the shift is doubled until it succeed
or a maximum of ten is reached.
If it still fails, it is better to use another preconditioning technique.

        Float shift = 1e-3;
        solver pa = mic (a,shift);

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/solver.h"
#ifdef _RHEOLEF_HAVE_EIGEN
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#include <Eigen/Sparse>
#pragma GCC diagnostic pop

namespace rheolef {

// =======================================================================
// rep
// =======================================================================
template<class T, class M>
class solver_mic_rep : public solver_abstract_rep<T,M> {
public:
// typedef:

  typedef solver_abstract_rep<T,M>        base;
  typedef typename base::size_type        size_type;
  typedef typename base::determinant_type determinant_type;

// allocator:

  explicit solver_mic_rep (const csr<T,M>& a, const T& shift);
  solver_mic_rep (const solver_mic_rep&);
  solver_abstract_rep<T,M>* clone() const;
  bool good() const { return true; }
  void update_values (const csr<T,M>& a);

// accessors:

  vec<T,M> trans_solve (const vec<T,M>& rhs) const;
  vec<T,M> solve       (const vec<T,M>& rhs) const;

protected:
// data:
  csr<T,M>           _a;
  T                  _shift;
  Eigen::IncompleteCholesky<T, Eigen::Lower, Eigen::AMDOrdering<int> > _mic_a;
};
// -----------------------------------------------------------------------------
// inlined
// -----------------------------------------------------------------------------
template<class T, class M>
inline
solver_mic_rep<T,M>::solver_mic_rep (const csr<T,M>& a, const T& shift)
 : solver_abstract_rep<T,M>(solver_option()),
   _a(a),
   _shift(shift),
   _mic_a()
{
  update_values (a);
}
template<class T, class M>
inline
solver_mic_rep<T,M>::solver_mic_rep (const solver_mic_rep<T,M>& x)
 : solver_abstract_rep<T,M>(x.option()),
   _a(x._a),
   _shift(x._shift),
   _mic_a()
{
  // Eigen::IncompleteCholesky copy cstor is non-copyable, so re-initialize for a copy
  update_values (_a);
}
template <class T, class M>
inline
solver_abstract_rep<T,M>*
solver_mic_rep<T,M>::clone() const
{
  typedef solver_mic_rep<T,M> rep;
  return new_macro (rep(*this));
}
// =======================================================================
// preconditioner interface
// =======================================================================
//<verbatim:
template <class T, class M>
solver_basic<T,M> mic (
  const csr<T,M>& a,
  const T &       shift    = 1e-3);
//>verbatim:

template <class T, class M>
inline
solver_basic<T,M> mic (const csr<T,M>& a, const T& shift)
{
  using rep = solver_mic_rep<T,M>;
  solver_basic<T,M> p;
  p.solver_basic<T,M>::base::operator= (new_macro(rep(a,shift)));
  return p;
}

} // namespace rheolef
#endif // _RHEOLEF_HAVE_EIGEN
#endif // _RHEOLEF_MIC_H
