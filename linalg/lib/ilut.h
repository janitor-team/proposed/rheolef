#ifndef _RHEOLEF_ILUT_H
#define _RHEOLEF_ILUT_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   28 february 2018

namespace rheolef {
/**
@linalgfunctionfile ilut incomplete LU factorization preconditionner 

Synopsis
========

        solver pa = ilut(a);

Description
===========
`ilut` is a function that returns the dual threshold incomplete LU factorization preconditionner
of its argument as a @ref solver_4.
The method is described in

        Yousef Saad,
        ILUT: a dual threshold incomplete LU factorization,
        Numer. Lin. Algebra Appl., 1(4), pp 387-402, 1994.

Options
=======
During the factorization, two dropping rules are used and
`ilut` supports two options:

`drop_tol` (float)
>	Any element whose magnitude is less than some tolerance is dropped.
>	This tolerance is obtained by multiplying the option tolerance `drop_tol` 
>	by the average magnitude of all the original elements in the current row.
>	By default, `drop_tol` is `1000*epsilon` where `epsilon` is
>	the machine precision associated to the @ref Float_2 type.

`fill_factor` (integer)
>       On each row, after elimination, only the `n_fillin` largest elements in
>       the L part and the fill largest elements in the U part are kept, in addition to the diagonal elements.
>       The option `fill_factor` is used to compute `n_fillin`:
>       `n_fillin = (nnz*fill_factor)/n + 1`
>       where `n` is the matrix size and `nnz` is its total number of non-zero entires.
>       With `fill_factor=1`, the incomplete factorization as about the same non-zero entries
>       as the initial matrix.
>       With `fill_factor=n`, the factorization is complete, up to the dropped elements.
>       By default `fill_factor=10`.

Example
=======
        int fill_factor = 10;
        double drop_tol = 1e-12;
        solver pa = ilut (a, fill_factor, drop_tol);

Implementation
==============
@showfromfile
*/
} // namespace rheolef

#include "rheolef/solver.h"
#ifdef _RHEOLEF_HAVE_EIGEN
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#include <Eigen/Sparse>
#pragma GCC diagnostic pop

namespace rheolef {
// =======================================================================
// rep
// =======================================================================
template<class T, class M>
class solver_ilut_rep : public solver_abstract_rep<T,M> {
public:
// typedef:

  typedef solver_abstract_rep<T,M>        base;
  typedef typename base::size_type        size_type;
  typedef typename base::determinant_type determinant_type;

// allocator:

  explicit solver_ilut_rep (const csr<T,M>& a, size_type fill_factor, T drop_tol);
  solver_ilut_rep (const solver_ilut_rep&);
  solver_abstract_rep<T,M>* clone() const;
  bool good() const { return true; }
  void update_values (const csr<T,M>& a);

// accessors:

  vec<T,M> trans_solve (const vec<T,M>& rhs) const;
  vec<T,M> solve       (const vec<T,M>& rhs) const;

protected:
// data:
  csr<T,M>           	  _a;
  size_type               _fill_factor;
  T                       _drop_tol;
  Eigen::IncompleteLUT<T> _ilut_a;
};
// -----------------------------------------------------------------------------
// inlined
// -----------------------------------------------------------------------------
template<class T, class M>
inline
solver_ilut_rep<T,M>::solver_ilut_rep (const csr<T,M>& a, size_type fill_factor, T drop_tol)
 : solver_abstract_rep<T,M>(solver_option()),
   _a(a),
   _fill_factor(fill_factor),
   _drop_tol(drop_tol),
   _ilut_a()
{
  update_values (a);
}
template<class T, class M>
inline
solver_ilut_rep<T,M>::solver_ilut_rep (const solver_ilut_rep<T,M>& x)
 : solver_abstract_rep<T,M>(x.option()),
   _a(x._a),
   _fill_factor(x._fill_factor),
   _drop_tol(x._drop_tol),
   _ilut_a()
{
  // Eigen::IncompleteLUT copy cstor is non-copyable, so re-initialize for a copy
  update_values (_a);
}
template <class T, class M>
inline
solver_abstract_rep<T,M>*
solver_ilut_rep<T,M>::clone() const
{
  typedef solver_ilut_rep<T,M> rep;
  return new_macro (rep(*this));
}
// =======================================================================
// preconditioner interface
// =======================================================================
//<verbatim:
template <class T, class M>
solver_basic<T,M> ilut (
  const csr<T,M>& a, 
  size_t fill_factor = 10, 
  T      drop_tol    = 1e3*std::numeric_limits<T>::epsilon());
//>verbatim:

template <class T, class M>
inline
solver_basic<T,M> ilut (const csr<T,M>& a, size_t fill_factor, T drop_tol)
{
  using rep = solver_ilut_rep<T,M>;
  solver_basic<T,M> p;
  p.solver_basic<T,M>::base::operator= (new_macro(rep(a,fill_factor,drop_tol)));
  return p;
}

} // namespace rheolef
#endif // _RHEOLEF_HAVE_EIGEN
#endif // _RHEOLEF_ILUT_H
