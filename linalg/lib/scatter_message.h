#ifndef _RHEOLEF_SCATTER_MESSAGE_H
#define _RHEOLEF_SCATTER_MESSAGE_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
//
// create distributed to sequential scatter context
// inspirated from petsc-2.0/vpscat.c: VecScatterCreate_PtoS(...)
//
#include "rheolef/compiler.h"
#ifdef _RHEOLEF_HAVE_MPI
namespace rheolef {


template<class Container, bool T_is_MPI_simple = false>
class scatter_message {};

// =====================================================================================
// when Container::value_type is a fixed-size type (i.e. a simple MPI data type)
// =====================================================================================
template<class Container>
class scatter_message<Container,false> {
public:
  typedef typename Container::size_type         size_type;
  typedef typename Container::value_type        value_type;
  typedef typename Container::allocator_type    allocator_type; 
  typedef          value_type                   base_value_type;

// data:
protected:
  value_type                                    _init_value;
  Container             	                _values;   // n_data
  std::vector<size_type>         		_indices;  // n_data
  std::vector<size_type>         		_procs;	   // n_proc
  std::vector<size_type>         		_starts;   // n_proc+1

public:
// data accessors:
  const Container&              values () const { return _values; }
        Container&              values ()       { return _values; }
  const std::vector<size_type>& indices() const { return _indices; }
        std::vector<size_type>& indices()       { return _indices; }
  const std::vector<size_type>& procs  () const { return _procs; }
        std::vector<size_type>& procs  ()       { return _procs; }
  const std::vector<size_type>& starts () const { return _starts; }
        std::vector<size_type>& starts ()       { return _starts; }
 
  std::list<std::pair<size_type,mpi::request> > requests; // n_proc
  std::vector<mpi::status>  	                sstatus;  // n_status
    
  std::vector<size_type>       		        local_slots;              // n_local
  std::vector<size_type>       		        local_slots_nonmatching;
  bool 		       	    		        local_nonmatching_computed;// n_local_nonmatching
  size_type 		        	        local_n_nonmatching;
  bool                 	    		        local_is_copy;
  size_type 		       		        local_copy_start;
  size_type                 		        local_copy_length;

// allocator:

  scatter_message(const value_type& init_value = value_type(), const allocator_type& alloc = allocator_type()) 
  : _init_value (init_value),
    _values(alloc),
    _indices(), 
    _procs(), 
    _starts(),
    requests(), 
    sstatus(),
    local_slots(),
    local_slots_nonmatching(),
    local_nonmatching_computed(false),
    local_n_nonmatching(0),
    local_is_copy(false),
    local_copy_start(0),
    local_copy_length(0)
  {}
  void resize (size_type n_data, size_type nproc) {
	_values.resize  (n_data, _init_value);
	_indices.resize (n_data);
	_procs.resize   (nproc);
	_starts.resize  (nproc+1);
  }
  template <class InputIterator>
  void load_values  (InputIterator x);

  template<class OutputIterator, class SetOp>
  void store_values (OutputIterator y, size_type i_receive, SetOp op) const;

// accessors:

  size_type n_proc() const   { return _procs.size(); }
  size_type n_data() const   { return _indices.size(); }
  size_type n_status() const { return sstatus.size(); }
  size_type n_local() const  { return local_slots.size(); }
  size_type n_local_nonmatching() const { return local_slots_nonmatching.size(); }
};
// =====================================================================================
// when Container::value_type is also a container variable-sized type (e.g. index_set)
// =====================================================================================
template<class Container>
class scatter_message<Container,true> : public scatter_message<std::vector<typename Container::size_type>, false> {
public:
  typedef scatter_message<std::vector<typename Container::size_type>, false> base;

  typedef typename base::size_type              size_type;
  typedef typename Container::value_type        value_type;        // e.g. index_set
  typedef typename value_type::value_type       base_value_type;   // e.g. size_type when value_type=index_set

// data:
protected:
  std::vector<base_value_type>             	_multi_values;   // n_multi_data
  std::vector<size_type>         		_multi_indices;  // n_multi_data
  std::vector<size_type>         		_multi_procs;	 // n_multi_proc
  std::vector<size_type>         		_multi_starts;   // n_multi_proc+1
  std::vector<size_type>         		_ptr;            // n_data+1
  std::vector<size_type>         		_multi_irecv2base_irecv; // empty-msg, when zero data sz
 
public:
// data accessors:
  const std::vector<base_value_type>&  values () const { return _multi_values; }
        std::vector<base_value_type>&  values ()       { return _multi_values; }

  const std::vector<size_type>& indices() const { return _multi_indices; }
        std::vector<size_type>& indices()       { return _multi_indices; }
  const std::vector<size_type>& procs  () const { return _multi_procs; }
        std::vector<size_type>& procs  ()       { return _multi_procs; }
  const std::vector<size_type>& starts () const { return _multi_starts; }
        std::vector<size_type>& starts ()       { return _multi_starts; }

  base& get_base() { return *this; }

// allocators:

  scatter_message() 
  : base(),
    _multi_values(), 
    _multi_indices(), 
    _multi_procs(), 
    _multi_starts(),
    _ptr(),
    _multi_irecv2base_irecv()
  {}

  void multi_init ();

  template <class InputIterator>
  void load_values  (InputIterator x);

  template<class OutputIterator, class SetOp>
  void store_values (OutputIterator y, size_type i_receive, SetOp op) const;

// accessors:

  size_type n_proc() const   { return _multi_procs.size(); }
  size_type n_data() const   { return _multi_indices.size(); }
};


} // namespace rheolef
// -------------------------------------------------------------
// not inlined : longer code
// -------------------------------------------------------------
#include "rheolef/scatter_message.icc"

#endif // _RHEOLEF_HAVE_MPI
#endif // _RHEOLEF_SCATTER_MESSAGE_H
