#ifndef RHEO_MSG_FROM_CONTEXT_PATTERN_H
#define RHEO_MSG_FROM_CONTEXT_PATTERN_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

# include "rheolef/msg_util.h"
namespace rheolef {

/*F:
NAME: msg_from_context -- gather (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Computes the receive compressed message pattern for gather
  and scatter.
ALGORITHM:
  msg_from_context_pattern

  "input": the ownership distribution and the indexes arrays
  |   msg_size(0:nproc)
  "output": the receive context (from) and an auxilliary array proc2from_proc
  |   from_proc(0:send_nproc-1), from_ptr(0:send_nproc), 
  |   proc2from_proc(0:nproc-1)
  begin
  |     i := 0
  |     from_ptr(0) := 0
  |     for iproc := 0 to nproc-1 do
  |       if msg_size(iproc) <> 0 then
  |         from_proc(i) := iproc
  |         from_ptr(i+1) := from_ptr(i) + msg_size(i)
  |         proc2from_proc(iproc) := i
  |         i := i+1
  |       endif
  |     endfor
  end
NOTE:
  At the end of the algorithm, we have i = send_nproc.
COMPLEXITY:
  Complexity is O(nproc).

METHODS: @msg_from_context_pattern
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   6 january 1999
END:
*/

//<msg_from_context_pattern:
template <
    class InputIterator1,
    class OutputIterator1,
    class OutputIterator2,
    class OutputIterator3>
void
msg_from_context_pattern (
    InputIterator1 		msg_size,		// nproc
    InputIterator1 		last_msg_size,	
    OutputIterator1      	from_proc,		// send_nproc
    OutputIterator2      	from_ptr,		// send_nproc+1
    OutputIterator3      	proc2from_proc)		// nproc
{
    typedef typename std::iterator_traits<InputIterator1>::value_type Size;
    Size iproc = 0;
    Size i = 0;
    Size ptr_val = 0;
    (*from_ptr++) = ptr_val;
    while (msg_size != last_msg_size) {
        Size sz = (*msg_size++);
        if (sz != 0) {
            (*from_proc++) = iproc;
            ptr_val += sz;
            (*from_ptr++) = ptr_val;
            (*proc2from_proc) = i;
	    i++;
        }
        proc2from_proc++;
        iproc++;
    }
}
//>msg_from_context_pattern:
} // namespace rheolef
#endif // RHEO_MSG_FROM_CONTEXT_PATTERN_H
