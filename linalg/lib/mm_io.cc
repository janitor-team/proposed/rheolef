///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
// matrix market io utilities

# include "rheolef/mm_io.h"
# include <sstream>

using namespace std;
namespace rheolef {

struct matrix_market
read_matrix_market_header (idiststream& ips)
{
  struct matrix_market mm;
  mm.format = matrix_market::max_format;
  if (!ips.do_load()) return mm; // when my_proc is not the io_proc
  istream& is = ips.is();
  bool already_have_a_spec_line = false;
  std::string symmetry;
  do {
    char c;
    is >> std::ws >> c;
    if (c != '%') {
      // no header: starts directly with sizes
      check_macro (isdigit(c), "matrix_market read: not in matrix market format");
      is.unget();
      break;
    }
    is >> std::ws >> c;
    bool have_a_spec_line = (c == '%');
    std::string line;
    getline (is, line);
    if (have_a_spec_line && ! already_have_a_spec_line) {
      std::stringstream spec (line);
      std::string head, matrix, type, valued;
      // see http://math.nist.gov/MatrixMarket/formats.html
      // and http://math.nist.gov/MatrixMarket/reports/MMformat.ps.gz , page 7
      spec >> head >> matrix >> type >> valued >> symmetry;
      check_macro (head     == "MatrixMarket", "matrix_market read: expect `%%MatrixMarket', get `"<<head<<"'");
      check_macro (matrix   == "matrix",       "matrix_market read: unsupported `"<<matrix  <<"' extension: only \"matrix\" format yet supported");
      check_macro (type     == "coordinate",   "matrix_market read: unsupported `"<<type    <<"': only \"coordinate\" format yet supported");
      check_macro (valued   == "real",         "matrix_market read: unsupported `"<<valued  <<"': only \"real\" values yet supported");
      already_have_a_spec_line = true;
    }
  } while (is.good());
  // when no header, assume the general format:
  if (symmetry == "") symmetry = "general";
       if (symmetry == "general")        mm.format = matrix_market::general;
  else if (symmetry == "symmetric")      mm.format = matrix_market::symmetric;
  else if (symmetry == "skew-symmetric") mm.format = matrix_market::skew_symmetric;
  else if (symmetry == "Hermitian")      mm.format = matrix_market::hermitian;
  else error_macro ("matrix_market read: unexpected `"<<symmetry  <<"' symmetry");
  return mm;
}

} // namespace rheolef
