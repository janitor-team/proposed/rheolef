#ifndef _RHEO_MPI_ASSEMBLY_END_H
#define _RHEO_MPI_ASSEMBLY_END_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/msg_util.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include <boost/functional.hpp>
#include <boost/iterator/transform_iterator.hpp>
#pragma GCC diagnostic pop

namespace rheolef {

/*F:
NAME: msg_assembly_end -- array or matrix assembly (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Finish a dense array or sparse matrix assembly.
COMPLEXITY:
  **TO DO**
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   23 march 1999
END:
*/

//<mpi_assembly_end:
template <
    class Container,
    class Message,
    class Size>
Size
mpi_assembly_end (
// input:
    Message&            receive,
    Message&            send,
    Size		receive_max_size,
// output:
    Container           x)
{
    typedef Size                          size_type;
    typedef typename Container::data_type data_type;
    // -----------------------------------------------------------------
    // 1) receive data and store it in container
    // -----------------------------------------------------------------

    // note: for wait_any, build an iterator adapter that scan the pair.second in [index,request]
    // and then get an iterator in the pair using iter.base(): retrive the corresponding index
    // for computing the position in the receive.data buffer
    typedef boost::transform_iterator<select2nd<size_type,mpi::request>,
		typename std::list<std::pair<size_type,mpi::request> >::iterator>
            request_iterator;

    while (receive.waits.size() != 0) {
        request_iterator iter_r_waits (receive.waits.begin(), select2nd<size_type,mpi::request>()),
                         last_r_waits (receive.waits.end(),   select2nd<size_type,mpi::request>());
	// waits on any receive...
        std::pair<mpi::status,request_iterator> pair_status = mpi::wait_any (iter_r_waits, last_r_waits);
	// check status
	boost::optional<int> i_msg_size_opt = pair_status.first.template count<data_type>();
	check_macro (i_msg_size_opt, "receive wait failed");
    	int iproc = pair_status.first.source();
	check_macro (iproc >= 0, "receive: source iproc = "<<iproc<<" < 0 !");
	// get size of receive and number in data
	size_type i_msg_size = (size_type)i_msg_size_opt.get();
        typename std::list<std::pair<size_type,mpi::request> >::iterator i_pair_ptr = pair_status.second.base();
        size_type i_receive = (*i_pair_ptr).first;
        size_type i_start = i_receive*receive_max_size; 
        for (size_type j = i_start; j < i_start + i_msg_size; j++) {
            x (receive.data[j]);
        }
        receive.waits.erase (i_pair_ptr);
    }
    // -----------------------------------------------------------------
    // 2) wait on sends
    // -----------------------------------------------------------------
    Size send_nproc = send.waits.size();
    std::vector<mpi::status> send_status (send_nproc);
    if (send.waits.size() != 0) {
      request_iterator iter_s_waits (send.waits.begin(), select2nd<size_type,mpi::request>()),
                       last_s_waits (send.waits.end(),   select2nd<size_type,mpi::request>());
      mpi::wait_all (iter_s_waits, last_s_waits, send_status.begin());
    }
    // -----------------------------------------------------------------
    // 3) clear send & receive messages [waits,data]
    // -----------------------------------------------------------------
    send.waits.clear();
    send.data.clear();
    receive.waits.clear();
    receive.data.clear();
    return x.n_new_entry();
}
//>mpi_assembly_end:
} // namespace rheolef
#endif //_RHEO_MPI_ASSEMBLY_END_H
