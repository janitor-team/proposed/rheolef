# ifndef _RHEOLEF_CG_H
# define _RHEOLEF_CG_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre Saramito
// DATE:   20 april 2009

namespace rheolef {
/**
@linalgfunctionfile cg conjugate gradient algorithm
@addindex conjugate gradient algorithm
@addindex iterative solver
@addindex preconditioner

Synopsis
========
@snippet cg.h verbatim_cg_synopsis

Example
=======

        solver_option sopt;
        sopt.max_iter = 100;
        sopt.tol = 1e-7;
        int status = cg (A, x, b, eye(), sopt);

Description
===========
This function solves the *symmetric positive definite* linear
system `A*x=b` with the conjugate gradient method.
The `cg` function follows the algorithm described on p. 15 in

        R. Barrett, M. Berry, T. F. Chan, J. Demmel, J. Donato,
        J. Dongarra, V. Eijkhout, R. Pozo, C. Romine and H. Van der Vorst,
        Templates for the solution of linear systems: building blocks for iterative methods, 
	SIAM, 1994.

The fourth argument of `cg` is a preconditionner:
here, the @ref eye_5 one is a do-nothing preconditionner, for simplicity.
Finally, the @ref solver_option_4 variable `sopt` transmits the stopping
criterion with `sopt.tol` and `sopt.max_iter`.

On return, the `sopt.residue` and `sopt.n_iter`
indicate the reached residue and the number of iterations
effectively performed.
The return `status` is zero when the prescribed tolerance `tol`
has been obtained, and non-zero otherwise.
Also, the `x` variable contains the approximate solution.
See also the @ref solver_option_4 for more controls
upon the stopping criterion.

Implementation
==============
@showfromfile
The present template implementation is inspired from 
the `IML++ 1.2` iterative method library,
http://math.nist.gov/iml++

@snippet cg.h verbatim_cg_synopsis
@snippet cg.h verbatim_cg
*/
} // namespace rheolef

#include "rheolef/solver_option.h"

namespace rheolef {

// [verbatim_cg_synopsis]
template <class Matrix, class Vector, class Vector2, class Preconditioner>
int cg (const Matrix &A, Vector &x, const Vector2 &Mb, const Preconditioner &M,
	const solver_option& sopt = solver_option())
// [verbatim_cg_synopsis]
// [verbatim_cg]
{
  typedef typename Vector::size_type  Size;
  typedef typename Vector::float_type Real;
  std::string label = (sopt.label != "" ? sopt.label : "cg");
  Vector b = M.solve(Mb);
  Real norm2_b = dot(Mb,b);
  if (sopt.absolute_stopping || norm2_b == Real(0)) norm2_b = 1;
  Vector Mr = Mb - A*x;
  Real  norm2_r = 0;
  if (sopt.p_err) (*sopt.p_err) << "[" << label << "] #iteration residue" << std::endl;
  Vector p;
  for (sopt.n_iter = 0; sopt.n_iter <= sopt.max_iter; sopt.n_iter++) {
    Vector r = M.solve(Mr);
    Real prev_norm2_r = norm2_r;
    norm2_r = dot(Mr, r);
    sopt.residue = sqrt(norm2_r/norm2_b);
    if (sopt.p_err) (*sopt.p_err) << "[" << label << "] " << sopt.n_iter << " " << sopt.residue << std::endl;
    if (sopt.residue <= sopt.tol) return 0;     
    if (sopt.n_iter == 0) {
      p = r;
    } else {
      Real beta = norm2_r/prev_norm2_r;
      p = r + beta*p;
    }
    Vector Mq = A*p;
    Real alpha = norm2_r/dot(Mq, p);
    x  += alpha*p;
    Mr -= alpha*Mq;
  }
  return 1;
}
// [verbatim_cg]

}// namespace rheolef
# endif // _RHEOLEF_CG_H
