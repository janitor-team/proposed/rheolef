#ifndef _RHEOLEF_SOLVER_GMRES_CG_H
#define _RHEOLEF_SOLVER_GMRES_CG_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
#include "rheolef/solver.h"
namespace rheolef {

template<class T, class M>
class solver_gmres_cg_rep : public solver_abstract_rep<T,M> {
public:
// typedef:

  typedef solver_abstract_rep<T,M>        base;
  typedef typename base::size_type        size_type;
  typedef typename base::determinant_type determinant_type;

// allocator:

  explicit solver_gmres_cg_rep (const csr<T,M>& a, const solver_option& opt = solver_option());
  solver_abstract_rep<T,M>* clone() const;
  bool initialized() const { return true; }
  void update_values (const csr<T,M>& a) { _a = a; }
  void set_preconditioner (const solver_basic<T,M>& p) { _precond = p; }

// accessors:

  vec<T,M> trans_solve (const vec<T,M>& rhs) const;
  vec<T,M> solve       (const vec<T,M>& rhs) const;
  determinant_type det() const;

protected:
// data:
  csr<T,M>                  _a;
  mutable solver_basic<T,M> _precond;
};

template<class T, class M>
inline
solver_gmres_cg_rep<T,M>::solver_gmres_cg_rep (const csr<T,M>& a, const solver_option& opt)
 : solver_abstract_rep<T,M>(opt),
   _a(a),
   _precond()
{
  update_values (_a);
}
template <class T, class M>
inline
solver_abstract_rep<T,M>*
solver_gmres_cg_rep<T,M>::clone() const
{
  typedef solver_gmres_cg_rep<T,M> rep;
  return new_macro (rep(*this));
}

} // namespace rheolef
#endif // _RHEOLEF_SOLVER_GMRES_CG_H
