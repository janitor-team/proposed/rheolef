///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
///
/// =========================================================================
// build vec from initializer lists
//
#include "rheolef/vec_concat.h"

namespace rheolef { namespace details {

template <class T, class M>
vec<T,M>
vec_concat<T,M>::build_vec() const
{
  // scalars are attributed to iproc0
  communicator comm;
  size_type my_proc = comm.rank();
  size_type iproc0  = constraint_process_rank (comm);
  // ------------------------------------
  // first pass: compute the vector size
  // ------------------------------------
  size_type s_sz = 0;
  size_type v_sz = 0;
  size_type v_dis_sz = 0;
  for(typename std::list<value_type>::const_iterator iter = _l.begin(); iter != _l.end(); ++iter) {
    const vec_concat_value<T,M>& x = *iter;
    switch (x.variant) {
      case value_type::scalar:
        s_sz++;
        break;
      case value_type::scalars:
        s_sz += x.ss.size();
        break;
      case value_type::vector:
        comm      = x.v.ownership().comm();
        v_sz     += x.v.ownership().size();
        v_dis_sz += x.v.ownership().dis_size();
        break;
    }
  }
  v_dis_sz += s_sz;
  if (my_proc == iproc0) { v_sz += s_sz; }
  distributor ownership (v_dis_sz, comm, v_sz);
  // ------------------------
  // second pass: copy values
  // ------------------------
  vec<T,M> u (ownership);
  typename vec<T,M>::iterator pu = u.begin();
  for(typename std::list<value_type>::const_iterator iter = _l.begin(); iter != _l.end(); ++iter) {
    const vec_concat_value<T,M>& x = *iter;
    switch (x.variant) {
      case value_type::scalar:
        if (my_proc == iproc0) {
          *pu++ = x.s;
        }
        break;
      case value_type::scalars:
        if (my_proc == iproc0) {
          for (typename std::vector<T>::const_iterator
	        iter = x.ss.begin(),
	        last = x.ss.end(); iter != last; ++iter, ++pu) {
            *pu = *iter;
          }
        }
        break;
      case value_type::vector:
        for (typename vec<T,M>::const_iterator
                iter = x.v.begin(),
                last = x.v.end(); iter != last; ++iter, ++pu) {
          *pu = *iter;
        }
        break;
    }
  }
  return u;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------
#define _RHEOLEF_instanciation(T,M)                                                     \
template class vec_concat<T,M>;

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

}} // namespace rheolef::details
