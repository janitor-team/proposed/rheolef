#ifndef _RHEO_MPI_SCATTER_END_H
#define _RHEO_MPI_SCATTER_END_H
///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/scatter_message.h"
#include "rheolef/msg_left_permutation_apply.h"

#include "rheolef/msg_util.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#include <boost/functional.hpp>
#include <boost/iterator/transform_iterator.hpp>
#pragma GCC diagnostic pop

namespace rheolef {

/*F:
NAME: mpi_scatter_end -- gather/scatter finalize (@PACKAGE@ @VERSION@)
DESCRIPTION:
  Finishes communication
  for distributed to sequential scatter context.
AUTHORS:
    LMC-IMAG, 38041 Grenoble cedex 9, France
    | Pierre.Saramito@imag.fr
DATE:   23 march 1999
END:
*/

//<mpi_scatter_end:
template <
    class InputIterator, 
    class OutputIterator, 
    class Message,
    class SetOp, 
    class Tag,
    class Comm>
void
mpi_scatter_end(
    InputIterator            		x,
    OutputIterator           		y,
    Message&			 	from,
    Message&			 	to,
    SetOp		     		op,
    Tag                      		tag,
    Comm                     		comm)
{
    typedef typename Message::base_value_type data_type; // the data type to be received by mpi
    typedef boost::transform_iterator<select2nd<size_t,mpi::request>, std::list<std::pair<size_t,mpi::request> >::iterator>
            request_iterator;

    // -----------------------------------------------------------
    // 1) wait on receives and unpack receives into local space
    // -----------------------------------------------------------
    while (from.requests.size() != 0) {
        request_iterator iter_r_waits (from.requests.begin(), select2nd<size_t,mpi::request>()),
                         last_r_waits (from.requests.end(),   select2nd<size_t,mpi::request>());
	// waits on any receive...
        std::pair<mpi::status,request_iterator> pair_status = mpi::wait_any (iter_r_waits, last_r_waits);
	// check status
	boost::optional<int> i_msg_size_opt = pair_status.first.count<data_type>();
	check_macro (i_msg_size_opt, "receive wait failed");
    	int iproc = pair_status.first.source();
	check_macro (iproc >= 0, "receive: source iproc = "<<iproc<<" < 0 !");
	// get size of receive and number in data
	size_t i_msg_size = (size_t)i_msg_size_opt.get();
        std::list<std::pair<size_t,mpi::request> >::iterator i_pair_ptr = pair_status.second.base();
        size_t i_receive = (*i_pair_ptr).first;
	check_macro (i_msg_size == from.starts()[i_receive+1] - from.starts()[i_receive], "unexpected size");

        // unpack receives into our local space
	from.store_values (y, i_receive, op);
        from.requests.erase (i_pair_ptr);
    }
    // -----------------------------------------------------------
    // 2) wait on sends 
    // -----------------------------------------------------------
    if (to.requests.size() != 0) {
      request_iterator iter_s_waits (to.requests.begin(), select2nd<size_t,mpi::request>()),
                       last_s_waits (to.requests.end(),   select2nd<size_t,mpi::request>());
      mpi::wait_all (iter_s_waits, last_s_waits);
    }
}
//>mpi_scatter_end:
} // namespace rheolef
#endif // _RHEO_MPI_SCATTER_END_H
