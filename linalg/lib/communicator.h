# ifndef _RHEO_COMMUNICATOR_H
# define _RHEO_COMMUNICATOR_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   27 november 1998

namespace rheolef {
/**
@linalgclassfile communicator MPI communicator for C++

Description
===========
In a distributed environment,
this class wraps the MPI usual communicator for C++,
as provided by the boost::mpi library.
In a sequential environment,
a dummy class is provided for compatibility purpose.

Example
=======

    communicator comm;
    cout << "my_proc = " << comm.rank() << endl
         << "nb_proc = " << comm.size() << endl;

Implementation
==============
@showfromfile
@snippet communicator.h verbatim_communicator
*/
} // namespace rheolef
#include "rheolef/compiler_mpi.h"

namespace rheolef {

  struct undefined_memory_model {};
  struct sequential {};

  // compile-time predicate: is_sequential, is_distributed 
  template<class M>
  struct is_sequential {
    static const bool value = false;
  };
  template<class M>
  struct is_distributed {
    static const bool value = false;
  };
  template<>
  struct is_sequential<sequential> {
    static const bool value = true;
  };
  template<>
  struct is_distributed<sequential> {
    static const bool value = false;
  };

  // useful in expressions of type mixing fields and class-functions:
  // promote memory_type to the field one
  template<class M1, class M2>
  struct promote_memory {
    typedef undefined_memory_model type;
  };
  template<>
  struct promote_memory<undefined_memory_model,sequential> {
    typedef sequential type;
  };
  template<>
  struct promote_memory<sequential,undefined_memory_model> {
    typedef sequential type;
  };
  template<>
  struct promote_memory<sequential,sequential> {
    typedef sequential type;
  };

} // namespace rheolef

#ifndef _RHEOLEF_HAVE_MPI
// -----------------------------------------------------------------------
// distributed code & library should compile even without mpi
// -----------------------------------------------------------------------
#  ifndef rheo_default_memory_model
#  define rheo_default_memory_model sequential
#  endif
namespace rheolef {
// [verbatim_communicator]
struct communicator {
  typedef std::vector<int>::size_type size_type;
  int size() const;
  int rank() const;
};
// [verbatim_communicator]
inline int communicator size() const { return 1; }
inline int communicator rank() const { return 0; }
} // namespace rheolef
#else
// -----------------------------------------------------------------------
// distributed code here
// -----------------------------------------------------------------------
namespace rheolef {
  using mpi::communicator;

  struct distributed {};
  
  template<>
  struct is_sequential<distributed> {
   static const bool value = false;
  };
  template<>
  struct is_distributed<distributed> {
    static const bool value = true;
  };
  template<>
  struct promote_memory<undefined_memory_model,distributed> {
    typedef distributed type;
  };
  template<>
  struct promote_memory<distributed,undefined_memory_model> {
    typedef distributed type;
  };
  template<>
  struct promote_memory<distributed,distributed> {
    typedef distributed type;
  };
} // namespace rheolef

#  ifndef rheo_default_memory_model
#  define rheo_default_memory_model distributed
#  endif
#endif // _RHEOLEF_HAVE_MPI

#endif // _RHEO_COMMUNICATOR_H
