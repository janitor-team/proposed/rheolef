///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================
# include "rheolef/disarray.h"
# include "rheolef/load_chunk.h"
namespace rheolef {
// ----------------------------------------------------------------------------
// class member functions
// ----------------------------------------------------------------------------
template <class T, class A>
disarray_rep<T,sequential,A>::disarray_rep (const A& alloc)
  : base(alloc),
    _ownership ()
{
}
template <class T, class A>
disarray_rep<T,sequential,A>::disarray_rep (size_type loc_size1, const T& init_val, const A& alloc)
  : base(loc_size1, init_val, alloc),
    _ownership (distributor (distributor::decide, communicator(), loc_size1))
{
}
template <class T, class A>
disarray_rep<T,sequential,A>::disarray_rep (const distributor& ownership, const T& init_val, const A& alloc)
 : base(ownership.size(),init_val), // TODO: add alloc extra-arg for heap_allocator
  _ownership(ownership)
{
}
template <class T, class A>
disarray_rep<T,sequential,A>::disarray_rep (const disarray_rep<T,sequential,A>& x)
  : base(x.size()),
    _ownership(x._ownership)
{
   std::copy (x.begin(), x.end(), begin()); 
}
template <class T, class A>
void
disarray_rep<T,sequential,A>::resize (const distributor& ownership, const T&  init_val)
{
    // note: also called by disarray_rep<T,distributed,A>, so should works in distributed mode
    _ownership = ownership;
    base::resize (_ownership.size(), init_val);
    std::fill (begin(), end(), init_val);
}
template <class T, class A>
void
disarray_rep<T,sequential,A>::resize (size_type loc_size1, const T& init_val)
{
    // note: also called by disarray_rep<T,distributed,A>, so should works in distributed mode
    _ownership.resize (distributor::decide, _ownership.comm(), loc_size1);
    base::resize (_ownership.size(), init_val);
    std::fill (begin(), end(), init_val);
}
template <class T, class A>
template <class GetFunction>
idiststream&
disarray_rep<T,sequential,A>::get_values (idiststream& ips, GetFunction get_element) {
  std::istream& is = ips.is();
  if (!load_chunk (is, begin(), end(), get_element))
	error_macro("read failed on input stream.");
  return ips;
}
template <class T, class A>
idiststream&
disarray_rep<T,sequential,A>::get_values (idiststream& ips) 
{
  return get_values (ips, _disarray_get_element_type<T>());
}
template <class T, class A>
template <class PutFunction>
odiststream&
disarray_rep<T,sequential,A>::put_values (odiststream& ops, PutFunction put_element) const
{
    std::ostream& os = ops.os();
    for (size_type i = 0; i < size(); i++) {
        put_element (os, operator[](i));
	os << std::endl;
    }
    return ops;
}
template <class T, class A>
odiststream&
disarray_rep<T,sequential,A>::put_values (odiststream& ops) const
{
  return put_values (ops, _disarray_put_element_type<T>());
}
template <class T, class A>
odiststream&
disarray_rep<T,sequential,A>::put_matlab (odiststream& ops) const
{
  ops << "[";
  put_values (ops, _disarray_put_matlab_type<T>());
  return ops << "];";
}
template <class T, class A>
void
disarray_rep<T,sequential,A>::dump (std::string name) const {
    std::ofstream os (name.c_str());
    std::cerr << "! file \"" << name << "\" created." << std::endl;
    odiststream ops(os);
    put_values(ops);
}
template <class T, class A>
template<class A2>
void
disarray_rep<T,sequential,A>::reverse_permutation (                // old_ownership for *this=iold2inew
        disarray_rep<size_type,sequential,A2>& inew2iold) const    // new_ownership
{
  check_macro (inew2iold.size() == size(), "reverse permutation[0:"<<inew2iold.size()
        <<"[ has incompatible dis_range with oriinal permutation[0:"<<size()<<"[");
  for (size_type iold = 0, nold = size(); iold < nold; iold++) {
    size_type inew = operator[] (iold);
    inew2iold [inew] = iold;
  }
}
template <class T, class A>
void
disarray_rep<T,sequential,A>::get_dis_indexes (std::set<size_type>& ext_idx_set) const
{ 
  ext_idx_set.clear(); 
}   
//=======================================
} // namespace rheolef
