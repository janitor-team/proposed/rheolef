///
/// This file is part of Rheolef.
///
/// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
///
/// Rheolef is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// Rheolef is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with Rheolef; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
/// 
/// =========================================================================

#include "rheolef/linalg.h"
namespace rheolef {
// ----------------------------------------------------------------------------
// build from diag
// ----------------------------------------------------------------------------
template<class T>
template<class A>
void
csr_rep<T,sequential>::build_from_diag (const disarray_rep<T,sequential,A>& d)
{
  resize (d.ownership(), d.ownership(), d.ownership().size());
  iterator ia = begin();
  for (size_type i = 0, n = d.size(); i < n; i++) {
    data_iterator q = ia[i];
    (*q).first  = i;
    (*q).second = d[i];
    ia[i+1] = ia[i] + 1;
  }
}
#ifdef _RHEOLEF_HAVE_MPI
template<class T>
template<class A>
void
csr_rep<T,distributed>::build_from_diag (const disarray_rep<T,distributed,A>& d)
{
  base::build_from_diag (d);
  _ext.resize  (d.ownership(), d.ownership(), 0);
  _jext2dis_j.resize(0);
  _dis_nnz = d.dis_size();
  _scatter_initialized = false;
}
#endif // _RHEOLEF_HAVE_MPI
template<class T, class M>
csr<T,M>
diag (const vec<T,M>& d)
{
  csr<T,M> a;
  a.data().build_from_diag (d.data());
  return a;
}
// ----------------------------------------------------------------------------
// instanciation in library
// ----------------------------------------------------------------------------

#define _RHEOLEF_instanciation_a(T,M,A)					\
template void csr_rep<T,M>::build_from_diag (const disarray_rep<T,M,A>&);

#define _RHEOLEF_instanciation(T,M)					\
template csr<T,M> diag (const vec<T,M>&);				\
_RHEOLEF_instanciation_a(T,M,std::allocator<T>)

_RHEOLEF_instanciation(Float,sequential)
#ifdef _RHEOLEF_HAVE_MPI
_RHEOLEF_instanciation(Float,distributed)
#endif // _RHEOLEF_HAVE_MPI

} // namespace rheolef
