#ifndef _RHEO_DISTRIBUTOR_H
#define _RHEO_DISTRIBUTOR_H
//
// This file is part of Rheolef.
//
// Copyright (C) 2000-2009 Pierre Saramito <Pierre.Saramito@imag.fr>
//
// Rheolef is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// Rheolef is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rheolef; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
// 
// =========================================================================
// AUTHOR: Pierre.Saramito@imag.fr
// DATE:   27 november 1998

namespace rheolef {
/**
@linalgclassfile distributor memory distribution table

Description
===========
Given an array size, this class decides which process will
own which part of a distributed memory array 
such as @ref disarray_4.

Example
=======
	
	distributor ownership (100);

TODO
====
Ambiguous  use of size() and dis_size().
Here size() refers to n_process(), while size(iproc)
and dis_size() refer to the size of the index ranges.
Then, size() should be avoided, and we should use n_process() instead.

Implementation
==============
@showfromfile
@snippet distributor.h verbatim_distributor
@snippet distributor.h verbatim_distributor_cont
*/
} // namespace rheolef

# include "rheolef/communicator.h"
# include "rheolef/dis_macros.h"
# include "rheolef/Vector.h"

namespace rheolef {

// [verbatim_distributor]
class distributor : public Vector<std::allocator<int>::size_type> {
public:

// typedefs:

	typedef std::allocator<int>::size_type size_type;
	typedef Vector<size_type>	       _base;
	typedef _base::iterator                iterator;
	typedef _base::const_iterator          const_iterator;
	typedef int                            tag_type;
	typedef communicator                   communicator_type;

// constants:

	static const size_type decide = size_type(-1);

// allocators/deallocators:

        distributor(
		size_type dis_size = 0,
		const communicator_type& c = communicator_type(),
		size_type loc_size = decide);

        distributor(const distributor&);
	~distributor();

        void resize(
		size_type dis_size = 0,
		const communicator_type& c = communicator_type(),
		size_type loc_size = decide);

// accessors:

	const communicator_type& comm() const;

	/// global and local sizes
        size_type dis_size () const;

	/// current process id
        size_type process () const;
	
	/// number of processes
        size_type n_process () const;

 	/// find iproc associated to a global index dis_i: CPU=log(nproc)
	size_type find_owner (size_type dis_i) const;

	/// global index range and local size owned by ip-th process
        size_type first_index (size_type iproc) const;
        size_type last_index (size_type iproc) const;
        size_type size (size_type iproc) const;

	/// global index range and local size owned by current process
        size_type first_index () const;
        size_type last_index () const;
        size_type size () const;

	/// true when dis_i in [first_index(iproc):last_index(iproc)[
        bool is_owned (size_type dis_i, size_type iproc) const;

        // the same with ip=current process
        bool is_owned (size_type dis_i) const;


	/// returns a new tag
	static tag_type get_new_tag();

// comparators:

   	bool operator== (const distributor&) const;
   	bool operator!= (const distributor&) const;
// [verbatim_distributor]

// data:
protected:
	communicator_type _comm;
// [verbatim_distributor_cont]
};
// [verbatim_distributor_cont]

// inline'd
inline
const distributor::communicator_type&
distributor::comm() const
{
  	return _comm;
}
inline
distributor::size_type
distributor::first_index(size_type j) const 
{
    	return at(j);
}
inline
distributor::size_type
distributor::last_index(size_type j) const 
{
    	return at(j+1);
}
inline
distributor::size_type
distributor::size(size_type j) const 
{
    	return last_index(j) - first_index(j);
}
inline
distributor::size_type
distributor::n_process() const 
{
#ifdef _RHEOLEF_HAVE_MPI
    	return _comm.size();
#else // _RHEOLEF_HAVE_MPI
    	return 1;
#endif // _RHEOLEF_HAVE_MPI
}
inline
distributor::size_type
distributor::process() const 
{
#ifdef _RHEOLEF_HAVE_MPI
    	return _comm.rank();
#else // _RHEOLEF_HAVE_MPI
    	return 0;
#endif // _RHEOLEF_HAVE_MPI
}
inline
distributor::size_type
distributor::first_index() const 
{
    	return first_index(process());
}
inline
distributor::size_type
distributor::last_index() const 
{
    	return last_index(process());
}
inline
distributor::size_type
distributor::size() const 
{
    	return size(process());
}
inline
distributor::size_type
distributor::dis_size() const 
{
	return at(n_process());
}
inline
bool
distributor::is_owned (size_type dis_i, size_type ip) const
{
        return dis_i >= first_index(ip) && dis_i < last_index(ip);
}
inline
bool
distributor::is_owned (size_type dis_i) const
{
        return is_owned (dis_i, process());
}
inline
bool
distributor::operator!= (const distributor& x) const
{
  return !operator==(x);
}
inline
bool
distributor::operator== (const distributor& x) const
{
  return (x.n_process() == n_process()) && (x.size() == size()) && (x.dis_size() == dis_size());
}

} // namespace rheolef
#endif // _RHEO_DISTRIBUTOR_H
